function checkAll(ele) {
	//alert('ddd');
	//debugger;

	var checkboxes = document.querySelectorAll("input[type='checkbox']");
	if (ele.checked) {
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].type == 'checkbox') {
				if (checkboxes[i].id != 'do_you_want_receipt' && checkboxes[i].id != 'is_new_admission' && checkboxes[i].id != 'do_you_collect_late_fee' && checkboxes[i].id != 'do_you_want_send_sms') {
					checkboxes[i].checked = true;
				}
			}
		}
	} else {
		for (var i = 0; i < checkboxes.length; i++) {
			console.log(i)
			if (checkboxes[i].type == 'checkbox') {
				if (checkboxes[i].id != 'do_you_want_receipt' && checkboxes[i].id != 'is_new_admission' && checkboxes[i].id != 'do_you_collect_late_fee' && checkboxes[i].id != 'do_you_want_send_sms') {
					checkboxes[i].checked = false;
				}
			}
		}
	}
	calculateTotalPayableAmount();
}


function select_all_fee_item() {
	var myEle = document.getElementById("fee_item_select");
	if (myEle) {
		document.getElementById("fee_item_select").click();
	}
}

window.onload = select_all_fee_item;


function dueAmountCalculate(row_id) {
	//alert(row_id);
	var paid_amount = Number(document.getElementById('paid_amount_' + row_id).value);
	var discount_amount = Number(document.getElementById('discount_amount_' + row_id).value);
	var hidden_paid_amount = Number(document.getElementById('hidden_paid_amount_' + row_id).value);
	document.getElementById('due_amount_' + row_id).value = hidden_paid_amount - (paid_amount + discount_amount);
	calculateTotalPayableAmount();
}


function discountOptionOpenClose(row_id) {
	//alert(row_id);
	var checkBox = document.getElementById('is_selected_' + row_id);
	if (checkBox.checked == true) {
		document.getElementById('discount_amount_' + row_id).readOnly = false;

		//Total amount and total discount amount calculation
		var total_paid_amount = Number(document.getElementById('total_paid_amount').value);
		var this_selected_amount = Number(document.getElementById('paid_amount_' + row_id).value);
		document.getElementById('total_paid_amount').value = (total_paid_amount + this_selected_amount);

	} else {
		document.getElementById('discount_amount_' + row_id).readOnly = true;
		document.getElementById('discount_amount_' + row_id).value = 0;

		//Total amount and total discount amount calculation
		var total_paid_amount = Number(document.getElementById('total_paid_amount').value);
		var this_selected_amount = Number(document.getElementById('paid_amount_' + row_id).value);
		document.getElementById('total_paid_amount').value = (total_paid_amount - this_selected_amount);

	}

}



function calculate_paidable_amount_after_discount(discount_amount, row_id) {
	debugger;
	var number_of_transport_fee_row = Number(document.getElementById('number_of_transport_fee_row').value);
	var start_row = 1;
	var row_num = Number(document.getElementById('num_of_row').value) - number_of_transport_fee_row;
	var total_discount = 0;
	while (start_row <= row_num) {
		if (row_id != start_row) {
			var r_discount_amount = document.getElementById('discount_amount_' + start_row).value;
			if (r_discount_amount != '') {
				total_discount = total_discount + Number(r_discount_amount);
			}
		}
		start_row++;
	}
	document.getElementById('total_discount').value = Number(total_discount) + Number(discount_amount);


	var n_discount_amount = Number(discount_amount);
	var paid_amount = Number(document.getElementById('hidden_paid_amount_' + row_id).value);
	if (n_discount_amount > paid_amount) {
		alert('Discounts can not be more than paidable amount.');
		document.getElementById('discount_amount_' + row_id).value = 0;
		document.getElementById('paid_amount_' + row_id).value = Number(paid_amount);
	} else {

		document.getElementById('paid_amount_' + row_id).value = Number(paid_amount) - Number(n_discount_amount);
	}
	calculateTotalPayableAmount();
}

function calculateTotalPayableAmount() {
	var start_row = 1;
	var row_num = Number(document.getElementById('num_of_row').value);
	var total_paid_amount = 0;
	while (start_row <= row_num) {
		var checkBox = document.getElementById('is_selected_' + start_row);
		if (checkBox.checked == true) {
			var paid_amount = document.getElementById('paid_amount_' + start_row).value;
			total_paid_amount = total_paid_amount + Number(paid_amount);
		}
		start_row++;
	}
	document.getElementById('total_paid_amount').value = total_paid_amount;
}