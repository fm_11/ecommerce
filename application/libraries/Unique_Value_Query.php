<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

 include_once APPPATH.'/libraries/Unique_Value_Query.php';

class Unique_Value_Query
{
    private $vals;
    private $baseArr;

    public function __construct( array $arr = array() )
    {
       $this->setBaseArray( $arr );
    }

    public function setBaseArray( $ba )
    {
       if( count( $ba ) < 1 ) return;
       $this->baseArray = $ba;
       $this->vals = array_unique( array_values( $ba ) );
    }

    public function getBaseArray(){ return $this->baseArray; }

    public function getKeyIndex( $key )
    {
        return array_search( $this->baseArray[ $key ], $this->vals );
    }
}
