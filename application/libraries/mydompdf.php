<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(dirname(__FILE__) . '/dompdf/autoload.inc.php');

class Mydompdf
{
    public function createPDF($html, $filename='', $download=true, $paper='A4', $orientation='landscape')
    {
        $dompdf = new Dompdf\DOMPDF();

        //$dompdf->setBasePath(realpath('http://localhost/demo/core_media/report_css/'));
        $dompdf->load_html($html);
        $dompdf->set_paper($paper, $orientation);
        $dompdf->render();

        //$canvas = $dompdf->get_canvas();
        //$font = Font_Metrics::get_font("SolaimanLipi", "regular");
        //  date_default_timezone_set('Asia/Dhaka');
        //$currentDateTime = date("Y/m/d H:i:s");
        //  $newDateTime = date('d-m-Y h:i A', strtotime($currentDateTime));
        //    $canvas->page_text(16, 800, "Page: {PAGE_NUM} of {PAGE_COUNT}, Print Date: $newDateTime", '', 8, array(0,0,0));
        if ($download) {
            $dompdf->stream($filename.'.pdf', array('Attachment' => 1));
        } else {
            $dompdf->stream($filename.'.pdf', array('Attachment' => 0));
        }
    }
}
