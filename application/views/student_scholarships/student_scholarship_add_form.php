<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_scholarships/student_scholarship_data_save" method="post">
  <div class="table-responsive-sm">
      <table  class="table">
        <thead>
        <tr>
            <th scope="col"><?php echo $this->lang->line('name'); ?></th>
            <th scope="col"><?php echo $this->lang->line('student_code'); ?></th>
            <th scope="col"><?php echo $this->lang->line('roll'); ?></th>
            <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($student_info as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
                <td>
                    <?php echo $row['roll_no']; ?>
                    <input type="hidden" name="roll_no_<?php echo $i; ?>" value="<?php echo $row['roll_no']; ?>"/>
                </td>
                <td>
                    <input type="checkbox" <?php if($row['already_save'] > 0){echo 'checked';} ?> name="is_allow_<?php echo $i; ?>">
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
  </div>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="year"
           value="<?php echo $year; ?>"/>

    <input type="hidden" class="input-text-short" name="class_id"
           value="<?php echo $class_id; ?>"/>

    <input type="hidden" class="input-text-short" name="section_id"
           value="<?php echo $section_id; ?>"/>

    <input type="hidden" class="input-text-short" name="group"
           value="<?php echo $group; ?>"/>

     <input type="hidden" class="input-text-short" name="shift_id"
            value="<?php echo $shift_id; ?>"/>

     <div class="float-right">
        <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
     </div>


</form>
