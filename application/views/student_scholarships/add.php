<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_scholarships/add" method="post">
    <div class="form-row">
        <div class="form-group col-md-4">
          <label><?php echo $this->lang->line('year'); ?></label>
          <select class="js-example-basic-single w-100" name="year" id="year" required="1">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <?php
              $i = 0;
              if (count($years)) {
                  foreach ($years as $list) {
                      $i++; ?>
                      <option
                          value="<?php echo $list['value']; ?>"
                          <?php if (isset($year)) {
                          if ($year == $list['value']) {
                              echo 'selected';
                          }
                      } ?>>
                      <?php echo $list['text']; ?>
                    </option>
                      <?php
                  }
              }
              ?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
          <select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <?php
              $i = 0;
              if (count($class_section_shift_marge_list)) {
                  foreach ($class_section_shift_marge_list as $list) {
                      $i++; ?>
                      <option
                          value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
                          <?php if (isset($class_shift_section_id)) {
                          if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
                              echo 'selected';
                          }
                      } ?>>
                      <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
                    </option>
                      <?php
                  }
              }
              ?>
          </select>
        </div>

        <div class="form-group col-md-4">
            <label><?php echo $this->lang->line('group'); ?></label>
            <select name="group" required="1" class="js-example-basic-single w-100">
                <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
                <?php foreach ($group_list as $row) { ?>
                    <option value="<?php echo $row['id']; ?>" <?php if(isset($group)){ if($group == $row['id']){ echo 'selected'; }} ?>><?php echo $row['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="float-right">
        <input class="btn btn-primary"  type="submit" class="submit" value="<?php echo $this->lang->line('process'); ?>">
    </div>
</form>

<?php
if(isset($scholarship_form)){
  echo $scholarship_form;
}
 ?>
