<form action="<?php echo base_url('teacher_attendance_settings/index'); ?>" method="post">
<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th scope="col">Teacher ID</th>
			<th scope="col"><?php echo $this->lang->line('name'); ?></th>
			<th scope="col"><?php echo $this->lang->line('designation'); ?></th>
			<th scope="col">In Time</th>
			<th scope="col">Out Time</th>
			<th scope="col">Flexible Min</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = 0;
		foreach ($teachers as $row):
			?>
			<tr>

			<tr>
				<td>
					<?php echo $row['teacher_code']; ?>
					<input  type="hidden" style="width: 155px;" value="<?php echo $row['id']; ?>" class="form-control"  name="teacher_id_<?php echo $i; ?>">
				</td>
				<td><?php echo $row['name']; ?></td>
		     	<td><?php echo $row['teacher_post']; ?></td>
				<td>
					<input style="width: 135px;padding: 10px;" value="<?php echo $row['in_time']; ?>" class="form-control"  name="in_time_<?php echo $i; ?>" type="time">
				</td>
				<td>
					<input style="width: 135px;padding: 10px;" value="<?php echo $row['out_time']; ?>" class="form-control"  name="out_time_<?php echo $i; ?>" type="time">
				</td>
				<td>
					<input style="width: 80px;padding: 10px;" value="<?php echo $row['flexible_min_for_late']; ?>" class="form-control"  name="flexible_min_for_late_<?php echo $i; ?>" type="text">
				</td>
			</tr>
		<?php $i++; endforeach; ?>
		</tbody>
	</table>
	<input type="hidden" value="<?php echo $i; ?>" name="total_row">
	<div class="btn-group float-right">
		<input class="btn btn-primary" name="process" type="submit" value="Save">
	</div>
</div>
</form>
