<?php
if ($action == 'edit') {
    ?>


    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>users/edit" enctype="multipart/form-data" method="post">
      <div class="form-row">
          <div class="form-group col-md-6">
            <label>Name</label>
            <input autocomplete="off" class="form-control" type="text" required="1" name="name" id="name"
                       value="<?php echo $user_info[0]->name; ?>">
          </div>
          <div class="form-group col-md-6">
            <label>User Name</label>
            <input autocomplete="off" class="form-control" type="text" required="1" name="user_name" id="user_name"
                       value="<?php echo $user_info[0]->user_name; ?>">
          </div>
      </div>


      <div class="form-row">
          <div class="form-group col-md-6">
            <label>Mobile</label>
            <input autocomplete="off" class="form-control" type="text" required="1" name="mobile" id="mobile"
                       value="<?php echo $user_info[0]->mobile; ?>">
          </div>

          <div class="form-group col-md-6">
            <label>Password</label>
            <input autocomplete="off" class="form-control" type="password" name="password" id="password">
          </div>
      </div>

      <div class="form-row">
          <div class="form-group col-md-5">
            <label>User Role</label>
            <select class="js-example-basic-single w-100" name="role_id" required="1">
                <option value="">-- Please Select --</option>
                <?php foreach ($user_roles as $user_role) { ?>
                    <option <?php if ($user_role->id == $user_info[0]->user_role) { ?>
                            selected="selected"
                            <?php } ?>value="<?php echo $user_role->id; ?>"><?php echo $user_role->role_name; ?></option>
                <?php } ?>
            </select>
            <input class="form-control" type="hidden" required="1" name="id" id="id"
                       value="<?php echo $user_info[0]->id; ?>">
          </div>

          <div class="form-group col-md-5">
            <label>New Picture</label>
            <input type="file" name="txtPhoto" id="txtPhoto" class="form-control">
          </div>

          <div class="form-group col-md-2 text-center">
            <?php if($user_info[0]->photo_location == ''){ ?>
            <img src="<?php echo base_url(); ?>core_media/admin_v3/images/faces/user.png"
            class="img" width="80" height="85"/>
          <?php }else{ ?>
          <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $user_info[0]->photo_location; ?>"
          class="img" width="80" height="85"/>
        <?php } ?>
          </div>

      </div>

      <div class="float-right">
           <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
      </div>
    </form>


    <?php
} else {
        ?>

    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>users/add" enctype="multipart/form-data" method="post">
      <div class="form-row">
          <div class="form-group col-md-6">
            <label>Name</label>
            <input autocomplete="off" class="form-control" type="text" required="1" name="name" id="name">
          </div>
          <div class="form-group col-md-6">
            <label>User Name</label>
            <input autocomplete="off" class="form-control" type="text" required="1" name="user_name" id="user_name">
          </div>
      </div>

      <div class="form-row">
          <div class="form-group col-md-6">
            <label>Password</label>
            <input autocomplete="off" class="form-control" type="password" required="1" name="password" id="password">
          </div>

          <div class="form-group col-md-6">
            <label>Mobile</label>
            <input autocomplete="off" class="form-control" type="text" required="1" name="mobile" id="mobile">
          </div>
      </div>

      <div class="form-row">
          <div class="form-group col-md-6">
            <label>User Role</label>
            <select class="js-example-basic-single w-100" name="role_id" required="1">
                <option value="">-- Please Select --</option>
                <?php foreach ($user_roles as $user_role) { ?>
                    <option value="<?php echo $user_role->id; ?>"><?php echo $user_role->role_name; ?></option>
                <?php } ?>
            </select>
          </div>

          <div class="form-group col-md-6">
            <label>Picture</label>
            <input type="file" name="txtPhoto" required id="txtPhoto" class="form-control">
          </div>
      </div>

      <div class="float-right">
        <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
        <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
      </div>

    </form>

    <?php
    }
?>
