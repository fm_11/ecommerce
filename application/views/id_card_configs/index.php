<script type="text/javascript" src="<?php echo base_url(); ?>core_media/js/jscolor/jscolor.js"></script>

<script>
	function resetConfirm() {
		var result = confirm("Are you sure to reset all color?");
		if (result == true) {
			return true;
		} else {
			return false;
		}
	}
	
	function checkNumberOfField(selected_id) {
		var class_show = document.getElementById("class_show").value;
		var section_show = document.getElementById("section_show").value;
		var group_show = document.getElementById("group_show").value;
		var shift_show = document.getElementById("shift_show").value;
		var roll_show = document.getElementById("roll_show").value;
		var year_show = document.getElementById("year_show").value;
		var mobile_show = document.getElementById("mobile_show").value;
		var fathers_name_show = document.getElementById("fathers_name_show").value;
		var mothers_name_show = document.getElementById("mothers_name_show").value;
		var date_of_birth_show = document.getElementById("date_of_birth_show").value;
		var total_selected_field = 0;
		if(class_show == '1' && selected_id != 'class_show'){ total_selected_field++; }
		if(section_show == '1' && selected_id != 'section_show'){ total_selected_field++; }
		if(group_show == '1' && selected_id != 'group_show'){ total_selected_field++; }
		if(shift_show == '1' && selected_id != 'shift_show'){ total_selected_field++; }
		if(roll_show == '1' && selected_id != 'roll_show'){ total_selected_field++; }
		if(year_show == '1' && selected_id != 'year_show'){ total_selected_field++; }
		if(mobile_show == '1' && selected_id != 'mobile_show'){ total_selected_field++; }
		if(fathers_name_show == '1' && selected_id != 'fathers_name_show'){ total_selected_field++; }
		if(mothers_name_show == '1' && selected_id != 'mothers_name_show'){ total_selected_field++; }
		if(date_of_birth_show == '1' && selected_id != 'date_of_birth_show'){ total_selected_field++; }
		//alert(total_selected_field);
		if(total_selected_field >= 7){
			document.getElementById(selected_id).value = "0";
			alert("You can't select more than 7 filed");
		}
	}
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>id_card_configs/index" method="post">

	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Main Color</label>
			<input type="text" autocomplete="off" readonly required name="main_color" id="main_color" class="form-control color" value="<?php echo $config_info[0]['main_color']; ?>">
		</div>

		<div class="form-group col-md-4">
			<label>Main Text Color</label>
			<input type="text" autocomplete="off" readonly required name="main_text_color" id="main_text_color" class="form-control color" value="<?php echo $config_info[0]['main_text_color']; ?>">
		</div>

		<div class="form-group col-md-4">
			<label>Secondary Text Color</label>
			<input type="text" autocomplete="off" readonly required name="secondary_text_color" id="secondary_text_color" class="form-control color" value="<?php echo $config_info[0]['secondary_text_color']; ?>">
		</div>

	</div>

	<div class="form-row">
		<div class="form-group col-md-2">
			<label>Class Show ?</label>
			<select name="class_show" id="class_show" onchange="checkNumberOfField('class_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['class_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['class_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label>Section Show ?</label>
			<select name="section_show" id="section_show" onchange="checkNumberOfField('section_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['section_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['section_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label>Group Show ?</label>
			<select name="group_show" id="group_show" onchange="checkNumberOfField('group_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['group_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['group_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label>Shift Show ?</label>
			<select name="shift_show" id="shift_show" onchange="checkNumberOfField('shift_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['shift_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['shift_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label>Roll Show ?</label>
			<select name="roll_show" id="roll_show" onchange="checkNumberOfField('roll_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['roll_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['roll_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label>Year Show ?</label>
			<select name="year_show" id="year_show" onchange="checkNumberOfField('year_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['year_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['year_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>

	</div>

	<div class="form-row">
		<div class="form-group col-md-3">
			<label>Mobile Show ?</label>
			<select name="mobile_show" id="mobile_show" onchange="checkNumberOfField('mobile_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['mobile_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['mobile_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label>Father's Name Show ?</label>
			<select name="fathers_name_show" id="fathers_name_show" onchange="checkNumberOfField('fathers_name_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['fathers_name_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['fathers_name_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label>Mother's Name Show ?</label>
			<select name="mothers_name_show" id="mothers_name_show" onchange="checkNumberOfField('mothers_name_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['mothers_name_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['mothers_name_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label>Date of Birth Show ?</label>
			<select name="date_of_birth_show" id="date_of_birth_show" onchange="checkNumberOfField('date_of_birth_show')" class="form-control" required="required">
				<option value="1" <?php if (isset($config_info)) {
					if ($config_info[0]['date_of_birth_show'] == '1') {
						echo 'selected';
					}
				} ?>>Yes
				</option>
				<option value="0" <?php if (isset($config_info)) {
					if ($config_info[0]['date_of_birth_show'] == '0') {
						echo 'selected';
					}
				} ?>>No
				</option>
			</select>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Back Part Top Text</label>
			<input type="text" autocomplete="off"  required name="back_part_top_text" class="form-control" value="<?php echo $config_info[0]['back_part_top_text']; ?>">
		</div>

		<div class="form-group col-md-6">
			<label>Back Part Bottom Text</label>
			<input type="text" autocomplete="off"  required name="back_part_bottom_text" class="form-control" value="<?php echo $config_info[0]['back_part_bottom_text']; ?>">
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-8">
			<label>Bar Code Text</label>
			<textarea class="form-control" required rows="6" name="bar_code_value"><?php echo $config_info[0]['bar_code_value']; ?></textarea>
		</div>

		<div class="form-group col-md-4">
			<label>Bar Code</label><br>
			<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/QR_CODE/<?php echo $config_info[0]['bar_code']; ?>" height="120px" width="120px">
		</div>
	</div>


	<input type="hidden" name="id" required class="smallInput wide" size="20%" value="<?php echo $config_info[0]['id']; ?>">

	<div class="float-right">
		<button type="submit" onclick="return resetConfirm()" class="btn btn-outline-warning btn-icon-text">
			<i class="ion ion-md-refresh"></i>
			Reset
		</button>
		<input class="btn btn-primary" type="submit" name="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
