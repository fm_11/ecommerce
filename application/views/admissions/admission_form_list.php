<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>admissions/admission_form_add"><span>Add New Form</span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Form Name</th>
        <th width="80" scope="col">Class</th>
        <th width="300" scope="col">Download</th>
        <th width="300" scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($add_form as $row):
        $i++;
        ?>
        <tr>
        <tr>
            <td style="vertical-align:middle">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align:middle"><?php echo $row['form_name']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['class_name']; ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url() . MEDIA_FOLDER; ?>/add_form/<?php echo $row['location']; ?>"
                   target="_blank"><?php echo $row['location']; ?></a>
            </td>
        <td style="vertical-align:middle">
            <a href="<?php echo base_url(); ?>admissions/admission_form_delete/<?php echo $row['id']; ?>" onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
        </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>