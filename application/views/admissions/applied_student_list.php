<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function msgStatusUpdate(id, payment_status) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>admissions/updateMsgStatusAppliedStudentStatus?id=" + id + '&&payment_status=' + payment_status, true);
        xmlhttp.send();
    }

</script>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>admissions/get_apply_student_list" method="post"
      enctype="multipart/form-data">
    <?php
    $class_id = $this->session->userdata('class_id');
    $pin = $this->session->userdata('pin');
    $year = $this->session->userdata('year');
    if ($class_id != '') {
        $class_id = $class_id;
    } else {
        $class_id = "";
    }
    if ($pin != '') {
        $pin = $pin;
    } else {
        $pin = "";
    }
    if ($year != '') {
        $year = $year;
    } else {
        $year = "";
    }
    ?>
    <table>
        <tr>
            <td style="vertical-align: bottom;" width="20%">
                <select class="smallInput" name="txtClass">
                    <option value="">-- Class --</option>
                    <?php
                    $i = 0;
                    if (count($class)) {
                        foreach ($class as $list) {
                            $i++;
                            ?>
                            <option
                                value="<?php echo $list['id']; ?>" <?php if($class_id == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="vertical-align: bottom;">
                <input type="text" autocomplete="off"  class="smallInput" value="<?php echo $pin; ?>" placeholder="PIN" name="txtPIN"/>
            </td>
            <td>
                &nbsp;
            </td>
            <td style="vertical-align: bottom;">
                <input type="text" autocomplete="off"  class="smallInput" value="<?php echo $year; ?>" placeholder="Year" name="txtYear"/>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <input type="submit" class="submit" value="Search">
            </td>
        </tr>
    </table>
</form>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Name</th>
        <th width="100" scope="col">Class</th>
        <th width="200" scope="col">PIN</th>
        <th width="100" scope="col">Year</th>
        <th width="200" scope="col">Payment Status</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>


    <?php
    $i = 0;
    foreach ($students as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34" style="vertical-align:middle">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align:middle"><?php echo $row['name']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['class_name']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['pin']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['year']; ?></td>
            <td style="vertical-align:middle" id="status_sction_<?php echo $row['id']; ?>">
                <?php
                if ($row['payment_status'] == 1) {
                    ?>
                    <a class="approve_icon" title="Approve" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['payment_status']; ?>)"></a>
                <?php
                } else {
                    ?>
                    <a class="reject_icon" title="Reject" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['payment_status']; ?>)"></a>
                <?php
                }
                ?>
            </td>

            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>admissions/applied_student_view/<?php echo $row['id']; ?>"
                   class="edit_icon" title="View"></a>
                <a href="<?php echo base_url(); ?>admissions/applied_student_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>

            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="7" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>