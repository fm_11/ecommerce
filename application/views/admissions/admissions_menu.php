<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>admissions/get_admission_process"><span>Admission Process</span></a></li>
    <li><a href="<?php echo base_url(); ?>admissions/get_admission_form_list"><span>Admission Form</span></a></li>
    <li><a href="<?php echo base_url(); ?>admissions/get_admission_contact"><span>Admission Contact</span></a></li>
    <li><a href="<?php echo base_url(); ?>admissions/get_apply_student_list"><span>Applied Student List</span></a></li>
</ul>

