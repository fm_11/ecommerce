<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>student_fees/fee_collection_add"><span><?php echo $this->lang->line('fees').' '.$this->lang->line('collect'); ?></span></a>

	   <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>student_fees/special_care_fee_add"><span><?php echo $this->lang->line('special_care').' '.$this->lang->line('fees').' '.$this->lang->line('collect'); ?></span></a>

       <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>student_fees/special_care_fee_collection_index"><span><?php echo $this->lang->line('special_care').' '.$this->lang->line('fees').' '.$this->lang->line('collection').' '.$this->lang->line('list'); ?></span></a>

</h2>
<br>
<hr>
<h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/fee_collection_index" method="post">
        <input type="text" autocomplete="off"  name="receipt_no" style="display: inline !important;" placeholder="Receipt No."
	    size="15" value="" style="display: -moz-stack !important;" required class="smallInput" id="receipt_no">
	    <input type="submit" name="Delete" value="Delete"/>
    </form>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="50" scope="col"><?php echo $this->lang->line('receipt_no'); ?></th>
        <th width="150" scope="col"><?php echo $this->lang->line('student').' '.$this->lang->line('name'); ?></th>
        <th width="60" scope="col"><?php echo $this->lang->line('roll'); ?></th>
        <th width="70" scope="col"><?php echo $this->lang->line('class'); ?></th>
        <th width="50" scope="col"><?php echo $this->lang->line('month'); ?></th>
        <th width="50" scope="col"><?php echo $this->lang->line('year'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($fees as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['receipt_no']; ?></td>
            <td><?php echo $row['student_name']; ?></td>
            <td><?php echo $row['roll_no']; ?></td>
            <td><?php echo $row['class']; ?></td>

            <td><?php echo DateTime::createFromFormat('!m', $row['month'])->format('F'); ?></td>
            <td><?php echo $row['year']; ?></td>
            <td style="vertical-align:middle">

                <a href="<?php echo base_url(); ?>student_fees/getStudentReceipt/<?php echo $row['id']; ?>/S"
                   target="_blank" title="Student Receipt"><?php echo $this->lang->line('student').' '.$this->lang->line('copy'); ?></a> |

                <a href="<?php echo base_url(); ?>student_fees/getStudentReceipt/<?php echo $row['id']; ?>/SC"
                  target="_blank" title="School Receipt"><?php echo $this->lang->line('institute').' '.$this->lang->line('copy'); ?></a> |

                <a href="<?php echo base_url(); ?>student_fees/fee_collection_delete/<?php echo $row['id']; ?>"
                  onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="9" style="color: #000000;" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>

    </tbody>
</table>
