<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/student_auto_absent_fine_approve" method="post">
    <label>Class</label>
    <select class="smallInput" name="class_id" id="class_id"
             required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>" <?php if(isset($class_id)){ if($class_id == $list['id']){ echo 'selected'; } } ?>><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>


    <label><?php echo $this->lang->line('month'); ?></label>
    <select class="smallInput" name="month" id="month" required="1">
        <?php
        $i = 1;
        while ($i <= 12) {
            $dateObj = DateTime::createFromFormat('!m', $i);
            ?>
            <option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
                echo 'selected';
            } ?>><?php echo $dateObj->format('F'); ?></option>
            <?php
            $i++;
        }
        ?>
    </select>
    

    <label><?php echo $this->lang->line('year'); ?></label>
    <select class="smallInput" name="year" id="year" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>
    
      <br>
    <br>
    <input type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
  

</form>

<br>


<div id="printableArea">
    <?php
    if (isset($adata)) {
        echo $process;
    }
    ?>
</div>
