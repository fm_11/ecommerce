<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
<?php
foreach ($students as $list) {
    ?>
    <option
        value="<?php echo $list['id']; ?>">
        <?php echo $list['name'] . ' (Roll-' . $list['roll_no'] . ')' . ' (ID-' . $list['student_code'] . ')'; ?>
    </option>
<?php
}
?>
