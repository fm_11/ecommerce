<script type="text/javascript">
    function get_fee_allocation_form_by_year_class(class_id,year){
        debugger;
        if(class_id == "" || year == ""){
            alert("Please select class and year.");
        }
		//alert(category_id);
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("allocation_form").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>student_fees/get_fee_allocation_form_by_year_class?class_id=" + class_id + '&&year=' + year, true);
        xmlhttp.send();
    }

</script>
  
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/fee_allocation_add" method="post">
	    <label><?php echo $this->lang->line('year'); ?></label>
        <select class="smallInput" name="year" id="year" onchange="get_fee_allocation_form_by_year_class(document.getElementById('class_id').value,this.value)"  required="1">
            <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
            <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
			<option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
        </select>
	
        <label><?php echo $this->lang->line('class'); ?></label>
        <select class="smallInput"  onchange="get_fee_allocation_form_by_year_class(this.value,document.getElementById('year').value)" id="class_id" name="class_id" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($class)) {
                foreach ($class as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>
	   <br><br>
	    <div id="allocation_form">

        </div>

        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
