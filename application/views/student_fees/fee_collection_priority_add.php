<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some item.");
            return false;
        } else {
            return true;
        }
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()"
      action="<?php echo base_url(); ?>student_fees/fee_collection_priority_add" method="post">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tr>
            <th colspan="4">
                <b><?php echo $this->lang->line('name'); ?></b>
                <input type="text" autocomplete="off"  name="name" class="smallInput wide" >
            </th>
        </tr>

        <tr>
            <th colspan="4">
                <b><?php echo $this->lang->line('year'); ?></b>
                <br>
                <select class="smallInput" name="year" id="year" style="width: 200px;" required="1">
                    <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                    <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
                    <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                </select>
            </th>
        </tr>

        <!-- Fee Category fee part -->
        <tr>
            <th colspan="4" style="text-align: center;">
                <b><?php echo $this->lang->line('fees').' '.$this->lang->line('category'); ?></b>
            </th>
        </tr>
        <tr>
            <th width="50" scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
            <th width="200" scope="col"><?php echo $this->lang->line('name'); ?></th>
            <th width="200" scope="col"><?php echo $this->lang->line('num_of').' '.$this->lang->line('priority'); ?></th>
            <th width="200" scope="col"><?php echo $this->lang->line('percentage').' '.$this->lang->line('of').' '.$this->lang->line('amount'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($fee_category as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td width="34">
                    <input type="checkbox" name="is_allow_<?php echo $i; ?>">
                    <input type="hidden" value="0" name="is_absent_<?php echo $i; ?>">
                    <input type="hidden" value="0" name="is_play_truant_<?php echo $i; ?>">
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" name="is_month_<?php echo $i; ?>" value="0">
                    <input type="hidden" name="category_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>">
                </td>
                <td>
                    <input type="text" autocomplete="off"  name="priority_<?php echo $i; ?>">
                </td>
                <td>
                    <input type="text" autocomplete="off"  name="percentage_<?php echo $i; ?>">
                </td>
            </tr>
        <?php endforeach; ?>
        <!-- End Fee Category fee part -->

        <!-- Absent Fine fee part -->
        <tr>
            <td width="34">
                <input type="checkbox" name="is_allow_<?php echo $i = $i + 1; ?>">
                <input type="hidden" value="1" name="is_absent_<?php echo $i; ?>">
                <input type="hidden" value="0" name="is_play_truant_<?php echo $i; ?>">
            </td>
            <td>
                <?php echo $this->lang->line('absent_fine'); ?>
                <input type="hidden" name="is_month_<?php echo $i; ?>" value="0">
            </td>
            <td>
                <input type="text" autocomplete="off"  name="priority_<?php echo $i; ?>">
            </td>
            <td>
                <input type="text" autocomplete="off"  name="percentage_<?php echo $i; ?>">
            </td>
        </tr>
        <!-- End Absent Fine fee part -->

        <!-- Play Truant Fine -->
        <tr>
            <td width="34">
                <input type="checkbox" name="is_allow_<?php echo $i = $i + 1; ?>">
                <input type="hidden" value="0" name="is_absent_<?php echo $i; ?>">
                <input type="hidden" value="1" name="is_play_truant_<?php echo $i; ?>">
            </td>
            <td>
                <?php echo $this->lang->line('play_truant_fine'); ?>
                <input type="hidden" name="is_month_<?php echo $i; ?>" value="0">
            </td>
            <td>
                <input type="text" autocomplete="off"  name="priority_<?php echo $i; ?>">
            </td>
            <td>
                <input type="text" autocomplete="off"  name="percentage_<?php echo $i; ?>">
            </td>
        </tr>
        <!-- End Play Truant Fine fee part -->


        <!-- Monthly fee part -->
        <tr>
            <th colspan="4" style="text-align: center;">
                <b><?php echo $this->lang->line('monthly').' '.$this->lang->line('fees'); ?></b>
            </th>
        </tr>

        <tr>
            <th width="50" scope="col">#</th>
            <th width="200" scope="col"><?php echo $this->lang->line('month'); ?>Month</th>
            <th width="200" scope="col"><?php echo $this->lang->line('num_of').' '.$this->lang->line('priority'); ?></th>
            <th width="200" scope="col"><?php echo $this->lang->line('percentage').' '.$this->lang->line('of').' '.$this->lang->line('amount'); ?></th>
        </tr>

        <?php
        $j = 0;
        while ($j < 12) {
            $i++;
            $j++;
            ?>
            <tr>

            <tr>
                <td width="34">
                    <input type="checkbox" name="is_allow_<?php echo $i; ?>">
                    <input type="hidden" value="0" name="is_absent_<?php echo $i; ?>">
                    <input type="hidden" value="0" name="is_play_truant_<?php echo $i; ?>">
                </td>
                <td>
                    <?php echo date("F", mktime(0, 0, 0, $j, 10)); ?>
                    <input type="hidden" name="is_month_<?php echo $i; ?>" value="1">
                    <input type="hidden" name="month_<?php echo $i; ?>" value="<?php echo $j; ?>">
                </td>
                <td>
                    <input type="text" autocomplete="off"  name="priority_<?php echo $i; ?>">
                </td>
                <td>
                    <input type="text" autocomplete="off"  name="percentage_<?php echo $i; ?>">
                </td>
            </tr>
        <?php } ?>
        <!-- End Monthly fee part -->

        <tr>
            <td colspan="4" style="text-align: right;">
                <input type="hidden" class="input-text-short" name="loop_time"
                       value="<?php echo $i; ?>"/>
                <input type="submit" class="submit" value="Save">
            </td>
        </tr>

    </table>

</form>