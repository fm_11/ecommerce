<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/class_wise_monthly_fee_add" method="post">
    <label>Year</label>
    <select class="smallInput" name="year" required="1">
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>

    <label>Class</label>
    <select class="smallInput" name="class_id" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>


    <label><?php echo $this->lang->line('amount'); ?></label>
    <input type="text" autocomplete="off"  name="amount" class="smallInput wide" size="20%" value="" required="1">

    <label><?php echo $this->lang->line('resident').' '.$this->lang->line('amount'); ?></label>
    <input type="text" autocomplete="off"  name="resident_amount" class="smallInput wide" size="20%" value="0" required="1">

    <label><?php echo $this->lang->line('remarks'); ?></label>
    <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="remarks"></textarea>

    <br>
    <br>
    <input type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form>
