<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>student_fees/class_wise_monthly_fee_add"><span><?php echo $this->lang->line('monthly').' '.$this->lang->line('fees').' '.$this->lang->line('allocate'); ?></span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="150" scope="col"><?php echo $this->lang->line('class'); ?></th>
        <th width="120" scope="col"><?php echo $this->lang->line('amount'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('resident').' '.$this->lang->line('student').' '.$this->lang->line('amount'); ?></th>
        <th width="120" scope="col"><?php echo $this->lang->line('year'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($fees as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['class_name']; ?></td>
            <td><?php echo $row['amount']; ?></td>
            <td><?php echo $row['resident_amount']; ?></td>
            <td><?php echo $row['year']; ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>student_fees/class_wise_fee_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="9" style="color: #000000;" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>

    </tbody>
</table>

