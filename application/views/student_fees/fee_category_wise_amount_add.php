<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/fee_category_wise_amount_add" method="post">
    <label><?php echo $this->lang->line('year'); ?></label>
    <select class="smallInput" name="year" required="1">
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>

    <label><?php echo $this->lang->line('class'); ?></label>
    <select class="smallInput" name="class_id" required="1">
        <option value="all">-- <?php echo $this->lang->line('all').' '.$this->lang->line('class'); ?> --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>

    <label><?php echo $this->lang->line('category'); ?></label>
    <select class="smallInput" name="category_id" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        $i = 0;
        if (count($fee_category_info)) {
            foreach ($fee_category_info as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>


    <label><?php echo $this->lang->line('amount'); ?></label>
    <input type="text" autocomplete="off"  name="amount" class="smallInput wide" size="20%" value="" required="1">

    <label><?php echo $this->lang->line('resident').' '.$this->lang->line('student').' '.$this->lang->line('amount'); ?></label>
    <input type="text" autocomplete="off"  name="resident_amount" class="smallInput wide" size="20%" value="0" required="1">

    <label><?php echo $this->lang->line('remarks'); ?></label>
    <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="remarks"></textarea>

    <br>
    <br>
    <input type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form>
