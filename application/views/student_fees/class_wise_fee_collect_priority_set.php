 <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/class_wise_fee_collect_priority_set" method="post">
           <label>Year</label>
		<select class="smallInput" name="year" required="1">
			<option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
			<option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
			<option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
		</select>
		
		<label>Class</label>
        <select class="smallInput" name="class_id" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($class_list)) {
                foreach ($class_list as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>
		
        
        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
