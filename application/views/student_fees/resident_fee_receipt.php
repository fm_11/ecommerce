<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $school_info->school_name; ?></title>
    <link href="<?php echo base_url(); ?>media/css/receipt.css" rel="stylesheet">

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
</head>
<body>
<table width="73%" cellpadding="0" cellspacing="0" id="box-table-a">
    <tr>
        <th width="100%" style="text-align:right" scope="col">
            <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
        </th>
    </tr>
</table>
<br>
<div class="wrapper" id="printableArea">
    <table class="header">
        <tbody>
        <tr>
            <td nowrap="nowrap" style="text-align:center;" colspan="2" width="100%">
            <table width="100%">
                  <tr>
                      <td width="30%">
                      <p><img src="<?php echo base_url(); ?>media/css/receipt_logo_s.png"></p>
                      </td>
                      
                      <td>
                        <strong><?php echo $this->lang->line('pay_to'); ?></strong><br>
                    <b><?php echo $school_info->school_name; ?></b><br>
                   <?php echo $this->lang->line('eiin'); ?>  <?php echo $school_info->eiin_number; ?><br>
                    <?php echo $school_info->address; ?><br>
                    <?php echo $school_info->web_address; ?>
                      </td>
                  </tr>
            </table>
                
            </td>          
        </tr>
        
         <tr>
            <td nowrap="nowrap" width="50%">				
				<span class="title"><?php echo $this->lang->line('receipt_no'); ?> #<?php echo $collection_info[0]['receipt_no']; ?></span><br>
				<?php echo $this->lang->line('receipt').' '.$this->lang->line('date'); ?>: <?php echo date("d/m/Y", strtotime($collection_info[0]['date'])); ?>				
            </td>
            <td width="50%" align="center">
                 <font class="paid"><?php echo $this->lang->line('receipt'); ?></font><br>
                (<b><?php echo $student_info[0]['student_code']; ?></b>)<br>
                <span style="color:red;"><?php echo $copy_for; ?></span>
            </td>
        </tr>
        </tbody>
    </table>

	
	
	
	<table class="items">
	    <thead>
		   <tr>
		       <td colspan="2">								       
                    <b><?php echo $this->lang->line('name'); ?>: <?php echo $student_info[0]['name']; ?></b>,
                   <?php echo $this->lang->line('class'); ?> : <?php echo $student_info[0]['class']; ?>,
                    <?php echo $this->lang->line('section'); ?>: <?php echo $student_info[0]['section']; ?>,
                    <?php echo $this->lang->line('roll'); ?>: <?php echo $student_info[0]['roll_no']; ?> 
                                    
                </div>
			   </td>
		   </tr>
		</thead>
	   
	    
        <tbody>
		
		 <tr>
                <td colspan="2"><b><?php echo $this->lang->line('resident').' '.$this->lang->line('fees'); ?></b></td>
          </tr>
		
        <tr class="title textcenter">
            <td width="70%"><?php echo $this->lang->line('month').'/'.$this->lang->line('year'); ?></td>
            <td width="30%"><?php echo $this->lang->line('amount'); ?></td>
        </tr>

        <?php
          $total = 0;
        if (isset($collection_info) && !empty($collection_info)) {
            ?>
            
            <?php
            foreach ($collection_info as $row):
                ?>

                <tr>
                    <td align="center">
					   <?php 
					    $monthNum  = $row['month'];
						$dateObj   = DateTime::createFromFormat('!m', $monthNum);
						echo $dateObj->format('F').', '.$row['year']; // March					 
					   ?>
					</td>
                    <td class="textcenter">
                        <?php
                        $total += $row['amount'];
                        ?>
                                                                      ৳<?php echo number_format($row['amount'], 2); ?>BDT
                    </td>
                </tr>

            <?php endforeach; ?>


        <?php
        }
        ?>


        <tr class="title">
            <td class="textright"><?php echo $this->lang->line('total'); ?>:</td>
            <td class="textcenter">৳<?php echo $this->lang->line('sl'); ?><?php echo number_format($total, 2); ?>BDT</td>
        </tr>

        </tbody>
    </table>
    <br> <br>
   ...............................................<br>
   <?php echo $this->lang->line('accountant'); ?>
</div>
</body>
</html>