<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/student_fee_waiver_from_total_amount_data_save" method="post">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <thead>
        <tr>
            <th width="50" scope="col">#</th>
          <th width="200" scope="col"><?php echo $this->lang->line('name'); ?></th>
            <th width="200" scope="col"><?php echo $this->lang->line('student_code'); ?></th>
            <th width="100" scope="col"><?php echo $this->lang->line('roll'); ?></th>
            <th width="200" scope="col">
                <?php echo $this->lang->line('waiver').' '.$this->lang->line('amount').' ('.$this->lang->line('tk').')'; ?> 
                 
            </th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($student_info as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td width="34">
                    <input type="checkbox" <?php if ($row['amount'] > 0) {
                        echo 'checked';
                    } ?> name="is_allow_<?php echo $i; ?>">
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" class="input-text-short" size="8"
                           style="text-align: right; width:75px;"
                           name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
                <td>
                    <?php echo $row['roll_no']; ?>
                </td>
                <td>
                    <input type="text" autocomplete="off"  name="waiver_amount_<?php echo $i; ?>"
                           value="<?php if ($row['amount'] > 0) {
                               echo $row['amount'];
                           } else {
                               echo 0;
                           } ?>">
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <br>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="year"
           value="<?php echo $year; ?>"/>

    <input type="hidden" class="input-text-short" name="class_id"
           value="<?php echo $class_id; ?>"/>

    <input type="hidden" class="input-text-short" name="section_id"
           value="<?php echo $section_id; ?>"/>

    <input type="hidden" class="input-text-short" name="group"
           value="<?php echo $group; ?>"/>

    <input type="submit" class="submit" value="Save">

</form>
<br/>
<div class="clear"></div>
