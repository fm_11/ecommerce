<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>


<form action="<?php echo base_url(); ?>student_fees/student_fee_due_list" method="post">
    <label>Class</label>
    <select name="class_id" id="class_id" class="smallInput" required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php for ($B = 0; $B < count($ClassList); $B++) { ?>
            <option value="<?php echo $ClassList[$B]['id']; ?>"><?php echo $ClassList[$B]['name']; ?></option>
        <?php } ?>
    </select>
    
     <label><?php echo $this->lang->line('section'); ?></label>
     <select name="section_id" id="section_id" class="smallInput"  required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php for ($C = 0; $C < count($SectionList); $C++) { ?>
            <option value="<?php echo $SectionList[$C]['id']; ?>"><?php echo $SectionList[$C]['name']; ?></option>
        <?php } ?>
    </select>
     <label>Year</label>
     <select name="year" id="year" class="smallInput" required="required">
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>
    <label>Months</label>
    <select class="smallInput" multiple="multiple" name="month[]" id="month" required="1" style="height:180px;">
        <?php
        $i = 1;
        while ($i <= 12) {
            $dateObj = DateTime::createFromFormat('!m', $i);
            ?>
            <option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
                echo 'selected';
            } ?>><?php echo $dateObj->format('F'); ?></option>
            <?php
            $i++;
        }
        ?>
    </select>
    
    <button type="submit"><?php echo $this->lang->line('view'); ?></button>
</form>
<br>
<?php
if (isset($due_list)) {
    ?>
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a">
        <tr>
            <th width="100%" style="text-align:right" scope="col">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
            </th>
        </tr>
    </table>
<?php
}
?>

<div id="printableArea">
    <?php
    if (isset($due_list)) {
        ?>
            <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                <thead>
                <tr>
                    <td style="text-align:center">
                        <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                        <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?></b><br>
                        <b style="font-size:13px;"><?php echo $this->lang->line('class'); ?>: <?php echo $HeaderInfo['ClassName']; ?>,
                            <?php echo $this->lang->line('section'); ?>: <?php echo $HeaderInfo['SectionName']; ?><br>
                            <?php echo $this->lang->line('payment').' '.$this->lang->line('due').' '.$this->lang->line('list_for'); ?>
                              <?php echo DateTime::createFromFormat('!m', $HeaderInfo['month'])->format('F'); ?>
                            , <?php echo $HeaderInfo['year']; ?></b>
    
                    </td>
                </tr>
                </thead>
           </table>
            <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <tr>
                <th width="15" style="vertical-align: middle;" scope="col" rowspan="2"><?php echo $this->lang->line('sl'); ?></th>
                <th width="150" style="vertical-align: middle;" scope="col" rowspan="2"><?php echo $this->lang->line('name'); ?></th>
                <th width="30" style="vertical-align: middle;" scope="col" rowspan="2"><?php echo $this->lang->line('roll'); ?></th> 
               
                <?php
                    if (isset($due_list[0]['fee_list']['A']) && !empty($due_list[0]['fee_list']['A'])) {
                      foreach ($due_list[0]['fee_list']['A'] as $row):
                    ?>
                    <th style="text-align:center;" width="130"   scope="col"><?php echo $row['sub_category_name']; ?></th>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[0]['fee_list']['T']) && !empty($due_list[0]['fee_list']['T'])) {
                      foreach ($due_list[0]['fee_list']['T'] as $row):
                    ?>
                    <th style="text-align:center;" width="130" scope="col"><?php echo $row['sub_category_name']; ?></th>
                    <?php
                      endforeach;
                } ?>
                
                
                <?php
                    if (isset($due_list[0]['fee_list']['Q']) && !empty($due_list[0]['fee_list']['Q'])) {
                      foreach ($due_list[0]['fee_list']['Q'] as $row):
                    ?>
                    <th  style="text-align:center;" width="130" scope="col"><?php echo $row['sub_category_name']; ?></th>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[0]['fee_list']['M']) && !empty($due_list[0]['fee_list']['M'])) {
                      foreach ($due_list[0]['fee_list']['M'] as $row):
                    ?>
                    <th style="text-align:center;" width="130" scope="col"><?php echo $row['sub_category_name']; ?></th>
                    <?php
                      endforeach;
                } ?>
            </tr>
            
            
            <tr>
                 <?php
                    if (isset($due_list[0]['fee_list']['A']) && !empty($due_list[0]['fee_list']['A'])) {
                      foreach ($due_list[0]['fee_list']['A'] as $row):
                    ?>
                    <th scope="col">
                        <?php echo $this->lang->line('allocted'); ?> <br>
                        <?php echo $this->lang->line('payment'); ?> <br>
                         <?php echo $this->lang->line('due'); ?>
                    </th>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[0]['fee_list']['T']) && !empty($due_list[0]['fee_list']['T'])) {
                      foreach ($due_list[0]['fee_list']['T'] as $row):
                    ?>
                    <th scope="col">
                         <?php echo $this->lang->line('allocted'); ?> <br>
                        <?php echo $this->lang->line('payment'); ?> <br>
                         <?php echo $this->lang->line('due'); ?>
                    </th>
                    <?php
                      endforeach;
                } ?>
                
                
                <?php
                    if (isset($due_list[0]['fee_list']['Q']) && !empty($due_list[0]['fee_list']['Q'])) {
                      foreach ($due_list[0]['fee_list']['Q'] as $row):
                    ?>
                     <th scope="col">
                         <?php echo $this->lang->line('allocted'); ?> <br>
                        <?php echo $this->lang->line('payment'); ?> <br>
                         <?php echo $this->lang->line('due'); ?>
                    </th>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[0]['fee_list']['M']) && !empty($due_list[0]['fee_list']['M'])) {
                      foreach ($due_list[0]['fee_list']['M'] as $row):
                    ?>
                     <th scope="col">
                          <?php echo $this->lang->line('allocted'); ?> <br>
                        <?php echo $this->lang->line('payment'); ?> <br>
                         <?php echo $this->lang->line('due'); ?>
                    </th>
                    <?php
                      endforeach;
                } ?>
            </tr>
            
            </thead>
            <tbody>
            
            <?php
                $i = 0;
                foreach ($due_list as $row):
            ?>
               
            <tr>
                <td  style="vertical-align: middle;" width="34">
                    <?php echo $i + 1; ?>
                </td>
                <td style="vertical-align: middle;"><?php echo $row['name']; ?></td>
                <td style="vertical-align: middle;"><?php echo $row['roll_no']; ?></td>
                   
                <?php
                    if (isset($due_list[$i]['fee_list']['A']) && !empty($due_list[$i]['fee_list']['A'])) {
                      foreach ($due_list[$i]['fee_list']['A'] as $annual_row):
                           $style = "";
                          if($annual_row['actual_allocated_amount_for_this_student'] == $annual_row['already_paid_amount']){
                             $style = "background-color:#339933;"; 
                          }else if($annual_row['already_paid_amount'] > 0){
                              $style = "background-color:#ffcc00;"; 
                          }else{
                               $style = "background-color:#ff0000;"; 
                          }
                ?>
                       <td style"<?php echo $style; ?>">
                           <?php echo $annual_row['actual_allocated_amount_for_this_student']; ?><br>
                           <?php echo $annual_row['already_paid_amount']; ?><br>
                                <b>
                                <?php 
                                echo $annual_row['due_amount'];
                                ?>
                               </b>
                       </td>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[$i]['fee_list']['T']) && !empty($due_list[$i]['fee_list']['T'])) {
                      foreach ($due_list[$i]['fee_list']['T'] as $tri_annual_row):
                          $style = "";
                          if($tri_annual_row['actual_allocated_amount_for_this_student'] == $tri_annual_row['already_paid_amount']){
                             $style = "background-color:#339933;"; 
                          }else if($tri_annual_row['already_paid_amount'] > 0){
                              $style = "background-color:#ffcc00;"; 
                          }else{
                               $style = "background-color:#ff0000;"; 
                          }
                    ?>
                       <td style="<?php echo $style; ?>">
                           <?php echo $tri_annual_row['actual_allocated_amount_for_this_student']; ?><br>
                           <?php echo $tri_annual_row['already_paid_amount']; ?><br>
                           <b><?php echo $tri_annual_row['due_amount']; ?></b>
                       </td>
                    <?php
                      endforeach;
                } ?>
                
                
                <?php
                    if (isset($due_list[$i]['fee_list']['Q']) && !empty($due_list[$i]['fee_list']['Q'])) {
                      foreach ($due_list[$i]['fee_list']['Q'] as $qua_row):
                          $style = "";
                          if($qua_row['actual_allocated_amount_for_this_student'] == $qua_row['already_paid_amount']){
                             $style = "background-color:#339933;"; 
                          }else if($qua_row['already_paid_amount'] > 0){
                              $style = "background-color:#ffcc00;"; 
                          }else{
                               $style = "background-color:#ff0000;"; 
                          }
                    ?>
                       <td style="<?php echo $style; ?>">
                           <?php echo $qua_row['actual_allocated_amount_for_this_student']; ?><br>
                           <?php echo $qua_row['already_paid_amount']; ?><br>
                           <b><?php echo $qua_row['due_amount']; ?></b>
                        </td>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[$i]['fee_list']['M']) && !empty($due_list[$i]['fee_list']['M'])) {
                      foreach ($due_list[$i]['fee_list']['M'] as $monthly_row):
                          $style = "";
                          if($monthly_row['actual_allocated_amount_for_this_student'] == $monthly_row['already_paid_amount']){
                             $style = "background-color:#339933;"; 
                          }else if($monthly_row['already_paid_amount'] > 0){
                              $style = "background-color:#ffcc00;"; 
                          }else{
                               $style = "background-color:#ff0000;"; 
                          }
                    ?>
                       <td style="<?php echo $style; ?>">
                           <?php echo $monthly_row['actual_allocated_amount_for_this_student']; ?><br>
                           <?php echo $monthly_row['already_paid_amount']; ?><br>
                           <b><?php echo $monthly_row['due_amount']; ?></b>
                       </td>
                    <?php
                      endforeach;
                } ?>
            </tr>
               
            <?php $i++; endforeach; ?>
            
            
            
            </tbody>
        </table>

    <?php
    }
    ?>
</div>