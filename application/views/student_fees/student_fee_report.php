<script>
    function student_by_class_section() {
        var ClassID = document.getElementById("class_id").value;
        var SectionID = document.getElementById("section_id").value;
        if (ClassID != '' && SectionID != '') {
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            }
            else {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("StudentSpace").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo base_url(); ?>students/ajax_student_by_class_section?class_id=" + ClassID + "&&section_id=" + SectionID, true);
            xmlhttp.send();
        }
    }

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>


<form action="<?php echo base_url(); ?>report/student_fee_report" method="post">
    &nbsp;&nbsp;  <?php echo $this->lang->line('year'); ?>: <select name="year" id="year" required="required">
        <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
    </select>
    &nbsp;&nbsp;  <?php echo $this->lang->line('month'); ?>: <select name="month" id="month">
        <option value="">--   <?php echo $this->lang->line('all'); ?> --</option>
        <?php
        $i = 1;
        while ($i <= 12) {
            $dateObj = DateTime::createFromFormat('!m', $i);
            ?>
            <option value="<?php echo $i; ?>"><?php echo $dateObj->format('F'); ?></option>
            <?php
            $i++;
        }
        ?>
    </select>

    &nbsp;&nbsp;  <?php echo $this->lang->line('class'); ?>: <select name="class_id" id="class_id" onchange="student_by_class_section()" required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php for ($B = 0; $B < count($ClassList); $B++) { ?>
            <option value="<?php echo $ClassList[$B]['id']; ?>"><?php echo $ClassList[$B]['name']; ?></option>
        <?php } ?>
    </select>
    &nbsp;&nbsp;<?php echo $this->lang->line('section'); ?>: <select name="section_id" id="section_id" onchange="student_by_class_section()" required="required">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php for ($C = 0; $C < count($SectionList); $C++) { ?>
            <option value="<?php echo $SectionList[$C]['id']; ?>"><?php echo $SectionList[$C]['name']; ?></option>
        <?php } ?>
    </select>

    &nbsp;&nbsp;<?php echo $this->lang->line('student'); ?>:<span id="StudentSpace"><select name="student_id">
            <option value="">--<?php echo $this->lang->line('all'); ?>--</option>
        </select></span>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <button type="submit"><?php echo $this->lang->line('view'); ?></button>
</form>
<br>
<?php
if (isset($fee_list)) {
    ?>
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a">
        <tr>
            <th width="100%" style="text-align:right" scope="col">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
            </th>
        </tr>
    </table>
<?php
}
?>

<div id="printableArea">
    <?php
    if (isset($fee_list)) {
        ?>
        <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
            <tr>
                <td colspan="7" style="text-align:center">
                    <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                    <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?></b><br>
                    <b style="font-size:13px;"><?php echo $this->lang->line('class'); ?>: <?php echo $HeaderInfo['ClassName']; ?>,
                        <?php echo $this->lang->line('section'); ?>: <?php echo $HeaderInfo['SectionName']; ?><br>
                        <?php echo $this->lang->line('payment').' '.$this->lang->line('report').' '.$this->lang->line('of'); ?>  <?php
                                if($HeaderInfo['month'] != ''){
                                    echo DateTime::createFromFormat('!m', $HeaderInfo['month'])->format('F');
                                }else{
                                    echo "All Month";
                                }

                            ?>
                        , <?php echo $HeaderInfo['year']; ?></b>

                </td>
            </tr>
            <tr>
                <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
                <th width="100" scope="col"><?php echo $this->lang->line('name'); ?></th>
                <th width="100" scope="col"><?php echo $this->lang->line('roll_no'); ?></th>
                <th width="100" scope="col"><?php echo $this->lang->line('payment').' '.$this->lang->line('amount'); ?></th>
                <th width="100" scope="col"><?php echo $this->lang->line('receipt_no'); ?></th>
                <th width="100" scope="col"><?php echo $this->lang->line('payment').' '.$this->lang->line('month'); ?></th>
                <th width="100" scope="col"><?php echo $this->lang->line('payment').' '.$this->lang->line('year'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($fee_list as $row):
                $i++;
                ?>
                <tr>

                <tr>
                    <td width="34">
                        <?php echo $i; ?>
                    </td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['roll_no']; ?></td>
                    <td><?php echo $row['paid_amount']; ?></td>
                    <td><?php echo $row['receipt_no']; ?></td>
                    <td><?php echo DateTime::createFromFormat('!m', $row['month'])->format('F'); ?></td>
                     <td><?php echo $row['year']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    <?php
    }
    ?>
</div>