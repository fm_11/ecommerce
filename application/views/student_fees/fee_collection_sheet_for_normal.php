<style>
.for_fee_collection{
  padding: 7px;
}
</style>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table">
      <tr>
          <td>
              <?php echo $this->lang->line('payment_method'); ?>
          </td>
          <td>
              <select class="for_fee_collection" name="mode_of_pay" id="mode_of_pay" required="1">
                  <option value="C"><?php echo $this->lang->line('cash'); ?></option>
              </select>
          </td>
          <td align="right">
             <?php echo $this->lang->line('receipt_no'); ?>
          </td>
          <td>
              <input type="text" autocomplete="off"  name="receipt_no" value="<?php echo $receipt_no; ?>">
          </td>
          <td colspan="2">
             <?php echo $this->lang->line('return').' '.$this->lang->line('amount'); ?> : <b><?php echo $collection_sheet['student_return_money']; ?></b>
          </td>
      </tr>


      <?php
      $total_allocated_fee = 0;
      $total_paid_fee = 0;
      $total_due_fee = 0;
      $total_already_paid = 0;

      //    echo '<pre>';
      //    print_r($collection_sheet['category_wise_fee_list']);
      //    die;
      if (isset($collection_sheet['category_wise_fee_list']) && !empty($collection_sheet['category_wise_fee_list'])) {
          ?>
          <tr>
              <th colspan="6">
                  <b><?php echo $this->lang->line('category').' '.$this->lang->line('wise').' '.$this->lang->line('fees'); ?></b>
              </th>
          </tr>
          <tr>
              <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('category'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('allocated').' '.$this->lang->line('fees'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('already').' '.$this->lang->line('paid').' '.$this->lang->line('amount'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('paid').' '.$this->lang->line('amount'); ?>
              </th>
              <th width="100" scope="col"><?php echo $this->lang->line('due').' '.$this->lang->line('amount'); ?></th>
          </tr>

          <?php
          $i = 0;
          while ($i < count($collection_sheet['category_wise_fee_list'])) {
              ?>
              <tr>

              <tr>
                  <td width="34">
                      <?php echo $i + 1; ?>
                  </td>
                  <td>
                      <?php echo $collection_sheet['category_wise_fee_list'][$i][$i]['category_name']; ?>
                  </td>
                  <td>
                      <?php
                      $total_allocated_fee += $collection_sheet['category_wise_fee_list'][$i][$i]['allocated_fee'];
              $total_paid_fee += $collection_sheet['category_wise_fee_list'][$i][$i]['paid_amount'];
              $total_due_fee += $collection_sheet['category_wise_fee_list'][$i][$i]['due_amount'];
              $total_already_paid += $collection_sheet['category_wise_fee_list'][$i][$i]['already_paid_amount'];

              echo $collection_sheet['category_wise_fee_list'][$i][$i]['allocated_fee']; ?>
                  </td>

                  <td>
                      <?php echo $collection_sheet['category_wise_fee_list'][$i][$i]['already_paid_amount']; ?>
                  </td>

                  <td>
                      <?php echo $collection_sheet['category_wise_fee_list'][$i][$i]['paid_amount']; ?>
                  </td>

                  <td style="vertical-align:middle">
                      <?php echo $collection_sheet['category_wise_fee_list'][$i][$i]['due_amount']; ?>
                  </td>

              </tr>
              <?php $i++;
          }
      } ?>



      <?php
      if (isset($collection_sheet['monthly_fee_list']) && !empty($collection_sheet['monthly_fee_list'])) {
          ?>
          <tr>
              <th colspan="6">
                  <b><?php echo $this->lang->line('month').' '.$this->lang->line('wise').' '.$this->lang->line('fees'); ?></b>
              </th>
          </tr>
          <tr>
              <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('month'); ?>Month</th>
              <th width="100" scope="col"><?php echo $this->lang->line('allocated').' '.$this->lang->line('fees'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('already').' '.$this->lang->line('paid').' '.$this->lang->line('amount'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('paid').' '.$this->lang->line('amount'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('due').' '.$this->lang->line('amount'); ?></th>
          </tr>
          <?php
          $i = 0;
          while ($i < count($collection_sheet['monthly_fee_list'])) {
              ?>
              <tr>

              <tr>
                  <td width="34">
                      <?php echo $i + 1; ?>
                  </td>
                  <td>
                      <?php
                      $dateObj   = DateTime::createFromFormat('!m', $collection_sheet['monthly_fee_list'][$i][$i]['month']);
              echo $dateObj->format('F'); // March?>
                  </td>
                  <td>
                      <?php
                      $total_allocated_fee += $collection_sheet['monthly_fee_list'][$i][$i]['allocated_fee'];
              $total_paid_fee += $collection_sheet['monthly_fee_list'][$i][$i]['paid_amount'];
              $total_due_fee += $collection_sheet['monthly_fee_list'][$i][$i]['due_amount'];
              $total_already_paid += $collection_sheet['monthly_fee_list'][$i][$i]['already_paid_amount'];
              echo $collection_sheet['monthly_fee_list'][$i][$i]['allocated_fee']; ?>
                  </td>
                  <td>
                      <?php echo $collection_sheet['monthly_fee_list'][$i][$i]['already_paid_amount']; ?>
                  </td>
                  <td>
                      <?php echo $collection_sheet['monthly_fee_list'][$i][$i]['paid_amount']; ?>
                  </td>
                  <td style="vertical-align:middle">
                      <?php echo $collection_sheet['monthly_fee_list'][$i][$i]['due_amount']; ?>
                  </td>

              </tr>
              <?php $i++;
          }
      } ?>


      <?php
      if (isset($collection_sheet['absent_and_play_truant_fine_list']) && !empty($collection_sheet['absent_and_play_truant_fine_list'])) {
          ?>
          <tr>
              <th colspan="6">
                  <b><?php echo $this->lang->line('absent').' '.$this->lang->line('or').' '.$this->lang->line('play_truant_fine'); ?></b>
              </th>
          </tr>
          <tr>
              <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
              <th width="200" scope="col"><?php echo $this->lang->line('is').' '.$this->lang->line('absent').' '.$this->lang->line('or').' '.$this->lang->line('play_truant_fine'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('allocated').' '.$this->lang->line('fine'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('already').' '.$this->lang->line('paid').' '.$this->lang->line('amount'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('paid').' '.$this->lang->line('amount'); ?></th>
              <th width="100" scope="col"><?php echo $this->lang->line('due').' '.$this->lang->line('amount'); ?></th>
          </tr>
          <?php
          $i = 0;
          while ($i < count($collection_sheet['absent_and_play_truant_fine_list'])) {
              ?>
              <tr>

              <tr>
                  <td width="34">
                      <?php echo $i + 1; ?>
                  </td>
                  <td>
                      <?php
                      if ($collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['is_absent_fine'] == 1) {
                          echo 'Absent Fine';
                      } else {
                          echo 'Play Truant Fine';
                      } ?>
                  </td>
                  <td>
                      <?php
                      $total_allocated_fee += $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['allocated_fine'];
              $total_paid_fee += $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['paid_amount'];
              $total_due_fee += $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['due_amount'];
              $total_already_paid += $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['already_paid_amount'];
              echo $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['allocated_fine']; ?>
                  </td>
                  <td>
                      <?php echo $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['already_paid_amount']; ?>
                  </td>
                  <td>
                      <?php echo $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['paid_amount']; ?>
                  </td>
                  <td>
                      <?php echo $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['due_amount']; ?>
                  </td>

              </tr>
              <?php $i++;
          }
      } ?>

      <tr>
          <td colspan="2" style="text-align: right;">
              <b><?php echo $this->lang->line('total'); ?></b>
          </td>
          <td>
              <b><?php echo $total_allocated_fee; ?></b>
          </td>
          <td>
              <b><?php echo $total_already_paid; ?></b>
          </td>
          <td>
              <b><?php echo $total_paid_fee; ?></b>
          </td>
          <td>
              <b><?php echo $total_due_fee; ?></b>
          </td>
  </table>
</div>
