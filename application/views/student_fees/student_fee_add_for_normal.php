<script type="text/javascript">
    function getStudentByClassAndSection(class_id, section_id) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("student_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>student_fees/getStudentByClassAndSection?class_id=" + class_id + '&&section_id=' + section_id, true);
        xmlhttp.send();
    }



    function getCollectionSheet() {
        var class_id = document.getElementById('class_id').value;
        var section_id = document.getElementById('section_id').value;
        var student_id = document.getElementById('student_id').value;
        var year = document.getElementById('year').value;
        var amount = document.getElementById('amount').value;

        if (class_id == '' || section_id == '' || student_id == '' || year == '' || amount == '') {
            alert("Please input all information");
            return false;
        }

        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("collection_form").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>student_fees/getCollectionSheetForNormal?class_id=" + class_id + '&& section_id='
            + section_id + '&& student_id=' + student_id + '&&year=' + year + '&&amount=' + amount, true);
        xmlhttp.send();
    }

</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/student_fee_add_for_normal" method="post">
    <label><?php echo $this->lang->line('year'); ?></label>
    <select class="smallInput" name="year" id="year" required="1">
        <option value="">-- Please Select --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>


    <label><?php echo $this->lang->line('class'); ?></label>
    <select class="smallInput" name="class_id" id="class_id"
            onchange="getStudentByClassAndSection(this.value,document.getElementById('section_id').value)" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>" <?php if($c_class_id == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>


    <label><?php echo $this->lang->line('section'); ?></label>
    <select class="smallInput" name="section_id" id="section_id"
            onchange="getStudentByClassAndSection(document.getElementById('class_id').value,this.value)" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        $i = 0;
        if (count($section)) {
            foreach ($section as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>" <?php if($c_section_id == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>


    <label><?php echo $this->lang->line('student'); ?></label>
    <select class="smallInput" style="width: 350px;" name="student_id" id="student_id" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
    	foreach ($students as $list) {
    	    ?>
    	    <option
    	        value="<?php echo $list['id']; ?>" <?php if($c_student_id == $list['id']){echo 'selected';} ?>>
    	        <?php echo $list['name'] . ' (Roll-' . $list['roll_no'] . ')' . ' (ID-' . $list['student_code'] . ')'; ?>
    	        </option>
    	<?php
    	  }
    	?>
    </select>

    <label><?php echo $this->lang->line('amount'); ?></label>
    <input type="text" autocomplete="off"  name="amount" id="amount" class="smallInput" size="20%" value="" required="1">

    <label><?php echo $this->lang->line('payment').' '.$this->lang->line('date'); ?></label>
    <input type="text" autocomplete="off"  name="payment_date" readonly style="background-color:#969696;" class="smallInput" size="20%" value="<?php echo  date('Y-m-d'); ?>" required="1">

    <br>
    <input type="submit" class="submit" value="Submit">
    <button type='button' onclick="getCollectionSheet()"><?php echo $this->lang->line('show').' '.$this->lang->line('collection').' '.$this->lang->line('sheet'); ?></button>

    <br>
    <br>

    <div id="collection_form">

    </div>


</form>
<link href="<?php echo base_url() . MEDIA_FOLDER; ?>/js/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/select2.min.js"></script>

<script type="text/javascript">
    $("#student_id").select2();
</script>
