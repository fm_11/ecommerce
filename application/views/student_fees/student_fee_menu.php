<script type="text/javascript" src="<?php echo base_url(); ?>core_media/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <?php
    if($fee_module_type == "S"){
    ?>
        <li><a href="<?php echo base_url(); ?>student_fees/fee_type_view"><span><?php echo $this->lang->line('fees').' '.$this->lang->line('type').' '.$this->lang->line('configuration'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/student_fee_category_index"><span><?php echo $this->lang->line('fees').' '.$this->lang->line('category'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/get_fee_sub_category"><span><?php echo $this->lang->line('sub').' '.$this->lang->line('category'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/fee_allocation_index"><span><?php echo $this->lang->line('allocation'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/fee_collection_index"><span><?php echo $this->lang->line('collection'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/student_fee_due_list"><span><?php echo $this->lang->line('due').' '.$this->lang->line('list'); ?></span></a></li>
		<!--<li><a href="<?php echo base_url(); ?>student_fees/student_fee_report"><span>Report</span></a></li>-->
		
    <?php
    }else{
    ?>
        <li><a href="<?php echo base_url(); ?>student_fees/student_fee_category_index"><span><?php echo $this->lang->line('fees').' '.$this->lang->line('category'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/category_wise_amount_index"><span><?php echo $this->lang->line('fees').' '.$this->lang->line('allocation'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/class_wise_monthly_fee_index"><span><?php echo $this->lang->line('monthly').' '.$this->lang->line('fees'); ?></span></a></li>        
        <li><a href="<?php echo base_url(); ?>student_fees/student_fee_collection_priority_index"><span><?php echo $this->lang->line('priority').' '.$this->lang->line('set'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/fee_collection_index_for_normal"><span><?php echo $this->lang->line('fees').' '.$this->lang->line('collection'); ?></span></a></li>      
    <?php
    }
    ?>
	    <li><a href="<?php echo base_url(); ?>student_fees/student_fee_waiver_index"><span><?php echo $this->lang->line('waiver'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/student_scholarship_index"><span><?php echo $this->lang->line('scholarship'); ?></span></a></li>
	    <li><a href="<?php echo base_url(); ?>student_fees/student_play_truant_fine"><span><?php echo $this->lang->line('play_truant_fine'); ?></span></a></li>
        <li><a href="<?php echo base_url(); ?>student_fees/student_absent_fine"><span><?php echo $this->lang->line('absent_fine'); ?></span></a></li>
        
</ul>

