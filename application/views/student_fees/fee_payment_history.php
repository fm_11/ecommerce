<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td colspan="7" style="text-align:center">
            <b style="font-size:15px;"><?php echo $payment_history[0]['student_name']; ?></b><br>
            <b><?php echo $this->lang->line('total').' '.$this->lang->line('payment').' '.$this->lang->line('history'); ?></b>

        </td>
    </tr>

    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('class'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('section'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('roll'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('month'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('year'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('payment').' '.$this->lang->line('date'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(4);
    foreach ($payment_history as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['class']; ?></td>
            <td><?php echo $row['section']; ?></td>
            <td><?php echo $row['roll_no']; ?></td>
            <td>
                <?php
                echo DateTime::createFromFormat('!m', $row['month'])->format('F');
                ?>
            </td>
            <td><?php echo $row['year']; ?></td>
            <td><?php echo date("d-m-Y", strtotime($row['date'])); ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>student_fees/getStudentReceipt/<?php echo $row['id']; ?>/SC"
                   target="_blank" title="View Details"><?php echo $this->lang->line('view').' '.$this->lang->line('details'); ?></a>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="9" style="color: #000000;" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>

    </tbody>
</table>

