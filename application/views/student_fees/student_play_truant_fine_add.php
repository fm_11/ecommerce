<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/student_play_truant_fine_add" method="post">
    <label><?php echo $this->lang->line('year'); ?></label>
    <select class="smallInput" name="year" id="year" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>

    <label><?php echo $this->lang->line('class'); ?></label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label><?php echo $this->lang->line('section'); ?></label>
    <select name="section_id" class="smallInput" required="1" id="section_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($section_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label><?php echo $this->lang->line('group'); ?></label>
    <select name="group" required="1" class="smallInput">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($group_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <br>
    <br>
    <input type="submit" class="submit" value="Process">
</form>
