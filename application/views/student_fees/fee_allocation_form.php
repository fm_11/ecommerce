<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('sub').' '.$this->lang->line('category'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('category'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('non-resident').' '.$this->lang->line('amount'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('resident').' '.$this->lang->line('amount'); ?></th>
        <th width="60" scope="col">#</th>
    </tr>

    <?php
    $i = 0;
    if (isset($allocated_data)) {
        ?>
        <?php
        foreach ($allocated_data as $row):
            ?>
            <tr>

            <tr>
                <td width="34">
                    <?php echo $i + 1; ?>
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    
                    <input type="hidden" name="sub_category_id_<?php echo $i; ?>"
                           value="<?php echo $row['id']; ?>">
                </td>
                <td>
                     <?php echo $row['category_name']; ?>
                    <input type="hidden" name="category_id_<?php echo $i; ?>"
                           value="<?php echo $row['category_id']; ?>">
                </td>
                
                <td>
                    <input type="text" autocomplete="off"  class="smallInput" style="text-align: right;"
                           name="amount_<?php echo $i; ?>"
                           value="<?php if($row['amount'] != ''){ echo $row['amount']; }else{ echo 0; } ?>">
                </td>
                
                 <td>
                    <input type="text" autocomplete="off"  class="smallInput" style="text-align: right;"
                           name="resident_amount_<?php echo $i; ?>"
                           value="<?php if($row['resident_amount'] != ''){ echo $row['resident_amount']; }else{ echo 0; } ?>">
                </td>
                
                <td style="vertical-align:middle">
                    <input type="checkbox" <?php if($row['is_select'] !=  ''){ echo 'checked'; } ?> name="is_selected_<?php echo $i; ?>">
                </td>
             
                
            </tr>
        <?php $i++; endforeach;
    } ?>

</table>
<input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>