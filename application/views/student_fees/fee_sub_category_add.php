<?php
if($action == 'edit'){
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/student_fee_sub_category_edit/" method="post">
        <label><?php echo $this->lang->line('name'); ?></label>
        <input type="text" autocomplete="off"  name="name" class="smallInput wide" required="1" size="20%" value="<?php echo $fee_sub_category_info[0]['name']; ?>">
        <label><?php echo $this->lang->line('category'); ?></label>
        <select class="smallInput" name="category_id" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($fee_category_info)) {
                foreach ($fee_category_info as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"<?php if($fee_sub_category_info[0]['category_id'] == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>
		
		
		<label><?php echo $this->lang->line('fees').' '.$this->lang->line('type'); ?></label>
        <select class="smallInput" name="fee_type" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <option value="A" <?php if($fee_sub_category_info[0]['fee_type'] == 'A'){echo 'selected';} ?>><?php echo $this->lang->line('annual'); ?></option>
			<option value="A" <?php if($fee_sub_category_info[0]['fee_type'] == 'T'){echo 'selected';} ?>><?php echo $this->lang->line('tri-annual'); ?></option>
			<option value="Q" <?php if($fee_sub_category_info[0]['fee_type'] == 'Q'){echo 'selected';} ?>><?php echo $this->lang->line('quarterly'); ?></option>
			<option value="M" <?php if($fee_sub_category_info[0]['fee_type'] == 'M'){echo 'selected';} ?>><?php echo $this->lang->line('monthly'); ?></option>
        </select>
		
		
		
        <label><?php echo $this->lang->line('remarks'); ?></label>
        <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="remarks"><?php echo $fee_sub_category_info[0]['remarks']; ?></textarea>

        <br>
        <br>
        <input type="hidden" name="id" value="<?php echo $fee_sub_category_info[0]['id']; ?>">
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
<?php
}
else{
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/fee_sub_category_add/" method="post">
        <label><?php echo $this->lang->line('name'); ?></label>
        <input type="text" autocomplete="off"  name="name" class="smallInput wide" size="20%" value="" required="1">
        <label><?php echo $this->lang->line('category'); ?></label>
        <select class="smallInput" name="category_id" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($fee_category_info)) {
                foreach ($fee_category_info as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>
		
		
		<label><?php echo $this->lang->line('fees').' '.$this->lang->line('type'); ?></label>
        <select class="smallInput" name="fee_type" required="1">
            <option value="">--<?php echo $this->lang->line('please_select'); ?> --</option>
            <option value="A"><?php echo $this->lang->line('annual'); ?></option>
			<option value="T"><?php echo $this->lang->line('tri-annual'); ?></option>
			<option value="Q"><?php echo $this->lang->line('quarterly'); ?></option>
			<option value="M"><?php echo $this->lang->line('monthly'); ?></option>
        </select>
		
		
        <label><?php echo $this->lang->line('remarks'); ?></label>
        <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="remarks"></textarea>

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
<?php
}
?>