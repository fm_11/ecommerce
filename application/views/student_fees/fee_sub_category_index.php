<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>student_fees/fee_sub_category_add"><span><?php echo $this->lang->line('add').' '.$this->lang->line('new').' '.$this->lang->line('sub').' '.$this->lang->line('category'); ?></span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('name'); ?></th>     
        <th width="200" scope="col"><?php echo $this->lang->line('category'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('fees').' '.$this->lang->line('type'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($fee_sub_category as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['fee_category_name']; ?></td>
            <td><?php echo $row['fee_type']; ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>student_fees/student_fee_sub_category_edit/<?php echo $row['id']; ?>"
                   title="Edit">Edit</a>&nbsp;
                <a href="<?php echo base_url(); ?>student_fees/student_fee_sub_category_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

