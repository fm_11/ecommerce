<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <tr>
        <th colspan="4">
            <b><?php echo $this->lang->line('name'); ?> :
                <?php echo $details[0]['priority_name']; ?></b>
        </th>
    </tr>

    <tr>
        <th colspan="4">
            <b><?php echo $this->lang->line('year'); ?>
                :
                <?php echo $details[0]['year']; ?></b>
        </th>
    </tr>


    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('name'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('num_of').' '.$this->lang->line('priority'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('percentage').' '.$this->lang->line('of').' '.$this->lang->line('amount'); ?></th>
    </tr>
    <?php
    $i = 0;
    foreach ($details as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td>
                <?php
                if ($row['is_absent_fine'] == 1) {
                    echo 'Absent Fine';
                } elseif ($row['is_play_truant_fine'] == 1) {
                    echo 'Play Truant Fine';
                }elseif ($row['is_month'] == 1){
                    echo date("F", mktime(0, 0, 0, $row['month'], 10));
                }else{
                    echo $row['category_name'];
                }
                ?>
            </td>
            <td>
                <?php echo $row['priority_number']; ?>
            </td>
            <td>
                <?php echo $row['percentage_of_amount']; ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
