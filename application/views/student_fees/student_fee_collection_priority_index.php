<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function useStatusUpdate(id, is_in_use) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>student_fees/updateUseStatusUpdate?id=" + id + '&&is_in_use=' + is_in_use, true);
        xmlhttp.send();
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>student_fees/fee_collection_priority_add"><span><?php echo $this->lang->line('priority').' '.$this->lang->line('add'); ?></span></a>

    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>student_fees/fee_priority_setting"><span><?php echo $this->lang->line('priority').' '.$this->lang->line('setting'); ?></span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="250" scope="col"><?php echo $this->lang->line('name'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('year'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('date'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('is_in').' '.$this->lang->line('use'); ?> ?</th>
        <th width="100" scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($data as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['year']; ?></td>
            <td><?php echo $row['date']; ?></td>

            <td style="vertical-align:middle" id="status_sction_<?php echo $row['id']; ?>">
                <?php
                if ($row['is_in_use'] == 1) {
                    ?>
                    <a class="approve_icon" title="Approve" href="#"
                       onclick="useStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_in_use']; ?>)"></a>
                    <?php
                } else {
                    ?>
                    <a class="reject_icon" title="Reject" href="#"
                       onclick="useStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_in_use']; ?>)"></a>
                    <?php
                }
                ?>
            </td>

            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>student_fees/fee_priority_view/<?php echo $row['id']; ?>"
                   title="View">  <?php echo $this->lang->line('view'); ?></a>
                <a href="<?php echo base_url(); ?>student_fees/fee_priority_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="9" style="color: #000000;" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>

    </tbody>
</table>