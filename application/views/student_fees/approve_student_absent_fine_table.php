
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/approve_absent_fine_save" method="post">
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
   
	 <tr>
        <th width="20" scope="col">&nbsp;<?php echo $this->lang->line('sl'); ?></th>     
		<th width="130" scope="col">&nbsp;<?php echo $this->lang->line('name'); ?></th>  
		<th width="130" scope="col">&nbsp;<?php echo $this->lang->line('roll'); ?></th>  
		<th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total').' '.$this->lang->line('days'); ?></th>	
		<th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total').' '.$this->lang->line('working').' '.$this->lang->line('days'); ?></th>	
        <th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total').' '.$this->lang->line('present').' '.$this->lang->line('days'); ?></th>
        <th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total').' '.$this->lang->line('holidays'); ?> </th>
		<th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total').' '.$this->lang->line('leave'); ?></th>
		<th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total').' '.$this->lang->line('absent'); ?></th>
		<th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('amount'); ?></th>
    </tr>
	
     </thead>
	 
	  <tbody>
    <?php
    $i = 0;
    foreach ($adata as $row):
       
        ?>
        <tr>
            <td width="34">
                &nbsp;<?php echo $i + 1; ?>
            </td>
          
			<td>
                &nbsp;<?php echo $row['name']; ?><br>               
            </td>
          <td>
                &nbsp;<?php echo $row['roll_no']; ?><br>               
            </td>
           <td align="center">&nbsp;<?php echo $total_days; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_working_days']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_present_days']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_holidays']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_leave_days']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_absent_days']; ?></td>
	       <td align="center">&nbsp;
	         <input type="text" autocomplete="off"  class="input-text-short" size="8"
                           style="text-align: right; width:75px;"
                           name="amount_<?php echo $i; ?>" value="<?php echo $row['total_absent_days'] * 50; ?>"/>
                           
            <input type="hidden" name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
	       </td>
        </tr>
    <?php  $i++; endforeach; ?>
	  
	    <tr>
            <td colspan="10" style="text-align: right;">
                <input type="hidden" class="input-text-short" name="loop_time"
                       value="<?php echo $i; ?>"/>
            <input type="hidden" class="input-text-short" name="year"
           value="<?php echo $year; ?>"/>
           
           <input type="hidden" class="input-text-short" name="month"
           value="<?php echo $month; ?>"/>
           
          <input type="hidden" class="input-text-short" name="class_id"
           value="<?php echo $class_id; ?>"/>
           
                <input type="submit" class="submit" value="Save">
            </td>
        </tr>
	  
    </tbody>

</table>

</form>