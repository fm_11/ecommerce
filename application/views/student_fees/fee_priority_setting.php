<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/fee_priority_setting" method="post">
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('class'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('priority'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $j = 0;
    foreach ($class as $row):
        $j++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $j; ?>
            </td>
            <td>
                <?php echo $row['name']; ?>
                <input type="hidden" value="<?php echo $row['id']; ?>" name="class_id_<?php echo $j; ?>">
            </td>
            <td>
                <select class="smallInput" name="priority_id_<?php echo $j; ?>">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php
                    $i = 0;
                    if (count($priority)) {
                        foreach ($priority as $list) {
                            $i++;
                            ?>
                            <option
                                    value="<?php echo $list['id']; ?>"<?php if($row['priority_id'] == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="3" style="text-align: right;">
            <input type="hidden" value="<?php echo $j; ?>" name="loop_time">
            <input type="submit" class="submit" value="Submit">
        </td>
    </tr>
    </tbody>
</table>
</form>

