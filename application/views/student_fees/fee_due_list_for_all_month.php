<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<style>
    #header{
        text-align:center;
        width:auto;
       
    }
</style>

<?php
if (isset($due_list)) {
    ?>
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a">
        <tr>
            <th width="100%" style="text-align:right" scope="col">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
            </th>
        </tr>
    </table>
<?php
}
?>

<div id="printableArea">
    <?php
    //echo '<pre>';
    //print_r($due_list);
    if (isset($due_list)) {
        ?>
            
            <?php
                        $colspan_array = array();
                        $head_colspan = 0;
                        foreach ($months as $month):
                        $annual_count = 0;
                        if(isset($due_list[0]['fee_list_'.$month]['A'])){
                            $annual_count = count($due_list[0]['fee_list_'.$month]['A']);
                        }
                        
                        $tri_annual_count = 0;
                        if(isset($due_list[0]['fee_list_'.$month]['T'])){
                            $tri_annual_count = count($due_list[0]['fee_list_'.$month]['T']);
                        }
                        
                        $quaterly_count = 0;
                        if(isset($due_list[0]['fee_list_'.$month]['Q'])){
                            $quaterly_count = count($due_list[0]['fee_list_'.$month]['Q']);
                        }
                        
                        $monthly_count = 0;
                        if(isset($due_list[0]['fee_list_'.$month]['M'])){
                            $monthly_count = count($due_list[0]['fee_list_'.$month]['M']);
                        }
                        
                        $colspan = $annual_count + $tri_annual_count + $monthly_count + $quaterly_count;
                        $head_colspan = $head_colspan + $colspan;
                        $colspan_array[$month] = $colspan;
                        endforeach;
                    ?>
            
            <table width="100%" cellpadding="0" border="1" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                
            <tr>
                <th colspan="<?php echo $head_colspan + 3; ?>">
                    <div id="header">
                        <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                        <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?></b><br>
                        <b style="font-size:13px;">Class: <?php echo $HeaderInfo['ClassName']; ?>,
                            <?php echo $this->lang->line('section'); ?>: <?php echo $HeaderInfo['SectionName']; ?><br>
                           <?php echo $this->lang->line('payment').' '.$this->lang->line('due').' '.$this->lang->line('list_for'); ?>
                              
                            <?php
                             foreach ($months as $month):
                               echo DateTime::createFromFormat('!m', $month)->format('F').', ';
                             endforeach;
                            ?>
                             <?php echo $HeaderInfo['year']; ?></b>
                          
                    </div>
                </th>
            </tr>    
            
            <tr>
                 <th width="15" style="vertical-align: middle;" scope="col" rowspan="2"><?php echo $this->lang->line('sl'); ?></th>
                <th width="150" style="vertical-align: middle;" scope="col" rowspan="2"><?php echo $this->lang->line('name'); ?></th>
                <th width="30" style="vertical-align: middle;" scope="col" rowspan="2"><?php echo $this->lang->line('roll'); ?></th> 
                <?php
               // echo $colspan;
                 foreach ($months as $month):
                 ?>
                    <th style="text-align:center;" scope="col" colspan="<?php echo $colspan_array[$month]; ?>">
                        <?php echo DateTime::createFromFormat('!m', $month)->format('F'); ?>
                    </th>
                <?php endforeach; ?>
            </tr>
            <tr>
                 <?php
                 foreach ($months as $month):
                 ?>
               
                <?php
                    if (isset($due_list[0]['fee_list_'.$month]['A']) && !empty($due_list[0]['fee_list_'.$month]['A'])) {
                      foreach ($due_list[0]['fee_list_'.$month]['A'] as $row):
                    ?>
                    <th style="text-align:center;" width="130"   scope="col">
                        <?php echo $row['sub_category_name']; ?>
                        </th>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[0]['fee_list_'.$month]['T']) && !empty($due_list[0]['fee_list_'.$month]['T'])) {
                      foreach ($due_list[0]['fee_list_'.$month]['T'] as $row):
                    ?>
                    <th style="text-align:center;" width="130" scope="col"><?php echo $row['sub_category_name']; ?></th>
                    <?php
                      endforeach;
                } ?>
                
                
                <?php
                    if (isset($due_list[0]['fee_list_'.$month]['Q']) && !empty($due_list[0]['fee_list_'.$month]['Q'])) {
                      foreach ($due_list[0]['fee_list_'.$month]['Q'] as $row):
                    ?>
                    <th  style="text-align:center;" width="130" scope="col"><?php echo $row['sub_category_name']; ?></th>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[0]['fee_list_'.$month]['M']) && !empty($due_list[0]['fee_list_'.$month]['M'])) {
                      foreach ($due_list[0]['fee_list_'.$month]['M'] as $row):
                    ?>
                    <th style="text-align:center;" width="130" scope="col"><?php echo $row['sub_category_name']; ?></th>
                    <?php
                      endforeach;
                } ?>
                
                <?php endforeach; ?>
                
            </tr>
            
            
            <tr>
                <?php
                 foreach ($months as $month):
                 ?>
                 <?php
                    if (isset($due_list[0]['fee_list_'.$month]['A']) && !empty($due_list[0]['fee_list_'.$month]['A'])) {
                      foreach ($due_list[0]['fee_list_'.$month]['A'] as $row):
                    ?>
                    <th scope="col">
                          <?php echo $this->lang->line('allocted'); ?> <br>
                        <?php echo $this->lang->line('payment'); ?> <br>
                         <?php echo $this->lang->line('due'); ?>
                    </th>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[0]['fee_list_'.$month]['T']) && !empty($due_list[0]['fee_list_'.$month]['T'])) {
                      foreach ($due_list[0]['fee_list_'.$month]['T'] as $row):
                    ?>
                    <th scope="col">
                        <?php echo $this->lang->line('allocted'); ?> <br>
                        <?php echo $this->lang->line('payment'); ?> <br>
                         <?php echo $this->lang->line('due'); ?>
                    </th>
                    <?php
                      endforeach;
                } ?>
                
                
                <?php
                    if (isset($due_list[0]['fee_list_'.$month]['Q']) && !empty($due_list[0]['fee_list_'.$month]['Q'])) {
                      foreach ($due_list[0]['fee_list_'.$month]['Q'] as $row):
                    ?>
                     <th scope="col">
                         <?php echo $this->lang->line('allocted'); ?> <br>
                        <?php echo $this->lang->line('payment'); ?> <br>
                         <?php echo $this->lang->line('due'); ?>
                    </th>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[0]['fee_list_'.$month]['M']) && !empty($due_list[0]['fee_list_'.$month]['M'])) {
                      foreach ($due_list[0]['fee_list_'.$month]['M'] as $row):
                    ?>
                     <th scope="col">
                         <?php echo $this->lang->line('allocted'); ?> <br>
                        <?php echo $this->lang->line('payment'); ?> <br>
                         <?php echo $this->lang->line('due'); ?>
                    </th>
                    <?php
                      endforeach;
                } ?>
                
                <?php endforeach; ?>
            </tr>
            
            </thead>
            <tbody>
            
            <?php
                $i = 0;
                 $style = "text-align:right;";
                foreach ($due_list as $row):
            ?>
               
            <tr>
                <td  style="vertical-align: middle; text-align:center;" width="34">
                    <?php echo $i + 1; ?>
                </td>
                <td style="vertical-align: middle; padding-left:3px;"><?php echo $row['name']; ?></td>
                <td style="vertical-align: middle; text-align:center;"><?php echo $row['roll_no']; ?></td>
                   
                  <?php
                 foreach ($months as $month):
                 ?>
                <?php
                    if (isset($due_list[$i]['fee_list_'.$month]['A']) && !empty($due_list[$i]['fee_list_'.$month]['A'])) {
                      foreach ($due_list[$i]['fee_list_'.$month]['A'] as $annual_row):
                          
                           
                ?>
                       <td style="<?php echo $style; ?>">
                           <?php echo $annual_row['actual_allocated_amount_for_this_student']; ?>&nbsp;<br>
                           <?php echo $annual_row['already_paid_amount']; ?>&nbsp;<br>
                                <b>
                                <?php 
                                echo $annual_row['due_amount'];
                                ?>
                               </b>&nbsp;
                       </td>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[$i]['fee_list_'.$month]['T']) && !empty($due_list[$i]['fee_list_'.$month]['T'])) {
                      foreach ($due_list[$i]['fee_list_'.$month]['T'] as $tri_annual_row):
                           
                    ?>
                       <td style="<?php echo $style; ?>">
                           <?php echo $tri_annual_row['actual_allocated_amount_for_this_student']; ?>&nbsp;<br>
                           <?php echo $tri_annual_row['already_paid_amount']; ?>&nbsp;<br>
                           <b><?php echo $tri_annual_row['due_amount']; ?></b>&nbsp;
                       </td>
                    <?php
                      endforeach;
                } ?>
                
                
                <?php
                    if (isset($due_list[$i]['fee_list_'.$month]['Q']) && !empty($due_list[$i]['fee_list_'.$month]['Q'])) {
                      foreach ($due_list[$i]['fee_list_'.$month]['Q'] as $qua_row):
                          
                    ?>
                       <td style="<?php echo $style; ?>">
                           <?php echo $qua_row['actual_allocated_amount_for_this_student']; ?>&nbsp;<br>
                           <?php echo $qua_row['already_paid_amount']; ?>&nbsp;<br>
                           <b><?php echo $qua_row['due_amount']; ?></b>&nbsp;
                        </td>
                    <?php
                      endforeach;
                } ?>
                
                <?php
                    if (isset($due_list[$i]['fee_list_'.$month]['M']) && !empty($due_list[$i]['fee_list_'.$month]['M'])) {
                      foreach ($due_list[$i]['fee_list_'.$month]['M'] as $monthly_row):
                         
                    ?>
                       <td style="<?php echo $style; ?>">
                           <?php echo $monthly_row['actual_allocated_amount_for_this_student']; ?>&nbsp;<br>
                           <?php echo $monthly_row['already_paid_amount']; ?>&nbsp;<br>
                           <b><?php echo $monthly_row['due_amount']; ?></b>&nbsp;
                       </td>
                    <?php
                      endforeach;
                } ?>
                 <?php endforeach; ?>
            </tr>
               
            <?php $i++; endforeach; ?>
            
            
            
            </tbody>
        </table>

    <?php
    }
    ?>
</div>