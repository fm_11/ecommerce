<script type="text/javascript">
    function getStudentByClassAndSection(class_id, section_id) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("student_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>student_fees/getStudentByClassAndSection?class_id=" + class_id + '&&section_id=' + section_id, true);
        xmlhttp.send();
    }
    
    function getSelectValues(select) {
      var result = [];
      var options = select && select.options;
      var opt;
    
      for (var i=0, iLen=options.length; i<iLen; i++) {
        opt = options[i];
    
        if (opt.selected) {
          result.push(opt.value || opt.text);
        }
      }
      return result;
    }

    function getCollectionSheet() {
        var class_id = document.getElementById('class_id').value;
        var section_id = document.getElementById('section_id').value;
        var student_id = document.getElementById('student_id').value;
        var year = document.getElementById('year').value;
        var collection_month = document.getElementById('collection_month').value;
        
        var monthList = document.getElementById('month');
        var month = getSelectValues(monthList);
        //alert(collection_month);
        
        var is_new_admission = document.getElementById('is_new_admission');
        var val_is_new_admission = 0;
        if(is_new_admission.checked == true){
            val_is_new_admission = 1;
        }
        

        if (class_id == '' || section_id == '' || student_id == '' || year == '' || month == '') {
            alert("Please select all information");
            return false;
        }

        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("collection_form").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>student_fees/getCollectionSheet?class_id=" + class_id + '&& section_id='
            + section_id + '&& student_id=' + student_id + '&&year=' + year + '&&month=' + month + '&&collection_month=' + collection_month + '&&val_is_new_admission=' + val_is_new_admission, true);
        xmlhttp.send();
    }

    function checkAll(ele) {
        //alert('ddd');
        //debugger;
        
        var checkboxes = document.querySelectorAll("input[type='checkbox']");
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    if (checkboxes[i].id != 'do_you_want_receipt' && checkboxes[i].id != 'is_new_admission' && checkboxes[i].id != 'do_you_collect_late_fee') {
                        checkboxes[i].checked = true;
                    }
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    if (checkboxes[i].id != 'do_you_want_receipt' && checkboxes[i].id != 'is_new_admission' && checkboxes[i].id != 'do_you_collect_late_fee') {
                        checkboxes[i].checked = false;
                    }
                }
            }
        }
        calculateTotalPayableAmount();
    }
    
    
    function dueAmountCalculate(row_id){
        //alert(row_id);
        var paid_amount = Number(document.getElementById('paid_amount_' + row_id).value);
        var discount_amount = Number(document.getElementById('discount_amount_' + row_id).value);
        var hidden_paid_amount = Number(document.getElementById('hidden_paid_amount_' + row_id).value);
        document.getElementById('due_amount_' + row_id).value = hidden_paid_amount - (paid_amount + discount_amount);
        calculateTotalPayableAmount();
    }
    
    
    function discountOptionOpenClose(row_id){
        //alert(row_id);
        var checkBox = document.getElementById('is_selected_' + row_id);
        if(checkBox.checked == true){
           document.getElementById('discount_amount_' + row_id).readOnly = false;
           
           //Total amount and total discount amount calculation
           var total_paid_amount = Number(document.getElementById('total_paid_amount').value);
           var this_selected_amount = Number(document.getElementById('paid_amount_' + row_id).value);
           document.getElementById('total_paid_amount').value = (total_paid_amount + this_selected_amount);

        }else {
           document.getElementById('discount_amount_' + row_id).readOnly = true;
           document.getElementById('discount_amount_' + row_id).value = 0;
           
           //Total amount and total discount amount calculation
           var total_paid_amount = Number(document.getElementById('total_paid_amount').value);
           var this_selected_amount = Number(document.getElementById('paid_amount_' + row_id).value);
           document.getElementById('total_paid_amount').value = (total_paid_amount - this_selected_amount);

        }
        
    }

    
    
    function calculate_paidable_amount_after_discount(discount_amount,row_id){
        debugger;
        var number_of_transport_fee_row = Number(document.getElementById('number_of_transport_fee_row').value);
        var start_row = 1;
        var row_num = Number(document.getElementById('num_of_row').value) - number_of_transport_fee_row;
        var total_discount = 0;
        while(start_row <= row_num){
            if(row_id != start_row){
                var r_discount_amount = document.getElementById('discount_amount_' + start_row).value;
                if(r_discount_amount != ''){
                    total_discount = total_discount + Number(r_discount_amount);
                }
            }
            start_row++;
        }
        document.getElementById('total_discount').value = Number(total_discount) + Number(discount_amount);
        
        
        var n_discount_amount = Number(discount_amount);
        var paid_amount = Number(document.getElementById('hidden_paid_amount_' + row_id).value);
        if(n_discount_amount > paid_amount ){
            alert('Discounts can not be more than paidable amount.');
            document.getElementById('discount_amount_' + row_id).value = 0;
            document.getElementById('paid_amount_' + row_id).value = Number(paid_amount);
        }else{
            document.getElementById('paid_amount_' + row_id).value = Number(paid_amount) - Number(n_discount_amount);
        }
        calculateTotalPayableAmount();
    }
    
    function calculateTotalPayableAmount(){
        var start_row = 1;
        var row_num = Number(document.getElementById('num_of_row').value);
        var total_paid_amount = 0;
        while(start_row <= row_num){
            var checkBox = document.getElementById('is_selected_' + start_row);
            if(checkBox.checked == true){
                var paid_amount = document.getElementById('paid_amount_' + start_row).value;
                total_paid_amount = total_paid_amount + Number(paid_amount);
            }
            start_row++;
        }
        document.getElementById('total_paid_amount').value = total_paid_amount;
    }
</script>

<style>
    input[readonly] {
    background-color: #e3e5e8;
    }
</style>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/fee_collection_add" method="post">
    <label><?php echo $this->lang->line('class'); ?></label>
    <select class="smallInput" name="class_id" id="class_id"
            onchange="getStudentByClassAndSection(this.value,document.getElementById('section_id').value)" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>" <?php if($c_class_id == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>


    <label><?php echo $this->lang->line('section'); ?></label>
    <select class="smallInput" name="section_id" id="section_id"
            onchange="getStudentByClassAndSection(document.getElementById('class_id').value,this.value)" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        $i = 0;
        if (count($section)) {
            foreach ($section as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>" <?php if($c_section_id == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>


    <label><?php echo $this->lang->line('student'); ?></label>
    <select class="smallInput" style="width: 350px;" name="student_id" id="student_id" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
	foreach ($students as $list) {
	    ?>
	    <option
	        value="<?php echo $list['id']; ?>" <?php if($c_student_id == $list['id']){echo 'selected';} ?>><?php echo $list['name'] . ' (' . $list['student_code'] . ')'; ?></option>
	<?php
	}
	?>
    </select>


    <label><?php echo $this->lang->line('year'); ?></label>
    <select class="smallInput" name="year" id="year" required="1">
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>
    
    <select class="smallInput" name="collection_month" id="collection_month" required="1">
        <?php
        $i = 1;
        while ($i <= 12) {
            $dateObj = DateTime::createFromFormat('!m', $i);
            ?>
            <option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
                echo 'selected';
            } ?>><?php echo $dateObj->format('F'); ?></option>
            <?php
            $i++;
        }
        ?>
    </select> <?php echo '( '.$this->lang->line('its').' '.$this->lang->line('collection').' '.$this->lang->line('month').' )'; ?>)

    <label><?php echo $this->lang->line('fees').' '.$this->lang->line('months'); ?></label>
    <select class="smallInput" multiple="multiple" name="month" id="month" required="1" style="height:180px;">
        <?php
        $i = 1;
        while ($i <= 12) {
            $dateObj = DateTime::createFromFormat('!m', $i);
            ?>
            <option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
                echo 'selected';
            } ?>><?php echo $dateObj->format('F'); ?></option>
            <?php
            $i++;
        }
        ?>
    </select>
    
   <?php echo $this->lang->line('is').' '.$this->lang->line('new').' '.$this->lang->line('admission').' ?'; ?>  <input type="checkbox" id="is_new_admission" name="is_new_admission">
    
    <button type='button' onclick="getCollectionSheet()"><?php echo $this->lang->line('process'); ?>Process</button>
    <br><br>

    <div id="collection_form">

    </div>

</form>

<link href="<?php echo base_url() . MEDIA_FOLDER; ?>/js/select2.min.css" rel="stylesheet"/>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/select2.min.js"></script>

<script type="text/javascript">
    $("#student_id").select2();
</script>

