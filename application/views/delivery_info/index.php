<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>delivery_info/update" method="post" enctype="multipart/form-data">
  <div class="form-row">

    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('inside_city_cost'); ?><span class="required_label">*</span></label>
      <input type="hidden" name="id"  class="form-control" value="<?php echo $delivery_info->id;  ?>">
      <input type="text" autocomplete="off"  name="inside_city_cost"  class="form-control" value="<?php echo $delivery_info->inside_city_cost;  ?>">
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('outside_city_cost'); ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  name="outside_city_cost"  class="form-control" value="<?php echo $delivery_info->outside_city_cost;  ?>">
    </div>

  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('description_inside'); ?></label>
      	<textarea  name="description_inside" id="tinyMceExample" maxlength="1000"onkeyup="count_alpha(this.value);"  rows="5" cols="50" class="form-control"><?php echo $delivery_info->description_inside;  ?></textarea>
      <!-- <input type="text" autocomplete="off"  name="description_inside"  class="form-control" value="<?php echo $delivery_info->description_inside;  ?>"> -->
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('description_outside'); ?></label>
      	<textarea  name="description_outside" id="tinyMceExample" maxlength="1000"onkeyup="count_alpha(this.value);"  rows="5" cols="50" class="form-control"><?php echo $delivery_info->description_outside;  ?></textarea>
      <!-- <input type="text" autocomplete="off"  name="description_outside"  class="form-control" value="<?php echo $delivery_info->description_outside;  ?>"> -->
    </div>

  </div>

  <div class="float-right">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
  </div>

</form>
