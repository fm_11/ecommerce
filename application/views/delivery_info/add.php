

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>delivery_info/add" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('inside_city_cost')?> <span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="inside_city_cost" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('outside_city_cost')?> <span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="outside_city_cost" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('description_inside')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="description_inside" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('description_outside')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="description_outside" required="1"/>
    </div>
  </div>


    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
