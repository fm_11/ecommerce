<!DOCTYPE html>
<html lang="en">
    <head>
        <title>School360 - উইজডম ল্যাবরেটরী স্কুল এন্ড কলেজ</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600&amp;subset=latin-ext" rel="stylesheet">

        <!-- CSS -->
        <link href="<?php echo base_url(); ?>core_media/land_page_assets/css/main.css" rel="stylesheet">

        <!-- JS -->
        <script src="<?php echo base_url(); ?>core_media/land_page_assets//js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="<?php echo base_url(); ?>core_media/land_page_assets//js/vendor/jquery-1.12.0.min.js"></script>
    </head>
    <body>
        <div class="site" id="page">
            <a class="skip-link sr-only" href="#main">Skip to content</a>

            <!-- Options headline effects: .rotate | .slide | .zoom | .push | .clip -->
            <section class="hero-section hero-section--image clearfix clip">
                <div class="hero-section__wrap">
                    <div class="hero-section__option">
                        <img src="<?php echo base_url(); ?>core_media/land_page_assets//images/index.jpg" alt="Hero section image">
                    </div>
                    <!-- .hero-section__option -->

                    <div class="container">
                        <div class="row">
                            <div class="offset-lg-2 col-lg-8">
                                <div class="title-01 title-01--11 text-center">
                                    <h2 class="title__heading">
                                        <span>আমরাই</span>
                                        <strong class="hero-section__words">
                                            <span class="title__effect is-visible">সেরা</span>
                                            <span class="title__effect">প্রথম</span>
                                            <span class="title__effect">ভবিষ্যত </span>
                                        </strong>
                                    </h2>
                                    <div class="title__description">উইজডম ল্যাবরেটরী স্কুল এন্ড কলেজ  & উইজডম সেন্ট্রাল কলেজ</div>

                                    <!-- Options btn color: .btn-success | .btn-info | .btn-warning | .btn-danger | .btn-primary -->
                                    <div class="title__action">
									<a href="http://wisdom.school360.app/login" class="btn btn-success">উইজডম ল্যাবরেটরী স্কুল এন্ড কলেজ</a>
									<a href="https://wisdomcollege.school360.app/login" class="btn btn-success">উইজডম সেন্ট্রাল কলেজ</a>
									</div>
                                </div> <!-- .title-01 -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="button-group">
        	<a href="http://school360.app/" class="btn btn-outline-success button-sm">School360°</a>
            <a href="http://inventiontec.com/" class="btn btn-outline-success button-sm">InventionTec</a>
        </div>

        <!-- JS -->
        <script src="<?php echo base_url(); ?>core_media/land_page_assets//js/plugins/animate-headline.js"></script>
        <script src="<?php echo base_url(); ?>core_media/land_page_assets//js/main.js"></script>
    </body>
</html>
