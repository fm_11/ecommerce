<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>customers/add" method="post">


  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Name<span style="color:red">*</span></label>
      <input type="text" class="form-control" name="name" required/>
    </div>
    <div class="form-group col-md-6">
      <label>Address<span style="color:red">*</span></label>
      <input type="text" class="form-control" name="address" required/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Phone<span style="color:red">*</span></label>
      <input type="number" class="form-control" name="phone" required/>
    </div>
    <div class="form-group col-md-4">
      <label>Email<span style="color:red">*</span></label>
      <input type="email" class="form-control" name="email" required/>
    </div>
    <div class="form-group col-md-4">
      <label>Password<span style="color:red">*</span></label>
      <input type="password" class="form-control" name="password" required/>
    </div>
  </div>

    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
