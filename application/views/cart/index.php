<script>
// Update item quantity
function updateCartItem(obj, rowid){
    $.get("<?php echo base_url('cart/updateItemQty/'); ?>", {rowid:rowid, qty:obj.value}, function(resp){
        if(resp == 'ok'){
            location.reload();
        }else{
            alert('Cart update failed, please try again.');
        }
    });
}
</script>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <a href="<?php echo base_url(); ?>products/index"><button  class="btn btn-secondary" type="button" id="dropdownMenuIconButton2" style="float: right;">
      Add More
  </button></a>
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">Name</th>
        <th scope="col">Quantity</th>
        <th scope="col">Price</th>
        <th scope="col">Sub-Total</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($cart as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><input type="number" class="form-control text-center" style="width: 50%; padding: 10px"value="<?php echo $row["qty"]; ?>" onchange="updateCartItem(this, '<?php echo $row["rowid"]; ?>')"></td>
            <td><?php echo $row['price']; ?></td>
            <td><?php echo $row['subtotal']; ?></td>
            <td><button class="btn btn-sm btn-danger" onclick="return confirm('Are you sure to delete item?')?window.location.href='<?php echo base_url('cart/removeItem/'.$row["rowid"]); ?>':false;">
              <i class="ti-trash"></i> </button></td>
            <!-- <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-trash"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>customers/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td> -->

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
