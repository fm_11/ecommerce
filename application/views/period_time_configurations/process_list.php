<script>
	function timeValueCheck(value,sl_num) {
		if (document.getElementById('is_checked_' + sl_num).checked) {
			document.getElementById('start_time_' + sl_num).value = "";
			document.getElementById('end_time_' + sl_num).value = "";

			document.getElementById('start_time_' + sl_num).readOnly = false;
			document.getElementById('end_time_' + sl_num).readOnly = false;
		} else {
			document.getElementById('start_time_' + sl_num).value = "";
			document.getElementById('end_time_' + sl_num).value = "";

			document.getElementById('start_time_' + sl_num).readOnly = true;
			document.getElementById('end_time_' + sl_num).readOnly = true;
		}
	}
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>period_time_configurations/data_save" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Period</th>
				<th scope="col">Start Time</th>
				<th scope="col">End Time</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$sl = 0;
			foreach ($periods as $row):
				?>
				<tr>
					<td>
						<input type="checkbox"  <?php if($row['start_time'] != ''){ echo 'checked'; }  ?> name="is_checked_<?php echo $sl; ?>" id="is_checked_<?php echo $sl; ?>" onclick="timeValueCheck(this.value,<?php echo $sl; ?>)">
					</td>
					<td>
						<?php echo $row['name']; ?>
						<input type="hidden" autocomplete="off"  required name="period_id_<?php echo $sl; ?>" id="period_id_<?php echo $sl; ?>" value="<?php echo $row['id']; ?>">
					</td>
					<td>
						<input style="padding: 7px; width: 170px;" <?php if($row['start_time'] == ''){ echo 'readonly'; }  ?> type="time" autocomplete="off"  required name="start_time_<?php echo $sl; ?>" id="start_time_<?php echo $sl; ?>" class="form-control" value="<?php if($row['start_time'] != ''){ echo $row['start_time']; }  ?>">
					</td>
					<td>
						<input style="padding: 7px; width: 170px;" <?php if($row['start_time'] == ''){ echo 'readonly'; }  ?> type="time" autocomplete="off"  required name="end_time_<?php echo $sl; ?>" id="end_time_<?php echo $sl; ?>" class="form-control" value="<?php if($row['end_time'] != ''){ echo $row['end_time']; }  ?>">
					</td>
				</tr>
				<?php $sl++; endforeach; ?>

			<tr>
				<td colspan="4">
					<input type="hidden" class="input-text-short" name="shift_id"
						   value="<?php echo $shift_id; ?>"/>
					<input type="hidden" class="input-text-short" name="total_period"
						   value="<?php echo $sl; ?>"/>
					<div class="float-right">
						<input class="btn btn-primary" type="submit" value="Save">
					</div>
				</td>
			</tr>

			</tbody>
		</table>
	</div>
</form>
