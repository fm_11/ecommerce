<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_batch_adds/excel_data_save" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<thead>
			<tr>
				<th scope="col">Name</th>
				<th scope="col">Index No.</th>
				<th scope="col">Gender</th>
				<th scope="col">Religion</th>
				<th scope="col">Category</th>
				<th scope="col">Mobile</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i = 0;
			foreach ($returnDataForShow as $row):
				?>
				<tr>

				<tr>
					<td>
						<input style="padding: 6px; width: 160px;border:1px #737373 solid;" class="form-control" type="text" name="name_<?php echo $i; ?>" value="<?php echo $row['name']; ?>"/>
					</td>

					<td>
						<input style="padding: 6px; width: 100px;border:1px #737373 solid;" class="form-control" type="text" name="index_num_<?php echo $i; ?>" value="<?php echo $row['index_num']; ?>"/>
					</td>

					<td>
						<select style="width: 100px !important;adding: 6px;border:1px #737373 solid; color: #000000;" class="form-control" name="gender_<?php echo $i; ?>" required="required">
							<option value="M" <?php if($row['gender'] == 'M'){ echo 'selected'; } ?>>Male</option>
							<option value="F" <?php if($row['gender'] == 'F'){ echo 'selected'; } ?>>Female</option>
							<option value="O" <?php if($row['gender'] == 'O'){ echo 'selected'; } ?>>Other</option>
						</select>
					</td>
					<td>
						<select style="width: 100px !important;adding: 6px;border:1px #737373 solid;color: #000000;" class="form-control" name="religion_<?php echo $i; ?>" required="1">
							<option value="">-- Please Select --</option>
							<?php
							if (count($religions)) {
								foreach ($religions as $list) {
									?>
									<option value="<?php echo $list['id']; ?>" <?php if (isset($row['religion'])) {
										if ($row['religion'] == trim($list['name'])) {
											echo 'selected';
										}
									} ?>><?php echo $list['name']; ?></option>
									<?php
								}
							}
							?>
						</select>
					</td>
					<td>
						<select style="width: 100px !important;adding: 6px;border:1px #737373 solid;color: #000000;" class="form-control" name="category_id_<?php echo $i; ?>" required="1">
							<option value="">-- Please Select --</option>
							<?php
							if (count($categories)) {
								foreach ($categories as $list) {
									 ?>
									<option value="<?php echo $list['id']; ?>" <?php if (isset($row['category'])) {
										if ($row['category'] == trim($list['name'])) {
											echo 'selected';
										}
									} ?>><?php echo $list['name']; ?></option>
									<?php
								}
							}
							?>
						</select>
					</td>

					<td>
						<input style="padding: 6px; width: 120px;border:1px #737373 solid;"  class="form-control" type="text" name="mobile_<?php echo $i; ?>" value="<?php echo $row['mobile']; ?>"/>
					</td>
				</tr>
				<?php $i++; endforeach; ?>
			</tbody>
		</table>
	</div>

	<input type="hidden" class="input-text-short" name="number_of_rows"
		   value="<?php echo $i; ?>"/>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>

</form>
