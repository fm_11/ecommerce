
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>classes/add" class="cmxform" method="post">
  <fieldset>
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('name'); ?> (English)</label>
          <input type="text" autocomplete="off"  class="form-control" id="name" required name="name" value="">
        </div>

	  <div class="form-group col-md-12">
		  <label for="inputEmail4"><?php echo $this->lang->line('name'); ?> (Bangla)</label>
		  <input type="text" autocomplete="off"  class="form-control" id="name_bangla" required name="name_bangla" value="">
	  </div>

        <div class="form-group col-md-12">
            <label for="inputshort_name"><?php echo $this->lang->line('student_code') . ' ' .$this->lang->line('short_form'); ?></label>
            <input type="text" autocomplete="off"  class="form-control" id="student_code_short_form" required
                name="student_code_short_form" value="">
        </div>
  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
