<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>classes/edit" class="cmxform" method="post">
	<fieldset>
		<div class="form-group col-md-12">
			<label for="inputEmail4"><?php echo $this->lang->line('name'); ?> (English)</label>
			<input type="text" autocomplete="off"  class="form-control" id="name" required name="name" value="<?php echo $class_info[0]['name']; ?>">
		</div>
		<div class="form-group col-md-12">
			<label for="inputEmail4"><?php echo $this->lang->line('name'); ?> (Bangla)</label>
			<input type="text" autocomplete="off"  class="form-control" id="name_bangla" required name="name_bangla" value="<?php echo $class_info[0]['name_bangla']; ?>">
		</div>
		<div class="form-group col-md-12">
			<label for="inputshort_name"><?php echo $this->lang->line('student_code') . ' ' .$this->lang->line('short_form'); ?></label>
			<input type="text" autocomplete="off"  class="form-control" id="student_code_short_form" required
				   name="student_code_short_form" value="<?php echo $class_info[0]['student_code_short_form']; ?>">
		</div>
	</fieldset>
	<div class="float-right">
		<input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $class_info[0]['id']; ?>">
		<input class="btn btn-primary" type="submit" value="Update">
	</div>
</form>
