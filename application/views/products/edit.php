<script>

    function myFunctionName() {

      var checkboxes = document.getElementsByName("sub_category_list[]");
      var checkboxesChecked = [];
      // loop over them all
      for (var i=0; i<checkboxes.length; i++) {


         if (checkboxes[i].checked) {
            checkboxesChecked.push(checkboxes[i]);
         }
      }
    if(checkboxesChecked.length)
    {
      return true;
    }
    alert('Please select at least one category.');
      return  false;
    }
</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>products/edit/<?php echo $products->id;?>" class="cmxform" method="post" enctype="multipart/form-data"  onSubmit="return myFunctionName()">
  <fieldset>

    <div class="row">
      <div class="col-md-8">
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('products').' '.$this->lang->line('name'); ?></label><span class="required_label">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="product_name" required name="product_name" value="<?php echo $products->product_name;?>">
        </div>
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('products').' '.$this->lang->line('code'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="product_code"  name="product_code" value="<?php echo $products->product_code;?>">
        </div>
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('phone').' '.$this->lang->line('number'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="phone_number"  name="phone_number" value="<?php echo $products->phone_number;?>">
        </div>
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('bikash').' '.$this->lang->line('marchent'); ?></label>
          <input type="text" autocomplete="off"  class="form-control" id="bikash_number"  name="bikash_number" value="<?php echo $products->bikash_number;?>">
        </div>
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('hot_deal'); ?></label>
          <select class="form-control" name="is_hot_deal">
    				<option value="0" <?php if($products->hot_deal=='0'){echo  'selected'; }?>><?php echo $this->lang->line('no'); ?></option>
    				<option value="1" <?php if($products->hot_deal=='1'){echo  'selected'; }?>><?php echo $this->lang->line('yes'); ?></option>
    			</select>
        </div>

        <div class="form-group col-md-12">
          <label for="inputEmail4">Short <?php echo $this->lang->line('description'); ?></label></label><span class="required_label">*</span></label>
            <textarea required="1" name="short_description" id="tinyMceExample1" maxlength="1000" onkeyup="count_alpha(this.value);"  rows="5" cols="50" class="form-control"><?php echo $products->short_description;?></textarea>
        </div>

        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('description'); ?></label></label><span class="required_label">*</span></label>
          	<textarea required="1" name="description" id="tinyMceExample"  rows="5" cols="50" class="form-control"><?php echo $products->description;?></textarea>
        </div>

        <div class="form-group col-md-12">
          <label for="upload">Feature Image <span class="required_label">*</span></label>
          <?php if(!empty($products->feature_image_path)){?>
          <img style="width: 100px;height:100px;" src="<?php echo base_url(). MEDIA_FOLDER; ?>/product/<?php echo $products->feature_image_path;?>" />
        <?php }?>
          <input type="file" name="txtFeaturePhoto" class="form-control">
          <input type="hidden" name="oldFeaturePhoto"  value="<?php echo $products->feature_image_path;?>">
          <span class="required_label" style="font-size: 11px;">
            * File Format ->JPEG , JPG and PNG.<br>
            * Image Size -> Height : 300PX and Width : 300PX
          </span>
        </div>

        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('upload').' '.$this->lang->line('image'); ?></label></label><span class="required_label">*</span></label>
          <div class="row">
          <div class="col-md-6">
            <?php if(!empty($products->image_path_1)){?>
						<img style="width: 100px;height:100px;" src="<?php echo base_url(). MEDIA_FOLDER; ?>/product/<?php echo $products->image_path_1;?>" />
					<?php }?>
           <input type="hidden" name="oldtxtPhoto1"  value="<?php echo $products->image_path_1;?>">
            <input type="file" name="txtPhoto1"  class="form-control"> <br>
						<?php if(!empty($products->image_path_2)){?>
						<img style="width: 100px;height:100px;" src="<?php echo base_url(). MEDIA_FOLDER; ?>/product/<?php echo $products->image_path_2;?>" />
					<?php }?>
					 <input type="hidden" name="oldtxtPhoto2"  value="<?php echo $products->image_path_2;?>">
            <input type="file" name="txtPhoto2"  class="form-control">
          </div>
          <div class="col-md-6">
						<?php if(!empty($products->image_path_3)){?>
						<img style="width: 100px;height:100px;" src="<?php echo base_url(). MEDIA_FOLDER; ?>/product/<?php echo $products->image_path_3;?>" />
					<?php }?>
					 <input type="hidden" name="oldtxtPhoto3"  value="<?php echo $products->image_path_3;?>">
            <input type="file" name="txtPhoto3"  class="form-control"><br>
						<?php if(!empty($products->image_path_4)){?>
						<img style="width: 100px;height:100px;" src="<?php echo base_url(). MEDIA_FOLDER; ?>/product/<?php echo $products->image_path_4;?>" />
					<?php }?>
					   <input type="hidden" name="oldtxtPhoto4"  value="<?php echo $products->image_path_4;?>">
            <input type="file" name="txtPhoto4"  class="form-control">
          </div>
         </div>
        </div>
      </div>
        <div class="col-md-4">
          <div class="form-group col-md-12">
            <label for="inputEmail4"><?php echo $this->lang->line('regular').' '.$this->lang->line('price'); ?></label>
            <input type="number" autocomplete="off" step="any"  class="form-control" id="regular_price"  name="regular_price" value="<?php echo $products->regular_price;?>">
          </div>
          <div class="form-group col-md-12">
            <label for="inputEmail4"><?php echo $this->lang->line('standard').' '.$this->lang->line('price'); ?></label><span class="required_label">*</span>
            <input type="number" autocomplete="off" step="any"  class="form-control" id="standard_price" required name="standard_price" value="<?php echo $products->standard_price;?>">
          </div>
          <div class="form-group col-md-12" >
            <label for="inputEmail4"><?php echo $this->lang->line('category'); ?></label><span class="required_label">*</span>
            <div style="height: 600px;overflow-y: auto;">
              <table id="alldata" class="table table-sm" style="width:100%">
                        <tbody>
                          <?php
                          if (count($sub_category)) {
                              foreach($sub_category as $list) {?>
                                <tr>

                                  <td colspan="1" style="background:#e3e8e7;">
                                    <div class="form-group mb-0">
                                      <div class="form-check">
                                        <label class="form-check-label" style="display: block;margin-left:2px;">
                                            <!-- <input class="ctgs form-check-input" name="sub_list[]" value="<?php echo $list['id'];?>" type="checkbox"> -->
                                          <?php echo $list['name'];?>
                                        </label>
                                      </div>
                                    </div>
                                  </td>
                              </tr>
                                <?php (int)$row=0;
                                foreach($list['sub_category'] as $product_details) {?>
                                     <tr>

                                   <td colspan="3">
                                     <div class="form-group ml-4 mb-0">
                                       <div class="form-check">
                                         <label class="form-check-label" style="display: block;">
                                             <input class="ctgs form-check-input" name="sub_category_list[]" <?php if($product_details['active']){echo  'checked'; }?> value=" <?php echo $product_details['id'];?>" type="checkbox">
                                               <?php echo $product_details['name'];?>
                                         </label>
                                       </div>
                                     </div>
                                   </td>
                               </tr>
                                 <?php
                                }
                              }

                          }
                          ?>


                              </tbody>
                      </table>
              </div>

          </div>
        </div>
    </div>

  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" name="submit" value="<?php echo $this->lang->line('update'); ?>">

  </div>
</form>
