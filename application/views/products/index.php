   <div class="table-sorter-wrapper col-lg-12 table-responsive">
     <table id="sortable-table-1" class="table">
                  <thead>
                      <tr>
                        <th><?php echo $this->lang->line('sl'); ?></th>
                        <th><?php echo $this->lang->line('title'); ?></th>
						            <th><?php echo $this->lang->line('item').' '.$this->lang->line('code'); ?> </th>
                        <th><?php echo $this->lang->line('regular') . ' ' .$this->lang->line('price'); ?></th>
                        <th><?php echo $this->lang->line('standard') . ' ' .$this->lang->line('price'); ?></th>
                        <th><?php echo $this->lang->line('publish') . ' ' .$this->lang->line('date'); ?></th>
                        <th><?php echo $this->lang->line('actions'); ?></th>
                      </tr>
                  </thead>
                  <tbody>
                        <?php
                          $i = (int)$this->uri->segment(3);
                        foreach ($products as $row):
                          $i++;
                          ?>
                          <tr>

                          <tr>
                              <td>
                                  <?php echo $i; ?>
                              </td>
                              <td><?php echo $row['product_name']; ?></td>
						              	  <td><?php echo $row['product_code']; ?></td>
                              <td><?php echo $row['regular_price']; ?></td>
                              <td><?php echo $row['standard_price']; ?></td>
                              <td><?php echo $row['publish_date']; ?></td>
                              <td>

                                    <div class="dropdown">
                                        <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ti-pencil-alt"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
									                      		<a class="dropdown-item" href="<?php echo base_url(); ?>products/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>products/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                                        </div>
                                    </div>
                              </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                </table>
                <div class="float-right">
                <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
