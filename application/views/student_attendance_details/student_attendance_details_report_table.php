<style>
	span.b {
		display: inline-block;
		width: 18%;
		height: auto;
		padding: 4px;
		border: 1px solid #000000;
	}

	.bac_color{
		background-color: #e0e0eb;
	}

	@media print {
		.bac_color{
			background-color: #e0e0eb;
		}
	}
</style>

<table width="100%" cellpadding="0" class="data_table" cellspacing="0" id="box-table-a" summary="StudentAttendanceReport">
    <thead>
	<tr>
		<th style="border: none;" colspan="<?php echo $number_of_days + 4;?>">
			<span class="b">Class: <?php echo $class_name; ?></span>
			<span class="b">Shift: <?php echo $shift_name; ?></span>
			<span class="b">Section: <?php echo $section_name; ?></span>
			<span class="b">Period: <?php echo $period_name; ?></span>
			<span class="b">
				Month: <?php
				$dateObj = DateTime::createFromFormat('!m', $month);
				echo $dateObj->format('F') . ', ' . $year;
				?>
			</span>
		</th>
	</tr>

    <tr>
        <th scope="col" class="td_center bac_color">&nbsp;<?php echo $this->lang->line('student_code'); ?></th>
        <th scope="col" class="td_center bac_color">&nbsp;<?php echo $this->lang->line('name'); ?></th>
        <th scope="col" class="td_center bac_color">&nbsp;<?php echo $this->lang->line('roll'); ?></th>
        <?php
        $i = 1;
        while ($i <= $number_of_days) {
            if ($i < 10) {
                $clm_date = '0' . $i;
            } else {
                $clm_date = $i;
            }
            echo '<th scope="col" class="td_center bac_color">&nbsp;' . $clm_date . '</th>';
            $i++;
        }
        ?>
        <th scope="col" class="td_center bac_color">&nbsp;<?php echo $this->lang->line('summary'); ?></th>
    </tr>

    </thead>


    <tbody>
    <?php
    $j = 0;
    foreach ($adata as $row):
        $j++;
        ?>
        <tr>
			<td style="min-width: 70px;"  class="td_center">
				<?php echo $row['student_code']; ?>
			</td>
            <td style="min-width: 200px;" class="td_left">
                <?php echo $row['name']; ?>
            </td>
            <td class="td_center"><?php echo $row['roll_no']; ?></td>
            <?php echo $row['data']; ?>
            <td class="td_center">
                    P-<?php echo $row['total_present_days']; ?><br>
                    A-<?php echo $row['total_absent_days']; ?><br>
                    L-<?php echo $row['total_leave_days']; ?><br>
                    H-<?php echo $row['total_holidays']; ?>
			</td>
        </tr>
    <?php endforeach; ?>
    </tbody>

</table>
