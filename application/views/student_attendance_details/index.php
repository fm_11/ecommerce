<form name="addForm" class="cmxform" id="commentForm" target="_blank"  action="<?php echo base_url(); ?>student_attendance_details/index" method="post">
	<div class="form-row">
		<div class="form-group col-md-2">
			<label><?php echo $this->lang->line('year'); ?></label>
			<select class="js-example-basic-single w-100" name="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option value="<?php echo $list['value']; ?>" <?php if (isset($year)) {
							if ($year == $list['value']) {
								echo 'selected';
							}
						} ?>><?php echo $list['text']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-2">
			<label>Month</label>
			<select class="js-example-basic-multiple w-100" name="month" id="month" required="1">
				<?php
				$i = 1;
				while ($i <= 12) {
					$dateObj = DateTime::createFromFormat('!m', $i); ?>
					<option value="<?php echo $i; ?>" <?php if(isset($month)){if ($i == $month) {
						echo 'selected';
					}}  ?>><?php echo $dateObj->format('F'); ?></option>
					<?php
					$i++;
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
			<select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
							<?php if (isset($class_shift_section_id)) {
								if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-2">
			<label><?php echo $this->lang->line('group'); ?></label>
			<select name="group_id" id="group_id" class="js-example-basic-single w-100" required="1">
				<option value="all">-- <?php echo $this->lang->line('all'); ?> --</option>
				<?php foreach ($GroupList as $row) { ?>
					<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Class Period</label>
			<select name="period_id" id="period_id" class="js-example-basic-single w-100" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<option value="FM">From Machine (Login & Logout)</option>
				<?php foreach ($period_list as $row) { ?>
					<option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="btn-group float-right">
		<!--		<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>-->
		<input type="submit" class="btn btn-primary" value="View Report">
	</div>
</form>
