<style>
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
            min-height: 20px !important;	
	}
        input[type='radio']{
            width:40px;
        }
</style>
<script type="text/javascript">
    $(document).ready(function(){	
        var skt_required =$("#cbo_is_skt_required").val();		
        if(skt_required=='0'){
                $('#txt_skt_amount').attr('readonly',"readonly");
        }
        else{
                $('#txt_skt_amount').removeAttr('readonly');
        }
        $("#cbo_is_skt_required").change(function(){			
            if($("#cbo_is_skt_required").val() == 1) {				
                $("#txt_skt_amount").removeAttr('readonly');
            }
            else{
                $("#txt_skt_amount").val('');
                $("#txt_skt_amount").attr('readonly',"readonly");
            }
        });					
    });
</script>
<?php
    echo form_open('config_generals/member_configuration');
    echo form_hidden('txt_id',isset($row->id)?$row->id:"");
?>
<?php //print_r($row);die;?>
<fieldset>
<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">
    <tr>
        <td width="100%" colspan=2>
            <div class="formContainer" style="border:none;width:98%">
                <ol style="border:none;width:98%"> 					
                    <li>
                        <label for="cbo_nominee_information">Nominee Information Required for Member <span class="required_field_indicator">*</span></label>
                        <div class="form_input_container">
                            <?php
                            //echo form_dropdown('cbo_nominee_information',$nominee_info,set_value('cbo_nominee_information',isset($row->nominee_info)?$row->nominee_info:""),'id="cbo_nominee_information"');
                            //echo form_error('cbo_nominee_information'); 
                            ?>
                            <div style="float:left;"><?php
                                //print_r($row->member_wise_nominee_info_required);
                                $yes = array(
                                    'name' => 'cbo_nominee_information',
                                    'id' => 'cbo_nominee_information_yes',
                                    'value' => '1',
                                    'style' => 'margin:0px 10px',
                                    //'checked'     => isset($row->is_branch_auto_code_need)?$row->is_branch_auto_code_need:"",
                                    'checked' => (isset($row->member_wise_nominee_info_required) && ($row->member_wise_nominee_info_required == 1)) ? 'checked' : ''
                                );
                                echo form_radio($yes) . "YES";
                                ?></div>
                            <div style="float:left;"><?php
                                $no = array(
                                    'name' => 'cbo_nominee_information',
                                    'id' => 'cbo_nominee_information_no',
                                    'value' => '0',
                                    'style' => 'margin:0px 10px',
                                    //'checked'     => (isset($is_branch_auto_code_need)&&$is_branch_auto_code_need==1)?'':(! isset($row->is_branch_auto_code_need) || (isset($row->is_branch_auto_code_need) && $row->is_branch_auto_code_need =='0'))?'checked':"",
                                    'checked' => (isset($row->member_wise_nominee_info_required) && ($row->member_wise_nominee_info_required == 0)) ? 'checked' : ''
                                );
                                echo form_radio($no) . "NO";
                                ?></div>
                                <?php echo form_error('cbo_nominee_information'); ?> 
                        </div>
                    </li>											
                    <li>
                        <label for="cbo_both_father_spouse_name_required_for_member">Both Father and Spouse name required for member<span class="required_field_indicator">*</span></label>
                        <div class="form_input_container">
                            <?php
                            echo form_dropdown('cbo_both_father_spouse_name_required_for_member', $options, set_value('cbo_both_father_spouse_name_required_for_member', isset($row->is_both_father_spouse_name_required_for_member) ? $row->is_both_father_spouse_name_required_for_member : ""));
                            echo form_error('cbo_both_father_spouse_name_required_for_member');
                            ?>
                        </div>
                    </li>
                    <li>
                        <label for="txt_is_skt_required">Is SKT required?<span class="required_field_indicator">*</span></label>
                        <div class="form_input_container">
                            <?php
                            echo form_dropdown('cbo_is_skt_required', $options, set_value('cbo_is_skt_required', isset($row->is_SKT_required) ? $row->is_SKT_required : ""), 'id="cbo_is_skt_required"');
                            echo form_error('cbo_is_skt_required');
                            ?>
                        </div>
                    </li>
                    <li>
                        <label for="txt_skt_amount">SKT Amount</label>
                        <div class="form_input_container">
                            <?php
                            echo form_input(array('name' => 'txt_skt_amount', 'class' => 'input_textbox', 'maxlength' => '50'), set_value('txt_skt_amount', isset($row->SKT_amount) ? $row->SKT_amount : ""), 'id="txt_skt_amount"');
                            echo form_error('txt_skt_amount');
                            ?>
                        </div>
                    </li>					
                    <li>
                        <label for="cbo_is_date_of_birth_required_in_member_information">Is Date of Birth required in Member Information?<span class="required_field_indicator">*</span></label>
                        <div class="form_input_container">
                            <?php
                            echo form_dropdown('cbo_is_date_of_birth_required_in_member_information', $options, set_value('cbo_is_date_of_birth_required_in_member_information', isset($row->is_date_of_birth_required_in_member_information) ? $row->is_date_of_birth_required_in_member_information : ""), 'id="cbo_is_date_of_birth_required_in_member_information"');
                            echo form_error('cbo_is_date_of_birth_required_in_member_information');
                            ?>
                        </div>
                    </li>
                    <li>
                        <label for="cbo_is_national_ID_required_for_member">Is National ID/Birth Registration No. Required?<span class="required_field_indicator">*</span></label>
                        <div class="form_input_container">
                            <?php
                                $national_id_birth_registration_options=array(0=>"No",1=>"Only National Id",2=>"Only Birth Registration No",3=>"Any One",4=>"Both");
                                echo form_dropdown('cbo_is_national_id_birth_reg_no_required',$national_id_birth_registration_options,  set_value('cbo_is_national_id_birth_reg_no_required',isset($row->is_national_id_or_birth_reg_no_required)?$row->is_national_id_or_birth_reg_no_required:0),'class=input_select');
                                echo form_error('cbo_is_national_id_birth_reg_no_required'); 
                            ?> 
                        </div>

                    </li>					
                </ol>
            </div>
        </td>
    </tr>
    <tr>
        <td class="formBottomBar">
            <div class="buttons" style="margin:0px 0px 0px 20px;">
<?php echo form_submit(array('name' => 'submit', 'id' => 'submit', 'class' => 'submit_buttons positive'), 'Save'); ?>
<?php echo form_button(array('name' => 'button', 'id' => 'button', 'value' => 'true', 'type' => 'reset', 'content' => 'Reset', 'class' => 'reset_buttons')); ?>
<?php echo form_button(array('name' => 'button', 'id' => 'button', 'value' => 'true', 'type' => 'reset', 'content' => 'Cancel', 'class' => 'cancel_buttons', 'onclick' => "window.location.href='" . site_url('config_generals/view') . "'")); ?>
            </div>
        </td>
        <td class="formBottomBar"></td>
    </tr>
</table>
</fieldset>
