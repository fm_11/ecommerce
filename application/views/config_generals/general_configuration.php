<style>
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
		min-height: 20px!important;	
	}	
</style>
<script type="text/javascript">
	//datepicker
	$(function(){
		$("#txt_po_establishment_date").datepicker({dateFormat: 'yy-mm-dd'});
		$("#txt_sw_start_date_of_operation").datepicker({dateFormat: 'yy-mm-dd'});	
	});
</script>
<?php
	echo form_open_multipart('config_generals/general_configuration');
	$img_name = '/media/images/add_big.png';
	$class_name = 'class="formTitleBar_edit"';
	$select_class = 'class="input_select"';
?>
<?php //print_r($row);die;?>
<fieldset>
<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">
		<tr>
			<td width="100%" colspan=2>
				<div class="formContainer" style="border:none;width:98%">
					<ol style="border:none;width:98%"> 
						<li>
							<label for="txt_po_name">Organization Name:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_po_name','id'=>'txt_po_name','class'=>'input_textbox','maxlength'=>'150');
								echo form_input($attr,set_value('txt_po_name',isset($row->po_name)?$row->po_name:""));
								echo form_error('txt_po_name'); 
							?>
							</div>
						</li>  
						<li>
							<label for="txt_po_code">Organization Code:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_po_code','id'=>'txt_po_code','class'=>'input_textbox','maxlength'=>'50');
								echo form_input($attr,set_value('txt_po_code',isset($row->po_code)?$row->po_code:""));
								echo form_error('txt_po_code'); 
							?>
							</div>
						</li>  
						<li>
							<label for="txt_po_establishment_date">Organization Establishment Date:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_po_establishment_date','id'=>'txt_po_establishment_date','class'=>'date_picker','maxlength'=>'10');
								echo form_input($attr,set_value('txt_po_establishment_date',isset($row->po_establishment_date)?$row->po_establishment_date:""));
								echo form_error('txt_po_establishment_date'); 
							?>
							</div>
						</li>  
						<li>
							<label for="txt_po_logo">Organization Logo:</label>
							<div class="form_input_container">
								<?php echo form_hidden('txt_po_logo_edit',isset($row->po_logo)?$row->po_logo:"");	?>
								<input type="file" id="txt_po_logo"  name="txt_po_logo" size="20" />
								<span class="explain" id="explain">File must be .jpg, .gif or .png and Allowed size <?php echo IMAGE_UPLOAD_SIZE . "KB"; ?></span><span style="float:right;"><?php if (isset($row->po_logo)) echo img(array('src'=>base_url().IMAGE_UPLOAD_PATH.$row->po_logo,'border'=>'0','alt'=>'','width'=>'25','height'=>'25'))?></span>	<?php echo form_error('txt_po_logo'); ?>
							</div>
						</li>
						<li>
								<label for="txt_is_insurance_required">Is Insurance Required?<span class="required_field_indicator">*</span></label>
								<div class="form_input_container">
								<?php 
									echo form_dropdown('cbo_is_insurance_required',$options,set_value('cbo_is_insurance_required',isset($row->is_insurance_required)?$row->is_insurance_required:""),'id="cbo_is_insurance_required"');
									echo form_error('cbo_is_insurance_required');?>
								</div>
						</li>
						<li>
							<label for="txt_multiplier_death_member_claim_insurance_amount">Insurance Claim Multiplier:<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_multiplier_death_member_claim_insurance_amount','id'=>'txt_multiplier_death_member_claim_insurance_amount','class'=>'input_textbox','maxlength'=>'2');
								echo form_input($attr,set_value('txt_multiplier_death_member_claim_insurance_amount',isset($row->multiplier_death_member_claim_insurance_amount)?$row->multiplier_death_member_claim_insurance_amount:""));
								echo form_error('txt_multiplier_death_member_claim_insurance_amount'); 
							?>
							</div>
						</li>						
						 		
					</ol>
				</div>
			</td>
		</tr>
		<tr>
			<td class="formBottomBar">
				<div class="buttons" style="margin:0px 0px 0px 20px;">
					<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>					
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals/view')."'"));?>
				</div>
			</td>
			<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
