
<hr>
<?php
$action = $this->uri->segment(2);
$controller = $this->uri->segment(1);
echo form_open_multipart($controller . '/' . $action);
$img_name = '/media/images/add_big.png';
$class_name = 'class="formTitleBar_edit"';
$select_class = 'class="input_select"';
?>

<div class="submit_form">
    <?php
    foreach ($row as $value) {
        $is_required = '';
        if ($value->is_required) {
            $is_required = "required='required'";
        }
        $label_class = "";
        if ($value->is_required == 1) {
            $label_class = " class='form-required' ";
        }
        if ($value->is_admin_access_only == "0" || $login_name == "admin") {
            echo "<div class='form-field'><label" . $label_class . ">", $value->label_name;

            echo "</label>";
            if ($value->field_type == "text") {
                echo form_input($value->field_name, set_value($value->field_name, isset($value->default_value) ? $value->default_value : ''), 'id="' . $value->field_name . '" class="input-text-short"' . $is_required);
            } elseif ($value->field_type == "textarea") {
                echo form_textarea($value->field_name, set_value($value->field_name, isset($value->default_value) ? $value->default_value : ''), 'id="' . $value->field_name . '" ' . $is_required);
            } elseif ($value->field_type == "password") {
                echo form_password($value->field_name, set_value($value->field_name, isset($value->default_value) ? $value->default_value : ''), 'id="' . $value->field_name . '" pattern=".{8,}" class="input-text-short" title="Password Minimum 8 Characters" ' . $is_required);
            } elseif ($value->field_type == "date") {
                echo form_input($value->field_name, set_value($value->field_name, isset($value->default_value) ? $value->default_value : ''), 'id="' . $value->field_name . '" type="date" class="input-text-short date_picker" ' . $is_required);
            } elseif ($value->field_type == "file") {
                echo form_upload($value->field_name, set_value($value->field_name, isset($value->default_value) ? $value->default_value : ''), 'id="' . $value->field_name . '"  ' . $is_required);
                if ($value->db_field_name == "po_logo") {
                    echo "<span class='explain' id='explain'>File must be .jpg, .gif or .png and Allowed size " . IMAGE_UPLOAD_SIZE . "KB</span><span style='float:right;'>";
                    if ($value->default_value) {
                        echo img(array('src' => base_url() . IMAGE_UPLOAD_PATH . $value->default_value, 'border' => '0', 'alt' => '', 'width' => '25', 'height' => '25'));
                    }
                    echo "</span>";
                }
            } elseif ($value->field_type == "select") {
                $drop_down_options = json_decode($value->field_value);
                //echo "<pre>";print_r($value);echo "</pre>";
                $array = array();
                foreach ($drop_down_options as $member => $data) {
                    $array[$member] = $data;
                }
                $drop_down_options = $array;
                echo form_dropdown($value->field_name, $drop_down_options, set_value($value->field_name, isset($value->default_value) ? $value->default_value : ''), 'id="' . $value->field_name . '" ' . $is_required);
            } elseif ($value->field_type == "number") {
                echo form_input($value->field_name, set_value($value->field_name, isset($value->default_value) ? $value->default_value : ''), 'id="' . $value->field_name . '" type="number" ' . $is_required);
            } elseif ($value->field_type == "radio") {
                $radio_options = json_decode($value->field_value);
                foreach ($radio_options as $index => $radio_option) {
                    $is_checked = $value->default_value == $index ? true : false;
                    echo "<div class='radio_div'>", form_radio($value->field_name, $index, $is_checked), "&nbsp;", $radio_option, "</div>";
                }
                if ($value->db_field_name == "is_auto_voucher_used") {
                    echo "<span class='explain' id='explain'>[If select Yes, then not possible to AIS day-end before MIS day-end]</span>";
                }
            } elseif ($value->field_type == "checkbox") {
                $checkbox_options = json_decode($value->field_value);
                $checkbox_value = null;
                $checkbox_label = null;
                foreach ($checkbox_options as $checkbox_index => $chkbox_lbl) {
                    $checkbox_value = $checkbox_index;
                    $checkbox_label = $chkbox_lbl;
                }
                $is_checked = $value->default_value == null ? false : true;
                echo "<div class='radio_div'>", form_checkbox($value->field_name, $checkbox_value, $is_checked), "&nbsp;", $checkbox_label, "</div>";
            }
            echo form_error($value->field_name);
            echo "&nbsp;";
            echo "</div>";
        }
    }
    ?>

    <hr>

    <div class="buttons-container">
        <div class="buttons-floating-container hidden" style="display: none;"></div>
        <div class="buttons-container-placeholder">
          <div class="float-right">
             <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
          </div>
    </div>

</div>
