<style type="text/css">
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
		min-height: 20px!important;	
	}	
</style>
<script>
$(document).ready(function(){
   if($("#cbo_show_spouse_name").val()==0){
       $("#cbo_spouse_head").attr('disabled',"disabled");
       $("#cbo_spouse_head").val(null);
       $("#cbo_spouse_head").attr('disabled',"disabled");  
       $("#temp").append("<input type='hidden' name='cbo_spouse_head' id='txt_spouse_head' value='' />");
   }
   if($("#cbo_purpose_option").val()==0){
       $("#cbo_collection_sheet_options").val('0');
       $("#cbo_collection_sheet_options").attr('disabled',"disabled");  
       $("#temp").append("<input type='hidden' name='cbo_collection_sheet_options' id='txt_collection_sheet_options' value='0' />");
   }
   if($("#cbo_auto_amount_type").val()==0){
       $("#cbo_show_advance_field").val('0');
       $("#cbo_show_advance_field").attr('disabled',"disabled");  
       $("#temp").append("<input type='hidden' name='cbo_show_advance_field' id='txt_show_advance_field' value='0' />");
   }
   $("#cbo_purpose_option").change(function(){
      if($(this).val()=="0"){
        $("#cbo_collection_sheet_options").val('0');
        $("#cbo_collection_sheet_options").attr('disabled',"disabled");
        //$("#cbo_collection_sheet_options").remove();
         $("#txt_collection_sheet_options").remove();
        $("#temp").append("<input type='hidden' name='cbo_collection_sheet_options' id='txt_collection_sheet_options' value='0' />");
      }
      else {
          $("#cbo_collection_sheet_options").removeAttr('disabled');
          $("#txt_collection_sheet_options").remove();
      }
   });
   $("#cbo_auto_amount_type").change(function(){
      if($(this).val()=="0"){
        $("#cbo_show_advance_field").val('0');
        $("#cbo_show_advance_field").attr('disabled',"disabled");
        //$("#cbo_collection_sheet_options").remove();
         $("#txt_show_advance_field").remove();
        $("#temp").append("<input type='hidden' name='cbo_show_advance_field' id='txt_show_advance_field' value='0' />");
      }
      else {
          $("#cbo_show_advance_field").removeAttr('disabled');
          $("#txt_show_advance_field").remove();
      }
   });
   $("#cbo_show_spouse_name").change(function(){
      if($(this).val()=="0"){
        $("#cbo_spouse_head").val('');
        $("#cbo_spouse_head").attr('disabled',"disabled");
        //$("#cbo_collection_sheet_options").remove();
        $("#txt_spouse_head").remove();
        $("#temp").append("<input type='hidden' name='cbo_spouse_head' id='txt_cbo_spouse_head' value='' />");
      }
      else {
          $("#cbo_spouse_head").removeAttr('disabled');
          $("#txt_spouse_head").remove();
      }
   });
});
</script>
<?php
    echo form_open('config_generals/config_collectionsheets');
    
?>
<fieldset>
	<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">
		<tr>
			<td width="100%" colspan="2">
				<div class="formContainer" style="border:none;width:98%">
					<ol style="border:none;width:98%"> 
                                            <?php
                                            foreach ($collection_sheet_configurations as $collectionsheet) {
                                               // echo "<pre>";print_r($collectionsheet);die;
                                                echo "<li><label for='".$collectionsheet->field_name."'>".$collectionsheet->label_name."</label>";
                                                echo "<div class='form_input_container'>";
                                                    if($collectionsheet->input_type=="select"){
                                                        $select_options=array();
                                                        $value=explode(",",$collectionsheet->value);
                                                        if(is_array($value)){
                                                            foreach($value as $new_value){
                                                                $new_value=explode("=>",$new_value);
                                                                for($i=0;$i<count($new_value);$i+=2){
                                                                    $select_options[$new_value[$i]]=$new_value[$i+1];
                                                                }                                                            
                                                            }                                                           
                                                        }
                                                        echo form_dropdown($collectionsheet->field_name, $select_options,$collectionsheet->default_value,' id="'.$collectionsheet->field_name.'"');
                                                    }
                                                    else if($collectionsheet->input_type="text"){
                                                        echo form_input($collectionsheet->field_name,$collectionsheet->default_value,' id="'.$collectionsheet->field_name.'"');
                                                    }
                                                echo "</div></li>";
                                            }
                                            ?>
							
					</ol>
				</div>
                            <div id="temp"></div>
			</td>
		</tr>
		<tr>
			<td class="formBottomBar">
				<div class="buttons" style="margin:0px 0px 0px 20px;">
					<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals/view')."'"));?>
				</div>
			</td>
			<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
