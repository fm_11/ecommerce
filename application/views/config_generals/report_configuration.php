<style>
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
		min-height: 20px!important;	
	}	
</style>
<?php
    echo form_open('config_generals/report_configuration');
    echo form_hidden('txt_id',isset($row->id)?$row->id:"");
?>
<fieldset>
	<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">
		<tr>
			<td width="100%" colspan=2>
				<div class="formContainer" style="border:none;width:98%">
					<ol style="border:none;width:98%"> 				
						<li>
							<label for="txt_report_header_line_1">Report Header Line #1<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_report_header_line_1','class'=>'input_textbox'),set_value('txt_report_header_line_1',isset($row->report_header_line_1)?$row->report_header_line_1:""));
								echo form_error('txt_report_header_line_1'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_report_header_line_2">Report Header Line #2<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_report_header_line_2','class'=>'input_textbox'),set_value('txt_report_header_line_2',isset($row->report_header_line_2)?$row->report_header_line_2:""));
								echo form_error('txt_report_header_line_2'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_report_header_line_3">Report Header Line #3<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_report_header_line_3','class'=>'input_textbox'),set_value('txt_report_header_line_3',isset($row->report_header_line_3)?$row->report_header_line_3:""));
								echo form_error('txt_report_header_line_3'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_report_footer_line_1">Report Footer Line #1<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 								
								echo form_input(array('name'=>'txt_report_footer_line_1','class'=>'input_textbox'),set_value('txt_report_footer_line_1',isset($row->report_footer_line_1)?$row->report_footer_line_1:""));
								echo form_error('txt_report_footer_line_1'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_report_footer_line_2">Report Footer Line #2<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 
								echo form_input(array('name'=>'txt_report_footer_line_2','class'=>'input_textbox'),set_value('txt_report_footer_line_2',isset($row->report_footer_line_2)?$row->report_footer_line_2:""));
								echo form_error('txt_report_footer_line_2'); 
							?>
							</div>
						</li>	
					</ol>
				</div>
			</td>
		</tr>
		<tr>
			<td class="formBottomBar">
				<div class="buttons" style="margin:0px 0px 0px 20px;">
					<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals/view')."'"));?>
				</div>
			</td>
			<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
