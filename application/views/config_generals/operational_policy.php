<style>
	label {
	    display: block;
	    float: left;
	    height: 20px;
	    width: 35%;
	    font-weight: bold;
	}
	ol li {
		min-height: 20px!important;	
	}	
</style>
<?php
    echo form_open('config_generals/operational_policy');
    echo form_hidden('txt_id',isset($row->id)?$row->id:"");
?>
<fieldset>
	<table class="addForm" border="0" cellspacing="0px" cellpadding="0px" width="100%">		
		<tr>
			<td width="100%" colspan=2>
				<div class="formContainer" style="border:none;width:98%">
					<ol style="border:none;width:98%">		
						
						<li>
							
							<label for="cbo_is_service_rules_present">Is Service Rules Present?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_service_rules_present',$options,set_value('cbo_is_service_rules_present',isset($row->is_service_rules_present)?$row->is_service_rules_present:""));
								echo form_error('cbo_is_service_rules_present'); 
							?>
							</div>
						</li>	
						
						<li>
							
							<label for="cbo_is_recruitment_policy_present">Is Recruitment Policy Present?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_recruitment_policy_present',$options,set_value('cbo_is_recruitment_policy_present',isset($row->is_recruitment_policy_present)?$row->is_recruitment_policy_present:""));
								echo form_error('cbo_is_recruitment_policy_present'); 
							?>
							</div>
						</li>
						
						<li>
							
							<label for="cbo_is_financial_policy_present">Is Financial Policy Present?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_financial_policy_present',$options,set_value('cbo_is_financial_policy_present',isset($row->is_financial_policy_present)?$row->is_financial_policy_present:""));
								echo form_error('cbo_is_financial_policy_present'); 
							?>
							</div>
						</li>
						
						<li>
							
							<label for="cbo_is_savings_and_credit_policy_present">Is Savings and Credit Policy Present?<span class="required_field_indicator">*</span></label>
							<div class="form_input_container">
							<?php 
								echo form_dropdown('cbo_is_savings_and_credit_policy_present',$options,set_value('cbo_is_savings_and_credit_policy_present',isset($row->is_savings_and_credit_policy_present)?$row->is_savings_and_credit_policy_present:""));
								echo form_error('cbo_is_savings_and_credit_policy_present'); 
							?>
							</div>
						</li>
						<li>
							<label for="txt_mra_licence_no">Mra Licence No:<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_mra_licence_no','id'=>'txt_mra_licence_no','class'=>'input_textbox','maxlength'=>'50');
								echo form_input($attr,set_value('txt_mra_licence_no',isset($row->mra_licence_no)?$row->mra_licence_no:""));
								echo form_error('txt_mra_licence_no'); 
							?>
							</div>
						</li>  
						<?php if(isset($login_name) && $login_name == 'admin') { ?>
						<li>
							<label for="txt_mra_licence_no">How long day end process can be deleted?<span class="required_field_indicator"></span></label>
							<div class="form_input_container">
							<?php 
								$attr = array('name'=>'txt_how_long_day_end_deleted','id'=>'txt_how_long_day_end_deleted','class'=>'input_textbox','maxlength'=>'4');
								echo form_input($attr,set_value('txt_how_long_day_end_deleted',isset($row->how_long_day_end_deleted)?$row->how_long_day_end_deleted:""));
								echo form_error('txt_how_long_day_end_deleted'); 
							?>
							</div>
						</li>  
						<?php } ?>				
					</ol>
				</div>
			</td>
		</tr>
		<tr>
			<td class="formBottomBar">
				<div class="buttons" style="margin:0px 0px 0px 20px;">
					<?php echo form_submit(array('name'=>'submit','id'=>'submit','class'=>'submit_buttons positive'),'Save');?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Reset','class'=>'reset_buttons'));?>
					<?php echo form_button(array('name'=>'button','id'=>'button','value'=>'true','type'=>'reset','content'=>'Cancel','class'=>'cancel_buttons','onclick'=>"window.location.href='".site_url('config_generals/view')."'"));?>
				</div>
			</td>
			<td class="formBottomBar"></td>
		</tr>
	</table>
</fieldset>
