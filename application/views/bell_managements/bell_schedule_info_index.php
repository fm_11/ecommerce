<a class="button_grey_round" style="margin-bottom: 5px;"
   href="<?php echo base_url(); ?>bell_managements/add_schedule_data"><span>Add New Schedule</span></a>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="20" scope="col">SL</th>
        <th width="140" scope="col">From Date</th>
        <th width="100" scope="col">To Date</th>
        <th width="20" scope="col">Day</th>      
        <th width="100" scope="col">Action</th>

    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($infos as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['from_date']; ?></td>
            <td><?php echo $row['to_date']; ?></td>
            <td><?php echo $row['for_day']; ?></td>          
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>bell_managements/view_details/<?php echo $row['id']; ?>"
                   title="View">View</a>&nbsp;
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>

    <tr class="footer">
        <td colspan="10" style="color: #000000;" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
</table>

