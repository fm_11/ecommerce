<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>core_media/timepicker/jquery-clockpicker.min.css">

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>bell_managements/schedule_data_edit/<?php echo $this->uri->segment(3); ?>"
      method="post">
	  
	 <label>Date</label>
    <input type="text" autocomplete="off"  name="date" class="smallInput wide" size="20%" value="<?php echo $schedule_info[0]['date']; ?>">
	  
    <label>Bell Time</label>
    <div class="input-group bell_time pull-center" data-placement="left" data-align="top" data-autoclose="true">
		<input type="text" autocomplete="off"  name="bell_time" class="smallInput wide" value="<?php echo date('H:i:s', strtotime($schedule_info[0]['bell_time'])); ?>">
		<span class="input-group-addon">
		<span class="glyphicon glyphicon-time"></span>
		</span>
	</div>
	
	<label>Bell Type</label>
    <select class="smallInput" name="bell_type">
			<option value="tiff_and_leave.wav" <?php if($schedule_info[0]['bell_type'] == 'tiff_and_leave.wav'){echo 'selected';} ?>>Tiff</option>
			<option value="1.wav" <?php if($schedule_info[0]['bell_type'] == '1.wav'){echo 'selected';} ?>>1</option>
			<option value="2.wav" <?php if($schedule_info[0]['bell_type'] == '2.wav'){echo 'selected';} ?>>2</option>
			<option value="3.wav" <?php if($schedule_info[0]['bell_type'] == '3.wav'){echo 'selected';} ?>>3</option>
			<option value="4.wav" <?php if($schedule_info[0]['bell_type'] == '4.wav'){echo 'selected';} ?>>4</option>
			<option value="5.wav" <?php if($schedule_info[0]['bell_type'] == '5.wav'){echo 'selected';} ?>>5</option>
			<option value="6.wav" <?php if($schedule_info[0]['bell_type'] == '6.wav'){echo 'selected';} ?>>6</option>
			<option value="7.wav" <?php if($schedule_info[0]['bell_type'] == '7.wav'){echo 'selected';} ?>>7</option>
			<option value="8.wav" <?php if($schedule_info[0]['bell_type'] == '8.wav'){echo 'selected';} ?>>8</option>
			<option value="tiff_and_leave.wav" <?php if($schedule_info[0]['bell_type'] == 'tiff_and_leave.wav'){echo 'selected';} ?>>End</option>
	</select>
	
    <br>
    <br>
    <input type="submit" class="submit" value="Update">
</form>

<!--Time Picker-->
<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/calender/all.css">
<script src="<?php echo base_url(); ?>core_media/calender/jquery.js"></script>
<script src="<?php echo base_url(); ?>core_media/calender/core.js"></script>
<script src="<?php echo base_url(); ?>core_media/calender/widget.js"></script>
<script src="<?php echo base_url(); ?>core_media/calender/datepicker.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>core_media/timepicker/jquery-clockpicker.min.js"></script>
<script type="text/javascript">
    $('.bell_time').clockpicker({
        placement: 'bottom',
        align: 'right',
        autoclose: true,
        'default': '20:48'
    });
</script>