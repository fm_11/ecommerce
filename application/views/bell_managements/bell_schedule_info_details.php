<table width="100%" style="color:black;" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="20" scope="col">SL</th>
        <th width="140" scope="col">Date</th>
        <th width="100" scope="col">For day</th>
		<th width="100" scope="col">Bell Time</th>
        <th width="20" scope="col">Bell Type</th> 
        <th width="20" scope="col">Is Completed</th>		
        <th width="20" scope="col">Action</th>	
    </tr>
    </thead>
    <tbody>
    <?php
	$date = $schedule_list[0]['date'];
	
	$color1 = "#ccddff";
	$color2 = "#ffe6ff";
	$color = $color1;
	
    $i = (int)$this->uri->segment(3);
    foreach ($schedule_list as $row):
        $i++;
		if($date != $row['date']){
			if($color == $color1){
				$color = $color2;
			}else{
				$color = $color1;
			}		
			$date = $row['date'];
		}
        ?>
        <tr>

        <tr style="background-color:<?php echo $color; ?>">
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo $row['for_day']; ?></td>
            <td><?php echo $row['bell_time']; ?></td>          
            <td><?php echo $row['bell_type']; ?></td> 
			<td><?php echo $row['is_completed']; ?></td> 
			<td style="vertical-align:middle">
			<?php if($row['is_completed'] == '0'){ ?>
			<a href="<?php echo base_url(); ?>admin_logins/schedule_data_edit/<?php echo $row['id']; ?>"
                   class="edit_icon" title="Edit"></a>
                <a href="<?php echo base_url(); ?>admin_logins/schedule_data_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
			<?php } ?>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>  
</table>

