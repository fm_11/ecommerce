<script>
    $(function () {
        $("#from_date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>core_media/timepicker/jquery-clockpicker.min.css">
   
   <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>bell_managements/add_schedule_data" method="post" enctype="multipart/form-data">

        <label>From Date</label>
        <input type="text" autocomplete="off"  name="from_date" value="<?php echo date('Y-m-d'); ?>" id="from_date" class="smallInput"  required="1">
		
		<label>To Date</label>
        <input type="text" autocomplete="off"  name="to_date" value="<?php echo date('Y-m-d'); ?>"  id="to_date" class="smallInput"  required="1">
		
		<label>Day</label>
		<select name="for_day[]" multiple style="height:120px; width:150px;" class="smallInput">
		<option value="sun">Sun</option>
		<option value="mon">Mon</option>
		<option value="tue">Tue</option>
		<option value="wed">Wed</option>
		<option value="thu">Thu</option>
		<option value="fri">Fri</option>
		<option value="sat">Sat</option>
		</select>
		<hr>
		<table width="100%">
		      <tr>
			      <td width="20%">
						<label>Bell Type</label>
						<select class="smallInput" name="bell_type_1">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label>Bell Time</label>
						<div class="input-group bell_time_1 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_1" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				        <label>Bell Type</label>
						<select class="smallInput" name="bell_type_2">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_2 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_2" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				    <label>Bell Type</label>
						<select class="smallInput" name="bell_type_3">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_3 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_3" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				     <label> Bell Type</label>
						<select class="smallInput" name="bell_type_4">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label>First Bell Time</label>
						<div class="input-group bell_time_4 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_4" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				   <td width="20%">
				     <label> Bell Type</label>
						<select class="smallInput" name="bell_type_5">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_5 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_5" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
			  </tr>
			  
			  	  
			  <tr>
			    <td colspan="5"><hr></td>
			  </tr>
			  
			  <tr>
			      <td width="20%">
						<label>Bell Type</label>
						<select class="smallInput" name="bell_type_6">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label>Bell Time</label>
						<div class="input-group bell_time_6 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_6" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				        <label>Bell Type</label>
						<select class="smallInput" name="bell_type_7">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_7 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_7" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				    <label>Bell Type</label>
						<select class="smallInput" name="bell_type_8">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_8 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_8" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				     <label> Bell Type</label>
						<select class="smallInput" name="bell_type_9">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label>First Bell Time</label>
						<div class="input-group bell_time_9 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_9" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				   <td width="20%">
				     <label> Bell Type</label>
						<select class="smallInput" name="bell_type_10">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_10 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_10" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
			  </tr>
			  
			  <tr>
			    <td colspan="5"><hr></td>
			  </tr>
			  
			  <tr>
			      <td width="20%">
						<label>Bell Type</label>
						<select class="smallInput" name="bell_type_11">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label>Bell Time</label>
						<div class="input-group bell_time_11 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_11" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				        <label>Bell Type</label>
						<select class="smallInput" name="bell_type_12">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_12 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_12" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				    <label>Bell Type</label>
						<select class="smallInput" name="bell_type_13">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_13 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_13" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				     <label> Bell Type</label>
						<select class="smallInput" name="bell_type_14">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label>First Bell Time</label>
						<div class="input-group bell_time_14 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_14" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div
				  </td>
				   <td width="20%">
				     <label> Bell Type</label>
						<select class="smallInput" name="bell_type_15">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_15 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_15" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
			  </tr>
			  
			   <tr>
			    <td colspan="5"><hr></td>
			  </tr>
			  
			  <tr>
			      <td width="20%">
						<label>Bell Type</label>
						<select class="smallInput" name="bell_type_16">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label>Bell Time</label>
						<div class="input-group bell_time_16 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_16" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				        <label>Bell Type</label>
						<select class="smallInput" name="bell_type_17">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>
						<div class="input-group bell_time_17 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_17" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				    <label>Bell Type</label>
						<select class="smallInput" name="bell_type_18">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>						
						<div class="input-group bell_time_18 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_18" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				  <td width="20%">
				     <label> Bell Type</label>
						<select class="smallInput" name="bell_type_19">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label>Bell Time</label>						
						<div class="input-group bell_time_19 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_19" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
				   <td width="20%">
				     <label> Bell Type</label>
						<select class="smallInput" name="bell_type_20">
						<option value="no">-- Select --</option>
						<option value="tiff_and_leave.wav">Tiff</option>
						<option value="1.wav">1</option>
						<option value="2.wav">2</option>
						<option value="3.wav">3</option>
						<option value="4.wav">4</option>
						<option value="5.wav">5</option>
						<option value="6.wav">6</option>
						<option value="7.wav">7</option>
						<option value="8.wav">8</option>
						<option value="tiff_and_leave.wav">End</option>
						</select>

						<label> Bell Time</label>					
						<div class="input-group bell_time_20 pull-center" data-placement="left" data-align="top" data-autoclose="true">
						<input type="text" autocomplete="off"  name="bell_time_20" class="smallInput" value="09:00">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
						</span>
						</div>
				  </td>
			  </tr>
			  
		</table>
		
		
		
		
		
        <br> <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
    <br />

<div class="clear"></div><br />

<!--Time Picker-->
<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/calender/all.css">
<script src="<?php echo base_url(); ?>core_media/calender/jquery.js"></script>
<script src="<?php echo base_url(); ?>core_media/calender/core.js"></script>
<script src="<?php echo base_url(); ?>core_media/calender/widget.js"></script>
<script src="<?php echo base_url(); ?>core_media/calender/datepicker.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>core_media/timepicker/jquery-clockpicker.min.js"></script>
<script type="text/javascript">
    $('.bell_time_20,.bell_time_19,.bell_time_18,.bell_time_17,.bell_time_16,.bell_time_15,.bell_time_14,.bell_time_13,.bell_time_12,.bell_time_11,.bell_time_10,.bell_time_9,.bell_time_8,.bell_time_7,.bell_time_6,.bell_time_5,.bell_time_4,.bell_time_3,.bell_time_2,.bell_time_1').clockpicker({
        placement: 'bottom',
        align: 'right',
        autoclose: true,
        'default': '20:48'
    });
</script>