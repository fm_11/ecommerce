
  <div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th colspan="2" width="50" scope="col"><b>File Format Download</b></th>
    </tr>
    </thead>
    <tbody>
      <tr>
          <td>
              <b>Without Student ID (Short Data)</b>
          </td>
          <td>
              <a href="<?php echo base_url(); ?>uploads/StudentDataFormatShortForm.xls">Download</a>
          </td>
      </tr>
    <tr>
        <td>
            <b>With Student ID</b>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>uploads/Student Data Format With Student ID.xls">Download</a>
        </td>
    </tr>
    <tr>
        <td>
            <b>Without Student ID</b>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>uploads/Student Data Format Without Student ID.xls">Download</a>
        </td>
    </tr>
    </tbody>
</table>
 </div>
<h4 class="card-title">Upload Data</h4>
<hr>

<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>student_batch_adds/index" method="post"
      enctype="multipart/form-data">

      <div class="form-row">
            <div class="form-group col-md-3">
                <label>Year</label><br>
				<select class="js-example-basic-single w-100" name="txtYear" id="txtYear" required="1">
					<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					<?php
					$i = 0;
					if (count($years)) {
						foreach ($years as $list) {
							$i++; ?>
							<option
									value="<?php echo $list['value']; ?>"
									<?php if (isset($year)) {
										if ($year == $list['value']) {
											echo 'selected';
										}
									} ?>>
								<?php echo $list['text']; ?>
							</option>
							<?php
						}
					}
					?>
				</select>
            </div>

            <div class="form-group col-md-3">
              <label>ID Segment Year</label><br>
				<select class="js-example-basic-single w-100" name="txtIdSegmentYear" id="txtIdSegmentYear" required="1">
					<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					<?php
					$i = 0;
					if (count($years)) {
						foreach ($years as $list) {
							$i++; ?>
							<option
									value="<?php echo $list['value']; ?>"
									<?php if (isset($id_segment_year)) {
										if ($id_segment_year == $list['value']) {
											echo 'selected';
										}
									} ?>>
								<?php echo $list['text']; ?>
							</option>
							<?php
						}
					}
					?>
				</select>
            </div>

            <div class="form-group col-md-3">
				<label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label><br>
				<select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
					<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					<?php
					$i = 0;
					if (count($class_section_shift_marge_list)) {
						foreach ($class_section_shift_marge_list as $list) {
							$i++; ?>
							<option
									value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
									<?php if (isset($class_shift_section_id)) {
										if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
											echo 'selected';
										}
									} ?>>
								<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
							</option>
							<?php
						}
					}
					?>
				</select>
            </div>
			  <div class="form-group col-md-3">
				  <label>Group</label><br>
				  <select class="js-example-basic-single w-100" name="txtGroup" required="1">
					  <option value="">-- Please Select --</option>
					  <?php
					  $i = 0;
					  if (count($groups)) {
						  foreach ($groups as $list) {
							  $i++; ?>
							  <option value="<?php echo $list['id']; ?>" <?php if (isset($group_id)) {
								  if ($group_id == $list['id']) {
									  echo 'selected';
								  }
							  } ?>><?php echo $list['name']; ?></option>
							  <?php
						  }
					  }
					  ?>
				  </select>
			  </div>
      </div>


      <div class="form-row">
		  <div class="form-group col-md-3">
			  <label>Category</label><br>
			  <select class="js-example-basic-single w-100" name="category_id" id="category_id" required="1">
				  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				  <?php
				  if (count($categories)) {
					  foreach ($categories as $list) {
						  ?>
						  <option
								  value="<?php echo $list['id']; ?>"
								  <?php if(isset($category_id)){ if ($category_id == $list['id']) {
									  echo 'selected';
								  }} ?>><?php echo $list['name']; ?></option>
						  <?php
					  }
				  }
				  ?>
			  </select>
		  </div>
            <div class="form-group col-md-3">
              <label>Is Auto Student ID?</label><br>
              <select class="js-example-basic-single w-100" name="isAutoID" required="1">
                  <option value="Y">Yes</option>
                  <option value="N">No</option>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label>Data Format Type?</label><br>
              <select class="js-example-basic-single w-100" name="DataTypeFormat" required="1">
                  <option value="SF">Short Format</option>
                  <option value="LF">Long Data Format</option>
              </select>
            </div>
            <div class="form-group col-md-3">
              <label>File</label><br>
              <input type="file" name="txtFile" required="1" class="form-control"> * File Format -> Excel-2003

            </div>
      </div>

		<div class="float-right">
		<button type="submit" class="btn btn-primary mr-2">Upload</button>
		</div>
</form>


  <?php
  if(isset($returnDataForShow)){
	  echo $data_table;
  }
  ?>



