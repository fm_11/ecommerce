<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_batch_adds/excel_data_save" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<thead>
			<tr>
				<th scope="col">Roll</th>
				<th scope="col">Name (English)</th>
				<th scope="col">Name (Bangla)</th>
				<th scope="col">Gender</th>
				<th scope="col">Religion</th>
				<th scope="col">Father's Name</th>
				<th scope="col">Mother's Name</th>
				<th scope="col">Mobile</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i = 0;
			foreach ($returnDataForShow as $row):
				?>
				<tr>

				<tr>
					<td>
						<input style="width: 60px !important; padding: 6px;border:1px #737373 solid;" class="form-control" type="text" name="roll_<?php echo $i; ?>" value="<?php echo $row['roll_no']; ?>"/>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;" class="form-control" type="text" name="name_english_<?php echo $i; ?>" value="<?php echo $row['name']; ?>"/>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;"  class="form-control" type="text" name="name_bangla_<?php echo $i; ?>" value="<?php echo $row['name']; ?>"/>
					</td>
					<td>
						<select style="width: 100px !important;adding: 6px;border:1px #737373 solid; color: #000000;" class="form-control" name="gender_<?php echo $i; ?>" required="required">
							<option value="M" <?php if($row['gender'] == 'M'){ echo 'selected'; } ?>>Male</option>
							<option value="F" <?php if($row['gender'] == 'F'){ echo 'selected'; } ?>>Female</option>
							<option value="O" <?php if($row['gender'] == 'O'){ echo 'selected'; } ?>>Other</option>
						</select>
					</td>
					<td>
						<select style="width: 120px !important;adding: 6px;border:1px #737373 solid;color: #000000;" class="form-control" name="religion_<?php echo $i; ?>" required>
							<?php
							if (count($religions)) {
								foreach ($religions as $list) {
									?>
									<option value="<?php echo $list['id']; ?>" <?php if($row['religion'] == trim($list['name'])){ echo 'selected'; } ?>><?php echo $list['name']; ?></option>
									<?php
								}
							} ?>
						</select>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;"  class="form-control" type="text" name="fathers_name_<?php echo $i; ?>" value="<?php echo $row['father_name']; ?>"/>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;"  class="form-control" type="text" name="mothers_name_<?php echo $i; ?>" value="<?php echo $row['mother_name']; ?>"/>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;"  class="form-control" type="text" name="mobile_<?php echo $i; ?>" value="<?php echo $row['guardian_mobile']; ?>"/>
					</td>
				</tr>
			<?php $i++; endforeach; ?>
			</tbody>
		</table>
	</div>
	<input type="hidden" class="input-text-short" name="year"
		   value="<?php echo $year; ?>"/>

	<input type="hidden" class="input-text-short" name="id_segment_year"
		   value="<?php echo $id_segment_year; ?>"/>

	<input type="hidden" class="input-text-short" name="class_id"
		   value="<?php echo $class_id; ?>"/>

		<input type="hidden" class="input-text-short" name="section_id"
			   value="<?php echo $section_id; ?>"/>

		<input type="hidden" class="input-text-short" name="group_id"
			   value="<?php echo $group_id; ?>"/>

	  <input type="hidden" class="input-text-short" name="category_id"
		   value="<?php echo $category_id; ?>"/>

		<input type="hidden" class="input-text-short" name="shift_id"
			   value="<?php echo $shift_id; ?>"/>

		<input type="hidden" class="input-text-short" name="number_of_rows"
			   value="<?php echo $i; ?>"/>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>

</form>
