<script>
    function exam_by_year(year) {
        if (year != '') {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("exam").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo base_url(); ?>progress_report/get_only_annual_exam_by_year?year=" + year, true);
            xmlhttp.send();
        }
    }

    function show_off_roll_section(view_type){
        if(view_type == 'A'){
            document.getElementById("from_roll_section").style.display = "none";
            document.getElementById("to_roll_section").style.display = "none";
        }
        if(view_type == 'S'){
            document.getElementById("from_roll_section").style.display = "";
            document.getElementById("to_roll_section").style.display = "";
        }
    }
</script>

<form name="addForm" target="_blank" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>progress_report/index"
      enctype="multipart/form-data" method="post">

<div class="form-row">
  <div class="form-group col-md-3">
    <label>Year</label>
    <select class="js-example-basic-single w-100" onchange="exam_by_year(this.value)" name="year" id="year" required="1">
	  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
	  <?php
	  $i = 0;
	  if (count($years)) {
		  foreach ($years as $list) {
			  $i++; ?>
			  <option
				  value="<?php echo $list['value']; ?>"
				  <?php if (isset($year)) {
					  if ($year == $list['value']) {
						  echo 'selected';
					  }
				  } ?>>
				  <?php echo $list['text']; ?>
			  </option>
			  <?php
		  }
	  }
	  ?>
    </select>
  </div>

  <div class="form-group col-md-3">
    <label>Exam</label>
    <select class="js-example-basic-single w-100" id="exam" name="exam" required="1">
        <option value="">-- Select --</option>
    </select>
  </div>
  <div class="form-group col-md-3">
    <label>Class</label>
	  <label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
	  <select class="js-example-basic-single w-100" required
			  name="class_shift_section_id" id="class_shift_section_id">
		  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
		  <?php
		  $i = 0;
		  if (count($class_section_shift_marge_list)) {
			  foreach ($class_section_shift_marge_list as $list) {
				  $i++; ?>
				  <option
					  value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
					  <?php if (isset($class_shift_section_id)) {
						  if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
							  echo 'selected';
						  }
					  } ?>>
					  <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
				  </option>
				  <?php
			  }
		  }
		  ?>
	  </select>
  </div>

	<div class="form-group col-md-3">
		<label>Group</label>
		<select class="js-example-basic-single w-100" name="group" required="1">
			<option value="">-- Select --</option>
			<?php
			$i = 0;
			if (count($group)) {
				foreach ($group as $list) {
					$i++; ?>
					<option
						value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
					<?php
				}
			}
			?>
		</select>
	</div>

</div>

<div class="form-row">
     <div class="form-group col-md-3">
        <label>Type</label>
        <select  class="js-example-basic-single w-100" onchange="show_off_roll_section(this.value)" name="view_type" required="1">
           <option value="A">All Student</option>
           <option value="S">Selected Roll</option>
        </select>
      </div>

     <div class="form-group col-md-3">
        <div id="from_roll_section" style="display:none;">
           <label>From Roll</label>
           <input type="text" autocomplete="off"  class="form-control"  name="from_roll" id="from_roll"/>
           <span>Use only number. (Example: 1,2,3 etc.)</span>
        </div>
     </div>

     <div class="form-group col-md-3">
        <div id="to_roll_section" style="display:none;">
           <label>To Roll</label>
           <input type="text" autocomplete="off"  class="form-control"  name="to_roll" id="to_roll"/>
            <span>Use only number. (Example: 1,2,3 etc.)</span>
        </div>
     </div>
</div>
    <div class="btn-group float-right">
<!--        <input type="submit" name="pdf_download" class="btn btn-light" value="Download">-->
        <input type="submit" name="Preview" class="btn btn-primary" value="Preview & Download">
    </div>
</form>
