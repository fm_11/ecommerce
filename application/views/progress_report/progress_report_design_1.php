<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?php  echo $title; ?></title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/paper.css">
  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>
  @page { size: A4 landscape }
  .main_content {
    width: 100%;
	height: 100%;
    box-sizing: border-box;
	border: 5px #538cc6 solid;
  }

  .body_content{
    width: 100%;
	  height: 100%;
    box-sizing: border-box;
  	border: 5px #ff8080 solid;
  }

  #header{
	 width:100%;
	 box-sizing: border-box;
	 height: 160px;
	}
	.header-left {
		width:60%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
		line-height: 0.3;
	}
	.logo {
		width:20%;
		float:left;
		height: 100%;
		box-sizing: border-box;
        text-align: center;
	}
	.header-right {
		width:20%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
	}
	.clr{ clear:both;}


   .frame{
        width: 100%;
        height: 100%;
        margin: auto;
        position: relative;
    }
    .log_frame{
        width: 100%;
        height: 100%;
        position: relative;
    }
    .img{
        max-height: 100%;
        max-width: 100%;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
    }

	.gpa_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 90%;
	  height: auto;
	  position: relative;
		top: 24px;
	}

	table, td,th {
	  border: 1px solid black;
    font-size: 15px;
    padding:  0px 5px 0px 5px;
	}

  .other_table_td {
    font-size: 13px;
    padding:  0px 0px 0px 0px;
    text-align: center;
	}


	#student_info{
	 width:100%;
	 box-sizing: border-box;
	 height: 200px;
	}

	.student_info-left {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
	}

	.student_info_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 100%;
	  height: auto;
	  position: relative;
	}

	#student_exam_info{
	 width:98%;
	 box-sizing: border-box;
	 height: auto;
   margin: 0px auto;

	}

	.exam_info-left {
		width:100%;
		float:center;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
    margin: 0px auto;
	}

	.mark_info_table{
	  border-collapse: collapse;
	  margin: 0 auto;
	  margin-top:10px;
	  width: 100%;
	  height: auto;
	  position: relative;
	}

  .weekly_monthly_info_table{
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 100%;
	  height: auto;
	  position: relative;
	}

	.td_center{
	  text-align: center;
	}

	.td_backgroud{
	  background-color: #d9e6f2;
    font-weight: bold;
    font-size: 13px !important;
	}

  #other_info{
    width:98%;
   	box-sizing: border-box;
   	height: auto;

  }

  .other_info_body {
		width:100%;
		float:center;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
    margin: 0px auto;
	}

  .other_info_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 100%;
	  height: auto;
	  position: relative;
	}

  .signature_info{
	  width:100%;
	  box-sizing: border-box;
	  height: 100px;
	  bottom: 0px;
	  position: absolute;
  }
  .signature_left {
		width:33%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
    vertical-align: bottom;
	}
	.signature_middle {
		width:34%;
		float:left;
		height: 100%;
		box-sizing: border-box;
    text-align: center;
    vertical-align: bottom;
	}
	.signature_right {
		width:33%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
    vertical-align: bottom;
	}

  .gpa_td{
	  font-size: 12px;
  }


  </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

<?php
$other_table_data_store = array();
$other_table_data_count = 0;
foreach ($student_result_data as $resut_main_row) {
    ?>

  <section class="sheet padding-1mm">

    <!-- Write HTML just like a web page -->
    <div class="main_content">
	    <div class="body_content">
		   <div id="header">
			 <div class="logo">
					<div class="frame">
						<?php if($mark_sheet_config['show_student_image'] == '1'){ ?>
						 <img class="img" src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $resut_main_row['result_process_all_data'][0][0]['photo']; ?>" width="85px" height="100px" />
						<?php } ?>
					</div>
			 </div>
			 <div class="header-left">
			  <h3 style="margin-top: 10px;"><?php echo $header_info[0]['school_name']; ?></h3>
				<h5 style="margin-top:5px !important"><?php echo $header_info[0]['address']; ?></h5>

				<div class="log_frame">
				    <img class="img" style="margin-top:-10px !important" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $header_info[0]['picture']; ?>" width="70px" height="80px" />
				</div>
				 <div style="margin-top: -78px;font-weight: bold; font-size:18px;"><u><?php echo $mark_sheet_config['marksheet_header']; ?></u></div>
			 </div>
			 <div class="header-right">
				 <table class="gpa_table">
					 <tr>
						 <th class="gpa_td">Range</th>
						 <th class="gpa_td">Grade</th>
						 <th class="gpa_td">GPA</th>
					 </tr>
					 <tr>
						 <td class="gpa_td">80-100</td>
						 <td class="gpa_td">A+</td>
						 <td class="gpa_td">5.0</td>
					 </tr>
					 <tr>
						 <td class="gpa_td">70-79</td>
						 <td class="gpa_td">A</td>
						 <td class="gpa_td">4.0</td>
					 </tr>
					 <tr>
						 <td class="gpa_td">60-69</td>
						 <td class="gpa_td">A-</td>
						 <td class="gpa_td">3.5</td>
					 </tr>
					 <tr>
						 <td class="gpa_td">50–59</td>
						 <td class="gpa_td">B</td>
						 <td class="gpa_td">3.0</td>
					 </tr>
					 <tr>
						 <td class="gpa_td">40–49</td>
						 <td class="gpa_td">C</td>
						 <td class="gpa_td">2.0</td>
					 </tr>
					 <tr>
						 <td class="gpa_td">33–39</td>
						 <td class="gpa_td">D</td>
						 <td class="gpa_td">1.0</td>
					 </tr>
					 <tr>
						 <td class="gpa_td">0–32</td>
						 <td class="gpa_td">F</td>
						 <td class="gpa_td">0.0</td>
					 </tr>
				 </table>
			 </div>
			</div>

			<div class="student_info">
			   <div class="student_info-left">
			   <table class="student_info_table">

				  <tr>
					<td width="34%">Name of Student : <b><?php echo $resut_main_row['result_process_all_data'][0][0]['student_name']; ?></b></td>
					<td width="33%">Class : <b><?php echo $class_name; ?></b></td>
          <td width="33%">Section : <b><?php echo $section_name; ?></b></td>
				  </tr>
				  <tr>
					<td width="34%">Student ID : <b><?php echo $resut_main_row['result_process_all_data'][0][0]['student_code']; ?></b></td>
					<td width="33%">Group : <b><?php echo $group_name; ?></b></td>
                    <td width="33%">Shift : <b><?php echo $shift_name; ?></b></td>
				  </tr>
				  <tr>
  					<td width="34%">Roll No. : <b><?php echo $resut_main_row['result_process_all_data'][0][0]['roll_no']; ?></b></td>
  					<td width="33%">Exam Name :
            <b>
              <?php
              foreach ($resut_main_row['result_process_all_data'] as $semester_data) {
                  if (!empty($semester_data[0]['result'])) {
					  if($semester_data[0]['is_annual_exam'] == 1){
						  echo $semester_data['exam_name'];
						  break;
					  }
                  }
              } ?>
            </b>
          </td>
            <td width="33%">Year : <b><?php echo $year; ?></b></td>
				  </tr>
				  </table>
			   </div>
			</div>

			<div class="student_exam_info">
			   <div class="exam_info-left">
					<table class="mark_info_table">
					  <tr>
					   	<th class="td_center td_backgroud"><b>EXAM DETAILS</b></th>
					  </tr>
            <tr>
               <td style="vertical-align:top;">
                   <table style="width:100%;" class="weekly_monthly_info_table">
                      <tr>
                        <td rowspan="2" width="20%" class="td_center td_backgroud">Subject</td>
                        <?php
                        foreach ($exam_list as $exam) {
                            echo '<td colspan="3" class="td_center td_backgroud">' . $exam['name'] . '</td>';
                        } ?>
                      </tr>

                    <tr>
                      <?php
                      foreach ($exam_list as $exam) {
                          ?>
                              <td class="td_center td_backgroud">Full Marks</td>
                              <td class="td_center td_backgroud">Marks Obtained</td>
                              <td class="td_center td_backgroud">Highest Marks</td>

                            <?php
                      } ?>
                    </tr>


                      <?php
                      foreach ($subject_list_for_grand as $subject) {
                          echo '<tr><td>' . $subject['subject_name'] . '</td>';
                          foreach ($resut_main_row['result_process_all_data'] as $semester_data) {
							    if($semester_data['exam_name'] == 'Monthly-3'){
									//echo '<pre>';
                                   // print_r($semester_data);
								}
                               
								  if(!empty($semester_data[0]['result'])){								
									  if (isset($semester_data[0]['result'][$subject['subject_code']])) {
										  echo '<td class="td_center"><b>'. round($semester_data[0]['result'][$subject['subject_code']]['credit']) .'</b></td>';
										  echo '<td class="td_center">'. round($semester_data[0]['result'][$subject['subject_code']]['total_obtain'], 2) .'</td>';
										  if (isset($semester_data['highest_marks'][$subject['subject_code']])) {
											  echo '<td class="td_center">'. round($semester_data['highest_marks'][$subject['subject_code']], 2) .'</td>';
										  } else {
											  echo '<td class="">&nbsp;</td>';
										  }
									  } else {
										  echo '<td class="">&nbsp;</td><td class="">&nbsp;</td><td class="">&nbsp;</td>';
									  } 
								  }else{
									  echo '<td class="">&nbsp;</td><td class="">&nbsp;</td><td class="">&nbsp;</td>';
								  }
							  
							  ?>
                           <?php
                          }
                          echo '</tr>';
                      } ?>

                      <tr>
                        <td class="">Drawing</td>
                        <?php
                        foreach ($exam_list as $exam) {
                            ?>
                                <td class="">&nbsp;</td>
                                <td class="">&nbsp;</td>
                                <td class="">&nbsp;</td>

                              <?php
                        } ?>
                      </tr>

                    <?php
                      $other_table_data_store = array();
					  foreach ($resut_main_row['result_process_all_data'] as $semester_data) {
						if(!empty($semester_data[0]['result'])){	
							$other_table_data_store[$other_table_data_count]['exam_name'] = $semester_data['exam_name'];
							$other_table_data_store[$other_table_data_count]['total_obtain_mark'] = round($semester_data[0]['total_obtain_mark'], 4);
							$other_table_data_store[$other_table_data_count]['average_percentage_for_final'] = round($semester_data[0]['average_percentage_for_final'], 2);
							$other_table_data_store[$other_table_data_count]['average_mark_for_final'] = round($semester_data[0]['average_mark_for_final'], 4);
							$other_table_data_store[$other_table_data_count]['gpa_with_optional'] = round($semester_data[0]['gpa_with_optional'], 2);
							$other_table_data_store[$other_table_data_count]['c_alpha_gpa_with_optional'] = $semester_data[0]['c_alpha_gpa_with_optional'];

							if($mark_sheet_config['merit_position'] == 'SW'){
								$position = $semester_data[0]['shift_position'];
							}else if($mark_sheet_config['merit_position'] == 'SECW'){
								$position = $semester_data[0]['section_position'];
							}else if($mark_sheet_config['merit_position'] == 'GW'){
								$position = $semester_data[0]['group_position'];
							}else{
								$position = $semester_data[0]['class_position'];
							}

							$other_table_data_store[$other_table_data_count]['position'] = $position;

							$other_table_data_store[$other_table_data_count]['number_of_failed_subject'] = $semester_data[0]['number_of_failed_subject'];
							$other_table_data_store[$other_table_data_count]['total_students'] = 1;
						}else{
							$other_table_data_store[$other_table_data_count]['exam_name'] = $semester_data['exam_name'];
							$other_table_data_store[$other_table_data_count]['total_obtain_mark'] = '-';
							$other_table_data_store[$other_table_data_count]['average_percentage_for_final'] = '-';
							$other_table_data_store[$other_table_data_count]['average_mark_for_final'] = '-';
							$other_table_data_store[$other_table_data_count]['gpa_with_optional'] = '-';
							$other_table_data_store[$other_table_data_count]['c_alpha_gpa_with_optional'] = '-';
							$other_table_data_store[$other_table_data_count]['class_position'] = '-';
							$other_table_data_store[$other_table_data_count]['number_of_failed_subject'] =  '-';
							$other_table_data_store[$other_table_data_count]['total_students'] = 1;
						}
						$other_table_data_count++; 
					 }
	                ?>


                   </table>
               </td>
					  </tr>

					</table>
				</div>
			</div>


      <div class="other_info">
        <div class="other_info_body">
          <table class="other_info_table">
              <tr>
                <td class="other_table_td" style="width:11% !important;font-weight: 600;">Exams</td>
                <td class="other_table_td" style="font-weight: 600;">Total Marks</td>
                <td class="other_table_td" style="font-weight: 600;">Percentage</td>
                <td class="other_table_td" style="font-weight: 600;">Average Mark</td>
                <td class="other_table_td" style="font-weight: 600;">Letter Grade</td>
                <td class="other_table_td" style="font-weight: 600;">Grade Point</td>

                <td class="other_table_td" style="font-weight: 600;">Result Status</td>
                <td class="other_table_td" style="font-weight: 600;">Failed Subject</td>
                <td class="other_table_td" style="font-weight: 600;">Total Students</td>
                <td class="other_table_td" style="font-weight: 600;">Rank/Position</td>
                <td class="other_table_td" style="font-weight: 600;">Working Days</td>
                <td class="other_table_td" style="font-weight: 600;">Day Present</td>
                <!-- <td class="other_table_td">Total Absent</td> -->
                <td class="other_table_td" style="font-weight: 600;">Discipline</td>
                <td class="other_table_td" style="font-weight: 600;">Clean<br>liness</td>
                <td class="other_table_td" style="font-weight: 600;">Handwriting</td>
                <td class="other_table_td" style="width:10% !important;font-weight: 600;">Comments</td>
              </tr>
              <?php
    $comments_row = 0;
    $total_grand_total_mark = 0;
    $total_grand_average_percentage_for_final = 0;
    $total_grand_average_mark = 0 ;
    foreach ($other_table_data_store as $other_tb_data) {
        $result_status = "Passed";
        $total_grand_total_mark += $other_tb_data['total_obtain_mark'];
        $total_grand_average_percentage_for_final += $other_tb_data['average_percentage_for_final'];
        $total_grand_average_mark += $other_tb_data['average_mark_for_final'];
        if ($other_tb_data['c_alpha_gpa_with_optional'] == 'F') {
            $result_status = "Failed";
        }
		if ($other_tb_data['c_alpha_gpa_with_optional'] == '-') {
            $result_status = "Absent";
        }
        echo '<tr>';
        echo '<td class="td_left ">' . $other_tb_data['exam_name'] . '</td>';
        echo '<td class="other_table_td">' . $other_tb_data['total_obtain_mark'] . '</td>';
		if($other_tb_data['average_percentage_for_final'] == '-'){
			echo '<td class="other_table_td">-</td>';
		}else{
			echo '<td class="other_table_td">' . $other_tb_data['average_percentage_for_final'] . '%</td>';
		}
        echo '<td class="other_table_td">' . round($other_tb_data['average_mark_for_final'], 4) . '</td>';
        echo '<td class="other_table_td">' . $other_tb_data['c_alpha_gpa_with_optional'] . '</td>';
        echo '<td class="other_table_td">' . $other_tb_data['gpa_with_optional'] . '</td>';
        echo '<td class="other_table_td">' . $result_status . '</td>';
        echo '<td class="other_table_td">' . $other_tb_data['number_of_failed_subject'] . '</td>';
        echo '<td class="other_table_td">' . $other_tb_data['total_students'] . '</td>';
        echo '<td class="other_table_td">' . $other_tb_data['position'] . '</td>';
        echo '<td class="other_table_td">' . '' . '</td>';
        echo '<td class="other_table_td">' . '' . '</td>';
        echo '<td class="other_table_td">' . '' . '</td>';
        echo '<td class="other_table_td">' . '' . '</td>';
        echo '<td class="other_table_td">' . '' . '</td>';
        if ($comments_row == 0) {
            echo '<td rowspan="'. count($other_tb_data) . '" class="other_table_td"></td>';
        }
        echo '</tr>';
        $comments_row++;
    } ?>
          <tr>
            <td class="td_center td_backgroud">Grand Total</td>
            <?php
               echo '<td class="td_center td_backgroud">' . '-' . '</td>';
    echo '<td class="td_center td_backgroud">' . round($total_grand_average_percentage_for_final, 4) . '</td>';
    echo '<td class="td_center td_backgroud">' . round($total_grand_average_mark, 4) . '</td>';
    echo '<td class="td_center" colspan="11">' . '' . '</td>'; ?>
          </tr>
          </table>
        </div>
      </div>


      <div class="signature_info">
            <div class="signature_left">
                 <?php  if (isset($signature['L'])) {
        if ($signature['L']['is_use'] == '1') { ?>

                   <img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['L']['location']; ?>" height="60" width="75"/>
                    <br>................................<br>
                   <span><?php echo $signature['L']['level']; ?></span>

                 <?php }
    } ?>&nbsp;
				<br>
				<span style="font-size:12px;float: left;"><b>Powered By: School360 </b>&nbsp;</span>
            </div>
            <div class="signature_middle">
              <?php  if (isset($signature['M'])) {
        if ($signature['M']['is_use'] == '1') { ?>
                <img  style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['M']['location']; ?>"  height="60" width="75"/>
                 <br>................................<br>
                 <span><?php echo $signature['M']['level']; ?></span>
              <?php }
    } ?>
            &nbsp;
           </div>
           <div class="signature_right">
             <?php  if (isset($signature['R'])) {
        if ($signature['R']['is_use'] == '1') { ?>
               <img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['R']['location']; ?>" height="60" width="75"/>
                <br>................................<br>
                <span><?php echo $signature['R']['level']; ?></span>
             <?php }
    } ?>&nbsp;

          </div>


      </div>


	    </div>
	</div>

  </section>
<?php
} ?>

</body>

</html>
