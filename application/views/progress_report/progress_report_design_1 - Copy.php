<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?php  echo $title; ?></title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/paper.css">
  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>
  @page { size: A4 landscape }
  .main_content {
    width: 100%;
	height: 100%;
    box-sizing: border-box;
	border: 5px #538cc6 solid;
  }

  .body_content{
    width: 100%;
	height: 100%;
    box-sizing: border-box;
	border: 5px #ff8080 solid;
  }

    #header{
	 width:100%;
	 box-sizing: border-box;
	 height: 190px;
	}
	.header-left {
		width:60%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
		line-height: 0.3;
	}
	.logo {
		width:20%;
		float:left;
		height: 100%;
		box-sizing: border-box;
        text-align: center;
	}
	.header-right {
		width:20%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
	}
	.clr{ clear:both;}


   .frame{
        width: 100%;
        height: 100%;
        margin: auto;
        position: relative;
    }
    .log_frame{
        width: 100%;
        height: 120px;
        margin: auto;
        position: relative;
    }
    img{
        max-height: 100%;
        max-width: 100%;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
    }

	.gpa_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 90%;
	  height: auto;
	  position: relative;
	  top:10px;
	}

	table, td, th {
	  border: 1px solid black;
    font-size: 14px;
	}

	#student_info{
	 width:100%;
	 box-sizing: border-box;
	 height: 200px;
	}

	.student_info-left {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
	}

	.student_info_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 98%;
	  height: auto;
	  position: relative;
	}

	#student_exam_info{
	 width:100%;
	 box-sizing: border-box;
	 height: auto;

	}

	.exam_info-left {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
	}

	.mark_info_table{
	  border-collapse: collapse;
	  margin: 0 auto;
	  margin-top:10px;
	  width: 98%;
	  height: auto;
	  position: relative;
	}

  .weekly_monthly_info_table{
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 100%;
	  height: auto;
	  position: relative;
	}

	.td_center{
	  text-align: center;
	}

	.td_backgroud{
	  background-color: #d9e6f2;
    font-weight: bold;
	}

  #other_info{
    width:100%;
   	box-sizing: border-box;
   	height: auto;
  }

  .other_info_body {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
	}

  .other_info_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 98%;
	  height: auto;
	  position: relative;
	}

  #signature_info{
    width:100%;
   	box-sizing: border-box;
   	height: 100px;
  }


  .signature_left {
		width:33%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
    vertical-align: bottom;
	}
	.signature_middle {
		width:34%;
		float:left;
		height: 100%;
		box-sizing: border-box;
    text-align: center;
    vertical-align: bottom;
	}
	.signature_right {
		width:33%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
    vertical-align: bottom;
	}


  </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

<?php
$other_table_data_store = array();
$other_table_data_count = 0;
foreach ($student_result_data as $resut_main_row) {
    ?>

  <section class="sheet padding-5mm">

    <!-- Write HTML just like a web page -->
    <div class="main_content">
	    <div class="body_content">
		   <div id="header">
			 <div class="logo">
					<div class="frame">
						 <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $resut_main_row['result_process_weekly_monthly_data'][0][0]['photo']; ?>" width="120px" height="130px" />
					</div>
			 </div>
			 <div class="header-left">
			   <h3 style="margin-top: 20px;"><?php echo $header_info[0]['school_name']; ?></h3>
				<span><?php echo $header_info[0]['address']; ?></span>
				<div class="log_frame">
				    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/marksheet_logo.png" width="80px" height="90px" />
				</div>
				<span><u>Progress Report<u></span>
			 </div>
			 <div class="header-right">
			    <table class="gpa_table">
				  <tr>
					<th>Range</th>
					<th>Grade</th>
					<th>GPA</th>
				  </tr>
				  <tr>
					<td>80-100</td>
					<td>A+</td>
					<td>5.0</td>
				  </tr>
				  <tr>
					<td>70-79</td>
					<td>A</td>
					<td>4.0</td>
				  </tr>
				  <tr>
					<td>60-69</td>
					<td>A-</td>
					<td>3.5</td>
				  </tr>
				  <tr>
					<td>50–59</td>
					<td>B</td>
					<td>3.0</td>
				  </tr>
				   <tr>
					<td>40–49</td>
					<td>C</td>
					<td>2.0</td>
				  </tr>
				   <tr>
					<td>33–39</td>
					<td>D</td>
					<td>1.0</td>
				  </tr>
				   <tr>
					<td>0–32</td>
					<td>F</td>
					<td>0.0</td>
				  </tr>
				</table>
			 </div>

			</div>

			<div class="student_info">
			   <div class="student_info-left">
			   <table class="student_info_table">
				  <tr>
					<td width="34%">Name of Student : <b><?php echo $resut_main_row['result_process_weekly_monthly_data'][0][0]['student_name']; ?></b></td>
					<td width="33%">Class : <b><?php echo $class_name; ?></b></td>
                    <td width="33%">Section : <b><?php echo $section_name; ?></b></td>
				  </tr>
				  <tr>
					<td width="34%">Student ID : <b><?php echo $resut_main_row['result_process_weekly_monthly_data'][0][0]['student_code']; ?></b></td>
					<td width="33%">Group : <b><?php echo $group_name; ?></b></td>
                    <td width="33%">Shift : <b><?php echo $shift_name; ?></b></td>
				  </tr>
				  <tr>
  					<td width="34%">Roll No. : <b><?php echo $resut_main_row['result_process_weekly_monthly_data'][0][0]['roll_no']; ?></b></td>
  					<td width="33%">Exam Name :
            <b>
              <?php
              foreach ($resut_main_row['result_process_semester_data'] as $semester_data) {
                  if ($semester_data[0]['is_annual_exam'] == 1) {
                      echo $semester_data['exam_name'];
                      break;
                  }
              } ?>
            </b>
          </td>
            <td width="33%">Year : <b><?php echo $year; ?></b></td>
				  </tr>
				  </table>
			   </div>
			</div>

			<div class="student_exam_info">
			   <div class="exam_info-left">
					<table class="mark_info_table">
					  <tr>
						<th class="td_center td_backgroud" colspan="2"><b>EXAM DETAILS</b></th>
					  </tr>
            <tr>
  					   <td style="width:30%;  vertical-align:top;" class="td_center">
                  <?php
                  $i = 0;

    foreach ($resut_main_row['result_process_weekly_monthly_data'] as $row) {
        $other_table_data_store[$other_table_data_count]['exam_name'] = $row['exam_name'];
        $other_table_data_store[$other_table_data_count]['total_obtain_mark'] = round($row[0]['total_obtain_mark'], 4);
        $other_table_data_store[$other_table_data_count]['average_percentage_for_final'] = round($row[0]['average_percentage_for_final'], 2);
        $other_table_data_store[$other_table_data_count]['average_mark'] = round($row[0]['average_mark'], 4);
        $other_table_data_store[$other_table_data_count]['gpa_with_optional'] = round($row[0]['gpa_with_optional'], 2);
        $other_table_data_store[$other_table_data_count]['c_alpha_gpa_with_optional'] = $row[0]['c_alpha_gpa_with_optional'];
        $other_table_data_store[$other_table_data_count]['class_position'] = $row[0]['position'];
        $other_table_data_store[$other_table_data_count]['number_of_failed_subject'] = $row[0]['number_of_failed_subject'];
        $other_table_data_store[$other_table_data_count]['total_students'] = 1;
        $other_table_data_count++; ?>
                    <table class="weekly_monthly_info_table">
                         <tr>
                            <th colspan="4"><?php echo $row['exam_name']; ?></th>
                         </tr>
                         <tr>
                           <th  style="width:40%;">Subject Name</th>
                           <th>Full Marks</th>
                           <th>Marks Obtained</th>
                           <th>Highest Marks</th>
                         </tr>
                         <?php
                         foreach ($row[$i]['result'] as $result) {
                             // echo '<pre>';
                             // print_r($row);
                             // die;?>
                           <tr>
                             <td><?php echo $result['subject_name']; ?></td>
                             <td><?php echo round($result['credit']); ?></td>
                             <td class="td_backgroud"><?php echo round($result['total_obtain'], 2); ?></td>
                             <td class="td_backgroud">
                               <?php
                               if (isset($row['highest_marks'][$result['subject_code']])) {
                                   echo round($row['highest_marks'][$result['subject_code']], 2);
                               } ?>
                             </td>
                           </tr>
                           <?php
                         } ?>
                         <tr>
                           <td colspan="3" style="text-align:right;"><b>Total </b></td>
                           <td class="td_backgroud"><b><?php echo round($row[0]['total_obtain_mark'], 4); ?></b></td>
                         </tr>
                         <tr>
                           <td colspan="4">
                             <b>
                             Average Mark (<?php echo $row[0]['average_percentage_for_final'] ?>%): <?php echo round($row[0]['average_mark'], 4); ?>
                             </b>
                           </td>
                         </tr>
                    </table>
                    <?php
    } ?>
  						 </td>
               <td style="width:70%; vertical-align:top;">
                   <table style="width:100%;" class="weekly_monthly_info_table">
                      <tr>
                        <td rowspan="2" class="td_center td_backgroud">Subject</td>
                        <td rowspan="2" class="td_center td_backgroud">Full Marks</td>
                        <?php
                        foreach ($exam_list as $exam) {
                            if ($exam['type'] == "S") {
                                echo '<td colspan="2" class="td_center td_backgroud">' . $exam['name'] . '</td>';
                            }
                        } ?>
                      </tr>

                    <tr>
                      <?php
                      foreach ($exam_list as $exam) {
                          if ($exam['type'] == "S") {
                              ?>

                              <!-- <td class="td_center td_backgroud">WR</td>
                              <td class="td_center td_backgroud">MCQ</td>
                              <td class="td_center td_backgroud">PRC</td> -->
                              <td class="td_center td_backgroud">Marks Obtained</td>
                              <td class="td_center td_backgroud">Highest Marks</td>

                            <?php
                          }
                      } ?>
                    </tr>





                      <?php
                      foreach ($subject_list as $subject) {
                          echo '<tr><td>' . $subject['subject_name'] . '</td>';
                          echo '<td class="td_center">' . round($subject['credit']) . '</td>';
                          foreach ($resut_main_row['result_process_semester_data'] as $semester_data) {

                              // echo '<pre>';
                              // print_r($semester_data);
                              // die;?>
                             <!-- <td  class="td_center"><?php echo round($semester_data[0]['result'][$subject['subject_code']]['written'], 2); ?></td>
                             <td class="td_center"><?php echo round($semester_data[0]['result'][$subject['subject_code']]['objective'], 2); ?></td>
                             <td class="td_center"><?php echo round($semester_data[0]['result'][$subject['subject_code']]['practical'], 2); ?></td> -->
                             <td class="td_center"><?php echo round($semester_data[0]['result'][$subject['subject_code']]['total_obtain'], 2); ?></td>
                             <td class="td_center"><?php
                             if (isset($semester_data['highest_marks'][$subject['subject_code']])) {
                                 echo round($semester_data['highest_marks'][$subject['subject_code']], 2);
                             } ?>
                             </td>
                           <?php
                          }
                          echo '</tr>';
                      } ?>


                      <tr>
                        <td class="">Drawing</td>
                        <td class="">&nbsp;</td>
                        <?php
                        foreach ($exam_list as $exam) {
                            if ($exam['type'] == "S") {
                                ?>
                                <td class="">&nbsp;</td>
                                <td class="">&nbsp;</td>

                              <?php
                            }
                        } ?>
                      </tr>



                      <tr>
                         <td colspan="2"></td>
                     <?php
                     foreach ($resut_main_row['result_process_semester_data'] as $semester_data) {
                         ?>

                        <td align="center" colspan="2"><b>Total Mark : <?php echo round($semester_data[0]['total_obtain_mark'], 4) ?></b></td>

                    <?php
                     } ?>
                     </tr>


                    <?php
                    foreach ($resut_main_row['result_process_semester_data'] as $semester_data) {
                        // echo '<pre>';
                        // print_r($semester_data);
                        // die;
                        $other_table_data_store[$other_table_data_count]['exam_name'] = $semester_data['exam_name'];
                        $other_table_data_store[$other_table_data_count]['total_obtain_mark'] = round($semester_data[0]['total_obtain_mark'], 4);
                        $other_table_data_store[$other_table_data_count]['average_percentage_for_final'] = round($semester_data[0]['average_percentage_for_final'], 2);
                        $other_table_data_store[$other_table_data_count]['average_mark'] = round($semester_data[0]['average_mark'], 4);
                        $other_table_data_store[$other_table_data_count]['gpa_with_optional'] = round($semester_data[0]['gpa_with_optional'], 2);
                        $other_table_data_store[$other_table_data_count]['c_alpha_gpa_with_optional'] = $semester_data[0]['c_alpha_gpa_with_optional'];
                        $other_table_data_store[$other_table_data_count]['class_position'] = $semester_data[0]['position'];
                        $other_table_data_store[$other_table_data_count]['number_of_failed_subject'] = $semester_data[0]['number_of_failed_subject'];
                        $other_table_data_store[$other_table_data_count]['total_students'] = 1;
                        $other_table_data_count++;
                    } ?>


                     <tr>
                        <td colspan="2"></td>
                    <?php
                    foreach ($resut_main_row['result_process_semester_data'] as $semester_data) {
                        ?>

                       <td align="center" colspan="2"><b>Average Mark (<?php echo $semester_data[0]['average_percentage_for_final']; ?>%) : <?php echo round($semester_data[0]['average_mark'], 4) ?></b></td>

                   <?php
                    } ?>
                    </tr>

                    <!-- <tr>
                   <?php
                   foreach ($resut_main_row['result_process_semester_data'] as $semester_data) {
                       if ($semester_data[0]['is_annual_exam'] == 1) {
                           ?>
                      <td align="right" colspan="<?php echo(count($resut_main_row['result_process_semester_data']) * 4) + 2; ?>"><b>Total Average Mark : <?php echo round($semester_data[0]['grand_average_mark'], 3) ?></b></td>
                  <?php
                      break;
                       }
                   } ?>
                   </tr>

                   <tr>
                  <?php
                  foreach ($resut_main_row['result_process_semester_data'] as $semester_data) {
                      if ($semester_data[0]['is_annual_exam'] == 1) {
                          ?>
                     <td align="right" colspan="<?php echo(count($resut_main_row['result_process_semester_data']) * 4) + 2; ?>"><b>Annual Position : <?php echo $semester_data[0]['annual_position']; ?></b></td>
                 <?php
                     break;
                      }
                  } ?>
                  </tr> -->

                   </table>
               </td>
					  </tr>

					</table>
				</div>
			</div>


      <div class="other_info">
        <div class="other_info_body">
          <table class="other_info_table">
              <tr>
                <td class="td_center td_backgroud" width="10%">Exam Name</td>
                <td class="td_center td_backgroud">Total Marks</td>
                <td class="td_center td_backgroud">Percentage</td>
                <td class="td_center td_backgroud">Average Mark</td>
                <td class="td_center td_backgroud">Letter Grade</td>
                <td class="td_center td_backgroud">Grade Point</td>
                <td class="td_center td_backgroud">Class Position</td>
                <td class="td_center td_backgroud">Result Status</td>
                <td class="td_center td_backgroud">Failed Subject</td>
                <td class="td_center td_backgroud">Total Students</td>
                <td class="td_center td_backgroud">Working Days</td>
                <td class="td_center td_backgroud">Total Present</td>
                <td class="td_center td_backgroud">Total Absent</td>
                <td class="td_center td_backgroud">Dsciplinary</td>
                <td class="td_center td_backgroud">Cleanliness</td>
                <td class="td_center td_backgroud">Hand Writing</td>
                <td class="td_center td_backgroud" width="15%">Comments</td>
              </tr>
              <?php
    $comments_row = 0;
    $total_grand_total_mark = 0;
    $total_grand_average_percentage_for_final = 0;
    $total_grand_average_mark = 0 ;
    foreach ($other_table_data_store as $other_tb_data) {
        $result_status = "Passed";
        $total_grand_total_mark += $other_tb_data['total_obtain_mark'];
        $total_grand_average_percentage_for_final += $other_tb_data['average_percentage_for_final'];
        $total_grand_average_mark += $other_tb_data['average_mark'];
        if ($other_tb_data['c_alpha_gpa_with_optional'] == 'F') {
            $result_status = "Failed";
        }
        echo '<tr>';
        echo '<td class="td_left">' . $other_tb_data['exam_name'] . '</td>';
        echo '<td class="td_center">' . $other_tb_data['total_obtain_mark'] . '</td>';
        echo '<td class="td_center">' . $other_tb_data['average_percentage_for_final'] . '%</td>';
        echo '<td class="td_center">' . round($other_tb_data['average_mark'], 4) . '</td>';
        echo '<td class="td_center">' . $other_tb_data['c_alpha_gpa_with_optional'] . '</td>';
        echo '<td class="td_center">' . $other_tb_data['gpa_with_optional'] . '</td>';
        echo '<td class="td_center">' . $other_tb_data['class_position'] . '</td>';
        echo '<td class="td_center">' . $result_status . '</td>';
        echo '<td class="td_center">' . $other_tb_data['number_of_failed_subject'] . '</td>';
        echo '<td class="td_center">' . $other_tb_data['total_students'] . '</td>';
        echo '<td class="td_center">' . '' . '</td>';
        echo '<td class="td_center">' . '' . '</td>';
        echo '<td class="td_center">' . '' . '</td>';
        echo '<td class="td_center">' . '' . '</td>';
        echo '<td class="td_center">' . '' . '</td>';
        echo '<td class="td_center">' . '' . '</td>';
        if ($comments_row == 0) {
            echo '<td rowspan="'. count($other_tb_data) . '" class="td_center"></td>';
        }
        echo '</tr>';
        $comments_row++;
    } ?>
          <tr>
            <td class="td_center td_backgroud">Grand Final</td>
            <?php
               echo '<td class="td_center td_backgroud">' . round($total_grand_total_mark, 4) . '</td>';
    echo '<td class="td_center td_backgroud">' . round($total_grand_average_percentage_for_final, 4) . '</td>';
    echo '<td class="td_center td_backgroud">' . round($total_grand_average_mark, 4) . '</td>';
    echo '<td class="td_center" colspan="12">' . '' . '</td>'; ?>
          </tr>
          </table>
        </div>
      </div>


      <div class="signature_info">
            <div class="signature_left">
                 <?php  if (isset($signature['L'])) {
        if ($signature['L']['is_use'] == '1') { ?>

                   <img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['L']['location']; ?>" height="60" width="75"/>
                    <br>................................<br>
                   <span><?php echo $signature['L']['level']; ?></span>

                 <?php }
    } ?>&nbsp;
            </div>
            <div class="signature_middle">
              <?php  if (isset($signature['M'])) {
        if ($signature['M']['is_use'] == '1') { ?>
                <img  style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['M']['location']; ?>"  height="60" width="75"/>
                 <br>................................<br>
                 <span><?php echo $signature['M']['level']; ?></span>
              <?php }
    } ?>
            &nbsp;
           </div>
           <div class="signature_right">
             <?php  if (isset($signature['R'])) {
        if ($signature['R']['is_use'] == '1') { ?>
               <img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['R']['location']; ?>" height="60" width="75"/>
                <br>................................<br>
                <span><?php echo $signature['R']['level']; ?></span>
             <?php }
    } ?>&nbsp;
          </div>

      </div>
      <span style="font-size:12px; float:right;"><b>Powered By: School360 </b>&nbsp;</span>

	    </div>
	</div>

  </section>
<?php
} ?>

</body>

</html>
