<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?php  echo $title; ?></title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>
  @page { size: A4 }
  .main_content {
    width: 100%;
	height: 100%;
    box-sizing: border-box;
	border: 5px #538cc6 solid;
  }

  .body_content{
    width: 100%;
	height: 100%;
    box-sizing: border-box;
	border: 5px #ff8080 solid;
  }

    #header{
	 width:100%;
	 box-sizing: border-box;
	 height: 175px;
	}
	.header-left {
		width:55%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
		line-height: 0.3;
	}
	.logo {
		width:20%;
		float:left;
		height: 100%;
		box-sizing: border-box;
        text-align: center;
	}
	.header-right {
		width:25%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
	}
	.clr{ clear:both;}


   .frame{
        width: 100%;
        height: 100%;
        margin: auto;
        position: relative;
    }
    .log_frame{
        width: 100%;
        height: 100px;
        margin: auto;
        position: relative;
    }
    .img{
        max-height: 100%;
        max-width: 100%;
        position: absolute;
        top: 10px;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
    }

	.gpa_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 90%;
	  height: auto;
	  position: relative;
	  top:20px;
	}

	table, td, th {
	  border: 1px solid black;
    font-size: 13px;
	}

	.student_info{
	 width:100%;
	 box-sizing: border-box;
	 height: 120px;
	}

  .summary_info{
   width:100%;
   box-sizing: border-box;
   height: auto;
  }

  .summery_info-left {
		width:25%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
    margin-top: 13px;
    margin-left: 5px;
    padding: 0px;
	}

  .summery_info-middle {
		width:50%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
    margin-top: 13px;
    margin-left: 5px;
    padding: 0px;
	}

  .summery_info-right {
		width:25%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
    margin-top: 13px;s
    margin-left: 5px;
    padding: 0px;
	}

  .summery_info-right1{
    width:20%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
    margin-top: 30px;
    margin-left: 20px;
    padding: 0px;
  }

	.student_info-left {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
	}

	.student_info_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 98%;
	  height: auto;
	  position: relative;
	}

	.student_exam_info{
	 width:100%;
	 box-sizing: border-box;
	 height: auto;

	}

	.exam_info-left {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
	}

	.mark_info_table{
	  border-collapse: collapse;
	  margin: 0 auto;
	  margin-top:10px;
	  width: 98%;
	  height: auto;
	  position: relative;
	}

  .subject_info_table{
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 100%;
	  height: auto;
	  position: relative;
	}

	.td_center{
	  text-align: center;
    padding: 1px;
    padding-top: 3px;
    padding-bottom: 3px;
	}

  .td_right{
	  text-align: right;
    padding: 1px;
    padding-top: 3px;
    padding-bottom: 3px;
	}
  .td_left{
    text-align: left;
    padding: 1px;
    padding-top: 3px;
    padding-bottom: 3px;
  }

	.td_backgroud{
	  background-color: #d9e6f2;
    font-weight: bold;
	}

  .signature_info{
	  width:100%;
	  box-sizing: border-box;
	  height: 100px;
	  bottom: 30px;
	  position: absolute;
  }
  .signature_left {
	  width:33%;
	  float:left;
	  height: 100%;
	  box-sizing: border-box;
	  text-align: center;
	  vertical-align: bottom;
  }
  .signature_middle {
	  width:34%;
	  float:left;
	  height: 100%;
	  box-sizing: border-box;
	  text-align: center;
	  vertical-align: bottom;
  }
  .signature_right {
	  width:33%;
	  float:left;
	  height: 100%;
	  box-sizing: border-box;
	  text-align: center;
	  vertical-align: bottom;
  }


  </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->

<?php
foreach ($student_result_data as $resut_main_row) {
    ?>

  <section class="sheet padding-5mm">

    <!-- Write HTML just like a web page -->
    <div class="main_content">
	    <div class="body_content">
		   <div id="header">
			 <div class="logo">
					<div class="frame">
						 <img class="img" src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $resut_main_row['result_process_data'][0][0]['photo']; ?>" width="120px" height="130px" />
					</div>
			 </div>
			 <div class="header-left">
			   <h3 style="margin-top: 20px;"><?php echo $header_info[0]['school_name']; ?></h3>
				<span><?php echo $header_info[0]['address']; ?></span>
				<div class="log_frame">
				    <img class="img" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $header_info[0]['picture']; ?>" width="80px" height="90px" />
				</div>
				 <span style="letter-spacing: 3px;line-height: 20px;
                          border-bottom: 2px solid #009933;">
							Progress Report
				 </span>
			 </div>
			 <div class="header-right">
			    <table class="gpa_table">
				  <tr>
					<th>Range</th>
					<th>Grade</th>
					<th>GPA</th>
				  </tr>
				  <tr>
					<td>80-100</td>
					<td>A+</td>
					<td>5.0</td>
				  </tr>
				  <tr>
					<td>70-79</td>
					<td>A</td>
					<td>4.0</td>
				  </tr>
				  <tr>
					<td>60-69</td>
					<td>A-</td>
					<td>3.5</td>
				  </tr>
				  <tr>
					<td>50–59</td>
					<td>B</td>
					<td>3.0</td>
				  </tr>
				   <tr>
					<td>40–49</td>
					<td>C</td>
					<td>2.0</td>
				  </tr>
				   <tr>
					<td>33–39</td>
					<td>D</td>
					<td>1.0</td>
				  </tr>
				   <tr>
					<td>0–32</td>
					<td>F</td>
					<td>0.0</td>
				  </tr>
				</table>
			 </div>

			</div>

			<div class="student_info">
			   <div class="student_info-left">
				   <table class="student_info_table" style="border-style : hidden!important;">
					   <tr>
						   <td style="border: none;" class="td_left" width="15%">Name of Student</td>
						   <td style="border: none;" class="td_center" width="5%">:</b></td>
						   <td style="border: none;" class="td_left" colspan="4"  width="80%"><b><?php echo $resut_main_row['result_process_data'][0][0]['student_name']; ?></b></td>
					   </tr>
					   <tr>
						   <td style="border: none;" class="td_left" width="15%">Father's Name</td>
						   <td style="border: none;" class="td_center" width="5%">:</b></td>
						   <td style="border: none;" class="td_left" colspan="4"  width="80%"><b><?php echo $resut_main_row['result_process_data'][0][0]['father_name']; ?></b></td>
					   </tr>
					   <tr>
						   <td style="border: none;" class="td_left" width="15%">Mother's Name</td>
						   <td style="border: none;" class="td_center" width="5%">:</b></td>
						   <td style="border: none;" class="td_left" colspan="4" width="80%"><b><?php echo $resut_main_row['result_process_data'][0][0]['mother_name']; ?></b></td>
					   </tr>
					   <tr>
						   <td style="border: none;" class="td_left" width="15%">Student ID</td>
						   <td style="border: none;" class="td_center" width="5%">:</b></td>
						   <td style="border: none;" class="td_left" width="30%"><b>&nbsp;<?php echo $resut_main_row['result_process_data'][0][0]['student_code']; ?></b></td>

						   <td style="border: none;" class="td_left" width="15%">Exam</td>
						   <td style="border: none;" class="td_center" width="5%">:</b></td>
						   <td style="border: none;" class="td_left" width="30%">
							   <b>
								   <?php
								   foreach ($resut_main_row['result_process_data'] as $semester_data) {
									   if ($semester_data['is_combined_result'] == 1) {
										   echo $semester_data['exam_name'];
										   break;
									   }
								   } ?>
							   </b>
						   </td>
					   </tr>

					   <tr>
						   <td style="border: none;" class="td_left" width="15%">Roll No.</td>
						   <td style="border: none;" class="td_center" width="5%">:</b></td>
						   <td style="border: none;" class="td_left" width="30%"><b><?php echo $resut_main_row['result_process_data'][0][0]['roll_no']; ?></b></td>

						   <td style="border: none;" class="td_left" width="15%">Year</td>
						   <td style="border: none;" class="td_center" width="5%">:</b></td>
						   <td style="border: none;" class="td_left" width="30%"><b><?php echo $year; ?></b></td>
					   </tr>

					   <tr>
						   <td style="border: none;" class="td_left" width="15%">Class</td>
						   <td style="border: none;" class="td_center" width="5%">:</b></td>
						   <td style="border: none;" class="td_left" width="30%"><b>&nbsp;<?php echo $class_name . '-' . $section_name . '-' . $shift_name; ?></b></td>

						   <td style="border: none;" class="td_left" width="15%">Group</td>
						   <td style="border: none;" class="td_center" width="5%">:</b></td>
						   <td style="border: none;" class="td_left" width="30%"><b><?php echo $group_name; ?></b></td>
					   </tr>
				   </table>
			   </div>
			</div>

			<div class="student_exam_info">
			   <div class="exam_info-left">
					<table class="mark_info_table">
					  <tr>
						<th class="td_center td_backgroud" colspan="2"><b>EXAM DETAILS</b></th>
					  </tr>
            <tr>

               <td style="width:100%; vertical-align:top;" class="td_center">
                   <table class="subject_info_table">
                      <tr>
                        <th style="min-width:100px;" rowspan="2" class="td_center td_backgroud">Subeject Name</td>
                     <?php
                     foreach ($exam_list as $exam) {
                         ?>
                           <th class="td_center td_backgroud" colspan="5"><b><?php echo  $exam['name']; ?></b></th>
                      <?php
                     } ?>
                      </tr>

                      <tr>
                         <?php
                         foreach ($exam_list as $exam) {
                             ?>

                                 <th class="td_center td_backgroud">Full Marks</th>
                                 <th class="td_center td_backgroud">Highest Marks</th>
                                 <th class="td_center td_backgroud">Total Marks</th>
                                 <th class="td_center td_backgroud">Letter Grade</th>
                                 <th class="td_center td_backgroud">Grade Point</th>


                          <?php
                         } ?>
                       </tr>


                           <?php

    foreach ($subject_list as $subject) {
        ?>
                               <tr>
                                 <td class="td_left td_backgroud">
                                   <?php echo $subject['subject_name']; ?>
                                 </td>
                             <?php
                               foreach ($resut_main_row['result_process_data'] as $result_data) {
                                   $total_mark_percentage = 0;
                                   $total_percentage = 0;
                                   // echo '<pre>';
                                   // print_r($result_data['highest_marks']);
                                   // die;?>
                                   <td style="font-weight: 600;">
                                     <?php
                                     if (isset($result_data[0]['result'][$subject['subject_code']]['credit'])) {
                                         echo round($result_data[0]['result'][$subject['subject_code']]['credit']);
                                     } ?>
                                   </td>

                                   <td>
                                     <?php
                                     if (isset($result_data['highest_marks'][$subject['subject_code']])) {
                                         echo round($result_data['highest_marks'][$subject['subject_code']]);
                                     } ?>
                                    </td>

                                    <td style="font-weight: 600;">
                                      <?php
                                      if (isset($result_data[0]['result'][$subject['subject_code']]['total_obtain'])) {
                                          echo round($result_data[0]['result'][$subject['subject_code']]['total_obtain'], 2);
                                      } ?>
                                   </td>

                                   <td>
                                     <?php
                                     if (isset($result_data[0]['result'][$subject['subject_code']]['alpha_gpa'])) {
                                         echo $result_data[0]['result'][$subject['subject_code']]['alpha_gpa'];
                                     } ?>
                                  </td>

                                  <td>
                                    <?php
                                    if (isset($result_data[0]['result'][$subject['subject_code']]['numeric_gpa'])) {
                                        echo round($result_data[0]['result'][$subject['subject_code']]['numeric_gpa'], 2);
                                    } ?>
                                 </td>



                               <?php
                               } ?>
                                 </tr>
                               <?php
    } ?>

                        <tr>
                          <td></td>
                          <?php
                           foreach ($exam_list as $exam) {
                               foreach ($resut_main_row['combined_result_details'] as $combined_result) {
                                   if ($exam['id'] == $combined_result['combined_exam_id']) {
                                       echo '<td class="td_center td_backgroud" colspan="5">Total Marks = ' . round($combined_result['total_obtained_mark'], 2) . '</td>';
                                   }
                               }
                           } ?>
                         </tr>

                         <tr>
                          <td></td>
                           <?php
                            foreach ($exam_list as $exam) {
                                foreach ($resut_main_row['combined_result_details'] as $combined_result) {
                                    if ($exam['id'] == $combined_result['combined_exam_id']) {
                                        $total_mark_percentage += $combined_result['calculable_mark_from_total'];
                                        $total_percentage += $combined_result['percentage_from_total'];
                                        echo '<td class="td_center td_backgroud" colspan="5">Percentage ('.$combined_result['percentage_from_total'] . '%)' . ' = ' . round($combined_result['calculable_mark_from_total'], 4) . '</td>';
                                    }
                                }
                            } ?>
                          </tr>


                          <tr>
                           <td></td>
                            <td class="td_center td_backgroud" colspan="<?php echo 5 * count($exam_list); ?>">Total Percentage (<?php echo $total_percentage ?>%) = <?php echo round($total_mark_percentage, 4); ?> </td>
                           </tr>




                   </table>
  						 </td>
					  </tr>

					</table>
				</div>
			</div>


			  <div class="summary_info">
				<div class="summery_info-left">
				   <table class="student_info_table">
					 <tr>
					   <th class="td_left" style="padding-left:5px;">
						 Result Status
					   </th>
					   <th class="td_center" style="color:green;">
						 <?php
						 foreach ($resut_main_row['result_process_data'] as $semester_data) {
							 if ($semester_data['is_combined_result'] == 1) {
								 if ($semester_data[0]['c_alpha_gpa_with_optional'] != 'F') {
									 echo 'Passed';
								 } else {
									 echo 'Failed';
								 }
								 break;
							 }
						 } ?>
					   </th>
					 </tr>

					 <tr>
					   <th  class="td_left" style="padding-left:5px;">
						 Class Position
					   </th>
					   <th  class="td_center">
						 <?php
						 foreach ($resut_main_row['result_process_data'] as $semester_data) {
							 if ($semester_data['is_combined_result'] == 1) {
								 if(isset($general_config->what_position_to_set_in_marksheet)){
									if($general_config->what_position_to_set_in_marksheet == '0'){
										echo $semester_data[0]['class_position'];
									}else if($general_config->what_position_to_set_in_marksheet == '1'){
										echo $semester_data[0]['shift_position'];
									}else if($general_config->what_position_to_set_in_marksheet == '2'){
										echo $semester_data[0]['section_position'];
									}else if($general_config->what_position_to_set_in_marksheet == '3'){
										echo $semester_data[0]['group_position'];
									}
								 }else{
									 echo $semester_data[0]['class_position'];
								 }
								 break;
							 }
						 } ?>
					   </th>
					 </tr>

					 <tr>
					   <th class="td_left" style="padding-left:5px;">
						 GPA
					   </th>
					   <th  class="td_center">
						 <?php
						 foreach ($resut_main_row['result_process_data'] as $semester_data) {
							 if ($semester_data['is_combined_result'] == 1) {
								 echo round($semester_data[0]['gpa_with_optional']);
								 break;
							 }
						 } ?>
					   </th>
					 </tr>

					 <tr>
					   <th class="td_left" style="padding-left:5px;">
						 Letter Grade
					   </th>
					   <th  class="td_center">
						 <?php
						 foreach ($resut_main_row['result_process_data'] as $semester_data) {
							 if ($semester_data['is_combined_result'] == 1) {
								 echo $semester_data[0]['c_alpha_gpa_with_optional'];
								 break;
							 }
						 } ?>
					   </th>
					 </tr>

					 <tr>
					   <th  class="td_left" style="padding-left:5px;">
						 Failed Subject (S)
					   </th>
					   <th  class="td_center">
						 <?php
						 foreach ($resut_main_row['result_process_data'] as $semester_data) {
							 if ($semester_data['is_combined_result'] == 1) {
								 echo $semester_data[0]['number_of_failed_subject'];
								 break;
							 }
						 } ?>
					   </th>
					 </tr>

					 <tr>
					   <th  class="td_left" style="padding-left:5px;">
						 Working Days
					   </th>
					   <th class="td_left" style="padding-left:5px;">

					   </th>
					 </tr>

					 <tr>
					   <th  class="td_left" style="padding-left:5px;">
						 Total Present
					   </th>
					   <th class="td_left" style="padding-left:5px;">

					   </th>
					 </tr>

					 <tr>
					   <th  class="td_left" style="padding-left:5px;">
						 Total Absent
					   </th>
					   <th class="td_center">

					   </th>
					 </tr>

				   </table>
				</div>


				<div class="summery_info-middle">
				   <table class="student_info_table">
					 <tr>
					   <td style="width:50%">
						 <table class="student_info_table">
						   <tr>
							 <th class="td_center" colspan="2">
							   <b>Moral & Behavior Evaluation</b>
							 </th>
						   </tr>
						   <tr>
							 <th style="width:20% !important" class="td_center">
							   &nbsp;
							 </th>
							 <th>
							   Excelent
							 </th>
						   </tr>

						   <tr>
							 <th style="width:20% !important" class="td_center">
							   &nbsp;
							 </th>
							 <th>
							   Good
							 </th>
						   </tr>

						   <tr>
							 <th style="width:20% !important" class="td_center">
							   &nbsp;
							 </th>
							 <th>
							   Average
							 </th>
						   </tr>

						   <tr>
							 <th style="width:20% !important" class="td_center">
							   &nbsp;
							 </th>
							 <th>
							   Poor
							 </th>
						   </tr>
						 </table>
					   </td>
					   <td style="width:50%">
						 <table class="student_info_table">
						   <tr>
							 <th class="td_center" colspan="2">
							   <b>Co-Curricular Activities</b>
							 </th>
						   </tr>
						   <tr>
							 <th style="width:20% !important" class="td_center">
							   &nbsp;
							 </th>
							 <th>
							   Sports
							 </th>
						   </tr>

						   <tr>
							 <th style="width:20% !important" class="td_center">
							   &nbsp;
							 </th>
							 <th>
							   Cultural Function
							 </th>
						   </tr>

						   <tr>
							 <th style="width:20% !important" class="td_center">
							   &nbsp;
							 </th>
							 <th>
							   Scout/BNCC
							 </th>
						   </tr>

						   <tr>
							 <th style="width:20% !important" class="td_center">
							   &nbsp;
							 </th>
							 <th>
							   Math Olympiad
							 </th>
						   </tr>
						 </table>
					   </td>
					 </tr>
					 <tr>
					   <td colspan="2" style="height:60px; vertical-align:top;">
						 <b><u>Comments:</u></b>
					   </td>
					 </tr>
				   </table>

				</div>

				<div class="summery_info-right1">
				   <table style="border:0px;">
					 <tr>
					   <td>
						 <img style="position: relative !important;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/QR_CODE/<?php echo $resut_main_row['result_process_data'][0][0]['qr_code']; ?>" width="120px" height="130px" />
					   </td>
					 </tr>
				   </table>
				 </div>


			  </div>

			<div class="signature_info">
				<div class="signature_left">
					<?php  if (isset($signature['L'])) {
						if ($signature['L']['is_use'] == '1') { ?>

							<img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['L']['location']; ?>" height="60" width="75"/>
							<br>................................<br>
							<span><?php echo $signature['L']['level']; ?></span>

						<?php }
					} ?>&nbsp;
				</div>
				<div class="signature_middle">
					<?php  if (isset($signature['M'])) {
						if ($signature['M']['is_use'] == '1') { ?>
							<img  style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['M']['location']; ?>"  height="60" width="75"/>
							<br>................................<br>
							<span><?php echo $signature['M']['level']; ?></span>
						<?php }
					} ?>
					&nbsp;
				</div>
				<div class="signature_right">
					<?php  if (isset($signature['R'])) {
						if ($signature['R']['is_use'] == '1') { ?>
							<img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['R']['location']; ?>" height="60" width="75"/>
							<br>................................<br>
							<span><?php echo $signature['R']['level']; ?></span>
						<?php }
					} ?>&nbsp;
				</div>
			</div>
			<span style="font-size: 12px;
    float: left;
    bottom: 30px;
    margin-left: -125px;
    position: absolute;
    width: 100%;"><b>Powered By: School360 </b>&nbsp;</span>


	    </div>
	</div>





  </section>
<?php
} ?>

</body>

</html>
