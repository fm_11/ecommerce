<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>

	<!-- Normalize or reset CSS with your favorite library -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/normalize.min.css">

	<!-- Load paper.css for happy printing -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/paper.css">

	<!-- Set page size here: A5, A4 or A3 -->
	<!-- Set also "landscape" if you need -->
	<style>
		@page { size: A4 landscape }

		.body_content{
			width: 100%;
			height: 100%;
			box-sizing: border-box;
			border: 10px #538cc6 solid;
		}

		.body_content_second{
			width: 100%;
			height: 100%;
			box-sizing: border-box;
			border: 6px #ff8080 solid
		}


		.header-center {
			width:100%;
			float:left;
			height: 320px;
			box-sizing: border-box;
			text-align: center;
		}

		.header_title{
			margin-top: 40px;
		}

		.address{
			margin-top: -40px;
			padding: 5px;
		}

		.admit_text{
			width: 200px;
			border: 1px black solid;
			margin: 0px auto;
			margin-top: 10px;
			padding: 10px;
			border-radius: 50px;
			background-color: #538cc6;
			color: #ffffff;
			font-weight: bold;
			font-size: 20px;
		}

		.sl_date{
			width: 90%;
			margin: 0px auto;
		}

		.sl{
			float: left;
			width: 30%;
			text-align:left;
			padding: 6px;
			font-weight:bold;
		}

		.date{
			float: right;
			width: 30%;
			text-align:right;
			padding: 6px;
			font-weight:bold;
		}

		.main_text{
			float: left;
			width:85%;
			height: 200px;
			box-sizing: border-box;
			text-align: center;
			margin:0px auto;
			text-align: justify;
			margin-left: 80px;
			font-size: 18px;
			line-height: 30px;
		}


		* {
			-webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
			color-adjust: exact !important;                 /*Firefox*/
		}

		#footer-content {
			position: absolute;
			text-align: right;
			width: 93%;
			font-size: 15px;
			bottom: 40px;
		}



	</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4 landscape">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
<section class="sheet padding-5mm">
	<div class="body_content">
		<div class="body_content_second">
			<div id="header-center" class="header-center">
				<h1 class="header_title"><?php echo $school_info->school_name; ?></h1><br>
				<div class="address"><?php echo $school_info->address; ?></div>
				<img  class="img" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info->picture; ?>" align="center"
					  width="90px" height="100px">
				<div class="admit_text">
					TESTIMONIAL
				</div>
				<div class="sl_date">
					<div class="sl">
						SL - <?php echo $testimonial_info[0]['sl_no']; ?>
					</div>
					<div class="date">
						Issue Date - <?php echo date("d/m/Y", strtotime($testimonial_info[0]['issue_date'])); ?>
					</div>
				</div>
			</div>

			<div class="main_text">
				This is to certify that <b><?php  echo $testimonial_info[0]['name'];  ?></b> son of
				<b><?php  echo $testimonial_info[0]['fathers_name'];  ?></b> and <b><?php  echo $testimonial_info[0]['mothers_name'];  ?></b>
				Address - <b><?php  echo $testimonial_info[0]['address'];  ?></b>
				 was a student of this institute. He duly passed the
				<b><?php  echo $testimonial_info[0]['exam_name'];  ?></b>
				exam under the <b><?php  echo $testimonial_info[0]['board_name'];  ?></b>
				held in <b><?php  echo $testimonial_info[0]['year'];  ?></b>
				from <b><?php  echo $testimonial_info[0]['group_name'];  ?></b> group bearing Roll No.
				<b><?php  echo $testimonial_info[0]['roll_no'];  ?></b> and
				Registration No. <b><?php  echo $testimonial_info[0]['reg_no'];  ?></b> in the
				session <b><?php  echo $testimonial_info[0]['session'];  ?></b> and achieved G.P.A
				<b><?php  echo $testimonial_info[0]['grade_point'];  ?></b> .
				His date of birth is <b><?php  echo date("d/m/Y", strtotime($testimonial_info[0]['date_of_birth']));  ?></b>.

				<br><br>

				He is a Bangladeshi by birth. To the best of my knowledge he did not take part in any
				subversive activity to the state or of discipline.
				<br><br>
				He has a good moral character. I wish his every success in life.
			</div>
			<div id="footer-content">Powered By: <span style="color:#0059b3;">School</span><span style="color:#ff8080;">360</span></div>
		</div>
	</div>
</section>

</body>

</html>
