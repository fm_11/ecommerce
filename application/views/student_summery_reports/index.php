<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>student_summery_reports/index" method="post">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('year'); ?></label>
      <select class="js-example-basic-single w-100" name="year" required="1">
          <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
          <?php
          $i = 0;
          if (count($years)) {
              foreach ($years as $list) {
                  $i++; ?>
                  <option value="<?php echo $list['value']; ?>" <?php if (isset($year)) {
                      if ($year == $list['value']) {
                          echo 'selected';
                      }
                  } ?>><?php echo $list['text']; ?></option>
                  <?php
              }
          }
          ?>
      </select>
    </div>
    <div class="form-group col-md-4">
      <label>Report Type</label>
      <select class="js-example-basic-single w-100" name="report_type" required="1">
        <option value="cw" <?php if (isset($report_type)) {
              if ($report_type == 'cw') {
                  echo 'selected';
              }
          } ?>>Class Wise</option>
        <option value="csw" <?php if (isset($report_type)) {
              if ($report_type == 'csw') {
                  echo 'selected';
              }
          } ?>>Class & Shift Wise</option>
        <option value="sw" <?php if (isset($report_type)) {
              if ($report_type == 'sw') {
                  echo 'selected';
              }
          } ?>>Section Wise</option>
      </select>
    </div>
  </div>

  <div class="btn-group float-right">
    <input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
    <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
    <input type="submit" class="btn btn-primary" value="View Report">
  </div>
</form>

<div id="printableArea">
    <?php
    if (isset($idata)) {
        echo $report;
    }
    ?>
</div>
