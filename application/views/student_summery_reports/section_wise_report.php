<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
		}
	</style>

	<?php
} ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <td colspan="4" style="text-align:center;">
            <b><?php echo $HeaderInfo['school_name']; ?><br>
            <?php if ($HeaderInfo['eiin_number'] != '') {
     echo "EIIN: " . $HeaderInfo['eiin_number'] . "<br>";
 } ?>
            <?php echo $title; ?> for <?php echo $year; ?><br>
            <?php
            if ($report_type == 'cw') {
                $r_type = 'Class Wise';
            } elseif ($report_type == 'csw') {
                $r_type = 'Class & Shift Wise';
            } else {
                $r_type = 'Section Wise';
            }
            echo 'Type : ' . $r_type;
            ?>
           </b>
        </td>
    </tr>

    <tr>
        <td align="center" scope="col"><b><?php echo $this->lang->line('sl'); ?></b></td>
        <td scope="col"><b>Class</b></td>
        <td scope="col"><b>Section</b></td>
        <td align="center" scope="col"><b>Total Student</b></td>
    </tr>

    </thead>
    <tbody>
    <?php
    $i = 0;
    $total_student = 0;
    foreach ($idata as $row):
        $i++;
        ?>
        <tr>
            <td align="center">
                <?php echo $i; ?>
            </td>
            <td>
                <?php echo $row['class_name']; ?>
            </td>
            <td>
                <?php echo $row['section_name']; ?>
            </td>
            <td align="center">
              <?php
              $total_student = $total_student + $row['total_student'];
              echo $row['total_student'];
               ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <tr>
      <td align="right" colspan="3">
        <b>Total</b>
      </td>
      <td align="center">
        <b>
        <?php
          echo $total_student;
         ?>
       </b>
      </td>
    </tr>
    </tbody>
</table>
</div>
