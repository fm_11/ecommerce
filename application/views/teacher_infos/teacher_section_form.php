<?php
if($action == 'edit'){
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_infos/teacher_section_edit" method="post">
        <label>Section Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtSection" value="<?php echo $section[0]['name']; ?>" required="1"/>

        <br>
        <input type="hidden" name="id" value="<?php echo $section[0]['id']; ?>">
        <input type="submit" class="submit" value="Update">
    </form>
    <br />

    <div class="clear"></div><br />


<?php
}
else{
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_infos/teacher_section_add" method="post">
        <label>Section Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtSection" required="1"/>

        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
    <br />

    <div class="clear"></div><br />

<?php
}
?>
