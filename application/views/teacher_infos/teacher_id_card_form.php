<script>
    function show_off_student_code_section(view_type){
        if(view_type == 'A'){
            document.getElementById("student_code_section").style.display = "none"; 
        }
        if(view_type == 'S'){
            document.getElementById("student_code_section").style.display = ""; 
        }
    }
</script>


<h2>Please Input All Correct Information</h2>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_infos/teacher_id_card_print" method="post"
      enctype="multipart/form-data">
  
    <label>ID Card For?</label>
    <select class="smallInput" name="IdCardFor" required="1">
        <option value="T">Teacher</option>
        <option value="S">Staff</option>
    </select>
   
  
    <label>Print For?</label>
    <select class="smallInput" name="IsPrintFor" required="1">
        <option value="Z">Zebra Printer</option>
        <option value="N">Normal Printer</option>
    </select>
    
    <label>Part?</label>
    <select class="smallInput" name="Part" required="1">
        <option value="F">Front Part</option>
        <option value="B">Back Part</option>
    </select>
  
  
    <br>
    <br>
    <input type="submit" class="submit" value="Process">
    <input type="reset" class="submit" value="Reset">
</form><br/>
<div class="clear"></div><br/>

