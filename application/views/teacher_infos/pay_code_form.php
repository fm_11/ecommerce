<?php
if ($action == 'edit') {
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_infos/pay_code_edit" method="post">
        <label>Pay Code Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtPayCodeName" value="<?php echo $pay_codes[0]['name']; ?>"
               required="1"/>
        <br>
        <input type="hidden" name="id" value="<?php echo $pay_codes[0]['id']; ?>">
        <input type="submit" class="submit" value="Update">
    </form>
    <br/>
    <div class="clear"></div><br/>
<?php
} else {
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_infos/pay_code_add" method="post">
        <label>Pay Code Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtPayCodeName" required="1"/>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
    <br/>

    <div class="clear"></div><br/>
<?php
}
?>
