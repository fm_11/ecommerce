<script>
    $(function() {
        $( "#txtDOB,#syn_date,#txtDOJ" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
    function openCloseSynDate(type) {
        if (type == '0') {
            document.getElementById("syn_area").style.display = "none";
        } else {
            document.getElementById("syn_area").style.display = "";
        }
    }
</script>

<?php
if($action == 'edit'){
    ?>
    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_infos/staff_edit" method="post" enctype="multipart/form-data">
        <label>Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $staff_info[0]['name']; ?>" name="txtName" required="1"/>

		 <label>Staff Index Number</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $staff_info[0]['staff_index_no']; ?>"  name="txtIndexNum" required="1"/>
		
		<label>Process Code</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $staff_info[0]['process_code']; ?>"
               name="process_code" required="1"/>
		
		
        <label>Post</label>
        <select class="smallInput" name="txtPost" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($post)) {
                foreach ($post as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"<?php if($staff_info[0]['post'] == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Shift</label>
        <?php
        foreach ($shift as $row):
            ?>
            <input type="checkbox" name="txtShift[]" <?php if ($row['shift_id'] != '') { ?> checked <?php } ?>
                   value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?>
        <?php endforeach; ?>

        <label>Pay Code</label>
        <select class="smallInput" name="txtPayCode" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($pay_codes)) {
                foreach ($pay_codes as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"<?php if($staff_info[0]['pay_code_id'] == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Staff Section</label>
        <select class="smallInput" name="txtSection" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($sections)) {
                foreach ($sections as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"<?php if($staff_info[0]['section'] == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Date of Joining</label>
        <input type="text" autocomplete="off"  name="txtDOJ" id="txtDOJ" class="smallInput wide" value="<?php echo $staff_info[0]['date_of_join']; ?>"  required="1">

        <label>Date of Birth</label>
        <input type="text" autocomplete="off"  name="txtDOB" id="txtDOB" value="<?php echo $staff_info[0]['date_of_birth']; ?>" class="smallInput wide"  required="1">

        <label>Address</label>
        <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="txtAddress"><?php echo $staff_info[0]['address']; ?></textarea>

        <label>Mobile</label>
        <input type="text" autocomplete="off"  name="txtMobile"  value="<?php echo $staff_info[0]['mobile']; ?>" class="smallInput wide" required="1">
        <input type="hidden" name="id"  value="<?php echo $staff_info[0]['id']; ?>" class="smallInput wide" required="1">


        <label>Is Allowed for Timekeeping</label>
        <input type="radio" value="1" checked
               name="allowed_for_timekeeping" <?php if ($staff_info[0]['allowed_for_timekeeping'] == '1') {
            echo 'checked';
        } ?> required onclick="openCloseSynDate(this.value)"/>Yes
        <input type="radio" value="0"
               name="allowed_for_timekeeping" <?php if ($staff_info[0]['allowed_for_timekeeping'] == '0') {
            echo 'checked';
        } ?>  onclick="openCloseSynDate(this.value)"/>No

        <span id="syn_area" <?php if($staff_info[0]['allowed_for_timekeeping'] == '0'){ ?> style="display: none;" <?php } ?>>
        <label>Synchronize Date </label>
        <input type="text" autocomplete="off"  name="syn_date" value="<?php echo $staff_info[0]['syn_date']; ?>" id="syn_date"
               class="smallInput wide">
         </span>



        <label>Photo</label>
        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/staff/<?php echo $staff_info[0]['photo_location']; ?>"
            height="150" width="150" style=" border:1px solid #666666;"><br><br>
        <input type="file" name="txtPhoto" class="smallInput"> Image Format -> JPEG , JPG and PNG. (Not Required)

        <br>
        <br>
        <input type="submit" class="submit" value="Update">
    </form>
    <br />
<?php
}else{
    ?>
    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_infos/staff_add" method="post" enctype="multipart/form-data">
        <label>Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtName" required="1"/>
		
		 <label>Staff Index Number</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtIndexNum" required="1"/>

		<label>Process Code</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="process_code" required="1"/>
		
        <label>Post</label>
        <select class="smallInput" name="txtPost" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($post)) {
                foreach ($post as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Shift</label>
        <?php
        foreach ($shift as $row):
            ?>
            <input type="checkbox" name="txtShift[]" value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?>
        <?php endforeach; ?>

        <label>Pay Code</label>
        <select class="smallInput" name="txtPayCode" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($pay_codes)) {
                foreach ($pay_codes as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Staff Section</label>
        <select class="smallInput" name="txtSection" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($sections)) {
                foreach ($sections as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Date of Joining</label>
        <input type="text" autocomplete="off"  name="txtDOJ" id="txtDOJ" class="smallInput wide"  required="1">

        <label>Date of Birth</label>
        <input type="text" autocomplete="off"  name="txtDOB" id="txtDOB" class="smallInput wide"  required="1">

        <label>Address</label>
        <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="txtAddress"></textarea>

        <label>Mobile</label>
        <input type="text" autocomplete="off"  name="txtMobile" class="smallInput wide" required="1">


        <label>Is Allowed for Timekeeping</label>
        <input type="radio" value="1" checked name="allowed_for_timekeeping" required onclick="openCloseSynDate(this.value)"/>Yes
        <input type="radio" value="0" name="allowed_for_timekeeping" onclick="openCloseSynDate(this.value)"/>No

        <span id="syn_area">
                <label>Synchronize Date </label>
                <input type="text" autocomplete="off"  name="syn_date" id="syn_date" class="smallInput wide">
        </span>

        <label>Photo</label>
        <input type="file" name="txtPhoto" required="1" class="smallInput"> * Image Format -> JPEG , JPG and PNG.

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
    <br />

<?php
}
?>
<div class="clear"></div><br />

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
