<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>teacher_infos/staff_add"><span>Add New Staff</span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="150" scope="col">Name</th>
		 <th width="150" scope="col">Index No.</th>
        <th width="150" scope="col">Post</th>
        <th width="150" scope="col">Section</th>
        <th width="150" scope="col">Mobile</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($staff_info as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
			 <td><?php echo $row['staff_index_no']; ?></td>
            <td><?php echo $row['post_name']; ?></td>
            <td><?php echo $row['section_name']; ?></td>
            <td><?php echo $row['mobile']; ?></td>

            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>teacher_infos/staff_edit/<?php echo $row['id']; ?>" class="edit_icon" title="Edit"></a>
                <a href="<?php echo base_url(); ?>teacher_infos/staff_delete/<?php echo $row['id']; ?>" onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>

            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

