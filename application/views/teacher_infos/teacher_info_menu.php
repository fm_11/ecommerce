<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>

<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>teacher_infos/teacher_section_info"><span>Section</span></a></li>
    <li><a href="<?php echo base_url(); ?>teacher_infos/teacher_post_info"><span>Post</span></a></li>
    <li><a href="<?php echo base_url(); ?>teacher_infos/pay_code_info"><span>Pay Code</span></a></li>
    <li><a href="<?php echo base_url(); ?>teacher_infos/educational_qualification"><span>Educational Qualification</span></a></li>
    <li><a href="<?php echo base_url(); ?>teacher_infos/index"><span>Teacher Info.</span></a></li>
    <li><a href="<?php echo base_url(); ?>teacher_infos/staff_info"><span>Staff Info.</span></a></li>
</ul>

