<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Student Id Card</title>
  <link rel="stylesheet" href="normalize.css">
  <link rel="stylesheet" href="paper.css">
  <style>
        .id-card-holder {
			width: 225px;
		    padding: 2px;
		    margin: 0 auto;
		    background-color: #1f1f1f;
		    border-radius: 3px;
		    position: relative;
		}
        

		.id-card {		  
	     	background-color: #fff;
			padding: 10px;
			border-radius: 10px;
			text-align: center;
			box-shadow: 0 0 1.5px 0px #b9b9b9;		
		}
		.id-card img {
			margin: 0 auto;
		}
		.header img {
			width: 45%;
    		margin-top: 10px;
			margin-bottom: -20px;
		}
		.photo img {
			width: 40%;
			height:75px;
    		margin-top: 18px;
			margin-bottom: -5px;
			border-radius: 8px;
		}
		h2 {
			font-size: 12px;
			
			margin-top: 24px;
			line-height:12px;
			color:#00001a;
			font-family: "Arial Black", Gadget, sans-serif;
		}
		h3 {
			font-size: 13px;	
			font-weight: 400;
			font-family: "Angsana New", Monaco, monospace;
			margin-top: 14px;
			
		}
		.qr-code img {
			width: 23%;
		}
		p {
			font-size: 11px;
			
			font-family: "Trebuchet MS", Helvetica, sans-serif;
		}
		
  </style>
</head>

<body class="receipt">



  
		<div class="id-card-holder">
			<div class="id-card">
				<div class="header">
					<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/id_card_qr_code.png">
				</div>
				<h2>Emergency : 01715083628</h2>	               				
				<hr style="border:1px #000000 solid;">
				<h2 style="font-size: 12px; margin-top: 5px;">If Found, Kindly Return To-</h2>
				<hr style="border:1px #000000 solid;">
				<h2 style="margin-bottom: -15;margin-top: 10px;"><b>Greenland Model School</b></h2>
				<h3 style="margin-bottom: -15px; margin-top: -8px;"><b>62, Subal Das Road (Chowdhury Bazar), Lalbag, Dhaka 1211.</b></h3>
				<h3 style="margin-bottom: -15px;">Phone: 029611933</h3>
				<h3 style="margin-bottom: -15px;">www.greenlandms.com</h3>	
				<h3 style="margin-bottom: -2px; margin-top: 20px;"><i><b>Note: This card not transferable</b></i></h3>					
			</div>
	    </div>
  
  
</body>
</html>


