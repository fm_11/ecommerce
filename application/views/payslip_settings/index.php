<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>payslip_settings/index" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Who will be the collected user?</label>
			<select class="js-example-basic-single w-100" required id="collected_user" name="collected_user">
				<option value="PC" <?php if ($config->collected_user == 'PC') {
					echo 'selected';
				} ?>>Payslip Creator</option>
				<option value="PA" <?php if ($config->collected_user == 'PA') {
					echo 'selected';
				} ?>>Payslip Approver</option>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Asset Head for Student Payment</label>
			<select class="js-example-basic-single w-100" required id="asset_head_id" name="asset_head_id">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php

				if (count($asset_category)) {
					foreach ($asset_category as $list) {
						?>
						<option
							value="<?php echo $list['id']; ?>" <?php if ($config->asset_head_id == $list['id']) {
							echo 'selected';
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				} ?>
			</select>
		</div>
	</div>

	<input type="hidden" name="id" value="<?php echo $config->id; ?>">

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
