<script>
    function getTemplateBody(template_id) {
        if (template_id == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //alert(xmlhttp.responseText);
                document.getElementById("template_body").value  = "";
                document.getElementById("template_body").value  = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>messages/getTemplateBodyById?template_id=" + template_id, true);
        xmlhttp.send();
    }


    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

</script>



<div class="table-responsive-sm">
	<table  class="table">
    <thead>
    <tr>
        <th colspan="2" width="50" scope="col"><b><?php echo $this->lang->line('file') . ' ' . $this->lang->line('format') . ' ' . $this->lang->line('download'); ?></b></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <b><?php echo $this->lang->line('file'); ?><?php echo $this->lang->line('format'); ?><?php echo $this->lang->line('download'); ?></b>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>uploads/WithMessageBody.xls"><?php echo $this->lang->line('download'); ?></a>
        </td>
    </tr>
    <tr>
        <td>
            <b><?php echo $this->lang->line('without') . ' ' . $this->lang->line('message') . ' ' . $this->lang->line('body'); ?></b>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>uploads/WithoutMessageBody.xls"><?php echo $this->lang->line('download'); ?></a>
        </td>
    </tr>
    </tbody>
</table>
</div>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>excel_files/to_excel_file" method="post"
      enctype="multipart/form-data">
	  
	  <div class="form-row">
		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('message') . ' ' . $this->lang->line('template'); ?></label>
			<select class="form-control" name="template_id" onchange="getTemplateBody(this.value)"
										required="1">
							<option value="N"><?php echo $this->lang->line('excel') . ' ' . $this->lang->line('file') . ' ' . $this->lang->line('with') . ' ' . $this->lang->line('template'); ?></option>
							<?php
							$i = 0;
							if (count($templates)) {
								foreach ($templates as $list) {
									$i++;
									?>
									<option
											value="<?php echo $list['id']; ?>"><?php echo $list['template_name']; ?></option>
									<?php
								}
							}
							?>
		   </select>
		</div>
		
		<div class="form-group col-md-6">
		   <label><?php echo $this->lang->line('message') . ' ' . $this->lang->line('body'); ?></label>
		   <textarea class="form-control" name="template_body" rows="5" cols="35" id="template_body"></textarea>
		</div>
	</div>
	<div class="form-row">	
		<div class="form-group col-md-6">
		  <label><?php echo $this->lang->line('message') . ' ' . $this->lang->line('type'); ?></label>
			<select class="form-control" name="sms_type" required="1">
				  <option value="E"><?php echo $this->lang->line('english'); ?></option>
				  <option value="B"><?php echo $this->lang->line('bangla'); ?></option>
		   </select>
		</div>
		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('file'); ?></label>
			<input type="file" name="txtFile" required="1" class="form-control"> * File Format -> Excel-2003
		</div>
	</div>	

    <div class="float-right">
	   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form><br/>
<div class="clear"></div>
