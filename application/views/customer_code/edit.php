

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>customer_code/edit/<?php echo $row_data->id; ?>" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('customer_header_code')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="customer_header_code" value="<?php echo $row_data->customer_header_code; ?>" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('customer_footer_code')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="customer_footer_code" value="<?php echo $row_data->customer_footer_code; ?>" required="1"/>
    </div>
  </div>
    <div class="float-right">
	   <input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $row_data->id; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
