

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>customer_code/add" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('customer_header_code')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="customer_header_code" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('customer_footer_code')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="customer_footer_code" required="1"/>
    </div>
  </div>


    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
