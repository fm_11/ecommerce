<script type="text/javascript" src="<?php echo base_url(); ?>core_media/js/jscolor/jscolor.js"></script>
<script>
	function resetConfirm() {
		var result = confirm("Are you sure to reset all color?");
		if (result == true) {
			return true;
		} else {
			return false;
		}
	}
</script>
	<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard_color_settings/index" method="post">
		<p class="mb-0">
			<b>	বিঃদ্রঃ ১.</b> বাম পাশের মেনুর জন্য এবং উপরের বারের জন্য এমন রং সেট করবেন না যেটা লেখার সাদা রং এবং হোভার এর কালো  রং এর সাথে খারাপ লাগে।
			<b>২.</b> বাম পাশের মেনু , উপরের বার এবং মেনু খোলা ও বন্ধ করার বাটনের রং পরবর্তী লগইন করার পর কার্যকর হবে।
			<b>৩.</b> কারিগরি টীম এর পক্ষ থেকে নির্ধারিত রং সেট করতে রিসেট বাটন এ ক্লিক করুন এবং কন্ফার্ম করুন। ধন্যবাদ
		</p>

		<hr>

		<div class="form-row">

			<div class="form-group col-md-4">
				<label>Left Dark Menu Background</label>
				<input type="text" onchange="checkSideBar(this.value)" autocomplete="off" style="background-color: <?php echo $config_info->left_menu_background_color; ?>" value="<?php echo $config_info->left_menu_background_color; ?>"  required name="left_menu_background_color" id="left_menu_background_color" class="form-control color">
			</div>

			<div class="form-group col-md-4">
				<label>Top Bar Background</label>
				<input type="text" onchange="checkTopBar(this.value)"  autocomplete="off" style="background-color: <?php echo $config_info->top_bar_background_color; ?>" value="<?php echo $config_info->top_bar_background_color; ?>"  required name="top_bar_background_color" id="top_bar_background_color" class="form-control color">
			</div>

			<div class="form-group col-md-4">
				<label>Top Menu Show/Hide Button Background</label>
				<input type="text"  onchange="checkTopBarButton(this.value)"  autocomplete="off" style="background-color: <?php echo $config_info->top_menu_show_hide_button_color; ?>" value="<?php echo $config_info->top_menu_show_hide_button_color; ?>"  required name="top_menu_show_hide_button_color" id="top_menu_show_hide_button_color" class="form-control color">
			</div>
		</div>

		<div class="form-row">
			<div class="form-group col-md-3">
				<label>Total Student Box Background</label>
				<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->total_student_background_color; ?>;" value="<?php echo $config_info->total_student_background_color; ?>"  required name="total_student_background_color" class="form-control color">
			</div>

			<div class="form-group col-md-3">
				<label>Present Box Background</label>
				<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->present_student_background_color; ?>;"  required name="present_student_background_color" class="form-control color" value="<?php echo $config_info->present_student_background_color; ?>">
			</div>

			<div class="form-group col-md-3">
				<label>Absent Box Background</label>
				<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->absent_student_background_color; ?>;" required name="absent_student_background_color" class="form-control color" value="<?php echo $config_info->absent_student_background_color; ?>">
			</div>

			<div class="form-group col-md-3">
				<label>Leave Box Background</label>
				<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->leave_student_background_color; ?>;"  required name="leave_student_background_color" class="form-control color" value="<?php echo $config_info->leave_student_background_color; ?>">
			</div>

		</div>

		<div class="form-row">
		<div class="form-group col-md-3">
			<label>Institute Info. Border</label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->school_name_border_color; ?>"  required name="school_name_border_color" class="form-control color" value="<?php echo $config_info->school_name_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Institute Head Info. Border</label>
			<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->head_name_border_color; ?>" required name="head_name_border_color" class="form-control color" value="<?php echo $config_info->head_name_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>SMS Balance Border</label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->sms_balance_border_color; ?>"  required name="sms_balance_border_color" class="form-control color" value="<?php echo $config_info->sms_balance_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Birthday Section Border</label>
			<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->birthday_border_color; ?>" value="<?php echo $config_info->birthday_border_color; ?>"  required name="birthday_border_color" class="form-control color">
		</div>
	</div>


	<div class="form-row">
		<div class="form-group col-md-3">
			<label>Class Wise Student Border</label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->class_wise_student_border_color; ?>"  required name="class_wise_student_border_color" class="form-control color" value="<?php echo $config_info->class_wise_student_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Gender Wise Student Border</label>
			<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->gender_wise_wise_border_color; ?>" required name="gender_wise_wise_border_color" class="form-control color" value="<?php echo $config_info->gender_wise_wise_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Income vs Expense Border</label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->income_vs_expense_border_color; ?>"  required name="income_vs_expense_border_color" class="form-control color" value="<?php echo $config_info->income_vs_expense_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Last 7 days collection Border</label>
			<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->last_7_days_collection_border_color; ?>" value="<?php echo $config_info->last_7_days_collection_border_color; ?>"  required name="last_7_days_collection_border_color" class="form-control color">
		</div>

	</div>



	<div class="form-row">

		<div class="form-group col-md-3">
			<label>Today's Collection Border</label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->todays_collection_border_color; ?>"  required name="todays_collection_border_color" class="form-control color" value="<?php echo $config_info->todays_collection_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Monthly Collection Border</label>
			<input type="text" autocomplete="off" style="background-color: <?php echo $config_info->monthly_collection_border_color; ?>" required name="monthly_collection_border_color" class="form-control color" value="<?php echo $config_info->monthly_collection_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Monthly Expense Border</label>
			<input type="text" autocomplete="off"  style="background-color: <?php echo $config_info->monthly_expense_border_color; ?>"  required name="monthly_expense_border_color" class="form-control color" value="<?php echo $config_info->monthly_expense_border_color; ?>">
		</div>

	</div>





	<input type="hidden" readonly autocomplete="off"  required name="id" class="form-control" value="<?php echo $config_info->id; ?>">
	<div class="float-right">
		<button type="submit" onclick="return resetConfirm()" class="btn btn-outline-warning btn-icon-text">
			<i class="ion ion-md-refresh"></i>
			Reset
		</button>
		<input class="btn btn-primary" type="submit" name="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>

<script>
	function checkSideBar(value){
		$('#sidebar').css('background-color',"#" + value);
	}
	function checkTopBar(value){
		$('#top_bar_background').css('background-color',"#" + value);
	}
	function checkTopBarButton(value){
		$('#top_menu_show_hide_button').css('background-color',"#" + value);
	}
</script>

</script>
