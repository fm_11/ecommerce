<script>
	function checkAllForSubjectAssign(ele) {
		// alert(55);
		var checkboxes = document.getElementsByTagName('input');
		if (ele.checked) {
			for (var i = 1; i < checkboxes.length; i++) {
				var cname = checkboxes[i].name.split("_");
				var test = cname[0] + cname[1];
				if (checkboxes[i].type == 'checkbox' && test == 'isallow') {
					checkboxes[i].checked = true;
				}
			}
			document.getElementById("btnCancelSingle").disabled = false;
			document.getElementById("btnCancelMultiple").disabled = false;
		} else {
			for (var i = 1; i < checkboxes.length; i++) {
				var cname = checkboxes[i].name.split("_");
				var test = cname[0] + cname[1];
				if (checkboxes[i].type == 'checkbox' && test == 'isallow') {
					checkboxes[i].checked = false;
				}
			}
			document.getElementById("btnCancelSingle").disabled = true;
			document.getElementById("btnCancelMultiple").disabled = true;
		}
	}
	
	function btnDisableCheck() {
		var inputElems = document.getElementsByTagName("input"),
				count = 0;
		for (var i = 0; i < inputElems.length; i++) {
			if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
				count++;
			}
		}
		if (count < 1) {
			document.getElementById("btnCancelSingle").disabled = true;
			document.getElementById("btnCancelMultiple").disabled = true;
		} else {
			document.getElementById("btnCancelSingle").disabled = false;
			document.getElementById("btnCancelMultiple").disabled = false;
		}
	}

	function validateOptionalAndCompulsorySubject(selected_item,fromSelect) {
        var selected_items = getSelectValues(selected_item);

        var check_items = "";
        if(fromSelect == 'CS'){
			check_items =  getSelectValues(document.getElementById("optional_subjects"));
		}else{
			check_items =  getSelectValues(document.getElementById("compulsory_subjects"));
		}

		var totalCheckItem = check_items.length;
        var totalSelectedItem = selected_items.length;
        var errorFound = 0;
        if(totalCheckItem > 0 && totalSelectedItem > 0){
			var i = 0;
			while (i < totalCheckItem){
				var checkRowItem = check_items[i];

				//check value
                var j = 0;
                while (j < totalSelectedItem){
                	if(checkRowItem == selected_items[j]){
						errorFound = 1;
                		break;
					}
                	j++;
				}
				if(errorFound == 1){
					break;
				}
				//end check value

				i++;
			}
			if(errorFound == 1){
				alert("You can't choose same subject");
				if(fromSelect == 'CS'){
					$('#compulsory_subjects').val('').trigger("change");
				}else{
					$('#optional_subjects').val('').trigger("change");
				}
				return false;
			}else{
				return true;
			}
		}
	}

	function getSelectValues(select) {
		var result = [];
		var options = select && select.options;
		var opt;

		for (var i=0, iLen=options.length; i<iLen; i++) {
			opt = options[i];

			if (opt.selected) {
				result.push(opt.value || opt.text);
			}
		}
		return result;
	}

	function removeAndAddRequired(clickType) {
		if(clickType == 'M'){
			document.getElementById("assign_type").required = false;
			document.getElementById("subject_id").required = false;

			document.getElementById("compulsory_subjects").required = true;
			document.getElementById("optional_subjects").required = true;
		}else{
			document.getElementById("assign_type").required = true;
			document.getElementById("subject_id").required = true;

			document.getElementById("compulsory_subjects").required = false;
			document.getElementById("optional_subjects").required = false;
		}
	}

</script>
<form method="post" action="<?php echo base_url(); ?>subject_assigns/single_and_multiple_assign_data_save">
<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th scope="col"><input type="checkbox" onchange="checkAllForSubjectAssign(this)"></th>
			<th scope="col">Roll</th>
			<th scope="col">Name</th>
			<th scope="col">Student ID</th>
		</tr>
		</thead>
		<tbody>


		<?php
		$i = 0;
		foreach ($student_info as $row):
			$i++;
			?>
			<tr>

			<tr>
				<td>
					<input type="checkbox" onclick="btnDisableCheck()" id="is_allow_<?php echo $i; ?>" name="is_allow_<?php echo $i; ?>">
				</td>
				<td>
					<?php echo $row['roll_no']; ?>
				</td>
				<td>
					<?php echo $row['name']; ?>
					<input type="hidden" class="input-text-short" size="8"
						   style="text-align: right; width:75px;"
						   name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>

					<input type="hidden" class="input-text-short" size="8"
						   style="text-align: right; width:75px;"
						   name="section_id_<?php echo $i; ?>" value="<?php echo $row['section_id']; ?>"/>

					<input type="hidden" class="input-text-short" size="8"
						   style="text-align: right; width:75px;"
						   name="shift_id_<?php echo $i; ?>" value="<?php echo $row['shift_id']; ?>"/>


				</td>
				<td>
					<?php echo $row['student_code']; ?>
				</td>

			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<input type="hidden" class="input-text-short" name="loop_time"
		   value="<?php echo $i; ?>"/>
	<input type="hidden" class="input-text-short" name="year"
		   value="<?php echo $year; ?>"/>

	<input type="hidden" class="input-text-short" name="class_id"
		   value="<?php echo $class_id; ?>"/>

	<input type="hidden" class="input-text-short" name="shift_id"
		   value="<?php echo $shift_id; ?>"/>

	<input type="hidden" class="input-text-short" name="section_id"
		   value="<?php echo $section_id; ?>"/>

	<input type="hidden" class="input-text-short" name="group"
		   value="<?php echo $group; ?>"/>

	<div class="float-right">
		<button type="button" id="btnCancelMultiple" onclick="removeAndAddRequired('M')" disabled class="btn btn-primary" data-toggle="modal" data-target="#MultipleDiv">
			Multiple
		</button>
		<button type="button" id="btnCancelSingle"  onclick="removeAndAddRequired('S')" disabled class="btn btn-success" data-toggle="modal" data-target="#SingleDiv">
			Single
		</button>
	</div>
</div>


<!-- Modal starts -->

<div class="modal fade" id="MultipleDiv" tabindex="-1" role="dialog" aria-labelledby="MultipleDivLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="MultipleDivLabel">Multiple Subject Assign</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="recipient-name" class="col-form-label">Compulsory Subjects</label><br>
					<select onchange="return validateOptionalAndCompulsorySubject(this,'CS')" class="js-example-basic-multiple" style="width: 100% !important;" multiple name="compulsory_subjects[]" id="compulsory_subjects" required="1">
						<?php
						$i = 0;
						if (count($subject_list)) {
							foreach ($subject_list as $list) {
								$i++; ?>
								<option value="<?php echo $list['subject_id']; ?>">
									<?php echo $list['name']; ?>
								</option>
								<?php
							}
						} ?>
					</select>
				</div>
				<div class="form-group">
					<label for="recipient-name" class="col-form-label">Optional Subjects</label><br>
					<select onchange="return validateOptionalAndCompulsorySubject(this,'OPT')" class="js-example-basic-multiple" style="width: 100% !important;"  multiple name="optional_subjects[]" id="optional_subjects" required="1">
						<?php
						$i = 0;
						if (count($subject_list)) {
							foreach ($subject_list as $list) {
								$i++; ?>
								<option value="<?php echo $list['subject_id']; ?>">
									<?php echo $list['name']; ?>
								</option>
								<?php
							}
						} ?>
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" name="MultipleSaveButton" class="btn btn-primary">Save</button>
				<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>






<div class="modal fade" id="SingleDiv" tabindex="-1" role="dialog" aria-labelledby="SingleDivLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="SingleDivLabel">Single Subject Assign</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div class="modal-body">
					<div class="form-group">
						<select class="form-control" name="assign_type" id="assign_type" required="1">
							<option value="">-- Select Type --</option>
							<option value="COM">Compulsory</option>
							<option value="OPT">Optional</option>
						</select>
					</div>
					<div class="form-group">
						<select class="form-control" name="subject_id" id="subject_id" required="1">
							<option value="">-- Select Subject --</option>
							<?php
							$i = 0;
							if (count($subject_list)) {
								foreach ($subject_list as $list) {
									$i++; ?>
									<option value="<?php echo $list['subject_id']; ?>">
										<?php echo $list['name']; ?>
									</option>
									<?php
								}
							} ?>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" name="SingleSaveButton" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
				</div>
		</div>
	</div>
</div>
</form>
<!-- Modal Ends -->
