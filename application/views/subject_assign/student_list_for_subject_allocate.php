<script>
function checkAllForSubjectAssign(ele) {
      // alert(55);
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 1; i < checkboxes.length; i++) {
                var cname = checkboxes[i].name.split("_");
                var test = cname[0] + cname[1];
                if (checkboxes[i].type == 'checkbox' && test == 'isallow') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 1; i < checkboxes.length; i++) {
				var cname = checkboxes[i].name.split("_");
				var test = cname[0] + cname[1];
				if (checkboxes[i].type == 'checkbox' && test == 'isallow') {
					checkboxes[i].checked = false;
				}
            }
        }
    }

    function checkAllForIsOptional(ele) {
		var checkboxes = document.getElementsByTagName('input');
		if (ele.checked) {
			for (var i = 1; i < checkboxes.length; i++) {
				var cname = checkboxes[i].name.split("_");
				var test = cname[0] + cname[1];
				//alert(test);
				if (checkboxes[i].type == 'checkbox' && test == 'isoptional') {
					checkboxes[i].checked = true;
				}
			}
		} else {
			for (var i = 1; i < checkboxes.length; i++) {
				var cname = checkboxes[i].name.split("_");
				var test = cname[0] + cname[1];
				//alert(test);
				if (checkboxes[i].type == 'checkbox' && test == 'isoptional') {
					checkboxes[i].checked = false;
				}
			}
		}
	}
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>subject_assigns/student_subject_assign_data_save" method="post">
  <div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th scope="col"><input type="checkbox" onchange="checkAllForSubjectAssign(this)"></th>
            <th scope="col">Name</th>
            <th scope="col">Student ID</th>
            <th scope="col">Roll</th>
			<th style="text-align: center;" scope="col">
				Is Optional Subject? &nbsp; <input type="checkbox" onchange="checkAllForIsOptional(this)">
			</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($student_info as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td>
                    <input type="checkbox" <?php if ($row['already_allocate_subject_id'] > 0) {
                echo 'checked';
            } ?> name="is_allow_<?php echo $i; ?>">
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" class="input-text-short" size="8"
                           style="text-align: right; width:75px;"
                           name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>

                    <input type="hidden" class="input-text-short" size="8"
                   style="text-align: right; width:75px;"
                   name="section_id_<?php echo $i; ?>" value="<?php echo $row['section_id']; ?>"/>

                    <input type="hidden" class="input-text-short" size="8"
                   style="text-align: right; width:75px;"
                   name="shift_id_<?php echo $i; ?>" value="<?php echo $row['shift_id']; ?>"/>


                </td>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
                <td>
                    <?php echo $row['roll_no']; ?>
                </td>
				 <td style="text-align: center;">
                    <input type="checkbox" <?php if ($row['is_optional'] > 0) {
                echo 'checked';
            } ?> name="is_optional_<?php echo $i; ?>">
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="year"
           value="<?php echo $year; ?>"/>

    <input type="hidden" class="input-text-short" name="class_id"
           value="<?php echo $class_id; ?>"/>

   <input type="hidden" class="input-text-short" name="section_id"
		 value="<?php echo $section_id; ?>"/>

   <input type="hidden" class="input-text-short" name="shift_id"
		 value="<?php echo $shift_id; ?>"/>


    <input type="hidden" class="input-text-short" name="subject_id"
           value="<?php echo $subject_id; ?>"/>

    <input type="hidden" class="input-text-short" name="group"
           value="<?php echo $group; ?>"/>

           <div class="float-right">
               <input type="submit" name="Process" class="btn btn-primary" value="Save">
           </div>
      </div>
</form>
