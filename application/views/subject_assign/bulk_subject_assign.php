<?php
$class_id = $this->session->userdata('class_id');
$year = $this->session->userdata('year');
?>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>subject_assigns/bulk_subject_assign" method="post">
    <div class="form-row">
        <div class="form-group col-md-4">
          <label>Year</label>
          <select class="js-example-basic-single w-100" name="year" id="year" required="1">
               <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
               <?php
               $i = 0;
               if (count($years)) {
                   foreach ($years as $list) {
                       $i++; ?>
                       <option
                           value="<?php echo $list['value']; ?>"
                           <?php if (isset($year)) {
                           if ($year == $list['value']) {
                               echo 'selected';
                           }
                       } ?>>
                       <?php echo $list['text']; ?>
                     </option>
                       <?php
                   }
               }
               ?>
           </select>
        </div>
        <div class="form-group col-md-4">
          <label>Class</label>
          <select class="js-example-basic-single w-100" name="class_id" required="1">
              <option value="">-- Please Select --</option>
              <?php
              $i = 0;
              if (count($class)) {
                  foreach ($class as $list) {
                      $i++; ?>
                      <option
                          value="<?php echo $list['id']; ?>" <?php if(isset($class_id)){ if($class_id == $list['id']){ echo 'selected'; }} ?>><?php echo $list['name']; ?></option>
                  <?php
                  }
              }
              ?>
          </select>
        </div>
    </div>
    <div class="float-right">
        <input type="submit" name="Process" class="btn btn-primary" value="Process">
    </div>
    </form>
