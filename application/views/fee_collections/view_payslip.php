<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title><?php  echo $title; ?></title>

	<!-- Normalize or reset CSS with your favorite library -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/normalize.min.css">

	<!-- Load paper.css for happy printing -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/paper.css">

	<!-- Set page size here: A5, A4 or A3 -->
	<!-- Set also "landscape" if you need -->
	<style>
		@page { size: A4 landscape }
		.main_content {
			width: 100%;
			height: auto;
			box-sizing: border-box;
		}


		.body_content{
			width: 100%;
			height: auto;
			box-sizing: border-box;
		}

		#header{
			width:100%;
			box-sizing: border-box;
			height: 50px;
		}
		.header-left {
			width:80%;
			float:left;
			height: 100%;
			text-align: left;
			display: table;
		}
		.logo {
			width:15%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: center;
			vertical-align: top;
		}

		.clr{ clear:both;}


		.frame{
			width: 100%;
			height: 100%;
			margin: auto;
			position: relative;
		}
		.log_frame{
			width: 100%;
			height: 120px;
			margin: auto;
			position: relative;
		}


		.img{
			max-height: 85%;
			max-width: 85%;
			position: absolute;
			top: 25px;
			bottom: 0;
			left: 10px;
			right: 0;
		}

		.gpa_table {
			border-collapse: collapse;
			margin: 0 auto;
			width: 90%;
			height: auto;
			position: relative;
			top:10px;
		}

		table, td, th {
			border: 1px solid black;
			padding: 3px;
			font-size: 13px;
		}

		#student_info{
			width:100%;
			box-sizing: border-box;
			height: 200px;
		}

		.student_info-left {
			width:100%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: left;
		}

		.student_info_table {
			border-collapse: collapse;
			margin: 0 auto;
			width: 98%;
			height: auto;
			position: relative;
		}

		#student_exam_info{
			width:100%;
			box-sizing: border-box;
			height: auto;

		}

		.exam_info-left {
			width:100%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: left;
		}

		.mark_info_table{
			border-collapse: collapse;
			margin: 0 auto;
			margin-top:10px;
			width: 98%;
			height: auto;
			position: relative;
		}

		.td_center{
			text-align: center;
		}

		.td_right{
			text-align: right;
		}



		.icon-bar {
			position: fixed;
			top: 40%;
			-webkit-transform: translateY(-50%);
			-ms-transform: translateY(-50%);
			transform: translateY(-50%);
		}

		.icon-bar a {
			display: block;
			text-align: center;
			padding: 10px;
			transition: all 0.3s ease;
			color: white;
			font-size: 13px;
			text-decoration:none;
			width:150px;
		}

		.icon-bar a:hover {
			background-color: #000;
		}


		#signature_info{
			width:100%;
			box-sizing: border-box;
			height: 100px;
		}


		.signature_left {
			width:66%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: left;
			vertical-align: bottom;
		}
		.signature_middle {
			width:34%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: center;
			vertical-align: bottom;
		}
		.signature_right {
			width:33%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: center;
			vertical-align: bottom;
		}

		.clear:before, .clear:after {
			content: "\0020";
			display: block;
			height: 0;
			overflow: hidden;
		}

		.clear:after {
			clear: both;
		}

		.td_backgroud{
			background-color: #d9e6f2;
			font-weight: bold;
		}

		.sheet {
			overflow: visible;
			height: auto !important;
		}


		.reortPrintOption{
			float: left;
			padding: 5px;
			width: 100%;
		}


		.printButton:link, .printButton:visited {
			background-color: #f44336;
			color: white;
			padding: 8px 8px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
		}

		.printButton:hover, .printButton:active {
			background-color: red;
		}

		.backToButton:link, .backToButton:visited {
			background-color: #a3c2c2;
			color: white;
			padding: 8px 8px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
		}

		.backToButton:hover, .backToButton:active {
			background-color: #527a7a;
		}
		.three_payslip_div{
			float:left;
			width:31%;
			text-align: center;
			height: auto;
			padding: 12px;
		}

	</style>

	<script>
		function printDiv(divName) {
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
		}
	</script>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4 landscape">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
<section class="sheet padding-5mm" id="printableArea">
	<div class="main_content clear" id="main_content">
		<div class="body_content clear" id="body_content">
			<?php
			$start = 0;
			while ($start < 3){
				?>
			<div class="three_payslip_div" <?php if($start != 0){ ?> style="border-left: 1px #000000 dashed;" <?php } ?>>
				<div style="float: right;font-size: 13px;font-weight: bold;">
					<?php
					if($start == 0){
						echo 'Bank Copy';
					}else if($start == 1){
						echo 'Institute Copy';
					}else{
						echo 'Student Copy';
					}
					?>
				</div>
				<div id="header" class="header">
					<div class="logo">
						<div class="frame">
							<img class="img" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info->picture; ?>" />
						</div>
					</div>
					<div class="header-left">
						<span style="display: table-cell;vertical-align: top;text-align: left; padding-left: 10px;">
							<span style="font-size: 16px;
			                   font-weight: bold;"><?php echo $school_info->school_name; ?></span><br>
							<span style="font-size: 12px;
			                   font-weight: bold;">
								<?php echo $school_info->address; ?>
							</span>
						</span>
					</div>
				</div>
				<div class="student_info">
					<div class="student_info-left">
						<table class="student_info_table">
							<tr>
								<td style="padding: 7px;" colspan="2" align="center"><b>Student Pay-slip<b/></td>
							</tr>
							<tr>
								<td width="100%" style="padding: 7px; line-height:1.3; ">
									Student ID : <b><?php echo $collection_sheet_info[0]['student_code']; ?></b><br>
									Name : <b><?php echo $collection_sheet_info[0]['student_name']; ?></b><br>
									Class & Section : <b><?php echo $collection_sheet_info[0]['class'] . ', ' . $collection_sheet_info[0]['section']; ?></b><br>
									Roll No. : <b><?php echo $collection_sheet_info[0]['roll_no']; ?></b><br>
									Year : <b><?php echo $collection_sheet_info[0]['year']; ?></b><br>
									Invoice No. : <b><?php echo $collection_sheet_info[0]['receipt_no']; ?></b><br>
									Payment Date : <?php echo date("d/m/Y", strtotime($collection_sheet_info[0]['date'])); ?>	<br>
								</td>
							</tr>
						</table>
					</div>
				</div>


				<div class="student_exam_info">
					<div class="exam_info-left">
						<table class="mark_info_table">
							<tr>
								<td width="70%" class="td_center td_backgroud">
									<b>Fee Name</b>
								</td>
								<td width="30%" class="td_center td_backgroud">
									<b>Paid Amount</b>
								</td>
							</tr>

							<?php
							$total_accual_amount = 0;
							$total_waiver_amount = 0;
							$total_paid_amount = 0;
							$total_due_amount = 0;
							if (isset($collection_sheet_details) && !empty($collection_sheet_details)) {
								foreach ($collection_sheet_details as $row):
									?>

									<tr>
										<td class="td_left">
											<?php
											$monthDateObj = DateTime::createFromFormat('!m', $row['month']);
											echo $row['sub_category'] . ', '. $monthDateObj->format('F');
											?>
										</td>

										<td class="td_right">
											<?php
											$total_paid_amount += $row['paid_amount']; ?>
											<?php echo number_format($row['paid_amount'], 2); ?>
										</td>
									</tr>

								<?php endforeach; ?>


								<?php
							}
							?>


							<?php
							if ($late_fee != '' && $late_fee > 0) {
								?>
								<tr>
									<td align="td_left"><?php echo $this->lang->line('late_fee'); ?></td>

									<td class="td_right">
										<?php
										$total_paid_amount +=  $late_fee;
										echo number_format($late_fee, 2); ?>
									</td>
								</tr>

								<?php
							}
							?>


							<?php
							if (!empty($transport_fees)) {
								?>

								<tr>
									<td  class="td_left td_backgroud" colspan="2"><b><?php echo $this->lang->line('transport').' '.$this->lang->line('fees'); ?> </b></td>
								</tr>

								<?php
								foreach ($transport_fees as $transport_row):
									?>

									<tr>
										<td align="td_left"><?php echo DateTime::createFromFormat('!m', $transport_row['month'])->format('F'); ?>, <?php echo $transport_row['year']; ?></td>
										<td class="td_right">
											<?php
											$total_paid_amount += $transport_row['amount']; ?>
											<?php echo number_format($transport_row['amount'], 2); ?>
										</td>
									</tr>
								<?php endforeach; ?>

								<?php
							}
							?>




							<tr>
								<td class="td_left td_backgroud">
									<b>Total</b>
								</td>
								<td class="td_right td_backgroud">
									<b><?php echo number_format($total_paid_amount, 2); ?></b>
								</td>
							</tr>

							<tr>
								<td class="td_left" colspan="2">&nbsp;<b><?php echo $this->lang->line('in_words'); ?>: <?php echo $this->numbertowords->convert_number($total_paid_amount); ?><?php echo $this->lang->line('tk').' '.$this->lang->line('only'); ?> .</b></td>
							</tr>
						</table>
						<span style="font-size: 12px; padding: 5px;">
							<b>Note: Payable includes waiver, discount & fine</b><br>
						</span>
						<span style="font-size: 13px; padding: 5px;">
							Created By : <?php echo $collection_sheet_info[0]['user_name']; ?><br>
						</span>
						<span style="font-size: 13px; padding: 5px;">
							<b>Powered By: <span style="color:#0059b3;">School</span><span style="color:#ff8080;">360</span> </b>
						</span>
					</div>
				</div>

				<div class="signature_info">
					<div class="signature_left">
						&nbsp;
					</div>
					<div class="signature_right">
						<br>.............................<br>
						<span style="font-size: 13px;">Received By</span>
					</div>

				</div>
			</div>
			<?php
				$start++;
				}
			?>
		</div>
	</div>
</section>
</body>

</html>
