
<script type="text/javascript">
    function getStudentByClassShiftSection(class_shift_section_id,group_id){
       //alert(class_shift_section_id);
       //alert(group_id);
       if(class_shift_section_id != ''){
         if (window.XMLHttpRequest)
         {
             xmlhttp = new XMLHttpRequest();
         }
         else
         {
             xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
         }
         xmlhttp.onreadystatechange = function()
         {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
             {
                // alert(xmlhttp.responseText);
                 document.getElementById("student_id").innerHTML = xmlhttp.responseText;
             }
         }
         xmlhttp.open("GET", "<?php echo base_url(); ?>students/getStudentByClassShiftSection?class_shift_section_id=" + class_shift_section_id + "&&group_id=" + group_id, true);
         xmlhttp.send();
       }
    }

    function getStudentInformationById(student_id) {
      if(student_id != ''){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {

                var jsonObject = JSON.parse((xmlhttp.responseText));
                //alert(jsonObject[0].name);
                var ImageFileLocation = jsonObject[0].photo;
                if(ImageFileLocation != '' && ImageFileLocation != 'default.png'){
                  document.getElementById("student_image").src = '<?php echo base_url() . MEDIA_FOLDER; ?>/student/' + ImageFileLocation;
                }else{
                  document.getElementById("student_image").src = '<?php echo base_url(); ?>/core_media/images/default.png';
                }
                //document.getElementById("student_id").innerHTML = xmlhttp.responseText;

            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>students/getStudentInformationById?student_id=" + student_id, true);
        xmlhttp.send();
      }else{
        //alert(55);
        document.getElementById("student_image").src = '<?php echo base_url(); ?>/core_media/images/default.png';
      }
    }


    function getSelectValues(select) {
    	var result = [];
    	var options = select && select.options;
    	var opt;

    	for (var i = 0, iLen = options.length; i < iLen; i++) {
    		opt = options[i];

    		if (opt.selected) {
    			result.push(opt.value || opt.text);
    		}
    	}
    	return result;
    }



    function getCollectionSheet() {
        var class_shift_section_id = document.getElementById('class_shift_section_id').value;
        var group_id = document.getElementById('group_id').value;
        var student_id = document.getElementById('student_id').value;
        var year = document.getElementById('year').value;
        var collection_month = document.getElementById('collection_month').value;

        var monthList = document.getElementById('month');
        var month = getSelectValues(monthList);
      //  alert(month);

        if (class_shift_section_id == '' || group_id == '' || student_id == '' || year == '' || month == '') {
            alert("Please select all information");
            return false;
        }

        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("collection_form").innerHTML = xmlhttp.responseText;
                document.getElementById("fee_item_select").click();
                $(".date").datepicker({
                  autoclose: true,
                  todayHighlight: true,
                  format: 'yyyy-mm-dd',
                }).datepicker();
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>fee_collections/getCollectionSheet?class_shift_section_id=" + class_shift_section_id + '&& group_id='
            + group_id + '&& student_id=' + student_id + '&&year=' + year + '&&month=' + month + '&&collection_month=' + collection_month, true);
        xmlhttp.send();
    }

    function feeStatusReportOpen() {
       var year = document.getElementById('year').value;
       var student_id = document.getElementById('student_id').value;
       var class_shift_section_id = document.getElementById('class_shift_section_id').value;
       var group_id = document.getElementById('group_id').value;
       if(year == ''){
         alert('Please select a year');
         return false;
       }
       if(class_shift_section_id == ''){
         alert('Please select a class/section/shift');
         return false;
       }
       if(group_id == ''){
         alert('Please select a group');
         return false;
       }
       if(student_id == ''){
         alert('Please select a student');
         return false;
       }
       var newURL = "<?php echo base_url(); ?>student_fee_status_details/index/" + student_id + '/' + year + '/' + class_shift_section_id + '/' + group_id;
       var win = window.open(newURL, '_blank');
       win.focus();
    }



	function payslipConfirm() {
		var result = confirm("Do you want to make payslip?");
		if (result == true) {
			return true;
		} else {
			return false;
		}
	}


</script>



    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>fee_collections/add" method="post">
       
        <div class="form-row">
            <div class="form-group col-md-4">
              <label><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
              <select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id"
               onchange="getStudentByClassShiftSection(this.value,document.getElementById('group_id').value);" required="1">
                  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                  <?php
                  $i = 0;
                  if (count($class_section_shift_marge_list)) {
                      foreach ($class_section_shift_marge_list as $list) {
                          $i++; ?>
                          <option
                              value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
                              <?php if (isset($class_shift_section_id)) {
                              if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
                                  echo 'selected';
                              }
                          } ?>>
                          <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
                        </option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group col-md-4">
                <label><?php echo $this->lang->line('group'); ?></label>
                <select onchange="getStudentByClassShiftSection(document.getElementById('class_shift_section_id').value,this.value);"
                class="js-example-basic-single w-100" name="group_id" id="group_id" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php
                    $i = 0;
                    if (count($groups)) {
                        foreach ($groups as $list) {
                            $i++; ?>
                            <option
                                value="<?php echo $list['id']; ?>" <?php if ($c_group_id == $list['id']) {
                                echo 'selected';
                            } ?>><?php echo $list['name']; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label><?php echo $this->lang->line('student'); ?></label>
                <select onchange="getStudentInformationById(this.value)" class="js-example-basic-single w-100" name="student_id"
                 id="student_id" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php
                foreach ($students as $list) {
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>" <?php if ($c_student_id == $list['id']) {
                        echo 'selected';
                    } ?>><?php echo $list['roll_no'] .  '-' . $list['name']; ?></option>
                <?php
                }
                ?>
                </select>
            </div>



        </div>
        <div class="form-row">
          <div class="form-group col-md-3">
              <label><?php echo $this->lang->line('year'); ?></label>
              <select class="js-example-basic-single w-100" name="year" id="year" required="1">
                  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                  <?php
                  $i = 0;
                  if (count($years)) {
                      foreach ($years as $list) {
                          $i++; ?>
                          <option
                              value="<?php echo $list['value']; ?>"
                              <?php if (isset($c_year)) {
                              if ($c_year == $list['value']) {
                                  echo 'selected';
                              }
                          } ?>>
                          <?php echo $list['text']; ?>
                        </option>
                          <?php
                      }
                  }
                  ?>
              </select>
          </div>

            <div class="form-group col-md-3">
                <label>
                  <?php echo $this->lang->line('collection').' '.$this->lang->line('month'); ?>
                </label>
                <select class="js-example-basic-multiple w-100" name="collection_month" id="collection_month" required="1">
                    <?php
                    $i = 1;
                    while ($i <= 12) {
                        $dateObj = DateTime::createFromFormat('!m', $i); ?>
                        <option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
                            echo 'selected';
                        } ?>><?php echo $dateObj->format('F'); ?></option>
                        <?php
                        $i++;
                    }
                    ?>
                </select>
            </div>



            <div class="form-group col-md-4">
                <label><?php echo $this->lang->line('fees').' '.$this->lang->line('month'); ?></label>
                <select class="js-example-basic-multiple w-100"  multiple="multiple" name="month" id="month" required="1">
                    <?php
                    $i = 1;
                    while ($i <= 12) {
                        $dateObj = DateTime::createFromFormat('!m', $i); ?>
                        <option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
                            echo 'selected';
                        } ?>><?php echo $dateObj->format('F'); ?></option>
                        <?php
                        $i++;
                    }
                    ?>
                </select>

                <!-- select 2 note
                I'd like an easy way for users to add all matches. For example the user

                Enters "A"
                Presses CTRL+A or Meta+A all matches are selected
                Presses enter all matches are added
                !-->
            </div>

            <div class="form-group col-md-2">
              <img class="img" id="student_image" src="<?php echo base_url(); ?>/core_media/images/default.png" width="80" height="85">
            </div>
        </div>

        <div class="float-right">
            <a id="view_details_url" class="btn btn-success" href="javascript:void" onclick="feeStatusReportOpen()">
              Paid Details Report
            </a>
            <button type='button'  class="btn btn-primary" onclick="getCollectionSheet()"><?php echo $this->lang->line('process'); ?></button>
        </div>


      <div id="collection_form">

      </div>


</form>
