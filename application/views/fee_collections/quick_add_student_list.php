
<div class="table-responsive-sm">
    <table  class="table">
    <thead>
    <tr>
      <th scope="col"><?php echo $this->lang->line('student_code'); ?></th>
        <th scope="col"><?php echo $this->lang->line('roll'); ?></th>
        <th scope="col"><?php echo $this->lang->line('name'); ?></th>

        <th scope="col"><?php echo $this->lang->line('group'); ?></th>
        <th scope="col" class="text-center"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($students as $row):
        $i++;
        ?>

        <tr>
            <td><?php echo $row['student_code']; ?></td>
          <td><?php echo $row['roll_no']; ?></td>
            <td><?php echo $row['name']; ?></td>


            <td><?php echo $row['group_name']; ?></td>
            <td>
              <form target="_blank" class="form-inline" method="post" action="<?php echo base_url(); ?>fee_collections/quick_fee_collect">
                <div class="form-group">
                  <select style="width:220px !important;" class="js-example-basic-multiple w-100" multiple name="month[]" id="month" required="1">
                      <?php
                      $i = 1;
                      while ($i <= 12) {
                          $dateObj = DateTime::createFromFormat('!m', $i); ?>
                          <option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
                              echo 'selected';
                          } ?>><?php echo $dateObj->format('F'); ?></option>
                          <?php
                          $i++;
                      }
                      ?>
                  </select>
                </div>
				  &nbsp;
                <input type="hidden" name="student_id" value="<?php echo $row['id']; ?>">
                <input type="hidden" name="class_shift_section_id" value="<?php echo $class_shift_section_id; ?>">
                <input type="hidden" name="group_id" value="<?php echo $group_id; ?>">
				<input type="hidden" name="student_category_id" value="<?php echo $row['category_id']; ?>">
                <button type="submit" style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm">
                  <i class="ti-check-box"></i>
                </button>

              </form>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
