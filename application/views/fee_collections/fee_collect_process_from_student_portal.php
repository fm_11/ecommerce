<style>
	.for_fee_collection{
		padding: 5px;
	}
</style>
<script>
	function checkAllowPolicy(){
	   var agree_button = document.getElementById('agree_button');
	   if(agree_button.checked){
	   	   return true;
	   }else{
		   alert("Please allow term & conditions");
		   return false;
	   }
	}

	function checkAllForStudentPortal(ele) {
		//alert('ddd');
		//debugger;

		var checkboxes = document.querySelectorAll("input[type='checkbox']");
		if (ele.checked) {
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].type == 'checkbox') {
					if (checkboxes[i].id != 'agree_button') {
						checkboxes[i].checked = true;
					}
				}
			}
		} else {
			for (var i = 0; i < checkboxes.length; i++) {
				console.log(i)
				if (checkboxes[i].type == 'checkbox') {
					if (checkboxes[i].id != 'agree_button') {
						checkboxes[i].checked = false;
					}
				}
			}
		}
		calculateTotalPayableAmountForStudentPortal();
	}


	function calculateTotalPayableAmountForStudentPortal() {
		var start_row = 1;
		var row_num = Number(document.getElementById('num_of_row').value);
		var total_paid_amount = 0;
		while (start_row <= row_num) {
			var checkBox = document.getElementById('is_selected_' + start_row);
			if (checkBox.checked == true) {
				var paid_amount = document.getElementById('paid_amount_' + start_row).value;
				total_paid_amount = total_paid_amount + Number(paid_amount);
			}
			start_row++;
		}
		var serviceCharge = Math.ceil(total_paid_amount * 2 / 100);
		document.getElementById('total_paid_amount').value = total_paid_amount;
		document.getElementById('service_charge_amount').value = serviceCharge;
		document.getElementById('total_amount_with_service_charge').value = serviceCharge + total_paid_amount;
	}


</script>
<?php
if (isset($is_quick_data_save)) {
?>
<form name="addForm" class="cmxform" id="commentForm"  onsubmit="return checkAllowPolicy()"  action="<?php echo base_url(); ?>student_guardian_panels/fee_payment_data_save" method="post">
	<?php
	}
	?>
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<tr>
				<th scope="col"><?php echo $this->lang->line('sl'); ?></th>
				<th scope="col"><?php echo $this->lang->line('category'); ?></th>
				<th scope="col"><?php echo $this->lang->line('amount'); ?></th>
				<?php
				$payment_colspan = "";
				if($payment_gateway_config->payable_amount_editable == '1'){
					$payment_colspan = " colspan = '2' ";
					echo '<th scope="col"><input type="checkbox" id="fee_item_select" onchange="checkAllForStudentPortal(this)" name="chk[]"/></th>';
				}
				?>
			</tr>

			<?php
			$total_amount = 0;
			$i = 0;
			if (isset($allocated_list) && !empty($allocated_list)) {
				foreach ($allocated_list as $row) {
						$current_due_amount = $row['actual_allocated_amount_for_this_student'] - ($row['already_paid_amount'] + $row['already_discount_amount']);
						if ($current_due_amount > 0) {
							$i++; ?>

							<tr>
								<td>
									<?php echo $i; ?>
								</td>
								<td>
									<?php echo $row['sub_category_name']; ?>
									<input type="hidden" name="category_id_<?php echo $i; ?>"
										   value="<?php echo $row['category_id']; ?>">
									<input type="hidden" name="sub_category_id_<?php echo $i; ?>"
										   value="<?php echo $row['sub_category_id']; ?>">
									<input type="hidden" name="month_<?php echo $i; ?>" value="<?php echo $row['month']; ?>">

									<input type="hidden" name="actual_amount_<?php echo $i; ?>"
										   value="<?php echo $row['actual_allocated_amount_for_this_student']; ?>">
									<input type="hidden" name="already_paid_amount_<?php echo $i; ?>"
										   value="<?php echo $row['already_paid_amount']; ?>">
									<input type="hidden" name="waiver_amount_<?php echo $i; ?>"
										   value="<?php echo $row['waiver_amount']; ?>">
									<input type="hidden" autocomplete="off"  class="for_fee_collection" style="text-align: right;" size="12"
										   name="discount_amount_<?php echo $i; ?>" id="discount_amount_<?php echo $i; ?>"
										     value="0">
								</td>

								<td>
									<?php
									$total_amount += $row['actual_allocated_amount_for_this_student'] - ($row['already_paid_amount'] + $row['already_discount_amount']);
									?>
									<input type="text" readonly autocomplete="off"  class="for_fee_collection" style="text-align: right;" size="12"
										   name="paid_amount_<?php echo $i; ?>"  id="paid_amount_<?php echo $i; ?>"
										   value="<?php echo $row['actual_allocated_amount_for_this_student'] - ($row['already_paid_amount'] + $row['already_discount_amount']); ?>">
									<input type="hidden" autocomplete="off"  class="for_fee_collection" style="text-align: right;" readonly size="12" id="due_amount_<?php echo $i; ?>" value="0">
									<input type="hidden" class="for_fee_collection" style="text-align: right;" id="hidden_paid_amount_<?php echo $i; ?>"
										   value="<?php echo $row['actual_allocated_amount_for_this_student'] - ($row['already_paid_amount'] + $row['already_discount_amount']); ?>">
								</td>

								<?php
								 if($payment_gateway_config->payable_amount_editable == '1'){
								?>

								<td>
									<input type="checkbox" id="is_selected_<?php echo $i; ?>" onclick="calculateTotalPayableAmountForStudentPortal()" name="is_selected_<?php echo $i; ?>">
								</td>

								<?php } ?>

							</tr>
							<?php
						}
				}
			}
			?>

			<tr>
				<td  colspan="2" style="text-align:right;">
					<b>Sub <?php echo $this->lang->line('total'); ?></b>&nbsp;
					<input type="hidden" name="transport_fee_row" style="text-align: right;"
						   class="for_fee_collection" size="12" id="transport_fee_row" value="0">
					<input type="hidden" autocomplete="off"  class="for_fee_collection"  size="12" readonly style="text-align: right;" id="total_discount" name="total_discount" value="0">
				</td>
				<td <?php echo $payment_colspan; ?>>
					<input type="text" autocomplete="off"  class="for_fee_collection"  size="12" readonly style="text-align: right;" id="total_paid_amount" name="total_paid_amount" value="<?php echo $total_amount; ?>">
				</td>
			</tr>

			<?php
			//service charge calculation
			$service_charge_percentage = 2;
			$service_charge_amount = ceil($total_amount * $service_charge_percentage / 100);
			$total_paid_amount = $total_amount + $service_charge_amount;
			//service charge calculation
			?>

			<tr>
				<td  colspan="2" style="text-align:right;">
					Payment Gateway Charge (2%)
				</td>
				<td <?php echo $payment_colspan; ?>>
					<input type="text" autocomplete="off" class="for_fee_collection" id="service_charge_amount" size="12" readonly style="text-align: right;" value="<?php echo $service_charge_amount; ?>">
				</td>
			</tr>

			<tr>
				<td  colspan="2" style="text-align:right;">
					<b>Total</b>&nbsp;
				</td>
				<td <?php echo $payment_colspan; ?>>
					<input type="text" autocomplete="off" class="for_fee_collection" size="12" id="total_amount_with_service_charge" readonly style="text-align: right;" value="<?php echo $total_amount + $service_charge_amount; ?>">
				</td>
			</tr>

			<tr>
				<td colspan="<?php if($payment_gateway_config->payable_amount_editable == '1'){ echo 4; }else{ echo 3; } ?>">
					<div class="form-check" style="float: right;">
						<label class="form-check-label text-muted">
							<input id="agree_button" type="checkbox" class="form-check-input">
							Agree with all term & conditions
							<i class="input-helper"></i></label>
					</div>
				</td>
			</tr>

		</table>

		<input type="hidden" name="num_of_row" id="num_of_row" value="<?php echo $i; ?>">
		<input type="hidden" value="<?php echo date('Y-m-d'); ?>" autocomplete="off"
			   name="manual_collection_date" id="manual_collection_date" required>
		<input type="hidden" id="number_of_transport_fee_row" value="0">
		<div class="float-right">
			<a class="btn btn-warning" href="<?php echo base_url(); ?>student_guardian_panels/fees_payment">
				<?php echo $this->lang->line('cancel'); ?>
			</a>
			<input type="submit" class="btn btn-primary"  value="<?php echo $this->lang->line('payment'); ?>">
		</div>
	</div>
	<?php
	if (isset($is_quick_data_save)) {
	?>
	<input type="hidden" name="class_id" value="<?php echo $class_id; ?>"/>
	<input type="hidden" name="shift_id" value="<?php echo $shift_id; ?>"/>
	<input type="hidden" name="section_id" value="<?php echo $section_id; ?>"/>
	<input type="hidden" name="group_id" value="<?php echo $group_id; ?>"/>
	<input type="hidden" name="student_category_id" value="<?php echo $student_category_id; ?>"/>
	<input type="hidden" name="student_id" value="<?php echo $student_id; ?>"/>
	<input type="hidden" name="collection_month" value="<?php echo $collection_month; ?>"/>
	<input type="hidden" name="year" value="<?php echo $year; ?>"/>
	<div class="text-right mt-4 font-weight-light">
		<span>
			<a href="<?php echo base_url(); ?>download_zone/School360PaymentPolicy.pdf" target="_blank" class="text-primary">Check Terms & Conditions</a>
		</span>
	</div>
</form>
<?php
}
?>



