<script type="text/javascript">
	function getStudentByClassShiftSection(class_shift_section_id){
		//alert(class_shift_section_id);
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		}
		else
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				document.getElementById("student_id").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", "<?php echo base_url(); ?>students/getStudentByClassShiftSection?class_shift_section_id=" + class_shift_section_id, true);
		xmlhttp.send();
	}

	function approveConfirm() {
		var result = confirm("Are you sure to approve?");
		if (result == true) {
			return true;
		} else {
			return false;
		}
	}


</script>
<?php
$num_of_row_show = $this->session->userdata('num_of_row_show');
$receipt_no = $this->session->userdata('receipt_no');
$amount = $this->session->userdata('amount');
$name = $this->session->userdata('name');
$roll_no = $this->session->userdata('roll_no');
$from_date = $this->session->userdata('from_date');
$to_date = $this->session->userdata('to_date');
$class_shift_section_id = $this->session->userdata('class_shift_section_id');
$student_id = $this->session->userdata('student_id');
?>
<form name="addForm" class="cmxform" id="commentForm"  method="post" action="<?php echo base_url(); ?>fee_collections/payslip_index">
	<div class="form-row">
		<div class="form-group col-md-1">
			<input type="text" placeholder="Num of Row" style="padding: 10px !important;" autocomplete="off"
				   name="num_of_row_show" value="<?php if (isset($num_of_row_show)) {
				echo $num_of_row_show;
			} ?>" class="form-control" id="num_of_row_show">
		</div>

		<div class="form-group col-md-2">
			<input type="text" style="padding: 10px !important;" autocomplete="off"
				   name="roll_no" placeholder="Roll No." value="<?php if (isset($roll_no)) {
				echo $roll_no;
			} ?>" class="form-control" id="roll_no">
		</div>

		<div class="form-group col-md-3">
			<input type="text" style="padding: 10px !important;" autocomplete="off"
				   name="name" placeholder="Name/Student ID" value="<?php if (isset($name)) {
				echo $name;
			} ?>" class="form-control" id="name">
		</div>

		<div class="form-group col-md-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" style="padding: 10px !important;" placeholder="yyyy-mm-dd"  name="from_date" <?php if (isset($from_date)) { ?>
					value="<?php echo $from_date; ?>"
				<?php } ?> class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                 <i class="simple-icon-calendar"></i>
             </span>
			</div>
		</div>
		<div class="form-group col-md-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" style="padding: 10px !important;" placeholder="yyyy-mm-dd"  name="to_date" <?php if (isset($to_date)) { ?> value="<?php echo $to_date; ?>"
				<?php } ?>  class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                 <i class="simple-icon-calendar"></i>
             </span>
			</div>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-2">
			<input type="text" style="padding: 10px !important;" autocomplete="off"
				   name="receipt_no" placeholder="Receipt No." value="<?php if (isset($receipt_no)) {
				echo $receipt_no;
			} ?>" class="form-control" id="receipt_no">
		</div>

		<div class="form-group col-md-2">
			<input type="text" style="padding: 10px !important;" autocomplete="off"
				   name="amount" placeholder="Amount" value="<?php if (isset($amount)) {
				echo $amount;
			} ?>" class="form-control" id="amount">
		</div>

		<div class="form-group col-md-3">
			<select class="js-example-basic-single  w-100" onchange="getStudentByClassShiftSection(this.value)"
					name="class_shift_section_id" style="padding: 10px !important;" id="class_shift_section_id">
				<option value=""><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
							<?php if (isset($class_shift_section_id)) {
								if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<select class="js-example-basic-single w-100" style="padding: 10px !important;" name="student_id" id="student_id">
				<option value="">-- Please Select --</option>
				<?php
				foreach ($students as $list) {
					?>
					<option
						value="<?php echo $list['id']; ?>"<?php if (isset($student_id)) {
						if ($student_id == $list['id']) {
							echo 'selected';
						}
					} ?>><?php echo $list['name'] . ' (' . $list['roll_no'] . ')'; ?></option>
					<?php
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-2">
			<button type="submit" style="padding: 13px 30px 13px 30px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
		</div>
	</div>

</form>

<hr>

<div class="table-responsive-sm">
	<table id="sortable-table-1" style="width: 100% !important;" class="table">
		<thead>
		<tr>
			<th scope="col"><?php echo $this->lang->line('sl'); ?></th>
			<th scope="col"><?php echo $this->lang->line('receipt_no'); ?></th>
			<th scope="col"><?php echo $this->lang->line('student').' '.$this->lang->line('name'); ?></th>
			<th scope="col"><?php echo $this->lang->line('roll'); ?></th>
			<th scope="col"><?php echo $this->lang->line('class'); ?>-Shift-Section</th>
			<th scope="col"><?php echo $this->lang->line('month') . ', ' . $this->lang->line('year'); ?></th>
			<th scope="col">Amount</th>
			<th scope="col"><?php echo $this->lang->line('actions'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = (int)$this->uri->segment(3);
		$total_amount = 0;
		foreach ($fees as $row):
			$i++;
			?>
			<tr>

			<tr>
				<td>
					<?php echo $i; ?>
				</td>
				<td><?php echo $row['receipt_no']; ?></td>
				<td><?php echo $row['student_name']; ?></td>
				<td><?php echo $row['roll_no']; ?></td>
				<td><?php echo $row['class_name'] . '-' . $row['shift_name'] . '-' . $row['section']; ?></td>
				<td><?php echo DateTime::createFromFormat('!m', $row['month'])->format('F') . ', ' . $row['year']; ?></td>
				<td>
					<?php
					$total_amount += $row['total_paid_amount'];
					echo $row['total_paid_amount'];
					?>
				</td>
				<td>
					<div class="dropdown">
						<button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="ti-pencil-alt"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">

							<a class="dropdown-item" onclick="return approveConfirm()" href="<?php echo base_url(); ?>fee_collections/payslip_approve/<?php echo $row['id']; ?>"
							   title="Payslip Approve">Approve</a>

							<a class="dropdown-item" href="<?php echo base_url(); ?>fee_collections/view_payslip/<?php echo $row['id']; ?>"
							   target="_blank" title="Payslip View">View</a>

							<a class="dropdown-item" onclick="return deleteConfirm()"
							   href="<?php echo base_url(); ?>fee_collections/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
						</div>
					</div>

				</td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="6" align="right"><b>Total</b></td>
			<td><b><?php echo number_format($total_amount,2); ?></b></td>
			<td>&nbsp;</td>
		</tr>
		</tbody>
	</table>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>
