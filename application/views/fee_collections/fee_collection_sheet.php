<style>
  .for_fee_collection{
    padding: 5px;
  }
</style>
<?php
if (isset($is_quick_data_save)) {
    ?>
    <form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>fee_collections/add" method="post">
<?php
}
?>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <tr>
        <th scope="col" width="90px"><?php echo $this->lang->line('sl'); ?></th>
        <th scope="col" width="150px"><?php echo $this->lang->line('sub').' '.$this->lang->line('category'); ?></th>
        <th scope="col"><?php echo $this->lang->line('actual_amount').' ('.$this->lang->line('after').' '.$this->lang->line('waiver').' )'; ?></th>
        <th scope="col"><?php echo $this->lang->line('already').' '.$this->lang->line('paid').' '.$this->lang->line('amount'); ?></th>
        <th scope="col"><?php echo $this->lang->line('waiver').' '.$this->lang->line('amount'); ?></th>
        <th scope="col"><?php echo $this->lang->line('already').' '.$this->lang->line('discount').' '.$this->lang->line('amount'); ?></th>
        <th scope="col"><?php echo $this->lang->line('discount'); ?></th>
        <th scope="col"><?php echo $this->lang->line('payable').' '.$this->lang->line('amount'); ?></th>
        <th scope="col"><?php echo $this->lang->line('due').' '.$this->lang->line('amount'); ?></th>
        <th scope="col">
            <input type="checkbox" id="fee_item_select" onchange="checkAll(this)" name="chk[]"/>
        </th>
    </tr>

    <?php
        $total_amount = 0;
        $i = 0;
        if (isset($allocated_list) && !empty($allocated_list)) {
        foreach ($allocated_list as $row) {
                $current_due_amount = $row['actual_allocated_amount_for_this_student'] - ($row['already_paid_amount'] + $row['already_discount_amount']);
                if ($current_due_amount > 0) {
                    $i++; ?>

            <tr>
                <td>
                    <?php echo $i; ?>
                </td>
                <td>
                    <b><?php echo $row['sub_category_name']; ?></b>
                    <input type="hidden" name="category_id_<?php echo $i; ?>"
                           value="<?php echo $row['category_id']; ?>">
                    <input type="hidden" name="sub_category_id_<?php echo $i; ?>"
                           value="<?php echo $row['sub_category_id']; ?>">
                    <input type="hidden" name="month_<?php echo $i; ?>" value="<?php echo $row['month']; ?>">
                </td>
                <td>
                    <?php echo $row['actual_allocated_amount_for_this_student']; ?>
                    <input type="hidden" name="actual_amount_<?php echo $i; ?>"
                           value="<?php echo $row['actual_allocated_amount_for_this_student']; ?>">
                </td>
                <td>
                    <?php echo $row['already_paid_amount']; ?>
                    <input type="hidden" name="already_paid_amount_<?php echo $i; ?>"
                           value="<?php echo $row['already_paid_amount']; ?>">
                </td>


                <td>
                    <?php echo $row['waiver_amount']; ?>
                    <input type="hidden" name="waiver_amount_<?php echo $i; ?>"
                           value="<?php echo $row['waiver_amount']; ?>">
                </td>

                <td>
                    <?php echo $row['already_discount_amount']; ?>
                </td>


                <td>
                    <input type="text" autocomplete="off"  class="for_fee_collection" style="text-align: right;" size="12"
                           name="discount_amount_<?php echo $i; ?>" id="discount_amount_<?php echo $i; ?>"
                         onkeyup="calculate_paidable_amount_after_discount(this.value,<?php echo $i; ?>)"  value="0">
                </td>


                <td>
                    <input type="text" autocomplete="off"  class="for_fee_collection" style="text-align: right;" size="12"
                          onkeyup="dueAmountCalculate('<?php echo $i; ?>')" name="paid_amount_<?php echo $i; ?>"  id="paid_amount_<?php echo $i; ?>"
                           value="<?php echo $row['actual_allocated_amount_for_this_student'] - ($row['already_paid_amount'] + $row['already_discount_amount']); ?>">


                    <input type="hidden" class="for_fee_collection" style="text-align: right;" id="hidden_paid_amount_<?php echo $i; ?>"
                   value="<?php echo $row['actual_allocated_amount_for_this_student'] - ($row['already_paid_amount'] + $row['already_discount_amount']); ?>">
                </td>

                <td>
                    <input type="text" autocomplete="off"  class="for_fee_collection" style="text-align: right;" readonly size="12" name="due_amount_<?php echo $i; ?>"  id="due_amount_<?php echo $i; ?>" value="0">
                </td>


                <td>
                    <input type="checkbox" id="is_selected_<?php echo $i; ?>" onclick="calculateTotalPayableAmount()" name="is_selected_<?php echo $i; ?>">
                </td>

            </tr>
    <?php
                }
        }
        }
    ?>


    <?php
    $transport_fee_row = 0;
    $number_of_transport_fee_row = 0;
    if ($is_transport_fee_applicable == '1' && !empty($transport_fees)) {
        ?>

     <tr>
            <th colspan="10">
               <?php echo $this->lang->line('transport').' '.$this->lang->line('fees'); ?>
            </th>
     </tr>

    <?php
       foreach ($transport_fees as $transport_row):
       $number_of_transport_fee_row++;
        $i++; ?>

    <tr>
        <td>
                    <?php echo $i; ?>
        </td>
        <td>
            <b><?php echo $transport_row['title']; ?></b>
        </td>

        <td>
            <?php echo $transport_row['actual_amount']; ?>
        </td>

        <td>
            <?php echo $transport_row['alreay_paid_transport_fee']; ?>
        </td>


        <td colspan="3">
            0
        </td>

        <td>
            <input type="hidden" name="transport_fee_month_<?php echo $transport_fee_row; ?>"
                           value="<?php echo $transport_row['month']; ?>">

           <input type="text" autocomplete="off"  readonly class="for_fee_collection" style="text-align: right;"
            size="12" name="paid_transport_fee_<?php echo $transport_fee_row; ?>" id="paid_amount_<?php echo $i; ?>"
            value="<?php echo($transport_row['actual_amount'] - $transport_row['alreay_paid_transport_fee']); ?>">
        </td>

        <td style="vertical-align:middle" colspan="2">
            <input type="checkbox" name="transport_fee_selected_<?php echo $transport_fee_row; ?>" id="is_selected_<?php echo $i; ?>"
            onclick="calculateTotalPayableAmount()">
        </td>

    </tr>



    <?php
      $transport_fee_row++;
        endforeach;
    }
    ?>


    <tr>
            <td colspan="6" style="text-align:right;">
            <b><?php echo $this->lang->line('total'); ?></b>&nbsp;
             </td>
            <td>
                <input type="hidden" name="transport_fee_row" style="text-align: right;"
                class="for_fee_collection" size="12" id="transport_fee_row" value="<?php echo $transport_fee_row; ?>">
                <input type="text" autocomplete="off"  class="for_fee_collection"  size="12" readonly style="text-align: right;" id="total_discount" name="total_discount" value="0">
            </td>
            <td colspan="3">
                <input type="text" autocomplete="off"  class="for_fee_collection"  size="12" readonly style="text-align: right;" id="total_paid_amount" name="total_paid_amount" value="0">
            </td>
    </tr>


    <tr>
        <th colspan="10"><?php echo $this->lang->line('payment').' '.$this->lang->line('information'); ?></th>
    </tr>

    <tr>
        <td colspan="5">
            <?php echo $this->lang->line('do_you_collect_late_fee'); ?> <input type="checkbox" id="do_you_collect_late_fee" name="do_you_collect_late_fee">
        </td>
        <td colspan="3" style="text-align:right;">
           <?php echo $this->lang->line('late_fee'); ?>
        </td>
        <td  colspan="2">
            <input type="text" autocomplete="off"  name="late_fee" style="text-align: right;" class="for_fee_collection" size="12" id="late_fee" value="0">
        </td>
    </tr>


    <tr>
        <td colspan="2">
            <?php echo $this->lang->line('payment_method'); ?>
            <select class="for_fee_collection" name="mode_of_pay" id="mode_of_pay" required="1">
                <option value="C">  <?php echo $this->lang->line('cash'); ?></option>
            </select>
        </td>


        <td  colspan="3">
            <?php echo $this->lang->line('receipt_no'); ?>
            <input type="text" autocomplete="off"  name="receipt_no" class="for_fee_collection"
             size="12" value="<?php echo $receipt_no; ?>">
        </td>

        <td colspan="3">
         <?php echo $this->lang->line('do_you_want_send_sms'); ?>

         <input type="checkbox" id="do_you_want_send_sms"
         <?php if ($general_config->fee_sms_send == '1') {
        echo 'checked';
    } ?> name="do_you_want_send_sms">
        </td>

        <td colspan="2" style="vertical-align: middle;" align="right">
           <?php echo $this->lang->line('do_you_want_receipt').' ?'; ?>
           <input type="checkbox" id="do_you_want_receipt"
           <?php if ($general_config->fee_receipt_checked == '1') {
          echo 'checked';
      } ?>  name="do_you_want_receipt">
            <input type="hidden" name="num_of_row" id="num_of_row" value="<?php echo $i; ?>">
        </td>
    </tr>

    <tr>
       <td  colspan="6" align="left">&nbsp;</td>
       <td  colspan="4" align="left">
         <div class="form-group col-md-12">
           <label>Collection Date</label>
           <div class="input-group date">
                   <input type="text" value="<?php echo date('Y-m-d'); ?>" autocomplete="off"
                    name="manual_collection_date" id="manual_collection_date" required class="form-control">
                   <span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
           </div>
         </div>
       </td>
    </tr>

</table>

<input type="hidden" id="number_of_transport_fee_row" value="<?php echo $number_of_transport_fee_row; ?>">
<div class="float-right">
  <input type="submit" class="btn btn-warning" onclick="return payslipConfirm();" name="make_payslip" value="Make Payslip">
  <input type="submit" class="btn btn-primary" name="submit" value="<?php echo $this->lang->line('submit'); ?>">
</div>
</div>

<input type="hidden" name="student_category_id" value="<?php echo $student_category_id; ?>"/>
<?php
if (isset($is_quick_data_save)) {
        ?>
        <input type="hidden" name="class_shift_section_id" value="<?php echo $class_shift_section_id; ?>"/>
        <input type="hidden" name="group_id" value="<?php echo $group_id; ?>"/>
        <input type="hidden" name="student_id" value="<?php echo $student_id; ?>"/>
        <input type="hidden" name="collection_month" value="<?php echo $collection_month; ?>"/>
        <input type="hidden" name="year" value="<?php echo $year; ?>"/>
  </form>
<?php
    }
?>
