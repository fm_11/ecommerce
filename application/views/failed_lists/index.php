<script>
	function getExamByYear(year){
		//alert(year);
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		}
		else
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				//alert(xmlhttp.responseText);
				document.getElementById("exam_id").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", "<?php echo base_url(); ?>exams/getExamByYear?year=" + year, true);
		xmlhttp.send();
	}
</script>
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>failed_lists/index" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Year</label>
			<select class="js-example-basic-single w-100" onchange="getExamByYear(this.value)" name="year" id="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['value']; ?>"
							<?php if (isset($year)) {
								if ($year == $list['value']) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['text']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('exam'); ?></label>
			<select name="exam_id" id="exam_id" class="js-example-basic-single w-100" required="required">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php for ($A = 0; $A < count($exam_list); $A++) { ?>
					<option value="<?php echo $exam_list[$A]['id']; ?>"
						<?php if (isset($exam_id)) {
							if ($exam_id == $exam_list[$A]['id']) {
								echo 'selected';
							}
						} ?>><?php echo $exam_list[$A]['name']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
			<select class="js-example-basic-single w-100"
					name="class_shift_section_id" id="class_shift_section_id" required>
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
							<?php if (isset($class_shift_section_id)) {
								if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="btn-group float-right">
		<input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
		<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
		<input type="submit" class="btn btn-primary" value="View Report">
	</div>
</form>

<?php
if(isset($failed_data)){
	echo $report;
}
?>
