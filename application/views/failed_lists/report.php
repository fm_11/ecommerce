<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;

		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
			text-align: center;
		}
	</style>

	<?php
} ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
		<thead>
		<tr>
			<td colspan="5"  class="h_td" style="text-align:center; line-height:1.5;">
				<b style="font-size: 18px;"><?php echo $HeaderInfo[0]['school_name']; ?></b><br>
				<span style="font-size: 13px;">
					<?php echo $HeaderInfo[0]['address']; ?><br>
					<?php echo $title; ?></b>
				</span>
			</td>
		</tr>
		<tr>
          <td colspan="5" scope="col" style="text-align:left;padding: 10px" class="h_td">
			  Class : <b><?php echo $class_shift_section_name; ?></b> ,
			  Year : <b><?php echo $year; ?></b> ,
			  Exam : <b><?php echo $exam_info[0]['name']; ?></b>
		  </td>
		</tr>
		<tr>
			<td scope="col" style="text-align:center;" class="h_td"><b>Student ID</b></td>
			<td scope="col" style="text-align:center;" class="h_td"><b>Roll No.</b></td>
			<td scope="col" style="text-align:center;" class="h_td"><b>Name</b></td>
			<td scope="col" style="text-align:center;" class="h_td"><b>Number of Subjects</b></td>
			<td scope="col" style="text-align:center;" class="h_td"><b>Failed Subjects</b></td>
		</tr>
		</thead>
		<tbody>

		<?php
		foreach ($failed_data as $arow):
			?>
			<tr>
				<td align="center">
					<?php
					echo $arow['student_code']; ?>
				</td>
				<td align="center">
					<?php
					echo $arow['roll_no']; ?>
				</td>
				<td align="left">
					<?php
					echo $arow['name'];
					?>
				</td>
				<td align="center">
					<?php
					$num_of_subjects = explode(",",$arow['failed_subjects']);
					echo count($num_of_subjects);
					?>
				</td>
				<td align="left">
					<?php
					echo $arow['failed_subjects']; ?>
				</td>
			</tr>
		<?php
		endforeach; ?>
		</tbody>
	</table>
</div>
