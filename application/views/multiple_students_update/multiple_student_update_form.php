<script>
    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }
</script>

<style>
	.custom_cls_for_this{
		padding: 6px; width: 100px;border:1px #737373 solid;
	}
</style>

<form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()"
      action="<?php echo base_url(); ?>multiple_students_update/data_save" method="post">
      <div class="table-sorter-wrapper col-lg-12 table-responsive">
        <table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
            <th scope="col">Name</th>
            <th scope="col">Roll</th>
            <th scope="col">Father's Name</th>
            <th scope="col">Mother's Name</th>
            <th scope="col">Section</th>
            <th scope="col">Group</th>
			<th scope="col">Category</th>
            <th scope="col">Shift</th>
            <th scope="col">Gender</th>
            <th scope="col">Blood Group</th>
            <th scope="col">Religion</th>
            <th scope="col">Mobile</th>
            <th scope="col">Process Code</th>
            <?php
                if ($is_transport_fee_applicable != '0') {
                    ?>
            <th scope="col">Transport Fee</th>
            <?php
                }
            ?>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($students as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td>
                    <input type="checkbox" name="is_allow_<?php echo $i; ?>">
                </td>
                <td>
                    <input type="text" style="width: 170px !important;" autocomplete="off"  class="custom_cls_for_this"
                           name="name_<?php echo $i; ?>" value="<?php echo $row['name']; ?>"/>
                    <input type="hidden" class="input-text-short"
                           name="id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <input style="width: 50px !important;" type="text" autocomplete="off"  class="custom_cls_for_this"
                           name="roll_no_<?php echo $i; ?>" value="<?php echo $row['roll_no']; ?>"/>
                </td>

                <td>
                    <input  style="width: 170px !important;" type="text" autocomplete="off"  class="custom_cls_for_this"
                           name="father_name_<?php echo $i; ?>" value="<?php echo $row['father_name']; ?>"/>
                </td>
                <td>
                    <input  style="width: 170px !important;" type="text" autocomplete="off"  class="custom_cls_for_this"
                           name="mother_name_<?php echo $i; ?>" value="<?php echo $row['mother_name']; ?>"/>
                </td>
                <td>
                    <select name="section_id_<?php echo $i; ?>" class="custom_cls_for_this">
                        <option value="">-- Select --</option>
                        <?php foreach ($section as $row1) { ?>
                            <option value="<?php echo $row1['id']; ?>"<?php if ($row1['id'] == $row['section_id']) {
                echo 'selected';
            } ?>><?php echo $row1['name']; ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="group_<?php echo $i; ?>" class="custom_cls_for_this">
                        <option value="">-- Select --</option>
                        <?php foreach ($group_list as $row3) { ?>
                            <option value="<?php echo $row3['id']; ?>"<?php if ($row3['id'] == $row['group']) {
                echo 'selected';
            } ?>><?php echo $row3['name']; ?></option>
                        <?php } ?>
                    </select>
                </td>

			<td>
				<select class="custom_cls_for_this" name="category_id_<?php echo $i; ?>" id="category_id_<?php echo $i; ?>">
					<option value="">-- Select --</option>
					<?php
					if (count($categories)) {
						foreach ($categories as $list) {
							?>
							<option
									value="<?php echo $list['id']; ?>"
									<?php if(isset($row['category_id'])){ if ($row['category_id'] == $list['id']) {
										echo 'selected';
									}} ?>><?php echo $list['name']; ?></option>
							<?php
						}
					}
					?>
				</select>
			</td>


                <td>
                    <select name="shift_<?php echo $i; ?>" class="custom_cls_for_this">
                        <option value="">-- Select --</option>
                        <?php foreach ($shift as $row3) { ?>
                            <option value="<?php echo $row3['id']; ?>"<?php if ($row3['id'] == $row['shift_id']) {
                echo 'selected';
            } ?>><?php echo $row3['name']; ?></option>
                        <?php } ?>
                    </select>
                </td>

                <td>
                    <select name="gender_<?php echo $i; ?>" class="custom_cls_for_this">
                        <option value="">-- Select --</option>
                        <option value="M" <?php if (isset($row['gender'])) {
                if ($row['gender'] == 'M') {
                    echo 'selected';
                }
            } ?>>Male
                        </option>
                        <option value="F" <?php if (isset($row['gender'])) {
                if ($row['gender'] == 'F') {
                    echo 'selected';
                }
            } ?>>Female
                        </option>
                    </select>
                </td>
                <td>
                    <select name="blood_group_id_<?php echo $i; ?>" class="custom_cls_for_this">
                        <option value="">-- Blood Group --</option>
                        <?php foreach ($blood_group_list as $row2) { ?>
                            <option value="<?php echo $row2['id']; ?>" <?php if (isset($row['blood_group_id'])) {
                if ($row2['id'] == $row['blood_group_id']) {
                    echo 'selected';
                }
            } ?>><?php echo $row2['name']; ?></option>

                        <?php } ?>
                    </select>
                </td>
                <td>
                    <select name="religion_<?php echo $i; ?>"  class="custom_cls_for_this">
                        <option value="">-- Select --</option>
						<?php foreach ($religions as $row6) { ?>
							<option value="<?php echo $row6['id']; ?>" <?php if (isset($row['religion'])) {
								if ($row6['id'] == $row['religion']) {
									echo 'selected';
								}
							} ?>><?php echo $row6['name']; ?></option>

						<?php } ?>

                    </select>
                </td>
                <td>
                    <input style="width: 110px !important;" type="text" autocomplete="off"  class="custom_cls_for_this"
                           name="guardian_mobile_<?php echo $i; ?>" value="<?php echo $row['guardian_mobile']; ?>"/>
                </td>
                <td>
                    <input type="text" autocomplete="off"  class="custom_cls_for_this"
                           name="process_code_<?php echo $i; ?>" value="<?php echo $row['process_code']; ?>"/>
                </td>


                <?php
                    if ($is_transport_fee_applicable != '0') {
                        ?>
                    <td>
                        <input type="text" autocomplete="off"  class="custom_cls_for_this"
                               name="transport_fee_amount_<?php echo $i; ?>" value="<?php echo $row['transport_fee_amount']; ?>"/>
                    </td>
                <?php
                    }
                ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
  </div>
    <input type="hidden" class="custom_cls_for_this" name="loop_time"
           value="<?php echo $i; ?>"/>
   <div class="float-right">
      <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
   </div>
   </form>
