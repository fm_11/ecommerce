<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>salary_types/add" method="post" enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="inputEmail4">Name</label>
                                      <input type="text" autocomplete="off"  class="form-control" id="name" required name="name" value="" placeholder="Salary Type Name">
                                   </div>
                                      <div class="form-group col-md-6">
                                          <label for="inputshort_name">Short Name</label>
                                          <input type="text" autocomplete="off"  class="form-control" id="short_name" required
                                              name="short_name" value="">
                                      </div>
                                  </div>

                              <div class="form-row">
                                  <div class="form-group col-md-6">
                                      <label for="txtType">Type</label>
                                      <select class="form-control select2-single"  name="type" required>
                                          <option value="A">Allowance</option>
                                          <option value="D">Deduction</option>
                                      </select>
                                  </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputpercentage_of_gross">Percentage of Gross</label>
                                        <input type="text" autocomplete="off"  class="form-control" id="percentage_of_gross" required
                                            name="percentage_of_gross" value="">
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="rounding_type">Rounding Type</label>
                                        <select class="form-control select2-single"  name="rounding_type" required>
                                            <option value="R">Round</option>
                                            <option value="C">Ceiling</option>
                                            <option value="F">Floor</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                          <label for="report_order">Report Order</label>
                                          <input type="text" autocomplete="off"  class="form-control" required id="report_order" name="report_order"
                                              value="">
                                      </div>
                                </div>



                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
</div>
</div>
</div>
