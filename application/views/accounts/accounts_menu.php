<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <?php
    if($accounts_module_type == "B"){
    ?>
        <li><a href="<?php echo base_url(); ?>accounts/get_income_category"><span>Income Categories</span></a></li>
        <li><a href="<?php echo base_url(); ?>accounts/get_expense_category"><span>Expense Categories</span></a></li>
        <li><a href="<?php echo base_url(); ?>accounts/get_all_deposit"><span>Add Deposit</span></a></li>
        <li><a href="<?php echo base_url(); ?>accounts/get_all_expense"><span>Add Expense</span></a></li>
        <li><a href="<?php echo base_url(); ?>accounts/get_all_fund_transfer_info"><span>Fund Transfer</span></a></li>
    <?php
    }else{
    ?>
       
    <?php
    }
    ?>

</ul>

