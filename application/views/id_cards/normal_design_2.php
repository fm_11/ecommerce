<!DOCTYPE html>
<html>
<head>
<title><?php echo $title; ?></title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    window.print();
</script>
<style>

@media print {

	.hr_class {
		background-color: #fff;
		border-top: 2px dashed <?php echo $id_card_config->main_color; ?>;
		margin-top: 12px;
		min-width: 100% !important;
	}

	.footer_main {
		text-align:right;
		margin-top: -7px;
		bottom: 40px;
		position: absolute;
		min-width: 79%;
		height: 38px;
	}

	.header img {
		width: 46px;
		margin-top: -59px;
		margin-left: -22px;
		height: 63px;
	}

  .rows-print-as-pages .row {
    page-break-before: always;
  }

	* {
		-webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
		color-adjust: exact !important;                 /*Firefox*/
	}

  /* include this style if you want the first row to be on the same page as whatever precedes it */
  /*
  .rows-print-as-pages .row:first-child {
    page-break-before: avoid;
  }
  */
}
@page { size: A4 landscape }
  .id-card {
    width: 225px;
    height: 305px;
    background-color: #fff;
    text-align: center;
    margin: 5px;
    border: 3px solid <?php echo $id_card_config->main_color; ?>;
    border-left: 25px solid <?php echo $id_card_config->main_color; ?>;
  }
  .id-card img {
    margin: 0 auto;
  }
  .header img {
    width: 46px;
      margin-top: -59px;
      margin-left: -22px;
      height: 63px;
  }
  .photo img {
    width: 35%;
margin-top: 15px;
margin-bottom: -7px;
border-radius: 50%;
  }
  h2 {
    font-size: 12px;
    margin-bottom: -19px;
    margin-top: 2px;
    color: #00001a;
    font-family: "Arial Black", Gadget, sans-serif;

}
  h3 {
    font-size: 13px;

  }
  .qr-code img {
    width: 23%;
  }
  p {
    font-size: 11px;
    color:#00001a;
    font-family: "Trebuchet MS", Helvetica, sans-serif;
  }
  .identity{
    background: #0c43a7;
      transform-origin: 0 0;
      position: relative;
      transform: rotate(270deg);
      margin-top: 8px;
      font-size: 10px;
      width: 85px;
      height: 20px;
      color: white;
      padding-top: 9px;
  }

  .studentid{
    margin-top: 5px;
  font-size: 12px;
  font-weight: bold;
  }
  .student_info{
margin-top: 10px;

  }

  .student_info-left {
font-size: 15px;
  }

  .student_info_table {
    margin-top: -7px;
        line-height: 12px;
        text-align: left;
        color:<?php echo $id_card_config->secondary_text_color; ?>;
  }

    table, td, th {
      font-size: 10px;
	  margin: auto;
    }
    .sheet {
        background: white;
        box-shadow: 0 0 0 rgba(0,0,0,0);
        margin: 5mm auto;
    }
    .photo{

      margin-top: -18px;
    }
    .page_breack {
        page-break-after: always;

    }


    .printButton:link, .printButton:visited {
          background-color: #f44336;
          color: white;
          padding: 8px 8px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
    }

    .printButton:hover, .printButton:active {
      background-color: red;
    }

    .backToButton:link, .backToButton:visited {
          background-color: #a3c2c2;
          color: white;
          padding: 8px 8px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
    }

    .backToButton:hover, .backToButton:active {
      background-color: #527a7a;
    }

.hr_class {
	background-color: #fff;
	border-top: 2px dashed <?php echo $id_card_config->main_color; ?>;
	margin-top: 12px;
}

.footer_main {
	text-align:right;
	margin-top: -7px;
	bottom: 40px;
	position: absolute;
	width: 69%;
	height: 38px;
}

</style>


</head>

<body>


<div class="container rows-print-as-pages" id="printableArea">
<div class="row">
  <?php
   $i = 1;
  foreach ($list as $row):
      //echo '<pre>';
      //print_r($row); die;

      ?>
    <div class="form-group col-md-3" style="margin-bottom: 0px !important">
    <section class="sheet">
  		<div class="id-card-holder">
  			<div class="id-card">
			  <h2 style="color:<?php echo $id_card_config->main_text_color; ?>; width: 80%; margin: 0px auto;margin-top: 2px;">
				  <span style="font-size: 13px;"><?php echo $school_info[0]['school_name']; ?></span><br>
				  <span style="font-size: 11px;color: <?php echo $id_card_config->secondary_text_color; ?> !important;"><?php echo $school_info[0]['address']; ?></span>
			  </h2>

  				<div class="header" style="position: absolute;">
  					<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info[0]['picture']; ?>">
  			    </div>
			  <div class="studentid">
				  <h3 style="color: <?php echo $id_card_config->secondary_text_color; ?> !important;">ID :<b> <?php echo $row['student_code']; ?></b></h3>
			  </div>
  				<div class="photo">
  					<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/<?php if ($row['photo']!='') {
          echo 'student/'.$row['photo'];
      } else {
          echo 'img/not_found.jpg';
      } ?>">

  				</div>

          <!-- <div class="identity">IDENTITY CARD</div>
          <div class="studentid">ID : <?php echo $row['student_code']; ?></div> -->
          <div class="student_info">
            <h3 style="text-align: center;color:<?php echo $id_card_config->main_text_color; ?>;"><b><?php echo $row['name']; ?></b><h3>
            <table class="student_info_table">

				<?php if($id_card_config->fathers_name_show == '1'){ ?>
					<tr>
						<td>Father. </td>
						<td>: <?php echo $row['father_name']; ?></td>
					</tr>
				<?php } ?>

				<?php if($id_card_config->mothers_name_show == '1'){ ?>
					<tr>
						<td>Mother. </td>
						<td>: <?php echo $row['mother_name']; ?></td>
					</tr>
				<?php } ?>

				<?php if($id_card_config->date_of_birth_show == '1'){ ?>
					<tr>
						<td>DOB </td>
						<td>: <?php echo date("d-m-Y", strtotime($row['date_of_birth'])); ?></td>
					</tr>
				<?php } ?>


			 <?php if($id_card_config->class_show == '1'){ ?>
				 <tr>
					 <td>Class  </td>
					 <td>: <?php echo $row['class_name']; ?></td>
				 </tr>
			<?php } ?>

			<?php if($id_card_config->section_show == '1'){ ?>
				 <tr>
					 <td>Section  </td>
					 <td>: <?php echo $row['section_name']; ?></td>
				 </tr>
			<?php } ?>

				<?php if($id_card_config->group_show == '1'){ ?>
             <tr>
             <td>Group  </td>
             <td>: <?php echo $row['group_name']; ?></td>
             </tr>

				<?php } ?>

				<?php if($id_card_config->shift_show == '1'){ ?>
             <tr>
             <td>Shift  </td>
             <td>: <?php echo $row['shift_name']; ?></td>
             </tr>
				<?php } ?>

				<?php if($id_card_config->roll_show == '1'){ ?>
             <tr>
             <td>Roll No.  </td>
             <td>: <?php echo $row['roll_no']; ?></td>
             </tr>
				<?php } ?>

				<?php if($id_card_config->year_show == '1'){ ?>
             <tr>
             <td>Year  </td>
             <td>: <?php echo date("Y"); ?></td>
             </tr>
				<?php } ?>

				<?php if($id_card_config->mobile_show == '1'){ ?>
             <tr>
             <td>Mobile  </td>
             <td>: <?php echo $row['guardian_mobile']; ?></td>
             </tr>
				<?php } ?>

              </table>
          </div>

				<div class="footer_main">
					<hr class="hr_class">

					<?php  if (isset($signature['L'])) { ?>
  				    <p style="margin-top: -20px; margin-right: 7px;">
                <!-- <b>&nbsp;</b><br> -->
                <?php if ($signature['L']['is_use'] == '1') { ?>
                   <img style="width: 98px; height: 30px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['L']['location']; ?>">

                <?php }
           } ?>
  				    <br>
					<p style="margin-right: 7px;border-top: 1px <?php echo $id_card_config->main_color; ?> solid; width:50%; float: right; margin-top: -22px;">
                <b>
                  <?php if (isset($signature['L'])) {
               echo $signature['L']['level'];
           }
                   ?>
                  &nbsp;
               </b>
  				</div>
  			</div>
  	    </div>
    </section>

    </div>
    <?php
    if ($i % 8 == 0) {
        echo '</div><div class="row">';
    }
    $i++;  endforeach; ?>
</div>
</div>
</body>
</html>
