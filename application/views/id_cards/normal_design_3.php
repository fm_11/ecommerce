<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script>
		function printDiv(divName) {
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
		}

		window.print();
	</script>
	<style>

		@media print {
			.identity{
				background-color: <?php echo $id_card_config->main_color; ?> !important;
				-webkit-print-color-adjust: exact;
				color: white;
			}
			.address{
				background-color: <?php echo $id_card_config->main_color; ?> !important;
				-webkit-print-color-adjust: exact;
				color: white;
			}
			.studentid{
				background-color: <?php echo $id_card_config->main_color; ?> !important;
				-webkit-print-color-adjust: exact;
				color: white;
			}
			.rows-print-as-pages .row {
				page-break-before: always;
			}
			/* include this style if you want the first row to be on the same page as whatever precedes it */
			/*
			.rows-print-as-pages .row:first-child {
			  page-break-before: avoid;
			}
			*/
		}
		@page { size: A4 landscape }

		.id-card {
			width: 225px;
			height: 305px;
			background-color: #fff;
			text-align: center;
			margin: 5px;
			border: 2px solid #142975;
			border-radius: 10px;
			margin-top: -352px;
		}
		.id-card img {
			margin: 0 auto;
		}
		.header img {
			width: 21%;
			margin-top: 21px;
			margin-bottom: -17px;
		}
		.photo img {
			width: 34%;
			height: 72px;
			margin-top: -43px;
			border-radius: 5px;
		}
		h2 {
			font-size: 12px;

			color: #00001a;

			font-family: "Arial Black", Gadget, sans-serif;
		}
		h3 {
			font-size: 13px;
			font-weight: 400;
			/* font-family: "Angsana New", Monaco, monospace; */
			margin-top: 10px;
			margin-bottom: -8px;
			color:#003300;
		}
		.qr-code img {
			width: 23%;
		}
		p {
			font-size: 11px;
			color:#00001a;
			font-family: "Trebuchet MS", Helvetica, sans-serif;
		}
		.identity{
			background: <?php echo $id_card_config->main_color; ?>;
			transform-origin: 0 0;
			position: relative;
			transform: rotate(270deg);
			font-size: 12px;
			width: 73px;
			/* height: 33px; */
			color: white;
			padding-top: 4px;
		}

		.studentid{
			position: absolute;
			transform-origin: 0;
			transform: rotate(-90deg);
			margin-left: 201px;
			background: <?php echo $id_card_config->main_color; ?>;
			/* height: 35px; */
			width: 73px;
			font-size: 12px;
			margin-top: -60px;
			color: white;
			text-align: center;
			padding-top: 4px;
		}
		.student_info{
			margin-top: -43px;
		}

		.student_info-left {
			font-size: 15px;
		}

		.student_info_table {
			margin-left: 15px;
			text-align: left;
			color:<?php echo $id_card_config->secondary_text_color; ?>;
		}
		table, td, th {
			font-size: 10px;

			line-height: 1;
		}
		.sheet {
			background: white;
			box-shadow: 0 0 0 rgba(0,0,0,0);
			margin: 5mm auto;
		}
		.photo{


		}
		.page_breack {
			page-break-after: always;

		}


		.printButton:link, .printButton:visited {
			background-color: #0c43a7;
			color: white;
			padding: 8px 8px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
		}

		.printButton:hover, .printButton:active {
			background-color: red;
		}

		.backToButton:link, .backToButton:visited {
			background-color: #a3c2c2;
			color: white;
			padding: 8px 8px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
		}

		.backToButton:hover, .backToButton:active {
			background-color: #0c43a7;
		}
		.address{
			background-color: <?php echo $id_card_config->main_color; ?>;
			color: #fff;
			font-family: none;
			height: 20px;
			padding-top: 2px;
			margin-top: -6px;
		}
	</style>


</head>

<body>


<div class="container rows-print-as-pages" id="printableArea">



	<div class="row">
		<?php
		$i = 1;
		foreach ($list as $row):
			?>
			<div class="form-group col-md-3" style="margin-bottom: 0px !important">
				<section class="sheet">
					<div class="id-card-holder">
						<div>
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 180 273" style="width: 231px;margin-left: 7px;margin-top: 2px;" xml:space="preserve">
            <path style="fill:<?php echo $id_card_config->main_color; ?>;" d="M-56.6,60.46c17.35-17.9,103.63-60.9,156.04-60.9c40.42,1,83.37-11.63,72.37,48.28 c0,2-10.34-24.55-56.84-24.55c-35.44,0-90.99,34.4-130.56,49.23"></path>
						</div>
						<div class="id-card">
							<div class="" >
								<img style="border-radius:50%;width: 35%;height: 74px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info[0]['picture']; ?>">
							</div>
							<h2 style="color:<?php echo $id_card_config->main_color; ?>;">
								<?php echo $school_info[0]['school_name']; ?>
							</h2>

							<h2 class="address">
								<?php echo $school_info[0]['address']; ?>
							</h2>

							<div style="margin-top: 48px;">
								<div class="photo">
									<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/<?php if ($row['photo']!='') {
										echo 'student/'.$row['photo'];
									} else {
										echo 'img/not_found.jpg';
									} ?>">

								</div>

								<div class="identity">IDENTITY CARD</div>
								<div class="studentid">ID <br> <?php echo $row['student_code']; ?></div>
							</div>

							<div class="student_info">
								<h3 style="text-align: center;color:<?php echo $id_card_config->main_text_color; ?>;"><b><?php echo $row['name']; ?></b><h3>
										<table class="student_info_table" style="">
											<?php if($id_card_config->fathers_name_show == '1'){ ?>
												<tr>
													<td>Father. </td>
													<td>: <?php echo $row['father_name']; ?></td>
												</tr>
											<?php } ?>

											<?php if($id_card_config->mothers_name_show == '1'){ ?>
												<tr>
													<td>Mother. </td>
													<td>: <?php echo $row['mother_name']; ?></td>
												</tr>
											<?php } ?>

											<?php if($id_card_config->date_of_birth_show == '1'){ ?>
												<tr>
													<td>DOB </td>
													<td>: <?php echo date("d-m-Y", strtotime($row['date_of_birth'])); ?></td>
												</tr>
											<?php } ?>

											<?php if($id_card_config->mobile_show == '1'){ ?>
												<tr>
													<td>Mobile  </td>
													<td>: <?php echo $row['guardian_mobile']; ?></td>
												</tr>
											<?php } ?>


											<?php if($id_card_config->class_show == '1'){ ?>
												<tr>
													<td>Class  </td>
													<td>: <?php echo $row['class_name']; ?></td>
												</tr>
											<?php } ?>

											<?php if($id_card_config->section_show == '1'){ ?>
												<tr>
													<td>Section  </td>
													<td>: <?php echo $row['section_name']; ?></td>
												</tr>
											<?php } ?>

											<?php if($id_card_config->group_show == '1'){ ?>
												<tr>
													<td>Group  </td>
													<td>: <?php echo $row['group_name']; ?></td>
												</tr>

											<?php } ?>

											<?php if($id_card_config->shift_show == '1'){ ?>
												<tr>
													<td>Shift  </td>
													<td>: <?php echo $row['shift_name']; ?></td>
												</tr>
											<?php } ?>

											<?php if($id_card_config->roll_show == '1'){ ?>
												<tr>
													<td>Roll No.  </td>
													<td>: <?php echo $row['roll_no']; ?></td>
												</tr>
											<?php } ?>

											<?php if($id_card_config->year_show == '1'){ ?>
												<tr>
													<td>Year  </td>
													<td>: <?php echo date("Y"); ?></td>
												</tr>
											<?php } ?>


										</table>
							</div>
							<div style="text-align:right; margin-right:1px; margin-top: -42px;">
								<?php  if (isset($signature['L'])) { ?>
								<p style="margin-top: -20px;">

									<?php if ($signature['L']['is_use'] == '1') { ?>

										<img style="width: 98px; height: 30px;margin-right: -7px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['L']['location']; ?>">
									<?php }
									} ?>
									<br>
								<p style="width:40%; float: right; margin-top: -18px;border-top: 1px <?php echo $id_card_config->main_color; ?> solid;">
									<b>
										<?php if (isset($signature['L'])) {
											echo $signature['L']['level'];
										}
										?>
										&nbsp;
									</b>
							</div>
						</div>
					</div>
				</section>
			</div>
			<?php
			if ($i % 8 == 0) {
				echo '</div><div class="row">';
			}
			$i++;  endforeach; ?>
	</div>
</body>
</html>
