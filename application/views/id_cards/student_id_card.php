<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            width: 300px;
            margin: auto;
            text-align: center;
            font-family: arial;
            border: 3px black solid;
        }

        .container {
            padding: 0 10px;
        }

        .container::after {
            content: "";
            clear: both;
            display: table;
        }

        .title {
            color: grey;
            font-size: 15px;
        }

    </style>
</head>
<body>

<div class="card">
    <br>
    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/<?php if($student_info_by_id[0]['photo']!=''){echo 'student/'.$student_info_by_id[0]['photo'];}else{
        echo 'img/not_found.jpg';
    } ?>" alt="John" style="width:50%">
    <div class="container">

        <table width="100%" style="font-size:13px;">
            <tr>
                <td>
                    <b style="font-size: 18px;"><?php echo $student_info_by_id[0]['name']; ?></b>
                </td>
            </tr>
            <tr>
                <td>
                    <b style="font-size: 15px;"><?php echo $student_info_by_id[0]['student_code']; ?></b>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo$student_info_by_id[0]['class_name']; ?>, Roll: <?php echo $student_info_by_id[0]['roll_no']; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <b><?php echo $school_info[0]['school_name']; ?></b>
                </td>
            </tr>
            <tr>
                <td>
                    Section: <?php echo $student_info_by_id[0]['section_name'];  ?>, Group:  <?php
                    echo $student_info_by_id[0]['group_name'];
                    ?>
                </td>
            </tr>
        </table>
        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/signature.png" style="width:50%;">
        <div style="font-size: 12px;">
            Authorized By<br>
            <b>Head Teacher<b></b><br>
            <?php echo $school_info[0]['school_name']; ?><br>
            <?php echo $school_info[0]['web_address']; ?>
        </div>
    </div><br>
</div>

</body>
</html>
