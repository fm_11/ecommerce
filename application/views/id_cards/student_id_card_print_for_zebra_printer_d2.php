<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Student Id Card</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/normalize.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/paper.css">
  <style>
    @page { size: 55mm 86mm } /* output size */
    body.receipt .sheet { width: 54mm; height: 86mm } /* sheet size */
        @media print { body.receipt { width: 54mm } } /* fix for Chrome */
        .id-card-holder{
            background:url(<?php echo base_url() . MEDIA_FOLDER; ?>/logos/watermark.png);
            background-repeat: no-repeat;
            background-position: center;

        }

      	@media print {
body {-webkit-print-color-adjust: exact;}
}

		.id-card {
			width:50mm;
			height:82mm;
			text-align: center;
            margin: 5px;
            border:	 3px double #008080;

		}
		.id-card img {
			margin: 0 auto;
		}

		.name_header{
		    width: 100%;
			background-color:#008080;
			margin: 0 auto;
			margin-top: -12px;
		}

		.header {
			width: 100%;
            margin-top: -17px;
		}

		.alignleft {
			float: left;
			text-align:center;
			width:36%;
			height: 60px;
			line-height: 43px;
			font-size: 13px;
			font-family: "Arial Black", Gadget, sans-serif;
			letter-spacing: -1px;
		}
		.aligncenter {
			float: left;
			text-align:center;
			width:33%;
			height: 60px;
			line-height: 60px;

		}
		.alignright {
			float: left;
			text-align:center;
			width:30%;
			height: 60px;
			line-height: 43px;
			font-size: 13px;
			font-family: "Arial Black", Gadget, sans-serif;
		}

		.header img {
			width: 65%;

		}
		.photo img {
			width: 46%;
			height:90px;
    		margin-top: -25px;
			margin-bottom: -10px;
			border-radius: 8px;
		}
		h2 {
			font-size: 14px;
			font-family: "Arial Black", Gadget, sans-serif;
		}
		h3 {
			font-size: 14px;
			font-weight: bold;
			font-family: "Angsana New", Monaco, monospace;
			margin-top: 13px;
			color:#000000;
		}
		.qr-code img {
			width: 23%;
		}
		p {
			font-size: 11px;
			color:#00001a;
			font-family: "Trebuchet MS", Helvetica, sans-serif;
		}

		.main_text{
		  text-align:left;
		  padding-left: 5px;
		}

		.bottom_div{
		    clear: both;
		    position: absolute;
		    width: 50mm;
            bottom:1.5mm;
            margin-bottom:1.5mm;
            border-top: 2px dashed #cc0000;
            text-align:center;
            height:32px;
            line-height: 30px;
            background-color:#008080;

		}

  </style>
</head>

<body class="receipt">

<?php
 $i = 0;
foreach ($list as $row):
    //echo '<pre>';
    //print_r($row); die;
    $i++;
    ?>

  <section id="watermark" class="sheet padding-1mm">
		<div class="id-card-holder">
			<div class="id-card">
			    <div class="name_header">
					<h2 style="color:#ffffff;"><?php echo $school_info[0]['school_name']; ?></h2>
				</div>

				<div class="header">
					<p class="alignleft"><b>&nbsp;<?php echo $row['student_code']; ?>&nbsp;</b></p>
					<p class="aligncenter"><img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info[0]['picture']; ?>"></p>
					<p class="alignright"><b><?php echo $row['section_name']; ?>&nbsp;</b></p>
				</div>

					<?php
                       // echo strlen(trim("MD. MASHIUR RAHMAN")).'/'.strlen("MD. SHAKIBUL ISLAM"); die;
                            $text_margin = "margin-bottom: -12px;";
                            $name_font_size = "15px;";
                            $name_margin_top = "";
                            $bottm_div_height = "32px";
                    ?>


				<div class="photo">
					<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/<?php if ($row['photo']!='') {
                        echo 'student/'.$row['photo'];
                    } else {
                        echo 'img/not_found.jpg';
                    } ?>">
				</div>



				<div class="main_text">
				     <table width="100%">
					    <tr>
						    <td colspan="2">
							   <h2 style="font-size: <?php echo $name_font_size . $name_margin_top; ?>
							   color:#800000; margin-bottom: -7px; line-height: 15px;">
							      <?php echo strtoupper(ltrim($row['name']));//strlen(trim( $row['name']));?>
							   </h2>
							</td>
						</tr>
						<tr>
							<td colspan="2">
							   <h3 style="<?php echo $text_margin; ?>"><b><?php echo strtoupper($row['group_name']); ?></b></h3>
							</td>
						</tr>
						<tr>
							<td colspan="2">
							   <h3 style="<?php echo $text_margin; ?>"><b>Academic Year:&nbsp;<?php echo $row['year']; ?></b></h3>
							</td>
						</tr>
						<tr>
							<td width="30%">
							   <h3 style="margin-bottom: -5px;font-size: 13px;"><b>ROLL:&nbsp;<?php echo $row['roll_no']; ?></b></h3>
							</td>
							<td style="text-align:right;">
						     	<h3 style="margin-bottom: -5px;font-size: 13px;"><b>SHIFT: <?php echo strtoupper($row['shift_name']); ?>&nbsp;</b></h3>
							</td>
						 </tr>


					 </table>


				</div>


				<div class="bottom_div" <?php if ($i != 1) { ?>style="margin-bottom: 1.5mm;" <?php } ?>>
				   <!--<span style="color:#ffffff; font-size:14px;"><b>SSC (VOCATIONAL)</b></h2>!-->
				   <span style="color:#ffffff; font-size:14px;"><b><?php echo 'Class - ' . $row['class_name']; ?></b></h2>
				</div>

			</div>
	    </div>
  </section>
 <?php endforeach; ?>
</body>
</html>
