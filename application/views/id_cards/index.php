<script>
function show_off_roll_section(view_type){
    if(view_type == 'A'){
        document.getElementById("from_roll_section").style.display = "none";
        document.getElementById("to_roll_section").style.display = "none";
    }
    if(view_type == 'S'){
        document.getElementById("from_roll_section").style.display = "";
        document.getElementById("to_roll_section").style.display = "";
    }
}
</script>

<form name="addForm" class="cmxform" target="_blank" id="commentForm"   action="<?php echo base_url(); ?>id_cards/index" method="post"
      enctype="multipart/form-data">
<div class="form-row">
  <div class="form-group col-md-5">
    <label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
    <select class="js-example-basic-single w-100"
     name="class_shift_section_id" id="class_shift_section_id" required>
        <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
        <?php
        $i = 0;
        if (count($class_section_shift_marge_list)) {
            foreach ($class_section_shift_marge_list as $list) {
                $i++; ?>
                <option
                    value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
                    <?php if (isset($class_shift_section_id)) {
                    if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
                        echo 'selected';
                    }
                } ?>>
                <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
              </option>
                <?php
            }
        }
        ?>
    </select>
  </div>


  <div class="form-group col-md-3">
    <label>Group</label>
    <select class="js-example-basic-single w-100" required name="txtGroup">
        <option value="">-- Please Select --</option>
        <?php
        $i = 0;
        if (count($group)) {
            foreach ($group as $list) {
                $i++; ?>
                <option
                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>
  </div>

  <div class="form-group col-md-2">
     <label>Is Double Page?</label>
     <select class="js-example-basic-single w-100" name="IsDoublePage" required="1">
         <option value="N">No</option>
         <!-- <option value="Y">Yes</option> -->
     </select>
  </div>

  <div class="form-group col-md-2">
     <label>Print For?</label>
     <select class="js-example-basic-single w-100" name="IsPrintFor" required="1">
         <option value="N">Normal Printer</option>
         <option value="Z">Zebra Printer</option>
     </select>
  </div>
</div>


<div class="form-row">

  <div class="form-group col-md-4">
    <label>Design?</label>
    <select class="js-example-basic-single w-100" name="design" required="1">
        <option value="D1">Design - 1</option>
        <option value="D2">Design - 2</option>
		 <option value="D3">Design - 3</option>
    </select>
  </div>

  <div class="form-group col-md-2">
    <label>Part?</label>
    <select class="js-example-basic-single w-100" name="Part" required="1">
        <option value="F">Front Part</option>
        <option value="B">Back Part</option>
    </select>
  </div>



  <div class="form-group col-md-2">
     <label>Type</label>
     <select  class="js-example-basic-single w-100" onchange="show_off_roll_section(this.value)" name="view_type" required="1">
        <option value="A">All Student</option>
        <option value="S">Selected Roll</option>
     </select>
   </div>

  <div class="form-group col-md-2">
     <div id="from_roll_section" style="display:none;">
        <label>From Roll</label>
        <input type="text" autocomplete="off"  class="form-control"  name="from_roll" id="from_roll"/>
        <span>Use only number. (Example: 1,2,3 etc.)</span>
     </div>
  </div>

  <div class="form-group col-md-2">
     <div id="to_roll_section" style="display:none;">
        <label>To Roll</label>
        <input type="text" autocomplete="off"  class="form-control"  name="to_roll" id="to_roll"/>
         <span>Use only number. (Example: 1,2,3 etc.)</span>
     </div>
  </div>

</div>

  <div class="btn-group float-right">
      <!-- <input type="submit" name="pdf_download" class="btn btn-light" value="Download"> -->
      <input type="submit" name="Preview" class="btn btn-primary" value="Preview">
  </div>
</form>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col" colspan="2">ID Card Sample Design</th>
    </tr>
    </thead>
    <tbody>
      <tr>
          <td>Design - 1</td>
          <td>
            <a target="_blank" href="<?php echo base_url(); ?>core_media/id_card_design/id_design_1.png">View</a>
          </td>
      </tr>
      <tr>
          <td>Design - 2</td>
          <td>
            <a target="_blank" href="<?php echo base_url(); ?>core_media/id_card_design/id_design_2.png">View</a>
          </td>
      </tr>
	  <tr>
          <td>Design - 3</td>
          <td>
            <a target="_blank" href="<?php echo base_url(); ?>core_media/id_card_design/id_design_3.png">View</a>
          </td>
      </tr>
    </tbody>
</table>
</div>
