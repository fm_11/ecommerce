<!DOCTYPE html>
<html>
<head>
<style>
table {
    border-collapse: collapse;
}

table, td, th {
    border: 1px solid black;
	text-align: center;
	height: auto;
}

.s_div {
    border: 2px solid #a1a1a1;   
    background: #e8eef7;
    width: 150px;
    border-radius: 25px;
	margin:0px auto;
	margin-top:10px;
	text-align:center;
}

.m_div {
    border-top: 2px solid #a1a1a1;   
	border-bottom: 2px solid #a1a1a1; 
    background: #e8eef7;
    width: 100%;  
	margin-top:10px;
	text-align:center;
}

.f_div { 
    background: #e8eef7;
    width: 100%;  
	margin-top:10px;
	text-align:center;
	font-size:11px;
	vertical-align: bottom;
}

 .page_breack {
     page-break-after: always;

}

</style>
</head>
<body>
<?php
if ($is_pdf_request != "Download") {
    ?>
   
                <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/student_id_card_pdf" method="post"
                      enctype="multipart/form-data">
                    <input type="hidden" name="txtClass" value="<?php echo $class_id; ?>">
                    <input type="hidden" name="txtSection" value="<?php echo $section_id; ?>">
                    <input type="hidden" name="txtGroup" value="<?php echo $group; ?>">
                    <input type="hidden" name="is_pdf_request" value="Download">               
                    <input type="submit" class="submit" value="Download PDF">
                </form>
            
    <?php
}
?>



<table width="400px" id="mydiv" align="center">
<?php
    $i = 0;
    foreach ($list as $row):
        $i++;
        ?>
        <tr <?php if ($i % 2 == 0) {
            echo 'class="page_breack"';
        } ?>>

    <td style="width:183px;  vertical-align: top; padding-top:5px;">
	    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/id_card_logo.png" style="width:80px; height: 80px;"/>
		<br><b><?php echo $school_info[0]['school_name']; ?></b>
		<br><span style="font-size:12px;"><?php echo $school_info[0]['address']; ?>
		<br>Mobile: <?php echo $school_info[0]['mobile']; ?></span>
		<br>
		
		<?php 
		if($row['photo'] == ''){
			$photo = "not_found.png";
		}else{
			$photo = $row['photo'];
		}
		?>
		
		<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $photo; ?>" style="width:80px; height: 80px;"/>
		
		<div class="s_div">
		<b><?php echo $row['name']; ?>
		<br><?php echo $row['student_code']; ?></b>
		</div>	
		<div class="f_div">
		<b><?php echo $school_info[0]['web_address']; ?></b>		
		</div>
	
	</td>
	<td>
	  &nbsp;
	</td>
    <td style="width:183px; vertical-align: top;">
	
	    <div class="s_div">
		<b>Blood Group: <?php echo $row['blood_group_name']; ?></b>		
		</div>
	
	    <div class="m_div">
		<b>Class: <?php echo $row['class_name']; ?>, Roll No.: <?php echo $row['roll_no']; ?></b>
		<br>Section: <?php echo $row['section_name']; ?>
		<br>Group: <?php echo $row['group_name']; ?>
		<br>Date of Birth: <?php echo $row['date_of_admission']; ?>
		<br>Expire Date: <?php echo date('Y-12-31'); ?>
		</div>
		<br>
		
		<div>
		<b>
		  1. This card is not transferable.
         <br>2. this card is to be submitted to the institute authority.</b>
		</div>
		<br>
		<div style="text-align:right;">
		<p>
		<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/signature.png" style="vertical-align:middle; height:50px; width:100px;"><br>
		Authorized By...................
		 </p>​
		</div>
	</td>
  </tr>
  
   <tr><td colspan="2">&nbsp;</td></tr>
  
<?php endforeach; ?>
</table>
</body>
</html>

