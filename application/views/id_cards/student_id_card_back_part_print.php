<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Student Id Card Back Part</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/normalize.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/paper.css">
  <style>
    @page { size: 55mm 86mm } /* output size */
    body.receipt .sheet { width: 54mm; height: 86mm } /* sheet size */
    @media print { body.receipt { width: 54mm } } /* fix for Chrome */

		.id-card {
		    /*background-image: url("https://kalabadha.school360.app/core_media/86373.jpg");*/
			width:50mm;
			height:82mm;
			background-color: #fff;						
			text-align: center;		  
            margin: 5px;
           
            border:	 3px double #000000;
            border-radius: 10px;		
		}
		.id-card img {
			margin: 0 auto;
		}
		.header img {
			width: 40%;
    		margin-top: 5px;
			margin-bottom: -13px;
		}
		.photo img {
			width: 37%;
			height:70px;
    		margin-top: -5px;
			margin-bottom: -5px;
			border-radius: 8px;
		}
		h2 {
			font-size: 14px;
			margin-bottom: -3px;
			color:#000000;
			font-family: "Arial Black", Gadget, sans-serif;
		}
		h3 {
			font-size: 13px;	
			font-family: "Angsana New", Monaco, monospace;
			margin-top: 12px;
			color:#000000;
		}
		.qr-code img {
			width: 23%;
		}
		p {
			font-size: 12px;
			color:#000000;
			font-family: "Trebuchet MS", Helvetica, sans-serif;
			margin-top:20px;
			line-height: 15px
		}
		
		.name_header{
		    width: 100%;
			background:#000000;
			margin: 0 auto;
			margin-top: -12px;
			border-top-left-radius: 6px;
			border-top-right-radius: 6px;
		}
		
		.main_text{
		  text-align:left;
		  padding-left: 5px;
		  margin-top: 5px;
          margin-bottom: 10px;
          vertical-align:middle;
		}
		
		@media print {
            body {-webkit-print-color-adjust: exact;}
        }
		
  </style>
</head>

<body class="receipt">
<?php
 $i = 0;
foreach ($list as $row):
    //echo '<pre>';
    //print_r($row); die;
    $i++;
    ?>	
  <section class="sheet padding-1mm">
		<div class="id-card-holder">
			<div class="id-card">
				 <div class="name_header">
					<h2 style="color:#ffffff;"><?php echo $school_info[0]['school_name']; ?></h2>
				</div>
				
				<div class="main_text">
				     <table width="100%">
					   
						<tr>
							<td>
							   <h3 style="margin-bottom: -8px; margin-top: 0px;"><b>FATHER:<?php echo strtoupper($row['father_name']); ?></b></h3>
							</td>
						</tr>
						<tr>
							<td>
							   <h3 style="margin-bottom: -8px;"><b>MOTHER:<?php echo strtoupper($row['mother_name']); ?></b></h3>
							</td>
						</tr>
						<tr>
							<td>
							   <h3 style="margin-bottom: -8px;"><b>MOBILE:<?php echo $row['guardian_mobile']; ?></b></h3>
							</td>
						</tr>
						<tr>
							<td>
							   <h3 style="margin-bottom: -13px;"><b>EXPIRE DATE: JUNE/2020</b></h3>
							</td>
						</tr>
            		 </table>
				</div>
			
			
			   
				<p style="border-top: 2px dashed #000000;padding-top: 10px;"><strong>এই কার্ডটি কেউ পেলে উল্লেখিত মোবাইল/ফোন নাম্বার এ যোগাযোগ করার জন্য অনুরোধ করা হলো।</strong></p>
			   
			   	<div style="position: absolute;width: 50mm;padding-top: 2px;
bottom: 7px;border-top: 2px dashed #000000;text-align:right; height:106px; margin-right:10px;">
			   	    
			   	    <div style="width:100%; float:left;">
			   	        <div style="width:40%; float:left;">
    			   	        <img style="width: 70px; height: 35px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/incharge.png">
        				    <br> 
        				    <p style="border-top: 1px #000000 solid; width:90%;
        				    font-size:13px; float: right; margin-top:1px;padding-right:5px;line-height: 15px;"><b>Academic <br>In-charge</b></p>
        				   
    			   	    </div>
    			   	    
    			   	   <div style="width:60%; float:right;">
    			   	        <img style="width: 100px; height: 39px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/signature.png">
        				    <br> 
        				    <p style="border-top: 1px #000000 solid; width:60%;
        				    font-size:13px; float: right; margin-top: -4px;padding-right:5px;line-height: 20px;"><b>Principal</b>
        				    
        				    </p>
    			   	    </div>
    			   	    <div style="width:100%; float:left;margin-top: -25px;font-size: 12px;line-height:13px;">
    			   	        Jamalpur Technical&nbsp;<br>School and College&nbsp;
    			   	        <br>Phone- 0981-63454&nbsp;
    			   	    </div>
			   	    </div>
			   	    
			   	   
				</div>
			</div>
	    </div>
  </section>
<?php endforeach; ?>
</body>
</html>
