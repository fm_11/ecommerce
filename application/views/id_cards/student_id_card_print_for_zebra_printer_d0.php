<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Student Id Card</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/normalize.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/paper.css">
  <style>
    @page { size: 55mm 86mm } /* output size */
    body.receipt .sheet { width: 54mm; height: 86mm } /* sheet size */
    @media print { body.receipt { width: 54mm } } /* fix for Chrome */

		.id-card {
		    /*background-image: url("https://kalabadha.school360.app/core_media/86373.jpg");*/
			width:50mm;
			height:82mm;
			background-color: #fff;
			text-align: center;
            margin: 5px;

            border:	 3px double #206020;
            border-radius: 10px;
		}
		.id-card img {
			margin: 0 auto;
		}
		.header img {
			width: 40%;
    		margin-top: 5px;
			margin-bottom: -13px;
		}
		.photo img {
			width: 37%;
			height:70px;
    		margin-top: -5px;
			margin-bottom: -5px;
			border-radius: 8px;
		}
		h2 {
			font-size: 14px;
			margin-bottom: -3px;
			color:#00001a;
			font-family: "Arial Black", Gadget, sans-serif;
		}
		h3 {
			font-size: 13px;
			font-weight: 400;
			font-family: "Angsana New", Monaco, monospace;
			margin-top: 6px;
			color:#003300;
		}
		.qr-code img {
			width: 23%;
		}
		p {
			font-size: 11px;
			color:#00001a;
			font-family: "Trebuchet MS", Helvetica, sans-serif;
		}

  </style>
</head>

<body class="receipt">


<?php
 $i = 0;
foreach ($list as $row):
     $i++;
    ?>
  <section class="sheet padding-1mm">
		<div class="id-card-holder">
			<div class="id-card">
				<div class="header">
					<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info[0]['picture']; ?>">
				</div>
				<h2>KALABADHA ML. HIGH SCHOOL</h2>
				<h3><b>EIIN 110081</b></h3>
				<div class="photo">
					<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/<?php if ($row['photo']!='') {
        echo 'student/'.$row['photo'];
    } else {
        echo 'img/not_found.jpg';
    } ?>">
				</div>
				<h2 style="font-size: 12px; margin-top: 8px;"><?php echo $row['name']; ?></h2>
				<h3 style="margin-bottom: 10px;"><b>ID&nbsp;<?php echo $row['student_code']; ?></b></h3>
				<hr style="background-color: #fff; border-top: 2px dashed #206020;">
				<div style="text-align:right; margin-right:10px; margin-top: -10px;">
          <?php  if (isset($signature['L'])) { ?>
				    <p><b><?php echo $signature['L']['level']; ?>&nbsp;</b><br>
              <?php
             if ($signature['L']['is_use'] == '1') { ?>
                <img style="width: 98px; height: 30px;"  src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['L']['location']; ?>" />

              <?php }
         } ?>&nbsp;

				</div>
			</div>
	    </div>
  </section>

<?php endforeach; ?>
</body>
</html>
