<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Student Id Card</title>
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/normalize.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/paper.css">
  <style>
    @page { size: 55mm 86mm } /* output size */
    body.receipt .sheet { width: 54mm; height: 86mm } /* sheet size */
    @media print { body.receipt { width: 54mm } } /* fix for Chrome */

		.id-card {
			width:50mm;
			height:82mm;
			background-color: #fff;
			text-align: center;
            margin: 5px;

            border:	 3px double #206020;
            border-radius: 10px;
		}
		.id-card img {
			margin: 0 auto;
		}
		.header img {
			width: 35%;
    		margin-top: 3px;
			margin-bottom: -16px;
		}
		.photo img {
			width: 33%;
			height:68px;
    		margin-top: 18px;
			margin-bottom: -5px;
			border-radius: 8px;
		}
		h2 {
			font-size: 14px;
			margin-bottom: -12px;
			margin-top: 10px;
			color:#00001a;
			font-family: "Arial Black", Gadget, sans-serif;
		}
		h3 {
			font-size: 11px;
			font-weight: 400;
			font-family: "Angsana New", Monaco, monospace;
			margin-top: 14px;
			color:#003300;
		}
		.qr-code img {
			width: 23%;
		}
		p {
			font-size: 11px;
			color:#00001a;
			font-family: "Trebuchet MS", Helvetica, sans-serif;
		}

  </style>
</head>

<body class="receipt">

<?php
 $i = 0;
foreach ($list as $row):
    //echo '<pre>';
    //print_r($row); die;
    $i++;
    ?>

  <section class="sheet padding-1mm">
		<div class="id-card-holder">
			<div class="id-card">
				<div class="header">
					<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info[0]['picture']; ?>">
				</div>
				<h2 style="color:#000066;">Greenland Model School</h2>
				<div class="photo">
					<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/<?php if ($row['photo']!='') {
        echo 'student/'.$row['photo'];
    } else {
        echo 'img/not_found.jpg';
    } ?>">
				</div>
				<h2 style="font-size: 12px; margin-top: 5px; color:#001a00;"><?php echo $row['name']; ?></h2>
				<h3 style="margin-bottom: -14px;"><b>ID&nbsp;<?php echo $row['student_code']; ?></b></h3>
				<h3 style="margin-bottom: -14px;"><b>Class:&nbsp;<?php echo $row['class_name']; ?></b></h3>
				<h3 style="margin-bottom: -14px;"><b>Section:&nbsp;<?php echo $row['section_name']; ?> & Roll:&nbsp;<?php echo $row['roll_no']; ?></b></h3>
			    <h3 style="margin-bottom: -2px;"><b>G. Mobile:&nbsp;<?php echo $row['guardian_mobile']; ?></b></h3>
				<hr style="background-color: #fff; border-top: 2px dashed #206020;">
				<div style="text-align:right; margin-right:10px; margin-top: -7px;">
          <?php  if (isset($signature['L'])) { ?>
				    <p><b>&nbsp;</b><br>
              <?php if ($signature['L']['is_use'] == '1') { ?>
                <img style="width: 98px; height: 30px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['L']['location']; ?>">
              <?php }
         } ?>
				    <br>
				    <p style="border-top: 1px #000000 solid; width:60%; float: right; margin-top: -6px">
              <b>
                <?php if (isset($signature['L'])) {
             echo $signature['L']['level'];
         }
                 ?>
                &nbsp;
             </b>
				</div>
			</div>
	    </div>
  </section>
  <?php endforeach; ?>
</body>
</html>
