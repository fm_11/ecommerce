<!DOCTYPE html>
<html>
<head>
<title><?php echo $title; ?></title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    window.print();
</script>
<style>

@media print {
  .identity{
    background-color: #0c43a7 !important;
    -webkit-print-color-adjust: exact;
    color: white;
  }

  .studentid{
    background-color: #0c43a7 !important;
    -webkit-print-color-adjust: exact;
    color: white;
  }
  .header{
     background-color: #0c43a7 !important;
     -webkit-print-color-adjust: exact;
     color: white;
  }
  .rows-print-as-pages .row {
    page-break-before: always;
  }
  /* include this style if you want the first row to be on the same page as whatever precedes it */
  /*
  .rows-print-as-pages .row:first-child {
    page-break-before: avoid;
  }
  */
  .id-card {

          border:	 3px solid  #142975;
          border-radius: 10px;
  }
}
@page { size: A4 landscape }

.id-card {
  width: 225px;
  height: 305px;
  background-color: #fff;
  text-align: center;
  margin: 5px;
  border: 3px solid <?php echo $id_card_config->main_color; ?>;
  border-radius: 5px;
}
.id-card img {
  margin: 0 auto;
}
.header img {
  width: 21%;
  margin-top: 21px;
  margin-bottom: -17px;
}
.photo img {
  width: 37%;
    border-radius: 5px;
}
h2 {
  font-size: 12px;
  margin-bottom: -19px;
  margin-top: 2px;
  color: #00001a;

  font-family: "Arial Black", Gadget, sans-serif;
}
h3 {
  font-size: 13px;
  font-weight: 400;
  /* font-family: "Angsana New", Monaco, monospace; */
  margin-top: 10px;
  margin-bottom: -8px;
  color:#003300;
}
.qr-code img {
  width: 23%;
}
p {
  font-size: 11px;
  color:#00001a;
  font-family: "Trebuchet MS", Helvetica, sans-serif;
}
.identity{
  background: #0c43a7;
    transform-origin: 0 0;
    position: relative;
    transform: rotate(270deg);
    font-size: 12px;
    width: 95px;
    height: 33px;
    color: white;
    padding-top: 10px;
}

.studentid{
  position: absolute;
  transform-origin: 0;
  transform: rotate(-90deg);
  margin-left: 203px;
  background: #0c43a7;
  height: 35px;
  width: 95px;
  font-size: 12px;
  margin-top: -50px;
  color: white;
  text-align: center;
  padding-top: 10px;
}
.student_info{
  margin-top: -32px;
}

.student_info-left {
font-size: 15px;
}

.student_info_table {

}
  table, td, th {
    font-size: 12px;
    margin-left: 46px;
    line-height: 1;
  }
  .sheet {
      background: white;
      box-shadow: 0 0 0 rgba(0,0,0,0);
      margin: 5mm auto;
  }

    .page_breack {
        page-break-after: always;

    }


    .printButton:link, .printButton:visited {
          background-color: #f44336;
          color: white;
          padding: 8px 8px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
    }

    .printButton:hover, .printButton:active {
      background-color: red;
    }

    .backToButton:link, .backToButton:visited {
          background-color: #a3c2c2;
          color: white;
          padding: 8px 8px;
          text-align: center;
          text-decoration: none;
          display: inline-block;
    }

    .backToButton:hover, .backToButton:active {
      background-color: #527a7a;
    }
    .fixed-header, .fixed-footer{

        width: 220px;
    }
</style>


</head>

<body>


<div class="container rows-print-as-pages" id="printableArea">
<div class="row">
  <?php
   $i = 1;
  foreach ($list as $row):
      ?>
      <div class="form-group col-md-3" style="margin-bottom: 0px !important">
      <section class="sheet">
    		<div class="id-card-holder">
          <div class="id-card">
            <div style="height: 30px;background-color: <?php echo $id_card_config->main_color; ?> !important;" class="header fixed-header">
              <h2 style="color: white;margin: auto;text-align: center;padding-top: 5px;">
                <?php echo $id_card_config->back_part_top_text; ?>
              </h2>
            </div>


            <div style="height: 203px;margin-top: 36px;">
             <div style="height: 50px;">
               <h2 style="color:<?php echo $id_card_config->main_text_color; ?>;">
                  <span><?php echo $school_info[0]['school_name']; ?></span><br>
               </h2>
             </div>
             <div style="height: 50px;">
               <h2 style="color:<?php echo $id_card_config->secondary_text_color; ?>;">
                  <span><?php echo $school_info[0]['address']; ?></span><br>
               </h2>
             </div>
              <div class="photo">       
                 <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/QR_CODE/<?php echo $id_card_config->bar_code; ?>">
              </div>
            </div>
            <div style="height: 30px;padding-top: 2px;background-color: <?php echo $id_card_config->main_color; ?> !important;" class="header fixed-footer">
              <h2 style="color: white;margin: auto;text-align: center;padding-top: 5px;">
				  <?php echo $id_card_config->back_part_bottom_text; ?>
              </h2>
            </div>
    			</div>
    	    </div>
      </section>
      </div>
    <?php
    if ($i % 8 == 0) {
        echo '</div><div class="row">';
    }
    $i++;  endforeach; ?>
</div>
</div>
</body>
</html>
