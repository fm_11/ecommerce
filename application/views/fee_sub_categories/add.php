<?php
if ($action == 'edit') {
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>fee_sub_categories/edit/" method="post">
        <input type="hidden" name="id" value="<?php echo $fee_sub_category_info[0]['id']; ?>">
        <div class="form-row">
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('name'); ?></label><br>
                <input type="text" autocomplete="off"  name="name" class="form-control" required="1" value="<?php echo $fee_sub_category_info[0]['name']; ?>">
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('category'); ?></label><br>
                <select class="js-example-basic-single w-100" name="category_id" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php
                    $i = 0;
    if (count($fee_category_info)) {
        foreach ($fee_category_info as $list) {
            $i++; ?>
                            <option
                                value="<?php echo $list['id']; ?>"<?php if ($fee_sub_category_info[0]['category_id'] == $list['id']) {
                echo 'selected';
            } ?>><?php echo $list['name']; ?></option>
                        <?php
        }
    } ?>
                </select>
            </div>
			<div class="form-group col-md-3">
				<label>Is Waiver Applicable?</label><br>
				<select class="js-example-basic-single w-100" name="is_waiver_applicable" required="1">
					<option value="0" <?php if ($fee_sub_category_info[0]['is_waiver_applicable'] == '0') {
						echo 'selected';
					} ?>><?php echo $this->lang->line('no'); ?></option>
					<option value="1" <?php if ($fee_sub_category_info[0]['is_waiver_applicable'] == '1') {
						echo 'selected';
					} ?>><?php echo $this->lang->line('yes'); ?></option>
				</select>
			</div>
			<div class="form-group col-md-3">
				<label>Order</label><br>
				<input type="number" autocomplete="off"  name="order" class="form-control" required="1" value="<?php echo $fee_sub_category_info[0]['order']; ?>">
			</div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label><?php echo $this->lang->line('remarks'); ?></label><br>
                <textarea id="wysiwyg" class="form-control" rows="7" cols="30" name="remarks"><?php echo $fee_sub_category_info[0]['remarks']; ?></textarea>
            </div>
        </div>
        <div class="float-right">
             <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
            <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
        </div>
    </form>
<?php
} else {
        ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>fee_sub_categories/add/" method="post">
        <div class="form-row">
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('name'); ?></label><br>
                <input type="text" autocomplete="off"  name="name" class="form-control" value="" required="1">
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('category'); ?></label><br>
                <select class="js-example-basic-single w-100" name="category_id" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php
                    $i = 0;
        if (count($fee_category_info)) {
            foreach ($fee_category_info as $list) {
                $i++; ?>
                            <option
                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                        <?php
            }
        } ?>
                </select>
            </div>
			<div class="form-group col-md-3">
				<label>Is Waiver Applicable?</label><br>
				<select class="js-example-basic-single w-100" name="is_waiver_applicable" required="1">
					<option value="0"><?php echo $this->lang->line('no'); ?></option>
					<option value="1"><?php echo $this->lang->line('yes'); ?></option>
				</select>
			</div>
			<div class="form-group col-md-3">
				<label>Order</label><br>
				<input type="number" autocomplete="off"  name="order" class="form-control" value="" required="1">
			</div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-12">
              <label><?php echo $this->lang->line('remarks'); ?></label><br>
              <textarea id="wysiwyg" class="form-control" rows="7" cols="30" name="remarks"></textarea>
          </div>
        </div>


        <div class="float-right">
            <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
            <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
        </div>
    </form>
<?php
    }
?>
