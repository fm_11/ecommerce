<div class="row">
	<div class="col-lg-4">
		<div class="border-bottom text-center pb-4">
			<?php
			if($student_info_by_id[0]['photo'] == '' || $student_info_by_id[0]['photo'] == 'default.png'){
				$image_src =  base_url() . 'core_media/images/default.png';
			}else{
				$image_src =  base_url() . MEDIA_FOLDER . '/student/' . $student_info_by_id[0]['photo'];
			}
			?>
			<img src="<?php echo $image_src; ?>" alt="profile" class="img-lg rounded-circle mb-3"/>
			<div class="mb-3">
				<h3><?php echo $student_info_by_id[0]['name']; ?></h3>
			</div>
			<p class="w-75 mx-auto mb-3">
				<?php echo $student_info_by_id[0]['student_code'] . ' (' . $student_info_by_id[0]['roll_no'] . ')'; ?><br>
				<?php echo $student_info_by_id[0]['class_name'] . '-' . $student_info_by_id[0]['shift_name'] . '-' . $student_info_by_id[0]['section_name']; ?>
			</p>
		</div>
		<div class="border-bottom py-4">
			<p>Subjects for <?php echo $student_info_by_id[0]['year']; ?></p>
			<div>
				<?php
				for ($l = 0; $l < count($subject_list_by_student); $l++) {
					?>
					<label class="badge badge-outline-dark">
						<?php
						echo $subject_list_by_student[$l]['name'] . ' (' . $subject_list_by_student[$l]['code'] . ')';
						if ($subject_list_by_student[$l]['is_optional'] == '1') {
							echo " (4th)";
						}
						?>
					</label>
					<?php
				}
				?>
			</div>
		</div>

		<div class="py-4">
			<p class="clearfix">
				  <span class="float-left">
					Status
				  </span>
				<span class="float-right text-muted">
					<?php
					if($student_info_by_id[0]['status'] == '1')
					{
						echo '<span class="badge badge-success">Yes</span>';
					}
					else
					{
						echo '<span class="badge badge-danger">No</span>';
					}
					?>
				  </span>
			</p>
			<p class="clearfix">
				  <span class="float-left">
					Guardian Mobile
				  </span>
				<span class="float-right text-muted">
					<?php echo $student_info_by_id[0]['guardian_mobile']; ?>
				  </span>
			</p>
			<p class="clearfix">
                          <span class="float-left">
                            Mail
                          </span>
				<span class="float-right text-muted">
                            <?php echo $student_info_by_id[0]['email']; ?>
				</span>
			</p>
			<p class="clearfix">
				  <span class="float-left">
					Process Code
				  </span>
				<span class="float-right text-muted">
				   <?php echo $student_info_by_id[0]['process_code']; ?>
				  </span>
			</p>
			<p class="clearfix">
				  <span class="float-left">
					Academic Year
				  </span>
				<span class="float-right text-muted">
					<?php echo $student_info_by_id[0]['year']; ?>
				  </span>
			</p>
		</div>
	</div>
	<div class="col-lg-8">
		<div class="d-flex justify-content-between">
			<div>
				<i class="ion ion-md-person"></i> <b>General Information</b>
			</div>
		</div>

		<div class="mt-4 py-2 border-top">
			<div class="col-md-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title"><?php echo $student_info_by_id[0]['name']; ?></h4>
						<div class="template-demo">
							<table class="table mb-0">
								<tbody>
								<tr>
									<td class="pl-0">Student ID</td>
									<td class="pr-0 text-right"><div class="badge badge-primary"><?php echo $student_info_by_id[0]['student_code']; ?></div></td>
								</tr>
								<tr>
									<td class="pl-0">Roll No.</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['roll_no']; ?></td>
								</tr>
								<tr>
									<td class="pl-0">Registration No.</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['reg_no']; ?></td>
								</tr>
								<tr>
									<th class="pl-0">Class</th>
									<th class="text-right"><?php echo $student_info_by_id[0]['class_name']; ?></th>
								</tr>

								<tr>
									<td class="pl-0">Shift</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['shift_name']; ?></td>
								</tr>
								<tr>
									<td class="pl-0">Section</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['section_name']; ?></td>
								</tr>
								<tr>
									<td class="pl-0">Group</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['group_name']; ?></td>
								</tr>
								<tr>
									<td class="pl-0">Process Code</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['process_code']; ?></td>
								</tr>
								<tr>
									<td class="pl-0">Date of Birth</td>
									<td class="pr-0 text-right"><?php echo date("d-m-Y", strtotime($student_info_by_id[0]['date_of_birth'])); ?></td>
								</tr>
								<tr>
									<td class="pl-0">Date of Admission</td>
									<td class="pr-0 text-right"><?php echo date("d-m-Y", strtotime($student_info_by_id[0]['date_of_admission'])); ?></td>
								</tr>
								<tr>
									<td class="pl-0">Gender</td>
									<td class="pr-0 text-right">
										<?php
										if($student_info_by_id[0]['gender'] == 'M'){
											echo 'Male';
										}else if($student_info_by_id[0]['gender'] == 'F'){
											echo 'Female';
										}else{
											echo 'Other';
										}
										?>
									</td>
								</tr>
								<tr>
									<td class="pl-0">Religion</td>
									<td class="pr-0 text-right">
										<?php
										echo $student_info_by_id[0]['religion_name'];
										?>
									</td>
								</tr>

								<tr>
									<td class="pl-0">Blood Group</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['blood_group']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Email</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['email']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Father's Name</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['father_name']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Mother's Name</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['mother_name']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Mother's National ID</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['mother_nid']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Guardian Mobile</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['guardian_mobile']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Guardian Mobile</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['second_guardian_mobile']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Student Mobile</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['student_mobile']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Birth Certificate No.</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['birth_certificate_no']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Present Address</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['present_address']; ?></td>
								</tr>

								<tr>
									<td class="pl-0">Permanent Address</td>
									<td class="pr-0 text-right"><?php echo $student_info_by_id[0]['permanent_address']; ?></td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
	#sortable-table-1 td {
		font-size: 0.80rem !important;
	}
	#sortable-table-1 th {
		font-size: 0.80rem !important;
		font-weight: bold;
	}
</style>

<div class="row col-lg-12">
	<div class="d-flex justify-content-between border-bottom col-lg-12">
		<div>
			<i class="ion ion-md-add"></i> <b>Fees Information for <?php echo $student_info_by_id[0]['year']; ?></b>
		</div>
	</div>
	<?php
	echo $fess_data_report;
	?>
</div>


<div class="row col-lg-12">
	<div class="d-flex justify-content-between border-bottom col-lg-12">
		<div>
			<i class="ion ion-md-star"></i> <b>Result Information for <?php echo $student_info_by_id[0]['year']; ?></b>
		</div>
	</div>

	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table width="100%" id="sortable-table-1" class="table">
			<thead>
			<tr>
				<th style="text-align:center;">Exam</th>
				<th style="text-align:center;">Total Credit</th>
				<th style="text-align:center;">Total Obtain</th>
				<th style="text-align:center;">Letter Grade</th>
				<th style="text-align:center;">Grade Point</th>
				<th style="text-align:center;">Class Position</th>
			</tr>
			</thead>

			<tbody>
			<?php
			foreach ($result_info_for_current_year as $row){
				?>
				<tr>

					<td style="text-align:left;">
						<?php echo $row['name']; ?>
					</td>

					<td style="text-align:center;">
						<?php echo $row['total_credit']; ?>
					</td>

					<td style="text-align:center;">
						<?php echo round($row['total_obtain_mark'],2); ?>
					</td>

					<td style="text-align:center;">
						<?php echo $row['c_alpha_gpa_with_optional']; ?>
					</td>

					<td style="text-align:center;">
						<?php echo round($row['gpa_with_optional'],2); ?>
					</td>

					<td style="text-align:center;">
						<?php echo $row['class_position']; ?>
					</td>

				</tr>
				<?php
			}
			?>
			</tbody>
		</table>
	</div>

</div>



