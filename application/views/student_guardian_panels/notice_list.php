<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th>SL</th>
			<th>Title</th>
			<th>Date</th>
			<th>Notice</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = (int)$this->uri->segment(3);
		foreach ($notices as $row):
			$i++;
			?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $row['title']; ?></td>
				<td><?php echo $row['date']; ?></td>
				<td><a href="<?php echo base_url() . MEDIA_FOLDER; ?>/notice/<?php echo $row['url']; ?>">Download</a></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>

