<!doctype html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
    
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>/core_media/css/login_css/school360-favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?php echo $title; ?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>/core_media/student_guardian_panels/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url(); ?>/core_media/student_guardian_panels/css/animate.min.css" rel="stylesheet"/>
    <!--  Light Bootstrap Table core CSS    -->
    <link href="<?php echo base_url(); ?>/core_media/student_guardian_panels/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>/core_media/student_guardian_panels/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="azure">

        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="<?php echo base_url(); ?>student_guardian_panels/index" class="simple-text">
                    <span style="color:#0059b3;">School</span><span style="color:#ff8080;">360</span>
                </a>
            </div>

            <ul class="nav">
                <?php echo $left_menu; ?>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <p>
                                    <?php
                                    $student_info = $this->session->userdata('session_student_info');
                                    echo $student_info[0]->name;
                                    ?>
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>student_guardian_panels/get_student_info">Profile</a></li>
                                <li><a href="<?php echo base_url(); ?>student_guardian_panels/change_password">Change Password</a></li>
                                <li><a href="<?php echo base_url(); ?>login/student_guardian_logout">Log out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <?php echo $maincontent; ?>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="<?php echo base_url(); ?>student_guardian_panels/index">
                                School360°
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    Copyright @ 2015-<?php echo date('Y'); ?> <a href="https://school360.app"> School360</a>
                </p>
            </div>
        </footer>
    </div>
</div>


</body>

<!--   Core JS Files   -->
<script src="<?php echo base_url(); ?>/core_media/student_guardian_panels/js/jquery-1.10.2.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/core_media/student_guardian_panels/js/bootstrap.min.js"
        type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="<?php echo base_url(); ?>/core_media/student_guardian_panels/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="<?php echo base_url(); ?>/core_media/student_guardian_panels/js/chartist.min.js"></script>


<!--  Notifications Plugin    -->
<script src="<?php echo base_url(); ?>/core_media/student_guardian_panels/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="<?php echo base_url(); ?>/core_media/student_guardian_panels/js/light-bootstrap-dashboard.js"></script>




<script type="text/javascript">
    $(document).ready(function () {

        demo.initChartist();

        $.notify({
            icon: 'pe-7s-gift',
            message: "Welcome to <b>School360  Dashboard</b> - a complete school/college management system."

        }, {
            type: 'info',
            timer: 4000
        });

    });
</script>

</html>