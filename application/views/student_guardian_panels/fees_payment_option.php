<form  name="addForm" id="commentForm"  class="cmxform" method="post" action="<?php echo base_url(); ?>fee_collections/fee_collect_process_from_student_portal">
	<div class="form-group">
		<select style="width:220px !important;" class="js-example-basic-multiple w-100" multiple name="month[]" id="month" required="1">
			<?php
			$i = 1;
			while ($i <= 12) {
				$dateObj = DateTime::createFromFormat('!m', $i); ?>
				<option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
					echo 'selected';
				} ?>><?php echo $dateObj->format('F'); ?></option>
				<?php
				$i++;
			}
			?>
		</select>
	</div>
	<input type="hidden" name="student_id" value="<?php echo $student_info[0]['id']; ?>">
	<input type="hidden" name="class_id" value="<?php echo $student_info[0]['class_id']; ?>">
	<input type="hidden" name="shift_id" value="<?php echo $student_info[0]['shift_id']; ?>">
	<input type="hidden" name="section_id" value="<?php echo $student_info[0]['section_id']; ?>">
	<input type="hidden" name="group_id" value="<?php echo $student_info[0]['group']; ?>">
	<input type="hidden" name="student_category_id" value="<?php echo $student_info[0]['category_id']; ?>">


	<div class="float-right">
		<button type='submit' class="btn btn-primary">
			<?php echo $this->lang->line('process'); ?> &nbsp;
			<i class="icon ion-md-menu float-right"></i>
		</button>
	</div>

</form>
