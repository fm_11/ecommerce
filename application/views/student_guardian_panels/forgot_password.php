<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Forgot Password School360 v4.0.1</title>
	<!-- base:css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/css/vendor.bundle.base.css">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/css/vertical-layout-light/style.css">
	<!-- endinject -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>core_media/admin_v3/images/favicon.png" />
</head>

<body class="sidebar-icon-only">
<div class="container-scroller">
	<div class="container-fluid page-body-wrapper full-page-wrapper">
		<div class="content-wrapper d-flex align-items-center auth px-0">
			<div class="row w-100 mx-0">
				<div class="col-lg-4 mx-auto">
					<div class="auth-form-light text-left py-5 px-4 px-sm-5">
						<div class="brand-logo">
							<img src="<?php echo base_url(); ?>core_media/admin_v3/images/school360-logo.png" alt="logo">
							<h6 class="font-weight-light">শিক্ষা প্রতিষ্ঠানের জন্য সব </h6>
						</div>
						<h4><?php echo $school_name; ?></h4>
						<h6 class="font-weight-light">Forgot Password <span style="color:#0059b3;">School</span><span style="color:#ff8080;">360</span> v4.0.1</h6>
						<form  method="post" action="<?php echo base_url(); ?>login/student_forgot_password" class="pt-3">

							<?php
							$message = $this->session->userdata('message');
							if ($message != '') {
								?>
								<div class="alert alert-success" role="alert">
									<?php
									echo $message;
									$this->session->unset_userdata('message'); ?>
								</div>
								<?php
							}
							?>

							<?php
							$exception = $this->session->userdata('exception');
							if ($exception != '') {
								?>
								<div class="alert alert-danger" role="alert">
									<?php
									echo $exception;
									$this->session->unset_userdata('exception'); ?>
								</div>
								<?php
							}
							?>
							<div class="form-group">
								<input type="text" required class="form-control form-control-lg" id="exampleInputEmail1" name="student_code" placeholder="Student ID">
							</div>

							<div class="mt-3">
								<input class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit" name="submit" value="SEND PASSWORD">
							</div>
							<div class="my-2 d-flex justify-content-between align-items-center">
								<div class="form-check">
									<label class="form-check-label text-muted">
										&nbsp;
									</label>
								</div>
								<a href="<?php echo base_url(); ?>login/student" class="auth-link text-black">Back to Login</a>
							</div>

							<div class="text-center mt-4 font-weight-light">
								<span>Copyright &copy;
									<a href="https://spatei.com/" target="_blank" class="text-primary">Spate Initiative Limited</a>
									</span>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- content-wrapper ends -->
	</div>
	<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- base:js -->
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/off-canvas.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/hoverable-collapse.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/template.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/settings.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/todolist.js"></script>
<!-- endinject -->
</body>

</html>
