<li <?php
if ($active_menu == 'dashboard') {
	echo 'class="active"';
}
?>>
	<a href="<?php echo base_url(); ?>student_guardian_panels/index">
		<i class="pe-7s-graph"></i>
		<p>Dashboard</p>
	</a>
</li>

<li <?php
if($active_menu == 'student_info'){
	echo 'class="active"';
}
?>>
	<a href="<?php echo base_url(); ?>student_guardian_panels/get_student_info">
		<i class="pe-7s-user"></i>
		<p>Student Profile</p>
	</a>
</li>

<li <?php
if($active_menu == 'leave_info'){
	echo 'class="active"';
}
?>>
	<a href="<?php echo base_url(); ?>student_guardian_panels/get_student_leave_info">
		<i class="pe-7s-note2"></i>
		<p>Student Leave</p>
	</a>
</li>

<li <?php
if($active_menu == 'student_fees'){
	echo 'class="active"';
}
?>>
	<a href="<?php echo base_url(); ?>student_guardian_panels/get_student_fees_info">
		<i class="pe-7s-news-paper"></i>
		<p>Student Fees</p>
	</a>
</li>



<li <?php
if($active_menu == 'timekeeping'){
	echo 'class="active"';
}
?>>
	<a href="<?php echo base_url(); ?>student_guardian_panels/timekeeping_report">
		<i class="pe-7s-science"></i>
		<p>Timekeeping Report</p>
	</a>
</li>



<li <?php
if($active_menu == 'message'){
	echo 'class="active"';
}
?>>
	<a href="<?php echo base_url(); ?>student_guardian_panels/get_all_notice">
		<i class="pe-7s-bell"></i>
		<p>Messages/Notice</p>
	</a>
</li>
