<form name="addForm" id="commentForm"   class="cmxform" action="<?php echo base_url(); ?>student_guardian_panels/get_student_fees_info" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('year'); ?></label><br>
			<select class="js-example-basic-single w-100" name="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option value="<?php echo $list['value']; ?>" <?php if (isset($year)) {
							if ($year == $list['value']) {
								echo 'selected';
							}
						} ?>><?php echo $list['text']; ?></option>
						<?php
					}
				}
				?>
			</select>
			<input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="Preview">
	</div>
</form>

<?php
if(isset($rdata)){
	echo $report;
}
?>





