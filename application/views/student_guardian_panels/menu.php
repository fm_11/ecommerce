<?php
$controller = $this->uri->segment(1);
$method = $this->uri->segment(2);
?>
<ul class="nav">
	<li class="nav-item <?php if ($controller == 'student_guardian_panels' && $method == 'index') { ?>active<?php } ?>">
		<a class="nav-link" href="<?php echo base_url(); ?>student_guardian_panels/index">
			<i class="ion ion ion-ios-archive menu-icon"></i>
			<span class="menu-title"><?php echo $this->lang->line('dashboard'); ?></span>
		</a>
	</li>

	<li class="nav-item  <?php if ($controller == 'student_guardian_panels' && $method == 'fees_payment') { ?>active<?php } ?>">
		<a class="nav-link" href="<?php echo base_url(); ?>student_guardian_panels/fees_payment">
			<i class="ion ion-md-cash menu-icon"></i>
			<span class="menu-title"><?php echo $this->lang->line('fees'); ?> Payment</span>
		</a>
	</li>

	<li class="nav-item  <?php if ($controller == 'student_guardian_panels' && $method == 'get_student_fees_info') { ?>active<?php } ?>">
		<a class="nav-link" href="<?php echo base_url(); ?>student_guardian_panels/get_student_fees_info">
			<i class="ion ion-md-paper menu-icon"></i>
			<span class="menu-title"><?php echo $this->lang->line('fees'); ?> Report</span>
		</a>
	</li>


	<li class="nav-item  <?php if ($controller == 'student_guardian_panels' && $method == 'timekeeping_report') { ?>active<?php } ?>">
		<a class="nav-link" href="<?php echo base_url(); ?>student_guardian_panels/timekeeping_report">
			<i class="ion ion-md-time menu-icon"></i>
			<span class="menu-title">Attendance Report</span>
		</a>
	</li>


	<li class="nav-item <?php if ($controller == 'student_guardian_panels' && $method == 'get_all_notice') { ?>active<?php } ?>">
		<a class="nav-link" href="<?php echo base_url(); ?>student_guardian_panels/get_all_notice">
			<i class="ion ion-md-list-box menu-icon"></i>
			<span class="menu-title">Notice</span>
		</a>
	</li>


	<li class="nav-item <?php if ($controller == 'student_guardian_panels' && $method == 'get_student_info') { ?>active<?php } ?>">
		<a class="nav-link" href="<?php echo base_url(); ?>student_guardian_panels/get_student_info">
			<i class="ion ion-md-happy menu-icon"></i>
			<span class="menu-title">Profile</span>
		</a>
	</li>

	<li class="nav-item <?php if ($controller == 'student_guardian_panels' && $method == 'change_password') { ?>active<?php } ?>">
		<a class="nav-link" href="<?php echo base_url(); ?>student_guardian_panels/change_password">
			<i class="ion ion-md-settings menu-icon"></i>
			<span class="menu-title">Change Password</span>
		</a>
	</li>

</ul>

