<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo $title; ?></title>
	<!-- base:css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/font-awesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/css/vendor.bundle.base.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/select2/select2.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/owl-carousel-2/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/owl-carousel-2/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/jvectormap/jquery-jvectormap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/morris.js/morris.css">
	<!-- End plugin css for this page -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/flag-icon-css/css/flag-icon.min.css"/>

	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">

	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/ti-icons/css/themify-icons.css">
	<!-- inject:css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/css/vertical-layout-light/style.css">
	<!-- endinject -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>core_media/admin_v3/images/favicon.png" />

	<style>
		input[readonly] {
			background-color: #e3e5e8;
		}
		.required_label {
			color:red;
		}
	</style>
</head>
<?php
$student_info = $this->session->userdata('session_student_info');
?>
<body class="sidebar-fixed sidebar-dark">
<div class="container-scroller">
	<!-- partial:partials/_navbar.html -->
	<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-primary">
		<div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
			<a class="navbar-brand brand-logo" href="<?php echo base_url(); ?>dashboard/index">
				<img src="<?php echo base_url(); ?>core_media/admin_v3/images/school360-logo.png" alt="logo"/>
			</a>
			<a class="navbar-brand brand-logo-mini" href="<?php echo base_url(); ?>dashboard/index">
				<img src="<?php echo base_url(); ?>core_media/admin_v3/images/school360-logo.png" alt="logo"/>
			</a>
		</div>
		<div class="navbar-menu-wrapper">
			<div id="top_bar_background" class="navbar-menu-wrapper-inner d-flex align-items-center justify-content-end">
				<button id="top_menu_show_hide_button" class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
					<span class="icon ion-md-menu"></span>
				</button>

				<ul class="navbar-nav mr-lg-2">
					<li class="nav-item d-none d-lg-block">
						<b style="color: white;">
							<?php echo $student_info[0]->contact_info[0]['school_name']; ?>
						</b>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url(); ?>student_guardian_panels/fees_payment">
							<button style="padding: .5rem 0.5rem;border: 1px #ffffff solid;color: #ffffff;" class="btn btn-outline-primary btn-sm" type="button">
								Fees Payment
							</button>
						</a>
					</li>

				</ul>

				<ul class="navbar-nav navbar-nav-right">
					<li class="nav-item nav-profile dropdown">
						<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
							<span class="nav-profile-name"> <?php  echo $student_info[0]->name; ?></span>
							<?php if($student_info[0]->photo == '' || $student_info[0]->photo == 'default.png'){ ?>
								<img src="<?php echo base_url(); ?>core_media/admin_v3/images/faces/user.png" alt="profile"/>
							<?php }else{ ?>
								<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $student_info[0]->photo; ?>" alt="profile"/>
							<?php } ?>

						</a>
						<div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
							<a href="<?php echo base_url(); ?>student_guardian_panels/change_password" class="dropdown-item">
								<i class="icon ion-md-settings text-primary"></i>
								<?php echo $this->lang->line('password_change'); ?>
							</a>
							<a href="<?php echo base_url(); ?>login/student_guardian_logout" class="dropdown-item">
								<i class="ion ion-md-log-out text-primary"></i>
								<?php echo $this->lang->line('logout'); ?>
							</a>
						</div>
					</li>
				</ul>
				<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
					<span class="icon ion-md-menu"></span>
				</button>
			</div>
		</div>
	</nav>
	<!-- partial -->
	<div class="container-fluid page-body-wrapper">
		<nav class="sidebar sidebar-offcanvas" id="sidebar">
			<?php echo $left_menu; ?>
		</nav>
		<!-- partial -->
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="row">
				<div class="col-md-12 grid-margin stretch-card">
						<div class="card">
							<div class="card-body">
								<?php
								$message = $this->session->userdata('message');
								if ($message != '') {
									?>
									<div class="alert alert-success" role="alert">
										<i class="ion ion-md-alert"></i>
										<?php
										echo $message;
										$this->session->unset_userdata('message'); ?>
									</div>
									<?php
								}
								?>

								<?php
								$exception = $this->session->userdata('exception');
								if ($exception != '') {
									?>
									<div class="alert alert-danger" role="alert">
										<i class="ion ion-md-alert"></i>
										<?php
										echo $exception;
										$this->session->unset_userdata('exception');
										?>
									</div>
									<?php
								}
								?>
								<h4 class="card-title"><?php echo $title; ?></h4>
								<?php echo $maincontent; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- content-wrapper ends -->
			<!-- partial:partials/_footer.html -->
			<footer class="footer mr-0 ml-0">
				<div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">
              Copyright © <a href="https://spatei.com/" target="_blank">
                Spate Initiative Limited
              </a>. All rights reserved.</span>
					<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"><?php echo $this->lang->line('spatei_title'); ?> <i class="ion ion-md-heart-empty"></i></span>
				</div>
			</footer>
			<!-- partial -->
		</div>
		<!-- main-panel ends -->
	</div>
	<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- base:js -->
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/jq.tablesort.js"></script>
<!-- inject:js -->
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/off-canvas.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/hoverable-collapse.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/template.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/settings.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/todolist.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/owl-carousel-2/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- End plugin js for this page -->
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Custom js for this page-->

<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/morris.js/morris.min.js"></script>

<script src="<?php echo base_url(); ?>core_media/js/fees_collection.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/dashboard.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/chart.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/morris.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/todolist.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/scripts.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<!-- End custom js for this page-->

<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/tinymce/tinymce.min.js"></script>


<script src="<?php echo base_url(); ?>core_media/admin_v3/js/formpickers.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/tablesorter.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/typeahead.js/typeahead.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/select2/select2.min.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/typeahead.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/select2.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/form-validation.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/bt-maxLength.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/calendar.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/editorDemo.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/modal-demo.js"></script>
</body>

</html>
