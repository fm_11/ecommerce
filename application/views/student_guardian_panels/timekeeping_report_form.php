<form name="addForm" id="commentForm"   class="cmxform" action="<?php echo base_url(); ?>student_guardian_panels/timekeeping_report" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Month</label>
			<select class="js-example-basic-single w-100" name="month" id="month" required="1">
				<option value="">-- Please Select --</option>
				<?php
				$i = 1;
				while ($i <= 12) {
					$dateObj = DateTime::createFromFormat('!m', $i);
					?>
					<option value="<?php echo $i; ?>" <?php if(isset($month)){ if($month == $i){ echo 'selected'; } } ?>><?php echo $dateObj->format('F'); ?></option>
					<?php
					$i++;
				}
				?>
			</select>
			<input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
		</div>

		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('year'); ?></label>
			<select class="js-example-basic-single w-100" name="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option value="<?php echo $list['value']; ?>" <?php if (isset($year)) {
							if ($year == $list['value']) {
								echo 'selected';
							}
						} ?>><?php echo $list['text']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Class Period</label>
			<select name="period_id" id="period_id" class="js-example-basic-single w-100" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<option value="FM">From Machine (Login & Logout)</option>
				<?php foreach ($period_list as $row) { ?>
					<option value="<?php echo $row['id']; ?>" <?php if(isset($period_id)){ if($period_id == $row['id']){ echo 'selected'; } } ?>><?php echo $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="Preview">
	</div>

</form>

<?php
if (isset($timekeeping_info)) {
	?>
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<thead>
			<tr>
				<th>SL</th>
				<th>Date</th>
				<th>Login Status</th>
			</tr>
			</thead>
			<tbody>

			<?php
			$i = 0;
			foreach ($timekeeping_info as $row):
				$i++;
				?>
				<tr>

				<tr>
					<td>
						<?php echo $i; ?>
					</td>
					<td><?php echo $row['date']; ?></td>
					<td>
						<?php
						if($row['login_status'] == 'P'){
							echo 'Present';
						}else if($row['login_status'] == 'A'){
							echo 'Absent';
						}else{
							echo 'Leave';
						}
						?>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php
}
?>



