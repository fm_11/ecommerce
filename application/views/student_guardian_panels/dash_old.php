<div class="col-md-6">
	<div class="card">
		<div class="header">
			<h4 class="title">Student Attendance Status</h4>
			<p class="category"> <?php echo date('M',strtotime('2012-'.date('m').'-01')).', '.date('Y'); ?> &nbsp;</p>
		</div>
		<div class="content">
			<div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>

			<div class="footer">
				<div class="legend">
					<i class="fa fa-circle text-info"></i> Present
					<i class="fa fa-circle text-danger"></i> Absent
					<i class="fa fa-circle text-warning"></i> Leave
				</div>
				<hr>
				<div class="stats">
					<i class="fa fa-clock-o"></i> This is verified data by school.
				</div>
			</div>
		</div>
	</div>
</div>
<div class="col-md-6">
	<div class="card ">
		<div class="header">
			<h4 class="title">Notice</h4>
			<p class="category">Last 10 Notice from School</p>
		</div>
		<div class="content">
			<div class="table-full-width">
				<table class="table">
					<tbody>
					<?php
					$i = (int)$this->uri->segment(3);
					foreach ($dashboard_notice as $row):
						$i++;
						?>
						<tr>
							<td>
								<label class="checkbox">
									<?php echo $i; ?>
								</label>
							</td>
							<td><?php echo $row['title']; ?> (<?php echo $row['date']; ?>)</td>
							<td class="td-actions text-right">
								<a href="<?php echo base_url() . MEDIA_FOLDER; ?>/notice/<?php echo $row['url']; ?>">
									<button type="button" rel="tooltip" title="Download or View Notice"
											class="btn btn-info btn-simple btn-xs">
										<i class="fa fa-edit"></i>
									</button>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>

			<div class="footer">
				<hr>
				<div class="stats">
					<i class="fa fa-history"></i> Updated 3 minutes ago
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	type = ['','info','success','warning','danger'];


	demo = {
		initPickColor: function(){
			$('.pick-class-label').click(function(){
				var new_class = $(this).attr('new-class');
				var old_class = $('#display-buttons').attr('data-class');
				var display_div = $('#display-buttons');
				if(display_div.length) {
					var display_buttons = display_div.find('.btn');
					display_buttons.removeClass(old_class);
					display_buttons.addClass(new_class);
					display_div.attr('data-class', new_class);
				}
			});
		},

		initChartist: function(){

			var dataSales = {
				labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
				series: [
					[287, 385, 490, 492, 554, 586, 698, 695, 752, 788, 846, 944],
					[67, 152, 143, 240, 287, 335, 435, 437, 539, 542, 544, 647],
					[23, 113, 67, 108, 190, 239, 307, 308, 439, 410, 410, 509]
				]
			};

			var optionsSales = {
				lineSmooth: false,
				low: 0,
				high: 800,
				showArea: true,
				height: "245px",
				axisX: {
					showGrid: false,
				},
				lineSmooth: Chartist.Interpolation.simple({
					divisor: 3
				}),
				showLine: false,
				showPoint: false,
			};

			var responsiveSales = [
				['screen and (max-width: 640px)', {
					axisX: {
						labelInterpolationFnc: function (value) {
							return value[0];
						}
					}
				}]
			];

			Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);


			var data = {
				labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
				series: [
					[542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895],
					[412, 243, 280, 580, 453, 353, 300, 364, 368, 410, 636, 695]
				]
			};

			var options = {
				seriesBarDistance: 10,
				axisX: {
					showGrid: false
				},
				height: "245px"
			};

			var responsiveOptions = [
				['screen and (max-width: 640px)', {
					seriesBarDistance: 5,
					axisX: {
						labelInterpolationFnc: function (value) {
							return value[0];
						}
					}
				}]
			];

			Chartist.Bar('#chartActivity', data, options, responsiveOptions);

			var dataPreferences = {
				series: [
					[25, 30, 20, 25]
				]
			};

			var optionsPreferences = {
				donut: true,
				donutWidth: 40,
				startAngle: 0,
				total: 100,
				showLabel: false,
				axisX: {
					showGrid: false
				}
			};

			Chartist.Pie('#chartPreferences', dataPreferences, optionsPreferences);

			Chartist.Pie('#chartPreferences', {
				labels: ['62%','32%','6%'],
				series: [<?php echo $present; ?>, <?php echo $absent; ?>, <?php echo $leave; ?>]
			});
		},

		initGoogleMaps: function(){
			var myLatlng = new google.maps.LatLng(40.748817, -73.985428);
			var mapOptions = {
				zoom: 13,
				center: myLatlng,
				scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
				styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]

			}
			var map = new google.maps.Map(document.getElementById("map"), mapOptions);

			var marker = new google.maps.Marker({
				position: myLatlng,
				title:"Hello World!"
			});

			// To add the marker to the map, call setMap();
			marker.setMap(map);
		},

		showNotification: function(from, align){
			color = Math.floor((Math.random() * 4) + 1);

			$.notify({
				icon: "pe-7s-gift",
				message: "Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for every web developer."

			},{
				type: type[color],
				timer: 4000,
				placement: {
					from: from,
					align: align
				}
			});
		}


	}


</script>
