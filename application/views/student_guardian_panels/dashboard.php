<div class="row">
	<div class="col-xl-6 d-flex grid-margin stretch-card">
		<div class="card">
			<div class="card-body py-6 class_wise_student_border">
				<h5 class="card-subtitle-lg">Student Attendance Status <?php echo date('M',strtotime('2012-'.date('m').'-01')).', '.date('Y'); ?></h5>
				<input type="hidden" id="students_chart_level" value="<?php echo $chart_level; ?>">
				<input type="hidden" id="students_chart_color"  value="<?php echo $chart_color; ?>">
				<input type="hidden" id="students_chart_value"  value="<?php echo $chart_value; ?>">
				<canvas id="order-by-location"></canvas>
			</div>
		</div>
	</div>

	<div class="col-xl-6 d-flex grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h3 class="text-dark card-title">Last 10 Notice from School</h3>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th class="border-0 pt-0">
								Title
							</th>

							<th class="border-0 pt-0">
								View
							</th>

						</tr>
						</thead>
						<tbody>
						<?php
						$i = 0;
						foreach ($dashboard_notice as $row):
							$i++;
							?>
							<tr>
								<td class="pl-0">
									<?php echo $row['title']; ?>
								</td>

								<td>
									<a href="<?php echo base_url() . MEDIA_FOLDER; ?>/notice/<?php echo $row['url']; ?>">
										<button type="button" rel="tooltip" title="Download or View Notice"
												class="btn btn-info btn-simple btn-xs">
											<i class="fa fa-edit"></i>
										</button>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


</div>
