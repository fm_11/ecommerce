<script>
    function myFunction() {
        var pass1 = document.getElementById("new_pass").value;
        var pass2 = document.getElementById("con_new_pass").value;
        var ok = true;
        if (pass1 != pass2) {
            alert("Password Doesn't Match !");
            document.getElementById("new_pass").style.borderColor = "#E34234";
            document.getElementById("con_new_pass").style.borderColor = "#E34234";
            ok = false;
        }
        return ok;
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>student_guardian_panels/change_password" class="cmxform" method="post">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Student Name/ ID</label>
			<input type="text" autocomplete="off"  class="form-control" disabled
				   value="<?php echo $name; ?> (<?php echo $student_code; ?>)">
			<input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
		</div>
		<div class="form-group col-md-6">
			<label>Current Password</label>
			<input type="password" class="form-control" autocomplete="off" name="current_pass" id="current_pass" required>
		</div>
	</div>


	<div class="form-row">
		<div class="form-group col-md-6">
			<label>New Password</label>
			<input type="password" class="form-control" name="new_pass" autocomplete="off"  id="new_pass" required>
		</div>
		<div class="form-group col-md-6">
			<label>Confirm New Password</label>
			<input type="password" class="form-control" autocomplete="off"  name="con_new_pass" id="con_new_pass" required>
		</div>
	</div>

		<div class="float-right">
			<input class="btn btn-primary" type="submit" value="Change">
		</div>
</form>
