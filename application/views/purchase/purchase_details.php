<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?php  echo $title; ?></title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>
  @page { size: A4 }
  .main_content {
    width: 100%;
	height: auto;
    box-sizing: border-box;
	border: 3px #538cc6 solid;
  }


  .body_content{
    width: 100%;
	height: auto;
    box-sizing: border-box;
	border: 3px #ff8080 solid;
  }

    #header{
	 width:100%;
	 box-sizing: border-box;
	 height: 105px;
	}
	.header-left {
		width:80%;
		float:left;
		height: 100%;
		text-align: left;
		display: table;
	}
	.logo {
		width:15%;
		float:left;
		height: 100%;
		box-sizing: border-box;
        text-align: center;
        vertical-align: top;
	}

	.clr{ clear:both;}


   .frame{
        width: 100%;
        height: 100%;
        margin: auto;
        position: relative;
    }
    .log_frame{
        width: 100%;
        height: 120px;
        margin: auto;
        position: relative;
    }


	.img{
        max-height: 85%;
        max-width: 85%;
        position: absolute;
        top: 5px;
        bottom: 0;
        left: 10px;
        right: 0;
    }

	.gpa_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 90%;
	  height: auto;
	  position: relative;
	  top:10px;
	}

	table, td, th {
	  border: 1px solid black;
	  padding: 3px;
	  font-size: 14px;
	}

	#student_info{
	 width:100%;
	 box-sizing: border-box;
	 height: 200px;
	}

	.student_info-left {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
	}

	.student_info_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 98%;
	  height: auto;
	  position: relative;
	}

	#student_exam_info{
	 width:100%;
	 box-sizing: border-box;
	 height: auto;

	}

	.exam_info-left {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
	}

	.mark_info_table{
	  border-collapse: collapse;
	  margin: 0 auto;
	  margin-top:10px;
	  width: 98%;
	  height: auto;
	  position: relative;
	}

	.td_center{
	  text-align: center;
	}

	.td_right{
	  text-align: right;
	}



.icon-bar {
  position: fixed;
  top: 40%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 10px;
  transition: all 0.3s ease;
  color: white;
  font-size: 13px;
  text-decoration:none;
  width:150px;
}

.icon-bar a:hover {
  background-color: #000;
}


  #signature_info{
    width:100%;
   	box-sizing: border-box;
   	height: 100px;
  }


  .signature_left {
		width:33%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
    vertical-align: bottom;
	}
	.signature_middle {
		width:34%;
		float:left;
		height: 100%;
		box-sizing: border-box;
    text-align: center;
    vertical-align: bottom;
	}
	.signature_right {
		width:33%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
    vertical-align: bottom;
	}

	.clear:before, .clear:after {
    content: "\0020";
    display: block;
    height: 0;
    overflow: hidden;
}

.clear:after {
    clear: both;
}

.td_backgroud{
	  background-color: #d9e6f2;
    font-weight: bold;
	}

  .sheet {
  overflow: visible;
  height: auto !important;
}


.reortPrintOption{
  float: left;
  padding: 5px;
  width: 100%;
}


.printButton:link, .printButton:visited {
      background-color: #f44336;
      color: white;
      padding: 8px 8px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
}

.printButton:hover, .printButton:active {
  background-color: red;
}

.backToButton:link, .backToButton:visited {
      background-color: #a3c2c2;
      color: white;
      padding: 8px 8px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
}

.backToButton:hover, .backToButton:active {
  background-color: #527a7a;
}


  </style>

  <script>
      function printDiv(divName) {
          var printContents = document.getElementById(divName).innerHTML;
          var originalContents = document.body.innerHTML;
          document.body.innerHTML = printContents;
          window.print();
          document.body.innerHTML = originalContents;
      }
  </script>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">

  <div class="reortPrintOption">
      <a class="backToButton" href="<?php echo base_url(); ?>purchase/add">
        Back to Add
      </a>
      <a class="printButton" href="javascript:void" onclick="printDiv('printableArea')">
       Print
     </a>
  </div>


  <section class="sheet padding-5mm" id="printableArea">

    <div class="main_content clear" id="main_content">
	    <div class="body_content clear" id="body_content">
		   <div id="header" class="header">
			 <div class="logo">
				<div class="frame">
					 <img class="img" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info->picture; ?>" />
				</div>
			 </div>
			 <div class="header-left">
			    <span style="display: table-cell;vertical-align: top;text-align: left;">
					<span style="font-size: 18px;
    font-weight: bold;"><?php echo $school_info->school_name; ?></span><br>
					<?php echo $school_info->address; ?>
				</span>
			 </div>
             <div class="clr"></div>
			</div>

			<hr>

			<div class="student_info">
			   <div class="student_info-left">
			     <table class="student_info_table">
				  <tr>
				    <td style="padding: 7px;" colspan="2" align="center"><b>Purchase Details<b/></td>
				  </tr>
				  <tr>
					<td width="50%" style="padding: 7px; line-height:1.6;">
					<b>  <?php echo $this->lang->line('supplier'); ?> :</b> <?php echo $purchase_master_info[0]['supplier_name'].'('.$purchase_master_info[0]['mobile'].')'; ?><br>
					 	<b>  <?php echo $this->lang->line('address'); ?> :</b> <?php echo $purchase_master_info[0]['address']; ?><br>
					  	<b> <?php echo $this->lang->line('purchase').' '.$this->lang->line('date'); ?> :</b> <?php echo $purchase_master_info[0]['purchase_date']; ?><br>
					 	<b>  <?php echo $this->lang->line('purchase_no'); ?> :</b> <?php echo $purchase_master_info[0]['purchase_no']; ?>
					</td>

				  </tr>
				  </table>
			   </div>
			   <div class="clr"></div>
			</div>


			<div class="student_exam_info">
			   <div class="exam_info-left">
					<table class="mark_info_table">
            <tr>
					     <td width="5%" class="td_center td_backgroud">
						    <b><?php echo $this->lang->line('sl'); ?></b>
						 </td>
             <td width="30%" class="td_left td_backgroud">
						     <b><?php echo $this->lang->line('product'); ?></b>
						 </td>
             <td width="16%" class="td_right td_backgroud">
						     <b><?php echo $this->lang->line('quantity'); ?></b>
						 </td>
             <td width="16%" class="td_right td_backgroud">
						     <b><?php echo $this->lang->line('unit_price'); ?></b>
						 </td>
             <td width="16%" class="td_right td_backgroud">
						     <b><?php echo $this->lang->line('total').' '.$this->lang->line('price'); ?></b>
						 </td>
					  </tr>

            <?php
            $total_quantity_amount = 0;
            $total_discount_amount = 0;
                $i = 0;
                foreach ($purchase_detail_info as $row):
                    $i++;
                    ?>
                    <tr>
                        <td class="td_left"><?php echo $i;?>		</td>
                        <td class="td_left">
                            <?php echo $row['product_name'].'('.$row['product_code'].')'; ?>
                        </td>
                        <td class="td_right">
                            <?php $total_quantity_amount += $row['quantity'];
                            echo number_format($row['quantity'], 2); ?>
                        </td>
                        <td class="td_right">
                            <?php $total_discount_amount += $row['unit_price'];
                           echo number_format($row['unit_price'], 2); ?>
                        </td>
                        <td class="td_right">
                            <?php echo number_format($row['total_amount'], 2); ?>
                        </td>
                    </tr>

                <?php endforeach; ?>
            <tr>
					   <td class=" td_right" colspan="4">
						    <b>Sub-Total</b>
						 </td>

             <td class="td_right ">
						    <?php echo number_format($purchase_master_info[0]['sub_total'], 2); ?>
						 </td>
					  </tr>
            <tr>
                <td colspan="4" class="td_right">
                    <span><b>Discount</b></span>
                    <span>
                         <?php echo number_format($purchase_master_info[0]['discount'], 2); ?>
                    </span>
                    <span><b>%</b>&nbsp;&nbsp;<b>(-)</b></span>
             </td>
              <td class="td_right">
                 <?php echo number_format($purchase_master_info[0]['discount_amount'], 2); ?>
               </td>
            </tr>
            <td colspan="4" class="td_right"><b> Grand-Total</b></td>
            <td class="td_right">
              <?php echo number_format($purchase_master_info[0]['total_amount'], 2); ?>
            </td>
            </tr>
            <tr>
                <td class="td_left" colspan="6">&nbsp;<b><?php echo $this->lang->line('in_words'); ?>: <?php echo $this->numbertowords->convert_number($purchase_master_info[0]['total_amount']); ?><?php echo $this->lang->line('tk').' '.$this->lang->line('only'); ?> .</b></td>
            </tr>
					</table>
				</div>
			</div>

			<!-- <div class="signature_info">
				<div class="signature_left">
					 <?php  if (isset($signature['L'])) {
                 if ($signature['L']['is_use'] == '1') { ?>

					   <img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['R']['location']; ?>" height="60" width="75"/>
						<br>................................<br>
					   <span><?php echo $signature['L']['level']; ?></span>

					 <?php }
             } ?>&nbsp;
				</div>
				<div class="signature_middle">
				  <?php  if (isset($signature['M'])) {
                 if ($signature['M']['is_use'] == '1') { ?>
					<img  style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['R']['location']; ?>"  height="60" width="75"/>
					 <br>................................<br>
					 <span><?php echo $signature['M']['level']; ?></span>
				  <?php }
             } ?>
				&nbsp;
			   </div>
			   <div class="signature_right">
				 <?php  if (isset($signature['R'])) {
                 if ($signature['R']['is_use'] == '1') { ?>
				   <img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['R']['location']; ?>" height="60" width="75"/>
					<br>................................<br>
					<span><?php echo $signature['R']['level']; ?></span>
				 <?php }
             } ?>&nbsp;
			  </div>

		  </div> -->
	    <span style="font-size:12px; float:right;margin-top: 5px;"><b>Powered By: School360 </b>&nbsp;</span>
	</div>
</div>
</section>
</body>

</html>
