<style>
.for_income{
  padding: 7px;
  max-width: 100px;
  border: 1px #000000 solid;
}
</style>
<script>
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
       xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            //document.getElementById("income_category_json_data" + id).innerHTML = xmlhttp.responseText;
            var obj = JSON.parse(xmlhttp.responseText);
           var option_html = "";
            option_html += '<option value="">-- Please Select --</option>'
            var i = 0;
            while (i < obj.length){
                option_html += '<option value="' + obj[i].id + '">' + obj[i].name + '</option>'
                i++;
            }
          //  alert(option_html);
            document.getElementById("inventory_product_json_data").value = option_html;
        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>/purchase/get_inventory_product_for_list", true);
    xmlhttp.send();

    function get_supplier_address(){
      var supplier_id = document.getElementById('supplier_id').value;
      if (window.XMLHttpRequest)
      {
        xmlhttp = new XMLHttpRequest();
      }
      else
      {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange = function()
      {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {

          document.getElementById("address").innerHTML = xmlhttp.responseText;
          $(".js-example-basic-multiple").select2();
        }
      }
      xmlhttp.open("GET", "<?php echo base_url(); ?>purchase/get_supplier_address?supplier_id=" + supplier_id, true);
      xmlhttp.send();
    }
    function delete_row(row_no) {
        var no = Number(row_no);
        document.getElementById("row_" + no + "").outerHTML = "";
        if(no != 1){
            document.getElementById("delete_section_" + (no - 1)).innerHTML = "<input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs' onclick='delete_row(" + (no - 1) + ")'>";
        }
        document.getElementById('num_of_row').value = no;
        document.getElementById('first_row').innerHTML = no;
        calculate_total_amount();
    }

    function add_row() {
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        //alert(baseUrl);
        var table = document.getElementById("my_data_table");
        var table_len = Number(document.getElementById('num_of_row').value);
        var row = table.insertRow(table_len).outerHTML = "<tr id='row_" + table_len + "'><td><span>"+table_len+"</span></td><td><select required class='js-example-basic-single w-100' id='inventory_product_id_" + table_len + "' name='inventory_product_id_" + table_len + "' ></select></td><td><input type='number' min='0' class='for_income text-right' placeholder='quantity' onchange='calculate_total_amount()'' required id='quantity_" + table_len + "' name='quantity_" + table_len + "'></td><td><input type='number' min='0' required class='for_income text-right' onchange='calculate_total_amount()' placeholder='unit price'  name='unit_price_" + table_len + "' id='unit_price_" + table_len + "'></td><td><input type='text' class='for_income text-right readonly-color'  required id='total_amount_" + table_len + "' name='total_amount_" + table_len + "' readonly></td><td id='delete_section_" + table_len +"'><input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs'  onclick='delete_row(" + table_len + ")'></td></tr>";

        if(table_len > 1){
            document.getElementById("delete_section_" + (table_len - 1)).innerHTML = "";
        }

        var inventory_product_json_data = document.getElementById("inventory_product_json_data").value;
        document.getElementById('inventory_product_id_' + table_len).innerHTML = inventory_product_json_data;

        document.getElementById('num_of_row').value = table_len + 1;
        $(".js-example-basic-single").select2();
        document.getElementById('first_row').innerHTML = table_len + 1;
    }
   function change_discount()
   {
     document.getElementById('discount').value=0;
     calculate_total_amount();
   }
    function calculate_total_amount() {

      var totalRow = Number(document.getElementById("num_of_row").value);
      var fixednumber = Number(document.getElementById("fixednumber").value);
      //alert(totalRow);
      var totalAmount = 0;
      var i = 0;
      while (i < totalRow) {
        debugger;
        var txtUnit = 0;
        if(document.getElementById("unit_price_"+i).value!='')
        {
          txtUnit = Number(document.getElementById("unit_price_"+i).value);
        }
        var txtQuantity =0;
        if(document.getElementById("quantity_"+i).value!='')
        {
          txtQuantity = Number(document.getElementById("quantity_"+i).value);
        }

        var totalPrice = Number(txtUnit*txtQuantity);
        document.getElementById("total_amount_"+i).value = totalPrice.toFixed(fixednumber);
        if(document.getElementById("total_amount_" + i).value != ''){
          totalAmount = totalAmount + totalPrice;
        }
        i++;
      }
      document.getElementById("sub_total").value = totalAmount.toFixed(fixednumber);
      var txtdiscount=  Number(document.getElementById("discount").value);
      var originalDiscount=0;
      if(txtdiscount>0)
      {
        originalDiscount = Number(totalAmount * (txtdiscount / 100));
        document.getElementById("discount_amount").value=originalDiscount.toFixed(fixednumber);
      }else{
        originalDiscount=Number(document.getElementById("discount_amount").value);
      }

      document.getElementById("total_amount").value=(totalAmount-originalDiscount).toFixed(fixednumber);
    }

    function calculateDueAmount(total_paid_amount) {
		var total_amount = Number(document.getElementById("total_amount").value);
		if(total_paid_amount > total_amount){
			alert("Cannot be paid more than grand total");
			document.getElementById("total_paid").value = 0;
			document.getElementById("total_due").value = 0;
		}else{
			if(total_amount <= 0){
				alert("Please add product items");
				document.getElementById("total_paid").value = 0;
				document.getElementById("total_due").value = 0;
			}else {
				document.getElementById("total_due").value = total_amount - total_paid_amount;
			}
		}
	}

</script>

<style>
.top-border-remove{
  border-top: 0px !important;
}
</style>

<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>purchase/add" method="post">
  <div class="form-row">
	  <div class="form-group col-md-3">
		  <label><?php echo $this->lang->line('purchase_no'); ?></label>&nbsp;<span class="required_label">*</span>
		  <div class="form-group">
			  <input type="text" autocomplete="off"  name="purchase_no" id="purchase_no" readonly value="<?php echo $purchase_no;?>" required class="form-control">
		  </div>
	  </div>
	  <div class="form-group col-md-3">
		  <label><?php echo $this->lang->line('suppliers'); ?></label>&nbsp;<span class="required_label">*</span>
		  <select onchange="get_supplier_address()" class="js-example-basic-single w-100" name="supplier_id" id="supplier_id" required="1">
		   <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
		   <?php
		   $i = 0;
		   if (count($suppliers)) {
			   foreach ($suppliers as $list) {
				   $i++; ?>
				   <option
						   value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
				   <?php
			   }
		   }
		   ?>
			</select>
	</div>
    <div class="form-group col-md-3">
       <label><?php echo $this->lang->line('date'); ?></label>&nbsp;<span class="required_label">*</span>
       <div class="input-group date">
               <input type="text" autocomplete="off"  name="purchase_date" id="purchase_date" required class="form-control">
               <span class="input-group-text input-group-append input-group-addon">
                   <i class="simple-icon-calendar"></i>
               </span>
       </div>
    </div>
	  <div class="form-group col-md-3">
		  <label><?php echo $this->lang->line('address'); ?></label>
		  <textarea class="form-group col-md-12" id="address" disabled></textarea>
	  </div>
  </div>

	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<tr>
				<th colspan="5">
					<?php echo $this->lang->line('purchase').' '.$this->lang->line('details'); ?>
				</th>
			</tr>
        </table>

    <table class="table" id="my_data_table">
      <tr>
          <th  scope="col" style="min-width:5%;"><?php echo $this->lang->line('sl'); ?></th>
          <th scope="col" style="min-width:200px;"><?php echo $this->lang->line('product'); ?></th>
          <th scope="col" style="min-width:10%; text-align: center;" class="text-center"><?php echo $this->lang->line('quantity'); ?></th>
          <th scope="col" style="min-width:20%;" class="text-center"><?php echo $this->lang->line('unit_price'); ?></th>
          <th scope="col" style="min-width:20%;" class="text-center"><?php echo $this->lang->line('total').' '.$this->lang->line('price'); ?></th>
          <th style="min-width:5%;" scope="col"><?php echo $this->lang->line('actions'); ?></th>
      </tr>
        <tr>
          <td><span id="first_row">1</span></td>
            <td>
                <select name="inventory_product_id_0" class="js-example-basic-single w-100" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php
                    $i = 0;
                    if (count($inventory_product)) {
                        foreach ($inventory_product as $list) {
                            $i++; ?>
                            <option
                                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </td>
            <td><input type="number" min="0" autocomplete="off"  id="quantity_0" required placeholder="quantity"  onchange="calculate_total_amount()"  class="for_income text-right" name="quantity_0"></td>
            <td><input type="number" min="0" autocomplete="off"  class="for_income text-right" placeholder="unit price" required id="unit_price_0" onchange="calculate_total_amount()" name="unit_price_0"></td>
            <td><input type="text" autocomplete="off"  id="total_amount_0" required    class="for_income text-right readonly-color" name="total_amount_0" readonly></td>
            <td><input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="add_row();" value="Add More"></td>
        </tr>
        <tr>
        <td colspan="4" class="text-right top-border-remove"><b> Sub-Total</b></td>
        <td class="top-border-remove"><input type="text" autocomplete="off"  id="sub_total" required    readonly class="for_income text-right readonly-color" name="sub_total" value=""></td>
        </tr>
         <tr>
             <td style="text-align: right; border: none" colspan="4">
                 <span><b>Discount</b></span>
                 <span>
                      <input type="number"  autocomplete="off" min="0" max="100"  id="discount" onchange="calculate_total_amount()" style="max-width: 60px;"    class="for_income text-right" name="discount">
                 </span>
                 <span><b>%</b>&nbsp;&nbsp;<b>(-)</b></span>
               </td>
              <td style="width: 100px; border: none">
              <input type="number" step="any" autocomplete="off"  id="discount_amount"  onchange="change_discount()"    class="for_income text-right" name="discount_amount">
            </td>
         </tr>
         <td colspan="4" class="text-right top-border-remove"><b> Grand-Total</b></td>
         <td class="top-border-remove"><input type="text" autocomplete="off"  id="total_amount" required    readonly class="for_income text-right readonly-color" name="total_amount" value=""></td>
         </tr>

		<tr>
			<td colspan="4" class="text-right top-border-remove"><b>Total Paid</b></td>
			<td class="top-border-remove">
				<input type="text" autocomplete="off" onkeyup="calculateDueAmount(this.value)" id="total_paid" required class="for_income text-right readonly-color" name="total_paid" value="">
			</td>
		</tr>

		<tr>
			<td colspan="4" class="text-right top-border-remove"><b>Total Due</b></td>
			<td class="top-border-remove">
				<input type="text" autocomplete="off" readonly id="total_due" required class="for_income text-right readonly-color" name="total_due" value="">
			</td>
		</tr>

    </table>
  </div>

    <input type="hidden" id="inventory_product_json_data" value="">
    <input type="hidden" id="num_of_row" name="num_of_row" value="1">
    <input type="hidden" id="fixednumber" name="fixednumber" value="2">
    <div class="float-right">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
