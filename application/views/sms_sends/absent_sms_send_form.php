<form name="addForm"  class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>sms_sends/absent_sms_send" method="post">
		<div class="form-row">
			<div class="form-group col-md-4">
				<label>Shift&nbsp;<span class="required_label">*</span></label><br>
				<select  class="js-example-basic-single w-100" name="shift_id" required="1">
					<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					<?php
                    if (count($shifts)) {
                        foreach ($shifts as $list) {
                        	?>
							<option value="<?php echo $list['id']; ?>" <?php if(isset($shift_id)){
								if($shift_id == $list['id']){ echo 'selected'; }
							} ?>><?php echo $list['name']; ?></option>
							<?php
                        }
                    }
                    ?>
				</select>
		</div>

		<div class="form-group col-md-4">
			<label for="date"><?php echo $this->lang->line('date'); ?>&nbsp;<span class="required_label">*</span></label><br>
			<div class="input-group date">
				<input type="text" autocomplete="off" required value="<?php echo $date; ?>" name="txtDate"  id="txtDate" class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                         <i class="simple-icon-calendar"></i>
                     </span>
			</div>
		</div>

		<div class="form-group col-md-4">
			<label>Class Period&nbsp;<span class="required_label">*</span></label><br>
			<select name="period_id" id="period_id" class="js-example-basic-single w-100" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<option value="FM">From Machine (Login &amp; Logout)</option>
				<?php foreach ($period_list as $row) { ?>
					<option value="<?php echo $row['id']; ?>" <?php if(isset($period_id)){
						if($period_id == $row['id']){ echo 'selected'; }
					} ?>><?php echo $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>

	</div>

	<div class="float-right">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('process'); ?>">
    </div>
</form>


<?php
if(isset($student_info)){
	echo $data_table;
}
?>
