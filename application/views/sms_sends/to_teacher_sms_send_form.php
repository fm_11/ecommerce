<script>
    function getTemplateBody(template_id) {
        if (template_id == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //alert(xmlhttp.responseText);
                document.getElementById("template_body").value = "";
                document.getElementById("template_body").value = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>messages/getTemplateBodyById?template_id=" + template_id, true);
        xmlhttp.send();
    }


    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some teacher.");
            return false;
        } else {
            return true;
        }
    }
</script>


<div>
    <?php
    if (isset($teacher_info)) {
        ?>
        <form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()" action="<?php echo base_url(); ?>sms_sends/to_teacher_sms_send"
              method="post"
              enctype="multipart/form-data">
			<div class="table-sorter-wrapper col-lg-12 table-responsive">
				<table id="sortable-table-1" class="table">
                <thead>
                <tr>
                    <td colspan="2" style="text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' . $this->lang->line('template') . ' ' . $this->lang->line('name'); ?>
                    </td>
                    <td colspan="5">
                        <select style="padding: 6px;" name="template_id" onchange="getTemplateBody(this.value)"
                                required="1">
                            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                            <?php
                            $i = 0;
                            if (count($templates)) {
                                foreach ($templates as $list) {
                                    $i++;
                                    ?>
                                    <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['template_name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="vertical-align: middle;text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' .$this->lang->line('body'); ?>
                    </td>
                    <td colspan="5">
                        <textarea name="template_body" required rows="5" cols="35" id="template_body"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="text-align: right;">
						<div class="btn-group float-right">
							<input type="submit" class="btn btn-primary" value="Send SMS">
						</div>
                    </td>
                </tr>
                <tr>
                    <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
                    <th scope="col">Teacher ID</th>
                    <th scope="col"><?php echo $this->lang->line('name'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('designation'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('mobile') . ' ' . $this->lang->line('number'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach ($teacher_info as $row):
                    ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="is_send_<?php echo $i; ?>">
                        </td>
                        <td><?php echo $row['teacher_code']; ?></td>
                        <td>
                            <?php echo $row['name']; ?>
                            <input type="hidden" name="teacher_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>">
                        </td>
                        <td><?php echo $row['post_name']; ?></td>
                        <td>
                            <input type="text" autocomplete="off"  value="<?php echo $row['mobile']; ?>" name="mobile_<?php echo $i; ?>">
                        </td>
                    </tr>
                    <?php $i++; endforeach; ?>
                <tr>
                    <td colspan="7" style="text-align: right;">
                        <input type="hidden" name="loop_time" id="loop_time" value="<?php echo $i; ?>">
						<div class="btn-group float-right">
							<input type="submit" class="btn btn-primary" value="Send SMS">
						</div>
                    </td>
                </tr>
                </tbody>
            </table>
			</div>
        </form>
        <?php
    }
    ?>
</div>
