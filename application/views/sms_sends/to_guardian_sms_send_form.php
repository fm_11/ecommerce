<script>
    function getTemplateBody(template_id) {
        if (template_id == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //alert(xmlhttp.responseText);
                document.getElementById("template_body").value  = "";
                document.getElementById("template_body").value  = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>messages/getTemplateBodyById?template_id=" + template_id, true);
        xmlhttp.send();
    }


    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }

</script>

<?php
$class_shift_section_id = $this->session->userdata('class_shift_section_id');
$group_id = $this->session->userdata('group_id');
?>


<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>sms_sends/to_guardian_sms_send" method="post"
      enctype="multipart/form-data">
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
        <select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($class_section_shift_marge_list)) {
                foreach ($class_section_shift_marge_list as $list) {
                    $i++; ?>
                    <option
                        value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
                        <?php if (isset($class_shift_section_id)) {
                        if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
                            echo 'selected';
                        }
                    } ?>>
                    <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
                  </option>
                    <?php
                }
            }
            ?>
        </select>
      </div>

      <div class="form-group col-md-4">
          <label><?php echo $this->lang->line('group'); ?></label>
          <select class="js-example-basic-single w-100" name="group_id" id="group_id" required="1">
              <option value="all">-- <?php echo $this->lang->line('all'); ?> --</option>
              <?php
              $i = 0;
              if (count($groups)) {
                  foreach ($groups as $list) {
                      $i++; ?>
                      <option
                          value="<?php echo $list['id']; ?>" <?php  if (isset($group_id)) { if ($group_id == $list['id']) {
                          echo 'selected';
                      }} ?>><?php echo $list['name']; ?></option>
                      <?php
                  }
              }
              ?>
          </select>
      </div>
   </div>

   <div class="btn-group float-right">
     <input type="submit" class="btn btn-primary" value="Process">
   </div>
</form>

    <?php
    if (isset($student_info)) {
        ?>
        <form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()"  action="<?php echo base_url(); ?>sms_sends/guardian_sms_send_action" method="post"
              enctype="multipart/form-data">
            <div class="table-sorter-wrapper col-lg-12 table-responsive">
            <table id="sortable-table-1" class="table">
                <thead>
                <tr>
                    <td colspan="2" style="text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' . $this->lang->line('template') . ' ' . $this->lang->line('name'); ?>
                    </td>
                    <td colspan="5">
                        <select style="padding:6px;" name="template_id" onchange="getTemplateBody(this.value)"
                                required="1">
                            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                            <?php
                            $i = 0;
                            if (count($templates)) {
                                foreach ($templates as $list) {
                                    $i++;
                                    ?>
                                    <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['template_name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="vertical-align: middle;text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' . $this->lang->line('body'); ?>
                    </td>
                    <td colspan="3" style="vertical-align: middle;">
                        <textarea name="template_body" required rows="10" cols="35" id="template_body"></textarea>
                    </td>
					<td colspan="2" style="vertical-align: middle;">
						<ul id="sms-counter" class="alert alert-success">
							<li>
								Encoding: <span class="encoding"></span>
							</li>
							<li>Length: <span class="length"></span></li>
							<li>Messages: <span class="messages"></span></li>
							<li>Per Message: <span class="per_message"></span></li>
							<li>Remaining: <span class="remaining"></span></li>
						</ul>
					</td>
                </tr>
                <tr>
                    <td colspan="7">
                      <div class="btn-group float-right">
                        <input type="submit" class="btn btn-primary" value="Send SMS">
                      </div>
                    </td>
                </tr>
                <tr>
                    <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
                    <th  scope="col"><?php echo $this->lang->line('student_code'); ?></th>
                    <th  scope="col"><?php echo $this->lang->line('name'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('roll'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('gender'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('father_name'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('guardian_mobile'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach ($student_info as $row):
                    ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="is_send_<?php echo $i; ?>">
                        </td>
                        <td><?php echo $row['student_code']; ?></td>
                        <td>
                            <?php echo $row['name']; ?>
                            <input type="hidden" name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>">
                            <input type="hidden" name="group_id_<?php echo $i; ?>" value="<?php echo $row['group']; ?>">
                        </td>
                        <td><?php echo $row['roll_no']; ?></td>
                        <td>
                            <?php
                            if ($row['gender'] == 'M') {
                                echo "Male";
                            } else {
                                echo "Female";
                            }
                            ?>
                        </td>
                        <td><?php echo $row['father_name']; ?></td>
                        <td>
                            <input type="text" autocomplete="off"  value="<?php echo $row['guardian_mobile']; ?>" name="guardian_mobile_<?php echo $i; ?>">
                        </td>
                    </tr>
                <?php $i++; endforeach; ?>
                <tr>
                    <td colspan="7" style="text-align: right;">
                        <input type="hidden" name="loop_time" value="<?php echo $i; ?>">
                        <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                        <input type="hidden" name="section_id" value="<?php echo $section_id; ?>">
                        <input type="hidden" name="shift_id" value="<?php echo $shift_id; ?>">
						<input type="hidden" name="group_id" value="<?php echo $group; ?>">
                        <input type="hidden" name="class_shift_section_id" value="<?php echo $class_shift_section_id; ?>">
                        <div class="btn-group float-right">
                          <input type="submit" class="btn btn-primary" value="Send SMS">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
          </div>
        </form>
        <?php
    }
    ?>
<script src="<?php echo base_url(); ?>core_media/js/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url(); ?>core_media/js/sms_counter.min.js"></script>
<script>
	$('#template_body').countSms('#sms-counter');
</script>
