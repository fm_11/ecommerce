<script>

    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }
</script>



<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>messages/voice_sms_send" method="post"
      enctype="multipart/form-data">
	  
	  <div class="form-row">
		<div class="form-group col-md-4">
			<label>Class</label>
			<select class="smallInput" name="txtClass" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class)) {
					foreach ($class as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if (isset($class_id)) {
							if ($class_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>	
		<div class="form-group col-md-4">
			<label>Section</label>
			<select class="smallInput" name="txtSection" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($section)) {
					foreach ($section as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if (isset($section_id)) {
							if ($section_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		
		<div class="form-group col-md-4">
			<label>Group</label>
			<select class="smallInput" name="txtGroup" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($group)) {
					foreach ($group as $list) {
						$i++;
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if (isset($group_id)) {
							if ($group_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>	

    <input type="submit" class="submit" value="Process">
</form><br/>

<div>
    <?php
    if (isset($student_info)) {
        ?>
        <form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()"  action="<?php echo base_url(); ?>messages/to_guardian_voice_sms_send" method="post"
              enctype="multipart/form-data">
            <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                <thead>
                <tr>
                    <td colspan="2" style="text-align: right;">
                        <?php echo $this->lang->line('audio'); ?>
                    </td>
                    <td colspan="5">
                        <select class="smallInput" name="audio_id"
                                required="1">
                            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                            <?php
                            $i = 0;
                            if (count($audios)) {
                                foreach ($audios as $list) {
                                    $i++;
                                    ?>
                                    <option
                                            value="<?php echo $list['id']; ?>"><?php echo $list['title']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="text-align: right;">
                        <input type="submit" class="submit" value="Send SMS">
                    </td>
                </tr>
                <tr>
                    <th width="50" scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
                    <th width="110" scope="col"><?php echo $this->lang->line('student_code'); ?></th>
                    <th width="160" scope="col"><?php echo $this->lang->line('name'); ?></th>
                    <th width="50" scope="col"><?php echo $this->lang->line('roll'); ?></th>
                    <th width="70" scope="col"><?php echo $this->lang->line('gender'); ?></th>
                    <th width="110" scope="col"><?php echo $this->lang->line('father_name'); ?></th>
                    <th width="100" scope="col"><?php echo $this->lang->line('guardian_mobile'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach ($student_info as $row):
                    ?>
                    <tr>
                        <td width="34">
                            <input type="checkbox" name="is_send_<?php echo $i; ?>">
                        </td>
                        <td><?php echo $row['student_code']; ?></td>
                        <td>
                            <?php echo $row['name']; ?>
                            <input type="hidden" name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>">
                        </td>
                        <td><?php echo $row['roll_no']; ?></td>
                        <td>
                            <?php
                            if ($row['gender'] == 'M') {
                                echo "Male";
                            } else {
                                echo "Female";
                            }
                            ?>
                        </td>
                        <td><?php echo $row['father_name']; ?></td>
                        <td>
                            <input type="text" autocomplete="off"  value="<?php echo $row['guardian_mobile']; ?>" name="guardian_mobile_<?php echo $i; ?>">
                        </td>
                    </tr>
                <?php $i++; endforeach; ?>
                <tr>
                    <td colspan="7" style="text-align: right;">
                        <input type="hidden" name="loop_time" value="<?php echo $i; ?>">
                        <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                        <input type="hidden" name="section_id" value="<?php echo $section_id; ?>">
                        <input type="hidden" name="group_id" value="<?php echo $group_id; ?>">
                        <input type="submit" class="submit" value="Send SMS">
                    </td>
                </tr>
                </tbody>
            </table>
            <br>
        </form><br/>
        <?php
    }
    ?>
</div>
<div class="clear"></div><br/>
