<script>
    function getTemplateBody(template_id) {
        if (template_id == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //alert(xmlhttp.responseText);
                document.getElementById("template_body").value  = "";
                document.getElementById("template_body").value  = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>messages/getTemplateBodyById?template_id=" + template_id, true);
        xmlhttp.send();
    }


    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }
</script>


    <?php
    if (isset($student_info)) {
        ?>
        <form name="addForm" class="cmxform" id="commentForm" onsubmit="return checkCheckBox()"  action="<?php echo base_url(); ?>sms_sends/absent_sms_send_post" method="post"
              enctype="multipart/form-data">
			<div class="table-sorter-wrapper col-lg-12 table-responsive">
				<table id="sortable-table-1" class="table">
					<thead>
                <tr>
                    <td colspan="2" style="text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' . $this->lang->line('template') . ' ' . $this->lang->line('name'); ?>
                    </td>
                    <td colspan="5">
                        <select style="padding:6px;" name="template_id" onchange="getTemplateBody(this.value)"
                                required="1">
                            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                            <?php
							if (count($templates)) {
							foreach ($templates as $list) {
							  ?>
							  <option value="<?php echo $list['id']; ?>"><?php echo $list['template_name']; ?></option>
							<?php
							  }
							  } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="vertical-align: middle;text-align: right;">
                        <?php echo $this->lang->line('message') . ' ' . $this->lang->line('body'); ?>
                    </td>
                    <td colspan="5">
                        <textarea name="template_body" required rows="10" cols="35" id="template_body"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="text-align: right;">
						<div class="btn-group float-right">
							<input type="submit" class="btn btn-primary" value="Send SMS">
						</div>
                    </td>
                </tr>
                <tr>
                    <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
                    <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('student_code'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('name'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('roll'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('class'); ?></th>
                    <th scope="col"><?php echo $this->lang->line('guardian_mobile'); ?> Number</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
        foreach ($student_info as $row):
                    ?>
                    <tr>
                        <td>
                            <input type="checkbox" name="is_send_<?php echo $i; ?>">
                        </td>
                        <td><?php echo $i + 1; ?></td>
                        <td>
							<?php echo $row['student_code']; ?>
						</td>
                        <td>
                            <?php echo $row['name']; ?>
                        </td>
                        <td><?php echo $row['roll_no']; ?></td>

                        <td><?php echo $row['class_name']; ?></td>
                        <td>
							<input type="hidden" autocomplete="off"  value="<?php echo $row['id']; ?>" name="student_id_<?php echo $i; ?>">
                            <input type="text" autocomplete="off"  value="<?php echo $row['guardian_mobile']; ?>" name="guardian_mobile_<?php echo $i; ?>">
                        </td>
                    </tr>
                <?php $i++;
        endforeach; ?>
                <tr>
                    <td colspan="7" style="text-align: right;">
						<input type="hidden" value="<?php echo $shift_id; ?>" name="shift_id">
                        <input type="hidden" name="loop_time" value="<?php echo $i; ?>">
						<div class="btn-group float-right">
							<input type="submit" class="btn btn-primary" value="Send SMS">
						</div>
                    </td>
                </tr>
                </tbody>
            </table>
			</div>
        </form>
        <?php
    }
    ?>
