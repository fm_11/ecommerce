<?php
//echo '<pre>'; print_r($ProcessInfo); die;
$is_written_allow = $class_wise_subject_details[0]['is_written_allow'];
$is_objective_allow = $class_wise_subject_details[0]['is_objective_allow'];
$is_practical_allow = $class_wise_subject_details[0]['is_practical_allow'];
$is_class_test_allow = $class_wise_subject_details[0]['is_class_test_allow'];


$written = $class_wise_subject_details[0]['written'];
$objective = $class_wise_subject_details[0]['objective'];
$practical = $class_wise_subject_details[0]['practical'];
$class_test = $class_wise_subject_details[0]['class_test'];
$tabindex = 0;
?>

<style>
    .display_none{
        display: none;
    }
</style>
<script>
    function checkCreditVsNumber(inputed_num,row_id,type){
       var allocated_written = '<?php echo $class_wise_subject_details[0]['written']; ?>';
       var allocated_objective = '<?php echo $class_wise_subject_details[0]['objective']; ?>';
       var allocated_practical = '<?php echo $class_wise_subject_details[0]['practical']; ?>';
       if(type == 'written'){
           if(Number(inputed_num) > Number(allocated_written)){
               alert("More than "+ allocated_written +" inputs can not be given on Creative Mark.");
               document.getElementById(row_id).value = "";
               return false;
           }
       }
       if(type == 'objective'){
           if(Number(inputed_num) > Number(allocated_objective)){
               alert("More than "+ allocated_objective +" inputs can not be given on Multiple Choice Mark.");
               document.getElementById(row_id).value = "";
               return false;
           }
       }
       if(type == 'practical'){
           if(Number(inputed_num) > Number(allocated_practical)){
               alert("More than "+ allocated_practical +" inputs can not be given on Practical Mark.");
               document.getElementById(row_id).value = "";
               return false;
           }
       }
       return true;
   }

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
            return true;
        } else if ( key < 48 || key > 57 ) {
            return false;
        } else {
           return true;
        }
   }

   function markingValueCheck(value,sl_num) {
     if (document.getElementById('is_absent_' + sl_num).checked) {
           document.getElementById('written_' + sl_num).value = "0";
           document.getElementById('objective_' + sl_num).value = "0";
           document.getElementById('practical_' + sl_num).value = "0";
           document.getElementById('class_test_' + sl_num).value = "0";

           document.getElementById('written_' + sl_num).readOnly = true;
           document.getElementById('objective_' + sl_num).readOnly = true;
           document.getElementById('practical_' + sl_num).readOnly = true;
           document.getElementById('class_test_' + sl_num).readOnly = true;
       } else {
         document.getElementById('written_' + sl_num).value = "0";
         document.getElementById('objective_' + sl_num).value = "0";
         document.getElementById('practical_' + sl_num).value = "0";
         document.getElementById('class_test_' + sl_num).value = "0";

         document.getElementById('written_' + sl_num).readOnly = false;
         document.getElementById('objective_' + sl_num).readOnly = false;
         document.getElementById('practical_' + sl_num).readOnly = false;
         document.getElementById('class_test_' + sl_num).readOnly = false;

       }
   }
</script>
<h4 class="card-title">
    Class: <?php echo $ClassSubjectName[0]['ClassName']; ?>&nbsp;,
    Subject: <?php echo $ClassSubjectName[0]['SubjectName'] . ' [' . $ClassSubjectName[0]['SubjectCode'] . ']'; ?>&nbsp;,Credit: <?php echo $credit; ?>
</h4>
Mark Distribution => <?php echo $marking_heads->written; ?> : <?php echo  $written.', '; ?> <?php echo $marking_heads->mcq; ?> : <?php echo  $objective.', '; ?> <?php echo $marking_heads->practical; ?> : <?php echo  $practical.', '; ?> <?php echo $marking_heads->class_test; ?> : <?php echo  $class_test; ?>
<div class="col-lg-12">
<form action="<?php echo base_url('markings/marking_data_save'); ?>" method="post">
    <div class="table-sorter-wrapper col-lg-12 table-responsive">
      <table id="sortable-table-1" class="table">
        <tr>
            <th>
                SL
            </th>
			      <th>
                Student ID
            </th>
            <th>
                Name
            </th>
            <th>
                Roll
            </th>


            <th <?php if ($is_class_test_allow == '0') { ?>class="display_none" <?php } ?>>
                <?php echo $marking_heads->class_test; ?> <br>
                <?php echo '('. $class_test . ')' ?>
            </th>

            <th <?php if ($is_written_allow == '0') { ?>class="display_none" <?php } ?>>
                <?php echo $marking_heads->written; ?> <br>
                <?php echo '('. $written . ')' ?>
            </th>
            <th <?php if ($is_objective_allow == '0') { ?>class="display_none" <?php } ?>>
                <?php echo $marking_heads->mcq; ?><br>
                <?php echo '('. $objective . ')' ?>
            </th>
            <th <?php if ($is_practical_allow == '0') { ?>class="display_none" <?php } ?>>
                <?php echo $marking_heads->practical; ?><br>
                <?php echo '('. $practical . ')' ?>
            </th>
            <th>
              Is Absent?
            </th>
        </tr>
        <input type="hidden" tabindex="-1" name="IterationNo" value="<?php echo count($ProcessInfo); ?>">
        <?php for ($A = 0; $A < count($ProcessInfo); $A++) { ?>
            <input type="hidden" tabindex="-1" name="student_id_<?php echo $A; ?>" value="<?php echo $ProcessInfo[$A]['id']; ?>">
            <input type="hidden" tabindex="-1" name="group_<?php echo $A; ?>" value="<?php echo $ProcessInfo[$A]['group']; ?>">

            <tr>
                <td><?php echo $A + 1; ?></td>
				 <td><?php echo $ProcessInfo[$A]['student_code']; ?></td>
                <td><?php echo $ProcessInfo[$A]['name']; ?></td>
                <td><?php echo $ProcessInfo[$A]['roll_no']; ?></td>


                <td <?php if ($is_class_test_allow == '0') { ?>class="display_none" <?php } ?>>
                    <?php
                    if ($is_class_test_allow != '0') {
                        $tabindex = 0;
                    } else {
                        $tabindex = -1;
                    }
                    ?>
                    <input  style="max-width:100px;" class="form-control" value="<?php echo $ProcessInfo[$A]['class_test']; ?>" tabindex="<?php echo $tabindex; ?>"
                     onkeypress="javascript:return validateNumber(event)"
                     onkeyup="return checkCreditVsNumber(this.value,'class_test_<?php echo $A; ?>','class_test')"
                     id="class_test_<?php echo $A; ?>" required="required" <?php if ($ProcessInfo[$A]['is_absent'] > 0) {
                           echo 'readonly';
                       } ?>
                    name="class_test_<?php echo $A; ?>" type="text">

                </td>

                <td <?php if ($is_written_allow == '0') { ?>class="display_none" <?php } ?>>
                    <?php
                    if ($is_written_allow != '0') {
                        $tabindex = 0;
                    } else {
                        $tabindex = -1;
                    }
                    ?>
                    <input  style="max-width:100px;" class="form-control" value="<?php echo $ProcessInfo[$A]['written']; ?>"
                    tabindex="<?php echo $tabindex; ?>" onkeypress="javascript:return validateNumber(event)"
                    onkeyup="return checkCreditVsNumber(this.value,'written_<?php echo $A; ?>','written')"
                    id="written_<?php echo $A; ?>" required="required" <?php if ($ProcessInfo[$A]['is_absent'] > 0) {
                          echo 'readonly';
                      } ?>  name="written_<?php echo $A; ?>" type="text">

                </td>

                <td <?php if ($is_objective_allow == '0') { ?>class="display_none" <?php } ?>>
                    <?php
                    if ($is_objective_allow != '0') {
                        $tabindex = 0;
                    } else {
                        $tabindex = -1;
                    }
                    ?>
                    <input style="max-width:100px;" class="form-control" value="<?php echo $ProcessInfo[$A]['objective']; ?>"
                    tabindex="<?php echo $tabindex; ?>" onkeypress="javascript:return validateNumber(event)"
                    onkeyup="return checkCreditVsNumber(this.value,'objective_<?php echo $A; ?>','objective')"
                     id="objective_<?php echo $A; ?>" required="required"  <?php if ($ProcessInfo[$A]['is_absent'] > 0) {
                           echo 'readonly';
                       } ?>  name="objective_<?php echo $A; ?>" type="text">

                </td>

                <td <?php if ($is_practical_allow == '0') { ?>class="display_none" <?php } ?>>
                    <?php
                    if ($is_practical_allow != '0') {
                        $tabindex = 0;
                    } else {
                        $tabindex = -1;
                    }
                    ?>
                    <input style="max-width:100px;" class="form-control" value="<?php echo $ProcessInfo[$A]['practical']; ?>"
                    tabindex="<?php echo $tabindex; ?>" onkeypress="javascript:return validateNumber(event)"
                     onkeyup="return checkCreditVsNumber(this.value,'practical_<?php echo $A; ?>','practical')"
                     id="practical_<?php echo $A; ?>" required="required"  <?php if ($ProcessInfo[$A]['is_absent'] > 0) {
                           echo 'readonly';
                       } ?>   name="practical_<?php echo $A; ?>" type="text">

                </td>
                <td>
                  <input type="checkbox" onclick="markingValueCheck(this.value,<?php echo $A; ?>)" <?php if ($ProcessInfo[$A]['is_absent'] > 0) {
                        echo 'checked';
                    } ?> name="is_absent_<?php echo $A; ?>" id="is_absent_<?php echo $A; ?>">
                </td>
            </tr>
        <?php } ?>
    </table>
  </div>
    <div class="float-right">
      <input type="hidden" name="exam_id" value="<?php echo $posted_exam_id; ?>">
      <input type="hidden" name="posted_class_shift_section_id" value="<?php echo $posted_class_shift_section_id; ?>">
      <input type="hidden" name="posted_group_id" value="<?php echo $posted_group_id; ?>">
      <input type="hidden" name="posted_subject_id" value="<?php echo $posted_subject_id; ?>">
      <input type="hidden" name="posted_year" value="<?php echo $posted_year; ?>">
      <!-- <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>"> -->
      <input class="btn btn-primary" type="submit" value="Save">
    </div>
</form>
</div>
