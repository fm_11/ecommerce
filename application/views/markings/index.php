<script>
	function subjectByClassSectionShiftAndExam() {
		var class_shift_section_id = document.getElementById("class_shift_section_id").value;
		var exam_id = document.getElementById("exam_id").value;
		if(class_shift_section_id != ''  && exam_id != ''){
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			}
			else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function () {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					document.getElementById("subject_id").innerHTML = xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>subjects/subjectByClassSectionShiftAndExam?class_shift_section_id=" + class_shift_section_id + "&&exam_id=" + exam_id, true);
			xmlhttp.send();
		}
	}

	function getExamByYear(year){
		//alert(year);
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		}
		else
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				//alert(xmlhttp.responseText);
				document.getElementById("exam_id").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", "<?php echo base_url(); ?>exams/getExamByYear?year=" + year, true);
		xmlhttp.send();
	}

	function alertForFileUpload()
	{
		var result = confirm("Are you sure to upload file?");
		if (result == true) {
			return true;
		}
		else {
			return false;
		}
	}
</script>

<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>markings/index" enctype="multipart/form-data" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Year</label>
			<select class="js-example-basic-single w-100" onchange="getExamByYear(this.value)" name="year" id="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['value']; ?>"
							<?php if (isset($year)) {
								if ($year == $list['value']) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['text']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Exam</label>
			<select name="exam_id" id="exam_id" class="js-example-basic-single w-100" required="required"
					onchange="subjectByClassSectionShiftAndExam()">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php for ($A = 0; $A < count($ExamList); $A++) { ?>
					<option value="<?php echo $ExamList[$A]['id']; ?>"
						<?php if (isset($m_exam_id)) {
							if ($m_exam_id == $ExamList[$A]['id']) {
								echo 'selected';
							}
						} ?>><?php echo $ExamList[$A]['name']. ' ('. $ExamList[$A]['year'] . ')'; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
			<select class="js-example-basic-single w-100"  onchange="subjectByClassSectionShiftAndExam()"
					name="class_shift_section_id" id="class_shift_section_id" required>
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
							<?php if (isset($class_shift_section_id)) {
								if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Group</label>
			<select name="group_id" class="js-example-basic-single w-100"  required="required">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php for ($C = 0; $C < count($GroupList); $C++) { ?>
					<option value="<?php echo $GroupList[$C]['id']; ?>" <?php if ($m_group_id == $GroupList[$C]['id']) {
						echo 'selected';
					} ?>><?php echo $GroupList[$C]['name']; ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Subject</label>
			<select name="subject_id"  class="js-example-basic-single w-100" id="subject_id" required="required">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php for ($A = 0; $A < count($subjects); $A++) { ?>
					<option value="<?php echo $subjects[$A]['subject_id']; ?>" <?php if ($m_subject_id == $subjects[$A]['subject_id']) {
						echo 'selected';
					} ?>>
						<?php echo $subjects[$A]['name'] . ' [' . $subjects[$A]['code'] . ']'; ?>
					</option>
				<?php } ?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Excel File</label>
			<input type="file" name="excelFile" id="excelFile" class="form-control">
		</div>

	</div>

	<div class="btn-group float-right">
		<input class="btn btn-dark" name="download" type="submit" value="Download">
		<input class="btn btn-success" onclick="return alertForFileUpload()" name="upload" type="submit" value="Upload">
		<input class="btn btn-primary" name="process" type="submit" value="Process">
	</div>
</form>


<?php
if(isset($marking_process_list)){
	echo $marking_process_list;
}
?>
