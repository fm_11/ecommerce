<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>asset_categories/edit" method="post">
    <div class="form-row">
      <div class="form-group col-md-4">
        <label><?php echo $this->lang->line('name'); ?></label>
        <input type="text" autocomplete="off"  name="name" required class="form-control" value="<?php echo $asset_category_info[0]['name']; ?>">
      </div>

      <div class="form-group col-md-4">
        <label>Code</label>
        <input type="text" autocomplete="off"  name="code" required class="form-control" value="<?php echo $asset_category_info[0]['code']; ?>">
      </div>

      <div class="form-group col-md-4">
        <label>Opening Balance</label>
        <input type="text" autocomplete="off"  name="opening_balance" class="form-control" value="<?php echo $asset_category_info[0]['opening_balance']; ?>">
      </div>
    </div>

    <div class="form-row">
      <div class="form-group col-md-12">
        <label><?php echo $this->lang->line('remarks'); ?></label>
        <textarea rows="5" cols="50" class="form-control" name="remarks"><?php echo $asset_category_info[0]['remarks']; ?></textarea>
      </div>
    </div>

    <div class="float-right">
      <input type="hidden" name="id" value="<?php echo $asset_category_info[0]['id']; ?>">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
