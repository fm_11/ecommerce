<style>
.small_custom_input{
  padding: 6px;
}
</style>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>result_grades/data_save/e" method="post">
<div class="table-responsive-sm">
   <table id="myTable" class="table  grade-list">
    <thead>
        <tr>
          <th colspan="5">
            <div class="form-group col-md-4">
              <label><?php echo $this->lang->line('class'); ?></label><br>
              <select class="js-example-basic-multiple w-100" disabled multiple="multiple" required="1">
                  <?php
                  $i = 0;
                  if (count($classes)) {
                      foreach ($classes as $list) {
                          $i++; ?>
                          <option
                              value="<?php echo $list['id']; ?>" <?php if ($list['id'] == $grade_data[0]['class_id']) {
                              echo 'selected';
                          } ?>><?php echo $list['name']; ?></option>
                      <?php
                      }
                  }
                  ?>
              </select>
              <input type="hidden" name="class_id" required value="<?php echo $grade_data[0]['class_id']; ?>" class="small_custom_input" />
            </div>
          </th>
        </tr>
        <tr>
            <th>Start Range</th>
            <th>End Range</th>
            <th>Grade</th>
            <th>Grade Point</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
      <?php
      $i = 0;
      foreach ($grade_data as $row):
          ?>
        <tr>
            <td>
                <input type="text" autocomplete="off"  value="<?php echo $row['start_range']; ?>" name="start_range_<?php echo $i; ?>" required class="small_custom_input" />
            </td>
            <td>
                <input type="text" autocomplete="off"  value="<?php echo $row['end_range']; ?>"  name="end_range_<?php echo $i; ?>" required class="small_custom_input"/>
            </td>
            <td>
                <input type="text" autocomplete="off"  value="<?php echo $row['alpha_gpa']; ?>" name="alpha_gpa_<?php echo $i; ?>" required class="small_custom_input"/>
            </td>
            <td>
                <input type="text" autocomplete="off"  value="<?php echo $row['numeric_gpa']; ?>"  name="numeric_gpa_<?php echo $i; ?>" required class="small_custom_input"/>
            </td>
            <td>
              <?php if ($i != 0) { ?>
                  <button style="padding: 0.20rem .80rem;" class="ibtnDel btn btn-danger btn-sm" type="button"><i class="ti-trash"></i></button>
            <?php } else { ?>
              <a class="deleteRow"></a>
                <?php } ?>
            </td>
        </tr>
          <?php $i++; endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" style="text-align: right;">
              <button style="padding: 0.20rem .80rem;" id="addrow" class="btn btn-primary btn-sm" type="button">
                  <i class="ti-plus"></i>
              </button>
              <input type="hidden" name="total_submited_row" id="total_submited_row" value="<?php echo $i; ?>" required class="small_custom_input"/>
            </td>
        </tr>
    </tfoot>
</table>
</div>
<div class="float-right">
    <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
</div>
</form>
