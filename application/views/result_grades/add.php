<style>
.small_custom_input{
  padding: 6px;
}
</style>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>result_grades/data_save/a" method="post">
<div class="table-responsive-sm">
   <table id="myTable" class="table  grade-list">
    <thead>
        <tr>
          <th colspan="5">
            <div class="form-group col-md-4">
              <label><?php echo $this->lang->line('class'); ?></label><br>
              <select class="js-example-basic-multiple w-100"  multiple="multiple" id="class_id" name="class_id[]" required="1">
                  <?php
                  $i = 0;
                  if (count($classes)) {
                      foreach ($classes as $list) {
                          $i++; ?>
                          <option
                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                      <?php
                      }
                  }
                  ?>
              </select>
            </div>
          </th>
        </tr>
        <tr>
            <th>Start Range</th>
            <th>End Range</th>
            <th>Grade</th>
            <th>Grade Point</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <input type="text" autocomplete="off"  name="start_range_0" required class="small_custom_input" />
            </td>
            <td>
                <input type="text" autocomplete="off"  name="end_range_0" required class="small_custom_input"/>
            </td>
            <td>
                <input type="text" autocomplete="off"  name="alpha_gpa_0" required class="small_custom_input"/>
            </td>
            <td>
                <input type="text" autocomplete="off"  name="numeric_gpa_0" required class="small_custom_input"/>
            </td>
            <td>
              <a class="deleteRow"></a>
            </td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" style="text-align: right;">
              <button style="padding: 0.20rem .80rem;" id="addrow" class="btn btn-primary btn-sm" type="button">
                  <i class="ti-plus"></i>
              </button>
              <input type="hidden" name="total_submited_row" id="total_submited_row" value="1" required class="small_custom_input"/>
            </td>
        </tr>
    </tfoot>
</table>
</div>
<div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
    <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
</div>
</form>
