<form name="updateForm" action="<?php echo base_url(); ?>headteacher_messages/edit/" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-4">
			<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/headteacher/<?php echo $message->location; ?>" class="img-lg rounded-circle mb-3"/>
			<input type="file" name="txtPhoto" class="form-control">
		</div>
	</div>
	<div class="form-row">
		<div class="form-group col-md-12">
			<label>Title</label>
			<input type="text" autocomplete="off" class="form-control"  name="txtTitle" value="<?php echo $message->title; ?>">
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-12">
			<label>Message</label>
			<textarea name="txtMessage" id="tinyMceExample" class="form-control"><?php echo $message->long_message; ?></textarea>
		</div>
	</div>


	<input type="hidden" name="id" value="<?php echo $message->id; ?>" class="smallInput">
	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
