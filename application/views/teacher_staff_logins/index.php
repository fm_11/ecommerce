<?php
$category_id = $this->session->userdata('category_id');
$date = $this->session->userdata('date');
?>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>teacher_staff_logins/index" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('date'); ?></label>
			<div class="input-group date">
				<input type="text" autocomplete="off"  name="date" required value="<?php if (isset($date)) {
					echo $date;
				} ?>" class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
			</div>
		</div>
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('category'); ?></label>
			<select class="js-example-basic-single w-100" name="category_id" id="category_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($categories)) {
					foreach ($categories as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['id']; ?>" <?php  if (isset($category_id)) { if ($category_id == $list['id']) {
							echo 'selected';
						}} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-primary" value="Process">
	</div>
</form>

<?php
if (isset($teachers)) {
	echo $process_table;
}
?>
