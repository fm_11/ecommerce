<script>
	function checkTeacherAll(ele) {
		var checkboxes = document.querySelectorAll("input[type='checkbox']");
		if (ele.checked) {
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].type == 'checkbox') {
					checkboxes[i].checked = true;
					var rowId = checkboxes[i].id;
					var rowNo = rowId.charAt(rowId.length-1);
					if(!isNaN(rowNo)){
						document.getElementById("login_time_" + rowNo).readOnly = true;
						document.getElementById("logout_time_" + rowNo).readOnly = true;
						document.getElementById("login_time_" + rowNo).value = "";
						document.getElementById("logout_time_" + rowNo).value = "";
					}
				}
			}
		} else {
			for (var i = 0; i < checkboxes.length; i++) {
				console.log(i)
				if (checkboxes[i].type == 'checkbox') {
					checkboxes[i].checked = false;
					var rowId = checkboxes[i].id;
					var rowNo = rowId.charAt(rowId.length-1);
					if(!isNaN(rowNo)){
						document.getElementById("login_time_" + rowNo).readOnly = false;
						document.getElementById("logout_time_" + rowNo).readOnly = false;
						var login_time = document.getElementById("expected_login_time_" + rowNo).value;
						var logout_time = document.getElementById("expected_logout_time_" + rowNo).value;
						document.getElementById("login_time_" + rowNo).value = login_time;
						document.getElementById("logout_time_" + rowNo).value = logout_time;
					}
				}
			}
		}
	}

	function checkSingleTeacherRow(rowNo,ele) {
		if (ele.checked) {
			document.getElementById("login_time_" + rowNo).readOnly = true;
			document.getElementById("logout_time_" + rowNo).readOnly = true;
			document.getElementById("login_time_" + rowNo).value = "";
			document.getElementById("logout_time_" + rowNo).value = "";
		}else{
			document.getElementById("login_time_" + rowNo).readOnly = false;
			document.getElementById("logout_time_" + rowNo).readOnly = false;
			var login_time = document.getElementById("expected_login_time_" + rowNo).value;
			var logout_time = document.getElementById("expected_logout_time_" + rowNo).value;
			document.getElementById("login_time_" + rowNo).value = login_time;
			document.getElementById("logout_time_" + rowNo).value = logout_time;
		}
	}
</script>

<style>
	input.largerCheckbox {
		width: 18px;
		height: 18px;
	}
</style>

<form action="<?php echo base_url('teacher_staff_logins/data_save'); ?>" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<thead>
			<tr>
				<th scope="col">Teacher ID</th>
				<th scope="col"><?php echo $this->lang->line('name'); ?></th>
				<th scope="col"><?php echo $this->lang->line('designation'); ?></th>
				<th scope="col">In Time</th>
				<th scope="col">Out Time</th>
				<th scope="col" style="text-align: center;">
					Is Absent?<br>
					<input checked onclick="checkTeacherAll(this)" type="checkbox" name="is_absent" id="is_absent">
				</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i = 0;
			foreach ($teachers as $row):
				?>
				<tr>

				<tr>
					<td>
						<?php echo $row['teacher_code']; ?>
						<input  type="hidden" style="width: 155px;" value="<?php echo $row['id']; ?>" class="form-control"  name="teacher_id_<?php echo $i; ?>">
						<input  type="hidden" value="<?php echo $row['process_code']; ?>" class="form-control"  name="process_code_<?php echo $i; ?>">
						<input  type="hidden" value="<?php echo $row['flexible_min_for_late']; ?>" class="form-control"  name="flexible_min_for_late_<?php echo $i; ?>">
					</td>
					<td><?php echo $row['name']; ?></td>
					<td><?php echo $row['teacher_post']; ?></td>
					<td>
						<input style="width: 135px;padding: 10px;" <?php if($row['logout_time'] == ''){ echo 'readonly'; } ?>  value="<?php echo $row['login_time']; ?>" class="form-control"  name="login_time_<?php echo $i; ?>" id="login_time_<?php echo $i; ?>" type="time">
						<input style="width: 135px;padding: 10px;" value="<?php echo $row['expected_login_time']; ?>" class="form-control"  name="expected_login_time_<?php echo $i; ?>" id="expected_login_time_<?php echo $i; ?>" type="hidden">
					</td>
					<td>
						<input style="width: 135px;padding: 10px;" <?php if($row['logout_time'] == ''){ echo 'readonly'; } ?> value="<?php echo $row['logout_time']; ?>" class="form-control"  name="logout_time_<?php echo $i; ?>" id="logout_time_<?php echo $i; ?>" type="time">
						<input style="width: 135px;padding: 10px;" value="<?php echo $row['expected_logout_time']; ?>" class="form-control"  name="expected_logout_time_<?php echo $i; ?>" id="expected_logout_time_<?php echo $i; ?>" type="hidden">
					</td>
					<td align="center">
						<input type="checkbox" onclick="checkSingleTeacherRow('<?php echo $i; ?>',this)" <?php if($row['logout_time'] == ''){ echo 'checked'; } ?> class="form-control" id="is_absent_<?php echo $i; ?>" name="is_absent_<?php echo $i; ?>">
					</td>
				</tr>
				<?php $i++; endforeach; ?>

				<tr>
					<td colspan="6" style="text-align: left;font-size: 18px;">
						<br>
						<b>
							<input type="checkbox" class="largerCheckbox" name="is_present_sms">&nbsp;Present SMS Send&nbsp;&nbsp;&nbsp;
							<input type="checkbox" class="largerCheckbox" name="is_absent_sms">&nbsp;Absent SMS Send&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</b>
					</td>
				</tr>

			</tbody>
		</table>
		<input type="hidden" value="<?php echo $i; ?>" name="total_row">
		<input type="hidden" value="<?php echo $date; ?>" name="date">
		<div class="btn-group float-right">
			<input class="btn btn-primary" name="process" type="submit" value="Save">
		</div>
	</div>
</form>
