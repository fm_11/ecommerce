
  <div class="card">
	<div class="card-body">
	  <ul class="nav nav-pills nav-pills-success" id="pills-tab" role="tablist">
		<li class="nav-item">
		  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-Asset" role="tab" aria-controls="pills-Asset" aria-selected="true">Asset</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" id="pills-Liabilities-tab" data-toggle="pill" href="#pills-Liabilities" role="tab" aria-controls="pills-Liabilities" aria-selected="false">Liabilities</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" id="pills-Income-tab" data-toggle="pill" href="#pills-Income" role="tab" aria-controls="pills-Income" aria-selected="false">Income</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" id="pills-Expenses-tab" data-toggle="pill" href="#pills-Expenses" role="tab" aria-controls="pills-Expenses" aria-selected="false">Expenses</a>
		</li>
	  </ul>
	  <div class="tab-content" id="pills-tabContent">
		<div class="tab-pane fade show active" id="pills-Asset" role="tabpanel" aria-labelledby="pills-Asset-tab">
		  <div class="media">
			<div class="media-body">
				<div class="table-sorter-wrapper col-lg-12 table-responsive">
				<table id="sortable-table-1" class="table">
					 <thead>
						 <tr>
						   <th class="sortStyle"><?php echo $this->lang->line('sl'); ?></th>
						   <th class="sortStyle"><?php echo $this->lang->line('name'); ?></th>
						   <th class="sortStyle">Code</th>
						   <th class="sortStyle">Opening Balance</th>
						 </tr>
					 </thead>
					 <tbody>
						   <?php
						   $i = 0;
						   foreach ($asset_head_list as $row):
							 $i++;
							 ?>
							 <tr>

							 <tr>
								 <td>
									 <?php echo $i; ?>
								 </td>
								 <td><?php echo $row['name']; ?></td>
								 <td><?php echo $row['code']; ?></td>
								 <td><?php echo $row['opening_balance']; ?></td>
							 </tr>
						   <?php endforeach; ?>
						 </tbody>
				   </table>
				</div>
			</div>
		  </div>
		</div>
		<div class="tab-pane fade" id="pills-Liabilities" role="tabpanel" aria-labelledby="pills-Liabilities-tab">
		  <div class="media">
			<div class="media-body">
			  <div class="table-sorter-wrapper col-lg-12 table-responsive">
			  <table id="sortable-table-1" class="table">
				   <thead>
					   <tr>
						 <th class="sortStyle"><?php echo $this->lang->line('sl'); ?></th>
						 <th class="sortStyle"><?php echo $this->lang->line('name'); ?></th>
						 <th class="sortStyle">Code</th>
						 <th class="sortStyle">Opening Balance</th>
					   </tr>
				   </thead>
				   <tbody>
						 <?php
						 $i = 0;
						 foreach ($lib_head_list as $row):
						   $i++;
						   ?>
						   <tr>

						   <tr>
							   <td>
								   <?php echo $i; ?>
							   </td>
							   <td><?php echo $row['name']; ?></td>
							   <td><?php echo $row['code']; ?></td>
							   <td><?php echo $row['opening_balance']; ?></td>

						   </tr>
						 <?php endforeach; ?>
					   </tbody>
				 </table>
			  </div>
			</div>
		  </div>
		</div>
		<div class="tab-pane fade" id="pills-Income" role="tabpanel" aria-labelledby="pills-Income-tab">
		  <div class="media">
			<div class="media-body">
			  <div class="table-sorter-wrapper col-lg-12 table-responsive">
			  <table id="sortable-table-1" class="table">
				   <thead>
					   <tr>
						 <th class="sortStyle"><?php echo $this->lang->line('sl'); ?></th>
						 <th class="sortStyle"><?php echo $this->lang->line('name'); ?></th>
						 <th class="sortStyle">Code</th>
						 <th class="sortStyle">Opening Balance</th>

					   </tr>
				   </thead>
				   <tbody>
						 <?php
						 $i = 0;
						 foreach ($income_head_list as $row):
						   $i++;
						   ?>
						   <tr>

						   <tr>
							   <td>
								   <?php echo $i; ?>
							   </td>
							   <td><?php echo $row['name']; ?></td>
							   <td><?php echo $row['code']; ?></td>
							   <td><?php echo $row['opening_balance']; ?></td>
						   </tr>
						 <?php endforeach; ?>
					   </tbody>
				 </table>
			  </div>
			</div>
		  </div>
		</div>
		<div class="tab-pane fade" id="pills-Expenses" role="tabpanel" aria-labelledby="pills-Expenses-tab">
		  <div class="media">
			<div class="media-body">
			  <div class="table-sorter-wrapper col-lg-12 table-responsive">
			  <table id="sortable-table-1" class="table">
				   <thead>
					   <tr>
						 <th class="sortStyle"><?php echo $this->lang->line('sl'); ?></th>
						 <th class="sortStyle"><?php echo $this->lang->line('name'); ?></th>
						 <th class="sortStyle">Code</th>
						 <th class="sortStyle">Opening Balance</th>

					   </tr>
				   </thead>
				   <tbody>
						 <?php
						 $i = 0;
						 foreach ($exp_head_list as $row):
						   $i++;
						   ?>
						   <tr>

						   <tr>
							   <td>
								   <?php echo $i; ?>
							   </td>
							   <td><?php echo $row['name']; ?></td>
							   <td><?php echo $row['code']; ?></td>
							   <td><?php echo $row['opening_balance']; ?></td>

						   </tr>
						 <?php endforeach; ?>
					   </tbody>
				 </table>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
