<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>student_report_absentees/index#attendance"
	  method="post">
	<div class="form-row">
		<div class="form-group col-md-3">
			<label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
			<select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
							<?php if (isset($class_shift_section_id)) {
								if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('group'); ?></label>
			<select name="group_id" id="group_id" class="js-example-basic-single w-100" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php foreach ($GroupList as $row) { ?>
					<option value="<?php echo $row['id']; ?>" <?php if(isset($group_id)){
						if($group_id == $row['id']){ echo 'selected'; }
					} ?>><?php echo $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Class Period</label>
			<select name="period_id" id="period_id" class="js-example-basic-single w-100" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php foreach ($period_list as $row) { ?>
					<option value="<?php echo $row['id']; ?>" <?php if(isset($period_id)){
						if($period_id == $row['id']){ echo 'selected'; }
					} ?>><?php echo $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="from_date"><?php echo $this->lang->line('date'); ?></label>
			<div class="input-group date">
				<input type="text" value="<?php if(isset($date)){ echo $date; } ?>" autocomplete="off"
					   name="date" id="date" required class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
			</div>
		</div>
	</div>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
		<input type="submit" class="btn btn-primary" value="View Report">
	</div>

</form>

<div id="printableArea">
	<?php
	if (isset($absentee_info)) {
		echo $report;
	}
	?>
</div>
