
STUDENT'S NAME:  <b><?php echo $Result[0]['StudentName']; ?></b><br>
FATHER'S NAME:  <b><?php echo $Result[0]['father_name']; ?></b><br>
MOTHER'S NAME:  <b><?php echo $Result[0]['mother_name']; ?></b><br>
DATE OF BIRTH:  <b><?php echo $Result[0]['date_of_birth']; ?></b><br>
CLASS: <b><?php echo $Result[0]['ClassName']; ?></b><br>
SECTION: <b><?php echo $Result[0]['SectionName']; ?></b><br>
ROLL NO: <b><?php echo $Result[0]['StudentRole']; ?></b><br>
REG NO: <b><?php echo $Result[0]['StudentReg']; ?></b><br>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <tbody>
    <tr>
        <th rowspan="2" style="width:1%;">CODE</th>
        <th rowspan="2" width="3%">SUBJECT</th>
        <th colspan="9" width="5%"><?php echo $Result[0]['ExamName']; ?></th>
    </tr>
    <tr>
        <td>Full Mark</td>
        <td>Creative</td>
        <td>Multiple Choice Question</td>
        <td>Practical</td>
        <td>Total Obtained</td>
        <td>Letter Grade</td>
        <td>Total Obtained Mark</td>
        <td>GPA(With Optional Subject)</td>
        <td>GPA(Without Optional Subject)</td>
    </tr>
    <?php for ($A = 0;
               $A < (count($Result) - 8);
               $A++) {
    ?>
    <tr <?php if ($Result[$A]['is_optional'] == '1') { ?> style="background-color: #99BC99" <?php } ?>>
        <td><?php echo $Result[$A]['SubjectCode']; ?></td>
        <td><?php echo $Result[$A]['SubjectName']; ?></td>
        <td><?php echo $Result[$A]['credit']; ?></td>
        <td><?php echo $Result[$A]['written']; ?></td>
        <td><?php echo $Result[$A]['objective']; ?></td>
        <td><?php echo $Result[$A]['practical']; ?></td>
        <td><?php echo($Result[$A]['practical'] + $Result[$A]['objective'] + $Result[$A]['written']); ?></td>
        <?php if ($Result[$A]['SubjectCode'] == '101') { ?>
            <td style="vertical-align: middle" rowspan="2"><?php echo $Result[$A]['AlphaGPA']; ?></td>
        <?php } ?>
        <?php if ($Result[$A]['SubjectCode'] == '107') { ?>
            <td style="vertical-align: middle" rowspan="2"><?php echo $Result[$A]['AlphaGPA']; ?></td>
        <?php } ?>
        <?php if ($Result[$A]['SubjectCode'] != '101' && $Result[$A]['SubjectCode'] != '102' && $Result[$A]['SubjectCode'] != '107' && $Result[$A]['SubjectCode'] != '108') { ?>
            <td><?php echo $Result[$A]['AlphaGPA']; ?></td>
        <?php } ?>
        <?php if ($A == 0) { ?>
            <td style="vertical-align: middle"
                rowspan="<?php echo count($Result); ?>"><b><?php echo $Result['TotalNumberAchived']; ?></b></td>
            <td style="vertical-align: middle"
                rowspan="<?php echo count($Result); ?>"><b><?php echo $Result['CGPA']; ?></b></td>
            <td style="vertical-align: middle"
                rowspan="<?php echo count($Result); ?>"><b><?php echo $Result['CGPAwithFourthSubject']; ?></b></td>
            </tr>
        <?php }
} ?>
    <tr>

    </tr>
    </tbody>
</table>
</div>
