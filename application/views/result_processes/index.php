<?php
$rp_group_id = $this->session->userdata('rp_group_id');
$rp_exam_id = $this->session->userdata('rp_exam_id');
$rp_class_shift_section_id = $this->session->userdata('rp_class_shift_section_id');
?>
<form  name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>result_processes/index" method="post">
  <div class="form-row">
      <div class="form-group col-md-4">
        <label>Exam</label>
        <select name="exam_id" class="js-example-basic-single w-100" required="required">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php for ($A = 0; $A < count($ExamList); $A++) { ?>
                <option value="<?php echo $ExamList[$A]['id']; ?>" <?php if($rp_exam_id == $ExamList[$A]['id']){ echo 'selected'; } ?>>
                  <?php echo $ExamList[$A]['name'] . ' ('. $ExamList[$A]['year'] . ')'; ?>
                </option>
            <?php } ?>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
        <select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($class_section_shift_marge_list)) {
                foreach ($class_section_shift_marge_list as $list) {
                    $i++; ?>
                    <option
                        value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
                        <?php if (isset($rp_class_shift_section_id)) {
                        if ($rp_class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
                            echo 'selected';
                        }
                    } ?>>
                    <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
                  </option>
                    <?php
                }
            }
            ?>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label>Group</label>
        <select name="group_id" id="group_id" class="js-example-basic-single w-100" required="required">
             <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
             <?php for ($C = 0; $C < count($GroupList); $C++) { ?>
                 <option value="<?php echo $GroupList[$C]['id']; ?>" <?php if($rp_group_id == $GroupList[$C]['id']){ echo 'selected'; } ?>><?php echo $GroupList[$C]['name']; ?></option>
             <?php } ?>
         </select>
      </div>
  </div>


  <div class="float-right">
    <input class="btn btn-primary" type="submit" value="Process">
  </div>
</form>


<?php
if (isset($student_result)) {
?>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>result_processes/result_process_data_save_new" method="post">
  <div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table">
            <thead>
            <tr>
                <td colspan="6" style="text-align:center">
                    <b style="font-size:15px;"><?php echo $ResultInfo['school_name']; ?><b><br>
                    <!-- <b style="font-size:13px;">EIIN: <?php echo $ResultInfo['eiin_number']; ?><b><br> -->
                    <b style="font-size:13px;">Class: <?php echo $ResultInfo['ClassName']; ?>,
                    Section: <?php echo $ResultInfo['SectionName']; ?><b><br>
                    <b style="font-size:13px;">Exam: <?php echo $ResultInfo['ExamName']; ?><b>
                </td>
            </tr>
            <tr>
                <th scope="col">SL</th>
                <th scope="col">Name</th>
                <th scope="col">Roll</th>
                <th scope="col">Total Obtained Mark</th>
                <th scope="col">GPA(Without Optional)</th>
                <th scope="col">GPA(With Optional)</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
        foreach ($student_result as $row):
                $i++; ?>
                <tr>

                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['roll_no']; ?></td>
                    <td><?php echo $row['total_number_achieved']; ?></td>
                    <td><?php echo $row['CGPAwithFourthSubject']; ?></td>
                    <td><?php echo $row['CGPA']; ?></td>
                </tr>
            <?php endforeach; ?>

        				<tr>
        					<td colspan="6">
        						<input type="hidden" class="input-text-short" name="exam_id"
        						value="<?php echo $exam_id; ?>"/>
        						<input type="hidden" class="input-text-short" name="class_id"
        						value="<?php echo $class_id; ?>"/>
        						<input type="hidden" class="input-text-short" name="section_id"
        						value="<?php echo $section_id; ?>"/>
        						<input type="hidden" class="input-text-short" name="shift_id"
        						value="<?php echo $shift_id; ?>"/>
        						<input type="hidden" class="input-text-short" name="group_id"
        						value="<?php echo $group_id; ?>"/>
                    <div class="float-right">
                      <input class="btn btn-primary" type="submit" value="Save">
                    </div>
        					</td>
        				</tr>

            </tbody>
        </table>
</div>
</form>
    <?php
    }
    ?>
