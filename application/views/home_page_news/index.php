<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">   <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">News</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>

    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($news_info as $row):
        $i++;
        ?>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
             <td style="color:<?php echo $row['text_color']; ?>;"><?php echo $row['news']; ?></td>
            <td>
              <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="ti-pencil-alt"></i>
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                  <a class="dropdown-item"  href="<?php echo base_url(); ?>home_page_news/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                  <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>home_page_news/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
              </div>
            </td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>
