<script type="text/javascript" src="<?php echo base_url(); ?>core_media/js/jscolor/jscolor.js"></script>
<form name="updateForm" action="<?php echo base_url(); ?>home_page_news/edit/" method="post">
	<div class="form-row">
		<div class="form-group col-md-9">
				<label>News</label>
				<input type="text" autocomplete="off"  name="txtNews"  class="form-control" value="<?php echo $news_info->news; ?>">
		</div>
		<div class="form-group col-md-3">
				<label>Text Color</label>
				<input type="text" autocomplete="off"  name="txtTextColor" class="form-control color" value="<?php echo $news_info->text_color; ?>">
		</div>
	</div>

	<input type="hidden" autocomplete="off"  name="id"  class="form-control" value="<?php echo $news_info->id; ?>">
	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
