<script type="text/javascript" src="<?php echo base_url(); ?>core_media/js/jscolor/jscolor.js"></script>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>home_page_news/add/" method="post">
	<div class="form-row">
		<div class="form-group col-md-9">
              <label>News</label>
                <input type="text" autocomplete="off"  name="txtNews"  class="form-control" value="">
		</div>
		<div class="form-group col-md-3">
	         <label>Text Color</label>
              <input type="text" autocomplete="off"  name="txtTextColor" class="form-control color" value="000000">
		</div>
	</div>
	<div class="float-right">
		<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
