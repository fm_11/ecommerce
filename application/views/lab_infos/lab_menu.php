<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function () {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>lab_infos/index"><span>Lab Manager Dashboard</span></a></li>
    <li><a href="<?php echo base_url(); ?>lab_infos/get_all_lab"><span>Lab Information</span></a></li>
	<li><a href="<?php echo base_url(); ?>lab_infos/pc_info_index"><span>PC Information</span></a></li>
</ul>

