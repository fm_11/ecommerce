<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>    
        <th width="200" scope="col">Lab</th>
        <th width="200" scope="col">Serial/Name</th>
        <th width="200" scope="col">MAC Address</th>
    </tr>
    </thead>
    <tbody>


    <?php
    $i = 0;
    foreach ($on_pc as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34" style="vertical-align:middle">
                <?php echo $i; ?>
            </td>         
            <td style="vertical-align:middle"><?php echo $row['lab_name']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['serial']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['mac_address']; ?></td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>