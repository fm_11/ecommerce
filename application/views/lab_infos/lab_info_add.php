<?php
if ($action == 'edit') {
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>lab_infos/lab_edit" method="post">
        <label>Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="name" value="<?php echo $lab_info[0]['name']; ?>"
               required="1"/>

        <label>Number of Computer</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $lab_info[0]['num_of_computer']; ?>" name="num_of_computer"
               required="1"/>

       

        <br>
        <br>
        <input type="hidden" name="id" value="<?php echo $lab_info[0]['id']; ?>">
        <input type="submit" class="submit" value="Update">
    </form>
    <br/>

    <div class="clear"></div><br/>


<?php
} else {
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>lab_infos/lab_add" method="post">
        <label>Name</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="name" required="1"/>

        
        <label>Number of Computer</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="num_of_computer" required="1"/>

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
    <br/>

    <div class="clear"></div><br/>

<?php
}
?>


