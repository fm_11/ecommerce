<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>


<table width="100%">
    <tr>
        <td>
            <h2>
                <a class="button_grey_round" style="margin-bottom: 5px;"
                   href="<?php echo base_url(); ?>lab_infos/lab_add"><span>Add New lab</span></a>
            </h2>
        </td>
    </tr>
</table>



<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>	
        <th width="200" scope="col">Name</th>
		<th width="200" scope="col">Number of Computer</th>               
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>


    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($labs as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34" style="vertical-align:middle">
                <?php echo $i; ?>
            </td>          
            <td style="vertical-align:middle"><?php echo $row['name']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['num_of_computer']; ?></td>
           
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>admin_logins/lab_edit/<?php echo $row['id']; ?>"
                   class="edit_icon" title="Edit"></a>
                <a href="<?php echo base_url(); ?>admin_logins/lab_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>

            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="8" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>