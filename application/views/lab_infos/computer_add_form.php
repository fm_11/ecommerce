<?php
if ($action == 'edit') {
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>lab_infos/computer_edit" method="post">
        <label>Serial</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="serial" value="<?php echo $pc_info[0]['serial']; ?>"
               required="1"/>

        <label>Mac Address</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="mac_address" value="<?php echo $pc_info[0]['mac_address']; ?>"
               required="1"/>

        <label>Processor ID</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="processor_id" value="<?php echo $pc_info[0]['processor_id']; ?>"
               required="1"/>
      
        <label>Lab</label>
        <select class="smallInput" name="lab_id" id="lab_id" required="1">
            <option value="">-- Select --</option>
            <?php
            $i = 0;
            if (count($lab)) {
                foreach ($lab as $list) {
                    $i++;
                    ?>
                    <option
                            value="<?php echo $list['id']; ?>" <?php if ($pc_info[0]['lab_id'] == $list['id']) {
                        echo 'selected';
                    } ?>><?php echo $list['name']; ?></option>
                    <?php
                }
            }
            ?>
        </select>

       

        <br>
        <br>
        <input type="hidden" name="id" value="<?php echo $pc_info[0]['id']; ?>">
        <input type="submit" class="submit" value="Update">
    </form>
    <br/>

    <div class="clear"></div><br/>


<?php
} else {
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>lab_infos/computer_add" method="post">
        <label>Serial</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="serial" required="1"/>

		<label>MAC Address</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="mac_address" required="1"/>
		
		<label>Processor ID</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="processor_id" required="1"/>
		
        <label>Lab</label>
        <select class="smallInput" name="lab_id" required="1">
            <option value="">-- Select --</option>
            <?php
            $i = 0;
            if (count($lab)) {
                foreach ($lab as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>


        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
    <br/>

    <div class="clear"></div><br/>

<?php
}
?>
