<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/highcharts/js/jquery.min.js"></script>
<style type="text/css">
    ${demo.css}
</style>

<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/highcharts/js/highcharts.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/highcharts/js/modules/exporting.js"></script>


<table width="100%" align="center" cellpadding="0" cellspacing="0">
    <tr>
	<hr>
        <td align="center">
            <select class="smallInput" name="lab_id" onchange="getCurrentValue()" style="width:200px;" id="lab_id">
                <option value="">-- Select Lab --</option>
                <?php
                $i = 0;
                if (count($lab)) {
                    foreach ($lab as $list) {
                        $i++;
                        ?>
                        <option
                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </td>
    </tr>

    <tr>
        <td  style="text-align: center;" align="center">
            <div style="height: auto; margin-left: 140px; width: 100%">
                <div class="wrap">
                    <div class="box box2 shadow2">
                        <h3>
                            Total Lab<br>
                            <span class="value" id="total_lab">Load...</span>
                        </h3>
                    </div>
                    <div class="box box3 shadow3">
                        <h3>
                            Total Computer<br>
                            <span class="value" id="total_pc">Load...</span>
                        </h3>
                    </div>
                    <div class="box box4 shadow4">
                        <h3>
                            <a href="<?php echo base_url(); ?>lab_infos/getOnComputer" target="_blank" style="color: black;">ON
                                Computer</a><br>
                            <span class="value" id="on_pc">Load...</span>
                            <input type="hidden" id="on_pc_hidden" value="0">

                            <input type="hidden" id="current_time_hidden" value="">
                            <input type="hidden" id="old_time_hidden" value="">
                        </h3>
                    </div>

                    <div class="box box1 shadow1">
                        <h3>
                            OFF Computer<br>
                            <span class="value" id="off_pc">Load...</span>
                            <input type="hidden" id="off_pc_hidden" value="0">
                        </h3>
                    </div>
                </div>
            </div>
        </td>
    </tr>

    <tr>
        <td style="text-align: center;">
            <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        </td>
    </tr>
</table>


<style>
    .wrap {
        margin-left: 15px;
    }

    .value {
        font-size: 20px;
        color: #A70000;
    }

    .box {
        width: 13%;
        height: 100px;
        float: left;
        background-color: white;
        margin: 25px 15px;
        border-radius: 5px;
    }

    .box h3 {
        font-family: 'Didact Gothic', sans-serif;
        font-weight: normal;
        text-align: center;
        padding-top: 25px;
        color: #000033;
        font-weight: bold;
    }

    .box1 {
        background-color: #EBA39E;
    }

    .box2 {
        background-color: #EDE89A;
    }

    .box3 {
        background-color: #9EEBA1;
    }

    .box4 {
        background-color: #9EEBBF;
    }
</style>



<script>

    window.onload(getCurrentValue());

    function getCurrentValue() {
        var lab_id = document.getElementById("lab_id").value;

        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var data = JSON.parse(xmlhttp.responseText);
                document.getElementById("total_lab").innerHTML = data.total_lab;
                document.getElementById("total_pc").innerHTML = data.total_pc;
                document.getElementById("on_pc").innerHTML = data.on_pc;
                document.getElementById("off_pc").innerHTML = data.off_pc;

                document.getElementById("on_pc_hidden").value = data.on_pc;
                document.getElementById("off_pc_hidden").value = data.off_pc;
                document.getElementById("old_time_hidden").value = data.old_time;
                document.getElementById("current_time_hidden").value = data.current_time;
                graphOpen();
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>lab_infos/getCurrentValue?lab_id=" + lab_id, true);
        xmlhttp.send();

    }


</script>

<script type="text/javascript">
    function graphOpen() {

        var on_pc_hidden = document.getElementById("on_pc_hidden").value;
        var off_pc_hidden = document.getElementById("off_pc_hidden").value;
        var current_time_hidden = document.getElementById("current_time_hidden").value;
        var old_time_hidden = document.getElementById("old_time_hidden").value;
		//alert(old_time_hidden);
        $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 1,//null,
                plotShadow: false
            },
            title: {
                text: '<b>Computer On/Off Status</b> <br><span style="color:red;">' + old_time_hidden + '</span> to<span style="color:red;">' + current_time_hidden + '</span>'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Computer On/Off Status',
                data: [
                    {
                        name: 'On PC',
                        y: Number(on_pc_hidden),
                        color: '#009900',
                        selected: true
                    },
                    {
                        name: 'Off PC',
                        y: Number(off_pc_hidden),
                        color: '#ff0000',
                        selected: true
                    }
                ]
            }]
        });
    }

</script>

