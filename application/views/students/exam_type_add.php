
<form name="addForm" class="cmxform" id="commentForm"    action="<?php echo base_url(); ?>exam_types/add" method="post">

<div class="form-row">
      <div class="form-group col-md-6">
          <label>Name</label>
          <input type="text" autocomplete="off"  name="name" class="form-control" required value="">
      </div>
      <div class="form-group col-md-6">
          <label>Is?</label>
          <select class="js-example-basic-single w-100" name="type" required="1">
              <option value="W">Weekly</option>
              <option value="M">Monthly</option>
              <option value="S">Semester</option>
          </select>
      </div>
</div>

<div class="float-right">
 <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
  <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
</div>
</form>
