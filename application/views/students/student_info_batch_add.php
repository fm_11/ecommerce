
  <div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th colspan="2" width="50" scope="col"><b>File Format Download</b></th>
    </tr>
    </thead>
    <tbody>
      <tr>
          <td>
              <b>Without Student ID (Short Data)</b>
          </td>
          <td>
              <a href="<?php echo base_url(); ?>uploads/StudentDataFormatShortForm.xls">Download</a>
          </td>
      </tr>
    <tr>
        <td>
            <b>With Student ID</b>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>uploads/Student Data Format With Student ID.xls">Download</a>
        </td>
    </tr>
    <tr>
        <td>
            <b>Without Student ID</b>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>uploads/Student Data Format Without Student ID.xls">Download</a>
        </td>
    </tr>
    </tbody>
</table>

<h4 class="card-title">Uload Data</h4>
<hr>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/student_info_batch_add" method="post"
      enctype="multipart/form-data">

      <div class="form-row">
            <div class="form-group col-md-4">
              <label>Year</label>
            	<select class="form-control" name="txtYear" required="1">
            		<option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
            		<option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
            		<option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
            	</select>
            </div>

            <div class="form-group col-md-4">
              <label>ID Segment Year</label>
            	<select class="form-control" name="txtIdSegmentYear" required="1">
            	    <option value="<?php echo date('Y') - 3; ?>"><?php echo date('Y') - 3; ?></option>
            	    <option value="<?php echo date('Y') - 2; ?>"><?php echo date('Y') - 2; ?></option>
            		<option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
            		<option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
            		<option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
            		<option value="<?php echo date('Y') + 2; ?>"><?php echo date('Y') + 2; ?></option>
            		<option value="<?php echo date('Y') + 3; ?>"><?php echo date('Y') + 3; ?></option>
            	</select>
            </div>

            <div class="form-group col-md-4">
              <label>Class</label>
                <select class="form-control" name="txtClass" required="1">
                    <option value="">-- Please Select --</option>
                    <?php
                    $i = 0;
                    if (count($class)) {
                        foreach ($class as $list) {
                            $i++; ?>
                            <option
                                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
      </div>

      <div class="form-row">
            <div class="form-group col-md-4">
              <label>Section</label>
              <select class="form-control" name="txtSection" required="1">
                  <option value="">-- Please Select --</option>
                  <?php
                  $i = 0;
                  if (count($section)) {
                      foreach ($section as $list) {
                          $i++; ?>
                          <option
                                  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>

            <div class="form-group col-md-4">
              <label>Group</label>
              <select class="form-control" name="txtGroup" required="1">
                  <option value="">-- Please Select --</option>
                  <?php
                  $i = 0;
                  if (count($group)) {
                      foreach ($group as $list) {
                          $i++; ?>
                          <option
                                  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>

            <div class="form-group col-md-4">
              <label>Shift</label>
              <select class="form-control" name="txtShift" required="1">
                  <option value="">-- Please Select --</option>
                  <?php
                  $i = 0;
                  if (count($shift)) {
                      foreach ($shift as $list) {
                          $i++; ?>
                          <option
                                  value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                          <?php
                      }
                  }
                  ?>
              </select>
            </div>
      </div>




      <div class="form-row">
            <div class="form-group col-md-4">
              <label>Is Auto Student ID?</label>
              <select class="form-control" name="isAutoID" required="1">
                  <option value="">-- Please Select --</option>
                  <option value="Y">Yes</option>
                  <option value="N">No</option>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label>Data Format Type?</label>
              <select class="form-control" name="DataTypeFormat" required="1">
                  <option value="SF">Short Format</option>
                  <option value="LF">Long Data Format</option>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label>File</label>
              <input type="file" name="txtFile" required="1" class="form-control"> * File Format -> Excel-2003

            </div>
      </div>

<div class="float-right">
<button type="submit" class="btn btn-primary mr-2">Upload</button>
</div>
</form>
</div>
