<style>
    .center_td{
        text-align:center;
        vertical-align:middle;
    }
    input:-moz-read-only { /* For Firefox */
        background-color: #f2f2f2;
    }

    input:read-only {
        background-color: #f2f2f2;
    }
</style>
<script>
    function readonlyOnOff(value,id){
        if(value == 0){
           document.getElementById(id).setAttribute("readonly","readonly");
           document.getElementById(id).value = "";
        }else{
           document.getElementById(id).removeAttribute("readonly");
        }
    }

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
            return true;
        } else if ( key < 48 || key > 57 ) {
            return false;
        } else {
           return true;
        }
   }

   function checkCreditVsNumber(row_num,typing_id){
        var credit = document.getElementById("credit_" + row_num).value;
        var is_class_test_allow = document.getElementById("is_class_test_allow_" + row_num).value;

        var class_test = Number(document.getElementById("class_test_" + row_num).value);
        var is_written_allow = document.getElementById("is_written_allow_" + row_num).value;
        var written = Number(document.getElementById("written_" + row_num).value);
        var is_objective_allow = document.getElementById("is_objective_allow_" + row_num).value;
        var objective = Number(document.getElementById("objective_" + row_num).value);
        var is_practical_allow = document.getElementById("is_practical_allow_" + row_num).value;
        var practical = Number(document.getElementById("practical_" + row_num).value);
        var total_number = 0;
        if(is_class_test_allow == 1){
           total_number = total_number + class_test;
        }
        if(is_written_allow == 1){
           total_number = total_number + written;
        }
        if(is_objective_allow == 1){
           total_number = total_number + objective;
        }
        if(is_practical_allow == 1){
           total_number = total_number + practical;
        }
        //alert(total_number);
        if(total_number > credit){
            alert("Adding all the numbers can not be more than " + credit);
            document.getElementById(typing_id).value = "";
            return false;
        }else{
            return true;
        }
   }

</script>
<?php //echo '<pre>'; print_r($subject); die;?>
<form name="addForm" class="cmxform" id="commentForm"
      action="<?php echo base_url(); ?>students/grace_number_save" method="post">
      <div class="table-sorter-wrapper col-lg-12 table-responsive">
        <table id="sortable-table-1" class="table">
    <tr>
            <td style="vertical-align:middle;">
                Subject
            </td>

			<td class="center_td">
                 Class Test / Other
            </td>
			<td style="vertical-align:middle;">
                 Creative
            </td>
			<td style="vertical-align:middle;">
                 Objective
            </td>
			<td style="vertical-align:middle;">
                 Practical
            </td>
        </tr>

        <?php for ($i = 0; $i < count($subject); $i++) { ?>
            <tr>
                <td style="width:200px; vertical-align:middle;">
                    <?php echo $subject[$i]['subject_name']; ?> (<?php echo $subject[$i]['code']; ?>)
                    <input name="subject_id_<?php echo $i; ?>"
                           type="hidden" value="<?php echo $subject[$i]['subject_id']; ?>">
                 </td>

				<td style="vertical-align:middle;">
				      <input name="class_test_grace_<?php echo $i; ?>" onkeypress="javascript:return validateNumber(event)" id="class_test_<?php echo $i; ?>" size="10" <?php if ($subject[$i]['is_class_test_allow'] == '' || $subject[$i]['is_class_test_allow'] == '0') {
    echo 'readonly';
} ?> class="form-control" type="text" value="<?php if ($subject[$i]['class_test_grace'] == '') {
    echo 0;
} else {
    echo $subject[$i]['class_test_grace'];
} ?>">
				</td>

				<td style="vertical-align:middle;">
				      <input name="written_grace_<?php echo $i; ?>" onkeypress="javascript:return validateNumber(event)" id="written_<?php echo $i; ?>" type="text" size="10" <?php if ($subject[$i]['is_written_allow'] == '' || $subject[$i]['is_written_allow'] == '0') {
    echo 'readonly';
} ?> class="form-control" value="<?php if ($subject[$i]['written_grace'] == '') {
    echo 0;
} else {
    echo $subject[$i]['written_grace'];
} ?>">
				</td>
				<td style="vertical-align:middle;">
				     <input name="objective_grace_<?php echo $i; ?>" onkeypress="javascript:return validateNumber(event)"  id="objective_<?php echo $i; ?>" type="text" <?php if ($subject[$i]['is_objective_allow'] == '' || $subject[$i]['is_objective_allow'] == '0') {
    echo 'readonly';
} ?>  size="10" class="form-control" value="<?php if ($subject[$i]['objective_grace'] == '') {
    echo 0;
} else {
    echo $subject[$i]['objective_grace'];
} ?>">
				</td>
				<td style="vertical-align:middle;">
			     	<input name="practical_grace_<?php echo $i; ?>" onkeypress="javascript:return validateNumber(event)" id="practical_<?php echo $i; ?>" type="text" <?php if ($subject[$i]['is_practical_allow'] == '' || $subject[$i]['is_practical_allow'] == '0') {
    echo 'readonly';
} ?> size="10" class="form-control" value="<?php if ($subject[$i]['practical_grace'] == '') {
    echo 0;
} else {
    echo $subject[$i]['practical_grace'];
} ?>">
				</td>
            </tr>
        <?php } ?>


    </table>
  </div>

     <input type="hidden" class="form-control" name="loop_time"
           value="<?php echo $i; ?>"/>
     <input type="hidden" class="form-control" name="class_id"
           value="<?php echo $class_id; ?>"/>
    <input type="hidden" class="form-control" name="exam_id"
           value="<?php echo $exam_id; ?>"/>

    <div class="float-right">
      <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
