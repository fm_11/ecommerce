<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/student_allocate_for_resident_data_save" method="post">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <thead>
        <tr>
            <th width="50" scope="col">#</th>
            <th width="200" scope="col">Name</th>
            <th width="200" scope="col">Student ID</th>
            <th width="100" scope="col">Roll</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($student_info as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td width="34">
                    <input type="checkbox" <?php if($row['already_save'] > 0){echo 'checked';} ?> name="is_allow_<?php echo $i; ?>">
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" class="input-text-short" size="8"
                           style="text-align: right; width:75px;"
                           name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
                <td>
                    <?php echo $row['roll_no']; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <br>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="year"
           value="<?php echo $year; ?>"/>

    <input type="hidden" class="input-text-short" name="class_id"
           value="<?php echo $class_id; ?>"/>

    <input type="hidden" class="input-text-short" name="section_id"
           value="<?php echo $section_id; ?>"/>

    <input type="hidden" class="input-text-short" name="group"
           value="<?php echo $group; ?>"/>
		   
   <input type="hidden" class="input-text-short" name="shift_id"
value="<?php echo $shift_id; ?>"/>

    <input type="submit" class="submit" value="Save">

</form>
<br />
<div class="clear"></div>
