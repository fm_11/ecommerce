
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/student_code_config" method="post">
    <div class="form-row">
            <div class="form-group col-md-6">
              <label>Is Auto Code Enable ?</label>
              <input type="radio" value="1" checked
                     name="is_auto_code_enable" <?php if (isset($config_info)) {
    if ($config_info[0]['is_auto_code_enable'] == '1') {
        echo 'checked';
    }
} ?> required/>Yes
              <input type="radio" value="0"
                     name="is_auto_code_enable" <?php if (isset($config_info)) {
    if ($config_info[0]['is_auto_code_enable'] == '0') {
        echo 'checked';
    }
} ?>/>No
            </div>
    </div>


    <div class="form-row">
            <div class="form-group col-md-6">
              <label>Segment 1:</label>
                <select name="segment_1" class="form-control" required="required">
                    <option value="NA">-- Not Applicable --</option>
                    <option value="ST" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'ST') {
        echo 'selected';
    }
} ?>>ST (Student)
                    </option>
                <option value="S" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'S') {
        echo 'selected';
    }
} ?>>S (School Section)
                    </option>
                <option value="C" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'C') {
        echo 'selected';
    }
} ?>>C (College Section)
                    </option>

                <option value="K" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'K') {
        echo 'selected';
    }
} ?>>K (Kindergarten Section)
                    </option>

                <option value="P" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'P') {
        echo 'selected';
    }
} ?>>P (Primary Section)
                    </option>


                    <option value="HS" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'HS') {
        echo 'selected';
    }
} ?>>HS (H.S.C Science)
                    </option>

                    <option value="HA" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'HA') {
        echo 'selected';
    }
} ?>>HA (H.S.C Arts)
                    </option>

                    <option value="HC" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'HC') {
        echo 'selected';
    }
} ?>>HC (H.S.C Commerce)
                    </option>

                     <option value="SS" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'SS') {
        echo 'selected';
    }
} ?>>SS (S.S.C Science)
                    </option>

                    <option value="SA" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'SA') {
        echo 'selected';
    }
} ?>>SA (S.S.C Arts)
                    </option>

                    <option value="SC" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'SC') {
        echo 'selected';
    }
} ?>>SC (S.S.C Commerce)
                    </option>


                    <option value="L" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_1'] == 'L') {
        echo 'selected';
    }
} ?>>L
                    </option>

                </select>
            </div>

            <div class="form-group col-md-6">
              <label>Segment 2:</label>
                <select name="segment_2" class="form-control" required="required">
                    <option value="YF" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_2'] == 'YF') {
        echo 'selected';
    }
} ?>>Year Full
                    </option>
                <option value="YS" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_2'] == 'YS') {
        echo 'selected';
    }
} ?>>Year Short
                    </option>
                </select>
            </div>
    </div>

    <div class="form-row">

            <div class="form-group col-md-6">
              <label>Segment 3:</label>
                <select name="segment_3" class="form-control" required="required">
                    <option value="CS" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_3'] == 'CS') {
        echo 'selected';
    }
} ?>>Class Short Form
                    </option>
                </select>
            </div>

            <div class="form-group col-md-6">
              <label>Segment 4:</label>
              <select name="segment_4" class="form-control" required="required">
                  <option value="NA">-- Not Applicable --</option>
                  <option value="Y" <?php if (isset($config_info)) {
    if ($config_info[0]['segment_4'] == 'Y') {
        echo 'selected';
    }
} ?>>Group Short Code
                  </option>
              </select>
            </div>
    </div>

    <div class="form-row">

            <div class="form-group col-md-6">
              <label>Auto Increment Option:</label>
                <select name="auto_increament_option" class="form-control" required="required">
                    <option value="CW" <?php if (isset($config_info)) {
    if ($config_info[0]['auto_increament_option'] == 'C') {
        echo 'selected';
    }
} ?>>Class Wise (Per Year)
                    </option>

                    <option value="FSW" <?php if (isset($config_info)) {
    if ($config_info[0]['auto_increament_option'] == 'FSW') {
        echo 'selected';
    }
} ?>>Full School Wise (Per Year)
                    </option>
                </select>

            </div>

            <div class="form-group col-md-6">
              <label>Auto Increment Code Length:</label>
                <select name="auto_inc_code_length" class="form-control" required="required">
                    <option value="2" <?php if (isset($config_info)) {
    if ($config_info[0]['auto_inc_code_length'] == '2') {
        echo 'selected';
    }
} ?>>2
                    </option>

            		 <option value="3" <?php if (isset($config_info)) {
    if ($config_info[0]['auto_inc_code_length'] == '3') {
        echo 'selected';
    }
} ?>>3
                    </option>

            		 <option value="4" <?php if (isset($config_info)) {
    if ($config_info[0]['auto_inc_code_length'] == '4') {
        echo 'selected';
    }
} ?>>4
                    </option>

            		 <option value="5" <?php if (isset($config_info)) {
    if ($config_info[0]['auto_inc_code_length'] == '5') {
        echo 'selected';
    }
} ?>>5
                    </option>
                </select>
            </div>
    </div>

    <div class="form-row">

            <div class="form-group col-md-6">
              <label>Code Separator:</label>
                <select name="code_separator" class="form-control" required="required">
                    <option value="NA">-- Not Applicable --</option>
                    <option value="." <?php if (isset($config_info)) {
    if ($config_info[0]['code_separator'] == '.') {
        echo 'selected';
    }
} ?>>.
                    </option>
                <option value="-" <?php if (isset($config_info)) {
    if ($config_info[0]['code_separator'] == '-') {
        echo 'selected';
    }
} ?>>-
                    </option>
                </select>
            </div>
    </div>

	<input type="hidden" name="id" required class="form-control wide" size="20%" value="<?php echo $config_info[0]['id']; ?>">
  <div class="float-right">
     <input class="btn btn-primary" type="submit" value="Save">
  </div>
</form>
