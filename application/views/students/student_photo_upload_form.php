
<form name="addForm" class="cmxform" id="commentForm"
      action="<?php echo base_url(); ?>students/student_photo_upload_data_save" enctype="multipart/form-data" method="post">
      <div class="table-sorter-wrapper col-lg-12 table-responsive">
        <table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th scope="col">SL.</th>
            <th scope="col">&nbsp;Image</th>
            <th scope="col">Name</th>
			      <th scope="col">Student ID</th>
            <th  scope="col">Roll</th>
            <th scope="col">Reg. No.</th>
            <th scope="col">Photo</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($students as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td class="td_ver">
                    <?php echo $i; ?>
                </td>
                 <td class="td_ver">
                        &nbsp;
                        <img style="height:60px; width:50px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/<?php if ($row['photo']!='') {
                echo 'student/'.$row['photo'];
            } else {
                echo 'img/not_found.jpg';
            } ?>">
                    </td>
                <td class="td_ver">
                    <?php echo $row['name']; ?>
                    <input type="hidden" class="input-text-short"
                           name="id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>

				<td class="td_ver">
                    <?php echo $row['student_code']; ?>
                </td>

                <td class="td_ver">
                    <?php echo $row['roll_no']; ?>
                </td>
                <td class="td_ver">
                    <?php echo $row['reg_no']; ?>
                </td>

				<td class="td_ver">
				     <input type="file" name="photo_<?php echo $i; ?>" />
				</td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
  </div>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>

   <div class="float-right">
   <button type="submit" class="btn btn-primary mr-2">Update</button>
   </div>

</form>
