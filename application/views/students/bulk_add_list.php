<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/bulk_add_data_save" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<thead>
			<tr>
				<th scope="col">Roll</th>
				<th scope="col">Name (English)</th>
				<th scope="col">Name (Bangla)</th>
				<th scope="col">Gender</th>
				<th scope="col">Religion</th>
				<th scope="col">Father's Name</th>
				<th scope="col">Mother's Name</th>
				<th scope="col">Mobile</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$i = 1;
			while ($i <= $number_of_rows){
				?>
				<tr>

				<tr>
					<td>
						<input style="width: 60px !important; padding: 6px;border:1px #737373 solid;" class="form-control" type="text" name="roll_<?php echo $i; ?>" value=""/>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;" class="form-control" type="text" name="name_english_<?php echo $i; ?>" value=""/>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;"  class="form-control" type="text" name="name_bangla_<?php echo $i; ?>" value=""/>
					</td>
					<td>
						<select style="width: 80px !important;adding: 6px;border:1px #737373 solid; color: #000000;" class="form-control" name="gender_<?php echo $i; ?>" required="required">
							<option value="M">Male</option>
							<option value="F">Female</option>
							<option value="O">Other</option>
						</select>
					</td>
					<td>
						<select style="width: 120px !important;adding: 6px;border:1px #737373 solid;color: #000000;" class="form-control" name="religion_<?php echo $i; ?>" required>
							<?php
							if (count($religions)) {
								foreach ($religions as $list) {
									?>
									<option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
									<?php
								}
							} ?>
						 </select>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;"  class="form-control" type="text" name="fathers_name_<?php echo $i; ?>" value=""/>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;"  class="form-control" type="text" name="mothers_name_<?php echo $i; ?>" value=""/>
					</td>
					<td>
						<input style="padding: 6px; width: 150px;border:1px #737373 solid;"  class="form-control" type="text" name="mobile_<?php echo $i; ?>" value=""/>
					</td>
				</tr>
			<?php $i++; } ?>
			</tbody>
		</table>
	</div>
	<input type="hidden" class="input-text-short" name="year"
		   value="<?php echo $year; ?>"/>

	<input type="hidden" class="input-text-short" name="class_id"
		   value="<?php echo $class_id; ?>"/>

	<input type="hidden" class="input-text-short" name="section_id"
		   value="<?php echo $section_id; ?>"/>

	<input type="hidden" class="input-text-short" name="group_id"
		   value="<?php echo $group_id; ?>"/>

	<input type="hidden" class="input-text-short" name="category_id"
		   value="<?php echo $category_id; ?>"/>

	<input type="hidden" class="input-text-short" name="shift_id"
		   value="<?php echo $shift_id; ?>"/>

	<input type="hidden" class="input-text-short" name="number_of_rows"
		   value="<?php echo $number_of_rows; ?>"/>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>

</form>
