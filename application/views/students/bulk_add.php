<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/bulk_add" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('year'); ?></label><br>
			<select class="js-example-basic-single w-100" name="year" id="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['value']; ?>"
							<?php if(isset($year)){ if ($year == $list['value']) {
								echo 'selected';
							}} ?>><?php echo $list['text']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label><br>
			<select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
							<?php if (isset($class_shift_section_id)) {
								if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('group'); ?></label><br>
			<select class="js-example-basic-single w-100" name="group_id" id="group_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($groups)) {
					foreach ($groups as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['id']; ?>"
							<?php if(isset($group_id)){ if ($group_id == $list['id']) {
								echo 'selected';
							}} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Category</label><br>
			<select class="js-example-basic-single w-100" name="category_id" id="category_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				if (count($categories)) {
					foreach ($categories as $list) {
						?>
						<option
								value="<?php echo $list['id']; ?>"
								<?php if(isset($category_id)){ if ($category_id == $list['id']) {
									echo 'selected';
								}} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Number of Rows</label><br>
			<input type="text" autocomplete="off" required  class="form-control" value="<?php if(isset($number_of_rows)){
				echo $number_of_rows;
			} ?>" name="number_of_rows"/>
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('process'); ?>">
	</div>
</form>

<?php
//echo $group_id; die;
if (isset($bulk_add_list)) {
	echo $bulk_add_list;
}
?>
