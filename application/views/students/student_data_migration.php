<form action="<?php echo base_url(); ?>students/student_data_migration"
      enctype="multipart/form-data" method="post">
    
    <label>From Year</label>
    <select name="from_year" class="smallInput" id="from_year" required="1">
        <option value="">-- Select --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>
    
    <label>To Year</label>
    <select name="to_year" class="smallInput" id="to_year" required="1">
        <option value="">-- Select --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>
    
    
    <label>From Class</label>
    <select class="smallInput" name="from_class" required="1">
        <option value="">-- Select --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>
    
    <label>To Class</label>
    <select class="smallInput" name="to_class" required="1">
        <option value="">-- Select --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>

    <label>
        <br>
        <input type="submit" class="submit" value="Process">
    </label>

</form>

