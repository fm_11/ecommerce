<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()"
      action="<?php echo base_url(); ?>students/student_data_migration_data_save" method="post">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <thead>
        <tr>
            <th width="10" scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
            <th width="50" scope="col">Name</th>
            <th width="35" scope="col">Current Roll</th>
            <th width="35" scope="col">New Roll</th>
            <th width="50" scope="col">Current Section</th>
            <th width="50" scope="col">New Section</th>
            <th width="35" scope="col">Current Group</th>
            <th width="35" scope="col">New Group</th>
            <th width="35" scope="col">Current Shift</th>
            <th width="35" scope="col">New Shift</th>
            <th width="35" scope="col">Status</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($students as $row):
            $i++;
            ?>
          

            <tr>
                <td>
                    <input type="checkbox" name="is_allow_<?php echo $i; ?>">
                </td>
                <td>
                    <input type="text" autocomplete="off"  readonly class="input-text-short" style="width: 100px;"
                           name="name_<?php echo $i; ?>" value="<?php echo $row['name']; ?>"/>
                    <input type="hidden" class="input-text-short"
                           name="id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <input type="text" autocomplete="off"  class="input-text-short" style="width: 25px;"
                           name="current_roll_no_<?php echo $i; ?>" value="<?php echo $row['roll_no']; ?>"/>
                </td>
                <td>
                    <?php
                    if($row['annual_position'] == ''){
                       $new_roll = $row['roll_no'];
                    }else{
                        $new_roll = str_pad($row['annual_position'], 2, "0", STR_PAD_LEFT);
                    }
                    
                    ?>
                    <input type="text" autocomplete="off"  class="input-text-short" style="width: 25px;"
                           name="new_roll_no_<?php echo $i; ?>" value="<?php echo $new_roll; ?>"/>
                </td>
                
                <td>
                    <select name="current_section_id_<?php echo $i; ?>" style="width: 50px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <?php foreach ($section as $row1) { ?>
                            <option value="<?php echo $row1['id']; ?>"<?php if ($row1['id'] == $row['section_id']) {
                                echo 'selected';
                            } ?>><?php echo $row1['name']; ?></option>
                        <?php } ?>
                    </select> &nbsp;
                </td>
                
                 <td>
                    <select name="new_section_id_<?php echo $i; ?>" style="width: 50px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <?php foreach ($section as $row1) { ?>
                            <option value="<?php echo $row1['id']; ?>"<?php if ($row1['id'] == $row['section_id']) {
                                echo 'selected';
                            } ?>><?php echo $row1['name']; ?></option>
                        <?php } ?>
                    </select> &nbsp;
                </td>
                
                
                <td>
                    <select name="current_group_<?php echo $i; ?>" style="width: 50px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <?php foreach ($group_list as $row3) { ?>
                            <option value="<?php echo $row3['id']; ?>"<?php if ($row3['id'] == $row['group']) {
                                echo 'selected';
                            } ?>><?php echo $row3['name']; ?></option>
                        <?php } ?>
                    </select> &nbsp;
                </td>
                
                <td>
                    <select name="new_group_<?php echo $i; ?>" style="width: 50px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <?php foreach ($group_list as $row3) { ?>
                            <option value="<?php echo $row3['id']; ?>"<?php if ($row3['id'] == $row['group']) {
                                echo 'selected';
                            } ?>><?php echo $row3['name']; ?></option>
                        <?php } ?>
                    </select> &nbsp;
                </td>
                
                
                <td>
                    <select name="current_shift_<?php echo $i; ?>" style="width: 50px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <?php foreach ($shifts as $row4) { ?>
                            <option value="<?php echo $row4['id']; ?>"<?php if ($row4['id'] == $row['shift_id']) {
                                echo 'selected';
                            } ?>><?php echo $row4['name']; ?></option>
                        <?php } ?>
                    </select> &nbsp;
                </td>
                
                <td>
                    <select name="new_shift_<?php echo $i; ?>" style="width: 50px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <?php foreach ($shifts as $row5) { ?>
                            <option value="<?php echo $row5['id']; ?>"<?php if ($row5['id'] == $row['shift_id']) {
                                echo 'selected';
                            } ?>><?php echo $row5['name']; ?></option>
                        <?php } ?>
                    </select> &nbsp;
                </td>
                
                 <td>
                    <select name="updated_status_<?php echo $i; ?>" style="width: 50px;" class="smallInput">
                        <option value="1">Active</option>
                        <option value="0">In Active</option>
                    </select> &nbsp;
                </td>
                
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <br>
    
    <input type="hidden" class="input-text-short"
                           name="from_class" value="<?php echo $from_class; ?>"/>
   <input type="hidden" class="input-text-short"
   name="to_class" value="<?php echo $to_class; ?>"/>
   
   
    <input type="hidden" class="input-text-short"
   name="from_year" value="<?php echo $from_year; ?>"/>
   
    <input type="hidden" class="input-text-short"
   name="to_year" value="<?php echo $to_year; ?>"/>
   
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="submit" class="submit" value="Migrate">

</form>
<br/>
<div class="clear"></div>
