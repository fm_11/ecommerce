<script type="text/javascript">
    function msgStatusUpdate(id, status) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>exams/updateMsgStatusExamStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }

</script>


   <div class="table-sorter-wrapper col-lg-12 table-responsive">
     <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Name</th>
         <th width="200" scope="col">Exam Type</th>
        <th width="200" scope="col">Year</th>
        <th width="200" scope="col">Publish Status</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
     $i = (int)$this->uri->segment(3);
    foreach ($exams as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['exam_type']; ?></td>
            <td><?php echo $row['year']; ?></td>

            <td  id="status_sction_<?php echo $row['id']; ?>">
                <?php
                if ($row['status'] == 1) {
                    ?>
                    <a class="badge badge-success mb-1" title="Published" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">Published</a>
                <?php
                } else {
                    ?>
                    <a class="badge badge-warning mb-1" title="Unpublished" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">Unpublished</a>
                <?php
                }
                ?>
            </td>

            <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" href="<?php echo base_url(); ?>exams/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>exams/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
 <?php echo $this->pagination->create_links(); ?>
 </div>
</div>
