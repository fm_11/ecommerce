<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>students/class_wise_extra_mark_head_add"><span>Add Class Wise Extra Mark</span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="150" scope="col">Class</th>
        <th width="150" scope="col">Exam Type</th>
        <th width="150" scope="col">Head</th>
        <th width="150" scope="col">Mark</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($heads as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['class_name']; ?></td>
            <td><?php echo $row['exam_type_name']; ?></td>
            <td><?php echo $row['head_name']; ?></td>
            <td><?php echo $row['mark']; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

