<style>
  .cus_input_cls{
    padding: 5px;
  }
</style>
<?php if ($exams->is_combined_result == 1) { ?>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table">
     <thead>
     <tr>
         <th scope="col">SL</th>
         <th scope="col">Exam Name</th>
         <th scope="col">Is Applicable?</th>
         <th scope="col">Percentage (From Total Mark)</th>
     </tr>
     </thead>
     <tbody>
       <?php
       $i = 0;
       foreach ($combined_exam_list as $row):

         ?>

         <tr>
             <td>
                 <?php echo $i + 1; ?>
             </td>
             <td><?php echo $row['name']; ?></td>

             <td>
               <select class="cus_input_cls" name="is_use_<?php echo $i; ?>">
                   <option value="0">No</option>
                   <option value="1"  <?php if (isset($row['combined_exam_id'])) {
             if ($row['combined_exam_id'] != '0') {
                 echo 'selected';
             }
         } ?>>Yes</option>
               </select>
             </td>

             <td>
               <input type="hidden" name="combined_exam_id_<?php echo $i; ?>"
                id="combined_exam_id_<?php echo $i; ?>" class="cus_input_cls" value="<?php echo $row['id']; ?>">
               <input type="text" autocomplete="off"  value="<?php if (isset($row['percentage'])) {
             echo $row['percentage'];
         } ?>" name="percentage_<?php echo $i; ?>" id="percentage_<?php echo $i; ?>" class="cus_input_cls">
             </td>
         </tr>
       <?php $i++; endforeach; ?>
     </tbody>
   </table>
   <input type="hidden" name="total_row" class="cus_input_cls" value="<?php echo $i; ?>">

</div>
<?php } ?>
