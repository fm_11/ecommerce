
   <div class="table-sorter-wrapper col-lg-12 table-responsive">
     <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">Name</th>
        <th scope="col">Type</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($exam_types as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td>
              <?php
                 if ($row['type'] == 'W') {
                     echo 'Weekly';
                 } elseif ($row['type'] == 'M') {
                     echo 'Monthly';
                 } else {
                     echo 'Semester';
                 }
              ?>
            </td>
            <td>
              
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                        <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>exam_types/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>

    </tbody>
  </table>
    </div>
