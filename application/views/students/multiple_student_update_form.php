<script>
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()"
      action="<?php echo base_url(); ?>students/multiple_student_update_data_save" method="post">
      <div class="table-sorter-wrapper col-lg-12 table-responsive">
        <table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th width="10" scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
            <th width="40" scope="col">Name</th>
            <th width="35" scope="col">Roll</th>
            <th width="40" scope="col">Father's Name</th>
            <th width="40" scope="col">Mother's Name</th>
            <th width="30" scope="col">Section</th>
            <th width="30" scope="col">Group</th>
            <th width="30" scope="col">Shift</th>
            <th width="30" scope="col">Gender</th>
            <th width="30" scope="col">Blood Group</th>
            <th width="30" scope="col">Religion</th>
            <th width="30" scope="col">Mobile</th>
            <th width="30" scope="col">Process Code</th>
            <?php
                if ($is_transport_fee_applicable != '0') {
                    ?>
            <th width="30" scope="col">Transport Fee</th>
            <?php
                }
            ?>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($students as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td>
                    <input type="checkbox" name="is_allow_<?php echo $i; ?>">
                </td>
                <td>
                    <input type="text" autocomplete="off"  class="input-text-short" style="width: 80px;"
                           name="name_<?php echo $i; ?>" value="<?php echo $row['name']; ?>"/>
                    <input type="hidden" class="input-text-short"
                           name="id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <input type="text" autocomplete="off"  class="input-text-short" style="width: 35px;"
                           name="roll_no_<?php echo $i; ?>" value="<?php echo $row['roll_no']; ?>"/>
                </td>

                <td>
                    <input type="text" autocomplete="off"  class="input-text-short" style="width: 80px;"
                           name="father_name_<?php echo $i; ?>" value="<?php echo $row['father_name']; ?>"/>
                </td>
                <td>
                    <input type="text" autocomplete="off"  class="input-text-short" style="width: 80px;"
                           name="mother_name_<?php echo $i; ?>" value="<?php echo $row['mother_name']; ?>"/>
                </td>
                <td>
                    <select name="section_id_<?php echo $i; ?>" style="width: 40px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <?php foreach ($section as $row1) { ?>
                            <option value="<?php echo $row1['id']; ?>"<?php if ($row1['id'] == $row['section_id']) {
                echo 'selected';
            } ?>><?php echo $row1['name']; ?></option>
                        <?php } ?>
                    </select> &nbsp;
                </td>
                <td>
                    <select name="group_<?php echo $i; ?>" style="width: 40px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <?php foreach ($group_list as $row3) { ?>
                            <option value="<?php echo $row3['id']; ?>"<?php if ($row3['id'] == $row['group']) {
                echo 'selected';
            } ?>><?php echo $row3['name']; ?></option>
                        <?php } ?>
                    </select> &nbsp;
                </td>


                <td>
                    <select name="shift_<?php echo $i; ?>" style="width: 40px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <?php foreach ($shift as $row3) { ?>
                            <option value="<?php echo $row3['id']; ?>"<?php if ($row3['id'] == $row['shift_id']) {
                echo 'selected';
            } ?>><?php echo $row3['name']; ?></option>
                        <?php } ?>
                    </select> &nbsp;
                </td>

                <td>
                    <select name="gender_<?php echo $i; ?>" style="width: 40px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <option value="M" <?php if (isset($row['gender'])) {
                if ($row['gender'] == 'M') {
                    echo 'selected';
                }
            } ?>>Male
                        </option>
                        <option value="F" <?php if (isset($row['gender'])) {
                if ($row['gender'] == 'F') {
                    echo 'selected';
                }
            } ?>>Female
                        </option>
                    </select> &nbsp;
                </td>
                <td>
                    <select name="blood_group_id_<?php echo $i; ?>" style="width: 40px;" class="smallInput">
                        <option value="">-- Blood Group --</option>
                        <?php foreach ($blood_group_list as $row2) { ?>
                            <option value="<?php echo $row2['id']; ?>" <?php if (isset($row['blood_group_id'])) {
                if ($row2['id'] == $row['blood_group_id']) {
                    echo 'selected';
                }
            } ?>><?php echo $row2['name']; ?></option>

                        <?php } ?>
                    </select> &nbsp;
                </td>
                <td>
                    <select name="religion_<?php echo $i; ?>" style="width:40px;" class="smallInput">
                        <option value="">-- Select --</option>
                        <option value="I" <?php if (isset($row['religion'])) {
                if ($row['religion'] == 'I') {
                    echo 'selected';
                }
            } ?>>Islam
                        </option>
                        <option value="H" <?php if (isset($row['religion'])) {
                if ($row['religion'] == 'H') {
                    echo 'selected';
                }
            } ?>>Hindu
                        </option>
                    </select>&nbsp;
                </td>
                <td>
                    <input type="text" autocomplete="off"  class="input-text-short" style="width: 50px;"
                           name="guardian_mobile_<?php echo $i; ?>" value="<?php echo $row['guardian_mobile']; ?>"/>
                </td>
                <td>
                    <input type="text" autocomplete="off"  class="input-text-short" style="width: 50px;"
                           name="process_code_<?php echo $i; ?>" value="<?php echo $row['process_code']; ?>"/>
                </td>


                <?php
                    if ($is_transport_fee_applicable != '0') {
                        ?>
                    <td>
                        <input type="text" autocomplete="off"  class="input-text-short" style="width: 50px;"
                               name="transport_fee_amount_<?php echo $i; ?>" value="<?php echo $row['transport_fee_amount']; ?>"/>
                    </td>
                <?php
                    }
                ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
  </div>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
   <div class="float-right">
      <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
   </div>
   </form>
