<script>
    function subject_by_class() {
        var ClassID = document.getElementById("class_id").value;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("SubjectSpace").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>index.php/students/ajax_subject_by_class?class_id=" + ClassID + "&&type=1", true);
        xmlhttp.send();
    }
</script>
<form action="<?php echo base_url(); ?>students/extra_marking_index" method="post">
    Exam: <select name="exam_id" class="smallInput" required="required">
        <option value="">--Select--</option>
        <?php for ($A = 0; $A < count($ExamList); $A++) { ?>
            <option value="<?php echo $ExamList[$A]['id']; ?>"><?php echo $ExamList[$A]['name']; ?></option>
        <?php } ?>
    </select>&nbsp;&nbsp;Class: <select name="class_id" class="smallInput" id="class_id" required="required" <?php if($add_extra_mark_with_total_obtained == 0){ ?>  onchange="subject_by_class()" <?php } ?>>
        <option value="">--Select--</option>
        <?php for ($B = 0; $B < count($ClassList); $B++) { ?>
            <option value="<?php echo $ClassList[$B]['id']; ?>"><?php echo $ClassList[$B]['name']; ?></option>
        <?php } ?>
    </select>
    
    
    &nbsp;&nbsp;Section: <select name="section_id"  class="smallInput" required="required">
        <option value="">--Select--</option>
        <?php for ($C = 0; $C < count($SectionList); $C++) { ?>
            <option value="<?php echo $SectionList[$C]['id']; ?>"><?php echo $SectionList[$C]['name']; ?></option>
        <?php } ?>
    </select>
    
     <br>
    <br>
    <hr>
    
     &nbsp;&nbsp;Group: <select name="group_id" class="smallInput"  required="required">
        <option value="">--Select--</option>
        <?php for ($C = 0; $C < count($GroupList); $C++) { ?>
            <option value="<?php echo $GroupList[$C]['id']; ?>"><?php echo $GroupList[$C]['name']; ?></option>
        <?php } ?>
    </select>
   
    
     &nbsp;&nbsp;Shift: <select name="shift_id" class="smallInput" required="required">
        <option value="">--Select--</option>
        <?php for ($C = 0; $C < count($ShiftList); $C++) { ?>
            <option value="<?php echo $ShiftList[$C]['id']; ?>"><?php echo $ShiftList[$C]['name']; ?></option>
        <?php } ?>
    </select>
    
    
    &nbsp;&nbsp;Subject:<span id="SubjectSpace">
        <select name="subject_id"  class="smallInput" id="subject_id" required="required">
            <?php
            if($add_extra_mark_with_total_obtained == 1){
            ?>
             <option value="NA">Not Applicable</option>
            <?php 
            }else{
            ?>
            <option value="">--Select--</option>
            <?php 
            }
            ?>
        </select></span>
    &nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="submit">Process</button>
</form>

<hr>