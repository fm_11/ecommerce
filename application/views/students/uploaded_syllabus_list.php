<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>students/student_syllabus_upload"><span>Add New Syllabus</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Class</th>
        <th width="200" scope="col">Year</th>
        <th width="100" scope="col">Page No</th>
        <th width="200" scope="col">Download</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($student_syllabus as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['class_name']; ?></td>

            <td><?php echo $row['year']; ?></td>
            <td><?php echo $row['page_no']; ?></td>
            <td>
                <a href="<?php echo base_url() . MEDIA_FOLDER; ?>/syllabus/<?php echo $row['location']; ?>"
                   target="_blank"><?php echo $row['location']; ?></a>
            </td>

            <td>
                <a href="<?php echo base_url(); ?>students/uploaded_syllabus_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="7" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>