<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/grand_exam_mark_data_save" method="post">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <thead>
        <tr>
            <th width="200" scope="col">Name</th>
            <th width="200" scope="col">Student ID</th>
            <th width="100" scope="col">Roll</th>		
			<th width="100" scope="col">Total Obtain Mark</th>			
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($student_info as $row):
            $i++;
            ?>
            <tr>

            <tr>              
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" class="input-text-short" size="8"
                           style="text-align: right; width:75px;"
                           name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
                <td>
                    <?php echo $row['roll_no']; ?>
                </td>
				
				<td>
                    <input type="text" autocomplete="off"  class="input-text-short" style="width: 200px;"
                           name="total_obtain_marks_<?php echo $i; ?>" value="<?php echo $row['total_obtain_marks']; ?>"/>
                </td>
            </tr>
        <?php endforeach; ?>
		
		    <tr>
			    <th colspan="3" align="right" scope="col">Total Mark of Exam</th>
				<th>
				<input type="text" autocomplete="off"  class="input-text-short" style="width: 200px;"
                        name="total_marks" value="<?php echo $student_info[0]['total_marks']; ?>"/>
				</th>
			</tr>
			
        </tbody>
    </table>
    <br>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="year"
           value="<?php echo $year; ?>"/>

    <input type="hidden" class="input-text-short" name="class_id"
           value="<?php echo $class_id; ?>"/>

    <input type="hidden" class="input-text-short" name="section_id"
           value="<?php echo $section_id; ?>"/>

    <input type="hidden" class="input-text-short" name="group"
           value="<?php echo $group; ?>"/>
		   
   <input type="hidden" class="input-text-short" name="shift_id"
   value="<?php echo $shift_id; ?>"/>
   
   <input type="hidden" class="input-text-short" name="exam_id"
   value="<?php echo $exam_id; ?>"/>
   
   <input type="hidden" class="input-text-short" name="percentage_of_grand_result"
   value="<?php echo $percentage_of_grand_result; ?>"/>

    <input type="submit" class="submit" value="Save">

</form>
<br />
<div class="clear"></div>
