
<form  name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>students/grace_number_index" method="post">
    <div class="form-row">
       <div class="form-group col-md-6">
         <label>Exam</label>
          <select name="exam_id" class="js-example-basic-single w-100" required="required">
              <option value="">--Select--</option>
              <?php for ($A = 0; $A < count($ExamList); $A++) { ?>
                  <option value="<?php echo $ExamList[$A]['id']; ?>"><?php echo $ExamList[$A]['name']. ' ('. $ExamList[$A]['year'] . ')'; ?></option>
              <?php } ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label>Class</label>
          <select name="class_id" class="js-example-basic-single w-100" id="class_id" required="required">
                <option value="">--Select--</option>
                <?php for ($B = 0; $B < count($ClassList); $B++) { ?>
                    <option value="<?php echo $ClassList[$B]['id']; ?>"><?php echo $ClassList[$B]['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="float-right">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('process'); ?>">
    </div>
</form>
