<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>students/student_code_config"><span>ID Config</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/class_index"><span>Class</span></a></li>
	<li><a href="<?php echo base_url(); ?>students/subject_index"><span>Subject</span></a></li>
    <li><a href="<?php echo base_url(); ?>class_wise_subjects/index"><span>Class Wise Subject</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/student_info_index"><span>Student Info.</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/student_result_upload"><span>Result Upload</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/student_routine_list"><span> Routine</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/student_syllabus_list"><span>Syllabus</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/exam_list"><span>Exam</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/marking_index"><span>Marking</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/result_index"><span>Result Process</span></a></li>
    <li><a href="<?php echo base_url(); ?>students/get_student_mark_sheet"><span>Mark Sheet</span></a></li>
</ul>

