
<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>students/student_photo_upload"
      enctype="multipart/form-data" method="post">
      <div class="form-row">
          <div class="form-group col-md-4">
            <label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
            <select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
                <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                <?php
                $i = 0;
                if (count($class_section_shift_marge_list)) {
                    foreach ($class_section_shift_marge_list as $list) {
                        $i++; ?>
                        <option
                            value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
                            <?php if (isset($class_shift_section_id)) {
                            if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
                                echo 'selected';
                            }
                        } ?>>
                        <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
                      </option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>

        <div class="form-group col-md-4">
          <label>Group</label>
          <select class="js-example-basic-single w-100" name="group" required="1">
              <option value="">-- Select --</option>
              <?php
              $i = 0;
              if (count($group_list)) {
                  foreach ($group_list as $list) {
                      $i++; ?>
                      <option
                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                      <?php
                  }
              }
              ?>
          </select>
        </div>
      </div>
      <div class="float-right">
      <button type="submit" class="btn btn-primary mr-2">Process</button>
      </div>
</form>
