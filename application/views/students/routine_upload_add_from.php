<h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/student_routine_upload" method="post" enctype="multipart/form-data">
        <label>Class</label>
        <select class="smallInput" name="txtClass" required="1">
        <option value="">-- Please Select --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                    <?php
                }
            }
            ?>           
       </select>
	   
	   <label>Section</label>
        <select class="smallInput" name="txtSection" required="1">
        <option value="">-- Please Select --</option>
        <?php
        $i = 0;
        if (count($section)) {
            foreach ($section as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                    <?php
                }
            }
            ?>           
       </select>
	   
	   <label>Group</label>
        <select class="smallInput" name="txtGroup" required="1">
        <option value="">-- Please Select --</option>
        <option value="S">Science</option>
        <option value="C">Commerce</option>
        <option value="A">Arts</option>
		<option value="N">N/A</option>		
       </select>
	   
	   <label>Year</label>
        <select class="smallInput" name="txtYear" required="1">
        <option value="">-- Please Select --</option>
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
		<option value="2018">2018</option>		
       </select>

        <label>Date</label>
        <input type="text" autocomplete="off"  class="smallInput" name="txtDate" id="txtDate" value="<?php echo date('Y-m-d') ?>"  required="1"/>

        <label>File</label>
        <input type="file" name="txtFile" required="1" class="smallInput"> * File Format -> PDF , DOC , DOCX  and xls
 
        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form><br />
<div class="clear"></div><br />

<script>
    $(function() {
        $( "#txtDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>

