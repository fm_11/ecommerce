
<form action="<?php echo base_url(); ?>students/multiple_student_update"
      enctype="multipart/form-data" method="post">
    <div class="form-row">
        <div class="form-group col-md-6">
          <label>Class</label>
          <select class="js-example-basic-single w-100" name="class" required="1">
              <option value="">-- Select --</option>
              <?php
              $i = 0;
              if (count($class)) {
                  foreach ($class as $list) {
                      $i++; ?>
                      <option
                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                      <?php
                  }
              }
              ?>
          </select>
        </div>
        <div class="form-group col-md-6">
          <label>Section</label>
          <select class="js-example-basic-single w-100" name="section" required="1">
              <option value="">-- Select --</option>
              <?php
              $i = 0;
              if (count($section)) {
                  foreach ($section as $list) {
                      $i++; ?>
                      <option
                              value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                      <?php
                  }
              }
              ?>
          </select>
        </div>
   </div>
   <div class="form-row">
       <div class="form-group col-md-6">
         <label>Group</label>
         <select class="js-example-basic-single w-100" name="group" required="1">
             <option value="">-- Select --</option>
             <?php
             $i = 0;
             if (count($group_list)) {
                 foreach ($group_list as $list) {
                     $i++; ?>
                     <option
                             value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                     <?php
                 }
             }
             ?>
         </select>
       </div>
       <div class="form-group col-md-6">
         <label>Shift</label>
         <select class="js-example-basic-single w-100" name="shift_id" required="1">
             <option value="">-- Select --</option>
             <?php
             $i = 0;
             if (count($shift)) {
                 foreach ($shift as $list) {
                     $i++; ?>
                     <option
                             value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                     <?php
                 }
             }
             ?>
         </select>
       </div>
  </div>
    <div class="float-right">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
    </form>
