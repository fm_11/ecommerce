<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>students/class_wise_extra_mark_head_index"><span>Back to List</span></a>
</h2>
<hr>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/class_wise_extra_mark_head_add" method="post">
    <label>Exam Type</label>
    <select name="exam_type_id" class="smallInput" required="1" id="exam_type_id">
        <option value="">--Please Select--</option>
        <?php foreach ($exam_types as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Class</label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--Please Select--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>
    <br><br>
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tr>
            <th>
               #
            </th>
            <th>
                Head
            </th>
            <th>
                Mark
            </th>
        </tr>

        <?php
        $i = 0;
        foreach ($heads as $row):
            ?>
            <tr>
                <td>
                    <input type="checkbox" name="is_allow_<?php echo $i; ?>">
                </td>
                <td><?php echo $row['name']; ?></td>
                <td>
                    <input size="10" value="" name="mark_<?php echo $i; ?>" type="text">

                    <input size="10" value="<?php echo $row['id']; ?>" required="required"
                           name="head_id_<?php echo $i; ?>" type="hidden">
                </td>
            </tr>
            <?php $i++; endforeach; ?>
    </table>

    <br>
    <br>
    <input size="10" value="<?php echo $i; ?>" required="required"
           name="loop_time" type="hidden">
    <input type="submit" class="submit" value="Submit">
</form>
