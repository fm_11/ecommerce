<div class="row">
      <div class="col-12">
              <div class="card mb-4">
                  <div class="card-body">
                      <h5 class="mb-4">
                        <?php echo $title; ?>
                      </h5>
        <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/print_admit" method="post"
              enctype="multipart/form-data">
            <label>Exam</label>
            <select class="smallInput" name="txtExam" required="1">
                <option value="">-- Please Select --</option>
                <?php
                $i = 0;
                if (count($exam)) {
                    foreach ($exam as $list) {
                        $i++;
                        ?>
                        <option
                            value="<?php echo $list['name'].' ('.$list['year'].')'; ?>"><?php echo $list['name'].' ('.$list['year'].')'; ?></option>
                        <?php
                    }
                }
                ?>
            </select>

            <label>Class</label>
            <select class="smallInput" name="txtClass" required="1">
                <option value="">-- Please Select --</option>
                <?php
                $i = 0;
                if (count($class)) {
                    foreach ($class as $list) {
                        $i++;
                        ?>
                        <option
                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>

            <label>Section</label>
            <select class="smallInput" name="txtSection" required="1">
                <option value="">-- Please Select --</option>
                <?php
                $i = 0;
                if (count($section)) {
                    foreach ($section as $list) {
                        $i++;
                        ?>
                        <option
                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>

            <label>Group</label>
            <select class="smallInput" name="txtGroup" required="1">
                <option value="">-- Please Select --</option>
                <?php
                $i = 0;
                if (count($group)) {
                    foreach ($group as $list) {
                        $i++;
                        ?>
                        <option
                            value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>

             <label>Student</label>
            <select class="smallInput" id="student_id" name="txtStudent">
                <option value="all">-- All --</option>
                <?php
                $i = 0;
                if (count($students)) {
                    foreach ($students as $list) {
                        $i++;
                        ?>
                        <option
                            value="<?php echo $list['id']; ?>"><?php echo $list['name'].'('.$list['student_code'].')'; ?></option>
                        <?php
                    }
                }
                ?>
            </select>

            <br>
            <br>
            <input type="submit" name="Preview" class="submit" value="Preview">
            <input type="submit" name="is_pdf_request" class="submit" value="Download">
        </form>
</div>
</div>
</div>
</div>
