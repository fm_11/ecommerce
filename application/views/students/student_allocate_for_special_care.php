
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/student_allocate_for_special_care" method="post">
    <label>Year</label>
    <select class="smallInput" name="year" id="year" required="1">
        <option value="">-- Please Select --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>

    <label>Class</label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--Please Select--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Shift</label>
    <select name="shift_id" class="smallInput" required="1" id="shift_id">
        <option value="">--Please Select--</option>
        <?php foreach ($shifts as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Section</label>
    <select name="section_id" class="smallInput" required="1" id="section_id">
        <option value="">--Please Select--</option>
        <?php foreach ($section_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Group</label>
    <select name="group" required="1" class="smallInput">
        <option value="">--Please Select--</option>
        <?php foreach ($group_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>
    <div class="float-right">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
    </form>
