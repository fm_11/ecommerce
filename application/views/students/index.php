<script type="text/javascript">
function passwordResetConfirm() {
        var result = confirm("Are you sure to reset password?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function msgStatusUpdate(id,status){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>students/updateMsgStatusStudentStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }
</script>


<?php
$roll = $this->session->userdata('roll');
$class_shift_section_id = $this->session->userdata('class_shift_section_id');
$group = $this->session->userdata('group');
$gender = $this->session->userdata('gender');
$religion = $this->session->userdata('religion');
$blood_group_id = $this->session->userdata('blood_group_id');
$student_status = $this->session->userdata('student_status');
//echo $student_status; die;
?>

<form name="addForm" class="cmxform" id="commentForm"  method="post" action="<?php echo base_url(); ?>students/index">
	<div class="form-row">
		<div class="form-group col-md-3">
			<?php
			$placeholder = $this->lang->line('roll') . ' ' . $this->lang->line('or') . ' ' . $this->lang->line('student_code');
			?>
			<input type="text" style="padding: 11px !important;" autocomplete="off" name="roll" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($roll)) {
				echo $roll;
			} ?>" class="form-control" id="roll">
		</div>
		<div class="form-group col-md-3">
			<select name="class_shift_section_id" class="js-example-basic-single w-100" style="padding: 10px !important;" id="class_shift_section_id">
				<option value="">-- <?php echo $this->lang->line('class') . '-' . $this->lang->line('shift') . '-' . $this->lang->line('section'); ?> --</option>
				<?php foreach ($class_section_shift_marge_list as $row) { ?>
					<option value="<?php echo $row['class_id'] . '-' . $row['shift_id'] . '-' . $row['section_id']; ?>"<?php if (($row['class_id'] . '-' . $row['shift_id'] . '-' . $row['section_id']) == $class_shift_section_id) {
						echo 'selected';
					} ?>><?php echo $row['class_name'] . '-' . $row['shift_name'] . '-'. $row['section_name']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<select name="group" class="js-example-basic-single w-100" style="padding: 10px !important;">
				<option value="">-- <?php echo $this->lang->line('group'); ?> --</option>
				<?php foreach ($group_list as $row) { ?>
					<option value="<?php echo $row['id']; ?>"<?php if ($row['id'] == $group) {
						echo 'selected';
					} ?>><?php echo $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<select name="gender"  class="js-example-basic-single w-100" style="padding: 10px !important;">
				<option value="">-- <?php echo $this->lang->line('gender'); ?> --</option>
				<option value="M" <?php if (isset($gender)) {
					if ($gender == 'M') {
						echo 'selected';
					}
				} ?>>Male
				</option>
				<option value="F" <?php if (isset($gender)) {
					if ($gender == 'F') {
						echo 'selected';
					}
				} ?>>Female
				</option>
			</select>
		</div>
	</div>
	<div class="form-row">
		<div class="form-group col-md-3">
			<select name="religion" class="js-example-basic-single w-100" style="padding: 10px !important;">
				<option value="">-- Religion --</option>
				<?php foreach ($religion_list as $row) { ?>
					<option value="<?php echo $row['id']; ?>" <?php if (isset($religion)) {
						if ($row['id'] == $religion) {
							echo 'selected';
						}
					} ?>><?php echo $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="form-group col-md-3">
			<select name="blood_group_id"  class="js-example-basic-single w-100" style="padding: 10px !important;">
				<option value="">-- Blood Group --</option>
				<?php foreach ($blood_group_list as $row) { ?>
					<option value="<?php echo $row['id']; ?>" <?php if (isset($blood_group_id)) {
						if ($row['id'] == $blood_group_id) {
							echo 'selected';
						}
					} ?>><?php echo $row['name']; ?></option>

				<?php } ?>
			</select>
		</div>

		<div class="form-group col-md-3">
			<select name="student_status" class="js-example-basic-single w-100" style="padding: 10px !important;">
				<option value="">-- <?php echo $this->lang->line('status'); ?> --</option>
				<option value="1" <?php if (isset($student_status)) {
					if ($student_status == '1') {
						echo 'selected';
					}
				} ?>>Active
				</option>
				<option value="0" <?php if (isset($student_status) && $student_status != '') {
					if ($student_status == '0') {
						echo 'selected';
					}
				} ?>>Inactive
				</option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<button type="submit" style="padding: 13px 30px 13px 30px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
		</div>
	</div>
</form>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table table-striped">
    <thead>
    <tr>
        <th><?php echo $this->lang->line('sl'); ?></th>
        <th><?php echo $this->lang->line('student_code'); ?></th>
        <th><?php echo $this->lang->line('name'); ?></th>
        <th><?php echo $this->lang->line('class'); ?></th>
        <th><?php echo $this->lang->line('section'); ?></th>
        <th><?php echo $this->lang->line('group'); ?></th>
        <th><?php echo $this->lang->line('shift'); ?></th>
        <th><?php echo $this->lang->line('roll'); ?></th>
        <th><?php echo $this->lang->line('status'); ?></th>
        <th><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($student as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td >
                <?php echo $i; ?>
            </td>
            <td>
                <?php echo $row['student_code']; ?>
            </td>

            <td>
              <?php
                if ($this->session->userdata('site_lang') == 'bangla') {
                    echo $row['name_bangla'];
                } else {
                    echo $row['name'];
                }
              ?>
            </td>
            <td><?php echo $row['class_name']; ?></td>
            <td><?php echo $row['section_name']; ?></td>
            <td><?php echo $row['group_name']; ?></td>
            <td><?php echo $row['shift_name']; ?></td>
            <td><?php echo $row['roll_no']; ?></td>
            <td>

                <span id="status_sction_<?php echo $row['id']; ?>">
                <?php
                if ($row['status'] == 1) {
                    ?>
					<button onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)" style="padding: 2px 10px 2px 10px !important;" class="btn btn-outline-secondary btn-sm"><i class="ti-check-box"></i></button>
                <?php
                } else {
                    ?>
					<button onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)" style="padding: 2px 10px 2px 10px !important;" class="btn btn-outline-danger btn-sm"><i class="ti-na"></i></button>
                <?php
                }
                ?>
                </span>
            </td>

            <td>
                    <div class="dropdown">
                        <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-pencil-alt"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                            <a class="dropdown-item" href="<?php echo base_url(); ?>students/student_info_view/<?php echo $row['id']; ?>">View</a>
                            <a class="dropdown-item" href="tel:<?php echo $row['guardian_mobile']; ?>">Call to Guardian</a>
                            <a class="dropdown-item" target="_blank" href="<?php echo base_url() . MEDIA_FOLDER; ?>/QR_CODE/<?php echo $row['qr_code']; ?>">View QR Code</a>
                            <a class="dropdown-item" onclick="return passwordResetConfirm()" href="<?php echo base_url(); ?>students/password_reset/<?php echo $row['id']; ?>">Password Reset</a>
                            <a class="dropdown-item" href="<?php echo base_url(); ?>students/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                            <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>students/student_info_delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                        </div>
                    </div>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
