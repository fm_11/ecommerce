<script>
    function subject_by_class() {
        var class_shift_section_id = document.getElementById("class_shift_section_id").value;
		var class_id = class_shift_section_id.substring(0, 1);
        if (class_id != '') {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("subject_space").innerHTML = xmlhttp.responseText;
                    // alert(xmlhttp.responseText);
                }
            }
            xmlhttp.open("GET", "<?php echo base_url(); ?>students/ajax_subject_by_class?class_id=" + class_id, true);
            xmlhttp.send();
        } else {
            document.getElementById("subject_space").innerHTML = 'Please Select Class....';
        }
    }

    function check_optional_subject(id) {
        // if (document.getElementById("is_optional_" + id).checked == true) {
        document.getElementById("subject_id_" + id).checked = true
        // }
    }
</script>

<style>
	.required_label {
		color:red;
	}
</style>

<?php
if(isset($student_info_by_id)){
	$class_shift_section_id = $student_info_by_id[0]['class_id'] . '-' . $student_info_by_id[0]['shift_id'] . '-' . $student_info_by_id[0]['section_id'];
}
?>

      <form name="addForm"  class="cmxform" id="commentForm" action="<?php echo base_url(); ?>students/<?php if ($action == 'edit') { ?>edit<?php } else { ?>add<?php } ?>"
            method="post"
            enctype="multipart/form-data">

		  <div class="form-row">
			  <div class="form-group col-md-2">
				  <label><?php echo $this->lang->line('year'); ?>&nbsp;<span class="required_label">*</span></label>
				  <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['year']; ?>"
				  <?php } else { ?> value="<?php echo date('Y'); ?>" <?php } ?> type="text" name="year" class="form-control">

			  </div>

			  <div class="form-group col-md-2">
				  <label><?php echo $this->lang->line('year'); ?> (For ID)&nbsp;<span class="required_label">*</span></label>
				  <input  value="<?php echo date('Y'); ?>" type="text" name="student_id_year" class="form-control" required>
			  </div>

			  <div class="form-group col-md-2">
				  <label><?php echo $this->lang->line('roll'); ?>&nbsp;<span class="required_label">*</span></label>
				  <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['roll_no']; ?>"
				  <?php } ?> type="text" name="roll_no" class="form-control" required="1">
			  </div>

			  <div class="form-group col-md-3">
				  <label><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?>&nbsp;<span class="required_label">*</span></label><br>
				  <select class="js-example-basic-single w-100" name="class_shift_section_id" onchange="subject_by_class()" id="class_shift_section_id" required="1">
					  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					  <?php
					  $i = 0;
					  if (count($class_section_shift_marge_list)) {
						  foreach ($class_section_shift_marge_list as $list) {
							  $i++; ?>
							  <option
									  value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
									  <?php if (isset($class_shift_section_id)) {
										  if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
											  echo 'selected';
										  }
									  } ?>>
								  <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
							  </option>
							  <?php
						  }
					  }
					  ?>
				  </select>
			  </div>

			  <div class="form-group col-md-3">
				  <label><?php echo $this->lang->line('group'); ?>&nbsp;<span class="required_label">*</span></label><br>
				  <select name="group" class="js-example-basic-single w-100" required="required">
					  <option value="">-- Please Select --</option>
					  <?php foreach ($group_list as $row) { ?>
						  <option value="<?php echo $row['id']; ?>" <?php if (isset($student_info_by_id)) {
							  if ($row['id'] == $student_info_by_id[0]['group']) {
								  echo 'selected';
							  }
						  } ?>><?php echo $row['name']; ?></option>

					  <?php } ?>
				  </select>
			  </div>
		  </div>

        <div class="form-row">

			<div class="form-group col-md-3">
				<label>Category <span class="required_label">*</span></label><br>
				<select class="js-example-basic-single w-100" name="category_id" id="category_id" required="1">
					<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					<?php
					if (count($categories)) {
						foreach ($categories as $list) {
							?>
							<option
									value="<?php echo $list['id']; ?>"
									<?php if(isset($student_info_by_id)){ if ($student_info_by_id[0]['category_id'] == $list['id']) {
										echo 'selected';
									}} ?>><?php echo $list['name']; ?></option>
							<?php
						}
					}
					?>
				</select>
			</div>

		  <div class="form-group col-md-3">
			<label for="name"><?php echo $this->lang->line('name'); ?>&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['name']; ?>"
		  <?php } ?> class="form-control" id="name" name="name" required="1"/>
		  </div>

			<div class="form-group col-md-3">
				<label><?php echo $this->lang->line('gender'); ?>&nbsp;<span class="required_label">*</span></label>
				<select name="gender" class="js-example-basic-single w-100" required="required">
					<option value="">-- Please Select --</option>
					<option value="M" <?php if (isset($student_info_by_id)) {
						if ($student_info_by_id[0]['gender'] == 'M') {
							echo 'selected';
						}
					} ?>>Male
					</option>
					<option value="F" <?php if (isset($student_info_by_id)) {
						if ($student_info_by_id[0]['gender'] == 'F') {
							echo 'selected';
						}
					} ?>>Female
					</option>
				</select>
			</div>

			<div class="form-group col-md-3">
				<label><?php echo $this->lang->line('religion'); ?>&nbsp;<span class="required_label">*</span></label>
				<select class="js-example-basic-single w-100" name="txtReligion" required="1">
					<option value="">-- Please Select --</option>
					<?php
					$i = 0;
					if (count($religions)) {
						foreach ($religions as $list) {
							$i++; ?>
							<option value="<?php echo $list['id']; ?>" <?php if(isset($student_info_by_id)){ if ($student_info_by_id[0]['religion'] == $list['id']) {
								echo 'selected';
							} } ?>><?php echo $list['name']; ?></option>
							<?php
						}
					} ?>
				</select>
			</div>
        </div>

	  <div class="form-row">
		  <div class="form-group col-md-3">
			  <label><?php echo $this->lang->line('father_name'); ?>&nbsp;<span class="required_label">*</span></label>
			  <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['father_name']; ?>"
			  <?php } else { ?> value="" <?php } ?>  type="text" required name="father_name" class="form-control">
		  </div>

		  <div class="form-group col-md-3">
			  <label><?php echo $this->lang->line('mother_name'); ?>&nbsp;<span class="required_label">*</span></label>
			  <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['mother_name']; ?>"
			  <?php } else { ?> value="" <?php } ?> type="text" required name="mother_name" class="form-control">
		  </div>
		  <div class="form-group col-md-3">
			  <label><?php echo $this->lang->line('guardian_mobile'); ?>&nbsp;<span class="required_label">*</span></label>
			  <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['guardian_mobile']; ?>"
			  <?php } else { ?> value="" <?php } ?> type="text" name="guardian_mobile" class="form-control" required="1">
		  </div>
		  <div class="form-group col-md-3">
			  <label><?php echo $this->lang->line('date_of_birth'); ?></label>
			  <div class="input-group date">
				  <input type="text" autocomplete="off"  name="date_of_birth" <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['date_of_birth']; ?>"
				  <?php } ?>  class="form-control">
				  <span class="input-group-text input-group-append input-group-addon">
                              <i class="simple-icon-calendar"></i>
                          </span>
			  </div>
		  </div>
	  </div>

		  <div class="form-row">
			  <div class="form-group col-md-3">
				  <label><?php echo $this->lang->line('father_nid'); ?></label>
				  <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['father_nid']; ?>"
				  <?php } else { ?> value="" <?php } ?> type="text" name="father_nid" class="form-control">
			  </div>

			  <div class="form-group col-md-3">
				  <label><?php echo $this->lang->line('mother_nid'); ?></label>
				  <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['mother_nid']; ?>"
				  <?php } else { ?> value="" <?php } ?> type="text" name="mother_nid" class="form-control">
			  </div>

			  <div class="form-group col-md-3">
				  <label><?php echo $this->lang->line('reg_no'); ?></label>
				  <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['reg_no']; ?>"
				  <?php } else { ?> value="0" <?php } ?> type="text" name="reg_no" class="form-control">
			  </div>
			  <div class="form-group col-md-3">
				  <label><?php echo $this->lang->line('student_code'); ?></label>
				  <input type="text" autocomplete="off"  <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['student_code']; ?>"
				  <?php } ?> class="form-control" name="student_code" readonly/>
			  </div>
		  </div>

		  <div class="form-row">
			  <div class="form-group col-md-6">
				  <label><?php echo $this->lang->line('present_address'); ?></label>
				  <textarea id="wysiwyg" class="form-control" rows="4" cols="20"
							name="present_address"><?php if (isset($student_info_by_id)) {
						  echo $student_info_by_id[0]['present_address'];
					  } ?></textarea>
			  </div>

			  <div class="form-group col-md-6">
				  <label><?php echo $this->lang->line('permanent_address'); ?></label>
				  <textarea id="wysiwyg" class="form-control" rows="4" cols="20"
							name="permanent_address"><?php if (isset($student_info_by_id)) {
						  echo $student_info_by_id[0]['permanent_address'];
					  } ?></textarea>
			  </div>
		  </div>


        <div class="form-row">
			<div class="form-group col-md-3">
				<label><?php echo $this->lang->line('process_code'); ?></label>
				<input type="text" autocomplete="off"  class="form-control" value="<?php if (isset($student_info_by_id)) {
					echo $student_info_by_id[0]['process_code'];
				} ?>"
					   name="process_code"/>
			</div>
			<div class="form-group col-md-3">
				<label><?php echo $this->lang->line('blood_group'); ?></label>
				<select name="blood_group_id" class="js-example-basic-single w-100">
					<option value="">-- Please Select --</option>
					<?php foreach ($blood_group_list as $row) { ?>
						<option value="<?php echo $row['id']; ?>" <?php if (isset($student_info_by_id)) {
							if ($row['id'] == $student_info_by_id[0]['blood_group_id']) {
								echo 'selected';
							}
						} ?>><?php echo $row['name']; ?></option>

					<?php } ?>
				</select>
			</div>

			<div class="form-group col-md-3">
				<label><?php echo $this->lang->line('email'); ?></label>
				<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['email']; ?>"
				<?php } else { ?> value="N/A" <?php } ?> type="text" name="email" class="form-control">
			</div>
              <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('date_of_admission'); ?></label>
                   <div class="input-group date">
                            <input type="text" autocomplete="off"  name="date_of_admission" <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['date_of_admission']; ?>"
                            <?php } ?>  class="form-control">
                            <span class="input-group-text input-group-append input-group-addon">
                                <i class="simple-icon-calendar"></i>
                            </span>
                    </div>
              </div>
        </div>


        <div class="form-row">
              <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('guardian_mobile'); ?> (<?php echo $this->lang->line('optional'); ?>)</label>
               <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['second_guardian_mobile']; ?>"
               <?php } else { ?> value="N/A" <?php } ?> type="text" name="second_guardian_mobile" class="form-control">

              </div>

              <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('student'); ?> <?php echo $this->lang->line('mobile'); ?> (<?php echo $this->lang->line('optional'); ?>)</label>
               <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['student_mobile']; ?>"
               <?php } else { ?> value="N/A" <?php } ?> type="text" name="student_mobile" class="form-control">

              </div>

              <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('birth_certificate_no'); ?></label>
               <input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['birth_certificate_no']; ?>"
               <?php } else { ?> value="" <?php } ?> type="text" name="birth_certificate_no" class="form-control">
              </div>
			<div class="form-group col-md-3">
				<label>Admitted <?php echo $this->lang->line('class'); ?></label>
				<select name="admitted_class_id" class="js-example-basic-single w-100" id="admitted_class_id">
					<option value="">-- Please Select --</option>
					<?php foreach ($class_list as $row) { ?>
						<option value="<?php echo $row['id']; ?>" <?php if (isset($student_info_by_id)) {
							if ($row['id'] == $student_info_by_id[0]['admitted_class_id']) {
								echo 'selected';
							}
						} ?>><?php echo $row['name']; ?></option>

					<?php } ?>
				</select>
			</div>
        </div>

        <div class="form-row">
              <div class="form-group col-md-12">
                <label><?php echo $this->lang->line('photo'); ?></label>
                <?php
                if ($action == 'edit') {
                    ?>
                    <input value="<?php echo $student_info_by_id[0]['id']; ?>" type="hidden" name="id" class="form-control">
                    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $student_info_by_id[0]['photo']; ?>" height="130"
                         width="110"><br>
                    <input type="file" name="photo" class="form-control"> * Image Format -> JPEG , JPG and PNG.
                <?php
                } else { ?>
                    <input type="file" name="photo" class="form-control"> * Image Format -> JPEG , JPG and PNG.
                <?php } ?>
              </div>
        </div>




          <h3><?php echo $this->lang->line('subject_list'); ?></h3>
          <hr>
          <?php
          if ($action == 'edit') {
              ?>
              <table class="table table-striped">
                  <tr>
                      <td><?php echo $this->lang->line('sl'); ?></td>
                      <td><?php echo $this->lang->line('subject'); ?></td>
                      <td>Is <?php echo $this->lang->line('optional'); ?></td>
                  </tr>
                  <?php for ($l = 0; $l < count($subject_list_by_student); $l++) { ?>
                      <tr>
                          <td>
                              <?php echo $l + 1; ?>
                          </td>
                          <td>
                              <?php echo $subject_list_by_student[$l]['name'] . ' (' . $subject_list_by_student[$l]['code'] . ')'; ?>
                          </td>
                          <td>
                              <?php if ($subject_list_by_student[$l]['is_optional'] == '1') {
                  echo "YES";
              } ?>
                          </td>
                      </tr>
                  <?php
                 } ?>
              </table>

          <?php
          } else {
              ?>
              <span id="subject_space"></span>
          <?php
          } ?>
          <br>
          <div class="float-right">
             <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
             <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
          </div>
      </form>
