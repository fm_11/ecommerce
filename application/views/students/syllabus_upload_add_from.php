<h2>Please Input All Correct Information</h2>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/student_syllabus_upload" method="post" enctype="multipart/form-data">
    <label>Class</label>
    <select class="smallInput" name="txtClass" required="1">
        <option value="">-- Please Select --</option>
        <?php
        $i = 0;
        if (count($class)) {
            foreach ($class as $list) {
                $i++;
                ?>
                <option
                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
            <?php
            }
        }
        ?>
    </select>

    <label>Year</label>
    <select class="smallInput" name="txtYear" required="1">
        <option value="">-- Please Select --</option>
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
    </select>

    <label>Page Number</label>
    <input type="text" autocomplete="off"  class="smallInput wide" name="txtPageNum" required="1"/>

    <label>File</label>
    <input type="file" name="txtPhoto" required="1" class="smallInput"> * File Format -> JPEG , JPG and PNG

    <br>
    <br>
    <input type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form><br />
<div class="clear"></div><br />

