<h1>
    Class: <?php echo $ClassSubjectName[0]['ClassName']; ?>&nbsp;,
    Subject: <?php 
    if($subject_id != null){
      echo $ClassSubjectName[0]['SubjectName'] . ' [' . $ClassSubjectName[0]['SubjectCode'] . ']'; ?>&nbsp;,Credit: <?php echo $ClassSubjectName[0]['credit'];   
    }
    ?>
</h1>
<form action="<?php echo base_url('students/extra_marking_process'); ?>"
    method="post">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tr>
            <th>
                SL
            </th>
            <th>
                Name
            </th>
            <th>
                Roll
            </th>
            <th>
                Student Code
            </th>
         
            
            <?php
               foreach ($extra_head as $col_row):
             ?>
             <th>
                <?php echo $col_row['head_name'] . '<br>('. $col_row['mark'] .')'; ?><br>
                <select name="allowable_percentage_<?php echo $col_row['head_id']; ?>" class="smallInput" required="required">
                    <?php 
             	     $len = 1;
             	     while($len <= 100){
             	   ?>
                   <option <?php if($len == 100){ echo 'selected'; } ?> value="<?php echo $len; ?>"><?php echo $len; ?></option>
                    <?php 
                        $len++; 
                        } 
                    ?>
                </select>
            </th>
             <?php endforeach; ?>
            
        </tr>
        
       
        <?php for ($A = 0; $A < count($student_info); $A++) { ?>
            <input type="hidden" name="student_id_<?php echo $A; ?>" value="<?php echo $student_info[$A]['id']; ?>">
            <tr>
                <td><?php echo $A + 1; ?></td>
                <td><?php echo $student_info[$A]['name']; ?></td>
                <td><?php echo $student_info[$A]['roll_no']; ?></td>
                <td><?php echo $student_info[$A]['student_code']; ?></td>
             
             <?php
               $row_num = 0;
               foreach ($extra_head as $datacol_row):
             ?>
             <td>
                 <?php
                 $mark = 0;
                 if(!empty($marks)){
                     if(isset($marks[$student_info[$A]['id'].'_'.$datacol_row['head_id']])){
                         $mark = $marks[$student_info[$A]['id'].'_'.$datacol_row['head_id']]['mark'];
                     }
                 }
                 ?>
                 <input  size="10" value="<?php echo $mark; ?>" required="required"
                           name="mark_<?php echo $datacol_row['head_id']. '_' .$A; ?>" type="text">
                           
                <input  size="10" value="<?php echo $datacol_row['head_id']; ?>" required="required"
                           name="head_id_<?php echo $datacol_row['head_id'] . '_' . $A; ?>" type="hidden">
             </td>
             <?php endforeach; ?>
                           
                           
            </tr>
        <?php } ?>
    </table>
    <input type="hidden" name="loop_time" value="<?php echo $A; ?>">
    <input type="hidden" name="exam_id" value="<?php echo $exam_id; ?>">
    <input type="hidden" name="subject_id" value="<?php echo $subject_id; ?>">
    <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
    <input type="hidden" name="section_id" value="<?php echo $section_id; ?>">
    <input type="hidden" name="group_id" value="<?php echo $group_id; ?>">
    <input type="hidden" name="shift_id" value="<?php echo $shift_id; ?>">
    &nbsp;<button type="submit">Submit</button>
</form>