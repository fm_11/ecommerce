<script>
function on_off_percentage_of_grand_result(value){
  //alert(value);
  if(value == '1'){
    document.getElementById("percentage_of_grand_result").value = "";
    document.getElementById("percentage_of_grand_result").readOnly = false;
  }else{
    document.getElementById("percentage_of_grand_result").value = "";
    document.getElementById("percentage_of_grand_result").readOnly = true;
  }
}

function get_exam_list_for_combined(is_combined_result) {
    var year = document.getElementById("year").value;
    if(year == ''){
      alert("Please input a year first");
      document.getElementById("is_combined_result").value = "0";
    }

    if (year != '' && is_combined_result == '1') {
      document.getElementById("combined_percentage").value = "";
      document.getElementById("combined_percentage").readOnly = false;
    }else{
      document.getElementById("combined_percentage").value = "";
      document.getElementById("combined_percentage").readOnly = true;
    }

    if (year != '' && is_combined_result == '1') {
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("combined_exam_section").innerHTML = xmlhttp.responseText;
                // alert(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>exams/get_exam_list_for_combined?year=" + year, true);
        xmlhttp.send();
    } else {
        document.getElementById("combined_exam_section").innerHTML = '';
        //return false;
    }
}
</script>
        <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>exams/add" method="post">

         <div class="form-row">
           <div class="form-group col-md-4">
             <label>Exam Type</label>
             <select name="exam_type_id" class="js-example-basic-single w-100"  required="required">
                 <option value="">-- Select --</option>
                 <?php for ($C = 0; $C < count($exam_types); $C++) { ?>
                     <option value="<?php echo $exam_types[$C]['id']; ?>"><?php echo $exam_types[$C]['name']; ?></option>
                 <?php } ?>
             </select>
          </div>
          <div class="form-group col-md-4">
            <label>Name</label>
            <input type="text" autocomplete="off"  name="name" class="form-control" required="required" value="">
          </div>
          <div class="form-group col-md-4">
            <label>Year</label>
            <input type="text" autocomplete="off"  name="year"  id="year" class="form-control" required="required" value="">
         </div>
        </div>

          <div class="form-row">
               <div class="form-group col-md-4">
                 <label>Exam Order</label>
                 <input type="text" autocomplete="off"  name="exam_order" class="form-control" required="required" value="">
              </div>
              <div class="form-group col-md-4">
                <label>Is Applicable for final calcultion ?</label>
                <select onchange="on_off_percentage_of_grand_result(this.value)" name="is_applicable_for_final_calcultion" class="form-control" required="required">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
             </div>
             <div class="form-group col-md-4">
               <label>Percentage of Grand Result</label>
               <input type="text" autocomplete="off"  name="percentage_of_grand_result" id="percentage_of_grand_result" readonly class="form-control" value="">
            </div>
          </div>


          <div class="form-row">

              <div class="form-group col-md-4">
                <label>Is Annual Exam ?</label>
                <select name="is_annual_exam" class="form-control" required="required">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
             </div>

             <div class="form-group col-md-4">
               <label>Is Combined results with other exam ?</label>
               <select onchange="return get_exam_list_for_combined(this.value)" name="is_combined_result" id="is_combined_result" class="form-control" required="required">
                   <option value="0">No</option>
                   <option value="1">Yes</option>
               </select>
            </div>

            <div class="form-group col-md-4">
              <label>Combined Percentage</label>
              <input type="text" autocomplete="off"  name="combined_percentage" id="combined_percentage" readonly class="form-control" value="">
           </div>

          </div>

          <div class="form-row" id="combined_exam_section">

          </div>

          <div class="float-right">
              <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
             <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
          </div>
        </form>
