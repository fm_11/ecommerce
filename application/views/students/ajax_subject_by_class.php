<?php if ($type == 'table') { ?>
    <table class="table table-striped">
        <thead>
        <tr>
            <th width="100" scope="col" style="text-align: left">Subject</th>
            <th width="20" scope="col">Select</th>
            <th width="20" scope="col">Is Optional</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ($subject_list as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td style="text-align: left"><?php echo $row['name'].' ('. $row['code'] . ')'; ?></td>
                <td><input type="checkbox"
                           name="subject_id[]"
                           id="subject_id_<?php echo $i; ?>"
                           value="<?php echo $row['subject_id']; ?>" checked></td>
                <td>
				<input type="radio" id="is_optional_<?php echo $i; ?>" name="is_optional"
                           value="<?php echo $row['subject_id']; ?>"
                           onclick="return check_optional_subject('<?php echo $i; ?>')">
				</td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php } else { ?>
    <select name="subject_id" required="required" class="js-example-basic-single w-100" >
        <option value="">--Select--</option>
        <?php for ($A = 0; $A < count($subject_list); $A++) { ?>
            <option
                value="<?php echo $subject_list[$A]['subject_id']; ?>"><?php echo $subject_list[$A]['name'] . ' [' . $subject_list[$A]['code'] . ']'; ?></option>
        <?php } ?>
    </select>
<?php } ?>
