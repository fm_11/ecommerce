<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
        <li><a href="<?php echo base_url(); ?>admin_logins/history_info"><span>History</span></a></li>
        <li><a href="<?php echo base_url(); ?>admin_logins/about_info"><span>About</span></a></li> 
        <li><a href="<?php echo base_url(); ?>admin_logins/contact_info"><span>Contact</span></a></li>
        <li><a href="<?php echo base_url(); ?>admin_logins/home_page_news_info"><span>Home Page News</span></a></li>
        <li><a href="<?php echo base_url(); ?>admin_logins/home_page_breaking_news_info"><span>Welcome  Msg.</span></a></li>
		<li><a href="<?php echo base_url(); ?>admin_logins/rules_and_regulation_info"><span>Rules and Regulation</span></a></li>
        <li><a href="<?php echo base_url(); ?>admin_logins/president_message_info"><span>President Msg.</span></a></li>
       <li><a href="<?php echo base_url(); ?>admin_logins/head_message_info"><span>Head Teacher Msg.</span></a></li>
	   <li><a href="<?php echo base_url(); ?>admin_logins/founder_info"><span>Founder Info.</span></a></li>
</ul>

