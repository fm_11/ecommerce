<?php
$controller = $this->uri->segment(1);
?>
<ul class="nav">
  <li class="nav-item  <?php if ($controller == 'dashboard') { ?>active<?php } ?>">
    <a class="nav-link" href="<?php echo base_url(); ?>dashboard/index">
      <i class="ion ion ion-ios-archive menu-icon"></i>
		<span class="menu-title"><?php echo $this->lang->line('dashboard'); ?></span>
    </a>
  </li>












<li class="nav-item <?php if (in_array($controller, array('products'))) { ?>active<?php } ?>">
	<a class="nav-link" data-toggle="collapse" href="#ecommerce" aria-expanded="false" aria-controls="ecommerce">
		<i class="ion ion ion-ios-basket menu-icon"></i>
		<span class="menu-title">Products</span>
		<i class="menu-arrow"></i>
	</a>
	<div class="collapse <?php if (in_array($controller, array('products','Sub_category','Basic_info','Category','Delivery_info','Mail_configuration'))) { ?>show<?php } ?>" id="ecommerce">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>products/add#products">Add New</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>products/index#products">View Products</a></li>

  	</ul>
	</div>
</li>
<li class="nav-item <?php if (in_array($controller, array('purchase'))) { ?>active<?php } ?>">
	<a class="nav-link" data-toggle="collapse" href="#purchase" aria-expanded="false" aria-controls="ecommerce">
		<i class="ion ion ion-ios-basket menu-icon"></i>
		<span class="menu-title">Purchase</span>
		<i class="menu-arrow"></i>
	</a>
	<div class="collapse <?php if (in_array($controller, array('purchase'))) { ?>show<?php } ?>" id="purchase">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>purchase/add#purchase">Add New</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>purchase/index#purchase">View Purchase</a></li>

  	</ul>
	</div>
</li>

<li class="nav-item <?php if (in_array($controller, array('orders'))) { ?>active<?php } ?>">
	<a class="nav-link" data-toggle="collapse" href="#order" aria-expanded="false" aria-controls="ecommerce">
		<i class="ion ion ion-ios-basket menu-icon"></i>
		<span class="menu-title">Orders</span>
		<i class="menu-arrow"></i>
	</a>
	<div class="collapse <?php if (in_array($controller, array('orders'))) { ?>show<?php } ?>" id="order">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>orders/index#order">Orders Manage</a></li>

  	</ul>
	</div>
</li>


<li class="nav-item <?php if (in_array($controller, array('suppliers'))) { ?>active<?php } ?>">
	<a class="nav-link" data-toggle="collapse" href="#supplier" aria-expanded="false" aria-controls="ecommerce">
		<i class="ion ion ion-ios-basket menu-icon"></i>
		<span class="menu-title">Supplier</span>
		<i class="menu-arrow"></i>
	</a>
	<div class="collapse <?php if (in_array($controller, array('supplier'))) { ?>show<?php } ?>" id="supplier">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>suppliers/add#supplier">Add New</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>suppliers/index#supplier">View Supplier</a></li>

  	</ul>
	</div>
</li>



<li class="nav-item <?php if (in_array($controller, array('users','user_roles','user_role_wise_privileges','login'))) { ?>active<?php } ?>">
	<p class="sidebar-title"><?php echo $this->lang->line('configuration'); ?></p>
	<a class="nav-link" data-toggle="collapse" href="#user_manage" aria-expanded="false" aria-controls="user_manage">
		<i class="ion ion-md-person menu-icon"></i>
		<span class="menu-title"><?php echo $this->lang->line('user') . ' ' .$this->lang->line('management'); ?></span>
		<i class="menu-arrow"></i>
	</a>
	<div class="collapse <?php if (in_array($controller, array('users','user_roles','user_role_wise_privileges','login'))) { ?>show<?php } ?>" id="user_manage">
		<ul class="nav flex-column sub-menu">
			<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>users/index#user_manage"><?php echo $this->lang->line('user') . ' ' .$this->lang->line('information'); ?></a></li>
			<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>user_roles/index#user_manage"><?php echo $this->lang->line('user') . ' ' .$this->lang->line('role'); ?></a></li>
			<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>login/change_password#user_manage"><?php echo $this->lang->line('password_change'); ?></a></li>
		</ul>
	</div>
</li>

<li class="nav-item <?php if (in_array($controller, array('Basic_info','Sub_category','slides','Category','Delivery_info','Mail_configuration'))) { ?>active<?php } ?>">
	<a class="nav-link" data-toggle="collapse" href="#basic_config" aria-expanded="false" aria-controls="ui-basic">
		<i class="ion ion-md-settings menu-icon"></i>
		<span class="menu-title">Basic <?php echo $this->lang->line('settings'); ?></span>
		<i class="menu-arrow"></i>
	</a>
	<div class="collapse <?php if (in_array($controller, array('Basic_info','Sub_category','slides','Category','Delivery_info','Mail_configuration'))) { ?>show<?php } ?>" id="basic_config">
		<ul class="nav flex-column sub-menu">

      <!-- <p class="sidebar-title">Categories</p> -->
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>slides/index#basic_config">Slide</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Category/index#basic_config">Category</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Sub_category/index#basic_config">Sub Category</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Basic_info/index#basic_config">Basic Info</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Mail_configuration/index#basic_config">Email Settings</a></li>
      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Delivery_info/index#basic_config">Delivery Details</a></li>
      <!-- <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Customer_code/index#basic_config">Customer Code</a></li> -->
		</ul>
	</div>
</li>



<li class="nav-item <?php if (in_array($controller, array('success_students','notice_lists','home_page_news','founder_infos','president_messages','welcome_messages','headteacher_messages','slides','managing_committees','headmaster_lists','donor_lists'))) { ?>active<?php } ?>">
<p class="sidebar-title"><?php echo $this->lang->line('website') . ' ' . $this->lang->line('manage'); ?></p>
<a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
  <i class="ion ion-md-tv menu-icon"></i>
	<span class="menu-title"><?php echo $this->lang->line('website'); ?></span>
  <i class="menu-arrow"></i>
</a>
<div class="collapse <?php if (in_array($controller, array('success_students','notice_lists','president_messages','founder_infos','home_page_news','welcome_messages','headteacher_messages','slides','managing_committees','headmaster_lists','donor_lists'))) { ?>show<?php } ?>" id="auth">
  <ul class="nav flex-column sub-menu">
	<!-- <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>donor_lists/index#auth">Donor Information</a></li> -->

  </ul>
</div>
</li>

<!-- <li>
	<a href="https://www.facebook.com/school360bd/" target="_blank">
	<div class="card bg-facebook d-flex align-items-center">
		<div class="card-body">
			<div class="d-flex flex-row align-items-center">
				<i class="ion ion-logo-facebook text-white icon-md"></i>
				<div class="ml-3">
					<h6 class="text-white">School360</h6>
					<p class="mt-2 text-white card-text">শিক্ষাপ্রতিষ্ঠানের জন্য সব </p>
				</div>
			</div>
		</div>
	</div>
	</a>
</li> -->
<li class="nav-item">
	<a href="<?php echo base_url(); ?>" target="_blank">
	<div style="background-color: #282F3A !important;" class="d-flex align-items-center">
		<div class="card-body">
			<button class="btn btn-outline-light btn-icon-text">
				<i class="ion ti-world btn-icon-prepend mdi-36px"></i>
				<span class="d-inline-block text-left">
					<!-- <small class="font-weight-light d-block">Website Demo</small> -->
				Preview
				</span>
			</button>
		</div>
	</div>
	</a>
</li>

</ul>
