<form name="addForm" class="cmxform" id="commentForm"   action="" method="post" enctype="multipart/form-data">

	<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $student_info_by_id[0]['photo']; ?>" height="200"
		 width="200"><br>
	<label>Name</label>
	<input type="text" autocomplete="off"  <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['name']; ?>"
	<?php } ?> class="smallInput wide" readonly name="name" required="1"/>

	<label>Student ID</label>
	<input type="text" autocomplete="off"  <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['student_code']; ?>"
	<?php } ?> class="smallInput wide" readonly name="name" required="1"/>


	<label>Process Code</label>
	<input
		type="text" <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['process_code']; ?>"
	<?php } ?> class="smallInput wide" readonly name="process_code" required="1"/>

	<label>Class</label>
	<select name="class_id" class="smallInput" required="required" onchange="subject_by_class()" id="class_id"
			disabled="disabled">
		<option value="">--Select--</option>
		<?php foreach ($class_list as $row) { ?>
			<option <?php if ($student_info_by_id[0]['class_id'] == $row['id']) { ?> selected="selected" <?php } ?>
				value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>

		<?php } ?>
	</select><br>
	<label>Subject List</label>
	<table width="40%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
		<tr>
			<td>Sl</td>
			<td>Subject</td>
			<td>Is Optional</td>
		</tr>
		<?php for ($l = 0; $l < count($subject_list_by_student); $l++) { ?>
			<tr>
				<td>
					<?php echo $l + 1; ?>
				</td>
				<td>
					<?php echo $subject_list_by_student[$l]['name'] . ' (' . $subject_list_by_student[$l]['code'] . ')'; ?>
				</td>
				<td>
					<?php if ($subject_list_by_student[$l]['is_optional'] == '1') {
						echo "YES";
					} ?>
				</td>
			</tr>
		<?php } ?>
	</table>
	<br>

	<label>Section:</label>
	<select name="section_id" class="smallInput" required="required" disabled="disabled">
		<option value="">--Select--</option>
		<?php foreach ($section_list as $row) { ?>
			<option <?php if ($student_info_by_id[0]['section_id'] == $row['id']) { ?> selected="selected" <?php } ?>
				value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
		<?php } ?>
	</select>
	<label>Group:</label><br>
	<select name="group" class="smallInput" required="required" disabled="disabled">
		<option value="">--Select--</option>
		<?php foreach ($group_list as $row) { ?>
			<option <?php if ($student_info_by_id[0]['group'] == $row['id']) { ?> selected="selected" <?php } ?>
				value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
		<?php } ?>
	</select><br>

	<label>Roll No:</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['roll_no']; ?>"
	<?php } ?> type="text" name="roll_no" class="smallInput wide" required="1" readonly>
	<label>Reg No:</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['reg_no']; ?>"
	<?php } ?> type="text" name="reg_no" class="smallInput wide" required="1" readonly>


	<label>Date of Birth</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['date_of_birth']; ?>"
	<?php } ?> type="text" name="date_of_birth" class="smallInput wide" required="1" readonly>
	<label>Date of Admission</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['date_of_admission']; ?>"
	<?php } ?> type="text" name="date_of_admission" class="smallInput wide" required="1" readonly>
	<label>Gender:</label>
	<select name="gender" class="smallInput" required="required" disabled="disabled">
		<option value="">--Select--</option>
		<option <?php if ($student_info_by_id[0]['gender'] == "M") { ?> selected="selected" <?php } ?>  value="M">Male
		</option>
		<option <?php if ($student_info_by_id[0]['gender'] == "F") { ?> selected="selected" <?php } ?>  value="F">
			Female
		</option>
	</select><br>

	<label>Religion</label>
	<select name="txtReligion" class="smallInput" required disabled>
		<option value="">-- Please Select --</option>
		<option value="I" <?php if ($student_info_by_id[0]['religion'] == "I") { ?> selected="selected" <?php } ?> >
			Islam
		</option>
		<option value="H" <?php if ($student_info_by_id[0]['religion'] == "H") { ?> selected="selected" <?php } ?> >
			Hindu
		</option>
	</select>

	<label>Blood Group:</label>
	<select name="blood_group_id" class="smallInput" required="required" disabled="disabled">
		<option value="">--Select--</option>
		<?php foreach ($blood_group_list as $row) { ?>
			<option <?php if ($student_info_by_id[0]['blood_group_id'] == $row['id']) { ?> selected="selected" <?php } ?>
				value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>

		<?php } ?>
	</select>

	<label>Father's Name</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['father_name']; ?>"
	<?php } ?> type="text" name="father_name" class="smallInput wide" readonly>
	<label>Father's National ID</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['father_nid']; ?>"
	<?php } ?> type="text" name="father_nid" class="smallInput wide" readonly>

	<label>Mother's Name</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['mother_name']; ?>"
	<?php } ?> type="text" name="mother_name" class="smallInput wide" readonly>
	<label>Mother's National ID</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['mother_nid']; ?>"
	<?php } ?> type="text" name="mother_nid" class="smallInput wide" readonly>

	<label>Present Address</label>
	<textarea id="wysiwyg" class="smallInput wide" readonly rows="7" cols="30"
			  name="present_address"><?php if (isset($student_info_by_id)) {
			echo $student_info_by_id[0]['present_address'];
		} ?></textarea>

	<label>Permanent Address</label>
	<textarea id="wysiwyg" readonly class="smallInput wide" rows="7" cols="30"
			  name="permanent_address"><?php if (isset($student_info_by_id)) {
			echo $student_info_by_id[0]['permanent_address'];
		} ?></textarea>

	<label>Guardian Mobile</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['guardian_mobile']; ?>"
	<?php } ?> type="text" name="guardian_mobile" readonly class="smallInput wide" required="1">

	<label>Email</label>
	<input <?php if (isset($student_info_by_id)) { ?> value="<?php echo $student_info_by_id[0]['email']; ?>"
	<?php } ?> type="text" name="email" class="smallInput wide" readonly>


	<label>Is Allowed for Timekeeping</label>
	<input type="radio" value="1" checked
		   name="allowed_for_timekeeping" <?php if (isset($student_info_by_id)) {
		if ($student_info_by_id[0]['allowed_for_timekeeping'] == '1') {
			echo 'checked';
		}
	} ?> required/>Yes
	<input type="radio" value="0"
		   name="allowed_for_timekeeping" <?php if (isset($student_info_by_id)) {
		if ($student_info_by_id[0]['allowed_for_timekeeping'] == '0') {
			echo 'checked';
		}
	} ?> />No

	<span
		id="syn_area" <?php if (isset($student_info_by_id)) {
		if ($student_info_by_id[0]['allowed_for_timekeeping'] == '0') { ?> style="display: none;" <?php }
	} ?>>
        <label>Synchronize Date </label>
        <input type="text" autocomplete="off"  name="syn_date" value="<?php if (isset($student_info_by_id)) {
			echo $student_info_by_id[0]['syn_date']; } ?>" id="syn_date" class="smallInput wide">
         </span>

</form>
