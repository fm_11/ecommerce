<?php
$controller = $this->uri->segment(1);
?>
<div class="container">
	<ul class="nav page-navigation">
		<li class="nav-item <?php if ($controller == 'dashboard') { ?>active<?php } ?>">
			<a class="nav-link" title="<?php echo $this->lang->line('dashboard'); ?>" href="<?php echo base_url(); ?>dashboard/index">
				<i class="ion ion ion-ios-archive menu-icon"></i>
				<span class="menu-title"><?php echo $this->lang->line('dashboard'); ?></span>
			</a>
		</li>

		<li class="nav-item mega-menu <?php if (in_array($controller, array('Basic_info','Sub_category','Category','Delivery_info','Mail_configuration'))) { ?>active<?php } ?>">
			<a href="#" class="nav-link">
				<i class="ion ion-md-settings menu-icon"></i>
				<span class="menu-title"><?php echo $this->lang->line('configuration'); ?></span>
				<i class="menu-arrow"></i>
			</a>

			<div class="submenu">
        <div class="col-group-wrapper row">
          <div class="col-group col-md-3">
            <p class="category-heading"><?php echo $this->lang->line('user') . ' ' .$this->lang->line('management'); ?></p>
            <ul class="submenu-item">
						<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>users/index"><?php echo $this->lang->line('user') . ' ' .$this->lang->line('information'); ?></a></li>
					    <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>user_roles/index"><?php echo $this->lang->line('user') . ' ' .$this->lang->line('role'); ?></a></li>
					    <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>login/change_password"><?php echo $this->lang->line('password_change'); ?></a></li>
            </ul>
          </div>
          <div class="col-group col-md-3">
            <p class="category-heading"><?php echo "Basic ".$this->lang->line('configuration'); ?></p>
            <ul class="submenu-item">
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Category/index">Category</a></li>
				      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Sub_category/index">Sub Category</a></li>
				      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Basic_info/index">Basic Info</a></li>
				      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Mail_configuration/index">Email Settings</a></li>
				      <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Delivery_info/index">Delivery Details</a></li>
				      <!-- <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>Customer_code/index">Customer Code</a></li> -->
						</ul>
          </div>
        </div>
      </div>
		</li>





		<li class="nav-item <?php if (in_array($controller, array('income_categories','exam_sessions','exam_routines','period_time_configurations','exam_routine_reports','shifts','chart_of_accounts','student_timekeepings','holidays','asset_categories','liabilities_categories','expense_categories','income_deposits','expense_details','fund_transfers'))) { ?>active<?php } ?> mega-menu">
			<a href="#" class="nav-link">
				<i class="ion ion-md-grid menu-icon"></i>
				<span class="menu-title"><?php echo $this->lang->line('products'); ?></span>
				<i class="menu-arrow"></i></a>
			<div class="submenu">
				<div class="col-group-wrapper row">

					<div class="col-group col-md-3">
						<p class="category-heading"><?php echo $this->lang->line('products'); ?></p>
						<ul class="submenu-item">
						<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>products/add">Add New</a></li>
					  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>products/index">View Products</a></li>


					</div>
				</div>
			</div>
		</li>


		<li class="nav-item mega-menu">
			<a href="#" class="nav-link">
				<i class="ion ion-logo-codepen menu-icon"></i>
				<span class="menu-title"><?php echo $this->lang->line('website'); ?></span>
				<i class="menu-arrow"></i></a>
			<!-- <div class="submenu">
				<div class="col-group-wrapper row">
					<div class="col-group col-md-3">
						<p class="category-heading"><?php echo $this->lang->line('website') . ' ' . $this->lang->line('information'); ?></p>
						<ul class="submenu-item">
							<li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>dashboard/notice_list"><?php echo $this->lang->line('notice'); ?></a></li>

					  </ul>
					</div>

				</div>
			</div> -->
		</li>
	</ul>
</div>
