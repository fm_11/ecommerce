<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $title; ?></title>
  <!-- base:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/font-awesome/css/font-awesome.min.css"/>
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/select2/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/owl-carousel-2/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/owl-carousel-2/owl.theme.default.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/morris.js/morris.css">
  <!-- End plugin css for this page -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/flag-icon-css/css/flag-icon.min.css"/>

  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/ti-icons/css/themify-icons.css">
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/css/vertical-layout-light/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>core_media/admin_v3/images/favicon.png" />

  <style>
    input[readonly] {
    background-color: #e3e5e8;
    }
	.required_label {
		color:red;
	}
  </style>
</head>
<?php
 $session_user = $this->session->userdata('user_info');
?>
<body class="sidebar-fixed sidebar-<?php echo $this->session->userdata('site_menu_color'); ?>">
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-primary">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
        <a class="navbar-brand brand-logo" href="<?php echo base_url(); ?>dashboard/index">

          <img src="<?php echo base_url(); ?>ecommerce_media/website/images/logo-3.png" alt="logo"/>
        </a>
        <a class="navbar-brand brand-logo-mini" href="<?php echo base_url(); ?>dashboard/index">
          <img src="<?php echo base_url(); ?>ecommerce_media/website/images/logo-3.png" alt="logo"/>
        </a>
      </div>
      <div class="navbar-menu-wrapper">
        <div id="top_bar_background" style="background-color: <?php echo $session_user[0]->top_bar_background_color; ?> !important;" class="navbar-menu-wrapper-inner d-flex align-items-center justify-content-end">
          <button id="top_menu_show_hide_button" style="background-color: <?php echo $session_user[0]->top_menu_show_hide_button_color; ?> !important;" class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="icon ion-md-menu"></span>
          </button>
          <!-- <ul class="navbar-nav mr-lg-2">
            <li class="nav-item nav-search d-none d-lg-block">
              <form class="form-inline" method="post" action="<?php echo base_url(); ?>students/index">
                <div class="input-group" style="border:1px #FFFFFF solid;" >
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="search">
                    </span>
                  </div>
                  <input type="text" autocomplete="off"  class="form-control" name="roll" placeholder="<?php echo $this->lang->line('roll'); ?> <?php echo $this->lang->line('or'); ?> <?php echo $this->lang->line('student_code'); ?>" aria-label="search" aria-describedby="search">
                </div>
              </form>
            </li>
          </ul> -->
          <ul class="navbar-nav navbar-nav-right">
            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-center" id="notificationDropdown" href="#" data-toggle="dropdown">
                <i class="icon ion-md-notifications-outline mx-0"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-success">
                      <i class="icon ion-md-information-circle-outline mx-0"></i>
                    </div>
                  </div>
                </a>


              </div>
            </li> -->
            <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                     <button style="padding: .5rem 0.5rem;border: 1px #ffffff solid;color: #ffffff;" class="btn btn-outline-primary btn-sm" type="button">
                            <?php if ($this->session->userdata('site_lang') == 'arabic') {  ?>
                              <i class="flag-icon flag-icon-sa" title="sa" id="sa"></i> العربية
                            <?php } elseif ($this->session->userdata('site_lang') == 'bangla') { ?>
                              বাংলা <i class="flag-icon flag-icon-bd" title="bd" id="bd"></i>
                           <?php } else { ?>
                              English <i class="flag-icon flag-icon-us" title="us" id="us"></i>
                          <?php } ?>
                    </button>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                    <a class="dropdown-item" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/bangla">
                      <i class="flag-icon flag-icon-bd" title="bd" id="bd"></i> বাংলা
                    </a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english">
                      <i class="flag-icon flag-icon-us" title="us" id="us"></i> English
                    </a>
                    <!-- <a class="dropdown-item" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic">
                      <i class="flag-icon flag-icon-sa" title="sa" id="sa"></i> العربية
                    </a> -->
                  </div>
            </li>
            <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                     <button style="padding: .5rem 0.5rem;border: 1px #ffffff solid;color: #ffffff;" class="btn btn-outline-primary btn-sm" type="button">
                       <?php if ($this->session->userdata('site_menu') == 'left' && $this->session->userdata('site_menu_color') == 'dark') {  ?>
                         Left Dark <i class="ti-arrow-left" style="color: #ffffff;"></i>
                       <?php } elseif ($this->session->userdata('site_menu') == 'left' && $this->session->userdata('site_menu_color') == 'light') {
                   ?>
                         Left Light <i class="ti-arrow-left" style="color: #ffffff;"></i>
                         <?php
               } else { ?>
                         Top <i class="ti-arrow-up"></i>
                     <?php } ?>
                    </button>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                    <a class="dropdown-item" href="<?php echo base_url(); ?>MenuSwitcher/switchMenu/left/dark">
                      <i class="ti-arrow-left"></i> Left Dark
                    </a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>MenuSwitcher/switchMenu/left/light">
                      <i class="ti-arrow-left"></i> Left Light
                    </a>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>MenuSwitcher/switchMenu/top">
                      <i class="ti-arrow-up"></i> Top
                    </a>
                  </div>
            </li>
            <li class="nav-item nav-profile dropdown">
              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                <span class="nav-profile-name"> <?php echo $session_user[0]->name; ?></span>
                <?php if($session_user[0]->photo_location == ''){ ?>
                <img src="<?php echo base_url(); ?>core_media/admin_v3/images/faces/user.png" alt="profile"/>
              <?php }else{ ?>
                <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $session_user[0]->photo_location; ?>" alt="profile"/>
              <?php } ?>

              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                <a href="<?php echo base_url(); ?>login/change_password/<?php echo $session_user[0]->id; ?>" class="dropdown-item">
                  <i class="icon ion-md-settings text-primary"></i>
                  <?php echo $this->lang->line('password_change'); ?>
                </a>
                <a href="<?php echo base_url(); ?>login/logout" class="dropdown-item">
                  <i class="ion ion-md-log-out text-primary"></i>
                  <?php echo $this->lang->line('logout'); ?>
                </a>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="icon ion-md-menu"></span>
          </button>
        </div>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_settings-panel.html -->
      <!-- <div class="theme-setting-wrapper">
        <div id="settings-trigger"><i class="icon ion-md-settings"></i></div>
        <div id="theme-settings" class="settings-panel">
          <i class="settings-close ion ion-md-close-circle-outline"></i>
          <p class="settings-heading">SIDEBAR SKINS</p>
          <div class="sidebar-bg-options" id="sidebar-light-theme">
            <div class="img-ss rounded-circle bg-light border mr-3"></div>Light
          </div>
          <div class="sidebar-bg-options selected" id="sidebar-dark-theme">
            <div class="img-ss rounded-circle bg-dark border mr-3"></div>Dark
          </div>
          <p class="settings-heading mt-2">HEADER SKINS</p>
          <div class="color-tiles mx-0 px-4">
            <div class="tiles success"></div>
            <div class="tiles warning"></div>
            <div class="tiles danger"></div>
            <div class="tiles primary"></div>
            <div class="tiles info"></div>
            <div class="tiles dark"></div>
            <div class="tiles default"></div>
          </div>
        </div>
      </div> -->
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" style="background-color: <?php echo $session_user[0]->left_menu_background_color; ?> !important;" id="sidebar">
       <?php echo $main_menu; ?>
    </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">


			  <!-- <div class="alert alert-danger col-sm-12" role="alert">
				  <marquee>
					  <b>
						  অনলাইন পেমেন্ট সহ সফটওয়্যার এ কিছু ফীচার এর আপডেট দেওয়া হয়েছে। সফটওয়্যার এ কেউ কোনো সমস্যা পেলে কারিগরি টীম কে জানানোর জন্য অনুরুধ করা হচ্ছে। School360 পরিবারের সাথে থাকার জন্য আপনাকে ধন্যবাদ
					  </b>
				  </marquee>
			  </div> -->


            <div class="col-sm-6">
                <h4 class="mb-1 font-weight-bold text-dark"><?php echo $title; ?></h4>
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb breadcrumb-custom">
						<li class="breadcrumb-item"><a href=""><?php echo strtoupper(str_replace("_", " " , $this->uri->segment(1))); ?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><span><?php echo strtoupper(str_replace("_", " " , $this->uri->segment(2))); ?></span></li>
					</ol>
				</nav>
            </div>
            <?php if ($this->uri->segment(1) == 'dashboard') { ?>
            <!-- <div class="col-sm-6">
              <div class="d-flex align-items-center justify-content-md-end">
                <a href="<?php echo base_url(); ?>students/index" class="btn btn-outline-secondary"><?php echo $this->lang->line('student_list'); ?></a>
                <a href="<?php echo base_url(); ?>fee_collections/add" class="btn btn-secondary ml-2">
                <?php echo $this->lang->line('fees') . ' ' . $this->lang->line('collect'); ?>
                </a>
              </div>
            </div> -->
            <?php } else {
                   if (isset($is_show_button) && $is_show_button != "") {
                       if ($is_show_button == "add") {
                           $class = 'class="btn btn-outline-primary"';
                           $btn_text = $this->lang->line('add_button');
                       } else {
                           $class = 'class="btn btn-outline-light"';
                           $btn_text = $this->lang->line('back_button');
                       } ?>
                                <div class="col-sm-6">
                                  <div class="d-flex align-items-center justify-content-md-end">
                                     <a <?php echo $class; ?> href="<?php echo base_url().$this->uri->segment(1).'/'.$is_show_button; ?>">
                                     <?php echo $btn_text; ?>
                                     </a>
                                 </div>
                               </div>
                               <?php
                   }
               } ?>
          </div>


          <?php if ($this->uri->segment(1) != 'dashboard') { ?>
          <div class="row">
                <div class="col-12 grid-margin">
                  <div class="card">
                    <div class="card-body">
              <?php } ?>
                      <?php
                            $message = $this->session->userdata('message');
                            if ($message != '') {
                                ?>
                                <div class="alert alert-success" role="alert">
									<i class="ion ion-md-alert"></i>
                                      <?php
                                      echo $message;
                                $this->session->unset_userdata('message'); ?>
                                </div>
                            <?php
                            }
                            ?>

                            <?php
                            $exception = $this->session->userdata('exception');
                            if ($exception != '') {
                                ?>
                                 <div class="alert alert-danger" role="alert">
									 <i class="ion ion-md-alert"></i>
									 <?php
                                          echo $exception;
                                          $this->session->unset_userdata('exception');
                                      ?>
                                  </div>
                            <?php
                            }
                            ?>
                      <?php
                        echo $maincontent;
                       if ($this->uri->segment(1) != 'dashboard') { ?>
                    </div>
                  </div>
                </div>
             </div>
           <?php } ?>
          </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer mr-0 ml-0">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">
              Copyright © <a href="https://spatei.com/" target="_blank">
                Spate Initiative Limited
              </a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"><?php echo $this->lang->line('spatei_title'); ?> <i class="ion ion-md-heart-empty"></i></span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

    <!-- base:js -->
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/jq.tablesort.js"></script>
    <!-- inject:js -->
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/off-canvas.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/hoverable-collapse.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/template.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/settings.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/todolist.js"></script>
    <!-- endinject -->
    <!-- plugin js for this page -->
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/chart.js/Chart.min.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/owl-carousel-2/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/jvectormap/jquery-jvectormap.min.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- End plugin js for this page -->
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Custom js for this page-->

    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/morris.js/morris.min.js"></script>

    <script src="<?php echo base_url(); ?>core_media/js/fees_collection.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/dashboard.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/chart.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/morris.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/todolist.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/scripts.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <!-- End custom js for this page-->

   <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/tinymce/tinymce.min.js"></script>


    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/formpickers.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/tablesorter.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/typeahead.js/typeahead.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/select2/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>core_media/admin_v3/js/typeahead.js"></script>
   <script src="<?php echo base_url(); ?>core_media/admin_v3/js/select2.js"></script>
   <script src="<?php echo base_url(); ?>core_media/admin_v3/js/form-validation.js"></script>
   <script src="<?php echo base_url(); ?>core_media/admin_v3/js/bt-maxLength.js"></script>
   <script src="<?php echo base_url(); ?>core_media/admin_v3/js/calendar.js"></script>
  <script src="<?php echo base_url(); ?>core_media/admin_v3/js/editorDemo.js"></script>
  <script src="<?php echo base_url(); ?>core_media/admin_v3/js/modal-demo.js"></script>
</body>

</html>
