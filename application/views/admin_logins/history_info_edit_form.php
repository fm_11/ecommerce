<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
</script>
<form name="updateForm" action="<?php echo base_url(); ?>admin_logins/update_history_info/" method="post">
    <label>Title</label>
    <input type="text" autocomplete="off"  name="txtTitle"  class="smallInput wide" size="100%" value="<?php echo $history_info->title; ?>">

    <label>Description</label>
    <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="txtDescription"><?php echo $history_info->description; ?></textarea>

    <br>
    <br>
    <br>
    <input type="hidden" name="id" size="100%" value="<?php echo $history_info->id; ?>">

    <input type="submit" class="submit" value="Update">
    <input type="reset" class="submit" value="Reset">
</form>