<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ]
    });
</script>
<form name="updateForm" action="<?php echo base_url(); ?>admin_logins/update_president_message_info/" method="post" enctype="multipart/form-data">
    <table>
        <tr>
            <td valign="top" scope="col">
                <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/headteacher/<?php echo $message->location; ?>"
                     style="border-radius: 10px;" width="250px" height="240px" class="img"/>
            </td>
        </tr>
        <tr>
            <td valign="top"scope="col">
                <table ellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
                    <tbody>
                    <tr>
                        <label>New Picture</label>
                        <input type="file" name="txtPhoto" class="smallInput">
                    </tr>

                    <tr>
                        <label>Title</label>
                        <input type="text" autocomplete="off"  name="txtTitle" class="smallInput wide" value="<?php echo $message->title; ?>">
                    </tr>

                    <tr>
                        <label>Message</label>
                        <textarea name="txtMessage"><?php echo $message->long_message; ?></textarea>
                    </tr>

                    <tr>
                        <th>
                            <input type="hidden" name="id" value="<?php echo $message->id; ?>" class="smallInput">
                            <input type="submit" class="submit" value="Update">
                        </th>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</form>
