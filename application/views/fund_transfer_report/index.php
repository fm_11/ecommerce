<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>fund_transfer_report/index" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('from_date'); ?></label>
      <div class="input-group date">
              <input type="text" autocomplete="off"  name="from_date" required value="<?php if (isset($from_date)) {
    echo $from_date;
} ?>" class="form-control">
              <span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
      </div>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('to_date'); ?></label>
      <div class="input-group date">
              <input type="text" autocomplete="off"  name="to_date" required value="<?php if (isset($to_date)) {
    echo $to_date;
} ?>" class="form-control">
              <span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
      </div>
    </div>
  </div>

  <div class="btn-group float-right">
    <input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
    <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
    <input type="submit" class="btn btn-primary" value="View Report">
  </div>
</form>

<div id="printableArea">
    <?php
    if (isset($idata)) {
        echo $report;
    }
    ?>
</div>
