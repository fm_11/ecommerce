<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th scope="col"><?php echo $this->lang->line('sl') ?></th>
			<th scope="col">MSISDN</th>
			<th scope="col">STATUS</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = (int)$this->uri->segment(3);
		foreach ($all_data as $row):
			$i++;
			?>

			<tr>
				<td>
					<?php echo $i; ?>
				</td>
				<td><?php echo $row['msisdn']; ?></td>
				<td><?php echo $row['status']; ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
