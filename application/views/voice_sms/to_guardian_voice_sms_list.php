<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th scope="col"><?php echo $this->lang->line('sl') ?></th>
			<th scope="col">Date</th>
			<th scope="col">Audio</th>
			<th scope="col">Status</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = (int)$this->uri->segment(3);
		foreach ($requests as $row):
			$i++;
			?>

			<tr>
				<td>
					<?php echo $i; ?>
				</td>
				<td><?php echo $row['date_time']; ?></td>
			    <td><?php echo $row['title']; ?></td>
				<td>
					<a target="_blank" href="<?php echo base_url(); ?>voice_sms/details_report/<?php echo $row['id']; ?>" class="btn btn-primary btn-xs mb-1">Pending</a>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>
