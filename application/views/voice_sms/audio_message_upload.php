
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>voice_sms/audio_message_upload" method="post" enctype="multipart/form-data">

	<div class="form-row">
		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('title'); ?></label>
			<input type="text" autocomplete="off"  class="form-control" name="title" required="1"/>
		</div>
		<div class="form-group col-md-6">
			<label>Select Voice <?php echo $this->lang->line('file'); ?></label>
			<input type="file" name="txtFile" required="1" class="form-control"> * File Format -> mp3
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('upload'); ?>">
	</div>
</form>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th scope="col"><?php echo $this->lang->line('sl') ?></th>
			<th scope="col">Title</th>
			<th scope="col"><?php echo $this->lang->line('actions')?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = (int)$this->uri->segment(3);
		foreach ($voices as $row):
			$i++;
			?>
			<tr>

			<tr>
				<td>
					<?php echo $i; ?>
				</td>
				<td><?php echo $row['title']; ?></td>
				<td>
					<a href="<?php echo base_url() . MEDIA_FOLDER; ?>/audio/<?php echo $row['location']; ?>" class="btn btn-primary btn-xs mb-1" title="Edit">Download</a>
					<a  onclick="return deleteConfirm()" href="<?php echo base_url(); ?>voice_sms/audio_delete/<?php echo $row['id']; ?>" class="btn btn-danger btn-xs mb-1" title="Edit">Delete</a>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>

