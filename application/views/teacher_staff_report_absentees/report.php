<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
		}
	</style>

	<?php
}
?>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
		<thead>
		<tr>
			<td  class="h_td" style="text-align:center; padding: 5px;" colspan="3">
				<b style="font-size: 20px;"><?php echo $HeaderInfo[0]['school_name']; ?></b><br>
				<?php echo $HeaderInfo[0]['address']; ?><br>
				<?php echo $title; ?></b>
			</td>
		</tr>

		<tr>
			<td class="h_td" colspan="3">
				<b>
					Date: <?php echo date("M jS, Y", strtotime($date)); ?>
				</b>
			</td>
		</tr>

		<tr>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;<?php echo $this->lang->line('sl'); ?></td>
			<td scope="col" class="h_td" align="center">&nbsp;Teacher ID</td>
			<td scope="col" class="h_td"  style="min-width: 200px !important;">&nbsp;<?php echo $this->lang->line('name'); ?></td>
		</tr>

		</thead>

		<tbody>
		<?php
		$i = 0;
		foreach ($absentee_info as $row):
			$i++;
			?>
			<tr>
				<td align="center">
					&nbsp;<?php echo $i; ?>
				</td>
				<td align="center">
					&nbsp;<?php echo $row['teacher_code']; ?>
				</td>
				<td align="left" style="min-width: 200px !important;">
					&nbsp;<?php echo $row['name']; ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
