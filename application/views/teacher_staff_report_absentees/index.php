<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>teacher_staff_report_absentees/index#attendance"
	  method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label for="from_date"><?php echo $this->lang->line('date'); ?></label>
			<div class="input-group date">
				<input type="text" value="<?php if(isset($date)){ echo $date; } ?>" autocomplete="off"
					   name="date" id="date" required class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
			</div>
		</div>
	</div>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
		<input type="submit" class="btn btn-primary" value="View Report">
	</div>

</form>

<div id="printableArea">
	<?php
	if (isset($absentee_info)) {
		echo $report;
	}
	?>
</div>
