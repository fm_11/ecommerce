<script>
	function checkCheckBox() {
		var inputElems = document.getElementsByTagName("input"),
				count = 0;
		for (var i = 0; i < inputElems.length; i++) {
			if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
				count++;
			}
		}
		if (count < 1) {
			alert("Please select some class");
			return false;
		} else {
			return true;
		}
	}

	function checkItemAll(ele) {
		var checkboxes = document.querySelectorAll("input[type='checkbox']");
		if (ele.checked) {
			for (var i = 0; i < checkboxes.length; i++) {
				if (checkboxes[i].type == 'checkbox' && checkboxes[i].disabled ==  false) {
					checkboxes[i].checked = true;
				}
			}
		} else {
			for (var i = 0; i < checkboxes.length; i++) {
				console.log(i)
				if (checkboxes[i].type == 'checkbox' && checkboxes[i].disabled ==  false) {
					checkboxes[i].checked = false;
				}
			}
		}
	}
</script>

<form name="addForm" class="cmxform" id="commentForm" onsubmit="return checkCheckBox()" action="<?php echo base_url(); ?>data_copy/class_copy#ui-basic" method="post">

<div class="row">
	<div class="col-md-1 p-2 bg-primary border text-center">
		<input type="checkbox" onclick="checkItemAll(this)" class="form-check-input">
	</div>
	<div class="col-md-2 p-2 bg-primary border text-center text-white">Name (English)</div>
	<div class="col-md-2 p-2 bg-primary border text-center text-white">Name (Bangla)</div>
	<div class="col-md-3 p-2 bg-primary border text-center text-white">ID Short Form</div>
</div>

	<?php
foreach ($classes as $key=>$value){
	$disabled = "";
	if(isset($class_list[trim(strtolower($value['name_english']))])){
		$disabled = "disabled";
	}
?>


<div class="form-inline">
	<div class="form-group">
		<div class="form-check form-check-success">
			<label class="form-check-label">
				<input <?php echo $disabled; ?> type="checkbox" name="selected_item_<?php echo $key; ?>" value="<?php echo $key; ?>"
					   class="form-check-input" <?php if($disabled == ''){ echo 'checked="1"'; } ?>>
				<i class="input-helper"></i>
			</label>
		</div>
		<input style="padding:8px;" name="name_english_<?php echo $key; ?>"  type="text" <?php echo $disabled; ?> value="<?php echo $value['name_english']; ?>" class="form-control" /> &nbsp;
		<input style="padding:8px;" name="name_bangla_<?php echo $key; ?>"  type="text" <?php echo $disabled; ?> value="<?php echo $value['name_bangla']; ?>" class="form-control" />&nbsp;
		<input style="padding:8px;" name="short_form_<?php echo $key; ?>"  type="text" <?php echo $disabled; ?> value="<?php echo $value['short_form']; ?>" class="form-control" />
	</div>
</div>


<?php } ?>

<div class="float-right">
	<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
</div>

</form>
