<table width="100%" cellpadding="0" class="data_table" cellspacing="0" id="box-table-a" summary="StudentReport">
      <thead>
      <tr>
          <?php
          if (isset($post_data['view_code'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('student_code') . '</th>';
          }

          if (isset($post_data['view_roll'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('roll') . '</th>';
          }
          if (isset($post_data['view_name'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('name') . '</th>';
          }

          if (isset($post_data['view_father_name'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('father_name') . '</th>';
          }
          if (isset($post_data['view_mother_name'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('mother_name') . '</th>';
          }
          if (isset($post_data['view_class'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('class') . '</th>';
          }
          if (isset($post_data['view_shift'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('shift') . '</th>';
          }
          if (isset($post_data['view_section'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('section') . '</th>';
          }
          if (isset($post_data['view_group'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('group') . '</th>';
          }
          if (isset($post_data['view_gender'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('gender') . '</th>';
          }
          if (isset($post_data['view_religion'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('religion') . '</th>';
          }
          if (isset($post_data['view_blood_group'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('blood_group') . '</th>';
          }
          if (isset($post_data['view_process_code'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('process_code') . '</th>';
          }
          if (isset($post_data['view_mobile'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('mobile') . '</th>';
          }
          if (isset($post_data['view_date_of_birth'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('date_of_birth') . '</th>';
          }
          if (isset($post_data['view_birth_certificate_no'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('birth_certificate_no') . '</th>';
          }
		  if (isset($post_data['view_custom_field'])) {
			  echo '<th scope="col" class="td_center" style="min-width: 180px;">' . $post_data['custom_field_text'] . '</th>';
		  }
          if (isset($post_data['view_photo'])) {
              echo '<th scope="col" class="td_center">' . $this->lang->line('photo') . '</th>';
          }
          ?>
      </tr>
      </thead>
      <tbody>
      <?php
      $i = 0;
      foreach ($students as $row):
          $i++;
          ?>
          <tr>

            <?php

            if (isset($post_data['view_code'])) {
                echo '<td class="td_center">' . $row['student_code'] . '</td>';
            }

            if (isset($post_data['view_roll'])) {
                echo '<td class="td_center">' . $row['roll_no'] . '</td>';
            }
            if (isset($post_data['view_name'])) {
                echo '<td class="td_left">' . $row['name'] . '</td>';
            }

            if (isset($post_data['view_father_name'])) {
                echo '<td class="td_left">' . $row['father_name'] . '</td>';
            }
            if (isset($post_data['view_mother_name'])) {
                echo '<td class="td_left">' . $row['mother_name'] . '</td>';
            }
            if (isset($post_data['view_class'])) {
                echo '<td class="td_center">' . $row['class_name'] . '</td>';
            }
            if (isset($post_data['view_shift'])) {
                echo '<td class="td_center">' . $row['shift_name'] . '</td>';
            }
            if (isset($post_data['view_section'])) {
                echo '<td class="td_center">' . $row['section_name'] . '</td>';
            }
            if (isset($post_data['view_group'])) {
                echo '<td class="td_center">' . $row['group_name'] . '</td>';
            }
            if (isset($post_data['view_gender'])) {
                if ($row['gender'] == 'M') {
                    $this_st_gender = 'Male';
                } elseif ($row['gender'] == 'F') {
                    $this_st_gender = 'Female';
                } else {
                    $this_st_gender = 'N/A';
                }
                echo '<td class="td_center">' . $this_st_gender . '</td>';
            }
            if (isset($post_data['view_religion'])) {
                if ($row['religion'] == 'I') {
                    $this_st_religion = 'Islam';
                } elseif ($row['religion'] == 'H') {
                    $this_st_religion = 'Hindu';
                } else {
                    $this_st_religion = 'N/A';
                }
                echo '<td class="td_center">' . $this_st_religion . '</td>';
            }
            if (isset($post_data['view_blood_group'])) {
                echo '<td class="td_center">' . $row['blood_group'] . '</td>';
            }
            if (isset($post_data['view_process_code'])) {
                echo '<td class="td_center">' . $row['process_code'] . '</td>';
            }
            if (isset($post_data['view_mobile'])) {
                echo '<td class="td_center">' . $row['guardian_mobile'] . '</td>';
            }
            if (isset($post_data['view_date_of_birth'])) {
                echo '<td class="td_center">' . $row['date_of_birth'] . '</td>';
            }
            if (isset($post_data['view_birth_certificate_no'])) {
                echo '<td class="td_center">' . $row['birth_certificate_no'] . '</td>';
            }
			if (isset($post_data['view_custom_field'])) {
				echo '<td class="td_center">&nbsp</td>';
			}
            if (isset($post_data['view_photo'])) {
                ?>
              <td class="td_center">
                  <img style="height:60px; width:50px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/<?php if ($row['photo']!='') {
                    echo 'student/'.$row['photo'];
                } else {
                    echo 'img/not_found.jpg';
                } ?>">
              </td>
            <?php
            } ?>

          </tr>
      <?php endforeach; ?>
      </tbody>
  </table>
