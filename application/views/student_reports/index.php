<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    function checkCustomField() {
		var isChecked = document.getElementById("view_custom_field").checked;
		if(isChecked == true){
			var custom_field_text = document.getElementById("custom_field_text").value;
			if(custom_field_text == ''){
				alert("Please input custom field header text");
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
</script>

<form name="addForm" id="addForm" onsubmit="return checkCustomField()" target="_blank" action="<?php echo base_url(); ?>student_reports/index" method="post">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label>Name/ Roll No. / Student ID</label>
      <input type="text" name="roll" value="<?php if (isset($roll)) {
    echo $roll;
} ?>" placeholder="Name or Roll or Student ID" class="form-control" id="roll">
    </div>
    <div class="form-group col-md-3">
      <label>Class</label>
      <select name="class_id" class="js-example-basic-single w-100" id="class_id">
          <option value="">-- All --</option>
          <?php foreach ($class_list as $row) { ?>
              <option value="<?php echo $row['id']; ?>"<?php if (isset($class_id)) {
    if ($row['id'] == $class_id) {
        echo 'selected';
    }
} ?>><?php echo $row['name']; ?></option>
          <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label>Group</label>
      <select name="group" class="js-example-basic-single w-100">
          <option value="">-- All --</option>
          <?php foreach ($group_list as $row) { ?>
              <option value="<?php echo $row['id']; ?>"<?php if (isset($group)) {
    if ($row['id'] == $group) {
        echo 'selected';
    }
} ?>><?php echo $row['name']; ?></option>
          <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label>Gender</label>
      <select name="gender" class="js-example-basic-single w-100">
          <option value="">-- All --</option>
          <option value="M" <?php if (isset($gender)) {
    if ($gender == 'M') {
        echo 'selected';
    }
} ?>>Male
          </option>
          <option value="F" <?php if (isset($gender)) {
    if ($gender == 'F') {
        echo 'selected';
    }
} ?>>Female
          </option>
      </select>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-3">
      <label>Section</label>
      <select name="section_id" class="js-example-basic-single w-100" id="section_id">
          <option value="">-- All --</option>
          <?php foreach ($section_list as $row) { ?>
              <option value="<?php echo $row['id']; ?>"<?php if (isset($section_id)) {
    if ($row['id'] == $section_id) {
        echo 'selected';
    }
} ?>><?php echo $row['name']; ?></option>
          <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label>Religion</label>
      <select name="religion" class="js-example-basic-single w-100">
          <option value="">-- All --</option>
          <option value="I" <?php if (isset($religion)) {
    if ($religion == 'I') {
        echo 'selected';
    }
} ?>>Islam
          </option>
          <option value="H" <?php if (isset($religion)) {
    if ($religion == 'H') {
        echo 'selected';
    }
} ?>>Hindu
          </option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label>Blood Group</label>
      <select name="blood_group_id" class="js-example-basic-single w-100">
          <option value="">-- All --</option>
          <?php foreach ($blood_group_list as $row) { ?>
              <option value="<?php echo $row['id']; ?>" <?php if (isset($blood_group_id)) {
    if ($row['id'] == $blood_group_id) {
        echo 'selected';
    }
} ?>><?php echo $row['name']; ?></option>

          <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label>Shift</label>
      <select name="shift_id"  class="js-example-basic-single w-100">
          <option value="">-- All --</option>
          <?php foreach ($shift_list as $row) { ?>
              <option value="<?php echo $row['id']; ?>" <?php if (isset($shift_id)) {
    if ($row['id'] == $shift_id) {
        echo 'selected';
    }
} ?>><?php echo $row['name']; ?></option>

          <?php } ?>
      </select>
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-3">
      <label>Status</label>
      <select name="student_status" class="js-example-basic-single w-100">
          <option value="">-- All --</option>
          <option value="1" <?php if (isset($student_status)) {
    if ($student_status == '1') {
        echo 'selected';
    }
} ?>>Active
          </option>
          <option value="0" <?php if (isset($student_status) && $student_status != '') {
    if ($student_status == '0') {
        echo 'selected';
    }
} ?>>Inactive
          </option>
      </select>
    </div>
  </div>


<hr>
<div class="form-row">
  <div class="form-group col-md-12">
    <label><b>Report Columns</b></label><br>
    <label class="checkbox inline">
      <input type="checkbox" name="view_code" checked value="code"> <?php echo $this->lang->line('student_code'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_name" checked value="name"> <?php echo $this->lang->line('name'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_roll" checked value="roll"> <?php echo $this->lang->line('roll'); ?>
    </label>
    <label class="checkbox inline">
        <input type="checkbox" name="view_father_name" value="father_name"> <?php echo $this->lang->line('father_name'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_mother_name" value="mother_name"> <?php echo $this->lang->line('mother_name'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_class" checked value="class"> <?php echo $this->lang->line('class'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_shift" checked value="shift"> <?php echo $this->lang->line('shift'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_section" checked value="section"> <?php echo $this->lang->line('section'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_group" checked value="group"> <?php echo $this->lang->line('group'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_gender" value="gender"> <?php echo $this->lang->line('gender'); ?>
    </label>
    <label class="checkbox inline">
        <input type="checkbox" name="view_religion" value="religion"> <?php echo $this->lang->line('religion'); ?>
    </label>
    <label class="checkbox inline">
        <input type="checkbox" name="view_blood_group" value="blood_group"> <?php echo $this->lang->line('blood_group'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_process_code" value="process_code"> <?php echo $this->lang->line('process_code'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_mobile" value="mobile"> <?php echo $this->lang->line('mobile'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_photo" value="photo"> <?php echo $this->lang->line('photo'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_date_of_birth" value="date_of_birth"> <?php echo $this->lang->line('date_of_birth'); ?>
    </label>
    <label class="checkbox inline">
      <input type="checkbox" name="view_birth_certificate_no" value="birth_certificate_no"> <?php echo $this->lang->line('birth_certificate_no'); ?>
    </label>
	 <label class="checkbox inline">
		<input type="checkbox" name="view_custom_field" id="view_custom_field" value="view_custom_field"> Custom Empty Filed
		 <input type="text" name="custom_field_text" id="custom_field_text" style="padding: 4px; border: 2px #000000 solid;">
	 </label>


  </div>
</div>

  <div class="btn-group float-right">
    <!-- <input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
    <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/> -->
    <input type="submit" class="btn btn-primary" value="View Report">
  </div>

</form>


<div id="printableArea">
    <?php
    if (isset($students)) {
        echo $report;
    }
    ?>
</div>
