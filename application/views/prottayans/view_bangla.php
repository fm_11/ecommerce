<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>

	<!-- Normalize or reset CSS with your favorite library -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/normalize.min.css">

	<!-- Load paper.css for happy printing -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/paper.css">

	<!-- Set page size here: A5, A4 or A3 -->
	<!-- Set also "landscape" if you need -->
	<style>
		@page { size: A4 }

		.body_content{
			width: 100%;
			height: 100%;
			box-sizing: border-box;
			border: 10px #538cc6 solid;
		}

		.body_content_second{
			width: 100%;
			height: 100%;
			box-sizing: border-box;
			border: 6px #ff8080 solid
		}


		.header-center {
			width:100%;
			float:left;
			height: 320px;
			box-sizing: border-box;
			text-align: center;
		}

		.header_title{
			margin-top: 40px;
		}

		.address{
			margin-top: -40px;
			padding: 5px;
		}

		.admit_text{
			width: 200px;
			border: 1px black solid;
			margin: 0px auto;
			margin-top: 10px;
			padding: 10px;
			border-radius: 50px;
			background-color: #538cc6;
			color: #ffffff;
			font-weight: bold;
			font-size: 20px;
		}

		.sl_date{
			width: 90%;
			margin: 0px auto;
			margin-top: 45px;
		}

		.sl{
			float: left;
			width: 50%;
			text-align:left;
			padding: 6px;
			font-weight:bold;
		}

		.date{
			float: right;
			width: 40%;
			text-align:right;
			padding: 6px;
			font-weight:bold;
		}

		.main_text{
			float: left;
			width:90%;
			height: 200px;
			box-sizing: border-box;
			text-align: center;
			margin:0px auto;
			text-align: justify;
			margin-left: 38px;
			font-size: 18px;
			line-height: 30px;
			margin-top: 70px;
			
		}


		* {
			-webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
			color-adjust: exact !important;                 /*Firefox*/
		}
		
		
	.signature_info{
		 width: 30%;
		box-sizing: border-box;
		height: 100px;
		bottom: 0px;
		float: left;
		right: 36px;
		position: absolute;
		text-align: center;
		bottom:100px;
	  }


		#footer-content {
			position: absolute;
			text-align: right;
			width: 90%;
			font-size: 15px;
			bottom: 40px;
		}



	</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
<section class="sheet padding-5mm">
	<div class="body_content">
		<div class="body_content_second">
			<div id="header-center" class="header-center">
				<h1 class="header_title"><?php echo $school_info->school_name_bangla; ?></h1><br>
				<div class="address"><?php echo $school_info->address_bangla; ?></div>
				<img  class="img" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info->picture; ?>" align="center"
					  width="90px" height="100px">
				<div class="admit_text">
					<?php echo $title; ?>
				</div>
				<div class="sl_date">
					<div class="sl">
						স্মারক নং - ............................................
					</div>
					<div class="date">
						তারিখ  - ........./......../...............
					</div>
				</div>
			</div>

			<div class="main_text">
				এই মর্মে প্রত্যয়ন করা যাচ্ছে যে, <b><?php  echo $student_info[0]['name_bangla'];  ?></b> 
				পিতা - <b><?php echo $fathers_name; ?></b>, মাতা - <b><?php echo $mothers_name; ?></b>  ঠিকানা - <b><?php echo $address; ?></b>। 
				সে অত্র বিদ্যলয়ে   <b><?php  echo $student_info[0]['class_name_bangla'];  ?></b>
				শ্রেণির রোল নং- <b><?php  echo $roll_no;  ?></b>  নিয়মিত ছাত্র/ছাত্রী।
				 আমার জানামতে সে সমাজ বা রাষ্ট্র বিরোধী কোন কাজে জড়িত নহে। তার নৈতিক চরিত্র ভাল। 
				তার জন্ম তারিখ   - <?php  echo $date_of_birth;  ?>। 
                 
				<br>
				<br>
				<br>
				<br>
			

                                                আমি তার জীবনের সার্বিক সফলতা ও উন্নতি কামনা করি।

			</div>
			
			
			<div class="signature_info">       			
				................................<br>
				<span>সাক্ষর</span>		
            </div>

			<div id="footer-content">Powered By: <span style="color:#0059b3;">School</span><span style="color:#ff8080;">360</span></div>
			
		</div>
	</div>
</section>

</body>

</html>
