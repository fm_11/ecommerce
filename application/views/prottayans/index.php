<script type="text/javascript">
	function getStudentByClassShiftSection(class_shift_section_id,group_id){
		//alert(class_shift_section_id);
		//alert(group_id);
		if(class_shift_section_id != ''){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					// alert(xmlhttp.responseText);
					document.getElementById("student_id").innerHTML = xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>students/getStudentByClassShiftSection?class_shift_section_id=" + class_shift_section_id + "&&group_id=" + group_id, true);
			xmlhttp.send();
		}
	}

	function getStudentInformationById(student_id) {
		if(student_id != ''){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{

					var jsonObject = JSON.parse((xmlhttp.responseText));
					//alert(jsonObject[0].name);
					var ImageFileLocation = jsonObject[0].photo;
					if(ImageFileLocation != '' && ImageFileLocation != 'default.png'){
						document.getElementById("student_image").src = '<?php echo base_url() . MEDIA_FOLDER; ?>/student/' + ImageFileLocation;
					}else{
						document.getElementById("student_image").src = '<?php echo base_url(); ?>/core_media/images/default.png';
					}
					document.getElementById("name").value = jsonObject[0].name;
					document.getElementById("fathers_name").value = jsonObject[0].father_name;
					document.getElementById("mothers_name").value = jsonObject[0].mother_name;
					document.getElementById("address").value = jsonObject[0].present_address;
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>students/getStudentInformationById?student_id=" + student_id, true);
			xmlhttp.send();
		}else{
			//alert(55);
			document.getElementById("student_image").src = '<?php echo base_url(); ?>/core_media/images/default.png';
		}
	}

</script>



<form name="addForm" class="cmxform" id="commentForm" target="_blank"  action="<?php echo base_url(); ?>prottayans/index" method="post">
	<div class="form-row">
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
			<select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id"
					onchange="getStudentByClassShiftSection(this.value,document.getElementById('group_id').value);" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
							<?php if (isset($class_shift_section_id)) {
								if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('group'); ?></label>
			<select onchange="getStudentByClassShiftSection(document.getElementById('class_shift_section_id').value,this.value);"
					class="js-example-basic-single w-100" name="group_id" id="group_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($groups)) {
					foreach ($groups as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('student'); ?></label>
			<select onchange="getStudentInformationById(this.value)" class="js-example-basic-single w-100" name="student_id"
					id="student_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				foreach ($students as $list) {
					?>
					<option
						value="<?php echo $list['id']; ?>"><?php echo $list['roll_no'] .  '-' . $list['name']; ?></option>
					<?php
				}
				?>
			</select>
			<input type="hidden" autocomplete="off" id="name" required name="name" class="form-control" value="">
		</div>

		<div class="form-group col-md-2">
			<img class="img" id="student_image" src="<?php echo base_url(); ?>/core_media/images/default.png" width="80" height="85">
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Father's Name</label>
			<input type="text" autocomplete="off" id="fathers_name" required name="fathers_name" class="form-control" value="">
		</div>

		<div class="form-group col-md-4">
			<label>Mother's Name</label>
			<input type="text" autocomplete="off" id="mothers_name" required name="mothers_name" class="form-control" value="">
		</div>

		<div class="form-group col-md-4">
			<label>Report Type</label>
			<select class="js-example-basic-single w-100" name="report_type">
				<option value="B">Bangla</option>
				<option value="E">English</option>
			</select>
		</div>

	</div>

	<div class="form-row">
		<div class="form-group col-md-12">
			<label>Address</label>
			<input type="text" autocomplete="off" id="address" required name="address" class="form-control" value="">
		</div>
	</div>


	<div class="float-right">
		<button type='submit' class="btn btn-primary">View</button>
	</div>

</form>
