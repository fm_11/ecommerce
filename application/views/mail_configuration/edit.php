

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>mail_configuration/edit/<?php echo $row_data->id; ?>" method="post">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_username')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_username" value="<?php echo $row_data->mail_username; ?>" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_password')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_password" value="<?php echo $row_data->mail_password; ?>" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_encryption')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_encryption" value="<?php echo $row_data->mail_encryption; ?>" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('admin_mail_address')?> <span class="required_label">*</span></label>
      <input type="email" autocomplete="off"  class="form-control" name="admin_mail_address" value="<?php echo $row_data->admin_mail_address; ?>" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_driver')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_driver" value="<?php echo $row_data->mail_driver; ?>" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_host')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_host" value="<?php echo $row_data->mail_host; ?>" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_port')?> <span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="mail_port" value="<?php echo $row_data->mail_port; ?>" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_from_address')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_from_address" value="<?php echo $row_data->mail_from_address; ?>" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_from_name')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_from_name" value="<?php echo $row_data->mail_from_name; ?>" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('customer_notify')?> <span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"  class="form-control" name="customer_notify" required="1" value="<?php echo $row_data->customer_notify; ?>"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('admin_notify')?> <span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"  class="form-control" name="admin_notify" required="1" value="<?php echo $row_data->admin_notify; ?>"/>
    </div>
  </div>

    <div class="float-right">
	   <input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $row_data->id; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
