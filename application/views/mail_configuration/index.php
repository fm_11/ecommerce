
<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>mail_configuration/update" method="post" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Mail Username<span class="required_label">*</span></label>
      <input type="hidden" name="id"  class="form-control" value="<?php echo $mail_info->id;  ?>">
      <input type="text" autocomplete="off"  name="mail_username"  class="form-control" value="<?php echo $mail_info->mail_username;  ?>">
    </div>
    <div class="form-group col-md-4">
      <label>Password<span class="required_label">*</span></label>
      <input type="password" autocomplete="off"  name="mail_password"  class="form-control" value="<?php echo $mail_info->mail_password;  ?>">
    </div>
    <div class="form-group col-md-4">
      <label>Encryption<span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  name="mail_encryption"  class="form-control" value="<?php echo $mail_info->mail_encryption;  ?>">
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Admin Mail Address<span class="required_label">*</span></label>
      <input type="email" autocomplete="off"  name="admin_mail_address"  class="form-control" value="<?php echo $mail_info->admin_mail_address;  ?>">
    </div>
    <div class="form-group col-md-4">
      <label>Mail Host<span class="required_label">*</span></label>
      <input type="email" autocomplete="off"  name="mail_host"  class="form-control" value="<?php echo $mail_info->mail_host;  ?>">
    </div>
    <div class="form-group col-md-4">
      <label>Mail Driver<span class="required_label">*</span></label>
      <input type="email" autocomplete="off"  name="mail_driver"  class="form-control" value="<?php echo $mail_info->mail_driver;  ?>">
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Mail Port<span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  name="mail_port"  class="form-control" value="<?php echo $mail_info->mail_port;  ?>">
    </div>
    <div class="form-group col-md-4">
      <label>Mail From Address<span class="required_label">*</span></label>
      <input type="email" autocomplete="off"  name="mail_from_address"  class="form-control" value="<?php echo $mail_info->mail_from_address;  ?>">
    </div>
    <div class="form-group col-md-4">
      <label>Mail From Name<span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  name="mail_from_name"  class="form-control" value="<?php echo $mail_info->mail_from_name;  ?>">
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Customer Notify<span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"  name="customer_notify"  class="form-control" value="<?php echo $mail_info->customer_notify;  ?>">
    </div>
    <div class="form-group col-md-4">
      <label>Admin Notify<span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"  name="admin_notify"  class="form-control" value="<?php echo $mail_info->admin_notify;  ?>">
    </div>
  </div>

  <div class="float-right">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
  </div>

</form>
