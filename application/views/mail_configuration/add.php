

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>mail_configuration/add" method="post">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_username')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_username" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_password')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_password" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_encryption')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_encryption" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('admin_mail_address')?> <span class="required_label">*</span></label>
      <input type="email" autocomplete="off"  class="form-control" name="admin_mail_address" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_driver')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_driver" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_host')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_host" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_port')?> <span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="mail_port" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_from_address')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_from_address" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('mail_from_name')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="mail_from_name" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('customer_notify')?> <span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"  class="form-control" name="customer_notify" required="1" value="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('admin_notify')?> <span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"  class="form-control" name="admin_notify" required="1" value="1"/>
    </div>
  </div>


    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
