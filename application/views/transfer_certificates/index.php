<?php
$sl_no = $this->session->userdata('sl_no');
?>

<form class="form-inline" method="post" action="<?php echo base_url(); ?>testimonials/index">
	<div class="col-md-offset-2 col-md-12">

		<label class="sr-only" for="FromDate">Sl No. / Name</label>
		<?php
		$placeholder = 'SL No. / Name';
		?>
		<input type="text" autocomplete="off"  style="max-width:200px;height: 37px;margin-top: -4px;"
			   name="sl_no" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($sl_no)) {
			echo $sl_no;
		} ?>" class="form-control" id="sl_no">
		<button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
	</div>
</form>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th scope="col"><?php echo $this->lang->line('sl'); ?></th>
			<th scope="col"><?php echo $this->lang->line('name'); ?></th>
			<th scope="col">Left Date</th>
			<th scope="col">Issue Date</th>
			<th scope="col"><?php echo $this->lang->line('actions'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = (int)$this->uri->segment(3);
		foreach ($testimonials as $row):
			$i++;
			?>
			<tr>
				<td><?php echo $row['sl_no']; ?></td>
				<td><?php echo $row['name']; ?></td>
				<td><?php echo $row['left_date']; ?></td>
				<td><?php echo $row['issue_date']; ?></td>
				<td>
					<div class="dropdown">
						<button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="ti-pencil-alt"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
							<a class="dropdown-item" target="_blank" href="<?php echo base_url(); ?>transfer_certificates/view/<?php echo $row['id']; ?>"><?php echo $this->lang->line('view'); ?></a>
						</div>
					</div>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>
