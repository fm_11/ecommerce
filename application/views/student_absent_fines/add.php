<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_absent_fines/add" method="post">
    <div class="form-row">
        <div class="form-group col-md-3">
            <label><?php echo $this->lang->line('year'); ?></label>
            <select class="form-control" name="year" id="year" required="1">
                <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
                <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label><?php echo $this->lang->line('class'); ?></label>
            <select name="class_id" class="form-control" required="1" id="class_id">
                <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
                <?php foreach ($class_list as $row) { ?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label><?php echo $this->lang->line('section'); ?></label>
            <select name="section_id" class="form-control" required="1" id="section_id">
                <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
                <?php foreach ($section_list as $row) { ?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label><?php echo $this->lang->line('group'); ?></label>
            <select name="group" required="1" class="form-control">
                <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
                <?php foreach ($group_list as $row) { ?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="float-right">
        <input type="submit" class="submit" value="Process">
    </div>

</form>
