<script type="text/javascript">
function statusChangeConfirm() {
		var result = confirm("Are you sure to change status?");
		if (result == true) {
			return true;
		} else {
			return false;
		}
	}
</script>
<?php
$order_code = $this->session->userdata('order_code');
?>
<form name="addForm" class="cmxform" id="commentForm"  method="post" action="<?php echo base_url(); ?>orders/index">
  <div class="form-row">
      <div class="form-group col-md-4">
        <input type="text" placeholder="Order No." style="padding: 10px !important;" autocomplete="off"
        name="order_code" value="<?php if (isset($order_code)) {
              echo $order_code;
          } ?>" class="form-control" id="order_code">
      </div>
      <div class="form-group col-md-2">
        <button type="submit" style="padding: 13px 30px 13px 30px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
      </div>
  </div>

</form>

<hr>

<div class="table-responsive-sm">
    <table id="sortable-table-1" style="width: 100% !important;" class="table">
		<thead>
    <tr>
        <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th scope="col">Order No.</th>
        <th scope="col">Customer Name</th>
        <th scope="col">Mobile</th>
        <th scope="col">Total Amount</th>
        <th scope="col">Status</th>
        <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($fees as $row):
        $i++;
        ?>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['order_code']; ?></td>
            <td><?php echo $row['customer_name']; ?></td>
            <td><?php echo $row['phone']; ?></td>
            <td><?php echo $row['total']; ?></td>
            <td>
              <?php
							$status_s="";
							$badge="";
              if($row['status'] == 'PRO'){
                $status_s= 'On Process';

								$badge='warning';
              }else if($row['status'] == 'D'){
                $status_s= 'Delivered';
								$badge='success';
              }
              else if($row['status'] == 'C'){
                $status_s= 'Canceled';
								$badge='danger';
              }else{
                $status_s= 'Pending';
									$badge='info';
              }

               ?>
							 <label class="badge badge-<?php echo $badge;?>"><?php echo $status_s;?></label>
            </td>
            <td>

                <div class="dropdown">
                    <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-pencil-alt"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                      <a class="dropdown-item" target="_blank" href="<?php echo base_url(); ?>orders/view_invoice/<?php echo $row['id']; ?>"
                      title="Status Change">View Invoice</a>
                      <a onclick="return statusChangeConfirm()" class="dropdown-item" href="<?php echo base_url(); ?>orders/order_status_change/<?php echo $row['id']; ?>/PRO"
                      title="Status Change">On Process</a>
                      <a onclick="return statusChangeConfirm()" class="dropdown-item" href="<?php echo base_url(); ?>orders/order_status_change/<?php echo $row['id']; ?>/D"
                      title="Status Change">Delivered</a>
                      <a onclick="return statusChangeConfirm()" class="dropdown-item" href="<?php echo base_url(); ?>orders/order_status_change/<?php echo $row['id']; ?>/C"
                      title="Status Change">Canceled</a>
                    </div>
                </div>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
    <div class="float-right">
        <?php echo $this->pagination->create_links(); ?>
    </div>
</div>
