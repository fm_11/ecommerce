
 <div class="card px-2" id="divPrintable">
     <div class="card-body">
         <div class="container-fluid d-flex justify-content-between">
           <div class="col-lg-3 pl-0">
             <img src="<?php echo base_url(); ?>ecommerce_media/website/images/logo-3.png" alt="">
             <p class="mt-5 mb-2"><b><?php echo $HeaderInfo[0]['school_name'] ?></b></p>
             <p><?php echo $HeaderInfo[0]['address'] ?></p>
             <p>Mobile: <?php echo $HeaderInfo[0]['mobile'] ?></p>
           </div>
           <div class="col-lg-3 pr-0">
             <h4 class="text-right">INV-#<?php echo $orders[0]['order_code']; ?></h4>
             <p class="mt-5 mb-2 text-right"><b>Invoice to</b></p>
             <p class="text-right"><?php echo $orders[0]['customer_name']; ?>
               <br> <?php echo $orders[0]['address']; ?>
               <br> Mobile: <?php echo $orders[0]['phone']; ?></p>
           </div>
         </div>
         <div class="container-fluid d-flex justify-content-between">
           <div class="col-lg-3 pl-0">
             <p class="mb-0 mt-5">
                Date: <?php echo date('jS F, Y', strtotime($orders[0]['created_date'])); ?>
              </p>

           </div>
         </div>
         <div class="container-fluid mt-5 d-flex justify-content-center w-100">
           <div class="table-responsive w-100">
               <table class="table">
                 <thead>
                   <tr>
                       <th>#</th>
                       <th>Description</th>
                       <th class="text-right">Quantity</th>
                       <th class="text-right">Unit cost</th>
                       <th class="text-right">Total</th>
                     </tr>
                 </thead>
                 <tbody>
                   <?php
                   $i = 0;
                   $total = 0;
                   foreach ($order_details as $row):
                       $i++;
                       ?>
                   <tr class="text-right">
                     <td class="text-left"><?php echo $i; ?></td>
                     <td class="text-left"><?php echo $row['product_name']; ?></td>
                     <td><?php echo $row['quantity']; ?></td>
                     <td><?php echo $row['sub_total'] / $row['quantity']; ?></td>
                     <td>
                       <?php
                       $total += $row['sub_total'];
                       echo $row['sub_total'];
                        ?>
                     </td>
                   </tr>

                   <?php endforeach; ?>
                 </tbody>
               </table>
             </div>
         </div>
         <div class="container-fluid mt-5 w-100">
           <p class="text-right mb-2">Sub - Total amount: ৳<?php echo number_format($total,2); ?></p>
           <p class="text-right">Delivery Cost : ৳80</p>
           <h4 class="text-right mb-5">Total : ৳<?php echo number_format($total + 80,2); ?></h4>
           <hr>
         </div>
     </div>
 </div>


 <div class="container-fluid w-100">
   <a href="javascript:void"  onclick="printPartOfPage()" class="btn btn-primary float-right mt-4 ml-2"><i class="ion ion-md-print mr-1"></i>Print</a>
  </div>

  <script>
	function printPartOfPage() {
		var title = document.title;
		var divElements = document.getElementById('divPrintable').innerHTML;
		var printWindow = window.open("", "_blank", "");
		//open the window
		printWindow.document.open();
		//write the html to the new window, link to css file
		//printWindow.document.write('<html><head><title>' + title + '</title><link rel="stylesheet" type="text/css" href="/Content/PrintMedia.css"></head><body>');
		printWindow.document.write('<html><head><title>' + title + '</title></head><body>');
		printWindow.document.write(divElements);
		printWindow.document.write('</body></html>');
		printWindow.document.title = "";
		printWindow.document.date = "";
		printWindow.document.close();
		printWindow.focus();
		//The Timeout is ONLY to make Safari work, but it still works with FF, IE & Chrome.

		setTimeout(function () {
			printWindow.print();
			printWindow.close();
		}, 50);

	}
</script>
