
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>Leave_types/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Leave Type</label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $types[0]['name']; ?>" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label>Short Name</label>
      <input type="text" autocomplete="off"  class="form-control" name="short_name" value="<?php echo $types[0]['short_name']; ?>" required="1"/>
    </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $types[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
