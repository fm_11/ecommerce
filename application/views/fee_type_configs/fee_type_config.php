
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>fee_type_configs/add" method="post">
      <div class="table-sorter-wrapper col-lg-12 table-responsive">
        <table id="sortable-table-1" class="table">
        <tr>
            <th colspan="2" scope="col">
              <div class="form-group">
                <div class="col-md-3">
                		 <label>Year</label>
                			<select class="form-control" name="year" required="1">
                          <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                          <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
                          <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                			</select>
                  </div>
               </div>
    		</th>
        </tr>

    	<tr>
            <th colspan="2" scope="col">Annual Fee Type Payment Month</th>
        </tr>

    	<tr>
    	    <td>January</td>
    		<td><input type="radio" value="1" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>February</td>
    		<td><input type="radio" value="2" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>March</td>
    		<td><input type="radio" value="3" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>April</td>
    		<td><input type="radio" value="4" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>May</td>
    		<td><input type="radio" value="5" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>June</td>
    		<td><input type="radio" value="6" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>July</td>
    		<td><input type="radio" value="7" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>August</td>
    		<td><input type="radio" value="8" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>September</td>
    		<td><input type="radio" value="9" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>October</td>
    		<td><input type="radio" value="10" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>November</td>
    		<td><input type="radio" value="11" name="annual_fee_type"></td>
    	</tr>

    	<tr>
    	    <td>December</td>
    		<td><input type="radio" value="12" name="annual_fee_type"></td>
    	</tr>





    	<tr>
            <th colspan="2" scope="col">Tri-Annual Fee Type Payment Month</th>
        </tr>

    	<tr>
    	    <td>January</td>
    		<td><input type="checkbox" value="1" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>February</td>
    		<td><input type="checkbox" value="2" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>March</td>
    		<td><input type="checkbox" value="3" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>April</td>
    		<td><input type="checkbox" value="4" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>May</td>
    		<td><input type="checkbox" value="5" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>June</td>
    		<td><input type="checkbox" value="6" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>July</td>
    		<td><input type="checkbox" value="7" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>August</td>
    		<td><input type="checkbox" value="8" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>September</td>
    		<td><input type="checkbox" value="9" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>October</td>
    		<td><input type="checkbox" value="10" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>November</td>
    		<td><input type="checkbox" value="11" name="tri_annual_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>December</td>
    		<td><input type="checkbox" value="12" name="tri_annual_fee_type[]"></td>
    	</tr>






    	<tr>
            <th colspan="2" scope="col">Quarterly Fee Type Payment Month</th>
        </tr>

    	<tr>
    	    <td>January</td>
    		<td><input type="checkbox" value="1" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>February</td>
    		<td><input type="checkbox" value="2" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>March</td>
    		<td><input type="checkbox" value="3" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>April</td>
    		<td><input type="checkbox" value="4" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>May</td>
    		<td><input type="checkbox" value="5" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>June</td>
    		<td><input type="checkbox" value="6" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>July</td>
    		<td><input type="checkbox" value="7" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>August</td>
    		<td><input type="checkbox" value="8" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>September</td>
    		<td><input type="checkbox" value="9" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>October</td>
    		<td><input type="checkbox" value="10" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>November</td>
    		<td><input type="checkbox" value="11" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>December</td>
    		<td><input type="checkbox" value="12" name="quarterly_fee_type[]"></td>
    	</tr>

    	<tr>
            <th colspan="2" scope="col">Monthly Fee Type Payment Month</th>
        </tr>

    	<tr>
    	    <td>January</td>
    		<td><input type="checkbox" value="1" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>February</td>
    		<td><input type="checkbox" value="2" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>March</td>
    		<td><input type="checkbox" value="3" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>April</td>
    		<td><input type="checkbox" value="4" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>May</td>
    		<td><input type="checkbox" value="5" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>June</td>
    		<td><input type="checkbox" value="6" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>July</td>
    		<td><input type="checkbox" value="7" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>August</td>
    		<td><input type="checkbox" value="8" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>September</td>
    		<td><input type="checkbox" value="9" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>October</td>
    		<td><input type="checkbox" value="10" checked checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>November</td>
    		<td><input type="checkbox" value="11" checked name="monthly_fee_type[]"></td>
    	</tr>

    	<tr>
    	    <td>December</td>
    		<td><input type="checkbox" value="12" checked name="monthly_fee_type[]"></td>
    	</tr>
      </table>
     </div>
     <div class="float-right">
        <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
     </div>
  </form>
