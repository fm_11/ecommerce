
   <div class="table-sorter-wrapper col-lg-12 table-responsive">
     <table id="sortable-table-1" class="table">
    <tr>
        <td>
            <form class="form-inline" action="<?php echo base_url(); ?>fee_type_configs/index" method="post">
                  <div class="form-group">
                    <div class="col-md-3">
                       <label  class="sr-only">Year</label>
                        <select class="form-control" name="year" required="required">
                            <option value="<?php echo date('Y') - 1; ?>" <?php if ($year == date('Y') - 1) {
    echo 'selected';
} ?>><?php echo date('Y') - 1; ?></option>
                            <option value="<?php echo date('Y'); ?>" <?php if ($year == date('Y')) {
    echo 'selected';
} ?>><?php echo date('Y'); ?></option>
                            <option value="<?php echo date('Y') + 1; ?>" <?php if ($year == date('Y') + 1) {
    echo 'selected';
} ?>><?php echo date('Y') + 1; ?></option>
                        </select>
                     </div>
                  </div>
                  <input type="submit" name="View" class="btn btn-primary" value="View">
            </form>
        </td>
    </tr>
    <tr>
        <th scope="col">Annual Fee Type Payment Month</th>
    </tr>
    <?php
    foreach ($annual_fee_types as $row):
        ?>
        <tr>

            <td>
                <?php
                $dateObj = DateTime::createFromFormat('!m', $row['month']);
                echo $dateObj->format('F');
                ?>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr>
        <th colspan="2" scope="col">Tri-Annual Fee Type Payment Month</th>
    </tr>

    <?php
    foreach ($tri_annual_fee_types as $row):
        ?>
        <tr>

            <td>
                <?php
                $dateObj = DateTime::createFromFormat('!m', $row['month']);
                echo $dateObj->format('F');
                ?>
            </td>
        </tr>
    <?php endforeach; ?>


    <tr>
        <th scope="col">Quarterly Fee Type Payment Month</th>
    </tr>

    <?php
    foreach ($quarterly_fee_types as $row):
        ?>
        <tr>

            <td>
                <?php
                $dateObj = DateTime::createFromFormat('!m', $row['month']);
                echo $dateObj->format('F');
                ?>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr>
        <th scope="col">Monthly Fee Type Payment Month</th>
    </tr>

    <?php
    foreach ($monthly_fee_types as $row):
        ?>
        <tr>

            <td>
                <?php
                $dateObj = DateTime::createFromFormat('!m', $row['month']);
                echo $dateObj->format('F');
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
</div>
