<h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>audio_uploads/audio_message_upload" method="post" enctype="multipart/form-data">
	
		<div class="form-row">
			<div class="form-group col-md-6">
				<label><?php echo $this->lang->line('title'); ?></label>
				<input type="text" autocomplete="off"  class="form-control" name="title" required="1"/>
			</div>
			<div class="form-group col-md-6">
				<label><?php echo $this->lang->line('file'); ?></label>
				<input type="file" name="txtFile" required="1" class="form-control"> * File Format -> mp3
			</div>
		</div>	
		
        <div class="float-right">
	   <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
	   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
    </form>
<div class="clear"></div><br />
