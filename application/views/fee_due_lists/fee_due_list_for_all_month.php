<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<script>
		function printDiv(divName) {
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
		}
	</script>

	<style>
		#header{
			text-align:center;
			width:auto;

		}
		table{
			border-collapse: collapse;
		}
		th,td{
			font-size: 14px;
			padding: 4px;
			border: 1px solid black;
		}
		a:link, a:visited {
			background-color: #f44336;
			color: white;
			padding: 8px 8px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
		}

		.td_center{
			text-align: center;
		}
		.td_left{
			text-align: left;
		}
		.td_right{
			text-align: right;
		}

		a:hover, a:active {
			background-color: red;
		}
		.reortPrintOption{
			float: left;
			padding: 5px;
		}
	</style>

</head>


<body>

<div class="reortPrintOption">
	<a href="<?php echo base_url(); ?>fee_due_lists/student_fee_due_list">
		Back to Index
	</a>
	<a href="javascript:void" onclick="printDiv('printableArea')">
		Print
	</a>
</div>


<div id="printableArea">
	<?php
	//echo '<pre>';
	//print_r($due_list);
	if (isset($due_list)) {
		?>

		<?php
		$colspan_array = array();
		$head_colspan = 0;
		foreach ($months as $month):
			$colspan = 0;
			if (isset($due_list[0]['fee_list_'.$month])) {
				$colspan += count($due_list[0]['fee_list_' . $month]);
			}
			$head_colspan = $head_colspan + $colspan;
			$colspan_array[$month] = $colspan;
		endforeach; ?>

		<table width="100%" cellpadding="0" border="1" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">

			<tr>
				<th colspan="<?php echo $head_colspan + 3; ?>">
					<div id="header">
						<b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
						<b style="font-size:13px;">Class: <?php echo $HeaderInfo['ClassName']; ?>,
							Section: <?php echo $HeaderInfo['SectionName']; ?><br>
							Payment Due List
							for
							<?php
							foreach ($months as $month):
								echo DateTime::createFromFormat('!m', $month)->format('F').', ';
							endforeach; ?>
							<?php echo $HeaderInfo['year']; ?></b>

					</div>
				</th>
			</tr>

			<tr>
				<th style="vertical-align: middle;" scope="col" rowspan="3">SL</th>
				<th style="vertical-align: middle; max-width:130px;" scope="col" rowspan="3">Name</th>
				<th style="vertical-align: middle;" scope="col" rowspan="3">Roll</th>
				<?php
				// echo $colspan;
				foreach ($months as $month):
					?>
					<th style="text-align:center;" scope="col" colspan="<?php echo $colspan_array[$month]; ?>">
						<?php echo DateTime::createFromFormat('!m', $month)->format('F'); ?>
					</th>
				<?php endforeach; ?>
			</tr>
			<tr>
				<?php
				foreach ($months as $month):
					?>

					<?php
					if (isset($due_list[0]['fee_list_'.$month]) && !empty($due_list[0]['fee_list_'.$month])) {
						foreach ($due_list[0]['fee_list_'.$month] as $row):
							?>
							<th style="text-align:center;" width="130"   scope="col">
								<?php echo $row['sub_category_name']; ?>
							</th>
						<?php
						endforeach;
					} ?>
				<?php endforeach; ?>

			</tr>


			<tr>
				<?php
				foreach ($months as $month):
					?>
					<?php
					if (isset($due_list[0]['fee_list_'.$month]) && !empty($due_list[0]['fee_list_'.$month])) {
						foreach ($due_list[0]['fee_list_'.$month] as $row):
							?>
							<th scope="col">
								Allocated<br>
								Payment<br>
								Due
							</th>
						<?php
						endforeach;
					} ?>

				<?php endforeach; ?>
			</tr>

			</thead>
			<tbody>

			<?php
			$i = 0;
			$style = "text-align:right;";
			foreach ($due_list as $row):
				?>

				<tr>
					<td  style="vertical-align: middle; text-align:center;" width="34">
						<?php echo $i + 1; ?>
					</td>
					<td style="vertical-align: middle; padding-left:3px;"><?php echo $row['name']; ?></td>
					<td style="vertical-align: middle; text-align:center;"><?php echo $row['roll_no']; ?></td>

					<?php
					foreach ($months as $month):
						?>
						<?php
						if (isset($due_list[$i]['fee_list_'.$month]) && !empty($due_list[$i]['fee_list_'.$month])) {
							foreach ($due_list[$i]['fee_list_'.$month] as $annual_row):
								?>
								<td style="<?php echo $style; ?>">
									<?php echo $annual_row['actual_allocated_amount_for_this_student']; ?>&nbsp;<br>
									<?php echo $annual_row['already_paid_amount']; ?>&nbsp;<br>
									<b>
										<?php
										echo $annual_row['due_amount']; ?>
									</b>&nbsp;
								</td>
							<?php
							endforeach;
						} ?>
					<?php endforeach; ?>
				</tr>

				<?php $i++;
			endforeach; ?>



			</tbody>
		</table>

		<?php
	}
	?>
</div>
</body>
</html>
