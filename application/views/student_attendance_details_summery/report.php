<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
		}
	</style>

	<?php
	$student_code_width = 'width="70"';
	$student_name_width = 'width="170"';
} ?>




<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <td  class="h_td" style="text-align:center; line-height:1.5;" colspan="9">
			<b style="font-size: 20px;"><?php echo $HeaderInfo[0]['school_name']; ?></b><br>
			<?php echo $HeaderInfo[0]['address']; ?><br>
			<?php echo $title; ?></b>
        </td>
    </tr>

	<tr>
		<td class="h_td" colspan="9">
			<b>Class: <?php echo $class_name; ?></b>,&nbsp;
			<b>Shift: <?php echo $shift_name; ?></b>,&nbsp;
			<b>Section: <?php echo $section_name; ?></b>,&nbsp;
			<b>Group: <?php echo $group_name; ?></b>,&nbsp;
			<b>Period: <?php echo $period_name; ?></b>,&nbsp;
			<b>
				Date: <?php echo date("M jS, Y", strtotime($from_date)); ?> - <?php echo date("M jS, Y", strtotime($to_date)); ?>
			</b>
		</td>
	</tr>

	 <tr>
        <td scope="col" class="h_td" <?php if (isset($is_pdf) && $is_pdf == 1) { echo $student_code_width; } ?> align="center">&nbsp;<?php echo $this->lang->line('student_code'); ?></td>
		<td scope="col" class="h_td" <?php if (isset($is_pdf) && $is_pdf == 1) { echo $student_name_width; } ?>  style="min-width: 200px !important;">&nbsp;<?php echo $this->lang->line('name'); ?></td>
		<td scope="col" class="h_td">&nbsp;<?php echo $this->lang->line('roll'); ?></td>
		<td align="center" class="h_td" scope="col"><?php echo $this->lang->line('days'); ?></td>
		<td align="center" class="h_td"scope="col">&nbsp;<?php echo $this->lang->line('working'); ?> <?php echo $this->lang->line('days'); ?></td>
        <td align="center" class="h_td"scope="col">&nbsp;<?php echo $this->lang->line('present'); ?> <?php echo $this->lang->line('days'); ?></td>
        <td align="center" class="h_td"scope="col"> <?php echo $this->lang->line('holidays'); ?></td>
		<td align="center" class="h_td"scope="col"><?php echo $this->lang->line('leave'); ?></td>
		<td align="center" class="h_td"scope="col"> <?php echo $this->lang->line('absent'); ?></td>
    </tr>

     </thead>
	 
	  <tbody>
    <?php
    $i = 0;
    foreach ($adata as $row):
        $i++;
        ?>
        <tr>
			<td align="center">
				&nbsp;<?php echo $row['student_code']; ?><br>
			</td>
			<td align="left" style="min-width: 200px !important;">
				&nbsp;<?php echo $row['name']; ?><br>
			</td>
          <td align="center">
                &nbsp;<?php echo $row['roll_no']; ?><br>
		  </td>
           <td align="center">&nbsp;<?php echo $total_days; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_working_days']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_present_days']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_holidays']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_leave_days']; ?></td>
		   <td align="center">&nbsp;<?php echo $row['total_absent_days']; ?></td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
