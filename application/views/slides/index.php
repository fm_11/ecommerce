<script type="text/javascript">
    function msgStatusUpdate(id,is_view){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("is_view_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>slides/updateMsgStatusSlideImageStatus?id=" + id + '&&is_view=' + is_view, true);
        xmlhttp.send();
    }
</script>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
   <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">Photo Title</th>
        <th scope="col">Date</th>
        <th scope="col">Is View</th>
        <th scope="col">Photo</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>


    <?php
    $i = 0;
    foreach ($images as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td style="vertical-align:middle">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align:middle"><?php echo $row['title']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['date']; ?></td>
            <td style="vertical-align:middle" id="is_view_sction_<?php echo $row['id']; ?>">
                <?php
                if ($row['is_view'] == 1) {
                    ?>
                    <a class="deleteTag" title="Approve" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_view']; ?>)"><i class="ti-check-box"></i></a>
                <?php
                } else {
                    ?>
                    <a class="deleteTag" title="Reject" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_view']; ?>)"><i class="ti-na"></i></a>
                <?php
                }
                ?>
            </td>
            <td style="vertical-align:middle">
                <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/images/slide/<?php echo $row['photo_location'] ?>" class="img-lg rounded-circle mb-3">
            </td>
            <td>
              <div class="dropdown">
                  <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="ti-pencil-alt"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                      <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>slides/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                  </div>
              </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
