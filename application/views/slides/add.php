<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>slides/add" method="post" enctype="multipart/form-data">

      <div class="form-row">
        <div class="form-group col-md-4">
        <label>Title</label>
        <input type="text" autocomplete="off"  class="form-control" name="txtTitle" required="1"/>
        </div>
        <div class="form-group col-md-4">
        <label>Date</label>
			<div class="input-group date">
				<input type="text" autocomplete="off" value="<?php echo date('Y-m-d') ?>" required name="txtDate"  id="txtDate" class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
						 <i class="simple-icon-calendar"></i>
					 </span>
			</div>
        </div>
        <div class="form-group col-md-4">
        <label>Image</label>
        <input type="file" name="txtPhoto" required="1" class="form-control">
			<span style="font-size: 11px;">
				* File Format ->JPEG , JPG and PNG.<br>
				* Image Size -> Height : 306PX and Width : 940PX
			</span>
        </div>
      </div>


      <div class="form-row">
        <div class="form-group col-md-6">
        <label>Text 1</label>
        <input type="text" autocomplete="off"  class="form-control" name="text_1" required="1"/>
        </div>
        <div class="form-group col-md-6">
        <label>Text 2</label>
        <input type="text" autocomplete="off"  class="form-control" name="text_2" required="1"/>
        </div>
      </div>

		<div class="float-right">
			<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
			<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
		</div>
</form>
