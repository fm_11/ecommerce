<script>
    function getTemplateBody(template_id) {
        if (template_id == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //alert(xmlhttp.responseText);
                document.getElementById("template_body").value  = "";
                document.getElementById("template_body").value  = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>messages/getTemplateBodyById?template_id=" + template_id, true);
        xmlhttp.send();
    }


    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }

    function checkCheckBox() {
        var inputElems = document.getElementsByTagName("input"),
            count = 0;
        for (var i = 0; i < inputElems.length; i++) {
            if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
                count++;
            }
        }
        if (count < 1) {
            alert("Please select some student.");
            return false;
        } else {
            return true;
        }
    }
</script>



<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>phonebook_lists/view_category_phoneBook_list" method="post"
      enctype="multipart/form-data">
	  
	  <div class="form-row">
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('receiver') . ' ' . $this->lang->line('category'); ?></label>
			<select class="form-control" name="CategoryType" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
					foreach ($category as $list) {
				?>
						<option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
				<?php
					}
				?>
			</select>
		</div>
	</div>	
    <div class="float-right">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('process'); ?>">
    </div>
	
</form>

<div>
    <?php
    if (isset($CategoryInfo)) {
		if($CategoryInfo['status']==1){
		?>
		<form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()"  action="<?php echo base_url(); ?>messages/SendcategoryPhoneBookSMS" method="post"
			  enctype="multipart/form-data">
			<div class="table-responsive-sm">
			   <table  class="table"> 
			   <thead>
				<tr>
					<td colspan="2" style="text-align: right;">
						<?php echo $this->lang->line('message') . ' ' . $this->lang->line('template') . ' ' . $this->lang->line('body'); ?>
					</td>
					<td colspan="5">
						<select class="smallInput" name="template_id" onchange="getTemplateBody(this.value)"
								required="1">
							<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
							<?php
							$i = 0;
							if (count($templates)) {
								foreach ($templates as $list) {
									$i++;
									?>
									<option
											value="<?php echo $list['id']; ?>"><?php echo $list['template_name']; ?></option>
									<?php
								}
							}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="vertical-align: middle;text-align: right;">
						<?php echo $this->lang->line('message') . ' ' . $this->lang->line('body'); ?>
					</td>
					<td colspan="5">
						<textarea name="template_body" required rows="5" cols="35" id="template_body"></textarea>
					</td>
				</tr>
				<tr>
					<td colspan="7" style="text-align: right;">
						<input type="submit" class="submit" value="Send SMS">
					</td>
				</tr>
				<tr>
					<th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
					<th scope="col"><?php echo $this->lang->line('name'); ?></th>
					<th scope="col"><?php echo $this->lang->line('category'); ?></th>
					<!--<th width="110" scope="col">Father's Name</th>-->
					<th scope="col"><?php echo $this->lang->line('mobile') . ' ' . $this->lang->line('number'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php
				$i = 0;
				foreach ($CategoryInfo['CategoryData'] as $row):
					?>
					<tr>
						<td width="34">
							<input type="checkbox" name="is_send_<?php echo $i; ?>">
						</td>

						<td>
							<?php echo $row['contact_name']; ?>
							<input type="hidden" name="smsReciver_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>">
						</td>
						<td><?php echo $CategoryInfo['name']; ?></td>

						<td>
							<input type="text" autocomplete="off"  value="<?php echo $row['contact_number']; ?>" name="mobile_<?php echo $i; ?>">
						</td>
					</tr>
				<?php $i++; endforeach; ?>
				<tr>
					<td colspan="9" style="text-align: right;">
						 <input type="hidden" name="loop_time" value="<?php echo $i; ?>">
						 <input type="hidden" name="categoryId" value="<?php echo $CategoryInfo['category_id']; ?>">
						<input type="submit" class="submit" value="Send SMS">
					</td>
				</tr>
				</tbody>
			</table>
			</div>
			<br>
		</form><br/>
		<?php
		}else{
			echo "Can't found any ".$CategoryInfo['name']." Data";
		}
    }

	if (isset($teacherSMSStatus)) {
		if($teacherSMSStatus==1){
			if (isset($teacherInfoStatus)) {
				if($teacherInfoStatus==1){
				?>
				<form name="addForm" class="cmxform" id="commentForm"   onsubmit="return checkCheckBox()"  action="<?php echo base_url(); ?>messages/teacherBirthDaySMS" method="post"
					  enctype="multipart/form-data">
					<div class="table-responsive-sm">
					   <table  class="table"> 
					   <thead>
						<tr>
							<td colspan="2" style="text-align: right;">
								<?php echo $this->lang->line('message'). ' ' . $this->lang->line('template') . ' ' . $this->lang->line('name'); ?>
							</td>
							<td colspan="5">
								<select class="smallInput" name="template_id" onchange="getTemplateBody(this.value)"
										required="1">
									<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
									<?php
									$i = 0;
									if (count($templates)) {
										foreach ($templates as $list) {
											$i++;
											?>
											<option
													value="<?php echo $list['id']; ?>"><?php echo $list['template_name']; ?></option>
											<?php
										}
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="vertical-align: middle;text-align: right;">
								<?php echo $this->lang->line('message') . ' ' . $this->lang->line('body'); ?>
							</td>
							<td colspan="5">
								<textarea name="template_body" required rows="5" cols="35" id="template_body"></textarea>
							</td>
						</tr>
						<tr>
							<td colspan="7" style="text-align: right;">
								<input type="submit" class="submit" value="Send SMS">
							</td>
						</tr>
						<tr>
							<th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
							<th scope="col"><?php echo $this->lang->line('teacher_id'); ?></th>
							<th scope="col"><?php echo $this->lang->line('teacher') . ' ' . $this->lang->line('name'); ?></th>
							<th scope="col"><?php echo $this->lang->line('subject'); ?></th>
							<th scope="col"><?php echo $this->lang->line('teacher') . ' ' . $this->lang->line('gender'); ?></th>

							<th width="100" scope="col"><?php echo $this->lang->line('teacher') . ' ' . $this->lang->line('number'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php
						$i = 0;
						foreach ($teacherInfo as $row):
							?>
							<tr>
								<td width="34">
									<input type="checkbox" name="is_send_<?php echo $i; ?>">
								</td>
								<td><?php echo $row['id']; ?></td>
								<td>
									<?php echo $row['name']; ?>
									<input type="hidden" name="teacher_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>">
								</td>
								<td>
									<?php echo $row['subject']; ?>

								</td>
								<td>
									<?php
									if ($row['gender'] == 'M') {
										echo "Male";
									} else {
										echo "Female";
									}
									?>
								</td>

								<!--<td><?php //echo $row['father_name']; ?></td>-->
								<td>
									<input type="text" autocomplete="off"  value="<?php echo $row['mobile']; ?>" name="teacher_mobile_<?php echo $i; ?>">
								</td>
							</tr>
						<?php $i++; endforeach; ?>
						<tr>
							<td colspan="9" style="text-align: right;">
								 <input type="hidden" name="loop_time" value="<?php echo $i; ?>">
								<input type="hidden" name="birthDate" id="birthDate" value="<?php echo date('Y-m-d'); ?>">
								<input type="submit" class="submit" value="Send SMS">
							</td>
						</tr>
						</tbody>
					</table>
					</div>
					<br>
				</form><br/>
				<?php
				}else{
					echo "No one's Birthday is Today";
				}
			}
		}
    }
    ?>
</div>
<div class="clear"></div><br/>
