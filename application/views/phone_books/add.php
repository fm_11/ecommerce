
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>phone_books/add" method="post">

	<div  class="form-row">
		<div class="form-group col-md-6">
			<label for="PhoneBookCategory">
				<?php echo $this->lang->line('phone_book') . ' ' . $this->lang->line('category'); ?>
			</label>
			<select name="PhoneBookCategory" required="required" class="js-example-basic-single w-100" id="class_id">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php foreach ($categoryList as $row) { ?>
					<option value="<?php echo $row['id']; ?>" ><?php echo $row['name']; ?></option>

				<?php } ?>
			</select>
		</div>

	</div>


	<div class="form-row">

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('name'); ?></label>
			<input required type="text" name="ContactName" class="form-control" value="">
		</div>

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('number'); ?></label>
			<input required type="text" name="ContactNumber" class="form-control" value="">
		</div>

	</div>


	<div class="form-row">

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('organization') . ' (' . $this->lang->line('if_any'); ?>)</label>
			<input type="text" autocomplete="off"  name="working_place" class="form-control" value="">
		</div>

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('designation') . ' (' . $this->lang->line('if_any'); ?>)</label>
			<input type="text" autocomplete="off"  name="designation" class="form-control" value="">
		</div>
	</div>

		<div class="form-row">
			<div class="form-group col-md-6">
				<label><?php echo $this->lang->line('address'); ?></label>
				<textarea name="address" rows="5" class="form-control"></textarea>
			</div>
			<div class="form-group col-md-6">
				<label><?php echo $this->lang->line('remarks'); ?></label>
				<textarea name="remarks" rows="5" class="form-control"></textarea>
			</div>
		</div>

    <div class="float-right">
	   <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
	   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
