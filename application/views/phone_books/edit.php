<?php
    /* echo '<pre>';
    print_r($categoryList); */
?>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>phone_books/edit/<?php echo $this->uri->segment(3); ?>" method="post">

<div class="form-row">
	<div class="form-group col-md-6">
		<label for="PhoneBookCategory"><?php echo $this->lang->line('phone_book') . ' ' . $this->lang->line('category'); ?></label>
		<select name="PhoneBookCategory" required="required" class="js-example-basic-single w-100" id="class_id">
			<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
			<?php foreach ($categoryList as $row) { ?>
				<option value="<?php echo $row['id']; ?>" <?php if ($row['id'] == $EditData['category_id']) {
    echo 'selected';
}
                                     ?> ><?php echo $row['name']; ?></option>

			<?php } ?>
		</select>
	</div>
</div>


	<div class="form-row">
		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('name'); ?></label>
			<input required type="text" name="ContactName" class="form-control" value="<?php echo $EditData['contact_name'];?>">
		</div>

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('number'); ?></label>
			<input required type="text" name="ContactNumber" class="form-control" value="<?php echo $EditData['contact_number'];?>">
		</div>
	</div>




	<div class="form-row">

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('organization') . ' (' . $this->lang->line('if_any'); ?>)</label>
			<input type="text" autocomplete="off"  name="working_place" class="form-control" value="<?php echo $EditData['working_place'];?>">
		</div>

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('designation') . ' (' . $this->lang->line('if_any'); ?>)</label>
			<input type="text" autocomplete="off"  name="designation" class="form-control" value="<?php echo $EditData['designation'];?>">
		</div>
	</div>

		<div class="form-row">
			<div class="form-group col-md-6">
				<label><?php echo $this->lang->line('address'); ?></label>
				<textarea name="address" rows="5" class="form-control"><?php echo $EditData['address'];?></textarea>
			</div>
			<div class="form-group col-md-6">
				<label><?php echo $this->lang->line('remarks'); ?></label>
				<textarea name="remarks" rows="5" class="form-control"><?php echo $EditData['remarks'];?></textarea>
			</div>
		</div>

    <div class="float-right">
	   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
