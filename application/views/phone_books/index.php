
<div class="table-responsive-sm">
   <table  class="table">
    <thead>
    <tr>
        <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th scope="col"><?php echo $this->lang->line('name'); ?></th>
        <th scope="col"><?php echo $this->lang->line('designation'); ?></th>
        <th scope="col"><?php echo $this->lang->line('number'); ?></th>
        <th scope="col"><?php echo $this->lang->line('category'); ?></th>
        <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i =  (int) $this->uri->segment(3);
    foreach ($PhoneBookList as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['contact_name']; ?></td>
            <td><?php echo $row['designation']; ?></td>
            <td><?php echo $row['contact_number']; ?></td>
            <td><?php echo $row['category_name']; ?></td>
            <td>
              <div class="dropdown">
                  <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="ti-pencil-alt"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                      <a class="dropdown-item" href="<?php echo base_url(); ?>phone_books/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                      <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>phone_books/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                  </div>
              </div>

            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
