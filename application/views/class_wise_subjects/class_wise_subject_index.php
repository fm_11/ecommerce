<script>
	function setFromClassAndExamTypeValue(class_id,exam_type_id) {
		document.getElementById("from_class_id").value = class_id;
		document.getElementById("from_exam_type_id").value = exam_type_id;
	}

	function checkSelectedClassAndExamType(to_class_id,to_exam_type_id) {
		var from_class_id = document.getElementById("from_class_id").value;
		var from_exam_type_id = document.getElementById("from_exam_type_id").value;
		if(from_class_id == to_class_id && from_exam_type_id == to_exam_type_id){
			alert("Both class or exam type can't be same. Please select an another class and exam type");
			document.getElementById("to_class_id").value = "";
			document.getElementById("to_exam_type_id").value = "";
			return false;
		}

		//check assign data
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		}
		else
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				if(xmlhttp.responseText == '0'){
					alert("Already configured for this class and exam type");
					document.getElementById("to_class_id").value = "";
					document.getElementById("to_exam_type_id").value = "";
					return false;
				}else{
					return true;
				}
			}
		}
		xmlhttp.open("GET", "<?php echo base_url(); ?>class_wise_subjects/checkAlreadyConfiguredConfig?to_class_id=" + to_class_id + "&&to_exam_type_id=" + to_exam_type_id, true);
		xmlhttp.send();

	}
</script>



<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table id="sortable-table-1" class="table">
	<thead>
	  <tr>
		  <th width="200" scope="col">Exam Type</th>
		  <th scope="col">Subject</th>
		  <th width="120" scope="col">Actions</th>
	  </tr>
	  </thead>
	  <tbody>
	  <?php
	  foreach ($subject_info as $row):
		  ?>

		<tr>
		   <td colspan="3"><b><?php echo $row['class_name']; ?></b></td>
		</tr>


		<?php
		   foreach ($ExamTypeList as $exam_type_row) {
			   ?>
		  <tr>
			  <td><?php echo $row['exam_type_'.$exam_type_row['id']]['exam_type_name']; ?></td>
			  <td><?php echo $row['exam_type_'.$exam_type_row['id']]['subject_name']; ?></td>
			  <td>
				  <a href="<?php echo base_url(); ?>class_wise_subjects/edit/<?php echo $row['class_id']; ?>/<?php echo $exam_type_row['id']; ?>"
					class="btn btn-warning btn-xs mb-1" title="Edit">Edit</a>

				  <?php if($row['exam_type_'.$exam_type_row['id']]['subject_name'] != 'N/A'){ ?>
					  <button type="button" onclick="setFromClassAndExamTypeValue('<?php echo $row["class_id"]; ?>','<?php echo $exam_type_row["id"]; ?>')" class="btn btn-success btn-xs mb-1" data-toggle="modal" data-target="#exampleModal-2">
						  Copy
					  </button>
				  <?php } ?>

			  </td>
		  </tr>
		<?php
		   }
		  ?>
	  <?php endforeach; ?>
	  </tbody>
  </table>
</div>




<!-- Modal starts -->
<div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel-2">Copy Mark Configuration</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" action="<?php echo base_url(); ?>class_wise_subjects/data_copy">
				<div class="modal-body">
					<div class="form-group">
						<select onchange="checkSelectedClassAndExamType(this.value,document.getElementById('to_exam_type_id').value)" class="form-control" name="to_class_id" id="to_class_id" required="1">
							<option value="">-- Select Class --</option>
							<?php
							if (count($class_list)) {
								foreach ($class_list as $list) {
									?>
									<option value="<?php echo $list['id']; ?>">
										<?php echo $list['name']; ?>
									</option>
									<?php
								}
							} ?>
						</select>
						<input type="hidden" class="form-control" name="from_class_id" id="from_class_id">
						<input type="hidden" class="form-control" name="from_exam_type_id" id="from_exam_type_id">
					</div>
					<div class="form-group">
						<select  onchange="checkSelectedClassAndExamType(document.getElementById('to_class_id').value,this.value)"  class="form-control" name="to_exam_type_id" id="to_exam_type_id" required="1">
							<option value="">-- Select Exam Type --</option>
							<?php
							if (count($ExamTypeList)) {
								foreach ($ExamTypeList as $list) {
									?>
									<option value="<?php echo $list['id']; ?>">
										<?php echo $list['name']; ?>
									</option>
									<?php
								}
							} ?>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success">Copy</button>
					<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal Ends -->
