<script>
    function readonlyOnOff(value,id_sl,inputType){

    	if(inputType == 'ct'){
			var isCTCheckBoxChecked = document.getElementById("is_class_test_allow_for_main_head").checked;
			if(isCTCheckBoxChecked == false){
				alert("Please check <?php echo $main_marking_heads->class_test;  ?> checkbox for class test Allow");
				document.getElementById('is_class_test_allow_' + id_sl).value = "0";
				return false;
			}
		}

    	if(inputType == 'wr'){
			var isWRCheckBoxChecked = document.getElementById("is_written_allow_for_main_head").checked;
			if(isWRCheckBoxChecked == false){
				alert("Please check <?php echo $main_marking_heads->written;  ?> checkbox for written Allow");
				document.getElementById('is_written_allow_' + id_sl).value = "0";
				return false;
			}
		}

		if(inputType == 'obj'){
			var isOBJCheckBoxChecked = document.getElementById("is_objective_allow_for_main_head").checked;
			if(isOBJCheckBoxChecked == false){
				alert("Please check <?php echo $main_marking_heads->mcq;  ?> checkbox for objective Allow");
				document.getElementById('is_objective_allow_' + id_sl).value = "0";
				return false;
			}
		}

		if(inputType == 'prac'){
			var isPRACCheckBoxChecked = document.getElementById("is_practical_allow_for_main_head").checked;
			if(isPRACCheckBoxChecked == false){
				alert("Please check <?php echo $main_marking_heads->practical;  ?> checkbox for practical Allow");
				document.getElementById('is_practical_allow_' + id_sl).value = "0";
				return false;
			}
		}


        if(value == 0){
           if(inputType == 'ct'){
             document.getElementById('class_test_' + id_sl).setAttribute("readonly","readonly");
             document.getElementById('class_test_' + id_sl).value = "";

             document.getElementById('class_test_pass_marks_' + id_sl).setAttribute("readonly","readonly");
             document.getElementById('class_test_pass_marks_' + id_sl).value = "";
           }
           if(inputType == 'wr'){
             document.getElementById('written_' + id_sl).setAttribute("readonly","readonly");
             document.getElementById('written_' + id_sl).value = "";

             document.getElementById('written_pass_marks_' + id_sl).setAttribute("readonly","readonly");
             document.getElementById('written_pass_marks_' + id_sl).value = "";
           }
           if(inputType == 'obj'){
             document.getElementById('objective_' + id_sl).setAttribute("readonly","readonly");
             document.getElementById('objective_' + id_sl).value = "";

             document.getElementById('objective_pass_marks_' + id_sl).setAttribute("readonly","readonly");
             document.getElementById('objective_pass_marks_' + id_sl).value = "";
           }
           if(inputType == 'prac'){
             document.getElementById('practical_' + id_sl).setAttribute("readonly","readonly");
             document.getElementById('practical_' + id_sl).value = "";

             document.getElementById('practical_pass_marks_' + id_sl).setAttribute("readonly","readonly");
             document.getElementById('practical_pass_marks_' + id_sl).value = "";
           }
        }else{
          if(inputType == 'ct'){
            document.getElementById('class_test_' + id_sl).removeAttribute("readonly");
            document.getElementById('class_test_pass_marks_' + id_sl).removeAttribute("readonly");
          }
          if(inputType == 'wr'){
            document.getElementById('written_' + id_sl).removeAttribute("readonly");
            document.getElementById('written_pass_marks_' + id_sl).removeAttribute("readonly");
          }
          if(inputType == 'obj'){
            document.getElementById('objective_' + id_sl).removeAttribute("readonly");
            document.getElementById('objective_pass_marks_' + id_sl).removeAttribute("readonly");
          }
          if(inputType == 'prac'){
             document.getElementById('practical_' + id_sl).removeAttribute("readonly");
             document.getElementById('practical_pass_marks_' + id_sl).removeAttribute("readonly");
          }
        }
    }

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
            return true;
        } else if ( key < 48 || key > 57 ) {
            return false;
        } else {
           return true;
        }
   }

   function checkCreditVsNumber(row_num,typing_id){
        var credit = document.getElementById("credit_" + row_num).value;

        var is_class_test_allow = document.getElementById("is_class_test_allow_" + row_num).value;
        var class_test = Number(document.getElementById("class_test_" + row_num).value);
	    var acceptance_class_test = Number(document.getElementById("acceptance_class_test_" + row_num).value);

        var is_written_allow = document.getElementById("is_written_allow_" + row_num).value;
        var written = Number(document.getElementById("written_" + row_num).value);
	    var acceptance_written = Number(document.getElementById("acceptance_written_" + row_num).value);

        var is_objective_allow = document.getElementById("is_objective_allow_" + row_num).value;
        var objective = Number(document.getElementById("objective_" + row_num).value);
	    var acceptance_objective = Number(document.getElementById("acceptance_objective_" + row_num).value);

        var is_practical_allow = document.getElementById("is_practical_allow_" + row_num).value;
        var practical = Number(document.getElementById("practical_" + row_num).value);
	    var acceptance_practical = Number(document.getElementById("acceptance_practical_" + row_num).value);

        var total_number = 0;
        if(is_class_test_allow == 1){
           total_number = total_number + (class_test * acceptance_class_test) / 100;
        }
        if(is_written_allow == 1){
           total_number = total_number + (written * acceptance_written) / 100;
        }
        if(is_objective_allow == 1){
           total_number = total_number + (objective * acceptance_objective) / 100;
        }
        if(is_practical_allow == 1){
           total_number = total_number + (practical * acceptance_practical) / 100;
        }
        //alert(total_number);
        if(total_number > credit){
            alert("Adding all the numbers can not be more than " + credit);
            document.getElementById(typing_id).value = "";
            return false;
        }else{
            return true;
        }
   }

</script>

<style>
.cls_custom_input{
  padding: 5px;
  max-width: 80px;
  min-width: 80px;
}
th{
  vertical-align: middle !important;
}
</style>

<form name="addForm" class="cmxform" id="commentForm"
      action="<?php echo base_url(); ?>class_wise_subjects/edit/<?php echo $this->uri->segment(3); ?>/<?php echo $this->uri->segment(4); ?>"
      method="post">
      <div class="table-sorter-wrapper col-lg-12 table-responsive">
        <table id="sortable-table-1" class="table">
      <thead>
        <tr>
            <th>
                Subject
            </th>

            <th>
                Select
            </th>
            <th>
                 Credit
            </th>

			<th style="text-align: center;">
			 <?php echo $main_marking_heads->class_test;  ?>
				<br>
				<input name="is_class_test_allow_for_main_head" id="is_class_test_allow_for_main_head" type="checkbox"
					<?php if ($class_wise_marking_heads['is_class_test_allow'] == '1') { ?> checked="checked" <?php } ?>
					   value="1">
			</th>
			<th style="text-align: center;">
			 <?php echo $main_marking_heads->written;  ?>
				<br>
				<input name="is_written_allow_for_main_head" type="checkbox" id="is_written_allow_for_main_head"
					<?php if ($class_wise_marking_heads['is_written_allow'] == '1') { ?> checked="checked" <?php } ?>
					   value="1">
			</th>
			<th style="text-align: center;">
			 <?php echo $main_marking_heads->mcq;  ?>
				<br>
				<input name="is_objective_allow_for_main_head" type="checkbox" id="is_objective_allow_for_main_head"
					<?php if ($class_wise_marking_heads['is_objective_allow'] == '1') { ?> checked="checked" <?php } ?>
					   value="1">
			</th>
			  <th style="text-align: center;">
			 <?php echo $main_marking_heads->practical;  ?>
				  <br>
				  <input name="is_practical_allow_for_main_head" type="checkbox" id="is_practical_allow_for_main_head"
					  <?php if ($class_wise_marking_heads['is_practical_allow'] == '1') { ?> checked="checked" <?php } ?>
						 value="1">
			</th>

            <th>
                 Groups
            </th>
            <th>
                 Order
            </th>
            <th>
                 Merge Code
            </th>
        </tr>
      </thead>

        <?php for ($i = 0; $i < count($subject); $i++) { ?>
        <tr>
            <td style="min-width:110px;">
                <?php echo $subject[$i]['name']; ?> (<?php echo $subject[$i]['code']; ?>)
             </td>
             <td>
	              	<input name="is_allow_<?php echo $i; ?>"
                       type="checkbox" <?php if ($subject[$i]['is_checked'] == '1') { ?> checked="checked" <?php } ?>
                       value="<?php echo $subject[$i]['id']; ?>">

                      <input name="subject_id_<?php echo $i; ?>"
                       type="hidden" value="<?php echo $subject[$i]['id']; ?>">

            </td>
            <td class="center_td" >
				      <select class="cls_custom_input" id="credit_<?php echo $i; ?>" name="credit_<?php echo $i; ?>">
                    <?php
                       $crdit_len = 100;
                       while ($crdit_len > 9) {
                           ?>
                         <option <?php if ($subject[$i]['credit'] == $crdit_len) { ?> selected="selected" <?php } ?>
                            value="<?php echo $crdit_len; ?>">
                           <?php echo $crdit_len; ?>
                         </option>
                     <?php
                         $crdit_len--;
                       }
                     ?>
             </select>
				</td>


				<td>
			          <select class="cls_custom_input"  onchange="readonlyOnOff(this.value,'<?php echo $i; ?>','ct')" id="is_class_test_allow_<?php echo $i; ?>" name="isClassTestAllow_<?php echo $i; ?>">
			     	        <option value="0" <?php if ($subject[$i]['is_class_test_allow'] == '0') {
    echo 'selected';
} ?>>No</option>
			     	        <option value="1" <?php if ($subject[$i]['is_class_test_allow'] == '1') {
    echo 'selected';
} ?>>Yes</option>
                      </select>

				      <input  name="class_test_<?php echo $i; ?>" onkeypress="javascript:return validateNumber(event)" onkeyup="return checkCreditVsNumber(<?php echo $i; ?>,'class_test_<?php echo $i; ?>')" id="class_test_<?php echo $i; ?>"
              <?php if ($subject[$i]['is_class_test_allow'] == ''
               || $subject[$i]['is_class_test_allow'] == '0') {
                    echo 'readonly';
                } ?>
                class="cls_custom_input" type="text" value="<?php echo $subject[$i]['class_test']; ?>">

          <input  name="class_test_pass_marks_<?php echo $i; ?>"  id="class_test_pass_marks_<?php echo $i; ?>" <?php if ($subject[$i]['is_class_test_allow'] == ''
           || $subject[$i]['is_class_test_allow'] == '0') {
                echo 'readonly';
            } ?>  class="cls_custom_input" type="text"
          value="<?php if($subject[$i]['class_test_pass_marks'] == ''){echo '0';}else{ echo $subject[$i]['class_test_pass_marks']; } ?>">


				<select class="cls_custom_input" id="acceptance_class_test_<?php echo $i; ?>" name="acceptance_class_test_<?php echo $i; ?>">
					<?php
					$input_len = 100;
					while ($input_len >= 10) {
						?>

						<option <?php if ($subject[$i]['acceptance_class_test'] == $input_len) { ?> selected="selected" <?php } ?> value="<?php echo $input_len; ?>"><?php echo $input_len; ?></option>

						<?php
						$input_len = $input_len - 1;
					}
					?>
				</select>


				</td>

				<td>
				      <select  class="cls_custom_input" onchange="readonlyOnOff(this.value,'<?php echo $i; ?>','wr')" id="is_written_allow_<?php echo $i; ?>" name="isWrittenAllow_<?php echo $i; ?>">
			     	        <option value="0" <?php if ($subject[$i]['is_written_allow'] == '0') {
    echo 'selected';
} ?>>No</option>
			     	        <option value="1" <?php if ($subject[$i]['is_written_allow'] == '1') {
    echo 'selected';
} ?>>Yes</option>
                      </select>
				      <input  name="written_<?php echo $i; ?>" onkeypress="javascript:return validateNumber(event)" onkeyup="return checkCreditVsNumber(<?php echo $i; ?>,'written_<?php echo $i; ?>')" id="written_<?php echo $i; ?>" type="text" <?php if ($subject[$i]['is_written_allow'] == '' || $subject[$i]['is_written_allow'] == '0') {
    echo 'readonly';
} ?> class="cls_custom_input" value="<?php echo $subject[$i]['written']; ?>">

<input  name="written_pass_marks_<?php echo $i; ?>" id="written_pass_marks_<?php echo $i; ?>"  <?php if ($subject[$i]['is_written_allow'] == ''
 || $subject[$i]['is_written_allow'] == '0') {
      echo 'readonly';
  } ?>   class="cls_custom_input" type="text"
value="<?php if($subject[$i]['written_pass_marks'] == ''){echo '0';}else{ echo $subject[$i]['written_pass_marks']; } ?>">

					<select class="cls_custom_input" id="acceptance_written_<?php echo $i; ?>" name="acceptance_written_<?php echo $i; ?>">
						<?php
						$input_len = 100;
						while ($input_len >= 10) {
							?>

							<option <?php if ($subject[$i]['acceptance_written'] == $input_len) { ?> selected="selected" <?php } ?> value="<?php echo $input_len; ?>"><?php echo $input_len; ?></option>

							<?php
							$input_len = $input_len - 1;
						}
						?>
					</select>

				</td>
				<td>
				     <select  class="cls_custom_input" onchange="readonlyOnOff(this.value,'<?php echo $i; ?>','obj')" id="is_objective_allow_<?php echo $i; ?>" name="isObjectiveAllow_<?php echo $i; ?>">
			     	       <option value="0" <?php if ($subject[$i]['is_objective_allow'] == '0') {
    echo 'selected';
} ?>>No</option>
			     	       <option value="1" <?php if ($subject[$i]['is_objective_allow'] == '1') {
    echo 'selected';
} ?>>Yes</option>
                      </select>
				     <input  name="objective_<?php echo $i; ?>" onkeypress="javascript:return validateNumber(event)"  onkeyup="return checkCreditVsNumber(<?php echo $i; ?>,'objective_<?php echo $i; ?>')" id="objective_<?php echo $i; ?>" type="text" <?php if ($subject[$i]['is_objective_allow'] == '' || $subject[$i]['is_objective_allow'] == '0') {
    echo 'readonly';
} ?>  size="10" class="cls_custom_input" value="<?php echo $subject[$i]['objective']; ?>">

<input  name="objective_pass_marks_<?php echo $i; ?>" id="objective_pass_marks_<?php echo $i; ?>" <?php if ($subject[$i]['is_objective_allow'] == ''
 || $subject[$i]['is_objective_allow'] == '0') {
      echo 'readonly';
  } ?>  class="cls_custom_input" type="text"
value="<?php if($subject[$i]['objective_pass_marks'] == ''){echo '0';}else{ echo $subject[$i]['objective_pass_marks']; } ?>">

					<select class="cls_custom_input" id="acceptance_objective_<?php echo $i; ?>" name="acceptance_objective_<?php echo $i; ?>">
						<?php
						$input_len = 100;
						while ($input_len >= 10) {
							?>

							<option <?php if ($subject[$i]['acceptance_objective'] == $input_len) { ?> selected="selected" <?php } ?> value="<?php echo $input_len; ?>"><?php echo $input_len; ?></option>

							<?php
							$input_len = $input_len - 1;
						}
						?>
					</select>


				</td>
				<td >
				    <select  class="cls_custom_input" onchange="readonlyOnOff(this.value,'<?php echo $i; ?>','prac')" id="is_practical_allow_<?php echo $i; ?>" name="isPracticalAllow_<?php echo $i; ?>">
			     	       <option value="0" <?php if ($subject[$i]['is_practical_allow'] == '0') {
    echo 'selected';
} ?>>No</option>
			     	       <option value="1" <?php if ($subject[$i]['is_practical_allow'] == '1') {
    echo 'selected';
} ?>>Yes</option>
            </select>
			     	<input  name="practical_<?php echo $i; ?>" onkeypress="javascript:return validateNumber(event)"  onkeyup="return checkCreditVsNumber(<?php echo $i; ?>,'practical_<?php echo $i; ?>')" id="practical_<?php echo $i; ?>" type="text" <?php if ($subject[$i]['is_practical_allow'] == '' || $subject[$i]['is_practical_allow'] == '0') {
    echo 'readonly';
} ?> size="10" class="cls_custom_input" value="<?php echo $subject[$i]['practical']; ?>">

<input  name="practical_pass_marks_<?php echo $i; ?>" id="practical_pass_marks_<?php echo $i; ?>" <?php if ($subject[$i]['is_practical_allow'] == ''
 || $subject[$i]['is_practical_allow'] == '0') {
      echo 'readonly';
  } ?>  class="cls_custom_input" type="text"
value="<?php if($subject[$i]['practical_pass_marks'] == ''){echo '0';}else{ echo $subject[$i]['practical_pass_marks']; } ?>">

					<select class="cls_custom_input" id="acceptance_practical_<?php echo $i; ?>" name="acceptance_practical_<?php echo $i; ?>">
						<?php
						$input_len = 100;
						while ($input_len >= 10) {
							?>

							<option <?php if ($subject[$i]['acceptance_practical'] == $input_len) { ?> selected="selected" <?php } ?> value="<?php echo $input_len; ?>"><?php echo $input_len; ?></option>

							<?php
							$input_len = $input_len - 1;
						}
						?>
					</select>

				</td>


				<td>
				    <select id="multi_groups" style="width:150px !important;" name="groups_<?php echo $i; ?>[]" multiple>
                        <?php
						$selected_groups = $subject[$i]['mapping_group_list'];
						if($subject[$i]['group_list'] != ''){
							$selected_groups = $subject[$i]['group_list'];
						}
						for ($C = 0; $C < count($groups); $C++) {
							?>
                         <option value="<?php echo $groups[$C]['id']; ?>" <?php if (in_array($groups[$C]['id'], explode(",", $selected_groups))) { ?> selected="selected" <?php } ?>><?php echo $groups[$C]['name']; ?></option>
                        <?php } ?>
                    </select>
				</td>

				<td>
			     	<select class="cls_custom_input"  style="min-width:50px;" name="orderNumber_<?php echo $i; ?>">

			     	   <?php
                          $order_len = 1;
                          while ($order_len <= 20) {
                              ?>

				       <option <?php if ($subject[$i]['order_number'] == $order_len) { ?> selected="selected" <?php } ?> value="<?php echo $order_len; ?>"><?php echo $order_len; ?></option>

                        <?php
                            $order_len++;
                          }
                        ?>
                    </select>
				</td>

        <td>
			<select class="cls_custom_input"  style="min-width:50px;" name="merge_code_<?php echo $i; ?>">
                <?php
                $merge_code_len = 0;
                while ($merge_code_len <= 20) {
                ?>
                <option <?php if ($subject[$i]['merge_code'] == $merge_code_len) { ?> selected="selected" <?php } ?> value="<?php echo $merge_code_len; ?>"><?php echo $merge_code_len; ?></option>
                <?php
                $merge_code_len++;
                }
                ?>
            </select>


			<input type="hidden" name="subject_type_<?php echo $i; ?>" value="<?php echo $subject[$i]['subject_type']; ?>">

		</td>


            </tr>
        <?php } ?>
    </table>
  </div>

      <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
       <div class="float-right">
          <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
       </div>
</form>
