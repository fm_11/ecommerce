
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>customers/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Name<span style="color:red">*</span></label>
      <input type="text" class="form-control" name="name" value="<?php echo $customers[0]['name']; ?>" required/>
    </div>
    <div class="form-group col-md-6">
      <label>Address<span style="color:red">*</span></label>
      <input type="text" class="form-control" name="address" value="<?php echo $customers[0]['address']; ?>" required/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Phone<span style="color:red">*</span></label>
      <input type="number" class="form-control" name="phone" value="<?php echo $customers[0]['phone']; ?>" required/>
    </div>
    <div class="form-group col-md-4">
      <label>Email<span style="color:red">*</span></label>
      <input type="email" class="form-control" name="email" value="<?php echo $customers[0]['email']; ?>" required/>
    </div>
    <div class="form-group col-md-4">
      <label>Password<span style="color:red">*</span></label>
      <input type="password" class="form-control" name="password" value="<?php echo $customers[0]['password']; ?>" required/>
    </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $customers[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
