<script>

	function val_educational_qualification()
	{
		var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        var checkedOne = Array.prototype.slice.call(checkboxes).some(x => x.checked);
		if (checkedOne)
		{
		  return true;
		}
		else
		{
		  alert ("Please select a educational qualification");
		  return false;
		}
	}

</script>

<style>
	.required_label {
		color:red;
	}
</style>

    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teachers/edit" onsubmit="return val_educational_qualification()" method="post"
          enctype="multipart/form-data">

          <div class="form-row">

			  <div class="form-group col-md-2">
				  <label>Teacher ID&nbsp;<span class="required_label">*</span></label>
				  <input type="text" autocomplete="off" value="<?php echo $teacher_info[0]['teacher_code']; ?>" readonly class="form-control"/>
			  </div>

			  <div class="form-group col-md-3">
				  <label><?php echo $this->lang->line('category'); ?>&nbsp;<span class="required_label">*</span></label>
				  <select class="js-example-basic-single w-100" name="category_id" required="1">
					  <option value="">-- Please Select --</option>
					  <?php
					  $i = 0;
					  if (count($categories)) {
						  foreach ($categories as $list) {
							  $i++; ?>
							  <option
									  value="<?php echo $list['id']; ?>" <?php if ($teacher_info[0]['category_id'] == $list['id']) {
								  echo 'selected';
							  } ?>><?php echo $list['name']; ?></option>
							  <?php
						  }
					  } ?>
				  </select>
			  </div>
            <div class="form-group col-md-4">
                <label><?php echo $this->lang->line('name'); ?>&nbsp;<span class="required_label">*</span></label>
                <input type="text" autocomplete="off"  value="<?php echo $teacher_info[0]['name']; ?>" class="form-control" name="txtName" required="1"/>
            </div>

			  <div class="form-group col-md-3">
				  <label>Gender&nbsp;<span class="required_label">*</span></label>
				  <div class="clearfix"></div>
				  <div class="custom-control custom-radio custom-control-inline">
					  <input type="radio"  <?php if ($teacher_info[0]['gender'] == 'M') {
						  echo 'checked';
					  } ?> class="custom-control-input" value="M" checked name="txtGender" required/><label class="custom-control-label">Male</label>
				  </div>
				  <div class="custom-control custom-radio custom-control-inline">
					  <input type="radio"  <?php if ($teacher_info[0]['gender'] == 'F') {
						  echo 'checked';
					  } ?>  class="custom-control-input" value="F" name="txtGender"/><label class="custom-control-label">Female</label>
				  </div>
			  </div>
          </div>

		<div class="form-row">
			<div class="form-group col-md-4">
				<label>Father's Name&nbsp;<span class="required_label">*</span></label>
				<input type="text" autocomplete="off" required name="txtFathersName" value="<?php echo $teacher_info[0]['fathers_name']; ?>" class="form-control">
			</div>
			<div class="form-group col-md-4">
				<label>Mother's Name&nbsp;<span class="required_label">*</span></label>
				<input type="text" autocomplete="off" required name="txtMothersName"  value="<?php echo $teacher_info[0]['mothers_name']; ?>" class="form-control">
			</div>
			<div class="form-group col-md-4">
				<label>Mobile&nbsp;<span class="required_label">*</span></label>
				<input type="text" autocomplete="off"  name="txtMobile"  value="<?php echo $teacher_info[0]['mobile']; ?>" class="form-control" required="1">
			</div>
		</div>

		<div class="form-row">
			<div class="form-group col-md-3">
				<label><?php echo $this->lang->line('designation'); ?>&nbsp;<span class="required_label">*</span></label>
				<select class="js-example-basic-single w-100" name="txtPost" required="1">
					<option value="">-- Please Select --</option>
					<?php
					$i = 0;
					if (count($post)) {
						foreach ($post as $list) {
							$i++; ?>
							<option
									value="<?php echo $list['id']; ?>" <?php if ($teacher_info[0]['post'] == $list['id']) {
								echo 'selected';
							} ?>><?php echo $list['name']; ?></option>
							<?php
						}
					} ?>
				</select>
			</div>

			<div class="form-group col-md-3">
				<label>Religion&nbsp;<span class="required_label">*</span></label>
				<select class="js-example-basic-single w-100" name="txtReligion" required="1">
					<option value="">-- Please Select --</option>
					<?php
					$i = 0;
					if (count($religions)) {
						foreach ($religions as $list) {
							$i++; ?>
							<option value="<?php echo $list['id']; ?>" <?php if ($teacher_info[0]['religion'] == $list['id']) {
								echo 'selected';
							} ?>><?php echo $list['name']; ?></option>
							<?php
						}
					} ?>
				</select>
			</div>

			<div class="form-group col-md-3">
				<label>Teacher Section&nbsp;<span class="required_label">*</span></label>
				<select class="js-example-basic-single w-100" name="txtSection" required="1">
					<option value="">-- Please Select --</option>
					<?php
					$i = 0;
					if (count($sections)) {
						foreach ($sections as $list) {
							$i++; ?>
							<option
									value="<?php echo $list['id']; ?>" <?php if ($teacher_info[0]['section'] == $list['id']) {
								echo 'selected';
							} ?>><?php echo $list['name']; ?></option>
							<?php
						}
					} ?>
				</select>
			</div>

			<div class="form-group col-md-3">
				<label>Is Show in Dashboard ?&nbsp;<span class="required_label">*</span></label>
				<select class="js-example-basic-single w-100" name="is_dashboard_show" required="1">
					<option value="0" <?php if($teacher_info[0]['is_dashboard_show'] == '0'){ echo 'selected'; } ?>>No</option>
					<option value="1" <?php if($teacher_info[0]['is_dashboard_show'] == '1'){ echo 'selected'; } ?>>Yes</option>
				</select>
			</div>

		</div>


		<div class="form-row">
			<div class="form-group col-md-6">
				<label>Present Address</label>
				<textarea id="wysiwyg" class="form-control" rows="7" cols="30" name="txtPresentAddress"><?php echo $teacher_info[0]['present_address']; ?></textarea>
			</div>
			<div class="form-group col-md-6">
				<label>Permanent Address</label>
				<textarea id="wysiwyg" class="form-control" rows="7" cols="30" name="txtPermanentAddress"><?php echo $teacher_info[0]['permanent_address']; ?></textarea>
			</div>
		</div>

          <div class="form-row">
			  <div class="form-group col-md-3">
				  <label>Teacher Index Number</label>
				  <input type="text" autocomplete="off"  value="<?php echo $teacher_info[0]['teacher_index_no']; ?>" class="form-control" name="txtIndexNum"/>
			  </div>

			  <div class="form-group col-md-3">
				  <label>Pay Code</label>
				  <select class="js-example-basic-single w-100" name="txtPayCode">
					  <option value="">-- Please Select --</option>
					  <?php
					  $i = 0;
					  if (count($pay_codes)) {
						  foreach ($pay_codes as $list) {
							  $i++; ?>
							  <option
									  value="<?php echo $list['id']; ?>" <?php if ($teacher_info[0]['pay_code_id'] == $list['id']) {
								  echo 'selected';
							  } ?>><?php echo $list['name']; ?></option>
							  <?php
						  }
					  } ?>
				  </select>
			  </div>

			  <div class="form-group col-md-3">
				  <label>Salary Bank Name</label>
				  <input type="text" autocomplete="off"  name="txtBankName" value="<?php echo $teacher_info[0]['bank_name']; ?>" class="form-control">
			  </div>
			  <div class="form-group col-md-3">
				  <label>Salary Bank Account</label>
				  <input type="text" autocomplete="off"  name="txtBankAcc" value="<?php echo $teacher_info[0]['bank_acc']; ?>" class="form-control">
			  </div>

          </div>

          <div class="form-row">

			  <div class="form-group col-md-3">
				  <label>Subject</label>
				  <input type="text" autocomplete="off"   value="<?php echo $teacher_info[0]['subject']; ?>" name="txtSubject" class="form-control">
			  </div>
			  <div class="form-group col-md-3">
				  <label>Date of Joining</label>
				  <div class="input-group date">
					  <input type="text" autocomplete="off"  name="txtDOJ" value="<?php echo $teacher_info[0]['date_of_join']; ?>" id="txtDOJ" class="form-control">
					  <span class="input-group-text input-group-append input-group-addon">
                         <i class="simple-icon-calendar"></i>
                     </span>
				  </div>

			  </div>
			  <div class="form-group col-md-3">
				  <label>Date of Birth</label>
				  <div class="input-group date">
					  <input type="text" autocomplete="off"  name="txtDOB"  value="<?php echo $teacher_info[0]['date_of_birth']; ?>" id="txtDOB" class="form-control">
					  <span class="input-group-text input-group-append input-group-addon">
                         <i class="simple-icon-calendar"></i>
                     </span>
				  </div>
			  </div>

			  <div class="form-group col-md-3">
				  <label>Blood Group</label>
				  <select class="js-example-basic-single w-100" name="blood_group_id">
					  <option value="">-- Please Select --</option>
					  <?php
					  $i = 0;
					  if (count($blood_group_list)) {
						  foreach ($blood_group_list as $list) {
							  $i++; ?>
							  <option
									  value="<?php echo $list['id']; ?>" <?php if ($teacher_info[0]['blood_group_id'] == $list['id']) {
								  echo 'selected';
							  } ?>><?php echo $list['name']; ?></option>
							  <?php
						  }
					  } ?>
				  </select>
			  </div>

          </div>

		<div class="form-row">
			<div class="form-group col-md-3">
				<label>Email</label>
				<input type="text" autocomplete="off"  value="<?php echo $teacher_info[0]['email']; ?>"  name="txtEmail" class="form-control">
			</div>

			<div class="form-group col-md-3">
				<label>Is Permanent&nbsp;<span class="required_label">*</span></label>
				<select class="js-example-basic-single w-100" name="txtIsPermanent" required="1">
					<option value="P" <?php if ($teacher_info[0]['is_permanent'] == 'P') {
						echo 'selected';
					} ?>>Permanent</option>
					<option value="PT" <?php if ($teacher_info[0]['is_permanent'] == 'PT') {
						echo 'selected';
					} ?>>Per Time</option>
				</select>
			</div>

			<div class="form-group col-md-3">
				<label>Process Code</label>
				<input type="text" autocomplete="off"  value="<?php echo $teacher_info[0]['process_code']; ?>" class="form-control" name="process_code"/>
			</div>
			<div class="form-group col-md-3">
				<label>National ID NO.</label>
				<input type="text" autocomplete="off"  value="<?php echo $teacher_info[0]['national_id']; ?>" name="txtNID" class="form-control">
			</div>

		</div>





          <div class="form-row">
			  <div class="form-group col-md-4">
				  <label>Shift&nbsp;<span class="required_label">*</span></label>
				  <div class="clearfix"></div>
				  <?php
				  $shift_sl = 0;
				  foreach ($shift as $row):
					  ?>
					  <div class="custom-control custom-checkbox custom-control-inline">
						  <input type="checkbox" class="custom-control-input" id="txtShift_<?php echo $shift_sl; ?>" name="txtShift[]"  <?php if ($row['shift_id'] != '') { ?> checked <?php } ?> value="<?php echo $row['id']; ?>">
						  <label for="txtShift_<?php echo $shift_sl; ?>" class="custom-control-label">
							  <?php echo $row['name']; ?>
						  </label>
					  </div>
					  <?php $shift_sl++; endforeach; ?>
			  </div>

			  <div class="form-group col-md-4">
				  <label for="EQ">Educational Qualification&nbsp;<span class="required_label">*</span></label>
				  <div class="clearfix"></div>
				  <?php
				  $edu_sl = 0;
				  foreach ($educational_qualification as $row):
					  ?>
					  <div class="custom-control custom-checkbox custom-control-inline">
						  <input type="checkbox" <?php if ($row['qua_id'] != '') { ?> checked <?php } ?> class="custom-control-input" id="txtEduQua_<?php echo $edu_sl; ?>" name="txtEduQua[]" value="<?php echo $row['id']; ?>">
						  <label for="txtEduQua_<?php echo $edu_sl; ?>" class="custom-control-label">
							  <?php echo $row['name']; ?>
						  </label>
					  </div>
					  <?php $edu_sl++;
				  endforeach; ?>
			  </div>

            <div class="form-group col-md-4">
              <label>Photo</label>
              <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/teacher/<?php echo $teacher_info[0]['photo_location']; ?>"
                  height="100" width="90" style=" border:1px solid #666666;">
              <input type="file" name="txtPhoto" class="form-control-file"> * Image Format -> JPEG , JPG and PNG.
            </div>
          </div>

          <div class="float-right">
            <input type="hidden" name="id" value="<?php echo $teacher_info[0]['id']; ?>" class="form-control">
             <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
          </div>
    </form>
