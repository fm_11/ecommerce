
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>inventory_warehouse/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('warehouse').' '.$this->lang->line('name') ?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $warehouse[0]['name']; ?>" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('code')?></label> <span class="required_label">*</span>
      <input type="text" autocomplete="off"  class="form-control" name="location" value="<?php echo $warehouse[0]['location']; ?>" required="1"/>
    </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $warehouse[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
