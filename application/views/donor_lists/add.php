<script>
	function setPermanentAddress() {
		var isChecked = document.getElementById("same_as_present").checked;
		if(isChecked == true){
			document.getElementById("permanent_address").value = document.getElementById("present_address").value;
			document.getElementById("permanent_ward").value = document.getElementById("present_ward").value;
			document.getElementById("permanent_post_office").value = document.getElementById("present_post_office").value;
			document.getElementById("permanent_thana").value = document.getElementById("present_thana").value;
			document.getElementById("permanent_district").value = document.getElementById("present_district").value;
		}else{
			document.getElementById("permanent_address").value = "";
			document.getElementById("permanent_ward").value = "";
			document.getElementById("permanent_post_office").value = "";
			document.getElementById("permanent_thana").value =  "";
			document.getElementById("permanent_district").value =  "";
		}
	}
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>donor_lists/add" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Member Name&nbsp;<span class="required_label">*</span></label>
      <input type="text"  class="form-control" name="name" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label>Father/Spouse Name&nbsp;<span class="required_label">*</span></label>
      <input type="text"  class="form-control" name="father_spouse_name" required="1"/>
    </div>
  </div>

	<div class="form-row">
		<div class="form-group col-md-3">
			<label >Donation Type&nbsp;<span class="required_label">*</span></label>
			<select class="js-example-basic-single w-100" name="donation_type">
				<option value="M">Monthly</option>
				<option value="Y">Yearly</option>
				<option value="O">Other</option>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Donation Amount&nbsp;<span class="required_label">*</span></label>
			<input type="text"  class="form-control" name="amount" required="1"/>
		</div>
		<div class="form-group col-md-3">
			<label>Mobile 1&nbsp;<span class="required_label">*</span></label>
			<input type="text"  class="form-control" name="mobile1" required="1"/>
		</div>
		<div class="form-group col-md-3">
			<label>Mobile 2</label>
			<input type="text"  class="form-control" name="mobile2"/>
		</div>
	</div>


  <span><b>Present Address</b></span>
	<hr>
  <div class="form-row">
    <div class="form-group col-md-12">
      <label>House No. / Village&nbsp;<span class="required_label">*</span></label>
      <input type="text"  class="form-control" id="present_address" name="present_address" required="1"/>
    </div>
  </div>

  <div class="form-row">
	    <div class="form-group col-md-3">
		  <label>Ward No.</label>
		  <input type="text" id="present_ward"  class="form-control" name="present_ward" />
	    </div>
		<div class="form-group col-md-3">
		  <label>Post Office</label>
		  <input type="text" id="present_post_office"  class="form-control" name="present_post_office"/>
		</div>
		<div class="form-group col-md-3">
		  <label>Thana/Upazilla</label>
		  <input type="text" id="present_thana"  class="form-control" name="present_thana"/>
		</div>
		  <div class="form-group col-md-3">
			  <label>District&nbsp;<span class="required_label">*</span></label>
			  <input type="text" id="present_district"  class="form-control" name="present_district" required="1"/>
		  </div>
  </div>

	<span>
		<b>Permanent Address</b>
		- Same as Present Address <input onclick="setPermanentAddress()" type="checkbox" id="same_as_present">
	</span>
	<hr>
	<div class="form-row">
		<div class="form-group col-md-12">
			<label>House No. / Village</label>
			<input type="textarea" id="permanent_address" class="form-control" name="permanent_address"/>
		</div>
	</div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label>Ward No.</label>
      <input type="text" id="permanent_ward" class="form-control" name="permanent_ward"/>
    </div>
    <div class="form-group col-md-3">
      <label>Post Office</label>
      <input type="text" id="permanent_post_office" class="form-control" name="permanent_post_office"/>
    </div>
    <div class="form-group col-md-3">
      <label>Thana/Upazilla</label>
      <input type="text" id="permanent_thana" class="form-control" name="permanent_thana" />
    </div>
	  <div class="form-group col-md-3">
		  <label>District</label>
		  <input type="text" id="permanent_district" class="form-control" name="permanent_district"/>
	  </div>
  </div>


    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
