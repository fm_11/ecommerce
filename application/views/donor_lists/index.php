<?php
$name = $this->session->userdata('name');
$present_district = $this->session->userdata('present_district');
$permanent_district = $this->session->userdata('permanent_district');
$mobile = $this->session->userdata('mobile');
$donation_type = $this->session->userdata('donation_type');

?>
<form name="addForm" class="cmxform" id="commentForm"  method="post" action="<?php echo base_url(); ?>donor_lists/index">
	<div class="form-row">

		<div class="form-group col-md-4">
			<input type="text" style="padding: 10px !important;" autocomplete="off"
				   name="name" placeholder="Name/Member ID" value="<?php if (isset($name)) {
				echo $name;
			} ?>" class="form-control" id="name">
		</div>

		<div class="form-group col-md-4">
			<input type="text" style="padding: 10px !important;" autocomplete="off"
				   name="present_district" placeholder="Present District/Thana/Upazilla" value="<?php if (isset($present_district)) {
				echo $present_district;
			} ?>" class="form-control" id="present_district">
		</div>

		<div class="form-group col-md-4">
			<input type="text" style="padding: 10px !important;" autocomplete="off"
				   name="permanent_district" placeholder="Permanent District/Thana/Upazilla" value="<?php if (isset($permanent_district)) {
				echo $permanent_district;
			} ?>" class="form-control" id="permanent_district">
		</div>

	</div>

	<div class="form-row">
		<div class="form-group col-md-4">
			<input type="text" style="padding: 10px !important;" autocomplete="off"
				   name="mobile" placeholder="Mobile Number" value="<?php if (isset($mobile)) {
				echo $mobile;
			} ?>" class="form-control" id="mobile">
		</div>

		<div class="form-group col-md-3">
			<select class="js-example-basic-single w-100" style="padding: 10px !important;" name="donation_type" id="donation_type">
				<option value="">-- Please Select --</option>
				<option value="M" <?php if($donation_type == 'M'){ echo 'selected'; } ?>>Monthly</option>
				<option value="Y" <?php if($donation_type == 'Y'){ echo 'selected'; } ?>>Yearly</option>
				<option value="O" <?php if($donation_type == 'O'){ echo 'selected'; } ?>>Other</option>
			</select>
		</div>
		<div class="form-group col-md-2">
			<button type="submit" style="padding: 13px 30px 13px 30px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
		</div>
	</div>

</form>

<hr>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col">SL</th>
		<th scope="col">Member ID</th>
        <th scope="col">Name</th>
        <th scope="col">Contact No</th>
		<th scope="col">Donation Type</th>
        <th scope="col">Amount</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($donor_lists as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
		    <td><?php echo $row['member_code']; ?></td>
            <td style="min-width: 200px;"><?php echo $row['name']; ?></td>
            <td><?php echo $row['mobile1']; ?></td>
		    <td>
				<?php
				if($row['donation_type'] == 'M'){
                    echo 'Monthly';
				}else if($row['donation_type'] == 'O'){
					echo 'Other';
				}else{
					echo 'Yearly';
				}
				?>
			</td>
            <td><?php echo $row['amount']; ?></td>
            <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" href="<?php echo base_url(); ?>donor_lists/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>donor_lists/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
	<?php echo $this->pagination->create_links(); ?>
</div>
</div>
