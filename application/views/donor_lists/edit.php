<script>
	function setPermanentAddress() {
		var isChecked = document.getElementById("same_as_present").checked;
		if(isChecked == true){
			document.getElementById("permanent_address").value = document.getElementById("present_address").value;
			document.getElementById("permanent_ward").value = document.getElementById("present_ward").value;
			document.getElementById("permanent_post_office").value = document.getElementById("present_post_office").value;
			document.getElementById("permanent_thana").value = document.getElementById("present_thana").value;
			document.getElementById("permanent_district").value = document.getElementById("present_district").value;
		}else{
			document.getElementById("permanent_address").value = "";
			document.getElementById("permanent_ward").value = "";
			document.getElementById("permanent_post_office").value = "";
			document.getElementById("permanent_thana").value =  "";
			document.getElementById("permanent_district").value =  "";
		}
	}
</script>



<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>donor_lists/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Member Name&nbsp;<span class="required_label">*</span></label>
      <input type="text"   class="form-control" name="name" value="<?php echo $donor_lists[0]['name']; ?>" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label>Father/Spouse Name&nbsp;<span class="required_label">*</span></label>
      <input type="text"   class="form-control" name="father_spouse_name" value="<?php echo $donor_lists[0]['father_spouse_name']; ?>" required="1"/>
    </div>
  </div>

  <div class="form-row">

	  <div class="form-group col-md-3">
		  <label >Donation Type&nbsp;<span class="required_label">*</span></label>
		  <select class="js-example-basic-single w-100" name="donation_type">
			  <option value="M" <?php if($donor_lists[0]['donation_type'] == 'M'){ echo 'selected'; } ?>>Monthly</option>
			  <option value="Y" <?php if($donor_lists[0]['donation_type'] == 'Y'){ echo 'selected'; } ?>>Yearly</option>
			  <option value="O" <?php if($donor_lists[0]['donation_type'] == 'O'){ echo 'selected'; } ?>>Other</option>
		  </select>
	  </div>
	  <div class="form-group col-md-3">
		  <label>Donation Amount&nbsp;<span class="required_label">*</span></label>
		  <input type="text"   class="form-control" name="amount" value="<?php echo $donor_lists[0]['amount']; ?>" required="1"/>
	  </div>

    <div class="form-group col-md-3">
      <label>Mobile 1&nbsp;<span class="required_label">*</span></label>
      <input type="text"   class="form-control" name="mobile1" value="<?php echo $donor_lists[0]['mobile1']; ?>" required="1"/>
    </div>
    <div class="form-group col-md-3">
      <label>Mobile 2</label>
      <input type="text"   class="form-control" name="mobile2" value="<?php echo $donor_lists[0]['mobile2']; ?>"/>
    </div>

  </div>


	<span><b>Present Address</b></span>
	<hr>


	<div class="form-row">
		<div class="form-group col-md-12">
			<label>House No. / Village</label>
			<input type="text"   class="form-control" id="present_address" name="present_address" value="<?php echo $donor_lists[0]['present_address']; ?>" required="1"/>
		</div>
	</div>

  <div class="form-row">
    <div class="form-group col-md-3">
      <label>Ward No.</label>
      <input type="text"   class="form-control" id="present_ward" name="present_ward" value="<?php echo $donor_lists[0]['present_ward']; ?>"/>
    </div>
    <div class="form-group col-md-3">
      <label>Post Office</label>
      <input type="text"   class="form-control" id="present_post_office" name="present_post_office" value="<?php echo $donor_lists[0]['present_post_office']; ?>"/>
    </div>
    <div class="form-group col-md-3">
      <label>Thana/Upazilla</label>
      <input type="text"   class="form-control" id="present_thana" name="present_thana" value="<?php echo $donor_lists[0]['present_thana']; ?>"/>
    </div>

    <div class="form-group col-md-3">
	  <label>District&nbsp;<span class="required_label">*</span></label>
	  <input type="text"   class="form-control" id="present_district" name="present_district" value="<?php echo $donor_lists[0]['present_district']; ?>" required="1"/>
    </div>

  </div>



	<span>
		<b>Permanent Address</b>
		- Same as Present Address <input onclick="setPermanentAddress()" type="checkbox" id="same_as_present">
	</span>
	<hr>

	<div class="form-row">
		<div class="form-group col-md-12">
			<label>House No. / Village</label>
			<input type="textarea"   class="form-control" id="permanent_address" name="permanent_address" value="<?php echo $donor_lists[0]['permanent_address']; ?>"/>
		</div>
	</div>

  <div class="form-row">
    <div class="form-group col-md-3">
      <label>Ward No.</label>
      <input type="text"   class="form-control" id="permanent_ward" name="permanent_ward" value="<?php echo $donor_lists[0]['permanent_ward']; ?>"/>
    </div>
    <div class="form-group col-md-3">
      <label>Post Office</label>
      <input type="text"   class="form-control" id="permanent_post_office" name="permanent_post_office" value="<?php echo $donor_lists[0]['permanent_post_office']; ?>"/>
    </div>
    <div class="form-group col-md-3">
      <label>Thana/Upazilla</label>
      <input type="text"   class="form-control" id="permanent_thana" name="permanent_thana" value="<?php echo $donor_lists[0]['permanent_thana']; ?>"/>
    </div>
    <div class="form-group col-md-3">
      <label>District</label>
      <input type="text"   class="form-control" id="permanent_district" name="permanent_district" value="<?php echo $donor_lists[0]['permanent_district']; ?>"/>
    </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $donor_lists[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
