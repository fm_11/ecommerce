<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fee_removes/fee_remove_data_save" method="post">
  <div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th scope="col">Roll</th>
            <th scope="col">Student ID</th>
            <th scope="col">Name</th>
            <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($retrun_data as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td>
                    <?php echo $row['roll_no']; ?>
                </td>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" name="student_id_<?php echo $i; ?>" value="<?php echo $row['student_id']; ?>"/>
                </td>
                <td>
                    <input type="checkbox" <?php if($row['is_fee_remove'] > 0){echo 'checked';} ?> name="is_allow_<?php echo $i; ?>">
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
  </div>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="year"
           value="<?php echo $year; ?>"/>

    <input type="hidden" class="input-text-short" name="class_id"
           value="<?php echo $class_id; ?>"/>

    <input type="hidden" class="input-text-short" name="section_id"
           value="<?php echo $section_id; ?>"/>

    <input type="hidden" class="input-text-short" name="group_id"
           value="<?php echo $group_id; ?>"/>

   <input type="hidden" class="input-text-short" name="shift_id"
    value="<?php echo $shift_id; ?>"/>

    <input type="hidden" class="input-text-short" name="fee_sub_category_id"
    value="<?php echo $fee_sub_category_id; ?>"/>

<div class="float-right">
   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
</div>

</form>
