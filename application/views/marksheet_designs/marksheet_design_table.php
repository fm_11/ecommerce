<div class="table-responsive-sm col-md-12">
	<table class="table table-hover">
		<tr>
			<td>Show Student Image ?</td>
			<td>
				<select style="padding: 7px;width: 200px;" name="show_student_image">
					<option value="1" <?php if(isset($configuration_data[0]['show_student_image'])){
						if($configuration_data[0]['show_student_image'] == '1'){ echo 'selected'; }
					} ?>>Yes</option>
					<option value="0" <?php if(isset($configuration_data[0]['show_student_image'])){
						if($configuration_data[0]['show_student_image'] == '0'){ echo 'selected'; }
					} ?>>No</option>
				</select>
			</td>
			<td>Marksheet Header</td>
			<td>
				<input type="text" name="marksheet_header" value="<?php if(isset($configuration_data[0]['marksheet_header'])){
					echo$configuration_data[0]['marksheet_header'];
				} ?>" style="padding: 7px;width: 200px;" >
			</td>
		</tr>

		<tr>
			<td>Show Show Watermark ?</td>
			<td>
				<select style="padding: 7px;width: 200px;" name="show_watermark">
					<option value="1" <?php if(isset($configuration_data[0]['show_watermark'])){
						if($configuration_data[0]['show_watermark'] == '1'){ echo 'selected'; }
					} ?>>Yes</option>
					<option value="0" <?php if(isset($configuration_data[0]['show_watermark'])){
						if($configuration_data[0]['show_watermark'] == '0'){ echo 'selected'; }
					} ?>>No</option>
				</select>
			</td>
			<td>Merit Position</td>
			<td>
				<select style="padding: 7px; width: 200px;" name="merit_position">
					<option value="CW" <?php if(isset($configuration_data[0]['merit_position'])){
						if($configuration_data[0]['merit_position'] == 'CW'){ echo 'selected'; }
					} ?>>Class Wise</option>
					<option value="SW" <?php if(isset($configuration_data[0]['merit_position'])){
						if($configuration_data[0]['merit_position'] == 'SW'){ echo 'selected'; }
					} ?>>Shift Wise</option>
					<option value="SECW" <?php if(isset($configuration_data[0]['merit_position'])){
						if($configuration_data[0]['merit_position'] == 'SECW'){ echo 'selected'; }
					} ?>>Section Wise</option>
					<option value="GW" <?php if(isset($configuration_data[0]['merit_position'])){
						if($configuration_data[0]['merit_position'] == 'GW'){ echo 'selected'; }
					} ?>>Group Wise</option>
				</select>
			</td>
		</tr>

		<tr>
			<td>Template Category</td>
			<td colspan="3">
				<select style="padding: 7px; width: 300px;" name="template_category">
					<option value="T1" <?php if(isset($configuration_data[0]['template_category'])){
						if($configuration_data[0]['template_category'] == 'T1'){ echo 'selected'; }
					} ?>>Design - 1 (With Highest Mark)</option>
					<option value="T2" <?php if(isset($configuration_data[0]['template_category'])){
						if($configuration_data[0]['template_category'] == 'T2'){ echo 'selected'; }
					} ?>>Design - 2 (Without Highest Mark)</option>
					<option value="T3" <?php if(isset($configuration_data[0]['template_category'])){
						if($configuration_data[0]['template_category'] == 'T3'){ echo 'selected'; }
					} ?>>Design - 3 (With All Mark Head)</option>
				</select>
			</td>
		</tr>
	</table>
</div>
