<script type="text/javascript">
	function get_configuration_by_year_class(class_id,year,type){
		debugger;
		if(class_id != "" && year != "" && type != ""){
			//alert(category_id);
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					document.getElementById("config_form").innerHTML = xmlhttp.responseText;
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>marksheet_designs/get_configuration_by_year_class?class_id=" + class_id + '&&year=' + year + '&&type=' + type, true);
			xmlhttp.send();
		}else{
			document.getElementById("config_form").innerHTML = "";
		}
	}

</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>marksheet_designs/index" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('year'); ?></label>
			<select class="js-example-basic-single w-100" onchange="get_configuration_by_year_class(document.getElementById('class_id').value,this.value,document.getElementById('type').value)" name="year" id="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['value']; ?>"
							<?php if (isset($year)) {
								if ($year == $list['value']) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['text']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('class'); ?></label>
			<select class="js-example-basic-single w-100"  onchange="get_configuration_by_year_class(this.value,document.getElementById('year').value,document.getElementById('type').value)" id="class_id" name="class_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class)) {
					foreach ($class as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Type</label>
			<select  class="js-example-basic-single w-100" onchange="get_configuration_by_year_class(document.getElementById('class_id').value,document.getElementById('year').value,this.value)"  id="type" required name="type">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<option value="CM">Combined Marksheet</option>
				<option value="GM">General Marksheet</option>
				<option value="GFM">Grand Final Marksheet</option>
			</select>
		</div>

	</div>
	<br><br>
	<div id="config_form">

	</div>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
