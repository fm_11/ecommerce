<script type="text/javascript">
    function getStudentByClassSectionAndGroup(class_id, section_id, group) {
        if (class_id == '' || section_id == '' || group == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("student_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>timekeepings/getStudentByClassSectionAndGroup?class_id=" + class_id + '&&section_id=' + section_id + '&&group=' + group, true);
        xmlhttp.send();
    }
</script>


<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>timekeepings/student_leave_add" method="post">
    <label>Class</label>
    <select name="class_id"
            onchange="getStudentByClassSectionAndGroup(this.value,document.getElementById('section_id').value,document.getElementById('group').value)"
            class="smallInput" required="1" id="class_id">
        <option value="">-- Please Select --</option>
        <?php foreach ($class as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Section</label>
    <select name="section_id"
            onchange="getStudentByClassSectionAndGroup(document.getElementById('class_id').value,this.value,document.getElementById('group').value)"
            class="smallInput" required="1" id="section_id">
        <option value="">-- Please Select --</option>
        <?php foreach ($section as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Group</label>
    <select name="group" id="group"
            onchange="getStudentByClassSectionAndGroup(document.getElementById('class_id').value,document.getElementById('section_id').value,this.value)"
            required="1" class="smallInput">
        <option value="">-- Not Applicable --</option>
        <?php
        if (count($groups)) {
            foreach ($groups as $list) {
                ?>
                <option
                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
            }
        }
        ?>
    </select>


    <label>Student Name</label>
    <select class="smallInput" name="student_id" id="student_id" style="width: 350px;" required="1">
        <option value="">-- Please Select --</option>
    </select>

    <label>From Date</label>
    <input type="text" autocomplete="off"  class="smallInput" placeholder="YYYY-MM-DD" name="txtFromDate" id="txtFromDate" required="1"/>

    <label>To Date</label>
    <input type="text" autocomplete="off"  class="smallInput" placeholder="YYYY-MM-DD" name="txtToDate" id="txtToDate" required="1"/>


    <label>Reason</label>
    <input type="text" autocomplete="off"  class="smallInput wide" name="txtReason" required="1"/>

    <br>
    <br>
    <input type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form>

<script>
    $(function () {
        $("#txtFromDate,#syn_date,#txtToDate").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
    
</script>

<link href="<?php echo base_url() . MEDIA_FOLDER; ?>/js/select2.min.css" rel="stylesheet"/>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/select2.min.js"></script>

<script type="text/javascript">
    $("#student_id").select2();
</script>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
