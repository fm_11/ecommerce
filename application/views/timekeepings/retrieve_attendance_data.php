<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>timekeepings/retrieve_attendance_data" method="post">
    <input type="submit" class="submit" value="Process">
    
</form>

<script>
    $(function() {
        $( "#txtDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });      
    });
</script>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
