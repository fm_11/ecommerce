<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>timekeepings/holiday_add"><span>Add Holiday</span></a>
       
   <a class="button_grey_round" style="margin-bottom: 5px;"
   href="<?php echo base_url(); ?>timekeepings/holiday_batch_add"><span>Batch Add Holiday</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Holiday Type</th>
        <th width="200" scope="col">Date</th>
        <th width="200" scope="col">Reason</th>
        <th width="200" scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($holidays as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['holiday_name']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo $row['remarks']; ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>timekeepings/holiday_edit/<?php echo $row['id']; ?>"
                   class="edit_icon" title="Edit"></a>
                <a href="<?php echo base_url(); ?>timekeepings/holiday_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="5" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>