

<form name="addForm" class="cmxform" id="commentForm"   enctype="multipart/form-data" action="<?php echo base_url(); ?>timekeepings/upload_timekeeping_data" method="post">
    <label>Excel File</label>
    <input type="file" class="smallInput" name="txtFile" id="file" required="1"/>
    <br>
    <br>
    <input type="submit" class="submit" value="Upload">
</form>


<script>
    $(function() {
        $( "#txtDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>