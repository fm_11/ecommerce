<h2>
	<a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>timekeepings/student_timekeepings_add"><span>Add Manual Login</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Name</th>
        <th width="200" scope="col">Class</th>
        <th width="200" scope="col">Section</th>
		<th width="200" scope="col">Date</th>
        <?php
		if($is_device_integrated_attendance_system == "Y"){
			$colspan = 7;
		?>
		<th width="200" scope="col">Login Time</th>
		<th width="200" scope="col">Logout Time</th>
        <?php		
		}else{
			$colspan = 6;
		?>
		<th width="200" scope="col">Login Status</th>
        <?php		
		}
		?>
       
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($timekeepings as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['student_name']; ?></td>
            <td><?php echo $row['class_name']; ?></td>
            <td><?php echo $row['section_name']; ?></td>
			 <td><?php echo $row['date']; ?></td>
          
			<?php
			if($is_device_integrated_attendance_system == "Y"){
			?>
			<td><?php echo $row['login_time']; ?></td>
			<td><?php echo $row['logout_time']; ?></td>
			<?php		
			}else{
			?>
			<td>
			<?php 
			   if($row['login_status'] == 'P'){
                   echo 'Present';
                }else if($row['login_status'] == 'A'){
                    echo 'Absent';
                }else{
                    echo 'Leave';
                }
			?>
			</td>
			<?php		
			}
			?> 
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="<?php echo $colspan; ?>" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>