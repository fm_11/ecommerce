<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>timekeepings/student_leave_add"><span>Add Leave</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="170" scope="col">Name</th>
        <th width="80" scope="col">Roll</th>
        <th width="80" scope="col">Class</th>
        <th width="80" scope="col">Section</th>
        <th width="100" scope="col">From Date</th>
        <th width="100" scope="col">To Date</th>
        <th width="100" scope="col">Syn. Date</th>
        <th width="200" scope="col">Reason</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($leaves as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['roll_no']; ?></td>
            <td><?php echo $row['class']; ?></td>
            <td><?php echo $row['section_name']; ?></td>
            <td><?php echo $row['date_from']; ?></td>
            <td><?php echo $row['date_to']; ?></td>
            <td><?php echo $row['syn_date']; ?></td>
            <td><?php echo $row['reason']; ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>timekeepings/student_leave_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="10" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>