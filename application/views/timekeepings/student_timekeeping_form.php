<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>timekeepings/add_student_login_data" method="post">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <thead>

        <tr>
            <td colspan="6" style="text-align: right;">
                <b>
                    Present SMS Send <input type="checkbox" name="is_present_sms">
                    Absent SMS Send <input type="checkbox" name="is_absent_sms">
                    Leave SMS Send <input type="checkbox" name="is_leave_sms">
                </b>
            </td>
        </tr>

        <tr>
            <th width="50" scope="col">SL
            <th width="150" scope="col">Student ID</th>
            <th width="200" scope="col">Name</th>
            <th width="50" scope="col">Call</th>
             <th width="250" scope="col">Status</th>
            <th width="100" scope="col">Roll</th>
            
            <th width="100" scope="col">Mobile</th>
           
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($student_info as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td width="34">
                    <?php echo $i; ?>
                </td>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                    <input type="hidden" name="name_<?php echo $i; ?>" value="<?php echo $row['name']; ?>"/>
                </td>
                  <td>
                    <a class="call_icon" href="tel:<?php echo $row['guardian_mobile']; ?>"></a>
                </td>
                 <td>
                    <input type="radio" name="login_status_<?php echo $i; ?>"
                           value="P" <?php if ($row['process_status'] == 'P') {
                        echo 'checked';
                    } elseif ($row['process_status'] == '') {
                        echo 'checked';
                    } ?> />Present
                    <input type="radio" name="login_status_<?php echo $i; ?>"
                           value="A" <?php if ($row['process_status'] == 'A') {
                        echo 'checked';
                    } ?> />Absent
                    <input type="radio" name="login_status_<?php echo $i; ?>"
                           value="L" <?php if ($row['process_status'] == 'L') {
                        echo 'checked';
                    } ?> />Leave
                </td>
                <td>
                    <?php echo $row['roll_no']; ?>
                </td>
              
                <td>
                    <input type="text" autocomplete="off"  name="guardian_mobile_<?php echo $i; ?>" value="<?php echo $row['guardian_mobile']; ?>">
                </td>
               
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <br>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="date"
           value="<?php echo $date; ?>"/>
    <input type="submit" class="submit" value="Save">

</form>
<br/>
<div class="clear"></div>
