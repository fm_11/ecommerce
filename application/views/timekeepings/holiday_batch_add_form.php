<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>timekeepings/holiday_batch_add" method="post">

    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tbody>
        <tr>        
            <td>
                <label>Month</label>
				<select class="smallInput" style="width:200px;" name="month" id="month" required="1">
					<option value="">-- Please Select --</option>
					<?php
					$i = 1;
					while ($i <= 12) {
						$dateObj = DateTime::createFromFormat('!m', $i);
						?>
						<option value="<?php echo $i; ?>" <?php if(isset($month)){ if($month == $i){ echo 'selected'; }} ?>><?php echo $dateObj->format('F'); ?></option>
						<?php
						$i++;
					}
					?>
				</select>
               </td>
			</tr>
		<tr>
				 <td>
					<label>Year</label>
					<select class="smallInput" style="width:200px;" name="year" required="1">
						<option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
						<option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
						<option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
					</select>
				</td>
        </tr>
		
        <tr>
            <td colspan="5" align="right">
                <input type="submit" class="submit" value="Process">
            </td>
        </tr>
        </tbody>
    </table>




</form>
<br>


<div>
    <?php
    if (isset($holidays)) {
        echo $process_table;
    }
    ?>
</div>
