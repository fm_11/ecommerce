<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function () {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>timekeepings/shift_list" title="Shift Information"><span>Shift</span></a></li>
    <li><a href="<?php echo base_url(); ?>timekeepings/upload_timekeeping_data"><span>Upload Data</span></a></li>
    <!--<li><a href="<?php echo base_url(); ?>timekeepings/retrieve_attendance_data"><span>Data Process</span></a></li>!-->
    <li><a href="<?php echo base_url(); ?>timekeepings/teacher_timekeeping_list" title="Teacher Timekeeping"><span>Teacher Time..</span></a></li>
    <li><a href="<?php echo base_url(); ?>timekeepings/student_timekeeping_list" title="Student Timekeeping"><span>Student Time..</span></a></li>
    <li><a href="<?php echo base_url(); ?>timekeepings/staff_timekeeping_list" title="Staff Timekeeping"><span>Staff Time..</span></a></li>
    <li><a href="<?php echo base_url(); ?>timekeepings/teacher_leave_list"><span>Teacher Leave</span></a></li>
    <li><a href="<?php echo base_url(); ?>timekeepings/student_leave_list"><span>Student Leave</span></a></li>
	<li><a href="<?php echo base_url(); ?>timekeepings/staff_leave_list"><span>Staff Leave</span></a></li>
    <li><a href="<?php echo base_url(); ?>timekeepings/holiday_list"><span>Holiday</span></a></li>
    <li><a href="<?php echo base_url(); ?>timekeepings/movement_register_list"><span>Movement</span></a></li>
</ul>

