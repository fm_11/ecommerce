<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>timekeepings/teacher_timekeepings_add" method="post">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <thead>
        <tr>
            <th width="50" scope="col">SL</th>
            <th width="150" scope="col">Name</th>
            <th width="120" scope="col">Index Number</th>
            <th width="130" scope="col">Post</th>
            <th width="200" scope="col">Status</th>
            <th width="100" scope="col" align="center">Leave Apply</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($teacher_info as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td width="34">
                    <?php echo $i; ?>
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" class="input-text-short" size="8"
                           style="text-align: right; width:75px;"
                           name="teacher_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <?php echo $row['teacher_index_no']; ?>
                </td>
                <td>
                    <?php echo $row['post_name']; ?>
                </td>
                <td>
                    <input type="radio" name="login_status_<?php echo $i; ?>"
                           value="P"  <?php if ($row['process_status'] == 'P') {
                        echo 'checked';
                    } elseif ($row['process_status'] == '') {
                        echo 'checked';
                    } ?> />Present
                    <input type="radio" name="login_status_<?php echo $i; ?>"
                           value="A" <?php if ($row['process_status'] == 'A') {
                        echo 'checked';
                    } ?> />Absent
                    <input type="radio" name="login_status_<?php echo $i; ?>"
                           value="L" <?php if ($row['process_status'] == 'L') {
                        echo 'checked';
                    } ?> />Leave
                </td>
                <td>
                    <?php
                    if ($row['leave_status'] != '') {
                        echo 'Yes';
                    }else{
                        echo 'No';
                    }
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <br>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="date"
           value="<?php echo $date; ?>"/>
    <input type="submit" class="submit" value="Save">

</form>
<br/>
<div class="clear"></div>
