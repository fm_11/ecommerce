<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>timekeepings/movement_register_add"><span>Add New Movement</span></a>
</h2>


<form action="<?php echo base_url(); ?>timekeepings/movement_register_list" method="post">
    <?php
    $person_type = $this->session->userdata('person_type');
    $name = $this->session->userdata('name');
    $date = $this->session->userdata('date');
    ?>

    <table width="100%">
        <tr>
            <td style="width: 20%;">
                <input type="text" autocomplete="off"  name="name" placeholder="Name" size="20" value="<?php if (isset($name)) {
                    echo $name;
                } ?>" style="display: -moz-stack !important;" class="smallInput" id="name">&nbsp;
            </td>

            <td style="width: 12%;">
                <input type="text" autocomplete="off"  name="date" placeholder="yyyy-mm-dd" size="10" value="<?php if (isset($date)) {
                    echo $date;
                } ?>" style="display: -moz-stack !important;" class="smallInput" id="date">&nbsp;
            </td>

            <td style="width: 12%;">
                <select name="person_type" class="smallInput">
                    <option value="T" <?php if (isset($person_type)) {
                        if ($person_type == 'T') {
                            echo 'selected';
                        }
                    } ?>> Teacher
                    </option>
                    <option value="ST" <?php if (isset($person_type)) {
                        if ($person_type == 'ST') {
                            echo 'selected';
                        }
                    } ?>>Staff
                    </option>
                </select>&nbsp;
            </td>

            <td>
                <input type="submit" name="Search" value="Search"/>
            </td>
        </tr>
    </table>
</form>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="180" scope="col">Name</th>
        <th width="80" scope="col">Date</th>
        <th width="100" scope="col">Start Time</th>
        <th width="100" scope="col">End Time</th>
        <th width="150" scope="col">Purpose</th>
        <th width="200" scope="col">Location</th>
        <th width="100" scope="col">Auto Login?</th>
        <th width="100" scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($records as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td>
            <?php echo date('h:i:s A', strtotime($row           ['start_time'])); ?>
</td>
            <td>
<?php echo date('h:i:s A', strtotime($row           ['end_time'])); ?>

</td>
            <td><?php echo $row['purpose']; ?></td>
            <td><?php echo $row['location']; ?></td>
            <td>
                <?php
                if ($row['attendance_auto'] == 'Y') {
                    echo 'Yes';
                }else{
                    echo 'No';
                }
                ?>
            </td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>timekeepings/movement_register_edit/<?php echo $row['id']; ?>"
                   class="edit_icon" title="Edit"></a>
                <a href="<?php echo base_url(); ?>timekeepings/movement_register_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="9" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
    </tbody>
</table>


<script>
    $(function () {
        $("#date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
