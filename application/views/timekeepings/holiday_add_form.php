<script>
    $(function() {
        $( "#txtDate" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<?php
if($action == 'edit'){
    ?>
    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>timekeepings/holiday_edit" method="post" enctype="multipart/form-data">
        <label>Date</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $holiday_info[0]['date']; ?>" name="txtDate" id="txtDate" required="1"/>

        <label>Holiday Type</label>
        <select class="smallInput" name="txtHolidayType" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($holiday_types)) {
                foreach ($holiday_types as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"<?php if($holiday_info[0]['holiday_type'] == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Remarks</label>
        <textarea id="wysiwyg" class="smallInput wide" required="1" rows="7" cols="30" name="txtRemarks"><?php echo $holiday_info[0]['remarks']; ?></textarea>

        <br>
        <br>
        <input type="hidden" class="smallInput wide" value="<?php echo $holiday_info[0]['id']; ?>" name="id" id="id" required="1"/>
        <input type="submit" class="submit" value="Update">
    </form>
    <br />
<?php
}else{
    ?>
    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>timekeepings/holiday_add" method="post" enctype="multipart/form-data">
        <label>Date</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtDate" id="txtDate" required="1"/>

        <label>Holiday Type</label>
        <select class="smallInput" name="txtHolidayType" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($holiday_types)) {
                foreach ($holiday_types as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>


        <label>Remarks</label>
        <textarea id="wysiwyg" class="smallInput wide" required="1" rows="7" cols="30" name="txtRemarks"></textarea>

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
    <br />

<?php
}
?>
<div class="clear"></div><br />

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>

