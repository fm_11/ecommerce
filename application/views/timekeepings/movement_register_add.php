<script>
    $(function () {
        $("#txtDate").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<style type="text/css">
    .input-group {
    }

    .pull-center {
        margin-left: auto;
        margin-right: auto;
    }
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/timepicker/jquery-clockpicker.min.css">

<h2>Please Input All Correct Information</h2>

<?php
if ($action == 'add') {
    ?>

    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>timekeepings/movement_register_add" method="post"
          enctype="multipart/form-data">
        <label>Teacher/Staff</label>
        <select class="smallInput" name="person_id" required="1">
            <option value="">-- Please Select --</option>
            <?php
            if (count($teachers)) {
                foreach ($teachers as $list) {
                    $i++;
                    ?>
                    <option
                        value="T-<?php echo $list['id']; ?>">
                        Teacher-<?php echo $list['name'] . '(' . $list['teacher_index_no'] . ')'; ?></option>
                <?php
                }
            }
            ?>

            <?php
            if (count($staff)) {
                foreach ($staff as $list) {
                    $i++;
                    ?>
                    <option
                        value="ST-<?php echo $list['id']; ?>">
                        Staff-<?php echo $list['name'] . '(' . $list['staff_index_no'] . ')'; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Date</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtDate" id="txtDate" required="1"/>

        <label>Out Time</label>

        <div class="input-group clockpicker1 pull-center" data-placement="left" data-align="top" data-autoclose="true">
            <input type="text" autocomplete="off"  name="outTime" class="smallInput wide" value="09:00" required="1">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-time"></span>
				</span>
        </div>

        <label>In Time</label>

        <div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
            <input type="text" autocomplete="off"  name="inTime" class="smallInput wide" value="16:00">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-time"></span>
				</span>
        </div>


        <label>Location</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtLocation" id="txtLocation" required="1"/>

        <label>Purpose</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtPurpose" id="txtPurpose" required="1"/>

        <label>Auto Attendance</label>
        <input type="radio" value="Y" name="txtAutoAttendance" required="1"/>Yes
        <input type="radio" value="N" name="txtAutoAttendance" required="1"/>No

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
<?php
} else {
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>timekeepings/movement_register_edit" method="post"
          enctype="multipart/form-data">

        <label>In Time</label>

        <div class="input-group clockpicker pull-center" data-placement="left" data-align="top" data-autoclose="true">
            <input type="text" autocomplete="off"  name="inTime" class="smallInput wide" value="<?php echo $movement_info[0]['end_time'] ?>">
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-time"></span>
				</span>
        </div>

        <label>Location</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $movement_info[0]['location'] ?>" name="txtLocation" id="txtLocation" required="1"/>

        <label>Purpose</label>
        <input type="text" autocomplete="off"  class="smallInput wide" value="<?php echo $movement_info[0]['purpose'] ?>" name="txtPurpose" id="txtPurpose" required="1"/>
        <br>
        <br>
        <input type="hidden" value="<?php echo $movement_info[0]['id'] ?>" name="id" id="id" required="1"/>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
<?php
}
?>
<br/>


<div class="clear"></div><br/>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>

<!--Time Picker-->


<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/timepicker/jquery-clockpicker.min.js"></script>
<script type="text/javascript">
    $('.clockpicker,.clockpicker1').clockpicker({
        placement: 'bottom',
        align: 'right',
        autoclose: true,
        'default': '20:48'
    });
</script>

