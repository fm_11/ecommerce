<div class="uk-width-large-2-4 midsection">
    <h3>Student Profile</h3>
    <hr>
<table  class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
    <tr>
        <td colspan="3" style="text-align:center; border:0px;"><img
                src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $student_info[0]['photo']; ?>"
                height="200" width="200" style=" border:1px solid #666666; border-radius:100px;"></td>
    </tr>
    <tr>
        <td colspan="3" align="center">
            <b><?php echo $student_info[0]['name']; ?></b>
        </td>
    </tr>
	
	 <tr>
        <td colspan="3" align="center">
            Student ID : <b><?php echo $student_info[0]['student_code']; ?></b>
        </td>
    </tr>

    <tr>
        <td colspan="3"
            style="text-align:center;" align="center">
            Roll : <?php echo $student_info[0]['roll_no']; ?>
        </td>
    </tr>
    <tr>
        <td align="right">Reg. No.</td>
        <td> :</td>
        <td><?php echo $student_info[0]['reg_no']; ?> </td>
    </tr>
    <tr>
        <td align="right">Class</td>
        <td> :</td>
        <td><?php echo $student_info[0]['class_name']; ?> </td>
    </tr>
    <tr>
        <td align="right">Section</td>
        <td> :</td>
        <td><?php echo $student_info[0]['section_name']; ?>
        </td>
    </tr>

    <tr>
        <td align="right">Group</td>
        <td> :</td>
        <td>
          <?php echo $student_info[0]['group_name']; ?>
        </td>
    </tr>


    <tr>
        <td align="right">Date of Birth</td>
        <td> :</td>
        <td><?php echo $student_info[0]['date_of_birth']; ?> </td>
    </tr>
    <tr>
        <td align="right">Gender</td>
        <td> :</td>
        <td>
            <?php
            if ($student_info[0]['gender'] == 'M') {
                echo 'Male';
            } else if ($student_info[0]['gender'] == 'F') {
                echo 'Female';
            } else {
                echo 'N/A';
            }
            ?>
        </td>
    </tr>
    <tr>
        <td align="right">Religion</td>
        <td> :</td>
        <td>
            <?php
            if ($student_info[0]['religion'] == 'I') {
                echo 'Islam';
            } else if ($student_info[0]['religion'] == 'H') {
                echo 'Hindu';
            } else {
                echo 'N/A';
            }
            ?>
        </td>
    </tr>

    <tr>
        <td align="right">Blood Group</td>
        <td> :</td>
        <td><?php echo $student_info[0]['blood_group']; ?> </td>
    </tr>

    <tr>
        <td align="right">Father's Name</td>
        <td> :</td>
        <td><?php echo $student_info[0]['father_name']; ?> </td>
    </tr>
    <tr>
        <td align="right">Father's NID</td>
        <td> :</td>
        <td><?php echo $student_info[0]['father_nid']; ?> </td>
    </tr>
    <tr>
        <td align="right">Mother's Name</td>
        <td> :</td>
        <td><?php echo $student_info[0]['mother_name']; ?> </td>
    </tr>
    <tr>
        <td align="right">Mother's NID</td>
        <td> :</td>
        <td><?php echo $student_info[0]['mother_nid']; ?> </td>
    </tr>
    <tr>
        <td align="right">Present Address</td>
        <td align="center"> :</td>
        <td><?php echo $student_info[0]['present_address']; ?> </td>
    </tr>
    <tr>
        <td align="right">Permanent Address</td>
        <td> :</td>
        <td><?php echo $student_info[0]['permanent_address']; ?></td>
    </tr>
    <tr>
        <td align="right">Guardian Mobile</td>
        <td> :</td>
        <td><?php echo $student_info[0]['guardian_mobile']; ?></td>
    </tr>
    <tr>
        <td align="right">Email</td>
        <td> :</td>
        <td><?php echo $student_info[0]['email']; ?></td>
    </tr>
</table>

<table   class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
    <tr>
        <th colspan="3">
            <font size="4+">
                <center>Subject List</center>
            </font>
        </th>
    </tr>
    <tr>
        <td>Sl</td>
        <td>Subject</td>
        <td>Is Optional</td>
    </tr>
    <?php for ($l = 0; $l < count($subject_list_by_student); $l++) { ?>
        <tr>
            <td>
                <?php echo $l + 1; ?>
            </td>
            <td>
                <?php echo $subject_list_by_student[$l]['name']; ?>
            </td>
            <td>
                <?php if ($subject_list_by_student[$l]['is_optional'] == '1') {
                    echo "YES";
                } ?>
            </td>
        </tr>
    <?php } ?>
</table>
</div>



