<script>
    $(function () {
        $("#date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<div class="uk-width-large-2-4 midsection">
    <h3>Staff Timekeeping Information</h3>
    <hr/>
    <div class="uk-margin">
        <form class="uk-form" action="<?php echo base_url(); ?>contents/getStaffTimeKeepingInformation" method="post">

            <?php
            $date = $this->session->userdata('date');
            if ($date != '') {
                $date = $date;
            } else {
                $date = "";
            }
            ?>
            <fieldset data-uk-margin>
               <input type="text" autocomplete="off"  name="date"  class="uk-width-large-1-4" id="date" size="15" placeholder="YYYY-mm-dd"
                       value="<?php if ($date != '') {
                           echo $date;
                       } else {
                           echo date('Y-m-d');
                       } ?>"/>
                <input type="submit" name="Search" class="uk-button uk-width-large-1-5" value="Search"/>
            </fieldset>
        </form>
    </div>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Name</th>
                <th>Index Number</th>
                <th>Post</th>
                <th>Date</th>
                <th>Login Status</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($staff_login_info as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['staff_name']; ?></td>
                    <td><?php echo $row['staff_index_no']; ?></td>
                    <td><?php echo $row['post_name']; ?></td>
                    <td><?php echo $row['date']; ?></td>
                    <td>
                        <?php
                        if ($row['login_status'] == 'A') {
                            echo 'Absent';
                        } else if ($row['login_status'] == 'P') {
                            echo 'Present';
                        } else if ($row['login_status'] == 'L') {
                            echo 'Leave';
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <!--  PAGINATION START -->
    <div class="pagination uk-text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <!--  PAGINATION END-->

</div>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>



