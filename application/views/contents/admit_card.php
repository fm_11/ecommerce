<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript">

    function PrintElem(elem) {
        Popup($(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title>my div</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }

</script>
<?php
if ($status == 0) {
    ?>
    <style>
        #site_title_des {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 17px;
            color: #95e17d;
            margin: 0px;
            margin-left: 6px;
            display: block;
            margin: 0px 0px 5px 5px
        }

        .red11 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: red;
            text-decoration: none;
            font-weight: normal
        }

        .red12bold {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: red;
            text-decoration: none;
            font-weight: bold
        }

        .black12bold {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000;
            text-decoration: none;
            font-weight: bold
        }

        .black12 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000;
            text-decoration: none;
            font-weight: normal
        }

        .black14bold {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 14px;
            color: #000;
            text-decoration: none;
            font-weight: bold;
            clip: rect(auto, auto, auto, auto)
        }

        .black14 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 14px;
            color: #000;
            text-decoration: none;
            font-weight: normal;
            clip: rect(auto, auto, auto, auto)
        }

        .black16bold {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 16px;
            color: #000;
            text-decoration: none;
            font-weight: bold
        }

        .links {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #00f;
            text-decoration: none;
            font-weight: bold
        }

        .links02 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #fff;
            text-decoration: none;
            font-weight: normal
        }

        .links:hover {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: red;
            text-decoration: none;
            font-weight: bold
        }

        .links02:hover {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            color: #0c9;
            text-decoration: none;
            font-weight: normal
        }

        .textfield05 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000;
            text-decoration: none;
            background-color: #f4f0f2;
            border: 1px solid #999;
            font-weight: normal;
            width: 205px;
            padding-top: 4px;
            padding-right: 4px;
            padding-left: 4px;
            padding-bottom: 4px;
            border-radius: 4px
        }

        .textfield06 {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 12px;
            color: #000;
            text-decoration: none;
            background-color: #f4f0f2;
            border: 1px solid #999;
            font-weight: normal;
            width: 200px;
            padding-top: 4px;
            padding-right: 4px;
            padding-left: 4px;
            padding-bottom: 4px;
            border-radius: 4px
        }
    </style>
    <table align="center" bgcolor="#FFFFFF" style="border-radius:20px; margin-top: 5px;" border="0" cellpadding="0" cellspacing="0"
           width="100%">
        <tbody>
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    <tr>
                        <td class="black16bold" align="center" height="100" valign="middle">
                            <span style="color:red;">Sorry PIN or Password Invalid !</span>
                            <br>
                            OR
                            <br>
                            <span style="color: red;">Your payment not paid.Please contact admission department.</span>
                            <br>
                            <a href="<?php echo base_url(); ?>" style="font-size: 12px; text-decoration: underline;">Back to Home</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
<?php
} else {
    ?>
    <div id="mydiv" style="margin-top: 10px; margin-bottom: 50px; background-color: #ffffff;">
        <table style="width: 8.2677in">
            <tbody>
            <tr>
                <td style="width: 200px">
                    <div style="border: 1px solid black; width: 150px; height: 190px">
                    <span style="text-align: center; display: block">
                        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $admit_card_status[0]['photo']; ?>" align="center" style="width: 150px; height: 190px;">
                    </span>
                    </div>

                </td>
                <td style="width: 450px; text-align: center;">
                    <span style="font-size: 25px;">Admit Card</span>
                </td>
                <td>
                    <div style="margin-bottom: 2px; height: 35px">
                        <span style="float: left; display: block; width: 55px"><b>PIN</b></span> <span
                            style="height: 20px; width: 50px; border-bottom: 1px dotted black; display: block; float: left; padding: 2px">
                             <?php echo $admit_card_status[0]['pin']; ?>
                        </span>

                    </div>
                    <div style="clear: both; margin-bottom: 2px; height: 35px">
                        <span style="float: left; display: block; width: 55px"><b>Class</b></span> <span
                            style="text-align:center;height: 23px; width: 50px; border: 1px solid black; display: block; float: left; padding: 2px"><b>
                                <?php echo $admit_card_status[0]['class_name']; ?>
                            </b></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 5px 0"><span style="float: left; width: 20%;"><b>Candidate
                            Name</b>&nbsp;</span><span
                        style="height: 23px; width: 78%; float: left; border: 1px solid black; display: block; padding: 2px">
                        <?php echo $admit_card_status[0]['name']; ?>
                    </span>

                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 5px 0"><span style="float: left; width: 20%;"><b>Father's
                            Name</b>&nbsp;</span><span
                        style="height: 23px; width: 78%; float: left; border: 1px solid black; display: block; padding: 2px">
                        <?php echo $admit_card_status[0]['father_name']; ?>
                    </span>
                </td>
            </tr>

            <tr>
                <td colspan="3" style="padding: 5px 0"><span style="float: left; width: 20%;"><b>Date of Birth</b>&nbsp;</span><span
                        style="height: 23px; width: 78%; float: left; border: 1px solid black; display: block; padding: 2px">
                        <?php echo $admit_card_status[0]['date_of_birth']; ?>
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-top: 15px">

                    <span style="float: left; width: 20%;"><b>Signature:</b></span><span
                        style="height: 23px; width: 250px; float: left; border-bottom: 1px dotted black; display: block; padding: 2px"></span>
                </td>
            </tr>
            </tbody>
        </table>


        <div style="clear: both; padding: 15px  0;">
            <span><b>বি:দ্র:&nbsp;</b> প্রবেশপত্র ছাড়া Admission Test এ অংশগ্রহণ করতে দেয়া হবে না।</span><br>
            <span
                style="padding-left: 3em">লিখিত নির্বাচনী পরীক্ষার প্রার্থী তালিকা ও সূচি  website এ প্রকাশিত হবে।</span>
        </div>
        <p style="font-size:10px;text-align:right">Powered by: SoftwareValley</p>
    </div>

    <input type="button" class="button" value="Print" onclick="PrintElem('#mydiv')"/>
<?php
}
?>
