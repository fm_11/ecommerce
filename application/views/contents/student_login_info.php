<script>
    $(function () {
        $("#date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<div class="uk-width-large-2-4 midsection">
    <h3>Student Timekeeping Information</h3>
    <hr/>
    <div class="uk-margin">
        <form class="uk-form" action="<?php echo base_url(); ?>contents/getStudentTimeKeepingInformation" method="post">

            <?php
            $class_id = $this->session->userdata('class_id');
            $section_id = $this->session->userdata('section_id');
            $group = $this->session->userdata('group');
            $date = $this->session->userdata('date');
            if ($date != '') {
                $date = $date;
            }
            else{
                $date = "";
            }
            ?>
            <fieldset data-uk-margin>
                <input type="text" autocomplete="off"  name="date" id="date" size="15" class="uk-width-large-1-4" placeholder="YYYY-mm-dd" value="<?php if($date != ''){echo $date;}else{echo date('Y-m-d');} ?>"/>

                <select name="class_id"  class="uk-width-large-1-4" id="class_id">
                    <option value="">--Class--</option>
                    <?php foreach ($class_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if($row['id'] == $class_id){ echo 'selected'; } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>


                <select name="section_id"  class="uk-width-large-1-4" id="section_id">
                    <option value="">--Section--</option>
                    <?php foreach ($section_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if($row['id'] == $section_id){ echo 'selected'; } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>

                <select name="group" class="uk-width-large-1-4">
                    <option value="">--Group--</option>
                    <?php foreach ($group_list as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"<?php if($row['id'] == $group){ echo 'selected'; } ?>><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>

                <input type="submit" name="Search" class="uk-button uk-width-large-1-5" value="Search"/>
            </fieldset>
        </form>
    </div>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Student ID</th>
                <th>Name</th>
                <th>Roll</th>
                <th>Class</th>
                <th>Section</th>
                <th>Group</th>
                <th>Date</th>
                <th>Login Status</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($student_login_info as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['student_code']; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['roll_no']; ?></td>
                    <td><?php echo $row['class_name']; ?></td>
                    <td><?php echo $row['section_name']; ?></td>
                    <td>
                        <?php echo $row['group_name']; ?>
                    </td>
                    <td><?php echo $row['date']; ?></td>
                    <td>
                        <?php
                        if ($row['login_status'] == 'A') {
                            echo 'Absent';
                        } else if ($row['login_status'] == 'P') {
                            echo 'Present';
                        } else if ($row['login_status'] == 'L') {
                            echo 'Leave';
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <!--  PAGINATION START -->
    <div class="pagination uk-text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <!--  PAGINATION END-->

</div>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>



