<div class="uk-width-large-2-4 midsection">
    <h3>All Headmaster</h3>
    <hr/>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Name</th>
                <th>Educational Qua.</th>
                <th>From Date</th>
                <th>To Date</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($all_headmasters as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['edu_qua']; ?></td>
                    <td><?php echo $row['from_date']; ?></td>
                    <td>
                        <?php
                        if($row['to_date'] == '0000-00-00'){
                            echo 'Current';
                        }
                        else{
                            echo $row['to_date'];
                        }
                        ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>