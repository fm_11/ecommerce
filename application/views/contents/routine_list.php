<div class="uk-width-large-2-4 midsection">
    <h3>All Routine</h3>
    <hr/>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Class</th>
                <th>Section</th>
                <th>Group</th>
                <th>Year</th>
                <th>Download</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($student_routine as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['class_name']; ?></td>
                    <td><?php echo $row['section_name']; ?></td>
                    <td>
                        <?php
                        if($row['group'] == 'C'){
                            echo 'Commerce';
                        }else if($row['group'] == 'A'){
                            echo  'Arts';
                        }else if($row['group'] == 'S'){
                            echo  'Science';
                        }else{
                            echo 'N/A';
                        }
                        ?>
                    </td>
                    <td><?php echo $row['year']; ?></td>
                    <td><a href="<?php echo base_url() . MEDIA_FOLDER; ?>/routine/<?php echo $row['location']; ?>"><i class="uk-icon-small uk-icon-eye"></i></a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <!--  PAGINATION START -->
    <div class="pagination uk-text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <!--  PAGINATION END-->

</div>