<style>
    td {
        border: 1px #d6e5fc solid;
    }
</style>

<table width="100%" style="height: 550px; border-radius: 15px;" border="0" align="center" cellpadding="0"
       cellspacing="0" bgcolor="#FFFFFF">
    <tbody>
    <tr>
        <td width="12" align="left" valign="top">
            &nbsp;</td>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td height="100" align="center" valign="middle" class="black16bold">
                        <b><?php echo $exam[0]['name']; ?>, <?php echo $exam[0]['year']; ?></b><br><br>
                        Student ID: <b><?php echo $Result['result_process_info'][0]['student_code']; ?></b>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="middle">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td align="center" valign="middle">
                                    <table width="100%" border="0" cellpadding="3" cellspacing="1"
                                           class="black12">
                                        <tbody>
                                        <tr>
                                            <td width="12%" align="left" valign="middle" bgcolor="#EEEEEE">
                                                Roll No
                                            </td>
                                            <td width="27%" align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $Result['result_process_info'][0]['roll_no']; ?>
                                            </td>
                                            <td width="22%" align="left" valign="middle" bgcolor="#EEEEEE">
                                                Name
                                            </td>
                                            <td width="39%" align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $Result['result_process_info'][0]['name']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">Class</td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $Result['result_process_info'][0]['class_name']; ?>
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">Father's Name
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $Result['result_process_info'][0]['father_name']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                Section
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $Result['result_process_info'][0]['section_name']; ?>
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                Mother's Name
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $Result['result_process_info'][0]['mother_name']; ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">Group</td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $Result['result_process_info'][0]['group_name']; ?>
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                Date of Birth
                                            </td>
                                            <td align="left" valign="middle" bgcolor="#EEEEEE">
                                                <?php echo $Result['result_process_info'][0]['date_of_birth']; ?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="40" align="center" valign="middle"><span class="black16bold">Subject-Wise Grade/   Mark Sheet</span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="middle">
                                    <table width="100%" border="1" cellpadding="3" cellspacing="1"
                                           class="black12">
                                        <tbody>
                                        <tr class="black12bold">
                                            <td width="10%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Code
                                            </td>
                                            <td width="25%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Subject
                                            </td>
                                            <td width="8%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Credit
                                            </td>
                                            <td width="8%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Creative
                                            </td>
                                            <td width="8%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Multiple <br> Choice
                                            </td>
                                            <td width="8%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Practical
                                            </td>
                                            <td width="15%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Total Obtained
                                            </td>
                                            <td width="15%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Letter <br> Grade
                                            </td>
                                            <td width="15%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Total <br> Obtained <br> Mark
                                            </td>
                                            <td width="10%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                GPA<br>(With Optional)
                                            </td>
                                            <td width="10%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                Class Place
                                            </td>
                                            <td width="10%" align="left" valign="middle" bgcolor="#AFB7BE">
                                                1st Place <br> Mark
                                            </td>
                                        </tr>

                                        <?php
                                        for ($A = 0; $A < (count($Result['result_process_details'])); $A++) {
                                            ?>
                                        <tr <?php if ($Result['result_process_details'][$A]['is_optional'] == '1') { ?> style="background-color: lightyellow;" <?php } ?>>
                                            <td><?php echo $Result['result_process_details'][$A]['subject_code']; ?></td>
                                            <td><?php echo $Result['result_process_details'][$A]['subject_name']; ?></td>
                                            <td style="text-align: center;"><?php echo $Result['result_process_details'][$A]['credit']; ?></td>
                                            <td style="text-align: center;"><?php echo $Result['result_process_details'][$A]['written']; ?></td>
                                            <td style="text-align: center;"><?php echo $Result['result_process_details'][$A]['objective']; ?></td>
                                            <td style="text-align: center;"><?php echo $Result['result_process_details'][$A]['practical']; ?></td>
                                            <td style="text-align: center;">
                                                <?php echo $Result['result_process_details'][$A]['total_obtain']; ?>
                                            </td>
                                            <?php if ($Result['result_process_details'][$A]['subject_code'] == '101') { ?>
                                                <td style="vertical-align: middle;text-align: center;" rowspan="2">
                                                    <?php echo $Result['result_process_details'][$A]['alpha_gpa']; ?>
                                                </td>
                                            <?php } ?>
                                            <?php if ($Result['result_process_details'][$A]['subject_code'] == '107') { ?>
                                                <td style="vertical-align: middle;text-align: center;" rowspan="2">
                                                    <?php echo $Result['result_process_details'][$A]['alpha_gpa']; ?>
                                                </td>
                                            <?php } ?>
                                            <?php if ($Result['result_process_details'][$A]['subject_code'] != '101' && $Result['result_process_details'][$A]['subject_code'] != '102' && $Result['result_process_details'][$A]['subject_code'] != '107' && $Result['result_process_details'][$A]['subject_code'] != '108') { ?>
                                                <td style="text-align: center;">
                                                    <?php echo $Result['result_process_details'][$A]['alpha_gpa']; ?>
                                                </td>
                                            <?php } ?>
                                            <?php if ($A == 0) { ?>
                                                <td style="vertical-align: middle; text-align: center;"
                                                    rowspan="<?php echo count($Result['result_process_details']); ?>">
                                                    <?php echo $Result['result_process_info'][0]['total_obtain_mark']; ?>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center;"
                                                    rowspan="<?php echo count($Result['result_process_details']); ?>">
                                                    <?php echo $Result['result_process_info'][0]['gpa_with_optional']; ?>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center;"
                                                    rowspan="<?php echo count($Result['result_process_details']); ?>">
                                                    <?php echo $position; ?>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center;"
                                                    rowspan="<?php echo count($Result['result_process_details']); ?>">
                                                    <?php echo number_format($max_total_obtain_mark[0]['max_total_obtain_mark'], 2); ?>
                                                </td>
                                                </tr>
                                            <?php }
                                        } ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    <b>Powered by: School360</b>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

<br><br>