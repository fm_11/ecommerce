<div class="uk-width-large-2-4 midsection">
    <h3>Success Student Information</h3>
    <hr/>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Name</th>
                <th>Passing Year</th>
                <th>Mobile</th>
                <th>Full Info.</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($students as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['year_of_passing']; ?></td>
                    <td><?php echo $row['mobile']; ?></td>
                    <td><a href="<?php echo  base_url(); ?>contents/getSuccessStudentsInfoByID/<?php echo $row['id']; ?>"><i class="uk-icon-small uk-icon-eye"></i></a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>