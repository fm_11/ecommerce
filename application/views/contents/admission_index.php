<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><?php echo $title; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>core_media/syllabus_css/syllabus.css"/>
</head>
<body>
<div style="margin:0px auto; width: 810px;">
    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/admission_form_logo.png" width="810px;" style="border-radius:20px;">
</div>
<div style="margin: 0px auto; width:810px; border-radius:20px;">
    <?php
    echo $main_content;
    ?>
</div>
<div style="margin:0px auto; width: 800px; text-align:center;">
    <br>
    <img src="<?php echo base_url(); ?>core_media/images/company_logo.png" align="center" title="CrimsonsTec">
    <br><br>
</div>
</body>
</html>





