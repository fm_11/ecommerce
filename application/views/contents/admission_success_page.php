<style>
    #site_title_des {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 17px;
        color: #95e17d;
        margin: 0px;
        margin-left: 6px;
        display: block;
        margin: 0px 0px 5px 5px
    }

    .red11 {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 11px;
        color: red;
        text-decoration: none;
        font-weight: normal
    }

    .red12bold {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 11px;
        color: red;
        text-decoration: none;
        font-weight: bold
    }

    .black12bold {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #000;
        text-decoration: none;
        font-weight: bold
    }

    .black12 {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #000;
        text-decoration: none;
        font-weight: normal
    }

    .black14bold {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 14px;
        color: #000;
        text-decoration: none;
        font-weight: bold;
        clip: rect(auto, auto, auto, auto)
    }

    .black14 {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 14px;
        color: #000;
        text-decoration: none;
        font-weight: normal;
        clip: rect(auto, auto, auto, auto)
    }

    .black16bold {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 16px;
        color: #000;
        text-decoration: none;
        font-weight: bold
    }

    .links {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 11px;
        color: #00f;
        text-decoration: none;
        font-weight: bold
    }

    .links02 {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 11px;
        color: #fff;
        text-decoration: none;
        font-weight: normal
    }

    .links:hover {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 11px;
        color: red;
        text-decoration: none;
        font-weight: bold
    }

    .links02:hover {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 11px;
        color: #0c9;
        text-decoration: none;
        font-weight: normal
    }

    .textfield05 {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #000;
        text-decoration: none;
        background-color: #f4f0f2;
        border: 1px solid #999;
        font-weight: normal;
        width: 205px;
        padding-top: 4px;
        padding-right: 4px;
        padding-left: 4px;
        padding-bottom: 4px;
        border-radius: 4px
    }

    .textfield06 {
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #000;
        text-decoration: none;
        background-color: #f4f0f2;
        border: 1px solid #999;
        font-weight: normal;
        width: 200px;
        padding-top: 4px;
        padding-right: 4px;
        padding-left: 4px;
        padding-bottom: 4px;
        border-radius: 4px
    }
</style>
<table align="center" bgcolor="#FFFFFF" style="border-radius:20px; margin-top: 5px;" border="0" cellpadding="0" cellspacing="0"
       width="100%">
    <tbody>
    <tr>
        <td valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                    <td class="black16bold" align="center" height="100" valign="middle">
                        <span style="color:green;"><?php echo $name; ?></span> you are Successfully submit your admission form.
                        <br>
                        <span style="color: red;">Please Collect your PIN</span>
                        <br>
                        Your PIN is : <span style="color:green;"><?php echo $pin; ?></span>
                        <br>
                        <a href="<?php echo base_url(); ?>" style="font-size: 12px; text-decoration: underline;">Back to Home</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>