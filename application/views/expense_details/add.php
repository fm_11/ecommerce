<style>
.for_income{
  padding: 7px;
}
</style>
<script>
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

            var obj = JSON.parse(xmlhttp.responseText);
            var option_html = "";
            option_html += '<option value="">-- Expense Category --</option>'
            var i = 0;
            while (i < obj.length) {
                option_html += '<option value="' + obj[i].id + '">' + obj[i].name + '</option>'
                i++;
            }

            document.getElementById("expense_category_json_data").value = option_html;

            //get deposit_method data
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var obj2 = JSON.parse(xmlhttp.responseText);
                    var option_html_2 = "";
                    option_html_2 += '<option value="">-- Deposit Method --</option>'
                    var i = 0;
                    while (i < obj2.length) {
                        option_html_2 += '<option value="' + obj2[i].id + '">' + obj2[i].name + '</option>'
                        i++;
                    }

                    document.getElementById("deposit_method_json_data").value = option_html_2;
                }
            }
            xmlhttp.open("GET", "<?php echo base_url(); ?>accounts/get_deposit_method_for_list", true);
            xmlhttp.send();
            //end deposit_method data

        }
    }
    xmlhttp.open("GET", "<?php echo base_url(); ?>accounts/get_expense_category_for_list", true);
    xmlhttp.send();


    function delete_row(row_no) {
        var no = Number(row_no);
        document.getElementById("row_" + no + "").outerHTML = "";
        if (no != 1) {
            document.getElementById("delete_section_" + (no - 1)).innerHTML = "<input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs' onclick='delete_row(" + (no - 1) + ")'>";
        }
        document.getElementById('num_of_row').value = no;
    }

    function add_row() {
        var getUrl = window.location;
        var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        //alert(baseUrl);

        var table = document.getElementById("my_data_table");
        var table_len = Number(document.getElementById('num_of_row').value);
        var row = table.insertRow(table_len).outerHTML = "<tr id='row_" + table_len + "'><td><select required class='js-example-basic-single w-100'  id='expense_category_id_" + table_len + "' name='expense_category_id_" + table_len + "' ></select></td><td><input type='text' required class='for_income' placeholder='Expense Description' name='expense_description_" + table_len + "'></td><td><select required class='for_income'  id='deposit_method_id_" + table_len + "' name='deposit_method_id_" + table_len + "' ></select></td><td><input type='text' required class='for_income' onkeyup='calculate_total_amount()'  placeholder='Amount' name='amount_" + table_len + "' id='amount_" + table_len + "'></td><td id='delete_section_" + table_len + "'><input type='button' value='Delete' class='btn btn-outline-danger btn-fw btn-xs' onclick='delete_row(" + table_len + ")'></td></tr>";
        if (table_len > 1) {
            document.getElementById("delete_section_" + (table_len - 1)).innerHTML = "";
        }

        var expense_category_json_data = document.getElementById("expense_category_json_data").value;
        document.getElementById('expense_category_id_' + table_len).innerHTML = expense_category_json_data;

        var deposit_method_json_data = document.getElementById("deposit_method_json_data").value;
        document.getElementById('deposit_method_id_' + table_len).innerHTML = deposit_method_json_data;

        document.getElementById('num_of_row').value = table_len + 1;
        $(".js-example-basic-single").select2();
    }

    function calculate_total_amount() {

      var totalRow = Number(document.getElementById("num_of_row").value);
    //  alert(totalRow);
      var totalAmount = 0;
      var i = 0;
      while (i < totalRow) {
        if(document.getElementById("amount_" + i).value != ''){
          totalAmount = totalAmount + Number(document.getElementById("amount_" + i).value);
        }
        i++;
      }
      document.getElementById("expense_total_amount").value = totalAmount;
    }

</script>


<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>expense_details/add" method="post">
  <div class="form-row">
    <div class="form-group col-md-3">
      <label>Date</label>
      <div class="input-group date">
              <input type="text" autocomplete="off"  name="date" id="date" required class="form-control">
              <span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
      </div>
    </div>
    <div class="form-group col-md-3">
      <label>Voucher Id</label>
      <input type="text" autocomplete="off"  name="expense_voucher_id" required id="expense_voucher_id" class="form-control" value="">
    </div>
    <div class="form-group col-md-3">
      <label>Total Amount</label>
      <input type="text" autocomplete="off"  name="expense_total_amount" readonly required
             id="expense_total_amount" class="form-control" value="">
    </div>
    <div class="form-group col-md-3">
      <label>Cost Center</label>
      <select class="js-example-basic-single w-100" name="cost_center" required="1">
       <option value="">-- Select --</option>
       <?php
       $i = 0;
       if (count($cost_center)) {
           foreach ($cost_center as $list) {
               $i++; ?>
               <option
                       value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
               <?php
           }
       }
       ?>
        </select>
    </div>
  </div>



  <div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table">
        <tr>
            <th colspan="5" style="font-size: 15px; font-weight: bold;">
                Expense Details
            </th>
        </tr>
    </table>

    <table class="table" id="my_data_table">
        <tr>
            <td>
                <select class="js-example-basic-single w-100" name="expense_category_id_0" required="1">
                    <option value="">-- Expense Category --</option>
                    <?php
                    $i = 0;
                    if (count($expense_category)) {
                        foreach ($expense_category as $list) {
                            $i++; ?>
                            <option
                                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </td>
            <td><input type="text" autocomplete="off"  placeholder="Expense Description" class="for_income" required id="expense_description_0" name="expense_description_0">
            </td>
            <td>
                <select class="for_income" name="deposit_method_id_0" required="1">
                    <option value="">-- Deposit Method --</option>
                    <?php
                    $i = 0;
                    if (count($deposit_method)) {
                        foreach ($deposit_method as $list) {
                            $i++; ?>
                            <option
                                    value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </td>
            <td><input type="text" autocomplete="off"  class="for_income"  id="amount_0" placeholder="Amount" onkeyup="calculate_total_amount()" class="form-control" required name="amount_0"></td>
            <td><input type="button" class="btn btn-outline-secondary btn-fw btn-xs" onclick="add_row();" value="Add More"></td>
        </tr>
    </table>
  </div>

    <input type="hidden" id="expense_category_json_data" value="">
    <input type="hidden" id="deposit_method_json_data" value="">
    <input type="hidden" id="num_of_row" name="num_of_row" value="1">
    <div class="float-right">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>

<script>
$(document).ajaxComplete(function () {
           $(".js-example-basic-single").select2();
       });
</script>
