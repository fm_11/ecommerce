<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>exam_sessions/edit" method="post">
	<div class="form-row">
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('name'); ?></label>
			<input type="text" autocomplete="off"  required name="name" class="form-control" value="<?php echo $exam_sessions->name; ?>">
		</div>
		<div class="form-group col-md-3">
			<label>Routine Header</label>
			<input type="text" autocomplete="off" required name="header" class="form-control" value="<?php echo $exam_sessions->header; ?>">
		</div>
		<div class="form-group col-md-3">
			<label>Start Time</label>
			<input type="time" autocomplete="off"  required name="start_time" class="form-control" value="<?php echo $exam_sessions->start_time; ?>">
		</div>
		<div class="form-group col-md-3">
			<label>End Time</label>
			<input type="time" autocomplete="off" required name="end_time" class="form-control" value="<?php echo $exam_sessions->end_time; ?>">
		</div>
	</div>
	<input type="hidden" required name="id" class="form-control" value="<?php echo $exam_sessions->id; ?>">
	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
