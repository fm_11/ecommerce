<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th><?php echo $this->lang->line('sl'); ?></th>
			<th><?php echo $this->lang->line('name'); ?></th>
			<th>Header</th>
			<th>Start Time</th>
			<th>End Time</th>
			<th><?php echo $this->lang->line('actions'); ?></th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = 0;
		foreach ($exam_sessions as $row):
			$i++;
			?>

			<tr>
				<td>
					<?php echo $i; ?>
				</td>
				<td><?php echo $row['name']; ?></td>
			    <td><?php echo $row['header']; ?></td>
				<td><?php echo date("g:i A", strtotime($row['start_time'])); ?></td>
				<td><?php echo date("g:i A", strtotime($row['end_time'])); ?></td>
				<td>

					<div class="dropdown">
						<button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="ti-pencil-alt"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
							<a class="dropdown-item" href="<?php echo base_url(); ?>exam_sessions/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
							<a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>exam_sessions/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
						</div>
					</div>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
