
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>groups/add" method="post">
<div class="form-row">
      <div class="form-group col-md-9">
        <label for="inputEmail4">Name</label>
        <input type="text" autocomplete="off"  class="form-control" id="name" required name="name" value="">
      </div>
	<div class="form-group col-md-3">
		<label>Order</label>
		<input type="number" autocomplete="off"  class="form-control" name="order" value="" required="1"/>
	</div>
  </div>
  <div class="float-right">
      <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
