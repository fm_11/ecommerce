
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>groups/edit" method="post">

  <div class="form-row">
      <div class="form-group col-md-9">
         <label>Group</label>
         <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $group[0]['name']; ?>" required="1"/>
      </div>
	  <div class="form-group col-md-3">
		  <label>Order</label>
		  <input type="number" autocomplete="off"  class="form-control" name="order" value="<?php echo $group[0]['order']; ?>" required="1"/>
	  </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $group[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
