<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>online_payment_gateway_setup/index" method="post">
	<div class="form-row">

		<div class="form-group col-md-3">
			<label>Default User for Online Payment</label>
			<select class="js-example-basic-single w-100" required id="user_id_for_payment" name="user_id_for_payment">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php

				if (count($users)) {
					foreach ($users as $list) {
						?>
						<option
								value="<?php echo $list['id']; ?>" <?php if ($config->user_id_for_payment == $list['id']) {
							echo 'selected';
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				} ?>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label>Asset Head for Student Payment</label>
			<select class="js-example-basic-single w-100" required id="asset_head_id_for_student_payment" name="asset_head_id_for_student_payment">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php

				if (count($asset_category)) {
					foreach ($asset_category as $list) {
						?>
						<option
							value="<?php echo $list['id']; ?>" <?php if ($config->asset_head_id_for_student_payment == $list['id']) {
							echo 'selected';
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				} ?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Asset Head for Payment Receive</label>
			<select class="js-example-basic-single w-100" required id="asset_head_id_for_payment_receive" name="asset_head_id_for_payment_receive">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php

				if (count($asset_category)) {
					foreach ($asset_category as $list) {
						?>
						<option
							value="<?php echo $list['id']; ?>" <?php if ($config->asset_head_id_for_payment_receive == $list['id']) {
							echo 'selected';
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				} ?>
			</select>
		</div>


		<div class="form-group col-md-3">
			<label>Payable Amount Editable</label>
			<select class="js-example-basic-single w-100" id="payable_amount_editable" name="payable_amount_editable">
				<option value="0" <?php if ($config->payable_amount_editable == '0') {
					echo 'selected'; } ?>>No</option>
				<option value="1" <?php if ($config->payable_amount_editable == '1') {
					echo 'selected'; } ?>>Yes</option>
			</select>
		</div>


	</div>

	<input type="hidden" name="id" value="<?php echo $config->id; ?>">

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
