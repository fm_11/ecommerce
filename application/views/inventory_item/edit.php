<script type="text/javascript">
    function getSubCategoryByCategoryId(category_id){
        // alert(category_id);
        document.getElementById("sub_category_id").innerHTML = "<option>--Please Select---</option>";
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("sub_category_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>inventory_item/ajax_subcategory_by_category?category_id=" + category_id, true);
        xmlhttp.send();
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>inventory_item/edit/<?php echo $row_data->id; ?>" method="post">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('item').' '.$this->lang->line('name') ?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $row_data->name; ?>" required="1"/>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('code')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="code" value="<?php echo $row_data->code; ?>" required="1"/>
    </div>
	  <div class="form-group col-md-4">
		  <label><?php echo $this->lang->line('warehouse').' '.$this->lang->line('name') ?> <span class="required_label">*</span></label>
		  <select  name="ware_house_id" class="js-example-basic-single w-100" required="required">
			  <?php foreach ($warehouse as $row) { ?>
				  <option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->ware_house_id)) {
					  if ($row['id'] == $row_data->ware_house_id) {
						  echo 'selected';
					  }
				  } ?>><?php echo $row['name']; ?></option>
			  <?php } ?>
		  </select>
	  </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label><?php echo $this->lang->line('category').' '.$this->lang->line('name') ?> <span class="required_label">*</span></label>
      <select onchange="getSubCategoryByCategoryId(this.value)" name="category_id" class="js-example-basic-single w-100" required="required">
           <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
		  <?php foreach ($category as $row) { ?>
			<option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->category_id)) {
			 if ($row['id'] == $row_data->category_id) {
			   echo 'selected';
			 }
		   } ?>><?php echo $row['category_name']; ?></option>
		  <?php } ?>
     </select>
    </div>
    <div class="form-group col-md-3">
		  <label><?php echo $this->lang->line('sub').' '.$this->lang->line('category').' '.$this->lang->line('name') ?> <span class="required_label">*</span></label>
		  <select  name="sub_category_id" class="js-example-basic-single w-100" id="sub_category_id" required="required">
			  <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
			  <?php foreach ($subcategory as $row) { ?>
				<option value="<?php echo $row['id']; ?>" <?php if (isset($row_data->sub_category_id)) {
				 if ($row['id'] == $row_data->sub_category_id) {
				   echo 'selected';
				 }
			   } ?>><?php echo $row['name']; ?></option>
			  <?php } ?>
		 </select>
    </div>
    <div class="form-group col-md-3">
      <label>Opening <?php echo $this->lang->line('quantity')?> <span class="required_label">*</span></label>
              <input type="text" name="opening_quantity" autocomplete="off" class="form-control" value="<?php echo $row_data->opening_quantity; ?>" required="1">
    </div>
    <div class="form-group col-md-3">
        <label>Sales Price <span class="required_label">*</span></label>
	    <input type="number" name="sales_price" autocomplete="off" class="form-control" value="<?php echo $row_data->sales_price; ?>" required="1">
    </div>
  </div>

    <div class="float-right">
	   <input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $row_data->id; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
