
<?php
$name = $this->session->userdata('name');
?>
<form  name="addForm" class="cmxform" id="commentForm"  method="post" action="<?php echo base_url(); ?>inventory_item/index">
	<div class="form-row">
		<div class="form-group col-md-3">
			<input type="text" autocomplete="off"  style="padding: 10px !important;" name="name" placeholder="Item Name" value="<?php if (isset($name)) {
				echo $name;
			} ?>" class="form-control" id="name">
		</div>
		<div class="form-group col-md-2">
			<button type="submit" style="padding: 13px 30px 13px 30px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
		</div>
	</div>
</form>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
    <table id="sortable-table-1" class="table table-striped">
    <thead>
    <tr>
      <th scope="col"><?php echo $this->lang->line('sl')?></th>
      <th scope="col"><?php echo $this->lang->line('code')?></th>
      <th scope="col"><?php echo $this->lang->line('name')?></th>
      <th scope="col"><?php echo $this->lang->line('sub') . ' ' . $this->lang->line('category')?></th>
      <th scope="col">Opening Quantity</th>
      <th scope="col">Price</th>
      <th scope="col"><?php echo $this->lang->line('actions')?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($item as $row):
        $i++;
        ?>
        <tr>
        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['code']; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['sname']; ?></td>
            <td><?php echo $row['opening_quantity']; ?></td>
            <td><?php echo $row['sales_price']; ?></td>
            <td>
                <div class="dropdown">
                    <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-pencil-alt"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                        <a class="dropdown-item" href="<?php echo base_url(); ?>inventory_item/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                        <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>inventory_item/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                    </div>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
