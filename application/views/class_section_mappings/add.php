<script>
	function checkCheckBox() {
		var inputElems = document.getElementsByTagName("input"),
			count = 0;
		for (var i = 0; i < inputElems.length; i++) {
			if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
				count++;
			}
		}
		if (count < 1) {
			alert("Please select some section");
			return false;
		} else {
			return true;
		}
	}

	function checkItemAll(ele) {
		var checkboxes = document.querySelectorAll("input[type='checkbox']");
		if (ele.checked) {
			for (var i = 0; i < checkboxes.length; i++) {
				checkboxes[i].checked = true;
			}
		} else {
			for (var i = 0; i < checkboxes.length; i++) {
				checkboxes[i].checked = false;
			}
		}
	}
</script>

<form name="addForm" class="cmxform" id="commentForm" onsubmit="return checkCheckBox()" action="<?php echo base_url(); ?>class_section_mappings/add" method="post">

	<div class="form-row">
		<div class="form-group col-md-4">
			<label for="class">Classes</label><br>
			<select class="js-example-basic-multiple w-100" multiple name="class_id[]" id="class_id" required="1">
				<?php
				if (count($class_list)) {
					foreach ($class_list as $list) {
						?>
						<option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-inline">
		<div class="form-group">
			<div class="form-check form-check-success">
				<label class="form-check-label">
					<input type="checkbox" onclick="checkItemAll(this)" class="form-check-input">
					<i class="input-helper"></i>
				</label>
			</div>
			<input style="padding:10px; width: 256px; background-color: #3D3D3D; color: white;" type="text" readonly value="Select Section for Selected Class" class="form-control" /> &nbsp;
		</div>
	</div>

	<hr>

	<?php
	$i = 0;
	foreach ($section_info as $row){
		?>

		<div class="form-inline">
			<div class="form-group">
				<div class="form-check form-check-success">
					<label class="form-check-label">
						<input type="checkbox" name="selected_item_<?php echo $i; ?>" value="<?php echo $i; ?>"
							   class="form-check-input">
						<i class="input-helper"></i>
					</label>
				</div>
				<input type="hidden" name="section_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>">
				<input style="padding:10px; width: 256px;" type="text" readonly value="<?php echo $row['name']; ?>" class="form-control" /> &nbsp;
			</div>
		</div>


		<?php $i++; } ?>

	<input type="hidden" name="total_row" value="<?php echo $i; ?>">

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>

</form>
