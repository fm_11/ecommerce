<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col"><?php echo $this->lang->line('sl') ?></th>
        <th scope="col"><?php echo $this->lang->line('sub').' '.$this->lang->line('category').' '.$this->lang->line('name') ?></th>
        <th scope="col"><?php echo $this->lang->line('category')?></th>
        <th scope="col"><?php echo $this->lang->line('code')?></th>
        <th scope="col"><?php echo $this->lang->line('actions')?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($subcategory as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['category_name']; ?></td>
            <td><?php echo $row['code']; ?></td>
            <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" href="<?php echo base_url(); ?>inventory_subcategory/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>inventory_subcategory/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
	<?php echo $this->pagination->create_links(); ?>
</div>
</div>
