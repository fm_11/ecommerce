<?php
 if (isset($is_pdf) && $is_pdf == 1) {
     ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<style>

body {
    font-family: 'SolaimanLipi', Arial, sans-serif !important;
}
table, td, th {
  border: 1px solid black;
}
table {
  border-collapse: collapse;
}
</style>

<?php
 } ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <td colspan="2" style="text-align:center;">
            <b><?php echo $HeaderInfo['school_name']; ?><br>
            <?php if ($HeaderInfo['eiin_number'] != '') {
     echo "EIIN: " . $HeaderInfo['eiin_number'] . "<br>";
 } ?>
            <?php echo $title; ?>
           </b>
        </td>
    </tr>

    <tr>
       <td align="center" colspan="2">
          <b><u>Date: <?php echo date("d-m-Y", strtotime($date)); ?></u></b>
       </td>
    </tr>



    <tr>
        <td scope="col" align="center"><b>Ledger</b></td>

        <td scope="col" align="center"><b>Balance</b></td>
    </tr>

    <tr>
        <td colspan="2" scope="col" align="left"><b>Asset</b></td>
    </tr>

    </thead>
    <tbody>
    <?php
    $i = 0;
    $total_asset = 0;
    foreach ($idata as $row):
        $i++;
        ?>
        <tr>
            <td><?php echo $row['name']; ?></td>

            <td align="right">
              <?php
               $total_asset += $row['balance'];
               echo number_format($row['balance'], 2);
              ?>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr>
        <td><b>Total</b></td>
        <td align="right">
          <b>
            <?php
            echo number_format($total_asset, 2);
            ?>
         </b>
        </td>
    </tr>

    <tr>
        <td colspan="2" scope="col" align="left"><b>Liabilities</b></td>
    </tr>

    <?php
    $total_liabilities = 0;
    foreach ($liabilities as $l_row):
        ?>
        <tr>
            <td><?php echo $l_row['name']; ?></td>

            <td align="right">
              <?php
               $total_liabilities += $l_row['opening_balance'];
               echo number_format($l_row['opening_balance'], 2);
              ?>
            </td>
        </tr>
    <?php endforeach; ?>


    <tr>
        <td><b>Total</b></td>
        <td align="right">
          <b>
            <?php
            echo number_format($total_liabilities, 2);
            ?>
         </b>
        </td>
    </tr>

    <tr>
        <td><b> Net Income(Profit/Loss)</b></td>
        <td align="right">
          <b>
            <?php
            echo number_format($total_asset - $total_liabilities, 2);
            ?>
         </b>
        </td>
    </tr>

    </tbody>
</table>
</div>
