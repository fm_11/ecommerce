<script type="text/javascript">
    function getStudentByClassShiftSectionYear(class_shift_section_id,year){
        //alert(class_shift_section_id + '-' + year);
        if(class_shift_section_id != '' && year != ''){
          if (window.XMLHttpRequest)
          {
              xmlhttp = new XMLHttpRequest();
          }
          else
          {
              xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange = function()
          {
              if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
              {
                  //alert(xmlhttp.responseText);
                  document.getElementById("student_id").innerHTML = xmlhttp.responseText;
              }
          }
          xmlhttp.open("GET", "<?php echo base_url(); ?>students/getStudentByClassShiftSectionYear?class_shift_section_id=" + class_shift_section_id + "&&year=" + year, true);
          xmlhttp.send();
        }
    }

    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>student_fee_status_details/index" method="post">

  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="Year">Year</label>
      <select class="js-example-basic-single w-100" onchange="getStudentByClassShiftSectionYear(document.getElementById('class_shift_section_id').value,this.value)" name="year" id="year" required="1">
          <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
          <?php
          $i = 0;
          if (count($years)) {
              foreach ($years as $list) {
                  $i++; ?>
                  <option
                      value="<?php echo $list['value']; ?>"
                      <?php if (isset($year)) {
                      if ($year == $list['value']) {
                          echo 'selected';
                      }
                  } ?>>
                  <?php echo $list['text']; ?>
                </option>
                  <?php
              }
          }
          ?>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
      <select class="js-example-basic-single w-100"
       onchange="getStudentByClassShiftSectionYear(this.value,document.getElementById('year').value)"
       name="class_shift_section_id" id="class_shift_section_id" required>
          <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
          <?php
          $i = 0;
          if (count($class_section_shift_marge_list)) {
              foreach ($class_section_shift_marge_list as $list) {
                  $i++; ?>
                  <option
                      value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
                      <?php if (isset($class_shift_section_id)) {
                      if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
                          echo 'selected';
                      }
                  } ?>>
                  <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
                </option>
                  <?php
              }
          }
          ?>
      </select>
    </div>
    <div class="form-group col-md-3">
        <label><?php echo $this->lang->line('group'); ?></label>
        <select class="js-example-basic-single w-100" name="group_id" id="group_id" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($groups)) {
                foreach ($groups as $list) {
                    $i++; ?>
                    <option
                        value="<?php echo $list['id']; ?>" <?php  if (isset($group_id)) { if ($group_id == $list['id']) {
                        echo 'selected';
                    }} ?>><?php echo $list['name']; ?></option>
                    <?php
                }
            }
            ?>
        </select>
    </div>
    <div class="form-group col-md-3">
      <label><?php echo $this->lang->line('student'); ?></label>
      <select class="js-example-basic-single w-100" name="student_id" id="student_id" required="1">
          <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
          <?php
          $i = 0;
          if (count($students)) {
              foreach ($students as $list) {
                  $i++; ?>
                  <option
                      value="<?php echo $list['id']; ?>" <?php if (isset($student_id)) {
                      if ($student_id == $list['id']) {
                          echo 'selected';
                      }
                  } ?>><?php echo $list['name']. "(". $list['roll_no'] . ")"; ?></option>
                  <?php
              }
          }
          ?>
      </select>

    </div>
  </div>


  <div class="btn-group float-right">
    <input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
    <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
    <input type="submit" class="btn btn-primary" value="View Report">
  </div>

</form>

<div id="printableArea">
    <?php
    if (isset($rdata)) {
        echo $report;
    }
    ?>
</div>
