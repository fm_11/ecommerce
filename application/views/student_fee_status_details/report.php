<?php
 if (isset($is_pdf) && $is_pdf == 1) {
     ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<style>

body {
    font-family: 'SolaimanLipi', Arial, sans-serif !important;
}
table, td, th {
  border: 1px solid black;
  font-size: 11px;
  padding-left: 3px !important;
  padding-right: 3px !important;
  padding: 0px;
}
table {
  border-collapse: collapse;
}
.h_td{
   font-weight: bold !important;
}
</style>

<?php
 } ?>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
    <thead>
	<?php if(isset($HeaderInfo)){ ?>
    <tr>
        <td class="center_td" colspan="<?php echo count($sub_category) + 5; ?>" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
              <?php echo $title; ?>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="<?php echo count($sub_category) + 5; ?>">
            <?php echo $this->lang->line('name'); ?>: <b><?php echo $student_info[0]['name']; ?></b>,
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $student_info[0]['roll_no']; ?></b>,
            <?php echo $this->lang->line('roll_no'); ?>: <b><?php echo $student_info[0]['class_name']; ?></b>
            <?php echo $this->lang->line('shit'); ?>: <b><?php echo $student_info[0]['shift_name']; ?></b>
        </td>
    </tr>

	<?php } ?>

    <tr>
        <th>
            <?php echo $this->lang->line('month'); ?>
        </th>
        <th style="text-align:center;min-width:120px;">
            <?php echo $this->lang->line('receipt_no'); ?><br>
            <?php echo $this->lang->line('date'); ?>
        </th>
        <?php
        foreach ($sub_category as $row):
            ?>
            <th>
                <?php echo $row['name']; ?>
            </th>
        <?php endforeach; ?>

       <th><?php echo $this->lang->line('special_care'); ?></th>
       <th><?php echo $this->lang->line('transport'); ?></th>
       <th><b><?php echo $this->lang->line('total'); ?></b></th>

    </tr>
    </thead>

    <tbody>
    <?php
   //  echo '<pre>';
   //  print_r($rdata);
   // die;
    $i = 1;
    $grand_total_amount = 0;
    while ($i <= count($rdata)) {
       // echo '<pre>';
       // print_r($rdata);
       // die;

        $k = 0;
        ?>
        <tr>
            <td>
                <?php
                 $dateObj   = DateTime::createFromFormat('!m', $i);
                 echo $dateObj->format('F'); // March
                ?>
            </td>

            <td style="text-align:center;">
                    <?php
                    $receipt_no_date = "";
					$already_receipt_for = array();
                    foreach ($rdata[$i] as $row2):
                       if ($row2['receipt_no_and_date'] != '') {
						   $receipt_no_date_explode = explode("(",$row2['receipt_no_and_date']);
						   $receipt_no_date_explode_receipt_no = trim($receipt_no_date_explode[0]);
						  // echo $receipt_no_date_explode_receipt_no; die;

						   if(!in_array($receipt_no_date_explode_receipt_no, $already_receipt_for)){
						   	  // echo array_search($receipt_no_date_explode_receipt_no, $already_receipt_for) . '/';
							   $receipt_no_date .= $row2['receipt_no_and_date'];
							  // echo $receipt_no_date . '/';
							   $already_receipt_for[] = $receipt_no_date_explode_receipt_no;
						   }
                       }
                    endforeach;
                    //  die;
                    echo $receipt_no_date;
                    //die;
                    ?>
            </td>



            <?php
              $row_total_amount = 0;
              foreach ($rdata[$i] as $row1):
              $row_total_amount += $row1['paid_amount'];
              $grand_total_amount +=  $row1['paid_amount'];
            ?>
                <td style="text-align:center;">
                    <?php echo $row1['paid_amount']; ?>
                </td>

                <?php
            endforeach; ?>

            <td style="text-align:center;">
                <b><?php echo $row_total_amount; ?></b>
            </td>

        </tr>
        <?php
        $i++;
    }
    ?>

      <tr>
        <td colspan="<?php echo count($sub_category) + 4; ?>" style="text-align:right;"><b>Total</b></td>
        <td style="text-align:center;"><b><?php echo $grand_total_amount; ?></b></td>
      </tr>


    </tbody>

</table>
</div>
