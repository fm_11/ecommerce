<script>
function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 1; i < checkboxes.length; i++) {
                var cname = checkboxes[i].name.split("_"); 
                var test = cname[0] + cname[1];          
                if (checkboxes[i].type == 'checkbox' && test == 'isallow') {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 1; i < checkboxes.length; i++) {
                console.log(i)
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }
</script>

<?php
function searchForId($id, $array) {
   foreach ($array as $key => $val) {
       if ($val['date'] === $id) {
           return $key;
       }
   }
   return null;
}
?>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>holidays/batch_add_data_save" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
            <th scope="col">Date</th>
            <th scope="col">Holiday Type</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 1;
        while($i <= $num_of_days){
        ?>

            <tr>
                <td>
                    <?php 
                    $date = $year. '-'. sprintf("%02d", $month) . '-' . sprintf("%02d", $i);
                    ?>
                    <input type="checkbox" <?php if (in_array($date, array_column($holidays, 'date'))) {
                        echo 'checked';
                    } ?> name="is_allow_<?php echo $i; ?>">
                </td>
                <td>
                    <?php 
                         echo $date . ' (' . date('l', strtotime($date)) . ')';		
                    ?>
                    <input type="hidden" class="input-text-short" size="8"
                           style="text-align: right; width:75px;"
                           name="date_<?php echo $i; ?>" value="<?php echo $date; ?>"/>
                </td>
                <td>
                    <?php
                     $holiday_type_id = "";
                      if (in_array($date, array_column($holidays, 'date'))) {
                           $key = array_search($date, array_column($holidays, 'date'));
                           $holiday_type_id = $holidays[$key]['holiday_type'];
                      }else{
                          $holiday_type_id = 1;
                      }
                    ?>
                    <select class="smallInput" name="txtHolidayType_<?php echo $i; ?>">
                        <option value="">-- Please Select --</option>
                        <?php
                        if (count($holiday_types)) {
                            foreach ($holiday_types as $list) {
                                ?>
                                <option value="<?php echo $list['id']; ?>" 
                                <?php if($holiday_type_id != ""){if($list['id'] == $holiday_type_id) {
                                     echo 'selected="selected"';
                                  }} ?>><?php echo $list['name']; ?></option>
                            <?php
                            }
                        }
                        ?>
                    </select>
                </td>
                
            </tr>
        <?php  $i++; } ?>
        </tbody>
    </table>
	</div>
    <br>
    
    <input type="hidden" class="input-text-short" name="year"
           value="<?php echo $year; ?>"/>

    <input type="hidden" class="input-text-short" name="month"
           value="<?php echo $month; ?>"/>
    
    <input type="hidden" class="input-text-short" name="num_of_days"
           value="<?php echo $num_of_days; ?>"/>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-primary" value="Save">
	</div>

</form>

