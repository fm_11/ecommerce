<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>holidays/batch_add" method="post">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
		<div class="form-row">
			<div class="form-group col-md-4">
				<label>Month</label>
				<select class="js-example-basic-single w-100" name="month" id="month" required="1">
					<option value="">-- Please Select --</option>
					<?php
					$i = 1;
					while ($i <= 12) {
						$dateObj = DateTime::createFromFormat('!m', $i);
						?>
						<option value="<?php echo $i; ?>" <?php if(isset($month)){ if($month == $i){ echo 'selected'; }} ?>><?php echo $dateObj->format('F'); ?></option>
						<?php
						$i++;
					}
					?>
				</select>
			</div>
			<div class="form-group col-md-4">
				<label for="Year">Year</label>
				<select class="js-example-basic-single w-100" name="year" id="year" required="1">
					<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					<?php
					$i = 0;
					if (count($years)) {
						foreach ($years as $list) {
							$i++; ?>
							<option
								value="<?php echo $list['value']; ?>"
								<?php if (isset($year)) {
									if ($year == $list['value']) {
										echo 'selected';
									}
								} ?>>
								<?php echo $list['text']; ?>
							</option>
							<?php
						}
					}
					?>
				</select>
			</div>
		</div>
	</table>
	<div class="btn-group float-right">
		<input type="submit" class="btn btn-primary" value="Process">
	</div>

</form>


<?php
if (isset($holidays)) {
	echo $process_table;
}
?>
