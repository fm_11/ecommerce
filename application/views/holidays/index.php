<div class="float-right">
	<a class="btn btn-outline-primary" href="<?php echo base_url(); ?>holidays/batch_add">Batch Add</a>
</div>

<?php
$holiday_type_id = $this->session->userdata('holiday_type_id');
$from_date = $this->session->userdata('from_date');
$to_date = $this->session->userdata('to_date');
?>
<form name="addForm" class="cmxform" id="commentForm"  method="post" action="<?php echo base_url(); ?>holidays/index">
	<div class="form-row">
		<div class="form-group col-md-3">
			<select class="js-example-basic-single  w-100" name="holiday_type_id" style="padding: 10px !important;" id="holiday_type_id">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($holiday_types)) {
					foreach ($holiday_types as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['id']; ?>"
							<?php if (isset($holiday_type_id)) {
								if ($holiday_type_id == $list['id']) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" style="padding: 10px !important;" placeholder="yyyy-mm-dd"  name="from_date" <?php if (isset($from_date)) { ?>
					value="<?php echo $from_date; ?>"
				<?php } ?> class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                 <i class="simple-icon-calendar"></i>
             </span>
			</div>
		</div>
		<div class="form-group col-md-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" style="padding: 10px !important;" placeholder="yyyy-mm-dd"  name="to_date" <?php if (isset($to_date)) { ?> value="<?php echo $to_date; ?>"
				<?php } ?>  class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                 <i class="simple-icon-calendar"></i>
             </span>
			</div>
		</div>

		<div class="form-group col-md-2">
			<button type="submit" style="padding: 13px 30px 13px 30px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
		</div>
	</div>
</form>

<hr>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">Holiday Type</th>
        <th  scope="col">Date</th>
        <th scope="col">Reason</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($holidays as $row):
        $i++;
        ?>
        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['holiday_name']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo $row['remarks']; ?></td>
            <td>
				<div class="dropdown">
					<button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="ti-pencil-alt"></i>
					</button>
					<div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
						<a class="dropdown-item" href="<?php echo base_url(); ?>holidays/edit/<?php echo $row['id']; ?>"
						   title="Edit"><?php echo $this->lang->line('edit'); ?></a>
						<a class="dropdown-item" onclick="return deleteConfirm()"
						   href="<?php echo base_url(); ?>holidays/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
					</div>
				</div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>
