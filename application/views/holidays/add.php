<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>holidays/add" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Holiday Type</label>
			<select class="js-example-basic-multiple w-100" name="txtHolidayType" required="1">
				<option value="">-- Please Select --</option>
				<?php
				$i = 0;
				if (count($holiday_types)) {
					foreach ($holiday_types as $list) {
						$i++;
						?>
						<option
							value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-6">
			<label for="Date">Date</label>
			<div class="input-group date">
				<input type="text" value="" autocomplete="off"
					   name="txtDate" id="txtDate" required class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
			</div>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-12">
			<label for="txtRemarks">Remarks</label>
			<textarea rows="5" cols="50" class="form-control" name="txtRemarks"></textarea>
		</div>
	</div>


	<div class="float-right">
		<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
    </form>
