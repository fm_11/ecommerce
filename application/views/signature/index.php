
<form name="addForm" class="cmxform" id="commentForm"
      action="<?php echo base_url(); ?>signature/data_save" enctype="multipart/form-data" method="post">
      <div class="table-sorter-wrapper col-lg-12 table-responsive">
        <table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Level</th>
            <th scope="col">Is Use?</th>
            <th scope="col">Is Use Class Teacher Signature?</th>
            <th scope="col">Signature</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($signatures as $row):
            $i++;
            ?>
            <tr>

            <tr>
              <td>
                     <img style="height:50px; width:80px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/<?php if ($row['location']!='') {
                echo 'signature/'.$row['location'];
            } else {
                echo 'img/not_found.jpg';
            } ?>">
                 </td>

                <td>
                    <?php echo $row['title']; ?>
                </td>

                <td>
                    <input type="text" autocomplete="off"  style="padding:6px;" value="<?php echo $row['level']; ?>" name="level_<?php echo $i; ?>"/>
                </td>

                <td style="text-align:center;">
                  <select name="is_use_<?php echo $i; ?>" required="required">
                      <option value="0" <?php if (isset($row['is_use'])) {
                if ($row['is_use'] == '0') {
                    echo 'selected';
                }
            } ?>>No
                      </option>
                      <option value="1" <?php if (isset($row['is_use'])) {
                if ($row['is_use'] == '1') {
                    echo 'selected';
                }
            } ?>>Yes
                      </option>
                  </select>
                </td>

                <td style="text-align:center;">
                  <select name="is_use_class_teacher_<?php echo $i; ?>" required="required">
                      <option value="0" <?php if (isset($row['is_use_class_teacher'])) {
                if ($row['is_use_class_teacher'] == '0') {
                    echo 'selected';
                }
            } ?>>No
                      </option>
                      <option value="1" <?php if (isset($row['is_use_class_teacher'])) {
                if ($row['is_use_class_teacher'] == '1') {
                    echo 'selected';
                }
            } ?>>Yes
                      </option>
                  </select>
                </td>

        				<td>
                     <input type="hidden"  name="id_<?php echo $i; ?>"  value="<?php echo $row['id']; ?>">
        				     <input type="file" id="file_<?php echo $i; ?>" name="file_<?php echo $i; ?>" />
        				</td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
  </div>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>

   <div class="float-right">
   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
   </div>

</form>
