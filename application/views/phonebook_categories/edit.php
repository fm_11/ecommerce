
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>phonebook_categories/edit" method="post">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('category') . ' ' . $this->lang->line('name'); ?></label>
			<input required type="text" name="categoryName" class="form-control" value="<?php echo $categoryInfo[0]['name']; ?>">
		</div>

		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('category') . ' ' . $this->lang->line('note'); ?></label>
			<input required type="text" name="CategoryNote" class="form-control" value="<?php echo $categoryInfo[0]['remarks']; ?>">
		</div>

	</div>

	<div class="float-right">
    <input required type="hidden" name="id" class="form-control" value="<?php echo $categoryInfo[0]['id']; ?>">
		 <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
