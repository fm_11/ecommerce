<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>store_management/add_warehouse_info"><span>Add Category</span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Name</th>
        <th width="200" scope="col">Location</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    /* echo '<pre>';
    print_r($warehouse_info); */
    foreach ($warehouse_info as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34"><?php echo $i; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['location']; ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>store_management/warehouse_edit/<?php echo $row['id']; ?>"
                   title="Edit">Edit</a>&nbsp;
                <a href="<?php echo base_url(); ?>store_management/warehouse_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>



