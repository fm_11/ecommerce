<?php
if($action == 'edit'){
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/student_fee_sub_category_edit/" method="post">
        <label>Name</label>
        <input type="text" autocomplete="off"  name="name" class="smallInput wide" required="1" size="20%" value="<?php echo $fee_sub_category_info[0]['name']; ?>">
        <label>Category</label>
        <select class="smallInput" name="category_id" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($fee_category_info)) {
                foreach ($fee_category_info as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"<?php if($fee_sub_category_info[0]['category_id'] == $list['id']){echo 'selected';} ?>><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>
		
		<label>Amount</label>
        <input type="text" autocomplete="off"  name="amount" class="smallInput wide" size="20%" value="<?php echo $fee_sub_category_info[0]['amount']; ?>" required="1">
		
		<label>Fee Type</label>
        <select class="smallInput" name="fee_type" required="1">
            <option value="">-- Please Select --</option>
            <option value="A" <?php if($fee_sub_category_info[0]['fee_type'] == 'A'){echo 'selected';} ?>>Annual</option>
			<option value="A" <?php if($fee_sub_category_info[0]['fee_type'] == 'T'){echo 'selected';} ?>>Tri-Annual</option>
			<option value="Q" <?php if($fee_sub_category_info[0]['fee_type'] == 'Q'){echo 'selected';} ?>>Quarterly</option>
			<option value="M" <?php if($fee_sub_category_info[0]['fee_type'] == 'M'){echo 'selected';} ?>>Monthly</option>
        </select>
		
		
		
        <label>Remarks</label>
        <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="remarks"><?php echo $fee_sub_category_info[0]['remarks']; ?></textarea>

        <br>
        <br>
        <input type="hidden" name="id" value="<?php echo $fee_sub_category_info[0]['id']; ?>">
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
<?php
}
else{
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>Store_management/add_sub_category" method="post">
		<label>Category</label>
        <select class="smallInput" name="category_id" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($category_info)) {
                foreach ($category_info as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['category_name'].'('. $list['code'] .')'; ?></option>
                <?php
                }
            }
            ?>
        </select>
		
		<label>Sub Category Name</label>
        <input type="text" autocomplete="off"  name="name" class="smallInput wide" size="20%" value="" required="1">

        <label>Code</label>
        <input type="text" autocomplete="off"  name="code" class="smallInput wide" size="20%" value="" required="1">


        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
    </form>
<?php
}
?>