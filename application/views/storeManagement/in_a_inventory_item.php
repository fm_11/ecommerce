<script type="text/javascript">
    function getSubCategoryByCategoryId(category_id) {
        if (category_id == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("sub_category_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>Store_management/getSubCategoryByCategoryId?category_id=" + category_id, true);
        xmlhttp.send();
    }

    function getItemBySubCategoryId(sub_category_id){
        if (sub_category_id == '') {
            return false;
        }
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("item_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>Store_management/getItemBySubCategoryId?sub_category_id=" + sub_category_id, true);
        xmlhttp.send();
    }

    $(function() {
        $( "#warrenty_to,#warrenty_from" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>


<?php
if ($action == 'edit') {
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fees/student_fee_sub_category_edit/" method="post">
        <label>Name</label>
        <input type="text" autocomplete="off"  name="name" class="smallInput wide" required="1" size="20%"
               value="<?php echo $fee_sub_category_info[0]['name']; ?>">
        <label>Category</label>
        <select class="smallInput" name="category_id" id="category_id" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($fee_category_info)) {
                foreach ($fee_category_info as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"<?php if ($fee_sub_category_info[0]['category_id'] == $list['id']) {
                        echo 'selected';
                    } ?>><?php echo $list['name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Amount</label>
        <input type="text" autocomplete="off"  name="amount" class="smallInput wide" size="20%"
               value="<?php echo $fee_sub_category_info[0]['amount']; ?>" required="1">

        <label>Fee Type</label>
        <select class="smallInput" name="fee_type" required="1">
            <option value="">-- Please Select --</option>
            <option value="A" <?php if ($fee_sub_category_info[0]['fee_type'] == 'A') {
                echo 'selected';
            } ?>>Annual
            </option>
            <option value="A" <?php if ($fee_sub_category_info[0]['fee_type'] == 'T') {
                echo 'selected';
            } ?>>Tri-Annual
            </option>
            <option value="Q" <?php if ($fee_sub_category_info[0]['fee_type'] == 'Q') {
                echo 'selected';
            } ?>>Quarterly
            </option>
            <option value="M" <?php if ($fee_sub_category_info[0]['fee_type'] == 'M') {
                echo 'selected';
            } ?>>Monthly
            </option>
        </select>


        <label>Remarks</label>
        <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30"
                  name="remarks"><?php echo $fee_sub_category_info[0]['remarks']; ?></textarea>

        <br>
        <br>
        <input type="hidden" name="id" value="<?php echo $fee_sub_category_info[0]['id']; ?>">
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form>
<?php
} else {
    /* echo '<pre>';
    print_r ($account_froup_info); */
    ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>Store_management/add_inventory_item_in" method="post">
        <label>Category</label>
        <select class="smallInput" onchange="getSubCategoryByCategoryId(this.value)" name="category_id" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($category_info)) {
                foreach ($category_info as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['category_name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Sub Category</label>
        <select class="smallInput" onchange="getItemBySubCategoryId(this.value)" name="sub_category_id" id="sub_category_id" required="1">
            <option value="">-- Please Select --</option>
        </select>

        <label>Item</label>
        <select class="smallInput" name="item_id" id="item_id" required="1">
            <option value="">-- Please Select --</option>
        </select>

        <label>Warehouse</label>
        <select class="smallInput" name="ware_house_id" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($ware_house)) {
                foreach ($ware_house as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['name'] . '(' . $list['location'] . ')'; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Vendor</label>
        <select class="smallInput" name="vendor_id" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($vendor_info)) {
                foreach ($vendor_info as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['company_name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Quantity</label>
        <input type="text" autocomplete="off"  name="quantity" class="smallInput" size="20%" value="" required="1">

        <label>Unit</label>
        <select class="smallInput" name="unit_id" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($item_unit)) {
                foreach ($item_unit as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"><?php echo $list['unit_name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Price</label>
        <input type="text" autocomplete="off"  name="price" class="smallInput" size="20%" value="" required="1">


        <label>Warranty From</label>
        <input type="text" autocomplete="off"  name="warrenty_from" id="warrenty_from" class="smallInput" size="20%" value="" required="1">

        <label>Warranty To</label>
        <input type="text" autocomplete="off"  name="warrenty_to" id="warrenty_to" class="smallInput" size="20%" value="" required="1">

        <label>Remarks</label>
        <textarea type="text" name="remarks" class="smallInput wide"></textarea>

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
    </form>
<?php
}
?>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>