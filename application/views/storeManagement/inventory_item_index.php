<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>store_management/add_inventory_item"><span>Add Inventory Item</span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Category</th>
        <th width="100" scope="col">Sub Category</th>
        <th width="200" scope="col">Item Name</th>
        <th width="100" scope="col">Quantity</th>
        <th width="200" scope="col">Warehouse</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    /* echo '<pre>';
    print_r($inventory_item_info); */
    foreach ($inventory_item_info as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['category_name']; ?></td>
            <td><?php echo $row['sub_category_name']; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['quantity'] . ' ' . $row['unit_name']; ?></td>
            <td><?php echo $row['warehouse']; ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>store_management/inventory_item_edit/<?php echo $row['id']; ?>"
                   title="Edit">Edit</a>&nbsp;
                <a href="<?php echo base_url(); ?>store_management/inventory_item_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>



