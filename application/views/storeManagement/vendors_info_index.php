<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>store_management/add_vendors"><span>Add Vendors</span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Company Name</th>
        <th width="200" scope="col">Contact Person</th>
        <th width="200" scope="col">Purchase Order</th>
        <!--<th width="200" scope="col">Group Under</th>-->

        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    /* echo '<pre>';
    print_r($vendors_info);  */
    foreach ($vendors_info as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['company_name']; ?></td>
            <td><?php echo $row['contact_person_name']; ?></td>
            <td><?php echo "Purchase Order"; ?></td>

            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>store_management/vendor_edit/<?php echo $row['id']; ?>"
                   title="Edit">Edit</a>&nbsp;
                <a href="<?php echo base_url(); ?>store_management/vendor_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>


