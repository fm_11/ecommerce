<script>
	function checkCheckBox() {
		var inputElems = document.getElementsByTagName("input"),
			count = 0;
		for (var i = 0; i < inputElems.length; i++) {
			if (inputElems[i].type == "checkbox" && inputElems[i].checked == true) {
				var rowSl = inputElems[i].value;
				var groups = document.getElementById("groups_" + rowSl).value;
				if(groups == ''){
					var subject_name = document.getElementById("subject_name_" + rowSl).value;
					alert("Please select group for " + subject_name);
					return false;
				}
				count++;
			}
		}

		if (count < 1) {
			alert("Please select some subject");
			return false;
		} else {
			return true;
		}
	}

	function checkItemAll(ele) {
		var checkboxes = document.querySelectorAll("input[type='checkbox']");
		if (ele.checked) {
			for (var i = 0; i < checkboxes.length; i++) {
				checkboxes[i].checked = true;
			}
		} else {
			for (var i = 0; i < checkboxes.length; i++) {
				checkboxes[i].checked = false;
			}
		}
	}
</script>

<style>
	th,td{
		vertical-align: middle !important;
	}
</style>
<h4>Select Subject for Class - <?php echo $class_info->name; ?></h4>
<form name="addForm" class="cmxform" id="commentForm" onsubmit="return checkCheckBox()" action="<?php echo base_url(); ?>class_wise_subject_mappings/edit" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<thead>

			<tr>
				<th scope="col">
					<div class="form-check form-check-success">
						<label class="form-check-label">
							<input type="checkbox" onclick="checkItemAll(this)" class="form-check-input">
							<i class="input-helper"></i>
						</label>
					</div>
				</th>
				<th scope="col">Subject</th>
				<th scope="col">Groups</th>
				<th scope="col">Subject Type</th>
			</tr>
			</thead>
			<tbody>

			<?php
			$i = 0;
			foreach ($subject_list as $row){
			?>
			<tr>
				<td>
					<div class="form-check form-check-success">
						<label class="form-check-label">
							<input type="checkbox" name="selected_item_<?php echo $i; ?>" value="<?php echo $i; ?>"
								   class="form-check-input" <?php if($row['subject_id'] != ''){ echo 'checked="1"'; } ?>>
							<i class="input-helper"></i>
						</label>
					</div>

				</td>

				<td>
					<?php echo $row['name']; ?>
					<input type="hidden" id="subject_name_<?php echo $i; ?>" value="<?php echo $row['name']; ?>">
					<input type="hidden" name="subject_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>">
				</td>

				<td>
					<select id="groups_<?php echo $i; ?>" style="width:200px !important;" name="groups_<?php echo $i; ?>[]" multiple>
						<?php for ($C = 0; $C < count($groups); $C++) { ?>
							<option value="<?php echo $groups[$C]['id']; ?>" <?php if (in_array($groups[$C]['id'], explode(",", $row['group_list']))) { ?> selected="selected" <?php } ?>><?php echo $groups[$C]['name']; ?></option>
						<?php } ?>
					</select>
				</td>

				<td>
					<select class="form-control" style="color: black;" id="subject_type_<?php echo $i; ?>" name="subject_type_<?php echo $i; ?>">
						<option value="COM" <?php if ($row['subject_type'] == 'COM') {
							echo 'selected';
						} ?>>Compulsory</option>

						<option value="CHO" <?php if ($row['subject_type'] == 'CHO') {
							echo 'selected';
						} ?>>Choosable</option>

						<option value="UNC" <?php if ($row['subject_type'] == 'UNC') {
							echo 'selected';
						} ?>>Uncountable </option>
					</select>
				</td>
			</tr>
			<?php $i++; } ?>

			</tbody>
		</table>

		<input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
		<input type="hidden" name="total_row" value="<?php echo $i; ?>">

		<div class="float-right">
			<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
		</div>
	</div>
</form>
