<script>
	function setFromClassValue(class_id) {
		document.getElementById("from_class_id").value = class_id;
	}

	function checkSelectedClass(class_id) {
		var from_class_id = document.getElementById("from_class_id").value;
		if(from_class_id == class_id){
			alert("Both class can't be same. Please select an another class");
			document.getElementById("class_id").value = "";
			return false;
		}

		//check assign data
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		}
		else
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				if(xmlhttp.responseText == '0'){
					alert("Already subjects assigned for this class");
					document.getElementById("class_id").value = "";
					return false;
				}else{

					return true;
				}
			}
		}
		xmlhttp.open("GET", "<?php echo base_url(); ?>class_wise_subject_mappings/checkAlreadyAssignSubject?class_id=" + class_id, true);
		xmlhttp.send();

	}
</script>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th scope="col">Class</th>
			<th scope="col">Subjects</th>
			<th scope="col">Actions</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = 0;
		foreach ($class_list as $row):
			?>

			<tr>
				<td><?php echo $row['name']; ?></td>
				<td>
					<?php
					if($row['subjects_name'] == ''){
						echo '-';
					}else{
						echo $row['subjects_name'];
					}
					?>
				</td>
				<td>

					<a href="<?php echo base_url(); ?>class_wise_subject_mappings/edit/<?php echo $row['id']; ?>"
					   class="btn btn-warning btn-xs mb-1" title="Edit">Edit</a>

					<?php if($row['subjects_name'] != ''){ ?>
						<button type="button" onclick="setFromClassValue('<?php echo $row["id"]; ?>')" class="btn btn-success btn-xs mb-1" data-toggle="modal" data-target="#exampleModal-2">
							Copy
						</button>
					<?php } ?>

				</td>
			</tr>

		<?php $i++; endforeach; ?>
		</tbody>
	</table>
</div>


<!-- Modal starts -->
<div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel-2">Class Wise Subject Copy</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" action="<?php echo base_url(); ?>class_wise_subject_mappings/data_copy">
			<div class="modal-body">
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Select Class</label>
						<select onchange="checkSelectedClass(this.value)" class="form-control" name="to_class_id" id="class_id" required="1">
							<option value="">-- Please Select --</option>
							<?php
							$i = 0;
							if (count($class_list)) {
								foreach ($class_list as $list) {
									$i++; ?>
									<option value="<?php echo $list['id']; ?>">
										<?php echo $list['name']; ?>
									</option>
									<?php
								}
							} ?>
						</select>
						<input type="hidden" class="form-control" name="from_class_id" id="from_class_id">
					</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Copy</button>
				<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- Modal Ends -->
