<?php
$mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");
?>

<style>
#pdf_table{
     border-collapse: collapse;
}
#pdf_table td, #pdf_table th {
    border: 1px solid #00000;
    padding: 5px;
}
</style>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
<?php
     if (isset($is_pdf) && $is_pdf == 1) {
         ?>
       <table width="100%" id="pdf_table" style="font-size:13px;" summary="Employee Pay Sheet">
<?php
     } else {
         ?>
  <table id="sortable-table-1" class="table">
<?php
     }
?>
    <thead>
    <tr>
        <td class="center_td" colspan="9" style="text-align:center; font-size:15px;">
            <b>
              <?php echo $HeaderInfo['school_name']; ?>
            </b><br>
            <b style="font-size:15px;">
              <?php echo $HeaderInfo['address']; ?><br>

                <?php echo $title; ?>
            </b>
        </td>
    </tr>
    <tr>
        <td colspan="9">
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
            <?php echo $this->lang->line('date'); ?>: <b><?php echo date("M jS, Y", strtotime($from_date)); ?> - <?php echo date("M jS, Y", strtotime($to_date)); ?></b>,
            <?php echo $this->lang->line('report_for'); ?>: <b><?php if ($report_for == '1') {
    echo 'Resident';
} elseif ($report_for == '0') {
    echo 'Non Resident';
} else {
    echo 'All';
} ?></b>
        </td>
    </tr>

	 <tr>
        <th><?php echo $this->lang->line('sl'); ?></th>
        <th><?php echo $this->lang->line('student_code'); ?></th>
        <th><?php echo $this->lang->line('name'); ?></th>
        <th><?php echo $this->lang->line('roll'); ?></th>
        <th><?php echo $this->lang->line('class'); ?></th>
        <th><?php echo $this->lang->line('month'); ?>/<?php echo $this->lang->line('year'); ?></th>
        <th><?php echo $this->lang->line('receipt_no'); ?>.</th>
        <th><?php echo $this->lang->line('date'); ?> <?php echo $this->lang->line('time'); ?></th>
        <th><?php echo $this->lang->line('paid'); ?> <?php echo $this->lang->line('amount'); ?> (<?php echo $this->lang->line('tk'); ?>.)</th>
    </tr>

     </thead>

	  <tbody>
    <?php
    $i = 0;
    $total_amount = 0;
    foreach ($rdata as $row):
        $i++;
        ?>
        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td>
                <?php echo $row['student_code']; ?><br>
            </td>
			      <td>
              <?php echo $row['name']; ?><br>
            </td>
            <td>
              <?php echo $row['roll_no']; ?><br>
            </td>
            <td align="center">
            <?php
            echo $row['class_name'];
             ?>
            </td>
           <td align="center"><?php echo $mons[$row['month']].', '.$row['year']; ?></td>
            <td align="center"><?php echo $row['receipt_no']; ?></td>
            <td align="center"><?php echo $row['entry_date']; ?></td>
            <td align="right"><?php $total_amount+= ($row['total_amount'] + $row['total_late_fee'] + $row['total_transport_fee']); echo number_format(($row['total_amount'] + $row['total_late_fee'] + $row['total_transport_fee']), 2); ?></td>
      </tr>
    <?php endforeach; ?>
	   <tr>
	       <td align="right" colspan="8"><b><?php echo $this->lang->line('total'); ?></b></td>
		     <td align="right"><b><?php echo number_format($total_amount, 2); ?> </b></td>
	   </tr>

	     <tr>
            <td class="textleft" colspan="9"><b><?php echo $this->lang->line('in_words'); ?>: <?php echo $this->numbertowords->convert_number($total_amount); ?> Taka Only.</b></td>
        </tr>

    </tbody>
</table>
</div>
