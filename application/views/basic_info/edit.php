
<style>
   .required_class{
     color: red;
   }
</style>
<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>basic_info/edit/<?php echo $basic_info->id;  ?>" method="post" class="needs-validation" novalidate enctype="multipart/form-data">

                              <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label for="validationTooltip001">Name</label><span class="required_class">*</span>
                                    <input type="text" class="form-control" id="validationTooltip001" required name="name"
                                    value="<?php echo $basic_info->name;  ?>">
                                    <input type="hidden" class="form-control" id="validationTooltip001" required name="id"
                                    value="<?php echo $basic_info->id;  ?>">
                                 </div>
                                 <div class="form-group col-md-6">
                                     <label for="inputPassword4">Contact Number</label><span class="required_class">*</span>
                                     <input type="text" class="form-control" id="contact_number" required
                                         name="contact_number" value="<?php echo $basic_info->contact_number;  ?>">
                                 </div>
                                </div>
                                <div class="form-row">
                                  <div class="form-group col-md-4">
                                      <label for="txtMobile">Helpline</label><span class="required_class">*</span>
                                      <input type="text" class="form-control" name="help_line" value="<?php echo $basic_info->help_line; ?>" required id="help_line">
                                  </div>
                                  <div class="form-group col-md-4">
                                      <label for="inputPassword4">Bkash Number</label><span class="required_class">*</span>
                                      <input type="text" class="form-control" id="bikash_number" required  name="bikash_number" value="<?php echo $basic_info->bikash_number; ?>">
                                  </div>
                                    <div class="form-group col-md-4">
                                          <label for="Timezone">Logo</label><span class="required_class">*</span>
                                          <br>
                                          <input type="hidden" name="old_image_path" value="<?php echo $basic_info->logo_path; ?>"/>
                                          <img src="<?php echo base_url(); ?>media/tailor/<?php echo $basic_info->logo_path; ?>" style=" border:1px solid #666666;" width="150" height="150">
                                          <br><br>
                                          <input type="file" id="inputImage" name="logo_path"
                                              accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                      </div>
                                </div>



                                <button type="submit" class="btn btn-primary d-block mt-3">Update</button>
                    </form>
            </div>
       </div>
      </div>
</div>
