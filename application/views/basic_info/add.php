
<style>
   .required_class{
     color: red;
   }
</style>

<div class="row">
            <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"><?php echo $title; ?></h5>

                            <form action="<?php echo base_url(); ?>basic_info/add" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                      <label for="validationTooltip001">Name</label><span class="required_class">*</span>
                                      <input type="text" class="form-control" id="validationTooltip001" required name="name"
                                      value="">
                                   </div>
                                      <div class="form-group col-md-6">
                                          <label for="inputPassword4">Contact Number</label><span class="required_class">*</span>
                                          <input type="text" class="form-control" id="contact_number" required
                                              name="contact_number" value="">
                                      </div>
                                  </div>

                                  <div class="form-row">
                                      <div class="form-group col-md-4">
                                          <label for="txtMobile">Helpline</label><span class="required_class">*</span>
                                          <input type="text" class="form-control" name="help_line" value="" required id="help_line">
                                      </div>
                                      <div class="form-group col-md-4">
                                          <label for="inputPassword4">Bkash Number</label><span class="required_class">*</span>
                                          <input type="text" class="form-control" id="bikash_number" required  name="bikash_number" value="">
                                      </div>
                                      <div class="form-group col-md-4">
                                            <label for="Timezone">Logo</label><span class="required_class">*</span>
                                            <input type="file" class="form-control"  id="inputImage" required name="logo_path"
                                                accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
                                        </div>
                                  </div>

                                <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
            </div>
       </div>
      </div>
</div>
