<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>basic_info/update" method="post" enctype="multipart/form-data">
  <div class="form-row">

    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('name'); ?><span class="required_label">*</span></label>
      <input type="hidden" name="id"  class="form-control" value="<?php echo $basic_info->id;  ?>">
      <input type="text" autocomplete="off"  name="name"  class="form-control" value="<?php echo $basic_info->name;  ?>">
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('contact'); ?><span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  name="contact_number"  class="form-control" value="<?php echo $basic_info->contact_number;  ?>">
    </div>

  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('help_line'); ?><span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  name="help_line"  class="form-control" value="<?php echo $basic_info->help_line;  ?>">
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('bkash'); ?><span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  name="bikash_number"  class="form-control" value="<?php echo $basic_info->bikash_number;  ?>">
    </div>

  </div>



  <div class="form-row">
    <div class="form-group col-md-6 text-center">
      <img style="max-width: 250px; max-height:  250px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $basic_info->logo_path; ?>"
      class="img"/>
    </div>

    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('new').' '.$this->lang->line('photo'); ?><span class="required_label">*</span></label>
      <input type="file" name="txtPhoto" id="txtPhoto" class="form-control">
      <span style="color: #FF0000;" id="file_error"></span>
    </div>
  </div>

  <div class="float-right">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
  </div>

</form>
