<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>notice_lists/add" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Title</label>
			<input type="text" autocomplete="off"  class="form-control" name="txtTitle" required="1"/>
		</div>
		<div class="form-group col-md-4">
			<label>Date</label>
			<div class="input-group date">
				<input type="text" autocomplete="off" value="<?php echo date('Y-m-d') ?>" required name="txtDate"  id="txtDate" class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                         <i class="simple-icon-calendar"></i>
                     </span>
			</div>
		</div>

		<div class="form-group col-md-4">
			<label>File</label>
			<input type="file" name="txtFile" required="1" class="form-control"> * File Format -> PDF , DOC , DOCX ,JPEG , JPG and PNG.
		</div>

	</div>
	<div class="float-right">
		<input type="reset" class="btn btn-danger" value="<?php echo $this->lang->line('cancel'); ?>">
	    <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
