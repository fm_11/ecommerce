<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" cellpadding="0" cellspacing="0" summary="Employee Pay Sheet" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">Title</th>
        <th scope="col">Date</th>
        <th scope="col">Notice</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($notices as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['title']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td>
                <a href="<?php echo base_url() . MEDIA_FOLDER; ?>/notice/<?php echo $row['url']; ?>"
                   target="_blank"><?php echo $row['url']; ?></a>
            </td>
            <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>notice_lists/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>
