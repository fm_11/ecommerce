<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
		}
	</style>

	<?php
} ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">

    <thead>
    <tr>
        <td colspan="8" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $this->lang->line('marking') . $this->lang->line('blank') . ' ' . $this->lang->line('sheet'); ?></b>

        </td>
    </tr>

    <tr>
        <td align="center">
            <?php echo $this->lang->line('academic'); ?> <?php echo $this->lang->line('year'); ?>
        </td>
        <td align="center">
            <?php echo $this->lang->line('class'); ?>
        </td>
        <td align="center">
            <?php echo $this->lang->line('section'); ?>
        </td>
        <td align="center">
            <?php echo $this->lang->line('group'); ?>
        </td>
        <td align="center">
            <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('students'); ?>
        </td>
        <td align="center">
           <?php echo $this->lang->line('shift'); ?>
        </td>
        <td align="center">
           <?php echo $this->lang->line('exam'); ?>
        </td>
        <td align="center">
           <?php echo $this->lang->line('subject'); ?>
        </td>
    </tr>

    <tr>
        <td align="center">
            <b><?php echo date('Y'); ?></b>
        </td>
        <td align="center">
            <b><?php echo $class_name; ?></b>
        </td>
        <td align="center">
            <b><?php echo $section_name; ?></b>
        </td>
        <td align="center">
            <b>
            <?php
             echo $group_name;
            ?>
            </b>
        </td>
        <td align="center">
            <b><?php echo count($info); ?></b>
        </td>
        <td align="center">
           <b><?php echo $shift_name; ?></b>
        </td>
        <td align="center">
           <b><?php echo $exam_info[0]['name']; ?></b>
        </td>
        <td align="center">
            <b>
            <?php
              if(!empty($subject_info)){
                  echo $subject_info[0]['name'];
              }
            ?></b>
        </td>
    </tr>

    <tr>
        <td scope="col" class="h_td" align="center">&nbsp;<?php echo $this->lang->line('sl'); ?></td>
        <td  scope="col"  class="h_td" align="left">&nbsp;<?php echo $this->lang->line('name'); ?></td>
        <td scope="col"  class="h_td" align="center">&nbsp;<?php echo $this->lang->line('roll'); ?></td>
        <td  scope="col" class="h_td" align="center">&nbsp;<?php echo $this->lang->line('class_test'); ?></td>
        <td  scope="col" class="h_td" align="center">&nbsp;<?php echo $this->lang->line('creative'); ?></td>
        <td  scope="col" class="h_td" align="center">&nbsp;<?php echo $this->lang->line('objective'); ?></td>
        <td scope="col" class="h_td" align="center">&nbsp;<?php echo $this->lang->line('practical'); ?></td>
        <td scope="col" class="h_td" align="center">&nbsp;<?php echo $this->lang->line('students'); ?> <?php echo $this->lang->line('signature'); ?></td>
    </tr>

    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($info as $row):
        $i++;
        ?>
        <tr>
            <td align="center">
                <?php echo $i; ?>
            </td>
            <td width="180">
               <?php echo $row['name']; ?>
            </td>
            <td align="center">&nbsp;<?php echo $row['roll_no']; ?></td>
            <td> &nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td>&nbsp;&nbsp;</td>
            <td> &nbsp;</td>
            <td> &nbsp;&nbsp;</td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
