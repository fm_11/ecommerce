<table width="100%" cellpadding="0" class="data_table" cellspacing="0" id="box-table-a" summary="StudentReport">
    <thead>
    <tr>
        <td colspan="10" style="background-color: #99bbff; height: 25px; font-size: 14px;">
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
          <?php echo $this->lang->line('section'); ?>: <b><?php echo $section_name; ?></b>,
            <?php echo $this->lang->line('group'); ?>: <b><?php echo $group_name; ?></b>,
            <?php echo $this->lang->line('shift'); ?>: <b><?php echo $shift_name; ?></b>,
            <?php echo $this->lang->line('exam'); ?>: <b><?php echo $exam_name . ', ' . $exam_year; ?></b>
        </td>
    </tr>
    <tr>
        <th width="240" scope="col" align="center">&nbsp;<?php echo $this->lang->line('subject'); ?> <?php echo $this->lang->line('name'); ?></th>
        <th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('subject'); ?> <?php echo $this->lang->line('code'); ?></th>
        <th align="center" scope="col">&nbsp;<?php echo $this->lang->line('credit'); ?></th>
        <th align="center" scope="col">&nbsp;<?php echo $marking_heads->written; ?></th>
        <th align="center" scope="col">&nbsp;<?php echo $marking_heads->mcq; ?></th>
        <th align="center" scope="col">&nbsp;<?php echo $marking_heads->practical; ?></th>
		<th align="center" scope="col">&nbsp;<?php echo $marking_heads->class_test; ?></th>
        <th align="center" scope="col">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('marks'); ?></th>
        <th align="center" scope="col">&nbsp;<?php echo $this->lang->line('letter_grade'); ?></th>
        <th align="center" scope="col">&nbsp;<?php echo $this->lang->line('grade_point'); ?></th>
    </tr>
    </thead>

    <tbody>
    <?php
    $i = 0;
    foreach ($result_data as $row):
        ?>
        <tr>
            <td colspan="10" style="background-color: #ebebe0; height: 25px; font-size: 14px;">
                &nbsp;<?php echo $this->lang->line('name'); ?>: <b><?php echo $row['student_name']; ?></b>,
                <?php echo $this->lang->line('student_code'); ?>: <b><?php echo $row['student_code']; ?></b>,
                <?php echo $this->lang->line('roll'); ?>: <b><?php echo $row['roll_no']; ?></b>,
              <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('obtain'); ?> <?php echo $this->lang->line('mark'); ?>: <b><?php echo round($row['total_obtain_mark'],2); ?></b>,
              <?php echo $this->lang->line('gpa'); ?>: <b><?php echo round($row['gpa_with_optional'],2); ?></b>,
              <?php echo $this->lang->line('grade'); ?>: <b><?php echo $row['c_alpha_gpa_with_optional']; ?></b>
            </td>
        </tr>

        <?php
        foreach ($result_data[$i]['result'] as $result_row):
            ?>

            <tr <?php if ($result_row['is_optional'] == '1') { ?> style="background-color: #ebebe0" <?php } ?>>
                <td>
                    &nbsp;<?php echo $result_row['subject_name']; ?>
                </td>
                <td align="center">
                    &nbsp;<?php echo $result_row['subject_code']; ?><br>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo round($result_row['credit'],2);
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo round($result_row['written'],2);
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo round($result_row['objective'],2);
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo round($result_row['practical'],2);
                    ?>
                </td>
				<td align="center">&nbsp;
					<?php
					echo round($result_row['class_test'],2);
					?>
				</td>
                <td align="center">&nbsp;
                    <?php
                    echo round($result_row['total_obtain'],2);
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo $result_row['alpha_gpa'];
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo round($result_row['numeric_gpa'],2);
                    ?>
                </td>
            </tr>
            <?php
        endforeach;
        ?>
        <?php
        $i++;
    endforeach;
    ?>
    </tbody>
</table>
