<table width="100%" cellpadding="0" class="data_table" cellspacing="0" id="box-table-a" summary="StudentReport">
    <thead>
    <tr>
        <td  align="center" colspan="<?php echo count($subject_list) + 3; ?>"
			 style="background-color: #99bbff; height: 25px; font-size: 14px;">
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
            <?php echo $this->lang->line('section'); ?>: <b><?php echo $section_name; ?></b>,
            <?php echo $this->lang->line('group'); ?>: <b><?php echo $group_name; ?></b>,
            <?php echo $this->lang->line('Shift'); ?>: <b><?php echo $shift_name; ?></b>,
            <?php echo $this->lang->line('exam'); ?>: <b><?php echo $exam_name . ', ' . $exam_year; ?></b>
        </td>
    </tr>

    <tr>
        <td width="20"  style="background-color: #ebebe0; font-size: 13px;"  rowspan="2" scope="col" align="center">&nbsp;<b><?php echo $this->lang->line('sl'); ?></b></td>
        <td width="150"  style="background-color: #ebebe0; font-size: 13px;"   rowspan="2" scope="col" align="center">&nbsp;<b><?php echo $this->lang->line('roll'); ?>
           <br><?php echo $this->lang->line('name'); ?><br><?php echo $this->lang->line('student_code'); ?></b></td>
        <?php
        foreach ($subject_list as $subject_row):
            ?>
            <td width="100" style="background-color: #ebebe0; font-size: 13px;" scope="col" align="center">
               <b> &nbsp;<?php echo $subject_row['name'] ?></b>
            </td>
            <?php
        endforeach;
        ?>
        <td width="70" rowspan="2"  style="background-color: #ebebe0; font-size: 13px;"  scope="col" align="center">
                 <b>
                     <?php echo $this->lang->line('position'); ?><br>
                     <?php echo $this->lang->line('grade'); ?><br>
                     <?php echo $this->lang->line('cgpa'); ?> (<?php echo $this->lang->line('with_optional'); ?>)<br>
                    <?php echo $this->lang->line('cgpa'); ?> (<?php echo $this->lang->line('without_optional'); ?>)<br>
                     <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('mark'); ?>
                </b>
        </td>
    </tr>
    <tr>
                <?php
                  foreach ($subject_list as $subject_row):
                ?>
                <td width="20"  style="background-color: #ebebe0; font-size: 13px;"  scope="col" align="center">
                    <b>
                    &nbsp;<?php echo $this->lang->line('objective'); ?><br>
                    &nbsp;<?php echo $this->lang->line('creative'); ?><br>
                    &nbsp;<?php echo $this->lang->line('class_test'); ?><br>
                    &nbsp;<?php echo $this->lang->line('practical'); ?><br>
                    &nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('marks'); ?><br>
                    &nbsp;<?php echo $this->lang->line('grade'); ?>
                    </b>
                </td>
                 <?php
                  endforeach;
                 ?>
    </tr>
    </thead>


    <tbody>
    <?php
    $i = 1;
    foreach ($result_data as $row):
      //echo '<pre>';
    //  print_r($row);
    //  die;
        ?>
        <tr>
            <td width="20" style="font-size: 13px;" scope="col" align="center">
                &nbsp;<?php echo $i; ?>
            </td>
            <td width="100" style="font-size: 13px;" scope="col" align="center">
                <b>&nbsp;<?php echo $row['roll_no']; ?><br><?php echo $row['student_name']; ?><br>&nbsp;<?php echo $row['student_code']; ?></b>
            </td>


            <?php
              $optional_sub_got_mark = 0;
            foreach ($subject_list as $subject_row):
                ?>
                <?php
                if (isset($row['result'][$subject_row['code']])) {
                    ?>
                    <td width="20" style="font-size: 13px;" scope="col" align="center">
                        &nbsp;<?php echo round($row['result'][$subject_row['code']]['objective'],2); ?><br>
                        &nbsp;<?php echo round($row['result'][$subject_row['code']]['written'],2); ?><br>
                        &nbsp;<?php echo round($row['result'][$subject_row['code']]['class_test'],2); ?><br>
                        &nbsp;<?php echo round($row['result'][$subject_row['code']]['practical'],2); ?><br>
                        &nbsp;<?php
                        echo round($row['result'][$subject_row['code']]['total_obtain'],2);


                    if ($row['result'][$subject_row['code']]['is_optional'] == '1') {
                        $optional_sub_got_mark = $row['result'][$subject_row['code']]['total_obtain'];
                    } ?><br>
                        &nbsp;<?php echo $row['result'][$subject_row['code']]['alpha_gpa']; ?>
                    </td>
                    <?php
                } else {
                    ?>
                    <td width="100" style="font-size: 13px;" scope="col" align="center">
                      &nbsp;<?php echo $this->lang->line('na'); ?>
                    </td>
                    <?php
                }
                ?>
                <?php
            endforeach;
            ?>
            <td width="60" style="font-size: 13px;" scope="col" align="center">
                <b>
                    &nbsp;<?php echo $row['class_position']; ?><br>
                    &nbsp;<?php echo $row['c_alpha_gpa_with_optional']; ?><br>
                    &nbsp;<?php echo round($row['gpa_with_optional'],2); ?><br>
                    &nbsp;<?php echo round($row['gpa_without_optional'],2); ?><br>
                    &nbsp;<?php echo round($row['total_obtain_mark'],2); ?>
                </b>
            </td>
        </tr>
        <?php
        $i++; endforeach;
    ?>
    </tbody>
</table>
