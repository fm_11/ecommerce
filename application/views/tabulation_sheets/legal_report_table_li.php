<style>
	.body_td{
		font-size: 12px !important;
	}
</style>
<table width="100%" cellpadding="0" class="data_table" cellspacing="0" id="box-table-a" summary="StudentReport">
	<thead>
	<tr>
		<td  align="left" colspan="<?php echo $colspan + 7; ?>"
			 style="background-color: #99bbff; padding: 10px; font-size: 14px;">
			<?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
			<?php echo $this->lang->line('section'); ?>: <b><?php echo $section_name; ?></b>,
			<?php echo $this->lang->line('group'); ?>: <b><?php echo $group_name; ?></b>,
			<?php echo $this->lang->line('Shift'); ?>: <b><?php echo $shift_name; ?></b>,
			<?php echo $this->lang->line('exam'); ?>: <b><?php echo $exam_name . ', ' . $exam_year; ?></b>
		</td>
	</tr>

	<tr>
		<td style="background-color: #ebebe0; font-size: 14px;" rowspan="2" scope="col" align="center">&nbsp;
			<b>ID</b>
		</td>
		<td style="background-color: #ebebe0; font-size: 14px;" rowspan="2" scope="col" align="center">&nbsp;
			<b><?php echo $this->lang->line('name'); ?></b>
		</td>
		<td style="background-color: #ebebe0; font-size: 14px;" rowspan="2" scope="col" align="center">&nbsp;
			<b><?php echo $this->lang->line('roll'); ?></b>
		</td>
		<?php
		foreach ($subject_list as $subject_row):
			$subject_colspan = 2;
			if($subject_row['is_written_allow'] == '1'){
				$subject_colspan += 1;
			}
			if($subject_row['is_objective_allow'] == '1'){
				$subject_colspan += 1;
			}
			if($subject_row['is_practical_allow'] == '1'){
				$subject_colspan += 1;
			}
			if($subject_row['is_class_test_allow'] == '1'){
				$subject_colspan += 1;
			}
			?>
			<td colspan="<?php echo $subject_colspan; ?>>" style="background-color: #ebebe0; font-size: 11px;" scope="col" align="center">
				<b> &nbsp;<?php echo $subject_row['name'] ?></b>
			</td>
		<?php
		endforeach;
		?>
		<td rowspan="2"  style="background-color: #ebebe0; font-size: 11px;"  scope="col" align="center">
			<b><?php echo $this->lang->line('position'); ?></b>
		</td>
		<td rowspan="2"  style="background-color: #ebebe0; font-size: 11px;"  scope="col" align="center">
			<b><?php echo $this->lang->line('grade'); ?></b>
		</td>
		<td rowspan="2"  style="background-color: #ebebe0; font-size: 11px;"  scope="col" align="center">
			<b><?php echo $this->lang->line('gpa'); ?></b>
		</td>
		<td rowspan="2"  style="background-color: #ebebe0; font-size: 11px;"  scope="col" align="center">
			<b><?php echo $this->lang->line('total'); ?></b>
		</td>
	</tr>
	<tr>
		<?php
		$td = '<td style="background-color: #ebebe0; font-size: 11px;"  scope="col" align="center"><b>';
		foreach ($subject_list as $subject_row):
			if($subject_row['is_written_allow'] == '1'){
				echo $td . $marking_heads->written . '</b></td>';
			}
			if($subject_row['is_objective_allow'] == '1'){
				echo $td . $marking_heads->mcq . '</b></td>';
			}
			if($subject_row['is_practical_allow'] == '1'){
				echo $td . $marking_heads->practical . '</b></td>';
			}
			if($subject_row['is_class_test_allow'] == '1'){
				echo $td . $marking_heads->class_test . '</b></td>';
			}
			?>
			<td style="background-color: #ebebe0; font-size: 11px;"  scope="col" align="center">
				<b><?php echo $this->lang->line('total'); ?></b>
			</td>
			<td style="background-color: #ebebe0; font-size: 11px;"  scope="col" align="center">
				<b><?php echo $this->lang->line('grade'); ?></b>
			</td>
		<?php
		endforeach;
		?>
	</tr>
	</thead>


	<tbody>
	<?php
	$i = 1;
	foreach ($result_data as $row):
		//echo '<pre>';
		//  print_r($row);
		//  die;
		?>
		<tr>
			<td class="body_td" scope="col" align="center">
				<b><?php echo $row['student_code']; ?></b>
			</td>
			<td class="body_td" scope="col" align="left">
				<b><?php echo $row['student_name']; ?></b>
			</td>
			<td class="body_td" scope="col" align="center">
				<b><?php echo $row['roll_no']; ?></b>
			</td>

			  <?php
					$td = '<td class="body_td" scope="col" align="center">';
					foreach ($subject_list as $subject_row):
					if (isset($row['result'][$subject_row['code']])) {
						$subject_colspan = 2;
						if ($subject_row['is_written_allow'] == '1') {
							$subject_colspan += 1;
							echo $td . round($row['result'][$subject_row['code']]['written'],2) . '</td>';
						}
						if ($subject_row['is_objective_allow'] == '1') {
							$subject_colspan += 1;
							echo $td . round($row['result'][$subject_row['code']]['objective'],2) . '</td>';
						}
						if ($subject_row['is_practical_allow'] == '1') {
							$subject_colspan += 1;
							echo $td . round($row['result'][$subject_row['code']]['practical'],2) . '</td>';
						}
						if ($subject_row['is_class_test_allow'] == '1') {
							$subject_colspan += 1;
							echo $td . round($row['result'][$subject_row['code']]['class_test'],2) . '</td>';
						}
						echo $td . round($row['result'][$subject_row['code']]['total_obtain'],2) . '</td>';
						echo $td . $row['result'][$subject_row['code']]['alpha_gpa'] . '</td>';
					}else{
						$subject_colspan = 2;
						if ($subject_row['is_written_allow'] == '1') {
							$subject_colspan += 1;
						}
						if ($subject_row['is_objective_allow'] == '1') {
							$subject_colspan += 1;
						}
						if ($subject_row['is_practical_allow'] == '1') {
							$subject_colspan += 1;
						}
						if ($subject_row['is_class_test_allow'] == '1') {
							$subject_colspan += 1;
						}
						echo '<td class="body_td" colspan="' . $subject_colspan . '" scope="col" align="center">' . $this->lang->line('na') . '</td>';
					}
				?>
			<?php
			endforeach;
			?>
			<td class="body_td" scope="col" align="center">
				<b>
					&nbsp;<?php echo $row['class_position']; ?>
				</b>
			</td>
			<td class="body_td" scope="col" align="center">
				<b>
					&nbsp;<?php echo $row['c_alpha_gpa_with_optional']; ?>
				</b>
			</td>
			<td class="body_td" scope="col" align="center">
				<b>
					&nbsp;<?php echo round($row['gpa_with_optional'],2); ?>
				</b>
			</td>
			<td class="body_td" scope="col" align="center">
				<b>
					&nbsp;<?php echo round($row['total_obtain_mark'],2); ?>
				</b>
			</td>
		</tr>
		<?php
		$i++; endforeach;
	?>
	</tbody>
</table>
