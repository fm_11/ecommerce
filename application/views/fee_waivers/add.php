<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fee_waivers/add" method="post">
    <div class="form-row">
        <div class="form-group col-md-3">
            <label><?php echo $this->lang->line('year'); ?></label>
            <select class="js-example-basic-single w-100" name="year" id="year" required="1">
                <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                <?php
                $i = 0;
                if (count($years)) {
                    foreach ($years as $list) {
                        $i++; ?>
                        <option
                            value="<?php echo $list['value']; ?>"
                            <?php if (isset($year)) {
                            if ($year == $list['value']) {
                                echo 'selected';
                            }
                        } ?>>
                        <?php echo $list['text']; ?>
                      </option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-3">
          <label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
          <select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <?php
              $i = 0;
              if (count($class_section_shift_marge_list)) {
                  foreach ($class_section_shift_marge_list as $list) {
                      $i++; ?>
                      <option
                          value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
                          <?php if (isset($class_shift_section_id)) {
                          if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
                              echo 'selected';
                          }
                      } ?>>
                      <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
                    </option>
                      <?php
                  }
              }
              ?>
          </select>
        </div>

        <div class="form-group col-md-3">
          <label><?php echo $this->lang->line('group'); ?></label>
          <select class="js-example-basic-single w-100" name="group_id" id="group_id" required="1">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <?php
              $i = 0;
              if (count($groups)) {
                  foreach ($groups as $list) {
                      $i++; ?>
                      <option
                          value="<?php echo $list['id']; ?>" <?php  if (isset($group_id)) { if ($group_id == $list['id']) {
                          echo 'selected';
                      }} ?>><?php echo $list['name']; ?></option>
                      <?php
                  }
              }
              ?>
          </select>
        </div>

        <div class="form-group col-md-3">
            <label>Fee Category</label>
            <select class="js-example-basic-single w-100" name="fee_sub_category_id" id="fee_sub_category_id" required="1">
                <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                <?php
                $i = 0;
                if (count($sub_categorys)) {
                    foreach ($sub_categorys as $list) {
                        $i++; ?>
                        <option
                            value="<?php echo $list['id']; ?>"
                          <?php if(isset($fee_sub_category_id)){ if ($fee_sub_category_id == $list['id']) {
                            echo 'selected';
                        }} ?>><?php echo $list['name']; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>

    </div>

    <div class="float-right">
        <input class="btn btn-primary"  type="submit" class="submit" value="<?php echo $this->lang->line('process'); ?>">
    </div>
</form>

<?php
if(isset($waivers_list)){
  echo $waivers_list;
}
 ?>
