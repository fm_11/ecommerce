<div class="table-responsive-sm">
    <table  class="table">
    <thead>
      <tr>
          <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
          <th scope="col"><?php echo $this->lang->line('name'); ?></th>
          <th scope="col"><?php echo $this->lang->line('class'); ?></th>
          <th scope="col"><?php echo $this->lang->line('roll'); ?></th>
          <th scope="col">Fees Category</th>
          <th scope="col">Is Percentage?</th>
          <th scope="col"><?php echo $this->lang->line('amount'); ?></th>
          <th scope="col"><?php echo $this->lang->line('year'); ?></th>
          <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
      </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($fees as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['student_name']; ?></td>
            <td><?php echo $row['class_name']; ?></td>
            <td><?php echo $row['roll_no']; ?></td>
            <td><?php echo $row['fees_category_name']; ?></td>
            <td>
              <?php
              if($row['is_percentage'] == '0'){
                echo 'No';
              }else{
                echo 'Yes';
              }
              ?>
            </td>
            <td><?php echo $row['amount']; ?></td>
            <td><?php echo $row['year']; ?></td>
            <td>
              <div class="dropdown">
                  <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="ti-pencil-alt"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                    <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>student_fee_waivers/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                  </div>
              </div>
            </td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
