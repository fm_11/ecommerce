<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_fee_waivers/student_fee_waiver_data_save" method="post">
  <div class="table-responsive-sm">
      <table  class="table">
        <thead>
        <tr>
            <th scope="col"><?php echo $this->lang->line('name'); ?></th>
            <th scope="col"><?php echo $this->lang->line('student_code'); ?></th>
            <th scope="col"><?php echo $this->lang->line('roll'); ?></th>
            <th scope="col">Is Percentage?</th>
            <th scope="col">
               Amount
            </th>
            <th scope="col"><input type="checkbox" onchange="checkAll(this)"></th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($student_info as $row):
            $i++;
            ?>
            <tr>

            <tr>

                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" class="form-control" name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
                <td>
                    <?php echo $row['roll_no']; ?>
                </td>
                <td>
                    <select class="form-control" name="is_percentage_<?php echo $i; ?>">
                       <option value="0">No</option>
                       <option value="1">Yes</option>
                    </select>
                </td>
                <td>
                    <input class="form-control" type="text" name="waiver_amount_<?php echo $i; ?>"
                           value="<?php if ($row['amount'] > 0) {
                         echo $row['amount'];
                        } else {
                            echo 0;
                        } ?>">
                </td>

                <td>
                    <input type="checkbox" <?php if ($row['amount'] > 0) {
                echo 'checked';
            } ?> name="is_allow_<?php echo $i; ?>">
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
  </div>

    <input type="hidden" class="form-control" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="form-control" name="year"
           value="<?php echo $year; ?>"/>

    <input type="hidden" class="form-control" name="class_id"
           value="<?php echo $class_id; ?>"/>

    <input type="hidden" class="form-control" name="section_id"
           value="<?php echo $section_id; ?>"/>

    <input type="hidden" class="form-control" name="group_id"
           value="<?php echo $group_id; ?>"/>

   <input type="hidden" class="form-control" name="shift_id"
   value="<?php echo $shift_id; ?>"/>

   <input type="hidden" class="form-control" name="fee_sub_category_id"
   value="<?php echo $fee_sub_category_id; ?>"/>

   <div class="float-right">
       <input class="btn btn-primary"  type="submit" class="submit" value="<?php echo $this->lang->line('submit'); ?>">
   </div>

</form>
