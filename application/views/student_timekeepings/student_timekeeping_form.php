<style>
	input.largerCheckbox {
		width: 18px;
		height: 18px;
	}
</style>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_timekeepings/add_student_login_data" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th scope="col">Student ID</th>
            <th scope="col">Name</th>
			<th scope="col">Roll</th>
			<th scope="col">Status</th>
			<th scope="col">Call</th>
            <th scope="col">Mobile</th>
        </tr>
        </thead>
        <tbody>


        <?php
        $i = 0;
        foreach ($student_info as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                    <input type="hidden" name="name_<?php echo $i; ?>" value="<?php echo $row['name']; ?>"/>
					<input type="hidden" name="group_id_<?php echo $i; ?>" value="<?php echo $row['group']; ?>"/>
                </td>
				<td>
					<?php echo $row['roll_no']; ?>
				</td>

                 <td>
                    <input type="radio" name="login_status_<?php echo $i; ?>"
                           value="P" <?php if ($row['process_status'] == 'P') {
                        echo 'checked';
                    } elseif ($row['process_status'] == '') {
                        echo 'checked';
                    } ?> />Present &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="login_status_<?php echo $i; ?>"
                           value="A" <?php if ($row['process_status'] == 'A') {
                        echo 'checked';
                    } ?> />Absent &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="login_status_<?php echo $i; ?>"
                           value="L" <?php if ($row['process_status'] == 'L') {
                        echo 'checked';
                    } ?> />Leave
                </td>

				<td>
					<a href="tel:<?php echo $row['guardian_mobile']; ?>">
						<i style="font-size: 22px;" class="ti-mobile"></i>
					</a>
				</td>

                <td>
                    <input type="text" style="max-width: 130px;" autocomplete="off"  name="guardian_mobile_<?php echo $i; ?>" value="<?php echo $row['guardian_mobile']; ?>">
                </td>
               
            </tr>
        <?php endforeach; ?>

		<tr>
			<td colspan="6" style="text-align: left;font-size: 18px;">
				<br>
				<b>
					<input type="checkbox" class="largerCheckbox" name="is_present_sms">&nbsp;Present SMS Send&nbsp;&nbsp;&nbsp;
					<input type="checkbox" class="largerCheckbox" name="is_absent_sms">&nbsp;Absent SMS Send&nbsp;&nbsp;&nbsp;
					<input type="checkbox" class="largerCheckbox" name="is_leave_sms">&nbsp;Leave SMS Send&nbsp;&nbsp;&nbsp;
				</b>
			</td>
		</tr>
        </tbody>
    </table>
	</div>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="date"
           value="<?php echo $date; ?>"/>
	<input type="hidden" class="input-text-short" name="period_id"
		   value="<?php echo $period_id; ?>"/>

	<input type="hidden" class="input-text-short" name="class_id"
		   value="<?php echo $class_id; ?>"/>
	<input type="hidden" class="input-text-short" name="section_id"
		   value="<?php echo $section_id; ?>"/>
	<input type="hidden" class="input-text-short" name="shift_id"
		   value="<?php echo $shift_id; ?>"/>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-primary" value="Save">
	</div>
</form>

