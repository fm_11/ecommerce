<?php
$period_id = $this->session->userdata('period_id');
$class_shift_section_id = $this->session->userdata('class_shift_section_id');
$date = $this->session->userdata('date');
?>

<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>student_timekeepings/add" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Date</label>
			<div class="input-group date">
				<input type="text" autocomplete="off"  name="date" <?php if (isset($date)) { ?> value="<?php echo $date; ?>"
				<?php } ?>  required class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
			  <i class="simple-icon-calendar"></i>
		    </span>
			</div>
		</div>
		<div class="form-group col-md-4">
			<label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
			<select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
							<?php if (isset($class_shift_section_id)) {
								if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label>Period</label>
			<select class="js-example-basic-single w-100" name="period_id" id="period_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_periods)) {
					foreach ($class_periods as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['id']; ?>" <?php  if (isset($period_id)) { if ($period_id == $list['id']) {
							echo 'selected';
						}} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-primary" value="Process">
	</div>
</form>

<?php
if(isset($student_info)){
	echo $student_timekeeping_data;
}
?>
