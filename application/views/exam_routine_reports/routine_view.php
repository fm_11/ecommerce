<table width="100%" cellpadding="0" class="data_table" cellspacing="0" id="box-table-a" summary="StudentReport">
	<thead>
<!--		<tr>-->
<!--			<td class="center_td" colspan="--><?php //echo count($subject_list) + 7; ?><!--" style="text-align:center">-->
<!--				<b style="font-size:16px;">--><?php //echo $HeaderInfo['school_name']; ?><!--</b><br>-->
<!--				<b style="font-size:14px;">--><?php //echo $HeaderInfo['address']; ?><!--<br>-->
<!--					<span style="font-size: 16px;">--><?php //echo $title; ?><!--</span><br>-->
<!--				</b>-->
<!--			</td>-->
<!--		</tr>-->
	<tr>
		<td align="left" colspan="<?php echo (count($classes) * count($exam_sessions)) + 1; ?>">
			<?php echo $this->lang->line('exam'); ?>: <b><?php echo $exam_name . ', ' . $exam_year; ?></b>
		</td>
	</tr>

	<tr>
		<td width="80" style="min-width: 100px;" scope="col" rowspan="2" align="center">
			<b><?php echo $this->lang->line('date'); ?></b>
		</td>
		<?php
		foreach ($classes as $class):
			?>
			<td colspan="2" scope="col" align="center">
				<b> <?php echo $class['name'] ?></b>
			</td>
		<?php
		endforeach;
		?>
	</tr>

	<tr>
		<?php
		foreach ($classes as $class):
			foreach ($exam_sessions as $exam_session):
			?>
			<td style="width: 100px !important;" scope="col" align="center">
				<b> <?php echo $exam_session['name']; ?></b>
				<br>
				<?php echo date("g:i A", strtotime($exam_session['start_time'])); ?><br>
				<?php echo date("g:i A", strtotime($exam_session['end_time'])); ?>
			</td>
		<?php
			endforeach;
		endforeach;
		?>
	</tr>
	</thead>

	<tbody>
	<?php
	foreach ($routine_data as $row):
		//  echo '<pre>';
		// print_r($row);
		// die;
		?>
		<tr>
			<td  style="min-width: 100px;" scope="col" align="center">
				<?php echo $row['date']; ?>
			</td>

			<?php
			foreach ($classes as $class_row):
			   foreach ($exam_sessions as $session_row):
				?>
				<td  scope="col" align="center">
					<?php echo $row[$class_row['id']][$session_row['id']]['subject_name']; ?>
				</td>
			<?php
			  endforeach;
			endforeach;
			?>
		</tr>
		<?php
		 endforeach;
		 ?>
	</tbody>
</table>
