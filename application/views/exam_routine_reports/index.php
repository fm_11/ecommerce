<script>
	function getExamByYear(year){
		//alert(year);
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		}
		else
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				//alert(xmlhttp.responseText);
				document.getElementById("exam_id").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", "<?php echo base_url(); ?>exams/getExamByYear?year=" + year, true);
		xmlhttp.send();
	}
</script>

<form name="addForm" target="_blank" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>exam_routine_reports/index" enctype="multipart/form-data" method="post">
	<div class="form-row">
		<div class="form-group col-md-3">
			<label>Year</label>
			<select class="js-example-basic-single w-100" onchange="getExamByYear(this.value)" name="year" id="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['value']; ?>"
							<?php if (isset($year)) {
								if ($year == $list['value']) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['text']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label>Exam</label>
			<select name="exam_id" id="exam_id" class="js-example-basic-single w-100" required="required">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php for ($A = 0; $A < count($ExamList); $A++) { ?>
					<option value="<?php echo $ExamList[$A]['id']; ?>"
						<?php if (isset($m_exam_id)) {
							if ($m_exam_id == $ExamList[$A]['id']) {
								echo 'selected';
							}
						} ?>><?php echo $ExamList[$A]['name']. ' ('. $ExamList[$A]['year'] . ')'; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="btn-group float-right">
		<input class="btn btn-primary" name="process" type="submit" value="View">
	</div>
</form>
