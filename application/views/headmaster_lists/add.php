<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>headmaster_lists/add" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Name&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" name="txtName" required="1"/>
		</div>
		<div class="form-group col-md-6">
			<label>Educational Qualification&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" name="txtEduQua" required="1"/>
		</div>
	</div>
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>From Date&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" placeholder="YYYY-mm-dd" id="txtFromDate" name="txtFromDate"  required="1"/>
		</div>
		<div class="form-group col-md-6">
			<label>To Date</label>
			<input type="text" autocomplete="off"  class="form-control" placeholder="YYYY-mm-dd" id="txtToDate"  name="txtToDate"/>
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
