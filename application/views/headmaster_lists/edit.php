<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>headmaster_lists/edit" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Name&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" value="<?php echo $headmasters[0]['name']; ?>" name="txtName" required="1"/>
		</div>
		<div class="form-group col-md-6">
			<label>Educational Qualification&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" value="<?php echo $headmasters[0]['edu_qua']; ?>" name="txtEduQua" required="1"/>
		</div>
	</div>
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>From Date&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" placeholder="YYYY-mm-dd" value="<?php echo $headmasters[0]['from_date']; ?>" id="txtFromDate"  name="txtFromDate"  required="1"/>
		</div>
		<div class="form-group col-md-6">
			<label>To Date</label>
			<input type="text" autocomplete="off"  class="form-control" placeholder="YYYY-mm-dd"value="<?php echo $headmasters[0]['to_date']; ?>" id="txtToDate"  name="txtToDate"/>
			<input type="hidden" class="form-control" placeholder="YYYY-mm-dd"value="<?php echo $headmasters[0]['id']; ?>"  name="id"  required="1"/>
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
