<div class="table-sorter-wrapper col-lg-12 table-responsive">
   <table id="sortable-table-1" class="table">
       <thead>
        <tr>
            <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
            <th scope="col"><?php echo $this->lang->line('name'); ?></th>
            <th scope="col"><?php echo $this->lang->line('number_of_post'); ?></th>
            <th scope="col"><?php echo $this->lang->line('shorting_order'); ?></th>
            <th scope="col"><?php echo $this->lang->line('teachers') . ' ' . $this->lang->line('designation'); ?> ?</th>
            <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ($post as $row):
            $i++;
            ?>
            <tr>
            <tr>
                <td>
                    <?php echo $i; ?>
                </td>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['num_of_post']; ?></td>
                <td><?php echo $row['shorting_order']; ?></td>
                <td>
                    <?php
                    if ($row['is_teacher'] == 1) {
                        echo 'Yes';
                    } else {
                        echo 'No';
                    }
                    ?>
                </td>
                <td>
                  <div class="dropdown">
                      <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="ti-pencil-alt"></i>
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                          <a class="dropdown-item" href="<?php echo base_url(); ?>teacher_post/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                          <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>teacher_post/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                      </div>
                  </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
