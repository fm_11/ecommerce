
  <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_post/add" method="post">
     <div class="form-row">
      <div class="form-group col-md-6">
        <label><?php echo $this->lang->line('name'); ?></label>
        <input type="text" autocomplete="off"  class="form-control" name="txtPost" required="1"/>
      </div>
      <div class="form-group col-md-6">
        <label><?php echo $this->lang->line('number_of_post'); ?></label>
        <input type="text" autocomplete="off"  class="form-control" name="num_of_post" required="1"/>
      </div>
    </div>

    <div class="form-row">
      <div class="form-group col-md-6">
        <label><?php echo $this->lang->line('shorting_order'); ?></label>
        <input type="text" autocomplete="off"  class="form-control" name="shorting_order" required="1"/>
      </div>
      <div class="form-group col-md-6">
        <label><?php echo $this->lang->line('teachers') . ' ' . $this->lang->line('designation'); ?> ?</label>
        <select class="js-example-basic-single w-100" required="1" name="is_teacher">
            <option value="">--Please Select --</option>
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select>
      </div>
    </div>

      <div class="float-right">
         <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
         <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
      </div>
  </form>
