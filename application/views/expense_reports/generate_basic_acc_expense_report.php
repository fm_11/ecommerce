<style>
#pdf_table{
     border-collapse: collapse;
}
#pdf_table td, #pdf_table th {
    border: 1px solid #00000;
    padding: 5px;
}
</style>
<?php
     if (isset($is_pdf) && $is_pdf == 1) {
         ?>
       <table width="100%" id="pdf_table" style="font-size:13px;" summary="Employee Pay Sheet">
<?php
     } else {
         ?>
<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
<?php
     }
?>
    <thead>
    <tr>
        <td colspan="7" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $this->lang->line('expense'); ?> <?php echo $this->lang->line('report'); ?> (<?php echo date("d-m-Y", strtotime($from_date)). ' to '. date("d-m-Y", strtotime($to_date)); ?>)<br>
                <?php echo $this->lang->line('cost'); ?> <?php echo $this->lang->line('center'); ?>: <?php echo  $HeaderInfo['cost_center_name']; ?></b>
        </td>
    </tr>

    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="150" scope="col"><?php echo $this->lang->line('category'); ?> <?php echo $this->lang->line('name'); ?></th>
        <th width="80" scope="col"><?php echo $this->lang->line('voucher_no'); ?></th>
        <th width="80" scope="col"><?php echo $this->lang->line('date'); ?></th>
        <th width="80" scope="col"><?php echo $this->lang->line('deposit_method'); ?></th>
        <th width="80" scope="col"><?php echo $this->lang->line('description'); ?></th>
        <th width="80" scope="col"><?php echo $this->lang->line('amount'); ?></th>
    </tr>

    </thead>
    <tbody>
    <?php
    $i = 0;
    $total = 0;
    //echo '<pre>';
    //print_r($idata);
    //die;
    foreach ($idata as $row):
        $i++;
        ?>
        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td>
                <?php echo $row['category_name']; ?>
            </td>
            <td><?php echo $row['expense_voucher_id']; ?></td>
            <td><?php echo $row['voucher_date']; ?></td>
            <td><?php echo $row['deposit_method_name']; ?></td>
            <td><?php echo $row['expense_description']; ?></td>
            <td align="right">
            <?php
              $total +=  $row['amount'];
              echo $row['amount'];
             ?>
            </td>
        </tr>
    <?php endforeach; ?>

        <tr>
           <td align="right" colspan="6"><b><?php echo $this->lang->line('total'); ?></b></td>
           <td align="right"><b><?php echo $total; ?></b></td>
        </tr>


    </tbody>
</table>
</div>
