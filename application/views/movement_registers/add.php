<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>movement_registers/add" method="post"
	  enctype="multipart/form-data">

	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Teacher/Staff</label>
			<select class="js-example-basic-single w-100" name="teacher_id" required="1">
				<option value="">-- Please Select --</option>
				<?php
				if (count($teachers)) {
					foreach ($teachers as $list) {
						$i++;
						?>
						<option
							value="<?php echo $list['id']; ?>"><?php echo $list['name'] . '(' . $list['teacher_code'] . ')'; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label>From Date</label>
			<div class="input-group date">
				<input type="text" autocomplete="off" name="from_date" required class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
			</div>
		</div>
		<div class="form-group col-md-4">
			<label>Out Time</label>
			<input type="time" autocomplete="off"  name="outTime" class="form-control" required="1">
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-4">
			<label>To Date</label>
			<div class="input-group date">
				<input type="text" autocomplete="off" name="to_date" class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
			</div>
		</div>
		<div class="form-group col-md-4">
			<label>In Time</label>
			<input type="time" autocomplete="off"  name="inTime" class="form-control">
		</div>

		<div class="form-group col-md-4">
			<label>Auto Attendance</label>
			<select class="js-example-basic-single w-100" name="txtAutoAttendance" required="1">
				<option value="N">No</option>
				<option value="Y">Yes</option>
			</select>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Location</label>
			<input type="text" autocomplete="off"  name="txtLocation" class="form-control" required="1">
		</div>
		<div class="form-group col-md-6">
			<label>Purpose</label>
			<input type="text" autocomplete="off"  name="txtLocation" class="form-control" required="1">
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
