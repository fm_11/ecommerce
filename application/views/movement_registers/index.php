
<?php
$name = $this->session->userdata('name');
$from_date = $this->session->userdata('from_date');
$to_date = $this->session->userdata('to_date');
?>
<form name="addForm" class="cmxform" id="commentForm"  method="post" action="<?php echo base_url(); ?>movement_registers/index">
	<div class="form-row">
		<div class="form-group col-md-3">
			<input type="text" style="padding: 10px !important;" autocomplete="off"
				   name="name" placeholder="Name/Student ID" value="<?php if (isset($name)) {
				echo $name;
			} ?>" class="form-control" id="name">
		</div>

		<div class="form-group col-md-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" style="padding: 10px !important;" placeholder="yyyy-mm-dd"  name="from_date" <?php if (isset($from_date)) { ?>
					value="<?php echo $from_date; ?>"
				<?php } ?> class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                 <i class="simple-icon-calendar"></i>
             </span>
			</div>
		</div>
		<div class="form-group col-md-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" style="padding: 10px !important;" placeholder="yyyy-mm-dd"  name="to_date" <?php if (isset($to_date)) { ?> value="<?php echo $to_date; ?>"
				<?php } ?>  class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                 <i class="simple-icon-calendar"></i>
             </span>
			</div>
		</div>
		<div class="form-group col-md-3">
			<button type="submit" style="padding: 13px 30px 13px 30px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
		</div>
	</div>
</form>

<hr>

<div class="table-responsive-sm">
	<table  class="table">
		<thead>
		<tr>
			<th scope="col">SL</th>
			<th scope="col">Name</th>
			<th scope="col">From Date</th>
			<th scope="col">To Date</th>
			<th scope="col">Out Time</th>
			<th scope="col">In Time</th>
			<th scope="col">Auto Login?</th>
			<th scope="col">Action</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$i = (int)$this->uri->segment(3);
		foreach ($records as $row):
			$i++;
			?>
			<tr>
				<td>
					<?php echo $i; ?>
				</td>
				<td><?php echo $row['name']; ?></td>
				<td><?php echo $row['from_date']; ?></td>
				<td><?php echo $row['to_date']; ?></td>
				<td>
					<?php echo date('h:i:s A', strtotime($row['start_time'])); ?>
				</td>
				<td>
					<?php
					if($row['end_time'] != ''){
						echo date('h:i:s A', strtotime($row['end_time']));
					}else{
						echo '-';
					}
					?>
				</td>

				<td>
					<?php
					if ($row['attendance_auto'] == 'Y') {
						echo 'Yes';
					}else{
						echo 'No';
					}
					?>
				</td>
				<td>

					<div class="dropdown">
						<button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="ti-pencil-alt"></i>
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
							<a class="dropdown-item" onclick="return deleteConfirm()"
							   href="<?php echo base_url(); ?>movement_registers/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
						</div>
					</div>

				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<div class="float-right">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>
