<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $title; ?></title>

    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        .box {
            border: 1px solid black;
        }

        th, td {
            padding: 6px;
        }

        .page_breack {
            page-break-after: always;
        }

        .plan {
            height: 200px;
            border: 1px solid black;
            width: 50%;
            vertical-align: top;
        }
		* {
			-webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
			color-adjust: exact !important;                 /*Firefox*/
		}

    </style>
</head>
<body>
<?php
if ($is_pdf_request != "Download") {
    ?>
    <script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript">

        function PrintElem(elem) {
            Popup($(elem).html());
        }

        function Popup(data) {
            var mywindow = window.open('', 'my div', 'height=400,width=600');
            mywindow.document.write('<html><head><title>my div</title>');
            /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            mywindow.print();
            mywindow.close();

            return true;
        }

    </script>
    <table width="100%"  align="right" cellpadding="0" cellspacing="0" id="box-table-a"
           summary="Employee Pay Sheet">
        <tr>
            <td>
                <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/print_seat_plan" method="post"
                      enctype="multipart/form-data">
                    <input type="hidden" name="txtClass" value="<?php echo $class_id; ?>">
                    <input type="hidden" name="class_shift_section_id" value="<?php echo $class_shift_section_id; ?>">
                    <input type="hidden" name="txtSection" value="<?php echo $section_id; ?>">
                    <input type="hidden" name="txtGroup" value="<?php echo $group; ?>">
                    <input type="hidden" name="txtExam" value="<?php echo $exam; ?>">
                    <input type="hidden" name="is_pdf_request" value="Download">
                    <input type="button" class="button" value="Print" onclick="PrintElem('#mydiv')"/>
                    <input type="submit" class="submit" value="Download PDF">
                </form>
            </td>
        </tr>
    </table>
    <?php
}
?>
<br>
<div id="mydiv">
    <table class="box"  align="center" style="margin-bottom:10px; margin-top:10px;">
        <tr>
            <?php
            $i = 0;
            $line_break = 6;
            foreach ($list as $row):
                if (($i == $line_break) && $i != 0) {
                    $page_break = 'class="page_breack"';
                    $line_break += 8;
                } else {
                    $page_break = "";
                }
                ?>
                <?php if ($i % 2 == 0 && $i != 0) {
                    echo '</tr><tr><td>&nbsp;</td></tr><tr ' . $page_break . '>';
                } ?>
                <td class="plan">
                    <table width="100%">
						<tr>
							<td colspan="3" style="text-align: center;" align="center">
								<span style="font-size: 18px;"><b><?php echo $school_info[0]['school_name']; ?></b></span>
							</td>
						</tr>
                        <tr>
                            <td width="25%">
                                <?php
                                if ($row['photo'] == '' || $row['photo'] == 'default.png') {
                                ?>
                                <img src="<?php echo base_url(); ?>/core_media/images/default.png" align="center"
                                     style="width: 60px; height: 60px;">
                                <?php
                                } else {
                                  ?>
                                  <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $row['photo']; ?>" align="center"
                                       style="width: 60px; height: 60px;">
                                  <?php
                                }
                                ?>

                            </td>
                            <td width="50%" style="text-align: center;vertical-align: top;">
                                <div style="font-size: 15px;
											border: 1px #000044 solid;
											padding: 5px;
											width: 50%;
											margin: 0px auto;
											margin-bottom: -13px;"><b>Seat Plan</b></div><br>
                                <span style="font-size: 15px;color:#000000;"><?php echo $exam; ?></span>
                            </td>
                            <td width="25%" align="center">
                                <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info[0]['picture']; ?>" align="center"
                                     style="width: 60px; height: 60px;">
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <span style="font-size: 12px; border-bottom: 1px black dotted; padding: 1px;">Name:  <b><?php echo $row['name']; ?></b></span><br>
                                <span style="font-size: 12px; border-bottom: 1px black dotted; padding: 1px;">Class:  <b><?php echo $row['class_name']; ?></b></span><br>
                                <span style="font-size: 12px; border-bottom: 1px black dotted; padding: 1px;">Section:   <b><?php echo $row['section_name']; ?></b></span><br>
                                <span style="font-size: 12px; border-bottom: 1px black dotted; padding: 1px;">Group: <b><?php echo $row['group_name']; ?></b></span><br>
                            </td>
                            <td style="text-align: center; padding-right: 30px; border:2px black solid;">
                                 <span style="font-size: 20px; border-bottom: 1px black dotted; padding: 1px;">Roll<br>
                                    <b><?php echo $row['roll_no']; ?></b></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right;">
                                <span style="font-size: 11px;">Powered by: School360</span>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    &nbsp;..
                </td>

                <?php $i++; endforeach; ?>
        </tr>
    </table>
</div>
</body>
</html>
