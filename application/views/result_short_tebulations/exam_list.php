<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
<?php
$i = 0;
if (count($exam_list)) {
    foreach ($exam_list as $list) {
        $i++; ?>
        <option
            value="<?php echo $list['id']; ?>"><?php echo $list['name'].'('.$list['year'].')'; ?></option>
    <?php
    }
}
?>
