<!DOCTYPE html>
<html>
<head>
<title><?php echo $title; ?></title>
<style>

    table{
      border-collapse: collapse;
    }
    th,td{
      font-size: 14px;
      padding: 4px;
      border: 1px solid black;
    }
    a:link, a:visited {
      background-color: #f44336;
      color: white;
      padding: 8px 8px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
    }
    a:hover, a:active {
      background-color: red;
    }
</style>
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>
</head>
<body>
  <?php
  if (isset($result_data)) {
      ?>
      <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a">
          <tr>
              <th width="100%" style="text-align:right" scope="col">
                   <a href="<?php echo base_url(); ?>result_short_tebulations/index">
                     Back to Index
                   </a>
                   <a href="javascript:void" onclick="printDiv('printableArea')">
                    Print
                   </a>
              </th>
          </tr>
      </table>
  <?php
  }
  ?>
<div id="printableArea">
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td class="center_td" colspan="<?php echo count($subject_list) + 7; ?>" style="text-align:center">
            <b style="font-size:16px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:14px;"><?php echo $HeaderInfo['address']; ?><br>
                <span style="font-size: 16px;"><?php echo $title; ?></span><br>
            </b>
        </td>
    </tr>
    <tr>
        <td align="left" colspan="<?php echo count($subject_list) + 7; ?>">
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
            <?php echo $this->lang->line('section'); ?>: <b><?php echo $section_name; ?></b>,
          <?php echo $this->lang->line('group'); ?>: <b><?php echo $group_name; ?></b>,
            <?php echo $this->lang->line('shift'); ?>: <b><?php echo $shift_name; ?></b>,
            <?php echo $this->lang->line('exam'); ?>: <b><?php echo $exam_name . ', ' . $exam_year; ?></b>
        </td>
    </tr>

    <tr>
       <td width="100" style="min-width:130px;" scope="col" align="center">
          <b><?php echo $this->lang->line('name'); ?></b>
       </td>

       <td width="80"  scope="col" align="center">
          <b><?php echo $this->lang->line('student_code'); ?></b>
       </td>

       <td width="80"   rowspan="2" scope="col" align="center">
         <b><?php echo $this->lang->line('roll'); ?></b>
      </td>


        <?php
        foreach ($subject_list as $subject_row):
            ?>
            <td  scope="col" align="center">
               <b> <?php echo $subject_row['name'] ?></b>
            </td>
            <?php
        endforeach;
        ?>

        <td width="70" rowspan="2"  scope="col" align="center">
             <b><?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('mark'); ?></b>
        </td>

        <td rowspan="2"  scope="col" align="center">
             <b><?php echo $this->lang->line('position'); ?></b>
        </td>
        <td  rowspan="2"  scope="col" align="center">
             <b>Letter <br> Garde</b>
        </td>
        <td rowspan="2"  scope="col" align="center">
             <b>Grade <br> Point</b>
        </td>
    </tr>
    </thead>


    <tbody>
    <?php
    $i = 1;
    foreach ($result_data as $row):
     //  echo '<pre>';
     // print_r($row);
     // die;
        ?>
        <tr>
            <td  scope="col" align="left">
                <?php echo $row['student_name']; ?>
            </td>
            <td  scope="col" align="center">
                <?php echo $row['student_code']; ?>
            </td>
            <td  scope="col" align="center">
                <?php echo $row['roll_no']; ?>
            </td>


            <?php
              $optional_sub_got_mark = 0;
            foreach ($subject_list as $subject_row):
                ?>
                <?php
                if (isset($row['result'][$subject_row['code']])) {
                    ?>
                    <td width="20"  scope="col" align="center">
                      <?php
                        echo round($row['result'][$subject_row['code']]['total_obtain'], 2); ?>
                    </td>
                    <?php
                } else {
                    ?>
                    <td width="100"  scope="col" align="center">
                      <?php echo $this->lang->line('na'); ?>
                    </td>
                    <?php
                }
                ?>
                <?php
            endforeach;
            ?>



            <td width="20"  scope="col" align="center">
                <?php echo round($row['total_obtain_mark'], 2); ?>
            </td>

            <td width="20"  scope="col" align="center">
                <?php echo $row['class_position']; ?>
            </td>
            <td width="20"  scope="col" align="center">
                <?php echo $row['c_alpha_gpa_with_optional']; ?>
            </td>
            <td width="20"  scope="col" align="center">
                <?php echo round($row['gpa_with_optional'], 2); ?>
            </td>
        </tr>
        <?php
        $i++; endforeach;
    ?>
    </tbody>
</table>
</div>
</body>
</html>
