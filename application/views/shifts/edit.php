
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>shifts/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Name</label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $shift[0]['name']; ?>" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label>Flexible Minute for Late</label>
      <input type="text" autocomplete="off"  class="form-control" value="<?php echo $shift[0]['flexible_late_minute']; ?>"  name="flexible_late_minute" required="1"/>
    </div>
  </div>


  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Login Time</label>
      <input type="time" autocomplete="off"  name="login_time" placeholder="Ex: 09:00:00"  value="<?php echo $shift[0]['login_time']; ?>" class="form-control" required>
    </div>

    <div class="form-group col-md-6">
      <label>Logout Time</label>
      <input type="time" autocomplete="off"  name="logout_time" required placeholder="Ex: 16:00:00" value="<?php echo $shift[0]['logout_time']; ?>" class="form-control">
    </div>
 </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $shift[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
