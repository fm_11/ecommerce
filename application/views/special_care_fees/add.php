<script type="text/javascript">
    function getStudentByClassAndSection(class_id, section_id) {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("student_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>student_fees/getStudentByClassAndSection?class_id=" + class_id + '&&section_id=' + section_id, true);
        xmlhttp.send();
    }

</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>special_care_fees/add" method="post">
  <div class="form-row">
      <div class="form-group col-md-4">
        <label><?php echo $this->lang->line('class'); ?></label>
        <select class="js-example-basic-single w-100" name="class_id" id="class_id"
                onchange="getStudentByClassAndSection(this.value,document.getElementById('section_id').value)" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($class)) {
                foreach ($class as $list) {
                    $i++; ?>
                    <option
                        value="<?php echo $list['id']; ?>" <?php if ($c_class_id == $list['id']) {
                        echo 'selected';
                    } ?>><?php echo $list['name']; ?></option>
                    <?php
                }
            }
            ?>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label><?php echo $this->lang->line('section'); ?></label>
        <select class="js-example-basic-single w-100" name="section_id" id="section_id"
                onchange="getStudentByClassAndSection(document.getElementById('class_id').value,this.value)" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($section)) {
                foreach ($section as $list) {
                    $i++; ?>
                    <option
                        value="<?php echo $list['id']; ?>" <?php if ($c_section_id == $list['id']) {
                        echo 'selected';
                    } ?>><?php echo $list['name']; ?></option>
                    <?php
                }
            }
            ?>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label><?php echo $this->lang->line('student'); ?></label>
        <select class="js-example-basic-single w-100" name="student_id" id="student_id" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
        foreach ($students as $list) {
            ?>
    	    <option
    	        value="<?php echo $list['id']; ?>" <?php if ($c_student_id == $list['id']) {
                echo 'selected';
            } ?>><?php echo $list['name'] . ' (' . $list['student_code'] . ')'; ?></option>
    	<?php
        }
        ?>
        </select>
      </div>
  </div>
  <div class="form-row">
      <div class="form-group col-md-4">
        <label><?php echo $this->lang->line('year'); ?></label>
        <select class="js-example-basic-single w-100" name="year" id="year" required="1">
            <option value="">--<?php echo $this->lang->line('please_select'); ?> --</option>
            <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
            <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
            <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label><?php echo $this->lang->line('month'); ?></label>
        <select class="js-example-basic-single w-100" name="month" id="month" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 1;
            while ($i <= 12) {
                $dateObj = DateTime::createFromFormat('!m', $i); ?>
                <option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
                    echo 'selected';
                } ?>><?php echo $dateObj->format('F'); ?></option>
                <?php
                $i++;
            }
            ?>
        </select>
      </div>
      <div class="form-group col-md-4">
        <label><?php echo $this->lang->line('amount'); ?></label>
        <input type="text" autocomplete="off"  name="amount" required class="form-control" value="">
      </div>
  </div>

  <div class="float-right">
      <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
      <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>

</form>
