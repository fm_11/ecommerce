<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo$title ;?></title>
	<!-- base:css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/vendors/css/vendor.bundle.base.css">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/admin_v3/css/horizontal-layout-light/style.css">
	<!-- endinject -->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>core_media/admin_v3/images/favicon.png" />
</head>

<body>
<?php
$session_user = $this->session->userdata('user_info');
?>
<div class="container-scroller">
	<!-- partial:../../partials/_horizontal-navbar.html -->
	<div class="horizontal-menu">
		<nav style="background-color: rgb(108, 185, 124) !important;" class="navbar top-navbar col-lg-12 col-12 p-0">
			<div class="container">
				<div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
					<a class="navbar-brand brand-logo" href="<?php echo base_url(); ?>">
						<img src="<?php echo base_url(); ?>core_media/admin_v3/images/school360-logo.png" alt="logo"/>
					</a>
					<a class="navbar-brand brand-logo-mini" href="<?php echo base_url(); ?>">
						<img src="<?php echo base_url(); ?>core_media/admin_v3/images/school360-logo.png" alt="logo"/>
					</a>
				</div>
				<div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
					<ul class="navbar-nav mr-lg-2">
						<li class="nav-item d-none d-lg-block">
							<b>
								শিক্ষা প্রতিষ্ঠানের জন্য সব
							</b>
						</li>
					</ul>
					<ul class="navbar-nav navbar-nav-right">
						<li class="nav-item">
							<a target="_blank" href="https://www.facebook.com/school360bd/">
								<button type="button" class="btn btn-social-icon btn-facebook"><i class="ion ion-logo-facebook"></i></button>
							</a>
							<a target="_blank" href="https://www.youtube.com/channel/UCb5tNcxTsUkdwcwD4EhwM5g/">
								<button type="button" class="btn btn-social-icon btn-youtube"><i class="ion ion-logo-youtube"></i></button>
							</a>
							<a target="_blank" href="https://www.facebook.com/groups/school360itclub/">
								<button type="button" class="btn btn-social-icon btn-facebook"><i class="ion ion-logo-facebook"></i></button>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<!-- partial -->
	<div class="container-fluid page-body-wrapper">
		<div class="main-panel">
			<div class="content-wrapper">
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-12">
										<div class="row portfolio-grid">

											<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
												<figure class="effect-text-in">
													<a href="<?php echo base_url(); ?>download_zone/School360App.apk">
														<img src="<?php echo base_url(); ?>core_media/images/school360.png" alt="image"/>
														<figcaption>
															<h4 style="color: black;">School360 Student App</h4>
															<p>Click here to download</p>
														</figcaption>
													</a>
												</figure>
											</div>

											<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
												<figure class="effect-text-in">
													<a href="https://download.anydesk.com/AnyDesk.exe">
														<img src="<?php echo base_url(); ?>core_media/admin_v3/images/samples/300x300/anydesk.jpg" alt="image"/>
														<figcaption>
															<h4>Anydesk</h4>
															<p>
																Click here to download
															</p>
														</figcaption>
													</a>
												</figure>
											</div>
											<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
												<figure class="effect-text-in">
													<a href="https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v7.8.6/npp.7.8.6.Installer.exe">
														<img src="<?php echo base_url(); ?>core_media/admin_v3/images/samples/300x300/Notepad_plus_plus.png" alt="image"/>
														<figcaption>
															<h4>Notepad++ 7.8.6</h4>
															<p>Click here to download</p>
														</figcaption>
													</a>
												</figure>
											</div>
											<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
												<figure class="effect-text-in">
													<a href="<?php echo base_url(); ?>download_zone/ChromeSetup.exe">
													<img src="<?php echo base_url(); ?>core_media/admin_v3/images/samples/300x300/chrome.jpg" alt="image"/>
													<figcaption>
														<h4>Google Chrome</h4>
														<p>Click here to download</p>
													</figcaption>
													</a>
												</figure>
											</div>
											<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
												<figure class="effect-text-in">
													<a href="https://zoom.us/client/latest/ZoomInstaller.exe">
													<img src="<?php echo base_url(); ?>core_media/admin_v3/images/samples/300x300/zoom.png" alt="image"/>
													<figcaption>
														<h4>ZOOM</h4>
														<p>Click here to download</p>
													</figcaption>
													</a>
												</figure>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- content-wrapper ends -->
			<!-- partial:../../partials/_footer.html -->
			<footer class="footer mr-0 ml-0">
				<div class="d-sm-flex justify-content-center justify-content-sm-between">
					<span>Copyright &copy;
					<a href="https://spatei.com/" target="_blank" class="text-primary">Spate Initiative Limited</a>
					All rights reserved
					</span>
				</div>
			</footer>
			<!-- partial -->
		</div>
		<!-- main-panel ends -->
	</div>
	<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- base:js -->
<script src="<?php echo base_url(); ?>core_media/admin_v3/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/off-canvas.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/hoverable-collapse.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/template.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/settings.js"></script>
<script src="<?php echo base_url(); ?>core_media/admin_v3/js/todolist.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<!-- End custom js for this page-->
</body>

</html>
