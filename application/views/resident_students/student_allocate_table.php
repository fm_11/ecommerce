<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>resident_students/resident_data_save" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
			<th scope="col">Roll</th>
            <th scope="col">Name</th>
            <th scope="col">Student ID</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ($student_info as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td>
                    <input type="checkbox" <?php if($row['already_save'] > 0){echo 'checked';} ?> name="is_allow_<?php echo $i; ?>">
                </td>
				<td>
					<?php echo $row['roll_no']; ?>
				</td>
                <td>
                    <?php echo $row['name']; ?>
                    <input type="hidden" class="input-text-short" size="8"
                           style="text-align: right; width:75px;"
                           name="student_id_<?php echo $i; ?>" value="<?php echo $row['id']; ?>"/>
                </td>
                <td>
                    <?php echo $row['student_code']; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
	</div>
    <input type="hidden" class="input-text-short" name="loop_time"
           value="<?php echo $i; ?>"/>
    <input type="hidden" class="input-text-short" name="year"
           value="<?php echo $year; ?>"/>

    <input type="hidden" class="input-text-short" name="class_id"
           value="<?php echo $class_id; ?>"/>

    <input type="hidden" class="input-text-short" name="section_id"
           value="<?php echo $section_id; ?>"/>

    <input type="hidden" class="input-text-short" name="group_id"
           value="<?php echo $group_id; ?>"/>
		   
   <input type="hidden" class="input-text-short" name="shift_id"
value="<?php echo $shift_id; ?>"/>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>

</form>
