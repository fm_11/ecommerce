<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>


<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>resident_student_reports/index" method="post">

	<div class="form-row">
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('year'); ?></label><br>
			<select class="js-example-basic-single w-100" name="year" id="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option
								value="<?php echo $list['value']; ?>"
								<?php if (isset($year)) {
									if ($year == $list['value']) {
										echo 'selected';
									}
								} ?>>
							<?php echo $list['text']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>


		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label><br>
			<select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
								value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
								<?php if (isset($class_shift_section_id)) {
									if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
										echo 'selected';
									}
								} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('group'); ?></label><br>
			<select class="js-example-basic-single w-100" name="group_id" id="group_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($groups)) {
					foreach ($groups as $list) {
						$i++; ?>
						<option
								value="<?php echo $list['id']; ?>" <?php if(isset($group_id)){ if ($group_id == $list['id']) {
							echo 'selected';
						} } ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label>Report Type</label><br>
			<select class="js-example-basic-single w-100" name="is_resident" id="is_resident" required="1">
				<option value="">-- Please Select --</option>
				<option value="Y" <?php if(isset($is_resident)){ if($is_resident == 'Y'){ echo 'selected'; } } ?>>Resident Student</option>
				<option value="N" <?php if(isset($is_resident)){ if($is_resident == 'N'){ echo 'selected'; } } ?>>Non-Resident Student</option>
			</select>
		</div>

	</div>

	<div class="btn-group float-right">
		<input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
		<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
		<input type="submit" class="btn btn-primary" value="View Report">
	</div>
</form>

<div id="printableArea">
    <?php
    if (isset($idata)) {
        echo $report;
    }
    ?>
</div>
