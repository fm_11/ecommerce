<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
		}
	</style>

	<?php
} ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
            <thead>
            <tr>
                <td colspan="5" style="text-align:center">
					 <b><?php echo $HeaderInfo['school_name']; ?></b><br>
					 <?php echo $HeaderInfo['address']; ?><br>
                      <?php echo $title; ?></b>
                </td>
            </tr>
			
			<tr>
			    <td colspan="5">
				   <b style="font-size:13px;">
				   Class: <?php echo $class_name; ?>,
				   Section: <?php echo $section_name; ?>,
				   Group: <?php echo $group_name; ?>,
				   Shift: <?php echo $shift_name; ?>,
				   Year: <?php echo $year; ?>
				   </b>
				</td>
			</tr>

            <tr>
                <th scope="col">&nbsp;SL</th>
				<th scope="col">&nbsp;Roll</th>
				<th scope="col">&nbsp;Student ID</th>
				<th scope="col">&nbsp;Name</th>
				<th scope="col">&nbsp;Mobile</th>
            </tr>

            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ($idata as $row):
                $i++;
                ?>
                <tr>
                    <td>
                        &nbsp;<?php echo $i; ?>
                    </td>
					<td>&nbsp;<?php echo $row['roll_no']; ?></td>
					<td>
					&nbsp;<?php echo $row['student_code']; ?>
					</td>
					 <td>
					&nbsp;<?php echo $row['name']; ?>
					</td>
                    <td>&nbsp;<?php echo $row['guardian_mobile']; ?></td>					
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
</div>
