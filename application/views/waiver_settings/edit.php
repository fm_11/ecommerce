<form  name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>waiver_settings/edit" method="post">
      <div class="form-row">
        <div class="form-group col-md-4">
          <label for="Year">Year</label>
          <select class="js-example-basic-single w-100" name="year" id="year" required="1">
              <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
              <?php
              $i = 0;
              if (count($years)) {
                  foreach ($years as $list) {
                      $i++; ?>
                      <option
                          value="<?php echo $list['value']; ?>"
                          <?php if (isset($waiver_info[0]['year'])) {
                          if ($waiver_info[0]['year'] == $list['value']) {
                              echo 'selected';
                          }
                      } ?>>
                      <?php echo $list['text']; ?>
                    </option>
                      <?php
                  }
              }
              ?>
          </select>
        </div>

        <div class="form-group col-md-4">
          <label>
            Applicable From Month (Wiaver)
          </label>
          <select class="js-example-basic-multiple w-100" name="applicable_month" id="applicable_month" required="1">
              <?php
              $i = 1;
              while ($i <= 12) {
                  $dateObj = DateTime::createFromFormat('!m', $i); ?>
                  <option value="<?php echo $i; ?>" <?php if ($waiver_info[0]['applicable_month'] == date('m')) {
                      echo 'selected';
                  } ?>><?php echo $dateObj->format('F'); ?></option>
                  <?php
                  $i++;
              }
              ?>
          </select>
        </div>

        <div class="form-group col-md-4">
          <label>
            Applicable From Month (Scholarship)
          </label>
          <select class="js-example-basic-multiple w-100" name="applicable_month_for_scholarship" id="applicable_month_for_scholarship" required="1">
              <?php
              $i = 1;
              while ($i <= 12) {
                  $dateObj = DateTime::createFromFormat('!m', $i); ?>
                  <option value="<?php echo $i; ?>" <?php if ($waiver_info[0]['applicable_month_for_scholarship'] == date('m')) {
                      echo 'selected';
                  } ?>><?php echo $dateObj->format('F'); ?></option>
                  <?php
                  $i++;
              }
              ?>
          </select>
        </div>
      </div>

      <div class="float-right">
         <input type="hidden" name="id" value="<?php echo $waiver_info[0]['id']; ?>">
    	   <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
    	   <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    	</div>
</form>
