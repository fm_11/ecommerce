<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">Year</th>
        <th scope="col">Applicable From Month (Wiaver)</th>
        <th scope="col">Applicable From Month (Scholarship)</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($waiver_info as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['year']; ?></td>

            <td>
              <?php echo DateTime::createFromFormat('!m', $row['applicable_month'])->format('F'); ?>
            </td>
            <td>
              <?php echo DateTime::createFromFormat('!m', $row['applicable_month_for_scholarship'])->format('F'); ?>
            </td>
            <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" href="<?php echo base_url(); ?>waiver_settings/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>waiver_settings/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
