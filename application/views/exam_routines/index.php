<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>exam_routines/index" enctype="multipart/form-data" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label>Exam</label>
			<select name="exam_id" id="exam_id" class="js-example-basic-single w-100" required="required">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php for ($A = 0; $A < count($ExamList); $A++) { ?>
					<option value="<?php echo $ExamList[$A]['id']; ?>"
						<?php if (isset($exam_id)) {
							if ($exam_id == $ExamList[$A]['id']) {
								echo 'selected';
							}
						} ?>><?php echo $ExamList[$A]['name']. ' ('. $ExamList[$A]['year'] . ')'; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="class"><?php echo $this->lang->line('class'); ?></label>
			<select name="class_id" id="class_id" class="js-example-basic-single w-100"  required="required">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php for ($C = 0; $C < count($classes); $C++) { ?>
					<option value="<?php echo $classes[$C]['id']; ?>" <?php if(isset($class_id)){if ($class_id == $classes[$C]['id']) {
						echo 'selected';
					}} ?>><?php echo $classes[$C]['name']; ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Group</label>
			<select name="group_id"  class="js-example-basic-single w-100" required="required">
				<?php for ($C = 0; $C < count($GroupList); $C++) { ?>
					<option value="<?php echo $GroupList[$C]['id']; ?>" <?php if(isset($group_id)){ if ($group_id == $GroupList[$C]['id']) {
						echo 'selected';
					}} ?>><?php echo $GroupList[$C]['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>


	<div class="btn-group float-right">
		<input class="btn btn-primary" name="process" type="submit" value="Process">
	</div>
</form>


<?php
if(isset($process_list)){
	echo $process_list;
}
?>
