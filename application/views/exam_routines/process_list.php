<script>
	function dateCheck(date,row_num) {
		var session = document.getElementById("session_id_" + row_num).value;
		if(session != ''){
			var total_subject = Number(document.getElementById("total_subject").value);
			var i = 0;
			while (i < total_subject){
				if(row_num != i){
					var rowDate = document.getElementById("date_" + i).value;
					var rowSession = document.getElementById("session_id_" + i).value;
					//alert(rowDate + '/' + date);
					//alert(session + '/' + rowSession);
					if(date == rowDate && session == rowSession){
						alert("There is an exam on this date and session");
						document.getElementById("date_" + row_num).value = "";
						document.getElementById("session_id_" + row_num).value = "";
						document.getElementById("room_no_id_" + row_num).value = "";
						break;
						return false;
					}
				}
				i++;
			}
			return true;
		}
		return true;
	}

	function roomNoCheck(room_no,row_num) {
		var date = document.getElementById("date_" + row_num).value;
		var dateCheckResult = dateCheck(date,row_num);
		//alert(dateCheckResult);
		if(dateCheckResult == false){
			return false;
		}
		var session_id = document.getElementById("session_id_" + row_num).value;
		if(date == '' || session_id == ''){
			alert("Please assign a date and session");
			document.getElementById("room_no_id_" + row_num).value = "";
			return false;
		}

		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		}
		else
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				 //alert(xmlhttp.responseText);
				 if(xmlhttp.responseText == 1){
					 alert("There is another exam in this room");
					 document.getElementById("room_no_id_" + row_num).value = "";
				 }
			}
		}
		xmlhttp.open("GET", "<?php echo base_url(); ?>exam_routines/checkRoomNoByDate?room_id=" + room_no + "&&session_id=" + session_id + "&&date=" + date, true);
		xmlhttp.send();
	}
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>exam_routines/routine_data_save" method="post">
	<div class="table-sorter-wrapper col-lg-12 table-responsive">
		<table id="sortable-table-1" class="table">
			<thead>
			<tr>
				<th scope="col">SL</th>
				<th scope="col">Code</th>
				<th scope="col">Name</th>
				<th scope="col">Date</th>
				<th scope="col">Session</th>
				<th scope="col">Room No.</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$sl = 0;
			foreach ($subject_list as $row):
			?>
				<tr>
					<td><?php echo $sl + 1; ?></td>
					<td><?php echo $row['code']; ?></td>
					<td>
						<?php echo $row['name']; ?>
						<input type="hidden" class="input-text-short" name="subject_id_<?php echo $sl; ?>"
							   value="<?php echo $row['subject_id']; ?>"/>
					</td>
					<td>
						<div class="input-group date" style="width: 250px;" >
							<input type="text" style="padding: 6px; width: 200px;" autocomplete="off" onchange="dateCheck(this.value,<?php echo $sl; ?>)"
								value="<?php echo $row['date']; ?>"   name="date_<?php echo $sl; ?>" id="date_<?php echo $sl; ?>" required class="form-control">
							<span class="input-group-text input-group-append input-group-addon">
                                <i class="simple-icon-calendar"></i>
                            </span>
						</div>
					</td>
					<td>
						<select style="padding: 6px; width: 150px;" onchange="roomNoCheck(document.getElementById('room_no_id_' + <?php echo $sl; ?>).value,<?php echo $sl; ?>)" name="session_id_<?php echo $sl; ?>" id="session_id_<?php echo $sl; ?>" required="1">
							<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
							<?php
							$i = 0;
							if (count($exam_sessions)) {
								foreach ($exam_sessions as $list) {
									$i++; ?>
									<option value="<?php echo $list['id']; ?>" <?php if($row['session_id'] == $list['id']){ echo 'selected'; } ?>><?php echo $list['name']; ?></option>
									<?php
								}
							}
							?>
						</select>
					</td>
					<td>
						<select style="padding: 6px; width: 150px;"  onchange="roomNoCheck(this.value,<?php echo $sl; ?>)" name="room_no_id_<?php echo $sl; ?>" id="room_no_id_<?php echo $sl; ?>" required="1">
							<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
							<?php
							$i = 0;
							if (count($rooms)) {
								foreach ($rooms as $list) {
									$i++; ?>
									<option value="<?php echo $list['id']; ?>" <?php if($row['room_no'] == $list['id']){ echo 'selected'; } ?>><?php echo $list['name']; ?></option>
									<?php
								}
							}
							?>
						</select>
					</td>
				</tr>
			<?php $sl++; endforeach; ?>

			<tr>
				<td colspan="6">
					<input type="hidden" class="input-text-short" name="exam_id"
						   value="<?php echo $exam_id; ?>"/>
					<input type="hidden" class="input-text-short" name="class_id"
						   value="<?php echo $class_id; ?>"/>
					<input type="hidden" class="input-text-short" name="group_id"
						   value="<?php echo $group_id; ?>"/>
					<input type="hidden" class="input-text-short" name="total_subject" id="total_subject"
						   value="<?php echo $sl; ?>"/>
					<div class="float-right">
						<input class="btn btn-primary" type="submit" value="Save">
					</div>
				</td>
			</tr>

			</tbody>
		</table>
	</div>
</form>
