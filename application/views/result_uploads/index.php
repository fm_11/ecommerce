<?php //echo MEDIA_FOLDER; die; ?>
<script type="text/javascript">
    function msgStatusUpdate(id,status){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
                document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>result_uploads/updateMsgStatusResultUploadStatus?id=" + id + '&&status=' + status, true);
        xmlhttp.send();
    }

</script>
<div class="row">
      <div class="col-12">
              <div class="card mb-4">
                  <div class="card-body">
                      <h5 class="mb-4">
                        <?php echo $title; ?>
                      </h5>
            <div class="table-responsive-sm">
                <table  class="table">
                  <thead>
                    <tr>
                        <th width="50" scope="col">SL</th>
                        <th width="200" scope="col">Result Type</th>
                        <th width="200" scope="col">Date</th>
                        <th width="200" scope="col">Download</th>
                        <th width="200" scope="col">Publish Status</th>
                        <th width="100" scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach ($student_results as $row):
                        $i++;
                        ?>
                        <tr>

                        <tr>
                            <td width="34">
                                <?php echo $i; ?>
                            </td>
                            <td><?php echo $row['result_type']; ?></td>
                            <td><?php echo $row['date']; ?></td>
                            <td>
                                <a href="<?php echo base_url() . MEDIA_FOLDER; ?>/result/<?php echo $row['location']; ?>"
                                   target="_blank"><?php echo $row['location']; ?></a>
                            </td>

                            <td id="status_sction_<?php echo $row['id']; ?>">
                                <?php
                                if ($row['status'] == 1) {
                                    ?>
                                    <a class="badge badge-success mb-1" title="Publish" href="#"
                                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">Publish</a>
                                <?php
                                } else {
                                    ?>
                                    <a class="badge badge-warning mb-1" title="Unpublish" href="#"
                                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['status']; ?>)">Unpublish</a>
                                <?php
                                }
                                ?>
                            </td>

                            <td>
                                <a href="<?php echo base_url(); ?>result_uploads/delete/<?php echo $row['id']; ?>"
                                   onclick="return deleteConfirm()" class="badge badge-danger mb-1" title="Delete">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    <tr class="footer">
                        <td colspan="6" align="right">
                            <!--  PAGINATION START  -->
                            <div class="pagination">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <!--  PAGINATION END  -->
                        </td>
                    </tr>
                    </tbody>
</table>
  </div>
</div>
</div>
</div>
</div>
