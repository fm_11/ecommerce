<div class="row">
      <div class="col-12">
              <div class="card mb-4">
                  <div class="card-body">
                      <h5 class="mb-4">
                        <?php echo $title; ?>
                      </h5>
                        <form name="addForm" class="cmxform" id="commentForm"   class="cmxform" id="commentForm" action="<?php echo base_url(); ?>result_uploads/add" method="post" enctype="multipart/form-data">
                          <div class="form-row">
                                <div class="form-group col-md-4">
                                  <label>Result Type</label>
                                  <input type="text" autocomplete="off"  class="form-control" name="txtResultType" required="1"/>
                                </div>
                                <div class="form-group col-md-4">
                                  <label>Date</label>
                                  <div class="input-group date">
                                           <input type="text" autocomplete="off"  name="txtDate" required value="<?php echo date('Y-m-d') ?>" class="form-control">
                                           <span class="input-group-text input-group-append input-group-addon">
                                               <i class="simple-icon-calendar"></i>
                                           </span>
                                   </div>
                                </div>
                                <div class="form-group col-md-4">
                                  <label>Remarks</label>
                                  <input type="text" autocomplete="off"  class="form-control" name="txtRemarks" required="1"/>

                                </div>
                          </div>

                          <div class="form-row">
                                <div class="form-group col-md-12">
                                  <label>File</label>
                                  <input type="file" name="txtFile" required="1" class="form-control"> * File Format -> PDF , DOC , DOCX  and xls
                                </div>
                          </div>

                          <button type="submit" class="btn btn-primary d-block mt-3">Submit</button>
                    </form>
                  </div>
             </div>
          </div>
       </div>
