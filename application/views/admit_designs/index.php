<script type="text/javascript" src="<?php echo base_url(); ?>core_media/js/jscolor/jscolor.js"></script>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>admit_designs/index" method="post">
	<div class="form-row">
		<div class="form-group col-md-3">
			<label>Background Color</label>
			<input type="text" readonly autocomplete="off" style="background-color: <?php echo $config_info->background_color; ?>" value="<?php echo $config_info->background_color; ?>"  required name="background_color" class="form-control color">
		</div>

		<div class="form-group col-md-3">
			<label>Header Color</label>
			<input type="text" readonly autocomplete="off"  style="background-color: <?php echo $config_info->header_color; ?>"  required name="header_color" class="form-control color" value="<?php echo $config_info->header_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Text Color</label>
			<input type="text" readonly autocomplete="off" style="background-color: <?php echo $config_info->text_color; ?>" required name="text_color" class="form-control color" value="<?php echo $config_info->text_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Main 1st Border</label>
			<input type="text" readonly autocomplete="off"  style="background-color: <?php echo $config_info->main_first_border_color; ?>"  required name="main_first_border_color" class="form-control color" value="<?php echo $config_info->main_first_border_color; ?>">
		</div>

	</div>

	<div class="form-row">

		<div class="form-group col-md-3">
			<label>Main 2nd Border</label>
			<input type="text" readonly autocomplete="off" style="background-color: <?php echo $config_info->main_second_border_color; ?>"  required name="main_second_border_color" class="form-control color" value="<?php echo $config_info->main_second_border_color; ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Design</label><br>
			<select style="padding: 10px;width: 250px;" name="design_type">
				<option value="WIR" <?php if($config_info->design_type == 'WIR'){ echo 'selected'; } ?>>WITHOUT ROUTINE</option>
				<option value="WR" <?php if($config_info->design_type == 'WR'){ echo 'selected'; } ?>>WITH ROUTINE</option>
			</select>
		</div>

	</div>
	<input type="hidden" readonly autocomplete="off"  required name="id" class="form-control" value="<?php echo $config_info->id; ?>">
	<div class="float-right">
		<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
