<?php
$from_date = $this->session->userdata('from_date');
$to_date = $this->session->userdata('to_date');
?>

<form name="addForm" class="cmxform" id="commentForm"  method="post" action="<?php echo base_url(); ?>fund_transfers/index">
	<div class="form-row">
		<div class="form-group col-md-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" style="padding: 10px !important;" placeholder="From Date"  name="from_date" <?php if (isset($from_date)) { ?>
					value="<?php if (isset($from_date)) {
						echo $from_date;
					} ?>"
				<?php } ?> class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
						 <i class="simple-icon-calendar"></i>
					 </span>
			</div>
		</div>
		<div class="form-group col-md-3">
			<div class="input-group date">
				<input type="text" autocomplete="off" style="padding: 10px !important;" placeholder="To Date"  name="to_date" <?php if (isset($to_date)) { ?>
					value="<?php if (isset($to_date)) {
						echo $to_date;
					} ?>"
				<?php } ?> class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
						 <i class="simple-icon-calendar"></i>
					 </span>
			</div>
		</div>
		<div class="form-group col-md-2">
			<button type="submit" style="padding: 13px 30px 13px 30px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
		</div>
	</div>
</form>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th scope="col"><?php echo $this->lang->line('date'); ?></th>
        <th scope="col"><?php echo 'From Asset Head'; ?></th>
        <th scope="col"><?php echo 'To Asset Head'; ?></th>
        <th scope="col"><?php echo $this->lang->line('amount'); ?></th>
        <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($tdata as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['date']; ?></td>
            <td><?php echo $row['from_asset_category_name']; ?></td>
            <td><?php echo $row['to_asset_category_name']; ?></td>
            <td><?php echo $row['amount']; ?></td>
            <td>
                   <div class="dropdown">
                       <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <i class="ti-pencil-alt"></i>
                       </button>
                       <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" target="_blank" href="<?php echo base_url(); ?>fund_transfers/view/<?php echo $row['id']; ?>"><?php echo $this->lang->line('view'); ?></a>
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>fund_transfers/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                       </div>
                   </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
