<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $title; ?></title>
    <link href="<?php echo base_url() . MEDIA_FOLDER; ?>/receipt_style/receipt.css" rel="stylesheet">

    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
</head>
<body>
<table width="73%" cellpadding="0" cellspacing="0" id="box-table-a">
    <tr>
        <th width="100%" style="text-align:right" scope="col">
            <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
        </th>
    </tr>
</table>
<br>
<div class="wrapper" id="printableArea">
    <table class="header">
        <tbody>
        <tr>
            <td nowrap="nowrap" style="text-align:center;" colspan="2" width="100%">
            <table width="100%">
                  <tr>
                    <td width="30%">
                    <p><img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info->picture; ?>"></p>
                    </td>

                   <td width="70%" style="text-align:right;">
                      <b><?php echo $school_info->school_name; ?></b><br>
                       <?php
                       if ($school_info->eiin_number != '') {
                           echo 'EIIN No.' . $school_info->eiin_number . '<br>';
                       }
                       ?>
                      <?php echo $school_info->address; ?><br>
                      <?php echo 'Mobile: ' . $school_info->mobile; ?><br>
                      <?php echo 'Email: ' . $school_info->email; ?>
                   </td>
                  </tr>
            </table>

            </td>
        </tr>

         <tr>
            <td nowrap="nowrap" width="30%">
			       Date: <?php echo date("d/m/Y", strtotime($fund_transfer_info[0]['date'])); ?>
            </td>
            <td width="70%" style="text-align:right;">
                  <font class="paid">Fund Transfer Voucher</font><br>
                  <span style="color:green;">Transaction By: <?php echo $fund_transfer_info[0]['user_name']; ?></span><br>
                  <span style="color:red;">Confidential</span>
            </td>
        </tr>
        </tbody>
    </table>




	<table class="items">

        <tbody>
        <tr class="title textcenter">
            <td width="20%">From</td>
            <td width="30%">To</td>
            <td width="30%">Amount</td>
        </tr>

        <tr>
            <td align="center"><?php echo $fund_transfer_info[0]['from_asset_head']; ?></td>
            <td align="center"><?php echo $fund_transfer_info[0]['to_asset_head']; ?></td>

            <td class="textcenter">
                <?php
                $total = $fund_transfer_info[0]['amount']; ?>
                ৳<?php echo number_format($fund_transfer_info[0]['amount'], 2); ?>BDT
            </td>
        </tr>


        <tr class="title">
            <td class="textright" colspan="2">Total:</td>
            <td class="textcenter">৳<?php echo number_format($total, 2); ?>BDT</td>
        </tr>

        <tr>
            <td class="textleft" colspan="3">&nbsp;<b>In Words: <?php echo $this->numbertowords->convert_number($total); ?> Taka Only.</b></td>
        </tr>


            <tr>
                <td colspan="3">
                    <br> <br>
                    ...............................................<br>
                   <b>Accountant</b>
                </td>
            </tr>

        </tbody>
    </table>
    Powered by: School360
</div>
</body>
</html>
