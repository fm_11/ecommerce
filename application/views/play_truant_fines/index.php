<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<div class="table-responsive-sm">
    <table  class="table">
        <thead>
    <tr>
        <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th scope="col"><?php echo $this->lang->line('student_code'); ?></th>
        <th scope="col"><?php echo $this->lang->line('student').' '.$this->lang->line('name'); ?></th>
        <th scope="col"><?php echo $this->lang->line('class'); ?></th>
        <th scope="col"><?php echo $this->lang->line('section'); ?></th>
        <th scope="col"><?php echo $this->lang->line('amount').'  ( '.$this->lang->line('tk').' )'; ?>
</th>
        <th scope="col"><?php echo $this->lang->line('year'); ?></th>
        <th scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($students as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['student_code']; ?></td>
            <td><?php echo $row['student_name']; ?></td>
            <td><?php echo $row['class_name']; ?></td>
            <td><?php echo $row['section_name']; ?></td>
            <td><?php echo $row['amount']; ?></td>
            <td><?php echo $row['year']; ?></td>
            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>student_fees/student_play_truant_fine_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>

    <tr class="footer">
        <td colspan="9" style="color: #000000;" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>

    </tbody>
</table>
</div>

