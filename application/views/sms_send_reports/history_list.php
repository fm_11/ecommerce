<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
		}
	</style>

	<?php
} ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
		<thead>
		<tr>
			<td colspan="8"  class="h_td" style="text-align:center; line-height:1.5;">
				<b><?php echo $HeaderInfo['school_name']; ?></b><br>
				<?php echo $HeaderInfo['address']; ?><br>
				<?php echo $title; ?> of <b>(<?php echo date("d-m-Y", strtotime($from_date)). ' to '. date("d-m-Y", strtotime($to_date)); ?>)</b>
			</td>
		</tr>

		<tr>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;<?php echo $this->lang->line('sl'); ?></td>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;Date Time</td>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;MSISDN</td>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;SMS Body</td>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;Student/Teacher</td>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;Number of SMS</td>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;Sender</td>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;Status</td>
		</tr>

		<?php
		$i = 1;
		foreach ($history_list as $rt_row):
				?>
				<tr>
					<td style="text-align: center;">
						<?php echo $i; ?>
					</td>
					<td>
						<?php echo $rt_row['date_time']; ?>
					</td>
					<td>
						<?php echo $rt_row['msisdn']; ?>
					</td>
					<td>
						<?php echo $rt_row['sms_body']; ?>
					</td>
					<td>
						<?php
						if($rt_row['student_id'] != '0'){
							echo $rt_row['student_name'] . '(' . $rt_row['student_code'] . ')';
						}
						if($rt_row['teacher_id'] != '0'){
							echo $rt_row['teacher_name'] . '(' . $rt_row['teacher_code'] . ')';
						}
						if($rt_row['student_id'] == '0' && $rt_row['teacher_id'] == '0'){
							echo '-';
						}
						?>
					</td>
					<td>
						<?php echo $rt_row['number_of_sms']; ?>
					</td>
					<td style="text-align: center;min-width: 120px;">
						<?php
							if($rt_row['sender_type'] == 'TG'){
								echo 'To Guardian';
							}else if($rt_row['sender_type'] == 'RN'){
								echo 'Result Notify';
							}else if($rt_row['sender_type'] == 'EX'){
								echo 'From Excel';
							}else if($rt_row['sender_type'] == 'PH'){
								echo 'Phone Book';
							}else if($rt_row['sender_type'] == 'AS'){
								echo 'Absent SMS';
							}else if($rt_row['sender_type'] == 'TS'){
								echo 'Teacher SMS';
							}else{
								echo 'Unidentified';
							}
						?>
					</td>
					<td>
						<?php echo $rt_row['sms_status']; ?>
					</td>
				</tr>
				<?php
		   $i++;
			 endforeach; ?>
		</tbody>
	</table>
</div>
