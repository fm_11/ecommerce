<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
		}
	</style>

	<?php
} ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
		<thead>
		<tr>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;Number List</td>
			<td scope="col" style="text-align: center;" class="h_td">&nbsp;Number List With Status</td>
		</tr>

		<tr>
			<td style="text-align: left; vertical-align: top; line-height: 20px; width: 50%;">
				<?php
				foreach ($history_details as $rt_row):
					if($rt_row['student_id'] != ''){
						echo $rt_row['student_code'] . ' - ' . $rt_row['student_name'] . ' ( ' . $rt_row['mobile_number'] . ' )<br>';
					}
					else if($rt_row['teacher_id'] != ''){
						echo $rt_row['teacher_code'] . ' - ' . $rt_row['name'] . ' ( ' . $rt_row['mobile_number'] . ' )<br>';
					}else{
						echo $rt_row['mobile_number'] . '<br>';
					}
				endforeach;
				?>
			</td>
			<td style="text-align: left; vertical-align: top;line-height: 20px; width: 50%;">
				<?php
				if(empty($details_with_status)){
					echo 'Not Found';
				}else{
					echo $details_with_status[0]['sms_shoot_id'];
				}
				?>
			</td>
		</tr>

		</tbody>
	</table>
</div>
