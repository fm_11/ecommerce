<script>
	function printDiv(divName) {
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
	}
</script>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>sms_send_reports/index#sms_report" method="post">
	<div class="form-row">

		<div class="form-group col-md-4">
			<label>SMS From</label><br>
			<select name="sms_from" class="js-example-basic-single w-100">
				<option value="ALL" <?php if(isset($sms_from)){ if($sms_from == 'ALL'){ echo 'selected'; }} ?>>ALL SMS</option>
				<option value="TG" <?php if(isset($sms_from)){ if($sms_from == 'TG'){ echo 'selected'; }} ?>>To Guardian</option>
				<option value="RN" <?php if(isset($sms_from)){ if($sms_from == 'RN'){ echo 'selected'; }} ?>>Result Notify</option>
				<option value="EX" <?php if(isset($sms_from)){ if($sms_from == 'EX'){ echo 'selected'; }} ?>>From Excel</option>
				<option value="PH" <?php if(isset($sms_from)){ if($sms_from == 'PH'){ echo 'selected'; }} ?>>Phone Book</option>
				<option value="AS" <?php if(isset($sms_from)){ if($sms_from == 'AS'){ echo 'selected'; }} ?>>Absent SMS</option>
				<option value="TS" <?php if(isset($sms_from)){ if($sms_from == 'TS'){ echo 'selected'; }} ?>>Teacher SMS</option>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('from_date'); ?></label><br>
			<div class="input-group date">
				<input type="text" autocomplete="off"  name="from_date" required value="<?php if (isset($from_date)) {
					echo $from_date;
				} ?>" class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
			</div>
		</div>
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('to_date'); ?></label><br>
			<div class="input-group date">
				<input type="text" autocomplete="off"  name="to_date" required value="<?php if (isset($to_date)) {
					echo $to_date;
				} ?>" class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                  <i class="simple-icon-calendar"></i>
              </span>
			</div>
		</div>
	</div>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-primary" value="View Report">
	</div>
</form>

<div id="printableArea">
	<?php
	if (isset($report)) {
		echo $report;
	}
	?>
</div>
