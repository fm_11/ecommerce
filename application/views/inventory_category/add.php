<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>inventory_category/add" method="post">


  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('category').' '.$this->lang->line('name') ?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="category_name" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('code')?> <span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="code" required="1"/>
    </div>
  </div>

    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
