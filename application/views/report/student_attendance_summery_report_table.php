<style>
    th,td {
        border: 1px solid;
    }
</style>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td class="center_td" colspan="<?php echo count($class) + 4; ?>" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $title; ?> <br>
              <?php echo $this->lang->line('for_month_of'); ?> <?php
                $dateObj = DateTime::createFromFormat('!m', $month);
                echo $dateObj->format('F') . ', ' . $year;
                ?>
            </b>
            <br><br>
        </td>
    </tr>

    <tr>
        <th class="center_td">&nbsp;<?php echo $this->lang->line('date'); ?></th>
        <th class="center_td"><?php echo $this->lang->line('description'); ?></th>
        <?php
        foreach ($class as $row):
            ?>
            <th class="center_td"><?php echo $row['name']; ?></th>
            <?php
        endforeach;
        ?>
        <th class="center_td"><?php echo $this->lang->line('total'); ?></th>
        <th class="center_td"><?php echo $this->lang->line('day'); ?></th>
    </tr>

    </thead>


    <tbody>
    <?php
    $i = 1;
    while ($i <= $number_of_days) {
        ?>
        <tr>
            <td style="text-align: center;">
                <?php
                if ($i < 10) {
                    $date = '0' . $i . '-' . $month . '-' . $year;
                } else {
                    $date = $i . '-' . $month . '-' . $year;
                }
                echo $date;
                ?>
            </td>
            <td align="right">
                <table width="100%">
                    <tr>
                        <td align="right">
                            <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('student'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('Present'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('absent'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('leave'); ?>
                        </td>
                    </tr>
                </table>
            </td>

            <?php
            echo $adata[$i]['data'];
            ?>

            <td align="center">
                <?php
                $day = date('l', strtotime($date));
                echo $day;
                ?>
            </td>
        </tr>
        <?php $i++;
    } ?>
    </tbody>

</table>
