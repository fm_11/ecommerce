<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td class="center_td" colspan="10" style="text-align:center">
            <b style="font-size:16px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:13px;">EIIN: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <span style="font-size: 16px;"><?php echo $title; ?></span><br>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="10" style="background-color: #99bbff; height: 25px; font-size: 14px;">
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
          <?php echo $this->lang->line('section'); ?>: <b><?php echo $section_name; ?></b>,
            <?php echo $this->lang->line('group'); ?>: <b><?php echo $group_name; ?></b>,
            <?php echo $this->lang->line('shift'); ?>: <b><?php echo $shift_name; ?></b>,
            <?php echo $this->lang->line('exam'); ?>: <b><?php echo $exam_name . ', ' . $exam_year; ?></b>
        </td>
    </tr>
    <tr>
        <th width="240" scope="col" align="center">&nbsp;<?php echo $this->lang->line('subject'); ?> <?php echo $this->lang->line('name'); ?></th>
        <th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('subject'); ?> <?php echo $this->lang->line('code'); ?></th>
        <th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('credit'); ?></th>
        <th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('creative'); ?></th>
        <th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('objective'); ?></th>
        <th width="80" align="center" scope="col">&nbsp;<?php echo $this->lang->line('practical'); ?></th>
        <th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('marks'); ?></th>
        <th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('letter_grade'); ?></th>
        <th width="120" align="center" scope="col">&nbsp;<?php echo $this->lang->line('grade_point'); ?></th>
    </tr>
    </thead>

    <tbody>
    <?php
    $i = 0;
    foreach ($result_data as $row):
        ?>

        <tr>
            <td colspan="10" style="background-color: #ebebe0; height: 25px; font-size: 14px;">
                &nbsp;<?php echo $this->lang->line('name'); ?>: <b><?php echo $row['student_name']; ?></b>,
                <?php echo $this->lang->line('student_code'); ?>: <b><?php echo $row['student_code']; ?></b>,
                <?php echo $this->lang->line('roll'); ?>: <b><?php echo $row['roll_no']; ?></b>,
              <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('obtain'); ?> <?php echo $this->lang->line('mark'); ?>: <b><?php echo $row['total_obtain_mark']; ?></b>,
              <?php echo $this->lang->line('gpa'); ?>: <b><?php echo $row['gpa_with_optional']; ?></b>,
              <?php echo $this->lang->line('grade'); ?>: <b><?php echo $row['c_alpha_gpa_with_optional']; ?></b>
            </td>
        </tr>

        <?php
        foreach ($result_data[$i]['result'] as $result_row):
            ?>

            <tr <?php if ($result_row['is_optional'] == '1') { ?> style="background-color: #ebebe0" <?php } ?>>
                <td>
                    &nbsp;<?php echo $result_row['subject_name']; ?>
                </td>
                <td align="center">
                    &nbsp;<?php echo $result_row['subject_code']; ?><br>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo $result_row['credit'];
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo $result_row['written'];
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo $result_row['objective'];
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo $result_row['practical'];
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo $result_row['total_obtain'];
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo $result_row['alpha_gpa'];
                    ?>
                </td>
                <td align="center">&nbsp;
                    <?php
                    echo $result_row['numeric_gpa'];
                    ?>
                </td>
            </tr>
            <?php
        endforeach;
        ?>
        <?php
        $i++;
    endforeach;
    ?>
    </tbody>
</table>
