<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td class="center_td" colspan="12" style="text-align:center">
            <br>
            <b style="font-size:20px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:16px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php echo $title; ?> <br>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="12">
           &nbsp;<?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
            <?php echo $this->lang->line('section'); ?>: <b><?php echo $section_name; ?></b>,
			<?php echo $this->lang->line('group'); ?>: <b><?php echo $group_name; ?></b>,
			<?php echo $this->lang->line('shift'); ?>: <b><?php echo $shift_name; ?></b>
        </td>
    </tr>

	 <tr>
        <th width="20" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('sl'); ?></th>
        <th width="150" scope="col" rowspan="2" align="center">&nbsp;<?php echo $this->lang->line('student_id'); ?></th>
		<th width="300" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('name'); ?></th>
        <th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('roll'); ?></th>
        <th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('group'); ?></th>
        <th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('section'); ?></th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('obtain'); ?> <?php echo $this->lang->line('mark'); ?> </th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('gpa'); ?> <?php echo $this->lang->line('with_optional'); ?></th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('gpa'); ?> <?php echo $this->lang->line('without_optional'); ?></th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('grade'); ?> <?php echo $this->lang->line('without_optional'); ?></th>
		<th width="100" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('grade'); ?> <?php echo $this->lang->line('with_optional'); ?></th>
		<th width="80" scope="col" rowspan="2">&nbsp;<?php echo $this->lang->line('position'); ?></th>

    </tr>

     </thead>

	  <tbody>
		<?php
		$i = 0;
		foreach ($rdata as $row):

		?>
        <tr>
            <td width="34">
                &nbsp;<?php echo $i + 1; ?>
            </td>
            <td align="center">
                &nbsp;<?php echo $row['student_code']; ?><br>
            </td>
			<td>
                &nbsp;<?php echo $row['name']; ?><br>
            </td>
             <td align="center">
                &nbsp;<?php echo $row['roll_no']; ?><br>
            </td>
              <td align="center">
                &nbsp;<?php echo $row['group_name']; ?><br>
            </td>
              </td>
              <td align="center">
                &nbsp;<?php echo $row['section_name']; ?><br>
            </td>

			<td align="center">
				&nbsp;<?php echo $row['total_obtain_mark']; ?><br>
			</td>
			<td align="center">
				&nbsp;<?php echo $row['gpa_without_optional']; ?><br>
			</td>

			<td align="center">
				&nbsp;<?php echo $row['gpa_with_optional']; ?><br>
			</td>

			<td align="center">
				&nbsp;<?php echo $row['c_alpha_gpa_without_optional']; ?><br>
			</td>

			<td align="center">
				&nbsp;<?php echo $row['c_alpha_gpa_with_optional']; ?><br>
			</td>

			<td align="center">
				&nbsp;<?php echo $all_student_position[$row['student_id']]['position']; ?><br>
			</td>
        </tr>
    <?php $i++; endforeach; ?>
    </tbody>

</table>
