<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }


    function start() {
        selectMonth();
    }
    window.onload = start;

    function selectMonth() {
        document.my_form.get_month.options[new Date().getMonth()].selected = true;
        document.my_form.get_day.options[(new Date().getUTCDate()) - 1].selected = true;
    }
</script>

<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>report/teacher_staff_report_absentee" id="my_form" name="my_form"
      method="post">
      <div class="form-row">
        <div class="form-group col-md-3">
          <label><?php echo $this->lang->line('person'); ?> <?php echo $this->lang->line('type'); ?></label>
          <select class="js-example-basic-single w-100"  name="person_type" id="person_type" required="required">
             <option value="T"><?php echo $this->lang->line('teacher'); ?></option>
             <option value="ST"><?php echo $this->lang->line('staff'); ?></option>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label>Day</label>
          <select name="get_day" id="get_day" class="js-example-basic-single w-100" required="required">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
              <option>11</option>
              <option>12</option>
              <option>13</option>
              <option>14</option>
              <option>15</option>
              <option>16</option>
              <option>17</option>
              <option>18</option>
              <option>19</option>
              <option>20</option>
              <option>21</option>
              <option>22</option>
              <option>23</option>
              <option>24</option>
              <option>25</option>
              <option>26</option>
              <option>27</option>
              <option>28</option>
              <option>29</option>
              <option>30</option>
              <option>31</option>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label>Month</label>
          <select class="js-example-basic-single w-100"  name="get_month" id="get_month">
              <option value='01'>January</option>
              <option value='02'>February</option>
              <option value='03'>March</option>
              <option value='04'>April</option>
              <option value='05'>May</option>
              <option value='06'>June</option>
              <option value='07'>July</option>
              <option value='08'>August</option>
              <option value='09'>September</option>
              <option value='10'>October</option>
              <option value='11'>November</option>
              <option value='12'>December</option>
          </select>
        </div>
        <div class="form-group col-md-3">
          <label>Year</label>
          <select name="get_year" class="js-example-basic-single w-100" name="get_year">
            <?php
            if (count($years)) {
                foreach ($years as $list) {
                    ?>
                    <option value="<?php echo $list['value']; ?>" <?php if (isset($year)) {
                        if ($year == $list['value']) {
                            echo 'selected';
                        }
                    } ?>><?php echo $list['text']; ?></option>
                    <?php
                }
            }
            ?>
          </select>
        </div>
      </div>


      <div class="btn-group float-right">
        <input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
          <!--<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/> -->
        <input type="submit" class="btn btn-primary" value="View Report">
      </div>
</form>


<div class="table-sorter-wrapper col-lg-12 table-responsive" id="printableArea">
    <?php
    if (isset($absentee_info)) {
        ?>
        <table width="100%" id="sortable-table-1" class="table">
            <thead>
            <tr>
                <td colspan="7" style="text-align:center">
                    <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                    <b style="font-size:13px;">
                      <?php //echo $this->lang->line('eiin');?> <?php //echo $HeaderInfo['eiin_number'];?>
                        <?php
                        if ($person_type == 'T') {
                            echo 'Teacher';
                        } else {
                            echo 'Staff';
                        } ?> <?php echo $this->lang->line('absentees'); ?> <?php echo $this->lang->line('list'); ?> <?php echo $this->lang->line('report_for'); ?> <?php echo date('d-M-Y', strtotime($date)); ?>
                      </b>

                </td>
            </tr>

            <tr>
                <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
                <th scope="col"><?php echo $this->lang->line('name'); ?></th>
                <th scope="col"><?php echo $this->lang->line('index_no'); ?></th>
                <th scope="col"><?php echo $this->lang->line('designation'); ?></th>
            </tr>

            </thead>
            <tbody>
            <?php
            $i = 0;
        foreach ($absentee_info as $row):
                $i++; ?>
                <tr>
                    <td>
                        <?php echo $i; ?>
                    </td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['index_no']; ?></td>
                    <td><?php echo $row['post_name']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    <?php
    }
    ?>
</div>
