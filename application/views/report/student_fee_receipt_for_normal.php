<script>
    $(function () {
        $("#from_date,#to_date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>report/getStudentFeeReceiptForNormal" method="post">

    <label>Class</label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--Please Select--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>



    <label><?php echo $this->lang->line('section'); ?></label>
    <select name="section_id" class="smallInput" required="1" id="section_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($section_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Group</label>
    <select name="group" required="1" class="smallInput">
        <option value="">-<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($group_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>


    <label><?php echo $this->lang->line('from_date'); ?></label>
    <input type="text" autocomplete="off"  name="from_date" required value="" placeholder="yyyy-mm-dd" style="display: -moz-stack !important; width: 150px;"
           class="smallInput" id="from_date">

    <label><?php echo $this->lang->line('to_date'); ?></label>
        <input type="text" autocomplete="off"  name="to_date" required value="" placeholder="yyyy-mm-dd" style="display: -moz-stack !important; width: 150px;"
               class="smallInput" id="to_date">


    <br><br>
    <input type="submit" class="submit" value="View Report">
    <br><br>
</form>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
