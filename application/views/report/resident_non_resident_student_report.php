<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>report/resident_non_resident_student" method="post">
    <label>Year</label>
    <select class="smallInput" name="year" id="year" required="1">
        <option value="">-- Please Select --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>

    <label>Class</label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--Please Select--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Shift</label>
    <select name="shift_id" class="smallInput" required="1" id="shift_id">
        <option value="">--Please Select--</option>
        <?php foreach ($shifts as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Section</label>
    <select name="section_id" class="smallInput" required="1" id="section_id">
        <option value="">--Please Select--</option>
        <?php foreach ($section_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Group</label>
    <select name="group" required="1" class="smallInput">
        <option value="">--Please Select--</option>
        <?php foreach ($group_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>
	
	<label>Report Type</label>
    <select class="smallInput" name="is_resident" id="is_resident" required="1">
        <option value="">-- Please Select --</option>
        <option value="Y">Resident Student</option>
        <option value="N">Non-Resident Student</option>      
    </select>

    <br>
    <br>
        <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
		<input type="submit" name="pdf_download" value="PDF Download"/>
		<input type="submit" class="submit" value="View Report">
</form>



<div id="printableArea">
    <?php
    if (isset($idata)) {
        echo $report;
    }
    ?>
</div>