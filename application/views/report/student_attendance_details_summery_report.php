<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }


    $(function () {
        $("#FromDate,#ToDate").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });




</script>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>report/getStudentAttendanceDetailsSummeryReport"
      method="post">

    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tbody>
        <tr>
            <td>
                <label><?php echo $this->lang->line('from_date'); ?></label>
                <input type="text" autocomplete="off"  class="smallInput" name="FromDate" id="FromDate" required="1"/>
            </td>

            <td>
                <label><?php echo $this->lang->line('to_date'); ?></label>
                <input type="text" autocomplete="off"  class="smallInput" name="ToDate" id="ToDate" required="1"/>
            </td>

            <td>
                <label><?php echo $this->lang->line('class'); ?></label>
                <select name="class_id"
                        style="width:150px;"  class="smallInput" id="class_id" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php foreach ($class as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>
            </td>

            <td>
                <label><?php echo $this->lang->line('section'); ?></label>
                <select name="section_id"
                        style="width:150px;"  class="smallInput" id="section_id" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php foreach ($section as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>

        <tr>
            <td colspan="5" align="right">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
                <input type="submit" name="pdf_download" value="PDF Download"/>
                <input type="submit" class="submit" value="View Report">
            </td>
        </tr>
        </tbody>
    </table>


</form>
<br>


<div id="printableArea">
    <?php
    if (isset($adata)) {
        echo $report;
    }
    ?>
</div>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
