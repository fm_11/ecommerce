<hr>
<div style="margin-left: 100px; font-size: 14px; font-weight: bold;">
    <?php
    if($fee_module_type == "S"){
        ?>
        <li><a href="<?php echo base_url(); ?>report/getStudentFeeStatusDetails"><span>Student Fee Status Details</span></a></li>
		<li><a href="<?php echo base_url(); ?>report/getCollectionSummeryStandard"><span>Collection Summery</span></a></li>
		<li><a href="<?php echo base_url(); ?>report/getCollectionDetailsStandard"><span>Collection Details Report</span></a></li>
		<li><a href="<?php echo base_url(); ?>report/getSpecialCareCollection"><span>Special Care Fee Collection Report</span></a></li>
		
        <?php
    }else{
        ?>
        <li><a href="<?php echo base_url(); ?>report/getStudentFeeStatus"><span>Fee Status</span></a></li>
        <li><a href="<?php echo base_url(); ?>report/getClassWiseCollectionSummery"><span>Collection Summery</span></a></li>
        <li><a href="<?php echo base_url(); ?>report/getStudentFeeReceiptForNormal"><span>Fee Receipt</span></a></li>
        <?php
    }
    ?>
</div>