
<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
            <thead>
            <tr>
                <td style="text-align:center;" colspan="<?php echo $number_of_days + 4; ?>" style="text-align:center">
                    <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                    <!-- <b style="font-size:13px;">EIIN: <?php //echo $HeaderInfo['eiin_number'];?><br> -->
                      <?php echo $this->lang->line('teacher'); ?> <?php echo $this->lang->line('attendance'); ?> <?php echo $this->lang->line('details'); ?><?php echo $this->lang->line('report'); ?> <br>
					  <?php echo $this->lang->line('for_month_of'); ?> <?php
                      $dateObj   = DateTime::createFromFormat('!m', $month);
                      echo $dateObj->format('F').', '.$year;
                      ?>
					  </b>
					  <br><br>
                </td>
            </tr>

            <tr>
			    <th width="20" scope="col">&nbsp;<?php echo $this->lang->line('sl'); ?></th>
                <th width="250" scope="col">&nbsp;<?php echo $this->lang->line('name'); ?></th>
                <th width="150" scope="col">&nbsp;<?php echo $this->lang->line('index_no'); ?></th>
                <?php
                $i = 1;
                while ($i <= $number_of_days) {
                    if ($i < 10) {
                        $clm_date = '0'.$i;
                    } else {
                        $clm_date = $i;
                    }
                    echo '<th width="50">'.$clm_date.'</th>';
                    $i++;
                }
                ?>
				<th width="150" scope="col">&nbsp;<?php echo $this->lang->line('summary'); ?></th>
            </tr>

            </thead>


			<tbody>
        <?php
        $j = 0;
        foreach ($adata as $row):
            $j++;
            ?>
            <tr>
                <td  style="text-align: center;"  class="columnHeader" width="34">
                    <?php echo $j; ?>
                </td>
                <td align="left"><b><?php echo $row['name']; ?></b></td>
                <td align="center"><?php echo $row['teacher_index_no']; ?></td>
                <?php echo $row['data']; ?>
                <td align="center">
                    TP-<?php echo $row['total_present_days']; ?><br>
                    TA-<?php echo $row['total_absent_days']; ?><br>
                    TL-<?php echo $row['total_leave_days']; ?><br>
                    TH-<?php echo $row['total_holidays']; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>

        </table>
