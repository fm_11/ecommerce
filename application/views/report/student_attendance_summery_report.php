<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }


</script>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>report/getStudentAttendanceSummeryReport" method="post">

    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tbody>
        <tr>
            <td>
                <label><?php echo $this->lang->line('month'); ?></label>
                <select class="smallInput" style="width:200px;" name="month" id="month" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php
                    $i = 1;
                    while ($i <= 12) {
                        $dateObj = DateTime::createFromFormat('!m', $i);
                        ?>
                        <option value="<?php echo $i; ?>"><?php echo $dateObj->format('F'); ?></option>
                        <?php
                        $i++;
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label><?php echo $this->lang->line('Year'); ?></label>
                <select class="smallInput" style="width:200px;" name="year" required="1">
                    <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                    <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
                    <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                </select>
            </td>
        </tr>

        <tr>
            <td colspan="5" align="right">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
                <input type="submit" name="pdf_download" value="PDF Download"/>
                <input type="submit" class="submit" value="View Report">
            </td>
        </tr>
        </tbody>
    </table>




</form>
<br>


<div id="printableArea">
    <?php
    if (isset($adata)) {
        echo $report;
    }
    ?>
</div>
