<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

   $(function () {
        $("#from_date,#to_date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });

</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>report/studentGrandResultReport" method="post">
    <label>Year</label>
    <select class="smallInput" name="year" id="year" required="1">
        <option value="">-- Please Select --</option>
        <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
        <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
        <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
    </select>

    <label><?php echo $this->lang->line('class'); ?></label>
    <select name="class_id" class="smallInput" required="1" id="class_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($class_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label>Shift</label>
    <select name="shift_id" class="smallInput" required="1" id="shift_id">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($shifts as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label><?php echo $this->lang->line('section'); ?></label>
    <select name="section_id" class="smallInput" required="1" id="section_id">
        <option value="">--Please Select--</option>
        <?php foreach ($section_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

    <label><?php echo $this->lang->line('group'); ?></label>
    <select name="group" required="1" class="smallInput">
        <option value="">--<?php echo $this->lang->line('please_select'); ?>--</option>
        <?php foreach ($group_list as $row) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
        <?php } ?>
    </select>

 <br><br>
    <input type="submit" class="submit" value="View Report">
    <br><br>
</form>

<div id="printableArea">
    <?php
    if (isset($rdata)) {
        echo $report;
    }
    ?>
</div>


<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>
