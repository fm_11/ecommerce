<style>
    .columnHeader{
        font-size: 14px;
        border: 1px #000000 solid;
    }
</style>

<div id="printableArea">
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <thead>
        <tr>
            <td colspan="<?php echo count($fee_category) + 19; ?>" style="text-align:center;">
                <b style="font-size:16px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                <b style="font-size:14px;">EIIN: <?php echo $HeaderInfo['eiin_number']; ?></b><br>
                <b style="font-size:14px;">Class: <?php echo $HeaderInfo['ClassName']; ?>,
                    Section: <?php echo $HeaderInfo['SectionName']; ?><br>
                    Group: <?php echo $HeaderInfo['group_name']; ?><br>
                    Student Fee Status
                    for year of <?php echo $HeaderInfo['year']; ?></b><br><br>
            </td>
        </tr>

        <tr>
            <td width="50" class="columnHeader" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;" scope="col">SL</td>
            <td width="100" class="columnHeader"  style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"  scope="col">St. ID</td>
            <td width="100" class="columnHeader"  style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"  scope="col">Name</td>
            <td width="50" class="columnHeader"  style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"  scope="col">Roll</td>

            <?php
            $i = 1;
            while($i <= $to_month){
                ?>
                <td width="50"  style="font-weight: bold; text-align: center;border-top: 1px #c7c7c7 solid;" class="columnHeader"  scope="col">
                    <?php echo date('M',strtotime('2012-'.$i.'-01')); ?>&nbsp;
                </td>
            <?php
                $i++;
            }
            ?>

            <?php
            foreach ($fee_category as $row):
            ?>
                <td width="50" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"  class="columnHeader"  scope="col"><?php echo $row['name']; ?>&nbsp;</td>
            <?php endforeach; ?>

            <td width="50" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"  class="columnHeader"  scope="col">Abs. Fine</td>
            <td width="70" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"  class="columnHeader"  scope="col">Play Tru. Fine</td>
            <td width="50" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"  class="columnHeader"  scope="col">Waiver</td>
			<td width="50" style="font-weight: bold;text-align: center;border-top: 1px #c7c7c7 solid;"  class="columnHeader"  scope="col">Total Due</td>
        </tr>
        </thead>

        <tbody>
        <?php
        $month_grand_total_array = array();
        $fee_category_grand_total_array = array();
        
        $grand_total_absent_fine = 0;
        $grand_total_absent_fine_paid = 0;
        $grand_total_absent_fine_due = 0;
        
        $grand_total_play_truant_fine = 0;
        $grand_total_play_truant_fine_paid = 0;
        $grand_total_play_truant_fine_due = 0;
        
        $grand_total_amount = 0;
        $grand_total_amount_paid = 0;
        $grand_total_amount_due = 0;
        
        $grand_waiver_amount_from_total = 0;
        
        $j = 0;
        foreach ($info as $row):
            $j++;
            ?>
            <tr>

            <tr>
                <td  style="text-align: center;"  class="columnHeader" width="34">
                    <?php echo $j; ?>
                </td>
                <td class="columnHeader" style="text-align: center;"  ><?php echo $row['student_code']; ?></td>
                <td class="columnHeader"  style="font-weight: bold;">&nbsp;<?php echo $row['name']; ?></td>
                <td class="columnHeader" style="text-align: center;"><?php echo $row['roll_no']; ?></td>

            <?php
            $k = 1;
            while($k <= $to_month){
            ?>
            
                <td width="50" style="text-align: center;" class="columnHeader">
                    <?php
                    echo $row['monthly_fee'][$k]['monthly_fee'].'<br>';
                    echo $row['monthly_fee'][$k]['paid_monthly_fee'].'<br>';
                    echo $row['monthly_fee'][$k]['due_monthly_fee'];
                    if(empty($month_grand_total_array[$k])){
                        $month_grand_total_array[$k]['monthly_fee'] = $row['monthly_fee'][$k]['monthly_fee'];
                        $month_grand_total_array[$k]['paid_monthly_fee'] = $row['monthly_fee'][$k]['paid_monthly_fee'];
                        $month_grand_total_array[$k]['due_monthly_fee'] = $row['monthly_fee'][$k]['due_monthly_fee'];
                    }else{
                        $old_monthly_fee_array_value = $month_grand_total_array[$k]['monthly_fee'];
                        $old_paid_monthly_fee_array_value = $month_grand_total_array[$k]['paid_monthly_fee'];
                        $old_due_monthly_fee_array_value = $month_grand_total_array[$k]['due_monthly_fee'];
                        
                        $month_grand_total_array[$k]['monthly_fee'] = ($old_monthly_fee_array_value + $row['monthly_fee'][$k]['monthly_fee']) ;
                        $month_grand_total_array[$k]['paid_monthly_fee'] = ($old_paid_monthly_fee_array_value + $row['monthly_fee'][$k]['paid_monthly_fee']);
                        $month_grand_total_array[$k]['due_monthly_fee'] = ($old_due_monthly_fee_array_value + $row['monthly_fee'][$k]['due_monthly_fee']);
                    }
                    
                    ?>
                </td>
                
            <?php
                $k++;
            }
         //    echo '<pre>';
           // print_r($fee_category); die;
            ?>

            <?php
        
            foreach ($fee_category as $my_row):
                ?>
                <td width="50" style="text-align: center;" class="columnHeader"  scope="col">
                    <?php
                    if(isset($row['category_wise_fee'][$my_row['id']])){
                        echo $row['category_wise_fee'][$my_row['id']]['amount'].'<br>';
                        echo $row['category_wise_fee'][$my_row['id']]['paid_fee'].'<br>';
                        echo $row['category_wise_fee'][$my_row['id']]['due_fee'];
                        
                        
                        
                        if(empty($fee_category_grand_total_array[$my_row['id']])){
                            $fee_category_grand_total_array[$my_row['id']]['amount'] = $row['category_wise_fee'][$my_row['id']]['amount'];
                            $fee_category_grand_total_array[$my_row['id']]['paid_fee'] = $row['category_wise_fee'][$my_row['id']]['paid_fee'];
                            $fee_category_grand_total_array[$my_row['id']]['due_fee'] = $row['category_wise_fee'][$my_row['id']]['due_fee'];
                        }else{
                            $old_cat_amount_fee_array_value = $fee_category_grand_total_array[$my_row['id']]['amount'];
                            $old_cat_paid_fee_array_value = $fee_category_grand_total_array[$my_row['id']]['paid_fee'];
                            $old_cat_due_fee_array_value = $fee_category_grand_total_array[$my_row['id']]['due_fee'];
                            
                            $fee_category_grand_total_array[$my_row['id']]['amount'] = ($old_cat_amount_fee_array_value + $row['category_wise_fee'][$my_row['id']]['amount']) ;
                            $fee_category_grand_total_array[$my_row['id']]['paid_fee'] = ($old_cat_paid_fee_array_value + $row['category_wise_fee'][$my_row['id']]['paid_fee']);
                            $fee_category_grand_total_array[$my_row['id']]['due_fee'] = ($old_cat_due_fee_array_value + $row['category_wise_fee'][$my_row['id']]['due_fee']);
                        }
                        
                    }else{
                        echo "N/C";
                        if(empty($fee_category_grand_total_array[$my_row['id']])){
                            $fee_category_grand_total_array[$my_row['id']]['amount'] = 0;
                            $fee_category_grand_total_array[$my_row['id']]['paid_fee'] = 0;
                            $fee_category_grand_total_array[$my_row['id']]['due_fee'] = 0;
                        }else{
                            $old_cat_amount_fee_array_value = $fee_category_grand_total_array[$my_row['id']]['amount'];
                            $old_cat_paid_fee_array_value = $fee_category_grand_total_array[$my_row['id']]['paid_fee'];
                            $old_cat_due_fee_array_value = $fee_category_grand_total_array[$my_row['id']]['due_fee'];
                            
                            $fee_category_grand_total_array[$my_row['id']]['amount'] = ($old_cat_amount_fee_array_value + 0) ;
                            $fee_category_grand_total_array[$my_row['id']]['paid_fee'] = ($old_cat_paid_fee_array_value + 0);
                            $fee_category_grand_total_array[$my_row['id']]['due_fee'] = ($old_cat_due_fee_array_value + 0);
                        }
                    }
                    ?>

                    &nbsp;</td>
            <?php endforeach; ?>

            <td width="50" style="text-align: center;" class="columnHeader"  scope="col">
                <?php echo $row['absent_fee']; ?><br>
                <?php echo $row['absent_fee_paid']; ?><br>
                <?php echo $row['absent_fee_due']; ?>
               
                <?php
                    $grand_total_absent_fine = $grand_total_absent_fine + $row['absent_fee'];
                    $grand_total_absent_fine_paid = $grand_total_absent_fine_paid + $row['absent_fee_paid'];
                    $grand_total_absent_fine_due = $grand_total_absent_fine_due + $row['absent_fee_due'];
                ?>
            </td>

            <td width="50" style="text-align: center;" class="columnHeader"  scope="col">
                <?php echo $row['play_truant_fine']; ?><br>
                <?php echo $row['play_truant_fine_paid']; ?><br>
                <?php echo $row['play_truant_fine_due']; ?>
                <?php
                    $grand_total_play_truant_fine = $grand_total_play_truant_fine + $row['play_truant_fine'];
                    $grand_total_play_truant_fine_paid = $grand_total_play_truant_fine_paid + $row['play_truant_fine_paid'];
                    $grand_total_play_truant_fine_due = $grand_total_play_truant_fine_due + $row['play_truant_fine_due'];
                ?>
            </td>
			
			<td width="50" style="text-align: center;" class="columnHeader"  scope="col">
                <?php 
                echo $row['waiver_amount_from_total']; 
                $grand_waiver_amount_from_total = $grand_waiver_amount_from_total + $row['waiver_amount_from_total'];
                ?>            
            </td>

            <td width="50" style="text-align: center;font-weight: bold;" class="columnHeader"  scope="col">
                <?php echo $row['total_fee']; ?><br>
                <?php echo $row['total_fee_paid']; ?><br>
                <?php echo $row['total_fee_due']; ?>
                
                <?php
 
                $grand_total_amount = $grand_total_amount + $row['total_fee'];
                $grand_total_amount_paid = $grand_total_amount_paid + $row['total_fee_paid'];
                $grand_total_amount_due = $grand_total_amount_due + $row['total_fee_due'];
              
                ?>
            </td>

        </tr>
        <?php 
       
        endforeach;
       
        ?>
        
        <tr>
            <td width="50" colspan="4" style="text-align: right;font-weight: bold;vertical-align: middle;" class="columnHeader"  scope="col">
                Grand Total&nbsp;
            </td>
            <?php
              //echo '<pre>';
              //print_r($fee_category_grand_total_array); die;
              foreach ($month_grand_total_array as $m_t_row):
            ?>
                 <td width="50" style="text-align: center;font-weight: bold;" class="columnHeader"  scope="col">
                    <?php echo $m_t_row['monthly_fee']; ?><br>
                    <?php echo $m_t_row['paid_monthly_fee']; ?><br>
                    <?php echo $m_t_row['due_monthly_fee']; ?>
                </td>
             <?php
              endforeach;
              
              foreach ($fee_category_grand_total_array as $c_t_row):
            ?>
            
                <td width="50" style="text-align: center;font-weight: bold;" class="columnHeader"  scope="col">
                        <?php echo $c_t_row['amount']; ?><br>
                        <?php echo $c_t_row['paid_fee']; ?><br>
                        <?php echo $c_t_row['due_fee']; ?>
                </td>
             <?php
              endforeach;
              ?>
              
              <td width="50" style="text-align: center;font-weight: bold;" class="columnHeader"  scope="col"> 
                        <?php echo $grand_total_absent_fine; ?><br>
                        <?php echo $grand_total_absent_fine_paid; ?><br>
                        <?php echo $grand_total_absent_fine_due; ?>
              </td>
              
              <td width="50" style="text-align: center;font-weight: bold;" class="columnHeader"  scope="col">   
                        <?php echo $grand_total_play_truant_fine; ?><br>
                        <?php echo $grand_total_play_truant_fine_paid; ?><br>
                        <?php echo $grand_total_play_truant_fine_due; ?>
              </td>
              
              <td width="50" style="text-align: center;font-weight: bold;" class="columnHeader"  scope="col">
                  <?php echo $grand_waiver_amount_from_total; ?>
              </td>
              
               <td width="50" style="text-align: center;font-weight: bold;" class="columnHeader"  scope="col"> 
                        <?php echo $grand_total_amount; ?><br>
                        <?php echo $grand_total_amount_paid; ?><br>
                        <?php echo $grand_total_amount_due; ?>
              </td>
            
        </tr>
        
        </tbody>


    </table>
</div>