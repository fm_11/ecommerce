<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }


</script>

<form name="addForm" class="cmxform" id="commentForm"   id="addForm" action="<?php echo base_url(); ?>report/getStudentAttendanceDetailReport"
      method="post">

    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tbody>
        <tr>
            <td>
                <label><?php echo $this->lang->line('month'); ?></label>
                <select class="smallInput" style="width:150px;"  name="month" id="month" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php
                    $i = 1;
                    while ($i <= 12) {
                        $dateObj = DateTime::createFromFormat('!m', $i);
                        ?>
                        <option value="<?php echo $i; ?>"><?php echo $dateObj->format('F'); ?></option>
                        <?php
                        $i++;
                    }
                    ?>
                </select>
            </td>

            <td>
                <label><?php echo $this->lang->line('Class'); ?></label>
                <select name="class_id"
                        style="width:150px;"  class="smallInput" id="class_id" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php foreach ($class as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>
            </td>

            &nbsp;
            <td>
                <label><?php echo $this->lang->line('section'); ?></label>
                <select name="section_id"
                        style="width:150px;"  class="smallInput" id="section_id" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php foreach ($section as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>
            </td>
            &nbsp;
            <td>
                <label><?php echo $this->lang->line('group'); ?></label>
                <select name="group" id="group" style="width:150px;" class="smallInput" required="1">
                    <option value="">-- Select --</option>
                    <?php foreach ($group as $row) { ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php } ?>
                </select>
            </td>

            <td>
                <label><?php echo $this->lang->line('year'); ?></label>
                <select class="smallInput" style="width:150px;" name="year" required="1">
                    <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                    <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
                    <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                </select>
            </td>
        </tr>

        <tr>
            <td colspan="5" align="right">
                <input type="button" onclick="printDiv('printableArea')" value="Print Result"/>
                <input type="submit" name="pdf_download" value="PDF Download"/>
                <input type="submit" class="submit" value="View Report">
            </td>
        </tr>
        </tbody>
    </table>


</form>
<br>


<div id="printableArea">
    <?php
    if (isset($adata)) {
        echo $report;
    }
    ?>
</div>
