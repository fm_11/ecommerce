<style>
    .center-justified {
    margin: 0 auto;
    text-align: justify;
    width: 80%;
    }
</style>
<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td style="text-align:center;">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:15px;"><?php echo $HeaderInfo['address']; ?></b><br>
            <b style="font-size:13px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <?php
                    echo $title;
                ?> <br>
            </b>
            <br>
        </td>
    </tr>


    <tbody>
            <?php
               foreach ($groups as $row):
                    if(isset($result_data[$row['id']])){
            ?>


                            <tr>
                                <td style=" text-align: left; font-size:13px;">
                                            <?php echo $row['name']; ?><br>
                                            <p style="padding-left:15px;">
                                                <?php
                                                    foreach ($result_data[$row['id']]['pass'] as $pass_row):
                                                ?>
                                                           <u># List of students passed in section <?php echo $pass_row['section_name'];?> #</u><br>
                                                           <?php echo $pass_row['result'] . " = " . count(explode(",",$pass_row['result'])) ."<br>"; ?>
                                               <?php endforeach; ?>
                                                <?php
                                                    foreach ($result_data[$row['id']]['fail'] as $fail_row):
                                                ?>
                                                           <u># List of students failed in section <?php echo $fail_row['section_name']; ?> #</u><br>
                                                           <?php echo $fail_row['result'] . " = " . count(explode(",",$fail_row['result'])) ."<br>"; ?>
                                               <?php endforeach; ?>
                                            </p>

                                 </td>
                            </tr>

           <?php
            }
           ?>
            <?php endforeach; ?>
    </tbody>
</table>
