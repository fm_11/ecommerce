<!DOCTYPE html>
<html>
<head>
<title><?php echo $title; ?></title>
</head>
<body>
<table width="100%" border="1" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <td class="center_td" colspan="<?php echo count($subject_list) + 3; ?>" style="text-align:center">
            <b style="font-size:16px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
            <b style="font-size:14px;"><?php echo $this->lang->line('eiin'); ?>: <?php echo $HeaderInfo['eiin_number']; ?><br>
                <span style="font-size: 16px;"><?php echo $title; ?></span><br>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td  align="center" colspan="<?php echo count($subject_list) + 3; ?>"
            style="height: 25px; font-size: 14px;">
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
            <?php echo $this->lang->line('section'); ?>: <b><?php echo $section_name; ?></b>,
          <?php echo $this->lang->line('group'); ?>: <b><?php echo $group_name; ?></b>,
            <?php echo $this->lang->line('Shift'); ?>: <b><?php echo $shift_name; ?></b>,
            <?php echo $this->lang->line('exam'); ?>: <b><?php echo $exam_name . ', ' . $exam_year; ?></b>
        </td>
    </tr>

    <tr>
        <td width="20" style="font-size: 13px;" rowspan="2" scope="col" align="center">&nbsp;<b><?php echo $this->lang->line('sl'); ?></b></td>
        <td width="150" style="font-size: 13px;"  rowspan="2" scope="col" align="center">&nbsp;<b><?php echo $this->lang->line('roll'); ?>
           <br><?php echo $this->lang->line('name'); ?><br><?php echo $this->lang->line('student_code'); ?></b></td>


        <?php
        foreach ($subject_list as $subject_row):
            ?>
            <td width="100" style="font-size: 13px;" scope="col" align="center">
               <b> &nbsp;<?php echo $subject_row['name'] ?></b>
            </td>
            <?php
        endforeach;
        ?>
        <td width="70" rowspan="2" style="font-size: 13px;" scope="col" align="center">
                 <b>
                     <?php echo $this->lang->line('position'); ?><br>
                     <?php echo $this->lang->line('grade'); ?><br>
                     <?php echo $this->lang->line('cgpa'); ?> (<?php echo $this->lang->line('with_optional'); ?>)<br>
                    <?php echo $this->lang->line('cgpa'); ?> (<?php echo $this->lang->line('without_optional'); ?>)<br>
                     <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('mark'); ?><br>
                     <?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('mark'); ?> (Without 4th)
                </b>
        </td>
    </tr>
    <tr>
                <?php
                  foreach ($subject_list as $subject_row):
                ?>
                <td width="20" style="font-size: 13px;" scope="col" align="center">
                    <b>
                    &nbsp;<?php echo $this->lang->line('objective'); ?><br>
                    &nbsp;<?php echo $this->lang->line('creative'); ?><br>
                    &nbsp;<?php echo $this->lang->line('class_test'); ?><br>
                    &nbsp;<?php echo $this->lang->line('practical'); ?><br>
                    &nbsp;<?php echo $this->lang->line('total'); ?> <?php echo $this->lang->line('marks'); ?><br>
                    &nbsp;<?php echo $this->lang->line('grade'); ?>
                    </b>
                </td>
                 <?php
                  endforeach;
                 ?>
    </tr>
    </thead>


    <tbody>
    <?php
    $i = 1;
    foreach ($result_data as $row):
      //echo '<pre>';
    //  print_r($row);
    //  die;
        ?>
        <tr>
            <td width="20" style="font-size: 13px;" scope="col" align="center">
                &nbsp;<?php echo $i; ?>
            </td>
            <td width="100" style="font-size: 13px;" scope="col" align="center">
                <b>&nbsp;<?php echo $row['roll_no']; ?><br><?php echo $row['student_name']; ?><br>&nbsp;<?php echo $row['student_code']; ?></b>
            </td>


            <?php
              $optional_sub_got_mark = 0;
            foreach ($subject_list as $subject_row):
                ?>
                <?php
                if (isset($row['result'][$subject_row['code']])) {
                    ?>
                    <td width="20" style="font-size: 13px;" scope="col" align="center">
                        &nbsp;<?php echo $row['result'][$subject_row['code']]['objective']; ?><br>
                        &nbsp;<?php echo $row['result'][$subject_row['code']]['written']; ?><br>
                        &nbsp;<?php echo $row['result'][$subject_row['code']]['class_test']; ?><br>
                        &nbsp;<?php echo $row['result'][$subject_row['code']]['practical']; ?><br>
                        &nbsp;<?php
                        echo $row['result'][$subject_row['code']]['total_obtain'];


                    if ($row['result'][$subject_row['code']]['is_optional'] == '1') {
                        $optional_sub_got_mark = $row['result'][$subject_row['code']]['total_obtain'];
                    } ?><br>
                        &nbsp;<?php echo $row['result'][$subject_row['code']]['alpha_gpa']; ?>
                    </td>
                    <?php
                } else {
                    ?>
                    <td width="100" style="font-size: 13px;" scope="col" align="center">
                      &nbsp;<?php echo $this->lang->line('na'); ?>
                    </td>
                    <?php
                }
                ?>
                <?php
            endforeach;
            ?>
            <td width="60" style="font-size: 13px;" scope="col" align="center">
                <b>
                    &nbsp;<?php echo $row['position']; ?><br>
                    &nbsp;<?php echo $row['c_alpha_gpa_with_optional']; ?><br>
                    &nbsp;<?php echo $row['gpa_with_optional']; ?><br>
                    &nbsp;<?php echo $row['gpa_without_optional']; ?><br>
                    &nbsp;<?php echo $row['total_obtain_mark']; ?><br>
                    &nbsp;<?php
                    if ($optional_sub_got_mark <= 40) {
                        echo $row['total_obtain_mark'] - $optional_sub_got_mark;
                    } else {
                        echo $row['total_obtain_mark'] - 40;
                    }
                    ?>
                </b>
            </td>
        </tr>
        <?php
        $i++; endforeach;
    ?>
    </tbody>
</table>

</body>
</html>
