<?php
 if (isset($is_pdf) && $is_pdf == 1) {
     ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<style>

body {
    font-family: 'SolaimanLipi', Arial, sans-serif !important;
}
table, td, th {
  border: 1px solid black;
}
table {
  border-collapse: collapse;
}
</style>

<?php
 } ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <td colspan="6" style="text-align:center;">
            <?php echo $HeaderInfo['school_name']; ?><br>
            <?php if ($HeaderInfo['eiin_number'] != '') {
     echo "EIIN: " . $HeaderInfo['eiin_number'] . "<br>";
 } ?>
            <?php echo $this->lang->line('income'); ?> <?php echo $this->lang->line('report'); ?> (<?php echo date("d-m-Y", strtotime($from_date)). ' to '. date("d-m-Y", strtotime($to_date)); ?>)
        </td>
    </tr>

    <tr>
        <td scope="col"><?php echo $this->lang->line('sl'); ?></td>
		<td scope="col"><?php echo $this->lang->line('date'); ?></td>
        <td scope="col"><?php echo $this->lang->line('category') . ' ' . $this->lang->line('name'); ?></td>
        <td scope="col"><?php echo $this->lang->line('deposit_method'); ?></td>
        <td scope="col"><?php echo $this->lang->line('cost_center'); ?></td>
        <td scope="col"><?php echo $this->lang->line('amount'); ?></td>
    </tr>

    </thead>
    <tbody>
    <?php
    $i = 0;
    $total_amount = 0;
    foreach ($idata as $row):
        $i++;
        ?>
        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td>
                <?php echo $row['date']; ?>
            </td>
			<td>
				<?php echo $row['category_name']; ?>
			</td>
            <td><?php echo $row['deposit_method_name']; ?></td>
            <td><?php echo $row['cost_center_name']; ?></td>
            <td align="right">
              <?php
              echo $row['amount'];
              $total_amount+= $row['amount'];
              ?>
            </td>
        </tr>
    <?php endforeach; ?>


    <tr>
        <td align="right" colspan="5"><b><?php echo $this->lang->line('total'); ?></b></td>
        <td align="right"><b><?php echo number_format($total_amount, 2); ?> </b></td>
    </tr>

      <tr>
           <td class="textleft" colspan="6"><b><?php echo $this->lang->line('in_words'); ?>: <?php echo $this->numbertowords->convert_number($total_amount); ?> Taka Only.</b></td>
       </tr>
    </tbody>
</table>
</div>
