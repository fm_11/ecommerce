<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
		}
	</style>

	<?php
}
?>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
		<thead>
		<tr>
			<td  class="h_td" style="text-align:center; padding: 5px;" colspan="6" scope="col">
				<b style="font-size: 20px;"><?php echo $HeaderInfo[0]['school_name']; ?></b><br>
				<?php echo $HeaderInfo[0]['address']; ?><br>
				<?php echo $title . ' of ' . date('F', mktime(0, 0, 0, $month, 10)) . ', ' . $year; ?><br>
			</td>
		</tr>

		<tr>
			<td style="padding: 5px;" colspan="6" scope="col" class="h_td">
				<b style="font-size: 13px;">
					Name: <?php echo $time_keeping_info[0]['name']; ?>,
					Teacher ID: <?php echo $time_keeping_info[0]['teacher_code']; ?>,
					Designation: <?php echo $time_keeping_info[0]['post_name']; ?>
				</b>
			</td>
		</tr>

		<tr>
			<th scope="col" style="text-align: center;" scope="col"><?php echo $this->lang->line('date'); ?></th>
			<th scope="col" style="text-align: center;" scope="col">Status</th>
			<th scope="col" style="text-align: center;" class="h_td">&nbsp;Login Time</th>
			<th scope="col" style="text-align: center;" class="h_td">&nbsp;Logout Time</th>
			<th scope="col" style="text-align: center;" class="h_td">&nbsp;Leave Type</th>
			<th scope="col" style="text-align: center;" class="h_td">&nbsp;Holiday Type</th>
		</tr>

		</thead>

		<tbody>
		<?php
		$i = 0;
		foreach ($time_keeping_info[0]['attendance_data'] as $row):
			$i++;
			?>
			<tr>
				<td align="center">
					<?php echo $row['date']; ?>
				</td>
				<td align="center">
					<?php echo $row['status']; ?>
				</td>
				<td align="center">
					&nbsp;<?php
					if($row['status'] == 'Present'){
						echo date('h:i:s A', strtotime($row['login_time']));
					}else{
						echo '-';
					}
					?>
				</td>
				<td align="center">
					<?php
					if($row['status'] == 'Present'){
						echo date('h:i:s A', strtotime($row['logout_time']));
					}else{
						echo '-';
					}
					?>
				</td>
				<td align="center">
					<?php
					if($row['status'] == 'Holiday'){
						echo $row['holiday_type'];
					}else{
						echo '-';
					}
					?>
				</td>
				<td align="center">
					<?php
					if($row['status'] == 'Leave'){
						echo $row['leave_type'];
					}else{
						echo '-';
					}
					?>
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
