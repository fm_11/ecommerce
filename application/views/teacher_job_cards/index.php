<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>teacher_job_cards/index#attendance"
	  method="post">
	<div class="form-row">
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('year'); ?></label>
			<select class="js-example-basic-single w-100" name="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option value="<?php echo $list['value']; ?>" <?php if (isset($year)) {
							if ($year == $list['value']) {
								echo 'selected';
							}
						} ?>><?php echo $list['text']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Month</label>
			<select class="js-example-basic-multiple w-100" name="month" id="month" required="1">
				<?php
				$i = 1;
				while ($i <= 12) {
					$dateObj = DateTime::createFromFormat('!m', $i); ?>
					<option value="<?php echo $i; ?>" <?php if(isset($month)){if ($i == $month) {
						echo 'selected';
					}}  ?>><?php echo $dateObj->format('F'); ?></option>
					<?php
					$i++;
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('teacher') . '/' . $this->lang->line('staff'); ?></label>
			<select class="js-example-basic-single w-100" name="teacher_id" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($teachers)) {
					foreach ($teachers as $list) {
						$i++; ?>
						<option value="<?php echo $list['id']; ?>" <?php if (isset($teacher_id)) {
							if ($teacher_id == $list['id']) {
								echo 'selected';
							}
						} ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>

	<div class="btn-group float-right">
		<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
		<input type="submit" class="btn btn-primary" value="View Report">
	</div>

</form>

<div id="printableArea">
	<?php
	if (isset($time_keeping_info)) {
		echo $report;
	}
	?>
</div>
