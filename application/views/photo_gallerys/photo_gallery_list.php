<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;" href="<?php echo base_url(); ?>photo_gallerys/gallery_photo_add"><span>Add New Photo</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
        <tr>
            <th width="50" scope="col">SL</th>
            <th width="200" scope="col">Event Name</th>
            <th width="200" scope="col">Photo Title</th>
            <th width="200" scope="col">Photo</th>
            <th width="100" scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>


        <?php
        $i = 0;
        foreach ($all_photos as $row):
            $i++;
            ?>
            <tr>

            <tr>
                <td width="34" style="vertical-align:middle">
                    <?php echo $i; ?>
                </td>
                <td style="vertical-align:middle"><?php echo $row['event_name']; ?></td>
                <td style="vertical-align:middle"><?php echo $row['title']; ?></td>
                <td style="vertical-align:middle">
                    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/gallery/<?php echo $row['photo_location'] ?>" height="120" width="150">
                </td>
                <td style="vertical-align:middle">
                    <a href="<?php echo base_url(); ?>photo_gallerys/gallery_photo_delete/<?php echo $row['id']; ?>" onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
                </td>
            </tr>
        <?php endforeach; ?>

        <tr class="footer">
            <td colspan="6" align="right">
                <!--  PAGINATION START  -->             
                <div class="pagination">
                    <?php echo $this->pagination->create_links(); ?>
                </div>  
                <!--  PAGINATION END  -->       
            </td>
        </tr>
    </tbody>
</table>