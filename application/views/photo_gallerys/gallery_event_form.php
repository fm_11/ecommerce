<?php
if($action == 'edit'){
?>


<h2>Update Event</h2>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>photo_gallerys/gallery_event_edit" method="post">
    <label>Event Name</label>
    <input type="text" autocomplete="off"  class="smallInput wide" name="txtEventName" value="<?php echo $events_info[0]['event_name']; ?>" required="1"/>


    <label>Remarks</label>
    <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="txtRemarks">
         <?php echo $events_info[0]['remarks']; ?>
    </textarea>

    <br>
    <br>
    <input type="hidden" name="id" value="<?php echo $events_info[0]['id']; ?>">
    <input type="submit" class="submit" value="Update">
    <input type="reset" class="submit" value="Reset">
</form>
<br />

<div class="clear"></div><br />


<?php
}
else{
?>

<h2>Add New Event</h2>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>photo_gallerys/gallery_event_add" method="post">
    <label>Event Name</label>
    <input type="text" autocomplete="off"  class="smallInput wide" name="txtEventName" required="1"/>

    <label>Remarks</label>
    <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="txtRemarks"></textarea>

    <br>
    <br>
    <input type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form>
<br />

<div class="clear"></div><br />

<?php
}
?>
