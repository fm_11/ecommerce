<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>category/add" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Name<span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label>Order No<span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="order_no" required="1"/>
    </div>
    <input type="text" autocomplete="off"  class="form-control" name="display_menu" value="1" hidden/>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Has Hot Deal?<span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"   name="is_hot_deal" value="1" />
    </div>
    <div class="form-group col-md-6">
      <label>Has Main Menu?<span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"   name="is_main_menu" value="1"/>
    </div>

  </div>
    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
