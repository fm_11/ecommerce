<script type="text/javascript">
    function msgStatusUpdate(id,status,columname){
        if (window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function()
        {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
              if(columname=='11')
              {
                //alert(xmlhttp.responseText);
                  document.getElementById("status_sction_" + id).innerHTML = xmlhttp.responseText;
              }else {
                  document.getElementById("status_sction_main_menu" + id).innerHTML = xmlhttp.responseText;
              }

            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>category/updateCaseStatus?id=" + id + '&&columname='+columname+'&&status=' + status, true);
        xmlhttp.send();
    }
</script>

<?php
$name = $this->session->userdata('name');
//echo $student_status; die;
?>
<form class="form-inline" method="post" action="<?php echo base_url(); ?>ad_case_status/index">
<div class="col-md-offset-2 col-md-12">
        <label class="sr-only" for="Name"><?php echo $this->lang->line('name'); ?></label>
        <?php
         $placeholder = $this->lang->line('name');
        ?>
        <input type="text" autocomplete="off"  style="height: 37px; margin-top: -4px;" name="name" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($roll)) {
            echo $name;
        } ?>" class="form-control" id="name">

        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>
</form>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
  <table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">Name</th>
        <th scope="col">Hot Deal</th>
        <th scope="col">Main Menu</th>
        <th scope="col">Order No</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($category as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td id="status_sction_<?php echo $row['id']; ?>">
                <?php
                if ($row['is_hot_deal'] == 1) {
                    ?>
                    <a class="deleteTag" title="Active" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_hot_deal']; ?>,11)"><i class="ti-check-box"></i></a>
                <?php
                } else {
                    ?>
                    <a class="deleteTag" title="Inactive" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_hot_deal']; ?>,11)"><i class="ti-na"></i></a>
                <?php
                }
                ?>
            </td>
            <td id="status_sction_main_menu<?php echo $row['id']; ?>">
                <?php
                if ($row['is_main_menu'] == 1) {
                    ?>
                    <a class="deleteTag" title="Active" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_main_menu']; ?>,01)"><i class="ti-check-box"></i></a>
                <?php
                } else {
                    ?>
                    <a class="deleteTag" title="Inactive" href="#"
                       onclick="msgStatusUpdate(<?php echo $row['id']; ?>,<?php echo $row['is_main_menu']; ?>,01)"><i class="ti-na"></i></a>
                <?php
                }
                ?>
            </td>
              <td><?php echo $row['order_no']; ?></td>
            <td>
                 <div class="dropdown">
                     <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         <i class="ti-pencil-alt"></i>
                     </button>
                     <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                         <a class="dropdown-item" href="<?php echo base_url(); ?>category/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                         <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>category/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                     </div>
                 </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
