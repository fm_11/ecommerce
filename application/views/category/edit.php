
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>category/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Name<span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $category[0]['name']; ?>" required="1"/>
    </div>
    <div class="form-group col-md-6">
      <label>Order No<span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="order_no" value="<?php echo $category[0]['order_no']; ?>" required="1"/>
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Has Hot Deal?<span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"   name="is_hot_deal" value="1" <?php if($category[0]['is_hot_deal']){ echo "checked";} ?>/>
    </div>
    <div class="form-group col-md-6">
      <label>Has Main Menu?<span class="required_label">*</span></label>
      <input type="checkbox" autocomplete="off"   name="is_main_menu" value="1" <?php if($category[0]['is_main_menu']){ echo "checked";} ?>/>
    </div>

  </div>
    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $category[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
