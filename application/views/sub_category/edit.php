
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>sub_category/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('category').' '.$this->lang->line('name') ?> <span class="required_label">*</span></label>
      <select class="js-example-basic-single w-100" name="category_id" required="1">
        <option value="">-- Please Select --</option>
        <?php foreach ($category as $row) { ?>
          <option value="<?php echo $row['id']; ?>" <?php if (isset($sub_category[0]['category_id'])) {
            if ($row['id'] == $sub_category[0]['category_id']) {
              echo 'selected';
            }
          } ?>><?php echo $row['name']; ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('sub').' '.$this->lang->line('category') ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $sub_category[0]['name']; ?>" required="1"/>
    </div>

  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('order_no'); ?><span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="order_no" value="<?php echo $sub_category[0]['order_no']; ?>" required="1"/>
    </div>

  </div>
    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $sub_category[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
