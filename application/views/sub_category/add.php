<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>sub_category/add" method="post">
  <div class="form-row">

    <div class="form-group col-md-6">
		  <label><?php echo $this->lang->line('category').' '.$this->lang->line('name') ?> <span class="required_label">*</span></label>
		  <select class="js-example-basic-single w-100" name="category_id" required="1">
			  <option value="">-- Please Select --</option>
			  <?php
			  if (count($category)) {
				  foreach ($category as $list) {
					  ?>
					  <option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
					  <?php
				  }
			  } ?>
		  </select>
	  </div>
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('sub').' '.$this->lang->line('category') ?><span class="required_label">*</span></label>
      <input type="text" autocomplete="off"  class="form-control" name="name" required="1"/>
    </div>

    <input type="text" autocomplete="off"  class="form-control" name="display_sub_menu" value="1" hidden/>
  </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label><?php echo $this->lang->line('order_no'); ?><span class="required_label">*</span></label>
      <input type="number" autocomplete="off"  class="form-control" name="order_no" required="1"/>
    </div>

  </div>
    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
