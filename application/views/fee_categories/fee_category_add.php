
<?php
if ($action == 'edit') {
    ?>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>fee_categories/edit" method="post">
      <div class="form-row">
              <div class="form-group col-md-6">
                <label><?php echo $this->lang->line('name'); ?></label>
                <input type="text" autocomplete="off"  required name="name" class="form-control" value="<?php echo $fee_category_info[0]['name']; ?>">
              </div>
              <div class="form-group col-md-6">
                <label><?php echo $this->lang->line('receipt_no'); ?> Prefix</label>
                <input type="text" autocomplete="off"  name="receipt_no_prefix" class="form-control" value="<?php echo $fee_category_info[0]['receipt_no_prefix']; ?>">
              </div>
      </div>

      <div class="form-row">
            <div class="form-group col-md-12">
              <label><?php echo $this->lang->line('remarks'); ?></label>
              <textarea id="wysiwyg" class="form-control" name="remarks"><?php echo $fee_category_info[0]['remarks']; ?></textarea>
            </div>
    </div>

    <input type="hidden" name="id" value="<?php echo $fee_category_info[0]['id']; ?>">
    <div class="float-right">
       <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
    </div>
</form>
<?php
} else {
        ?>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>fee_categories/add" method="post">
      <div class="form-row">
          <div class="form-group col-md-6">
            <label><?php echo $this->lang->line('name'); ?></label>
            <input type="text" autocomplete="off"  required name="name" class="form-control" value="">
          </div>
          <div class="form-group col-md-6">
            <label><?php echo $this->lang->line('receipt_no'); ?> Prefix</label>
            <input type="text" autocomplete="off"  name="receipt_no_prefix" class="form-control" value="">
          </div>
       </div>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label><?php echo $this->lang->line('remarks'); ?></label>
            <textarea id="wysiwyg" class="form-control" name="remarks"></textarea>
          </div>
        </div>
        <div class="float-right">
           <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
           <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
        </div>
    </form>
<?php
    }
?>
