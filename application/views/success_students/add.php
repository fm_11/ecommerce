 <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>success_students/add" method="post" enctype="multipart/form-data">
      <div class="form-row">
        <div class="form-group col-md-6">
        <label>Name&nbsp;<span class="required_label">*</span></label>
        <input type="text" autocomplete="off"  class="form-control" name="txtName" required="1"/>
        </div>
        <div class="form-group col-md-6">
		    <label>Year of Passing&nbsp;<span class="required_label">*</span></label>
        <input type="text" autocomplete="off"  class="form-control" name="txtYearPassing" required="1"/>
        </div>
	  </div>
	 <div class="form-row">
        <div class="form-group col-md-6">
		    <label>Organization&nbsp;<span class="required_label">*</span></label>
        <input type="text" autocomplete="off"  class="form-control" name="txtOrg" required="1"/>
        </div>
		 <div class="form-group col-md-6">
			 <label>Position&nbsp;<span class="required_label">*</span></label>
			 <input type="text" autocomplete="off"  class="form-control" required name="txtPosition"/>
		 </div>
	 </div>
	 <div class="form-row">
		 <div class="form-group col-md-6">
			 <label>Educational Qualification</label>
			 <input type="text" autocomplete="off"  class="form-control" name="txtEduQua"/>
		 </div>
        <div class="form-group col-md-6">
		    <label>Mobile&nbsp;<span class="required_label">*</span></label>
        <input type="text" autocomplete="off"  class="form-control" name="txtMobile" required="1"/>
        </div>
	 </div>
	 <div class="form-row">
        <div class="form-group col-md-6">
        <label>Email</label>
        <input type="text" autocomplete="off"  class="form-control" name="txtEmail"/>
        </div>
        <div class="form-group col-md-6">
        <label>Address&nbsp;<span class="required_label">*</span></label>
        <input type="text" autocomplete="off"  class="form-control"  name="txtAddress"  required="1"/>
        </div>
      </div>
	 <div class="float-right">
		<input type="reset" class="btn btn-danger" value="<?php echo $this->lang->line('cancel'); ?>">
        <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line('submit'); ?>">
	 </div>
    </form>
