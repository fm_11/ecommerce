<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table table-striped">
    <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">Name</th>
        <th scope="col">Organization</th>
        <th scope="col">Position</th>
		<th scope="col">Mobile</th>
		<th scope="col">Email</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($success_student as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td>
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
			<td><?php echo $row['org']; ?></td>
			<td><?php echo $row['position']; ?></td>
            <td><?php echo $row['mobile']; ?></td>
            <td><?php echo $row['email']; ?></td>

            <td>
              <div class="dropdown">
                  <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="ti-pencil-alt"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
					  <a class="dropdown-item" href="<?php echo base_url(); ?>success_students/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                      <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>success_students/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                  </div>
              </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
	<?php echo $this->pagination->create_links(); ?>
</div>
</div>
