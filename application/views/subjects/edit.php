
                    <form name="addForm" class="cmxform" id="commentForm"   class="cmxform" id="commentForm" action="<?php echo base_url(); ?>subjects/edit/<?php echo $this->uri->segment(3); ?>"
                          method="post">

                          <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label>Subject</label>
                                  <input type="text" autocomplete="off"  name="name"  class="form-control" required value="<?php echo $subject[0]['name']; ?>">
                                </div>
                                  <div class="form-group col-md-6">
                                    <label>Subject Short Name</label>
                                    <input type="text" autocomplete="off"  name="short_name"  class="form-control" required value="<?php echo $subject[0]['short_name']; ?>">
                                  </div>
                            </div>


                            <div class="form-row">
                                  <div class="form-group col-md-6">
                                    <label>Code</label>
                                    <input type="text" autocomplete="off"  name="code"  class="form-control" required value="<?php echo $subject[0]['code']; ?>">
                                  </div>
                                    <div class="form-group col-md-6">
                                      <label>Relational Subject</label>
                                      <input type="text" autocomplete="off"  name="relational_subject_id" class="form-control" value="<?php echo $subject[0]['relational_subject_id']; ?>">
                                    </div>
                              </div>
                              <div class="float-right">
                                 <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
                              </div>
                    </form>
