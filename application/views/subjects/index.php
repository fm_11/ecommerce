
          <div class="table-responsive-sm">
            <table  class="table">
              <thead>
              <tr>
                  <th scope="col">SL</th>
                  <th scope="col">Name</th>
                  <th scope="col">Short Name</th>
                  <th scope="col">Code</th>
                  <th scope="col">Relational Subject</th>
                  <th scope="col">Actions</th>
              </tr>
              </thead>
              <tbody>
              <?php
              $i = 0;
              foreach ($subject as $row):
                  $i++;
                  ?>
                  <tr>

                  <tr>
                      <td>
                          <?php echo $i; ?>
                      </td>
                      <td><?php echo $row['name']; ?></td>
                      <td><?php echo $row['short_name']; ?></td>
                      <td><?php echo $row['code']; ?></td>
                       <td><?php echo $row['relational_subject_id']; ?></td>
                      <td>
                        <div class="dropdown">
                            <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ti-pencil-alt"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                                <a class="dropdown-item" href="<?php echo base_url(); ?>subjects/edit/<?php echo $row['id']; ?>">Edit</a>
                                <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>subjects/delete/<?php echo $row['id']; ?>">Delete</a>
                            </div>
                        </div>
                      </td>
                  </tr>
                  <?php endforeach; ?>
                 </tbody>
                </table>
              </div>
