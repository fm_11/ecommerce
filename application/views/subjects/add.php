
                      <form name="addForm" id="commentForm"   class="cmxform" action="<?php echo base_url(); ?>subjects/add" method="post">

                        <div class="form-row">
                              <div class="form-group col-md-6">
                                <label>Subject Name</label>
                                <input type="text" autocomplete="off"  name="name" class="form-control" required value="">
                              </div>
                                <div class="form-group col-md-6">
                                  <label>Subject Short Name</label>
                                  <input type="text" autocomplete="off"  name="short_name" class="form-control" required value="">
                                </div>
                          </div>

                          <div class="form-row">
                                <div class="form-group col-md-6">
                                  <label>Code</label>
                                  <input type="text" autocomplete="off"  name="code" required class="form-control" value="">
                                </div>
                                  <div class="form-group col-md-6">
                                    <label>Relational Subject</label>
                                    <input type="text" autocomplete="off"  name="relational_subject_id" class="form-control" value="">
                                  </div>
                            </div>


                            <div class="float-right">
                               <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
                               <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
                            </div>
                      </form>
