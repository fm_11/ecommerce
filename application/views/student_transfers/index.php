<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>


<h2>
<a class="button_grey_round" style="margin-bottom: 5px;"
   href="<?php echo base_url(); ?>student_transfers/add"><span>Add New</span></a>              
</h2>


<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="20" scope="col">SL</th>
		<th width="50" scope="col">Student ID</th>
        <th width="80" scope="col">Name</th>
        <th width="30" scope="col">Old Class</th>
		<th width="30" scope="col">New Class</th>
        <th width="20" scope="col">Old Section</th>
		<th width="20" scope="col">New Section</th>
        <th width="30" scope="col">Old Group</th>
		<th width="30" scope="col">New Group</th>
        <th width="30" scope="col">Old Shift</th>
		<th width="30" scope="col">New Shift</th>      
        <th width="40" scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($transfers as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['student_code']; ?></td>		
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['old_class_name']; ?></td>
            <td><?php echo $row['new_class_name']; ?></td>
            <td><?php echo $row['old_section_name']; ?></td>
            <td><?php echo $row['new_section_name']; ?></td>
            <td><?php echo $row['old_group_name']; ?></td>
			<td><?php echo $row['new_group_name']; ?></td>
			<td><?php echo $row['old_shift_name']; ?></td>
			<td><?php echo $row['new_shift_name']; ?></td>
			
            <td style="vertical-align:middle">                           
                <a href="<?php echo base_url(); ?>student_transfers/delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>               
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>

    <tr class="footer">
        <td colspan="12" style="color: #000000;" align="right">
            <!--  PAGINATION START  -->
            <div class="pagination">
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <!--  PAGINATION END  -->
        </td>
    </tr>
</table>

