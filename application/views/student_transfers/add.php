<script type="text/javascript">
    function getStudentForDropdown() {
        
        var class_id = document.getElementById('old_class_id').value;
        var section_id = document.getElementById('old_section_id').value;
        var group_id = document.getElementById('old_group_id').value;
        var shift_id = document.getElementById('old_shift_id').value;
        //alert(group_id);
       
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //alert(xmlhttp.responseText);
                document.getElementById("student_id").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "<?php echo base_url(); ?>students/getStudentForDropdown?class_id=" + class_id + '&&section_id=' + section_id + '&&group_id=' + group_id + '&&shift_id=' + shift_id, true);
        xmlhttp.send();
    }

</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>student_transfers/add" method="post">
    
    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
        <tr>
            <td width="50%">
                        <label>Old Class</label>
                        <select class="smallInput" name="old_class_id" id="old_class_id"
                                onchange="getStudentForDropdown()" required="1">
                            <option value="">-- Please Select --</option>
                            <?php
                            $i = 0;
                            if (count($class_list)) {
                                foreach ($class_list as $list) {
                                    $i++;
                                    ?>
                                    <option
                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    
                    
                        <label>Old Section</label>
                        <select class="smallInput" name="old_section_id" id="old_section_id"
                                onchange="getStudentForDropdown()" required="1">
                            <option value="">-- Please Select --</option>
                            <?php
                            $i = 0;
                            if (count($section_list)) {
                                foreach ($section_list as $list) {
                                    $i++;
                                    ?>
                                    <option
                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        
                        <label>Old Group</label>
                        <select class="smallInput" name="old_group_id" id="old_group_id"
                                onchange="getStudentForDropdown()" required="1">
                            <option value="">-- Please Select --</option>
                            <?php
                            $i = 0;
                            if (count($group_list)) {
                                foreach ($group_list as $list) {
                                    $i++;
                                    ?>
                                    <option
                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        
                        
                        <label>Old Shift</label>
                        <select class="smallInput" name="old_shift_id" id="old_shift_id"
                                onchange="getStudentForDropdown()" required="1">
                            <option value="">-- Please Select --</option>
                            <?php
                            $i = 0;
                            if (count($shift_list)) {
                                foreach ($shift_list as $list) {
                                    $i++;
                                    ?>
                                    <option
                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
            </td>
            <td width="50%">
                  <label>New Class</label>
                        <select class="smallInput" name="new_class_id" id="new_class_id" required="1">
                            <option value="">-- Please Select --</option>
                            <?php
                            $i = 0;
                            if (count($class_list)) {
                                foreach ($class_list as $list) {
                                    $i++;
                                    ?>
                                    <option
                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    
                    
                        <label>New Section</label>
                        <select class="smallInput" name="new_section_id" id="new_section_id" required="1">
                            <option value="">-- Please Select --</option>
                            <?php
                            $i = 0;
                            if (count($section_list)) {
                                foreach ($section_list as $list) {
                                    $i++;
                                    ?>
                                    <option
                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        
                        <label>New Group</label>
                        <select class="smallInput" name="new_group_id" id="new_group_id" required="1">
                            <option value="">-- Please Select --</option>
                            <?php
                            $i = 0;
                            if (count($group_list)) {
                                foreach ($group_list as $list) {
                                    $i++;
                                    ?>
                                    <option
                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        
                        
                        <label>New Shift</label>
                        <select class="smallInput" name="new_shift_id" id="new_shift_id" required="1">
                            <option value="">-- Please Select --</option>
                            <?php
                            $i = 0;
                            if (count($shift_list)) {
                                foreach ($shift_list as $list) {
                                    $i++;
                                    ?>
                                    <option
                                        value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                    <?php
                                }
                            }
                            ?>
                       </select>
            </td>
        </tr>
    </table>
    
    <label>Student</label>
    <select class="smallInput" style="width: 350px;" name="student_id" id="student_id" required="1">
        <option value="">-- Please Select --</option>
    </select>
    
    <label>Update all payment transaction ?</label>
    <input type="radio" value="1" name="update_payment_transac" />Yes
    <input type="radio" value="0" checked name="update_payment_transac"/>No
    
    <label>Reason</label>
    <textarea id="wysiwyg" required class="smallInput wide" rows="7" cols="30"
              name="reason"></textarea>
    

    <br><br>
    <input type="submit" class="submit" value="Transfer">

</form>

<link href="<?php echo base_url() . MEDIA_FOLDER; ?>/js/select2.min.css" rel="stylesheet"/>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/select2.min.js"></script>

<script type="text/javascript">
    $("#student_id").select2();
</script>

