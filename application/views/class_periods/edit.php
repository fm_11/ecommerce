
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>class_periods/edit" method="post">

  <div class="form-row">
    <div class="form-group col-md-6">
      <label>Leave Type</label>
      <input type="text" autocomplete="off"  class="form-control" name="name" value="<?php echo $periods[0]['name']; ?>" required="1"/>
    </div>
  </div>

    <div class="float-right">
       <input type="hidden" name="id" value="<?php echo $periods[0]['id']; ?>">
       <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
    </div>
</form>
