<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>suppliers/edit/<?php echo $row_data->id; ?>" enctype="multipart/form-data" class="cmxform" method="post">
  <fieldset>
     <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('suppliers').' '.$this->lang->line('name'); ?><span class="required_label" style="color:red">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="name"  name="name" required value="<?php echo $row_data->name; ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('mobile');?><span class="required_label" style="color:red">*</span></label>
          <input type="number" autocomplete="off" required class="form-control" id="mobile"  name="mobile" value="<?php echo $row_data->mobile; ?>">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('address'); ?><span class="required_label" style="color:red">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="address"  name="address" required value="<?php echo $row_data->address; ?>">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('note'); ?></label>
          <textarea class="form-control" id="note" autocomplete="off" name="note" value=""><?php echo $row_data->note; ?></textarea>
        </div>
      </div>
  </fieldset>
	<div class="float-right">
		<input type="hidden" autocomplete="off"  class="form-control" id="id" required name="id" value="<?php echo $row_data->id; ?>">
		<input class="btn btn-primary" type="submit" value="Update">
	</div>
</form>
