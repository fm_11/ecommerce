<?php
$name = $this->session->userdata('name');
?>
<form class="form-inline" method="post" action="<?php echo base_url(); ?>suppliers/index">
<div class="col-md-offset-2 col-md-12">
        <label class="sr-only" for="Name"><?php echo $this->lang->line('name'); ?></label>
        <?php
         $placeholder = $this->lang->line('suppliers').' '.$this->lang->line('name');
        ?>
        <input type="text" autocomplete="off"  style="height: 37px; margin-top: -4px;" name="name" placeholder="<?php echo $placeholder; ?>" value="<?php if (isset($name)) {
            echo $name;
        } ?>" class="form-control" id="name">

        <button type="submit" style="margin-top: -5px;" class="btn btn-primary"><?php echo $this->lang->line('search'); ?></button>
</div>
</form>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th><?php echo $this->lang->line('sl'); ?></th>
        <th><?php echo $this->lang->line('name'); ?></th>
		<th><?php echo $this->lang->line('mobile'); ?></th>
        <th><?php echo $this->lang->line('address'); ?></th>
        <th><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = (int)$this->uri->segment(3);
    foreach ($suppliers as $row):
        $i++;
        ?>
        <tr>
            <td>
                <?php echo $i; ?>
            </td>
              <td><?php echo $row['name']?></td>
              <td><?php echo $row['mobile']; ?></td>
              <td><?php echo $row['address']; ?></td>
              <td>
                <div class="dropdown">
                    <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="ti-pencil-alt"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                        <a class="dropdown-item" href="<?php echo base_url(); ?>suppliers/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                        <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>suppliers/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                    </div>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div class="float-right">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
