<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>suppliers/add" class="cmxform" enctype="multipart/form-data" method="post">
  <fieldset>
     <div class="form-row">
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('suppliers').' '.$this->lang->line('name'); ?><span class="required_label" style="color:red">*</span></label>
          <input type="text" autocomplete="off"  class="form-control" id="name"  name="name" required value="">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('mobile').' '.$this->lang->line('number'); ?><span class="required_label" style="color:red">*</span></label>
          <input type="number" autocomplete="off" required class="form-control" id="mobile"  name="mobile" value="">
        </div>
        <div class="form-group col-md-4">
          <label for="inputEmail4"><?php echo $this->lang->line('address')?><span class="required_label" style="color:red">*</span></label>
          <input type="text" autocomplete="off" class="form-control" id="address" name="address" required value="">
        </div>
      </div>
      <div class="form-row">
        <div class="form-group col-md-12">
          <label for="inputEmail4"><?php echo $this->lang->line('note'); ?></label>
          <textarea class="form-control" id="note" autocomplete="off" name="note" value=""></textarea>
        </div>
      </div>
  </fieldset>
  <div class="float-right">
    <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>
</form>
