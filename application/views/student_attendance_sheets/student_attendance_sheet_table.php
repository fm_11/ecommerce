<style>
	span.b {
		display: inline-block;
		width: 18%;
		height: auto;
		padding: 4px;
		border: 1px solid #000000;
	}

	.bac_color{
		background-color: #e0e0eb;
	}

	@media print {
		.bac_color{
			background-color: #e0e0eb;
		}
	}
</style>
<table width="100%" cellpadding="0" class="data_table" cellspacing="0" id="box-table-a" summary="StudentAttendanceReport">
    <thead>
	 <tr>
		 <th style="border: none;" colspan="<?php echo $number_of_days + 3;?>">
			 <span class="b">Class: <?php echo $class_name; ?></span>
			 <span class="b">Shift: <?php echo $shift_name; ?></span>
			 <span class="b">Section: <?php echo $section_name; ?></span>
			 <span class="b">Academic Year: <?php echo $year; ?></span>
			 <span class="b">Month: <?php echo $month_name; ?></span>
		 </th>
	 </tr>

    <tr>
        <th scope="col" class="td_center bac_color">Student Id</th>
        <th scope="col" class="td_center bac_color">Roll</th>
        <th scope="col" class="td_center bac_color"><?php echo $this->lang->line('name'); ?></th>
        <?php
        $i = 1;
        while ($i <= $number_of_days) {
            if ($i < 10) {
                $clm_date = '0' . $i;
            } else {
                $clm_date = $i;
            }
            echo '<th scope="col" class="td_center bac_color">' . $clm_date . '</th>';
            $i++;
        }
        ?>
    </tr>

    </thead>
    <tbody>
    <?php
    $i = 0;
    $total_student = 0;
    foreach ($adata as $row):
        $i++;
        ?>
        <tr>
            <td class="td_center">
                <?php echo $row['student_code']; ?>
            </td>
            <td class="td_center">
                <?php echo $row['roll_no']; ?>
            </td>
            <td style="min-width: 200px;" class="td_left">
                <?php echo $row['name']; ?>
            </td>
           <?php echo $row['data']; ?>
        </tr>
    <?php endforeach; ?>
    <tr>
      <td  class="td_right" colspan="<?php echo $number_of_days+3;?>">
        <b>Total Student: <?php echo $i;?></b>
      </td>
    </tr>
    </tbody>
</table>
