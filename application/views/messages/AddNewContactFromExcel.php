
<table width="40%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th colspan="2" width="50" scope="col"><b><?php echo $this->lang->line('file') . ' ' . $this->lang->line('format') . ' ' . $this->lang->line('download'); ?></b></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <b><?php echo $this->lang->line('phone_book') . ' ' . $this->lang->line('number'); ?> Number</b>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>uploads/PhonoBookNumber.xls"><?php echo $this->lang->line('download'); ?></a>
        </td>
    </tr>
    </tbody>
</table>

<hr>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>messages/AddNewContactFromExcel" method="post"
      enctype="multipart/form-data">
	<div class="form-row"> 
		<div class="form-group col-md-6">
			<label><?php echo $this->lang->line('category'); ?></label>
			<select class="smallInput" name="category_id" required="1">
							<option value="">-- <?php echo $this->lang->line('select_category'); ?> --</option>
							<?php
							$i = 0;
							if (count($categoryList)) {
								foreach ($categoryList as $list) {
									$i++;
									?>
									<option
											value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
									<?php
								}
							}
							?>
		   </select>
		</div>
		
		<div class="form-group col-md-6">
			<label>File</label>
			<input type="file" name="txtFile" required="1" class="smallInput"> * File Format -> Excel-2003
		</div>
	</div>
    <input type="submit" class="submit" value="Process">
</form><br/>
<div class="clear"></div>
