<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function () {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>messages/ViewSMSTemplateIndex"><span><?php echo $this->lang->line('sms') . ' ' . $this->lang->line('template'); ?></span></a></li>
    <li><a href="<?php echo base_url(); ?>messages/ViewCategoryIndex"><span><?php echo $this->lang->line('category'); ?></span></a></li>
    <li><a href="<?php echo base_url(); ?>messages/PhoneBookIndex"><span><?php echo $this->lang->line('phone_book'); ?></span></a></li>
    <li><a href="<?php echo base_url(); ?>messages/audio_message_upload"><span><?php echo $this->lang->line('audio') . ' ' . $this->lang->line('upload'); ?></span></a></li>
    <li><a href="<?php echo base_url(); ?>messages/all_type_send_sms"><span><?php echo $this->lang->line('sms') . ' ' . $this->lang->line('send'); ?></span></a></li>
    <li><a href="<?php echo base_url(); ?>messages/GetGuardianSMSReport"><span><?php echo $this->lang->line('guardian') . ' ' . $this->lang->line('sms') . ' ' . $this->lang->line('report'); ?></span></a></li>
    <li><a href="<?php echo base_url(); ?>messages/GetDetailsSMSReport"><span><?php echo $this->lang->line('sms') . ' ' . $this->lang->line('details') . ' ' . $this->lang->line('report'); ?></span></a></li>
    <li><a href="<?php echo base_url(); ?>messages/SMSSummaryReport"><span><?php echo $this->lang->line('sms') . ' ' . $this->lang->line('summary') . ' ' . $this->lang->line('report'); ?></span></a></li>
</ul>
