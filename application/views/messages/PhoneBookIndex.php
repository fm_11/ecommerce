<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>messages/AddNewContact"><span><?php echo $this->lang->line('add') . ' ' . $this->lang->line('contact'); ?></span></a>

    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>messages/AddNewContactFromExcel"><span><?php echo $this->lang->line('batch_add') . ' ' . $this->lang->line('contact'); ?></span></a>
</h2>

<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('contact') . ' ' . $this->lang->line('name'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('contact') . ' ' . $this->lang->line('number'); ?></th>
        <th width="200" scope="col"><?php echo $this->lang->line('phone_book') . ' ' . $this->lang->line('category'); ?></th>
        <th width="100" scope="col"><?php echo $this->lang->line('actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($PhoneBookList as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34">
                <?php echo $i; ?>
            </td>
            <td><?php echo $row['contact_name']; ?></td>
            <td><?php echo $row['contact_number']; ?></td>
            <td><?php echo $row['category']; ?></td>
            <td style="vertical-align:middle">
				<a href="<?php echo base_url(); ?>messages/EditPhoneBookData/<?php echo $row['id']; ?>"
                   title="Edit"><?php echo $this->lang->line('edit'); ?></a>&nbsp;
                <a href="<?php echo base_url(); ?>messages/DeletePhoneBookData/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>

        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
