<div style="height: auto; width: 100%">
    <div class="wrap">
        <div class="box box4 shadow4">
            <h3>
                <?php echo $this->lang->line('sms') . ' ' . $this->lang->line('balance'); ?><br>
                <span class="value"><?php echo $sms_balance; ?></span>
            </h3>
        </div>
    </div>
    <div id="graph_container" style="min-width: 310px; height: auto; max-width: 600px; margin: 0 auto"></div>
</div>
<br><br>
<style>
    .wrap {
        margin-left: 15px;
    }

    .value{
        font-size: 20px;
        color: #A70000;
    }

    .box {
        width: 13%;
        height: 100px;
        float: left;
        background-color: white;
        margin: 25px 15px;
        border-radius: 5px;
    }

    .box h3 {
        font-family: 'Didact Gothic', sans-serif;
        font-weight: normal;
        text-align: center;
        padding-top: 25px;
        color: #000033;
        font-weight: bold;
    }

    .box1 {
        background-color: #EBA39E;
    }

    .box2 {
        background-color: #EDE89A;
    }

    .box3 {
        background-color: #9EEBA1;
    }

    .box4 {
        background-color: #9EEBBF;
    }
</style>
