
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>messages/AddNewContact" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('contact') . ' ' . $this->lang->line('name'); ?></label>
			<input required type="text" name="ContactName" class="smallInput wide" size="20%" value="">
		</div>
		
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('contact') . ' ' . $this->lang->line('number'); ?></label>
			<input min="11" max="11" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required type="text" name="ContactNumber" class="smallInput wide" size="20%" value="">
		</div>
		
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('phone_book') . ' ' . $this->lang->line('category'); ?></label>
			<select name="PhoneBookCategory" required="required" class="smallInput" id="class_id">
				<option value="">-- <?php echo $this->lang->line('select_category'); ?> --</option>
				<?php foreach ($categoryList as $row) { ?>
					<option value="<?php echo $row['id']; ?>" ><?php echo $row['name']; ?></option>

				<?php } ?>
			</select>
		</div>	
	</div>
	
    <input name="submit" type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form>
