<script type="text/javascript">
function count_alpha(val){
	var length = val.length;
/*	alert(length);
*/
	var diff = 160-length;
	document.getElementById("count_alert").innerHTML = diff+" characters left.";

/*	document.getElementById("count_alert").innerHTML = length+" characters.";
*/
}
</script>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>messages/AddNewTemplate" method="post">

	<div class="form-row">
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('template') . ' ' . $this->lang->line('name'); ?></label>
			<input required type="text" name="templateName" class="smallInput wide" size="20%" value="">
		</div>	
		
		<div class="form-group col-md-3">
			<label><?php echo $this->lang->line('template') . ' ' . $this->lang->line('body'); ?></label>
			<textarea required name="templateBody" id="sms_content" maxlength="160"onkeyup="count_alpha(this.value);" rows="5" cols="50" class="smallInput wide"></textarea>

		</div>
		
		<div class="form-group col-md-3">
		<label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('admission') . ' ' . $this->lang->line('welcome') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="smallInput" name="is_admission_welcome_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>
		
		<div class="form-group col-md-3">
		<label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('present') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="smallInput" name="is_present_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>
	</div>
	
	<div class="form-row">	
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('absent') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="smallInput" name="is_absent_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('leave') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="smallInput" name="is_leave_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>
		</div>
		
		<div class="form-group col-md-4">
			<label><?php echo $this->lang->line('is_this') . ' ' . $this->lang->line('bangla') . ' ' . $this->lang->line('sms'); ?> ?</label>
			<select class="smallInput" name="is_bangla_sms">
				<option value="0"><?php echo $this->lang->line('no'); ?></option>
				<option value="1"><?php echo $this->lang->line('yes'); ?></option>
			</select>

		</div>
	</div>	

    <br>
	<div class="row">&nbsp;&nbsp;<span id="count_alert"></span></div>
    <br>
    <input name="submit" type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form>
