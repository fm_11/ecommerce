<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;
		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
		}
	</style>

	<?php
} ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
        <thead>
        <tr>
            <td colspan="7" style="text-align:center;padding: 5px;">
				<b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
				<?php echo $HeaderInfo['address']; ?><br>
                    <?php
                    echo 'Teacher & Staff ' . $title . ' ' . $this->lang->line('of') . ' ' . date('d-M-Y', strtotime($date));
                    ?>
				</b>
            </td>
        </tr>

		<tr>
			<th style="text-align:center;" scope="col">Teacher ID</th>
			<th style="text-align:center;" scope="col"><?php echo $this->lang->line('name'); ?></th>
			<th style="text-align:center;" scope="col"><?php echo $this->lang->line('designation'); ?></th>
			<th style="text-align:center;" scope="col"><?php echo $this->lang->line('date'); ?></th>
			<th style="text-align:center;" scope="col"><?php echo $this->lang->line('login'); ?> <?php echo $this->lang->line('time'); ?></th>
			<th style="text-align:center;" scope="col"><?php echo $this->lang->line('logout'); ?> <?php echo $this->lang->line('time'); ?></th>
			<th style="text-align:center;" scope="col">Late Min.</th>
		</tr>

        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ($time_keeping_info as $row):
		$i++;
        ?>

		<tr>
			<td style="text-align:center;"><?php echo $row['teacher_code']; ?></td>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['post_name']; ?></td>
			<td style="text-align:center;"><?php echo $row['date']; ?></td>
			<td style="text-align:center;"><?php echo date('h:i:s A', strtotime($row['login_time'])); ?></td>
			<td style="text-align:center;"><?php echo date('h:i:s A', strtotime($row['logout_time'])); ?></td>
			<td style="text-align:center;"><?php echo $row['late_min']; ?></td>
		</tr>

        <?php endforeach; ?>
        </tbody>
    </table>
</div>
