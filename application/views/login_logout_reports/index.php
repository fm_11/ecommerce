<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>login_logout_reports/index#attendance" method="post">
  <div class="form-row">
          <div class="form-group col-md-4">
            <label for="Date">Date</label>
            <div class="input-group date">
                    <input type="text" autocomplete="off"  name="date" required value="<?php if (isset($date)) {
    echo $date;
} ?>" class="form-control">
                    <span class="input-group-text input-group-append input-group-addon">
                        <i class="simple-icon-calendar"></i>
                    </span>
            </div>
          </div>
  </div>

  <div class="btn-group float-right">
    <input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
    <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
    <input type="submit" class="btn btn-primary" value="View Report">
  </div>

</form>
<?php
if (isset($time_keeping_info)) {
    ?>
<h4 class="card-title"><?php echo $title; ?></h4>
<div id="printableArea" class="row table-sorter-wrapper col-lg-12 table-responsive">
  <?php
      echo $report_view; ?>
</div>

<?php
}
?>
