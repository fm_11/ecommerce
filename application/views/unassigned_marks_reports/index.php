<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>unassigned_marks_reports/index" method="post">
  <div class="form-row">
    <div class="form-group col-md-4">
        <label><?php echo $this->lang->line('exam'); ?></label>
        <select class="js-example-basic-single w-100" name="exam_id" id="exam_id" required="1">
            <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
            <?php
            $i = 0;
            if (count($exam_info)) {
                foreach ($exam_info as $list) {
                    $i++; ?>
                    <option
                        value="<?php echo $list['id']; ?>" <?php  if (isset($exam_id)) { if ($exam_id == $list['id']) {
                        echo 'selected';
                    }} ?>><?php echo $list['name']; ?></option>
                    <?php
                }
            }
            ?>
        </select>
    </div>
  </div>

  <div class="btn-group float-right">
    <input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
    <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
    <input type="submit" class="btn btn-primary" value="View Report">
  </div>
</form>

<div id="printableArea">
    <?php
    if (isset($return_data)) {
        echo $report;
    }
    ?>
</div>
