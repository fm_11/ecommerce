<?php
 if (isset($is_pdf) && $is_pdf == 1) {
     ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<style>

body {
    font-family: 'SolaimanLipi', Arial, sans-serif !important;
}
table, td, th {
  border: 1px solid black;
  font-size: 11px;
  padding-left: 3px !important;
  padding-right: 3px !important;
  padding: 0px;
}
table {
  border-collapse: collapse;
}
.h_td{
   font-weight: bold !important;
}
</style>

<?php
 } ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <td colspan="6"  class="h_td" style="text-align:center; line-height:1.5;">
            <b><?php echo $HeaderInfo['school_name']; ?></b><br>
            <?php echo $HeaderInfo['address']; ?><br>
            <?php echo $title; ?> of <?php echo $exam_name; ?></b>
        </td>
    </tr>

    <tr>
        <td scope="col" class="h_td"><b>Class-Section-Shift</b></td>
        <td scope="col" style="text-align:center;" class="h_td"><b>Group</b></td>
        <td style="text-align:center;" scope="col" class="h_td"><b>Total Subject</b></td>
        <td style="text-align:center;" scope="col" class="h_td"><b>Completed Subject</b></td>
        <td style="text-align:center;" scope="col" class="h_td"><b>Remaining Subject</b></td>
        <td scope="col" class="h_td"><b>Remaining Subject List</b></td>
    </tr>
    </thead>
    <tbody>

    <?php
    foreach ($return_data as $row):
      ?>
        <tr>
            <td><?php echo $row['class_shift_section']; ?></td>
            <td style="text-align:center;"><?php echo $row['group_name']; ?></td>
            <td style="text-align:center;"><?php echo $row['total_subject']; ?></td>
            <td style="text-align:center;"><?php echo $row['completed_subject']; ?></td>
            <td style="text-align:center;"><?php echo $row['remaining_subject']; ?></td>
            <td><?php echo $row['remaining_subject_list']; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
