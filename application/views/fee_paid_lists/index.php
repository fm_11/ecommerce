<script>
	function printDiv(divName) {
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
	}
</script>


<form name="addForm" class="cmxform" id="commentForm" target="_blank"  action="<?php echo base_url(); ?>fee_paid_lists/index" method="post">
	<div class="form-row">
		<div class="form-group col-md-4">
			<label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
			<select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($class_section_shift_marge_list)) {
					foreach ($class_section_shift_marge_list as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
							<?php if (isset($class_shift_section_id)) {
								if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label for="Year">Year</label>
			<select class="js-example-basic-single w-100" name="year" id="year" required="1">
				<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
				<?php
				$i = 0;
				if (count($years)) {
					foreach ($years as $list) {
						$i++; ?>
						<option
							value="<?php echo $list['value']; ?>"
							<?php if (isset($year)) {
								if ($year == $list['value']) {
									echo 'selected';
								}
							} ?>>
							<?php echo $list['text']; ?>
						</option>
						<?php
					}
				}
				?>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Group</label>
			<select class="js-example-basic-single w-100" name="txtGroup" required="1">
				<option value="">-- Please Select --</option>
				<?php
				if (count($groupList)) {
					foreach ($groupList as $list) {
						?>
						<option
							value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>


	</div>

	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Months</label>
			<select  class="js-example-basic-multiple w-100" multiple name="month[]" id="month" required="1">
				<?php
				$i = 1;
				while ($i <= 12) {
					$dateObj = DateTime::createFromFormat('!m', $i); ?>
					<option value="<?php echo $i; ?>" <?php if ($i == date('m')) {
						echo 'selected';
					} ?>><?php echo $dateObj->format('F'); ?></option>
					<?php
					$i++;
				}
				?>
			</select>
		</div>
	</div>


	<div class="float-right">
		<button class="btn btn-primary" type="submit"><?php echo $this->lang->line('view'); ?></button>
	</div>

</form>
