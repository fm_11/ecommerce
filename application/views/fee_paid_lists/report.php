<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<script>
		function printDiv(divName) {
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
		}
	</script>

	<style>
		#header{
			text-align:center;
			width:auto;

		}
		table{
			border-collapse: collapse;
		}
		th,td{
			font-size: 14px;
			padding: 4px;
			border: 1px solid black;
		}

		a:link, a:visited {
			background-color: #f44336;
			color: white;
			padding: 8px 8px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
		}

		a:hover, a:active {
			background-color: red;
		}

		.td_center{
			text-align: center;
		}
		.td_left{
			text-align: left;
		}
		.td_right{
			text-align: right;
		}



		.reortPrintOption{
			float: left;
			padding: 5px;
		}
	</style>
</head>
<body>

<div class="reortPrintOption">
	<a href="<?php echo base_url(); ?>fee_paid_lists/index">
		Back to Index
	</a>
	<a href="javascript:void" onclick="printDiv('printableArea')">
		Print
	</a>
</div>

<div id="printableArea">
	<?php
	//echo '<pre>';
	//print_r($paid_list);
	if (isset($paid_list)) {
		?>

		<table width="100%" cellpadding="0" border="1" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
			<tr>
				<th colspan="<?php echo count($months) + 4; ?>">
					<div id="header">
						<b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
						<b style="font-size:13px;">Class: <?php echo $HeaderInfo['ClassName']; ?>,
							Section: <?php echo $HeaderInfo['SectionName']; ?><br>
							Paid List for
							<?php
							foreach ($months as $month):
								echo DateTime::createFromFormat('!m', $month)->format('F').', ';
							endforeach; ?>
							<?php echo $HeaderInfo['year']; ?></b>

					</div>
				</th>
			</tr>

			<tr>
				<th style="vertical-align: middle;" scope="col" rowspan="2">Student ID</th>
				<th style="vertical-align: middle; max-width:130px;" rowspan="2" scope="col">Name</th>
				<th style="vertical-align: middle;" rowspan="2" scope="col">Roll</th>
				<?php
				// echo $colspan;
				foreach ($months as $month):
					?>
					<th style="text-align:center; max-width:250px;" scope="col">
						<?php echo DateTime::createFromFormat('!m', $month)->format('F'); ?>
					</th>
				<?php endforeach; ?>

				<th width="45" style="vertical-align: middle;" rowspan="2" scope="col">Total</th>
			</tr>

			</thead>
			<tbody>

			<?php
			$i = 0;
			$grand_total_paid_amount = 0;
			foreach ($paid_list as $row):
				?>

				<?php
				$total_paid_amount = 0;
				$process_monthly_td_data = array();
				foreach ($months as $month):

					$due_data_string = ""; ?>
					<?php
					if (isset($paid_list[$i]['fee_list_'.$month]) && !empty($paid_list[$i]['fee_list_'.$month])) {
						foreach ($paid_list[$i]['fee_list_'.$month] as $annual_row):
							if ($annual_row['total_paid_amount'] > 0) {
								$total_paid_amount += $annual_row['total_paid_amount'];
								$due_data_string .= $annual_row['sub_category_name'] . ': ' .$annual_row['total_paid_amount'] . ', ';
							}

						endforeach;
					} ?>

					<?php
					$process_monthly_td_data[$month]['all_sub_category_name'] = $due_data_string;
				endforeach;
				?>

				<?php
				if($total_paid_amount > 0 && $total_paid_amount != ''){
					?>
					<tr>
						<td  style="vertical-align: middle; text-align:center;">
							<?php echo $row['student_code']; ?>
						</td>
						<td style="vertical-align: middle; max-width:130px"><?php echo $row['name']; ?></td>
						<td style="vertical-align: middle; text-align:center;"><?php echo $row['roll_no']; ?></td>

						<?php
						foreach ($process_monthly_td_data as $process_monthly_td_row):
							?>
							<td style="text-align:left; max-width:250px;">
								<?php
								$process_monthly_td_row_r = rtrim($process_monthly_td_row['all_sub_category_name'], ", ");
								echo $process_monthly_td_row_r;
								?>
							</td>
						<?php
						endforeach;
						?>


						<td align="right">
							<b>
								<?php
								$grand_total_paid_amount += $total_paid_amount;
								echo number_format($total_paid_amount, 2); ?>
							</b>
						</td>
					</tr>
				<?php } ?>

				<?php $i++;
			endforeach; ?>

			<tr>
				<td align="right" colspan="<?php echo count($months) + 3; ?>">
					<b>Total</b>
				</td>
				<td align="right">
					<b><?php echo number_format($grand_total_paid_amount, 2); ?></b>
				</td>
			</tr>

			<tr>
				<td  align="left" colspan="<?php echo count($months) + 4; ?>">
					<b><?php echo $this->lang->line('in_words'); ?>: <?php echo $this->numbertowords->convert_number($grand_total_paid_amount); ?> Taka Only.</b>
				</td>
			</tr>

			</tbody>
		</table>

		<?php
	}
	?>

</div>
</body>
</html>
