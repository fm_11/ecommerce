
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<form name="updateForm" action="<?php echo base_url(); ?>welcome_messages/edit/" method="post">

	<div class="form-row">
		<div class="form-group col-md-12">
			<label>News</label>
			<textarea name="txtNews" id="tinyMceExample" rows="10" class="form-control"><?php echo $breaking_news_info->breaking_news; ?></textarea>
			<input type="hidden" name="id" value="<?php echo $breaking_news_info->id; ?>">
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
