<form name="addForm" class="cmxform"  id="commentForm"  action="<?php echo base_url(); ?>managing_committees/add" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Name&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" name="txtName" required="1"/>
		</div>
		<div class="form-group col-md-6">
			<label>Post&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" name="txtPost" required="1"/>
		</div>
	</div>
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Mobile&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" name="txtMobile"  required="1"/>
		</div>
		<div class="form-group col-md-6">
			<label>File&nbsp;<span class="required_label">*</span></label>
			<input type="file" name="txtPhoto" required="1" class="form-control">
			<span style="font-size: 11px;">
				* File Format -&gt;JPEG , JPG and PNG.<br>
				* Image Size -&gt; Height : 400PX and Width : 400PX
			</span>
		</div>
	</div>

	<div class="float-right">
		<input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
	</div>
</form>
