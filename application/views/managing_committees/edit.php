<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>managing_committees/edit" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Name&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" value="<?php echo $members[0]['name']; ?>" name="txtName" required="1"/>
		</div>
		<div class="form-group col-md-6">
			<label>Post&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control"  value="<?php echo $members[0]['post']; ?>" name="txtPost" required="1"/>
		</div>
	</div>
	<div class="form-row">
		<div class="form-group col-md-6">
			<label>Mobile&nbsp;<span class="required_label">*</span></label>
			<input type="text" autocomplete="off"  class="form-control" value="<?php echo $members[0]['mobile']; ?>" name="txtMobile"  required="1"/>
		</div>
		<div class="form-group col-md-6">
			<label>File</label>
			<input type="file" name="txtPhoto" class="form-control"> * File Format -> JPEG , JPG and PNG.
		</div>
	</div>

	<input type="hidden" class="smallInput wide" value="<?php echo $members[0]['id']; ?>" name="id"  required="1"/>

	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
