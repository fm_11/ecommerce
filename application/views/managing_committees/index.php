<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
    <thead>
    <tr>
        <th scope="col">SL</th>
        <th scope="col">Name</th>
        <th scope="col">Post</th>
        <th scope="col">Mobile</th>
        <th scope="col">Photo</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($managing_committee as $row):
        $i++;
        ?>

        <tr>
            <td style="vertical-align:middle;">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align:middle"><?php echo $row['name']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['post']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['mobile']; ?></td>
            <td style="vertical-align:middle">
                <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/managing_committee/<?php echo $row['photo_location'] ?>" class="img-lg rounded-circle mb-3">
            </td>

            <td>
              <div class="dropdown">
                  <button style="padding: 0.20rem .80rem;" class="btn btn-danger btn-sm dropdown-toggle" type="button" id="dropdownMenuIconButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="ti-pencil-alt"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton2">
                      <a class="dropdown-item" href="<?php echo base_url(); ?>managing_committees/edit/<?php echo $row['id']; ?>"><?php echo $this->lang->line('edit'); ?></a>
                      <a class="dropdown-item" onclick="return deleteConfirm()" href="<?php echo base_url(); ?>managing_committees/delete/<?php echo $row['id']; ?>"><?php echo $this->lang->line('delete'); ?></a>
                  </div>
              </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
