
                      <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>global_configs/index" method="post">

                    <div class="form-row">
    									<div class="form-group col-md-4">
    									  <label for="validationTooltip001">Annual Result Calculation Method</label>
                            <select name="annual_mark_calculate_method" class="form-control">

                                <option value="" <?php if (isset($config_info)) {
    if ($config_info[0]['annual_mark_calculate_method'] == "") {
        echo 'selected';
    }
} ?>>Not Applicable
                                </option>

                                <option value="SWP" <?php if (isset($config_info)) {
    if ($config_info[0]['annual_mark_calculate_method'] == 'SWP') {
        echo 'selected';
    }
} ?>>Subject Wise Add Mark Using Exam Percentage
                                </option>
                        		<option value="TMS" <?php if (isset($config_info)) {
    if ($config_info[0]['annual_mark_calculate_method'] == 'TMS') {
        echo 'selected';
    }
} ?>>Total Mark Summation
                                </option>
                            </select>
    								   </div>
    									  <div class="form-group col-md-4">
      										  <label for="is_pass_to_different_segment_of_result">Is Pass to Different Segment of Result ?</label>
                            <select  name="is_pass_to_different_segment_of_result" class="form-control" required="required">
                                <option value="1" <?php if (isset($config_info)) {
    if ($config_info[0]['is_pass_to_different_segment_of_result'] == '1') {
        echo 'selected';
    }
} ?>>Yes
                                </option>
                        		<option value="0" <?php if (isset($config_info)) {
    if ($config_info[0]['is_pass_to_different_segment_of_result'] == '0') {
        echo 'selected';
    }
} ?>>No
                                </option>
                            </select>
    									  </div>

                        <div class="form-group col-md-4">
                              <label for="add_extra_mark_with_total_obtained">Add Extra Mark With Total Obtained Mark ?</label>
                              <select name="add_extra_mark_with_total_obtained" class="form-control" required="required">
                                  <option value="1" <?php if (isset($config_info)) {
    if ($config_info[0]['add_extra_mark_with_total_obtained'] == '1') {
        echo 'selected';
    }
} ?>>Yes
                                  </option>
                          		<option value="0" <?php if (isset($config_info)) {
    if ($config_info[0]['add_extra_mark_with_total_obtained'] == '0') {
        echo 'selected';
    }
} ?>>No
                                  </option>
                              </select>
                        </div>
    								  </div>


                      <div class="form-row">
                          <div class="form-group col-md-4">
                            <label>Calculable Total Mark Percentage ?</label>
                            <select  name="calculable_total_mark_percentage" class="form-control" required="required">
                                <?php
                                  $len = 1;
                                  while ($len <= 100) {
                                      ?>
                               <option <?php if (isset($config_info)) {
                                          if ($config_info[0]['calculable_total_mark_percentage'] == $len) { ?> selected="selected" <?php }
                                      } ?> value="<?php echo $len; ?>"><?php echo $len; ?></option>
                                <?php
                                    $len++;
                                  }
                                ?>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label>Place Show Will be Marksheet ?</label>
                            <select name="place_show_will_be_marksheet" class="form-control" required="required">
                                <option value="1" <?php if (isset($config_info)) {
                                    if ($config_info[0]['place_show_will_be_marksheet'] == '1') {
                                        echo 'selected';
                                    }
                                } ?>>Yes
                                </option>
                        		<option value="0" <?php if (isset($config_info)) {
                                    if ($config_info[0]['place_show_will_be_marksheet'] == '0') {
                                        echo 'selected';
                                    }
                                } ?>>No
                                </option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label>Is Calculate CGPA When Fail ?</label>
                            <select name="is_calculate_cgpa_when_fail" class="form-control" required="required">
                                <option value="1" <?php if (isset($config_info)) {
                                    if ($config_info[0]['is_calculate_cgpa_when_fail'] == '1') {
                                        echo 'selected';
                                    }
                                } ?>>Yes
                                </option>
                        		<option value="0" <?php if (isset($config_info)) {
                                    if ($config_info[0]['is_calculate_cgpa_when_fail'] == '0') {
                                        echo 'selected';
                                    }
                                } ?>>No
                                </option>
                            </select>
                          </div>
                      </div>


                      <div class="form-row">
                          <div class="form-group col-md-4">
                            <label>How to Generate Position ?</label>
                            <select name="how_to_generate_position" class="form-control" required="required">
                                <option value="C" <?php if (isset($config_info)) {
                                    if ($config_info[0]['how_to_generate_position'] == 'C') {
                                        echo 'selected';
                                    }
                                } ?>>Based on Class
                                </option>
                        		<option value="CS" <?php if (isset($config_info)) {
                                    if ($config_info[0]['how_to_generate_position'] == 'CS') {
                                        echo 'selected';
                                    }
                                } ?>>Based on Class and Section
                                </option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label>Number of Subject Allow for Grace ?</label>
                            <select class="form-control" name="num_of_subject_allow_for_grace">
                                   <option value="0">Not Applicable</option>
                             	   <?php
                                      $len = 1;
                                      while ($len <= 20) {
                                          ?>

                        	       <option <?php if ($config_info[0]['num_of_subject_allow_for_grace'] == $len) { ?> selected="selected" <?php } ?> value="<?php echo $len; ?>"><?php echo $len; ?></option>

                                    <?php
                                        $len++;
                                      }
                                    ?>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label>Can more than one person be in the same position ?</label>
                            <select name="multiple_atudent_allow_same_position" class="form-control" required="required">
                                <option value="1" <?php if (isset($config_info)) {
                                        if ($config_info[0]['multiple_atudent_allow_same_position'] == '1') {
                                            echo 'selected';
                                        }
                                    } ?>>Yes
                                </option>
                        		<option value="0" <?php if (isset($config_info)) {
                                        if ($config_info[0]['multiple_atudent_allow_same_position'] == '0') {
                                            echo 'selected';
                                        }
                                    } ?>>No
                                </option>
                            </select>
                          </div>
                      </div>


                      <div class="form-row">
                          <div class="form-group col-md-4">
                            <label>Marksheet Design ?</label>
                            <select name="marksheet_design" class="form-control" required="required">
                                <option value="N" <?php if (isset($config_info)) {
                                        if ($config_info[0]['marksheet_design'] == 'N') {
                                            echo 'selected';
                                        }
                                    } ?>>Normal View
                                </option>
                        		<option value="D" <?php if (isset($config_info)) {
                                        if ($config_info[0]['marksheet_design'] == 'D') {
                                            echo 'selected';
                                        }
                                    } ?>>Details View 1
                                </option>

                                <option value="D2" <?php if (isset($config_info)) {
                                        if ($config_info[0]['marksheet_design'] == 'D2') {
                                            echo 'selected';
                                        }
                                    } ?>>Details View 2
                                </option>

                                <option value="D3" <?php if (isset($config_info)) {
                                        if ($config_info[0]['marksheet_design'] == 'D3') {
                                            echo 'selected';
                                        }
                                    } ?>>Details View 3 (Follow -> Sarishabari Model Madrasah)
                                </option>

                            </select>

                          </div>
                          <div class="form-group col-md-4">
                            <label>Is Process Bangla (101 and 102) & English (107 and 108) Separate ?</label>
                            <select name="is_process_bangla_english_separate" class="form-control" required="required">
                                <option value="1" <?php if (isset($config_info)) {
                                        if ($config_info[0]['is_process_bangla_english_separate'] == '1') {
                                            echo 'selected';
                                        }
                                    } ?>>Yes
                                </option>
                        		<option value="0" <?php if (isset($config_info)) {
                                        if ($config_info[0]['is_process_bangla_english_separate'] == '0') {
                                            echo 'selected';
                                        }
                                    } ?>>No
                                </option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label>Is Transport Fee Applicable ?</label>
                            <select name="is_transport_fee_applicable" class="form-control" required="required">
                                <option value="1" <?php if (isset($config_info)) {
                                        if ($config_info[0]['is_transport_fee_applicable'] == '1') {
                                            echo 'selected';
                                        }
                                    } ?>>Yes
                                </option>
                        		<option value="0" <?php if (isset($config_info)) {
                                        if ($config_info[0]['is_transport_fee_applicable'] == '0') {
                                            echo 'selected';
                                        }
                                    } ?>>No
                                </option>
                            </select>
                          </div>
                      </div>


                      <div class="form-row">
                          <div class="form-group col-md-4">
                            <label>Fee Module Type</label>
                              <select name="fee_module_type" class="form-control" required="required">
                                  <option value="S" <?php if (isset($config_info)) {
                                        if ($config_info[0]['fee_module_type'] == 'S') {
                                            echo 'selected';
                                        }
                                    } ?>>Standard
                                  </option>
                          		<option value="N" <?php if (isset($config_info)) {
                                        if ($config_info[0]['fee_module_type'] == 'N') {
                                            echo 'selected';
                                        }
                                    } ?>>Normal
                                  </option>
                              </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label>Accounts Module Type</label>
                              <select  name="accounts_module_type" class="form-control" required="required">
                                  <option value="B" <?php if (isset($config_info)) {
                                        if ($config_info[0]['accounts_module_type'] == 'B') {
                                            echo 'selected';
                                        }
                                    } ?>>Basic
                                  </option>
                          		<option value="A" <?php if (isset($config_info)) {
                                        if ($config_info[0]['accounts_module_type'] == 'A') {
                                            echo 'selected';
                                        }
                                    } ?>>Advanched
                                  </option>
                              </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label>Is Device Integrated Attendance System ?</label>
                              <select name="is_device_integrated_attendance_system" class="form-control" required="required">
                                  <option value="Y" <?php if (isset($config_info)) {
                                        if ($config_info[0]['is_device_integrated_attendance_system'] == 'Y') {
                                            echo 'selected';
                                        }
                                    } ?>>Yes
                                  </option>
                          		<option value="N" <?php if (isset($config_info)) {
                                        if ($config_info[0]['is_device_integrated_attendance_system'] == 'N') {
                                            echo 'selected';
                                        }
                                    } ?>>No
                                  </option>
                              </select>
                          </div>
                      </div>


                      <div class="form-row">
                          <div class="form-group col-md-4">
                            <label>Software Start Year</label>
                            <select name="software_start_year" class="form-control" id="software_start_year" required="1">
                                <option value="">-- Select --</option>
                                <?php
                                $i = 2015;
                                while ($i < (date('Y') + 1)) {
                                    ?>
                                     <option value="<?php echo $i; ?>" <?php if ($i == $config_info[0]['software_start_year']) {
                                        echo 'selected';
                                    } ?>><?php echo $i; ?></option>
                                <?php
                                $i++;
                                }
                                ?>
                            </select>
                          </div>

                          <div class="form-group col-md-4">
                            <label>Software Current Year</label>
                            <select name="software_current_year" class="form-control" id="software_current_year" required="1">
                                <option value="">-- Select --</option>
                                <?php
                                $i = 2019;
                                while ($i < (date('Y') + 1)) {
                                    ?>
                                     <option value="<?php echo $i; ?>" <?php if ($i == $config_info[0]['software_current_year']) {
                                        echo 'selected';
                                    } ?>><?php echo $i; ?></option>
                                <?php
                                $i++;
                                }
                                ?>
                            </select>
                          </div>

                          <div class="form-group col-md-4">
                            <label>Default Language</label>
                              <select name="default_language" class="form-control" required="required">
                                  <option value="E" <?php if (isset($config_info)) {
                                    if ($config_info[0]['default_language'] == 'E') {
                                        echo 'selected';
                                    }
                                } ?>>English
                                  </option>
                          		<option value="B" <?php if (isset($config_info)) {
                                    if ($config_info[0]['default_language'] == 'B') {
                                        echo 'selected';
                                    }
                                } ?>>Bangla
                                  </option>
                              </select>
                          </div>

                      </div>

                      <div class="form-row">
                          <div class="form-group col-md-4">
                            <label>Is show all previous due amount to collection sheet ?</label>
                              <select name="show_due_amount_to_collection_sheet" class="form-control" required="required">
                                  <option value="1" <?php if (isset($config_info)) {
                                    if ($config_info[0]['show_due_amount_to_collection_sheet'] == '1') {
                                        echo 'selected';
                                    }
                                } ?>>Yes
                                  </option>
                          		<option value="0" <?php if (isset($config_info)) {
                                    if ($config_info[0]['show_due_amount_to_collection_sheet'] == '0') {
                                        echo 'selected';
                                    }
                                } ?>>No
                                  </option>
                              </select>
                          </div>

						  <div class="form-group col-md-4">
							  <label>Menu Type</label>
							  <select name="menu_type" class="form-control" required="required">
								  <option value="T" <?php if (isset($config_info)) {
									  if ($config_info[0]['menu_type'] == 'T') {
										  echo 'selected';
									  }
								  } ?>>Top
								  </option>
								  <option value="LD" <?php if (isset($config_info)) {
									  if ($config_info[0]['menu_type'] == 'LD') {
										  echo 'selected';
									  }
								  } ?>>Left Dark
								  </option>

								  <option value="LL" <?php if (isset($config_info)) {
									  if ($config_info[0]['menu_type'] == 'LL') {
										  echo 'selected';
									  }
								  } ?>>Left Light
								  </option>
							  </select>
						  </div>
                    </div>


                	<input type="hidden" name="id" required class="smallInput wide" size="20%" value="<?php echo $config_info[0]['id']; ?>">

                  <div class="float-right">
                     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
                  </div>
                </form>
