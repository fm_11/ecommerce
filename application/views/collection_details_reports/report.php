<?php
 if (isset($is_pdf) && $is_pdf == 1) {
     ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
<style>

body {
    font-family: 'SolaimanLipi', Arial, sans-serif !important;
}
table, td, th {
  border: 1px solid black;
}
table {
  border-collapse: collapse;
}
</style>

<?php
 } ?>


<div class="table-sorter-wrapper col-lg-12 table-responsive">
<table width="100%" id="sortable-table-1" class="table">
    <thead>
    <tr>
        <td class="center_td" colspan="2" style="text-align:center">
            <b style="font-size:15px;"><?php echo $HeaderInfo['school_name']; ?></b><br>
                <?php echo $title; ?>
            </b>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <?php echo $this->lang->line('class'); ?>: <b><?php echo $class_name; ?></b>,
            Date: <b><?php echo date("M jS, Y", strtotime($from_date)); ?> to <?php echo date("M jS, Y", strtotime($to_date)); ?></b>
        </td>
    </tr>
     </thead>

	  <tbody>
    <?php
    $i = 0;
    $total_amount = 0;
    foreach ($rdata as $row):
        $i++;
        ?>
        <tr>
            <td align="right" width="50%">
                &nbsp;<?php echo $row['sub_category_name']; ?><br>
            </td>
			<td>
                &nbsp;<?php $total_amount+= $row['total_amout']; echo number_format($row['total_amout'], 2); ?><br>
            </td>
        </tr>
    <?php endforeach; ?>
	   <tr>
	       <td align="right"><b><?php echo $this->lang->line('total'); ?>&nbsp;</b></td>
		   <td><b><?php echo number_format($total_amount, 2); ?> &nbsp;</b></td>
	   </tr>

	   <tr>
        <td class="textleft" colspan="2">&nbsp;<b><?php echo $this->lang->line('in_words'); ?>: <?php echo $this->numbertowords->convert_number($total_amount); ?> Taka Only.</b></td>
     </tr>

    </tbody>

</table>
</div>
