<script type="text/javascript">
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>

<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>collection_details_reports/index" method="post">
  <div class="form-row">
    <div class="form-group col-md-4">
      <label>Class</label>
      <select class="js-example-basic-single w-100" name="class_id" id="class_id">
          <option value="all">-- <?php echo $this->lang->line('all'); ?> <?php echo $this->lang->line('class'); ?> --</option>
          <?php
          $i = 0;
          if (count($class)) {
              foreach ($class as $list) {
                  $i++; ?>
                  <option
                      value="<?php echo $list['id']; ?>" <?php if (isset($class_id)) {
                      if ($class_id == $list['id']) {
                          echo 'selected';
                      }
                  } ?>><?php echo $list['name']; ?></option>
                  <?php
              }
          }
          ?>
      </select>
    </div>
    <div class="form-group col-md-4">
        <label><?php echo $this->lang->line('from_date'); ?></label>
        <div class="input-group date">
            <input type="text" autocomplete="off"  name="from_date" <?php if (isset($from_date)) { ?> value="<?php echo $from_date; ?>"
            <?php } ?>  required class="form-control">
            <span class="input-group-text input-group-append input-group-addon">
                <i class="simple-icon-calendar"></i>
            </span>
        </div>
    </div>
    <div class="form-group col-md-4">
      <label><?php echo $this->lang->line('to_date'); ?></label>
      <div class="input-group date">
          <input type="text" autocomplete="off"  name="to_date" <?php if (isset($to_date)) { ?> value="<?php echo $to_date; ?>"
          <?php } ?>  required class="form-control">
          <span class="input-group-text input-group-append input-group-addon">
              <i class="simple-icon-calendar"></i>
          </span>
      </div>
    </div>
  </div>


  <div class="btn-group float-right">
    <input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
    <input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
    <input type="submit" class="btn btn-primary" value="View Report">
  </div>

</form>

<div id="printableArea">
    <?php
    if (isset($rdata)) {
        echo $report;
    }
    ?>
</div>
