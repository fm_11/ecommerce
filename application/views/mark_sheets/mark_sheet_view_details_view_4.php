<?php
function get_numeric_gpa_to_alfa_gpa($gpa)
{
    if (($gpa >= '5')) {
        return 'A+';
    } elseif ($gpa >= '4') {
        return "A";
    } elseif ($gpa >= '3.5') {
        return "A-";
    } elseif ($gpa >= '3.0') {
        return "B";
    } elseif ($gpa >= '2.0') {
        return "C";
    } elseif ($gpa >= '1.0') {
        return "D";
    } elseif ($gpa <= 0) {
        return "F";
    }
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/result.css">
</head>
<body class="scms-result-print">
    <?php if ($is_pdf != 1) { ?>
        <button class="printButton" onclick="CallPrint()">Print this page</button>
    <?php } ?>
<style>
    .printButton {
        position: absolute;
        top: 20px;

        left: 20px;

        height: 30px;
    }
</style>

<script type="text/javascript">
    function CallPrint() {
        window.print();
    }
</script>

<?php //echo $Result['result_process_info'][0]['name']; echo '<pre>'; print_r($Result); die;?>

<?php
    $i = 0;
    foreach ($Result as $row):
?>
<div class="wraperResult" id="printMarkSheet">
    <div class="resHdr">
        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/marksheet_logo.png" alt="" class="resLogo">
        <div class="schoolIdentity">
            <h1> <?php echo $school_info[0]['school_name']; ?></h1>
            <div class="hdrText">
                <span><?php echo $exam[0]['name']; ?>, <?php echo $exam[0]['year']; ?></span>
            </div>
        </div>
    </div>

    <div class="resContainer">

         <div class="resTophdr">
            <div class="restopleft">
                <div><span>STUDENT ID</span><i>: </i><b><em>&nbsp;&nbsp;<?php echo $row['student_code']; ?></em></b></div>
                <div><span>NAME</span><i>: </i><b><em>&nbsp;&nbsp;<?php echo $row['student_name']; ?></em></b></div>
                <div><span>FATHER'S NAME</span><i>: </i><em>&nbsp; <?php echo $row['father_name']; ?></em></div>
                <div><span>MOTHER'S NAME</span><i>: </i><em>&nbsp; <?php echo $row['mother_name']; ?></em></div>
                <div><span>DATE OF BIRTH</span><i>: </i><em>&nbsp;
                <?php

               // echo $row['date_of_birth'];
                if ($row['date_of_birth'] != '0000-00-00' && $row['date_of_birth'] != '') {
                    echo date("d-m-Y", strtotime($row['date_of_birth']));
                } else {
                    echo 'N/A';
                }

                ?>
                </em></div>
            </div>

            <div class="restopleft rgttopleft">
                <div><span style="width: 60px; font-size: 12px;">CLASS</span><i>: </i><em> <?php echo $class_name; ?></em></div>
                <div><span style="width: 60px; font-size: 12px;">GROUP</span><i>: </i><em><?php echo $group_name; ?></em></div>
                <div><span style="width: 60px; font-size: 12px;">SECTION</span><i>: </i><em> <?php echo $section_name; ?></em></div>
                <div><span style="width: 60px; font-size: 12px;">ROLL NO</span><i>: </i><em> <?php echo $row['roll_no']; ?></em></div>
            </div>
        </div>


        <div class="resmidcontainer">
            <h2 class="markTitle">Subject-Wise Grade &amp; Mark Sheet</h2>
            <table class="pagetble_middle">
                <tbody>
                <tr>
                    <th class="res1" rowspan="2">CODE</th>
                    <th class="res2 cTitle" rowspan="2">SUBJECT</th>
                    <th colspan="9"><span><?php echo $exam[0]['name']; ?></th>
                </tr>
                <tr>
                    <td class="res1">Full Marks</td>
                    <td class="res1">Highest Marks</td>
                    <td class="res1">Multiple Choice</td>
                    <td class="res1">Creative</td>
                    <td class="res1">Class Test</td>
                    <td class="res1">Practical</td>
                    <td class="res1">Total Obtained</td>
                    <td class="res1">Letter Grade</td>
                    <td class="res1">GP</td>
                </tr>

            	<?php
                    $j = 0;
                     $total_creadit = 0;
                     $optional_sub_got_mark = 0;
                    foreach ($Result[$i]['result'] as $result_row):
                        $total_creadit = $total_creadit + $result_row['credit'];
                ?>

                <tr <?php if ($result_row['is_optional'] == '1') { ?> style="background-color: lightyellow" <?php } ?>>
						<td align="center">
					     	&nbsp;<?php echo $result_row['subject_code']; ?><br>
				     	</td>

				    	<td style="text-align:left;">
						    &nbsp;<?php echo $result_row['subject_name']; ?>
    					</td>

    					<td align="center">&nbsp;
    						<?php
                            echo $result_row['credit'];
                            ?>
    					</td>

						<td>
						    <?php echo $highest_marks[$result_row['subject_code']]; ?>
						</td>

            <td align="center">&nbsp;
              <?php
                      echo $result_row['objective'];
                      ?>
            </td>
              <td align="center">&nbsp;
              <?php
                      echo $result_row['written'];
                      ?>
            </td>


							<td align="center">&nbsp;
                				<?php
                                echo $result_row['class_test'];
                                ?>
                			</td>


                			<td align="center">&nbsp;
                				<?php
                                echo $result_row['practical'];
                                ?>
                			</td>
                			<td align="center">&nbsp;
                				<?php
                          echo $result_row['total_obtain'];

                          if ($result_row['is_optional'] == '1') {
                              $optional_sub_got_mark = $result_row['total_obtain'];
                          }
                        ?>
                			</td>

					<?php if ($is_process_bangla_english_separate == 1 && $result_row['subject_code'] == '101') { ?>
							<td style="vertical-align: middle;" rowspan="2">
								 <?php
                                 echo $result_row['alpha_gpa'];
                                 ?>
							 </td>
					<?php } ?>

					<?php if ($is_process_bangla_english_separate == 1 && $result_row['subject_code'] == '107') { ?>
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>


						<?php if ($is_process_bangla_english_separate == 0 || ($result_row['subject_code'] != '101' && $result_row['subject_code'] != '102' && $result_row['subject_code'] != '107' && $result_row['subject_code'] != '108')) { ?>
							<td>
							   <?php echo $result_row['alpha_gpa']; ?>
							</td>
					 <?php } ?>

					   <?php if ($result_row['subject_code'] == '101') { ?>
							<td style="vertical-align: middle;" rowspan="2">
							     <?php echo number_format($result_row['numeric_gpa'], 2); ?>
						    </td>
						<?php } ?>

						<?php if ($result_row['subject_code'] == '107') { ?>
							<td style="vertical-align: middle" rowspan="2">
							     <?php echo number_format($result_row['numeric_gpa'], 2); ?>
							</td>
						<?php } ?>



						<?php if ($result_row['subject_code'] != '101' && $result_row['subject_code'] != '102' && $result_row['subject_code'] != '107' && $result_row['subject_code'] != '108') { ?>
							<td>
							   <?php echo number_format($result_row['numeric_gpa'], 2); ?>
							</td>
						<?php } ?>

					</tr>

    		    	<?php
                        $j++;
                        endforeach;
                    ?>

                <tr>
				    <td colspan="2" style="text-align:right; font-weight:bold;">Total Exam Marks&nbsp;</td>

				    <td style="text-align:center; font-weight:bold;"><?php echo $total_creadit; ?></td>
				    <td style="text-align:center; font-weight:bold;" colspan="5">
				        Obtained Marks & GPA
				    </td>
				    <td style="text-align:center; font-weight:bold;"><?php echo $row['total_obtain_mark']; ?></td>
				    <td style="text-align:center; font-weight:bold;">
              <?php
              if ($row['c_alpha_gpa_with_optional'] == 'F' && $row['number_of_failed_subject'] <= 1) {
                  echo '';
              } else {
                  echo $row['c_alpha_gpa_with_optional'];
              }

               ?>
            </td>
				    <td style="text-align:center; font-weight:bold;">
              <?php
              if ($row['c_alpha_gpa_with_optional'] == 'F' && $row['number_of_failed_subject'] <= 1) {
                  echo '';
              } else {
                  echo number_format($row['gpa_with_optional'], 2);
              }

              ?>
            </td>

				</tr>


				<tr>
				    <td style="border-right:1px black solid; font-size:11px;" colspan="11">
				         <table width="100%" style="border:0px; padding:5px;">
				             <tr>


				                 <td width="35%" style="border:0px; vertical-align:top;" >
				                     <table width="100%" style="border:0px;" >

				                          <tr>
                                                <td width="50%" colspan="2"  class="bottom_td" style="border:1px black solid;">
                                                 Result Summery
                                                </td>

                                            </tr>

                                            <tr>
                                                <td width="35%"  class="bottom_td" style="border:1px black solid;">
                                                 Position
                                                </td>
                                                <td width="15%" class="bottom_td"  style="border:1px black solid;">
                                                <b><?php echo $row['position']; ?></b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="35%" class="bottom_td"  style="border:1px black solid;">
                                                Total Mark (Without 4th)
                                                </td>
                                                <td width="15%" class="bottom_td"  style="border:1px black solid;">
                                                <b>
                                                  <?php
                                                  if ($optional_sub_got_mark <= 40) {
                                                      echo $row['total_obtain_mark'] - $optional_sub_got_mark;
                                                  } else {
                                                      echo $row['total_obtain_mark'] - 40;
                                                  }
                                                  ?>
                                                </b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="35%" class="bottom_td"  style="border:1px black solid;">
                                                GPA (Without 4th)
                                                </td>
                                                <td width="15%" class="bottom_td"  style="border:1px black solid;">
                                                <b><?php
                                                echo $row['gpa_without_optional'];
                                                ?></b>
                                                </td>
                                            </tr>




                                            <tr>
                                                <td width="25%"  class="bottom_td" style="border:1px black solid;">
                                                Failed Subjects
                                                </td>
                                                <td width="25%" class="bottom_td"  style="border:1px black solid;">
                                                <b><?php echo $row['number_of_failed_subject']; ?></b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="25%"  class="bottom_td" style="border:1px black solid;">
                                                1st Place Mark
                                                </td>
                                                <td width="25%"  class="bottom_td" style="border:1px black solid;">
                                                <b><?php echo $max_total_obtain_mark[0]['max_total_obtain_mark']; ?></b>
                                                </td>
                                            </tr>



				                     </table>
				                 </td>


				                 <td width="35%" style="border:0px; vertical-align:top;" >
				                     <table width="100%" style="border:0px;" >
                                            <tr>
                                                <td width="50%" colspan="2" class="bottom_td" style="border:1px black solid;">
                                                 Moral & Behavior Evaluation
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="10%" class="bottom_td"  style="border:1px black solid;">

                                                </td>
                                                <td width="15%" class="bottom_td" align="left" style="border:1px black solid;">
                                                <b>Outstanding</b>
                                                </td>
                                            </tr>


                                            <tr>
                                                <td width="10%" class="bottom_td"  style="border:1px black solid;">

                                                </td>
                                                <td width="15%" class="bottom_td" align="left" style="border:1px black solid;">
                                                <b>Excellent</b>
                                                </td>
                                            </tr>


                                            <tr>
                                                <td width="10%" class="bottom_td"  style="border:1px black solid;">

                                                </td>
                                                <td width="15%" class="bottom_td"  style="border:1px black solid;">
                                                <b>Very Good</b>
                                                </td>
                                            </tr>

                                             <tr>
                                                <td width="10%" class="bottom_td"  style="border:1px black solid;">

                                                </td>
                                                <td width="15%" class="bottom_td"  style="border:1px black solid;">
                                                <b>Good</b>
                                                </td>
                                            </tr>

                                             <tr>
                                                <td width="10%" class="bottom_td"  style="border:1px black solid;">

                                                </td>
                                                <td width="15%" class="bottom_td"  style="border:1px black solid;">
                                                <b>Need Imporvement</b>
                                                </td>
                                            </tr>


				                     </table>
				                 </td>

				                  <td width="30%" style="border:0px; vertical-align:top;" >
				                     <table width="100%" style="border:0px;" >
                                             <tr>
                                                <td width="100%" class="bottom_td"  style="border:1px black solid;">
                                                   &nbsp;&nbsp; <b> Comments </b>&nbsp;&nbsp;
                                                </td>

                                            </tr>
                                            <tr>
                                                <td width="100%"  style="border:1px black solid; height:135px;">
                                                   &nbsp;
                                                </td>
                                            </tr>
				                     </table>
				                 </td>


				             </tr>
				        </table>

				    </td>
				</tr>


				<tr>
				    <td style="height:100px;  vertical-align: bottom" colspan="11">
				        <table width="100%" style="border:0px;">
				            <tr>
				                <td width="50%" style="border:0px;">
                            <!-- <img  style="height: 60px;
    margin-top: -5px;
    width: 127px;
    margin-left: 9px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/1class_teacher_sig.png" alt=""> -->

                            <b>
                                    &nbsp;&nbsp;............................................................<br>
                                    &nbsp;&nbsp;Class Teacher<br>
                                    </b>
				                </td>
				                <td width="50%" style="border:0px;">
                          <img  style="height:60px; margin-top: -5px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/marksheet_signature.png" alt="">
				                    <b>
                                    &nbsp;&nbsp;............................................................<br>
                                    &nbsp;&nbsp;Head Teacher<br>
                                    </b>
				                </td>
				            </tr>
				        </table>
				    </td>
				</tr>

                </tbody>
            </table>
            <p style="font-size:12px;text-align:right; margin-top: 280px;"><b>Powered by: School360</b> &nbsp;&nbsp;</p>
        </div>
    </div>
    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/certificate-bg.png" alt="" class="result-bg">
</div>
<div class="page-break">&nbsp;</div>
<?php
    $i++;
    endforeach;
?>
</div>
<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/arctext.js"></script>
<script>
    jQuery(document).ready(function () {
        var valB = jQuery('.Bengalicmn .avrg').html();
        var rowspanL = jQuery('.pagetble_middle .Bengalicmn').find('.hide').length;
        jQuery('.Bengali .avrgvl').parent().attr('rowspan', rowspanL + 1).children('.avrgvl').html(valB).css('font-weight', 'bold');
        jQuery('.pagetble_middle .Bengalicmn').find('.hide').hide();
        var valE = jQuery('.Englishcmn .avrg').html();
        var rowspanL = jQuery('.pagetble_middle .Englishcmn').find('.hide').length;
        jQuery('.English .avrgvl').parent().attr('rowspan', rowspanL + 1).children('.avrgvl').html(valE).css('font-weight', 'bold');
        jQuery('.pagetble_middle .Englishcmn').find('.hide').hide();
        jQuery('.instName').arctext({radius: 300});

    });
</script>
</body>
</html>
