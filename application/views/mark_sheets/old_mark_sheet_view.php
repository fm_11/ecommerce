<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/result.css">
</head>
<body class="scms-result-print">
    <?php if($is_pdf != 1){ ?>
        <button class="printButton" onclick="CallPrint()">Print this page</button>
    <?php } ?>
<style>
    .printButton {
        position: absolute;
        top: 20px;

        left: 20px;

        height: 30px;
    }
</style>

<script type="text/javascript">
    function CallPrint() {
        window.print();
    }
</script>

<?php //echo $Result['result_process_info'][0]['name']; echo '<pre>'; print_r($Result); die; ?>

<?php
    $i = 0;
    foreach ($Result as $row):
?>
<div id="printMarkSheet">
<div class="wraperResult" style="background-color: #<?php echo $admit_card_background_color; ?>; color:#<?php echo $admit_card_text_color; ?>;">
<div style="background: url(<?php echo base_url() . MEDIA_FOLDER; ?>/logos/marksheet_background.png) no-repeat center;">
    <div class="resHdr">
        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/marksheet_logo.png" alt="" class="resLogo">
        <div class="schoolIdentity">
            <h1> <?php echo $school_info[0]['school_name']; ?></h1>
            <div class="hdrText">
                <span><?php echo $exam[0]['name']; ?>, <?php echo $exam[0]['year']; ?></span>
            </div>
        </div>
    </div>

    <div class="resContainer">
        <div class="resTophdr">
            <div class="restopleft">
                <div><span>STUDENT ID</span><i>: </i><b><em>&nbsp;&nbsp;<?php echo $row['student_code']; ?></em></b></div>
                <div><span>NAME</span><i>: </i><b><em>&nbsp;&nbsp;<?php echo $row['student_name']; ?></em></b></div>
                <div><span>FATHER'S NAME</span><i>: </i><em>&nbsp; <?php echo $row['father_name']; ?></em></div>
                <div><span>MOTHER'S NAME</span><i>: </i><em>&nbsp; <?php echo $row['mother_name']; ?></em></div>
                <div><span>DATE OF BIRTH</span><i>: </i><em>&nbsp; 
                <?php 
               // echo $row['date_of_birth'];
                if($row['date_of_birth'] != '0000-00-00' && $row['date_of_birth'] != ''){
                    echo date("d-m-Y", strtotime($row['date_of_birth']));
                }else{
                    echo 'N/A';
                }
                
                ?>
                </em></div>
            </div>

            <div class="restopleft rgttopleft">
                <div><span style="width: 60px; font-size: 12px;">CLASS</span><i>: </i><em> <?php echo $class_name; ?></em></div>
                <div><span style="width: 60px; font-size: 12px;">GROUP</span><i>: </i><em><?php echo $group_name; ?></em></div>
                <div><span style="width: 60px; font-size: 12px;">SECTION</span><i>: </i><em> <?php echo $section_name; ?></em></div>
                <div><span style="width: 60px; font-size: 12px;">ROLL NO</span><i>: </i><em> <?php echo $row['roll_no']; ?></em></div>
            </div>
        </div>

        <div class="resmidcontainer">
            <h2 class="markTitle"  style="color:#<?php echo $admit_card_text_color; ?>;">Subject-Wise Grade &amp; Mark Sheet</h2>
            <table class="pagetble_middle">
                <tbody>
                <tr>
                    <th class="res1" rowspan="2">CODE</th>
                    <th class="res2 cTitle" rowspan="2">SUBJECT</th>
                    <th colspan="10"><span><?php echo $exam[0]['name']; ?></th>
                </tr>
                <tr>
                    <td class="res1">Credit</td>
                    <td class="res1">Creative</td>
                    <td class="res1">Objective</td>
                    <td class="res1">Practical</td>
                    <td class="res1">Total Mark</td>
                    <td class="res1">Letter Grade</td>
                    <td class="res1">Total Obtained Mark</td>                    
                    <td class="res1">GPA<br>(With Optional)</td>
                    <?php
                    if($place_show_will_be_marksheet == 1){
                    ?>
    					<td class="res1">Class Place</td>
    					<td class="res1">1st Place <br> Mark</td>
					<?php
                    }
                    ?>
                </tr>

				<?php
				$j = 0;
				foreach ($Result[$i]['result'] as $result_row):
				?>

				  <tr <?php if ($result_row['is_optional'] == '1') { ?> style="background-color: lightyellow" <?php } ?>>            
					<td align="center">
						&nbsp;<?php echo $result_row['subject_code']; ?><br>
					</td>
					<td style="text-align:left;">
						&nbsp;<?php echo $result_row['subject_name']; ?>
					</td>
					<td align="center">&nbsp;
						<?php
						echo $result_row['credit'];
						?>
					</td>
					<td align="center">&nbsp;
						<?php
						echo $result_row['written'];
						?>
					</td>
					<td align="center">&nbsp;
						<?php
						echo $result_row['objective'];
						?>
					</td>
					<td align="center">&nbsp;
						<?php
						echo $result_row['practical'];
						?>
					</td>
					<td align="center">&nbsp;
						<?php
						echo $result_row['total_obtain'];
						?>
					</td>
					
					<?php if ($is_process_bangla_english_separate == 1 && $result_row['subject_code'] == '101') { ?>
							<td style="vertical-align: middle;" rowspan="2">
								 <?php
								 echo $result_row['alpha_gpa'];
								 ?>
							 </td>
					<?php } ?>
					
					<?php if ($is_process_bangla_english_separate == 1 && $result_row['subject_code'] == '107') { ?>
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
					<?php if ($is_process_bangla_english_separate == 0 || ($result_row['subject_code'] != '101' && $result_row['subject_code'] != '102' && $result_row['subject_code'] != '107' && $result_row['subject_code'] != '108')) { ?>
							<td>
							   <?php echo $result_row['alpha_gpa']; ?>
							</td>
					<?php } ?>
					
					<?php if ($j == 0) { ?>
							<td style="vertical-align: middle" rowspan="<?php echo count($Result[$i]['result']); ?>">
									<?php echo $row['total_obtain_mark']; ?>
							</td>
							<td style="vertical-align: middle" rowspan="<?php echo count($Result[$i]['result']); ?>">
									<?php echo $row['gpa_with_optional']; ?>
							</td>
							
        					<?php
                            if($place_show_will_be_marksheet == 1){
                            ?>
                				<td style="vertical-align: middle"rowspan="<?php echo count($Result[$i]['result']); ?>">
    								 <?php echo $row['position']; ?>
    							</td>
    							<td style="vertical-align: middle"rowspan="<?php echo count($Result[$i]['result']); ?>">
    								   <?php echo $max_total_obtain_mark[0]['max_total_obtain_mark']; ?>
    							</td>
        					<?php
                            }
                            ?>
							
                        </tr>
                    <?php 
                      } 
					 ?>

				<?php
				$j++;
				endforeach;
				?>
				
				
				<?php
                if($exam[0]['is_annual_exam'] == 1){
                ?>
				<tr>
                    <td colspan="11" style="text-align:right;">
                        Grant Mark&nbsp;
                    </td>
                    <td>
                        <b><?php echo $row['grand_average_mark']; ?></b>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="11" style="text-align:right;">
                         Annual Position&nbsp;
                    </td>
                    <td>
                        <b><?php echo $row['annual_position']; ?></b>
                    </td>
                </tr>
				<?php } ?>
				
                </tbody>
            </table>
            <p style="font-size:13px;text-align:left;">
                 <b>
                 <br><br>&nbsp;&nbsp;............................................................<br>
                  &nbsp;&nbsp;Head of the Institute
                </b>
            </p>
            <p style="font-size:10px;text-align:right;"><b>Powered by: School360</b> &nbsp;&nbsp;</p>
            <br><br>
        </div>
    </div>
    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/certificate-bg.png" alt="" class="result-bg">
</div>
</div>
<div class="page-break">&nbsp;</div>
<?php
	$i++;
    endforeach;
?>
</div>
<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/arctext.js"></script>
<script>
    jQuery(document).ready(function () {
        var valB = jQuery('.Bengalicmn .avrg').html();
        var rowspanL = jQuery('.pagetble_middle .Bengalicmn').find('.hide').length;
        jQuery('.Bengali .avrgvl').parent().attr('rowspan', rowspanL + 1).children('.avrgvl').html(valB).css('font-weight', 'bold');
        jQuery('.pagetble_middle .Bengalicmn').find('.hide').hide();
        var valE = jQuery('.Englishcmn .avrg').html();
        var rowspanL = jQuery('.pagetble_middle .Englishcmn').find('.hide').length;
        jQuery('.English .avrgvl').parent().attr('rowspan', rowspanL + 1).children('.avrgvl').html(valE).css('font-weight', 'bold');
        jQuery('.pagetble_middle .Englishcmn').find('.hide').hide();
        jQuery('.instName').arctext({radius: 300});

    });
</script>
</body>
</html>