<?php
//echo SCHOOL_NAME; die;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
    
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/result.css">
</head>
<body class="scms-result-print">
<button class="printButton" onclick="CallPrint()">Print this page</button>
<style>
    .printButton {
        position: absolute;
        top: 20px;

        left: 20px;

        height: 30px;
    }
    .bottom_td{
        height:10px !important;
    }
     .top_box_td{
        height:5px !important;
    }
</style>

<script type="text/javascript">
    function CallPrint() {
        window.print();
    }
</script>

<?php //echo $is_process_bangla_english_separate; die; //echo '<pre>'; print_r($Result); die; ?>

<?php
    $i = 0;
    foreach ($Result as $row):
?>
<div id="printMarkSheet">
<div class="wraperResult">
    <div class="resHdr">
        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/marksheet_logo.png" alt="" class="resLogo">
        <div class="schoolIdentity" style="margin-top: -15px;">
            <h1> <?php echo $school_info[0]['school_name']; ?></h1>
            <div class="hdrText">
                <span><?php echo $exam[0]['name']; ?>, <?php echo $exam[0]['year']; ?></span>
            </div>
        </div>
                
    </div>

    <div class="resContainer">
        <div class="resTophdr">
            <div class="restopleft">
                <div><span>STUDENT ID</span><i>: </i><b><em>&nbsp;&nbsp;<?php echo $row['student_code']; ?></em></b></div>
                <div><span>NAME</span><i>: </i><b><em>&nbsp;&nbsp;<?php echo $row['student_name']; ?></em></b></div>
                <div><span>FATHER'S NAME</span><i>: </i><em>&nbsp; <?php echo $row['father_name']; ?></em></div>
                <div><span>MOTHER'S NAME</span><i>: </i><em>&nbsp; <?php echo $row['mother_name']; ?></em></div>
                <div><span>DATE OF BIRTH</span><i>: </i><em>&nbsp; <?php echo date("d-m-Y", strtotime($row['date_of_birth']));; ?></em></div>
            </div>

            <div class="restopleft rgttopleft">
                <div><span style="width: 60px; font-size: 12px;">CLASS</span><i>: </i><em> <?php echo $class_name; ?></em></div>
                <div><span style="width: 60px; font-size: 12px;">GROUP</span><i>: </i><em><?php echo $group_name; ?></em></div>
                <div><span style="width: 60px; font-size: 12px;">SECTION</span><i>: </i><em> <?php echo $section_name; ?></em></div>
                <div><span style="width: 60px; font-size: 12px;">ROLL NO</span><i>: </i><em> <?php echo $row['roll_no']; ?></em></div>
            </div>
        </div>
        


        <div class="resmidcontainer">
            <table class="pagetble_middle">
                <tbody>
                
				<tr>
				    <td colspan="<?php echo count($extra_head) + 10 + $is_annual_exam; ?>">
				        <h2 class="markTitle">Subject-Wise Grade &amp; Mark Sheet</h2>
				    </td>
				</tr>
                <tr>
                    <th class="res1" rowspan="2">&nbsp;CODE&nbsp;</th>
                    <th class="res2 cTitle" rowspan="2" style="min-width:200px;">SUBJECT</th>
                    <th colspan="<?php echo count($extra_head) + 8; ?>"><span><?php echo $exam[0]['name']; ?></th>
                </tr>
                <tr>
                    <td class="res1">&nbsp;Credit&nbsp;</td>
                    <td class="res1">&nbsp;Creative&nbsp;</td>
                    <td class="res1">&nbsp;Objective&nbsp;</td>
                    <td class="res1">&nbsp;Practical&nbsp;</td>
                    
                    <td class="res1">&nbsp;Total Mark <br> (<?php echo $row['calculable_amount']; ?>%)&nbsp;</td>
                    
                    <?php
                    foreach ($extra_head as $col_row):
                    ?>
                        <td class="res1">
                        <?php echo $col_row['head_name'] . '<br>('. $col_row['mark'] .')'; ?>
                        </td>
                    <?php endforeach; ?>
                    
                    <?php if($is_annual_exam == 1){ ?>
                    <td class="res1">&nbsp;1st and 2nd Semester (20%)&nbsp;</td>
                    <?php } ?>
                    
                    <td class="res1">&nbsp;Total Mark&nbsp;</td>
                    <td class="res1">&nbsp;Grade Point&nbsp;</td>
                    <td class="res1">&nbsp;Letter Grade&nbsp;</td>
                   
                 
                </tr>

				<?php
				$j = 0;
				$optinal_subject_found = 0;
				$optinal_subject_total_mark = 0;
				$total_credit_mark = 0;
			
				foreach ($Result[$i]['result'] as $result_row):
				?>

				  <tr <?php if ($result_row['is_optional'] == '1') { $optinal_subject_found = 1; $optinal_subject_total_mark = $result_row['total_obtain']; ?> style="background-color: lightyellow" <?php } ?>>            
					<td align="center">
						&nbsp;<?php echo $result_row['subject_code']; ?><br>
					</td>
					<td style="text-align:left;">
						&nbsp;<?php echo $result_row['subject_name']; ?>
					</td>
					<td align="center">
						<?php echo $result_row['credit']; ?>
						<?php
						$total_credit_mark = $total_credit_mark + $result_row['credit'];
						?>
					</td>
					<td align="center">
						<?php
						echo $result_row['written'];
						?>
					</td>
					<td align="center">
						<?php
						echo $result_row['objective'];
						?>
					</td>
					<td align="center">
						<?php
						echo $result_row['practical'];
						?>
					</td>
					
					<td align="center">&nbsp;
						<?php
						echo $result_row['calculable_total_obtain'];
						?>
					</td>
					
					<?php
                    foreach ($extra_head as $col_row):
                    ?>
                        <td  align="center">
                        <?php 
                         echo $result_row['extra_mark'][$col_row['head_id']]['mark'];
                        ?>
                        </td>
                    <?php endforeach; ?>
                    
                    <?php if($is_annual_exam == 1){ ?>
                       <td align="center">
                           <?php
    						echo $result_row['total_number_get_from_other_exam'];
    						?>
                       </td>
                    <?php } ?>
                    
                    <td align="center">
						<?php
						echo $result_row['total_obtain'];
						?>
					</td>
					
					<?php if ($is_process_bangla_english_separate == 1 && $result_row['subject_code'] == '101') { ?>
							<td style="vertical-align: middle;" rowspan="2">
								 <?php
								
								 echo number_format($result_row['numeric_gpa'],2);
								 ?>
							 </td>
							 
							 <td style="vertical-align: middle;" rowspan="2">
								 <?php
							
								 echo $result_row['alpha_gpa'];
								 ?>
							 </td>
					<?php } ?>
					
					<?php if ($is_process_bangla_english_separate == 1 && $result_row['subject_code'] == '107') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
					
				    <?php if (SCHOOL_NAME == 'wisdomcollege' && $result_row['subject_code'] == '174') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
					<?php if (SCHOOL_NAME == 'wisdomcollege' && $result_row['subject_code'] == '176') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
					<?php if (SCHOOL_NAME == 'wisdomcollege' && $result_row['subject_code'] == '178') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
					<?php if (SCHOOL_NAME == 'wisdomcollege' && $result_row['subject_code'] == '265') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
					<?php if (SCHOOL_NAME == 'wisdomcollege' && $result_row['subject_code'] == '239') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
					<?php if (SCHOOL_NAME == 'wisdomcollege' && $result_row['subject_code'] == '267') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
					<?php if (SCHOOL_NAME == 'wisdomcollege' && $result_row['subject_code'] == '109') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
					<?php if (SCHOOL_NAME == 'wisdomcollege' && $result_row['subject_code'] == '249') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
						<?php if (SCHOOL_NAME == 'wisdomcollege' && $result_row['subject_code'] == '269') { ?>
					    
					    <td style="vertical-align: middle" rowspan="2">
							 <?php 
							
							 echo number_format($result_row['numeric_gpa'],2); 
							 ?>
						</td>
						
						<td style="vertical-align: middle" rowspan="2">
							 <?php echo $result_row['alpha_gpa']; ?>
						</td>
					<?php } ?>
					
						<?php if ($is_process_bangla_english_separate == 0 || 
						($result_row['subject_code'] != '101' && $result_row['subject_code'] != '102' 
						&& $result_row['subject_code'] != '107' && $result_row['subject_code'] != '108'
						&& $result_row['subject_code'] != '174' && $result_row['subject_code'] != '175'
						&& $result_row['subject_code'] != '176' && $result_row['subject_code'] != '177'
						&& $result_row['subject_code'] != '178' && $result_row['subject_code'] != '179'
						&& $result_row['subject_code'] != '265' && $result_row['subject_code'] != '266'
						&& $result_row['subject_code'] != '239' && $result_row['subject_code'] != '240'
						&& $result_row['subject_code'] != '267' && $result_row['subject_code'] != '268'
						&& $result_row['subject_code'] != '109' && $result_row['subject_code'] != '110'
						&& $result_row['subject_code'] != '249' && $result_row['subject_code'] != '250'
						&& $result_row['subject_code'] != '269' && $result_row['subject_code'] != '270'
						)) { ?>
							<td>
							   <?php
							 
							   echo number_format($result_row['numeric_gpa'],2);
							   ?>
							</td>
					<?php } ?>
					
					<?php if ($is_process_bangla_english_separate == 0 || 
					($result_row['subject_code'] != '101' && $result_row['subject_code'] != '102'
					&& $result_row['subject_code'] != '107' && $result_row['subject_code'] != '108'
					&& $result_row['subject_code'] != '174' && $result_row['subject_code'] != '175'
					&& $result_row['subject_code'] != '176' && $result_row['subject_code'] != '177'
					&& $result_row['subject_code'] != '178' && $result_row['subject_code'] != '179'
					&& $result_row['subject_code'] != '265' && $result_row['subject_code'] != '266'
					&& $result_row['subject_code'] != '239' && $result_row['subject_code'] != '240'
					&& $result_row['subject_code'] != '267' && $result_row['subject_code'] != '268'
					&& $result_row['subject_code'] != '109' && $result_row['subject_code'] != '110'
					&& $result_row['subject_code'] != '249' && $result_row['subject_code'] != '250'
					&& $result_row['subject_code'] != '269' && $result_row['subject_code'] != '270'
					)) { ?>
							<td>
							   <?php echo $result_row['alpha_gpa']; ?>
							</td>
					<?php } ?>
					
				

				<?php
				$j++;
				endforeach;
				?>
				
				<tr>
				    <td colspan="<?php echo count($extra_head) + 7 + $is_annual_exam; ?>" style="text-align:right; font-weight:bold;">Total &nbsp;</td>
				   
				    <td style="text-align:center; font-weight:bold;"><?php echo $row['total_obtain_mark']; ?></td>
				     <td style="text-align:center; font-weight:bold;">
				        <?php 
				          echo $row['gpa_with_optional'];
				        ?>
				    </td>
				    <td style="text-align:center; font-weight:bold;"><?php echo $row['c_alpha_gpa_with_optional']; ?></td>
				</tr>
				
				<tr>
				    <td style="border-right:1px black solid;" colspan="<?php echo count($extra_head) + 10 + $is_annual_exam; ?>">
				         <table width="50%" style="border:0px; padding:5px;">
				             <tr>
				                 <td width="100%" style="border:0px; vertical-align:top;" >
				                     <table width="100%" style="border:0px;" >
                                            <tr>
                                                <td colspan="2" class="bottom_td" style="border:1px black solid;">
                                                <b>Result Others Details</b>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td width="25%" class="bottom_td"  style="border:1px black solid;">
                                                Total Credit Mark
                                                </td>
                                                <td width="25%" class="bottom_td"  style="border:1px black solid;">
                                                <b><?php echo $total_credit_mark; ?></b>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td width="25%" class="bottom_td"  style="border:1px black solid;">
                                                Total Obtained Mark
                                                </td>
                                                <td width="25%" class="bottom_td"  style="border:1px black solid;">
                                                <b><?php echo $row['total_obtain_mark']; ?></b>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td width="25%" class="bottom_td"  style="border:1px black solid;">
                                                Total Obtained Mark <br> (Without 40 Optional)
                                                </td>
                                                <td width="25%" class="bottom_td"  style="border:1px black solid;">
                                                <b>
                                                <?php 
                                                     if($optinal_subject_found == 1){
                            				              if($optinal_subject_total_mark >= 40){
                            				                  echo ($row['total_obtain_mark'] - 40);
                            				              }else{
                            				                  echo ($row['total_obtain_mark'] - $optinal_subject_total_mark);
                            				              }
                            				          }else{
                            				              echo $row['total_obtain_mark'];
                            				          }
                                                ?>
                                                </b>
                                                </td>
                                            </tr>
                                            
                                            
                                             
                                            
                                            
                                            <tr>
                                                <td width="25%"  class="bottom_td" style="border:1px black solid;">
                                                GPA (With Optional)
                                                </td>
                                                <td width="25%" class="bottom_td"  style="border:1px black solid;">
                                                <b><?php echo $row['gpa_with_optional']; ?></b>
                                                </td>
                                            </tr>
                                            
                                             <tr>
                                                <td width="25%"  class="bottom_td" style="border:1px black solid;">
                                                GPA (Without Optional)
                                                </td>
                                                <td width="25%"  class="bottom_td" style="border:1px black solid;">
                                                <b><?php echo $row['gpa_without_optional']; ?></b>
                                                </td>
                                            </tr>
                                            
                                            
                                            <tr>
                                                <td width="25%"  class="bottom_td" style="border:1px black solid;">
                                                 Position
                                                </td>
                                                <td width="25%" class="bottom_td"  style="border:1px black solid;">
                                                <b><?php echo $row['position']; ?></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="25%"  class="bottom_td" style="border:1px black solid;">
                                                1st Place Mark
                                                </td>
                                                <td width="25%"  class="bottom_td" style="border:1px black solid;">
                                                <b><?php echo $max_total_obtain_mark[0]['max_total_obtain_mark']; ?></b>
                                                </td>
                                            </tr>
				                     </table>
				                 </td>
				             </tr>
				        </table>
				    </td>
				</tr>
				
				<tr>
				    <td style="height:80px;  vertical-align: bottom" colspan="<?php echo count($extra_head) + 14 + $is_annual_exam; ?>">
				        <table width="100%" style="border:0px;">
				            <tr>
				                <td width="50%" style="border:0px;">
				                    <div style="margin-top: 25px;">
				                    <b>
                                    &nbsp;&nbsp;............................................................<br>
                                    &nbsp;&nbsp;Class Teacher Signature<br>
                                    </b>
                                    </div>
				                </td>
				                <td width="50%" style="border:0px;">
				                    <img  style="height:60px; margin-top: -5px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/marksheet_signature.png" alt="">
				                    <div style="margin-top: -20px;">
				                    <b>
                                    &nbsp;&nbsp;............................................................<br>
                                    &nbsp;&nbsp;Principal Signature<br>
                                    </b>
                                    </div>
				                </td>
				            </tr>
				        </table>
				    </td>
				</tr>
	
				
                </tbody>
            </table>
            
            
            <p style="font-size:13px;text-align:right;margin-top: -15px;">
                 <b>
                  <p style="font-size:10px;text-align:right;"><b>Powered by: School360</b> &nbsp;&nbsp;</p>
                </b>
            </p>
            <br></br>
            
        </div>
    </div>
    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/certificate-bg.png" alt="" class="result-bg">
</div>
 
<div class="page-break">&nbsp;</div>
<?php
	$i++;
    endforeach;
?>
</div>
<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/marksheet_css/arctext.js"></script>
<script>
    jQuery(document).ready(function () {
        var valB = jQuery('.Bengalicmn .avrg').html();
        var rowspanL = jQuery('.pagetble_middle .Bengalicmn').find('.hide').length;
        jQuery('.Bengali .avrgvl').parent().attr('rowspan', rowspanL + 1).children('.avrgvl').html(valB).css('font-weight', 'bold');
        jQuery('.pagetble_middle .Bengalicmn').find('.hide').hide();
        var valE = jQuery('.Englishcmn .avrg').html();
        var rowspanL = jQuery('.pagetble_middle .Englishcmn').find('.hide').length;
        jQuery('.English .avrgvl').parent().attr('rowspan', rowspanL + 1).children('.avrgvl').html(valE).css('font-weight', 'bold');
        jQuery('.pagetble_middle .Englishcmn').find('.hide').hide();
        jQuery('.instName').arctext({radius: 300});

    });
</script>
</body>
</html>