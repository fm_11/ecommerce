<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title><?php  echo $title; ?></title>

	<!-- Normalize or reset CSS with your favorite library -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/normalize.min.css">

	<!-- Load paper.css for happy printing -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/paper.css">

	<!-- Set page size here: A5, A4 or A3 -->
	<!-- Set also "landscape" if you need -->
	<style>
		@page { size: A4 }
		.main_content {
			width: 100%;
			height: 100%;
			box-sizing: border-box;
			border: 5px #538cc6 solid;
		}

		.body_content{
			width: 100%;
			height: 100%;
			box-sizing: border-box;
			border: 5px #ff8080 solid;
		}

		#header{
			width:100%;
			box-sizing: border-box;
			height: 180px;
		}
		.header-left {
			width:55%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: center;
			line-height: 0.3;
		}
		.logo {
			width:20%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: center;
		}
		.header-right {
			width:25%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: center;
		}
		.clr{ clear:both;}


		.frame{
			width: 100%;
			height: 100%;
			margin: auto;
			position: relative;
		}
		.log_frame{
			width: 100%;
			height: 100px;
			margin: auto;
			position: relative;
		}
		.img{
			max-height: 100%;
			max-width: 100%;
			position: absolute;
			top: 10px;
			bottom: 0;
			left: 0;
			right: 0;
			margin: auto;
		}

		.gpa_table {
			border-collapse: collapse;
			margin: 0 auto;
			width: 90%;
			height: auto;
			position: relative;
			top:30px;
		}

		table, td, th {
			border: 1px solid black;
			font-size: 13px;
			font-weight: 500;
		}

		#student_info{
			width:100%;
			box-sizing: border-box;
			height: 200px;
		}

		#summary_info{
			width:100%;
			box-sizing: border-box;
			height: auto;
		}

		.summery_info-left {
			width:25%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: left;
			margin-top: 13px;
			margin-left: 5px;
			padding: 0px;
		}

		.summery_info-middle {
			width:50%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: left;
			margin-top: 13px;
			margin-left: 5px;
			padding: 0px;
		}

		.summery_info-right {
			width:25%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: left;
			margin-top: 13px;s
		margin-left: 5px;
			padding: 0px;
		}

		.summery_info-right1{
			width:20%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: left;
			margin-top: 30px;
			margin-left: 20px;
			padding: 0px;
		}

		.student_info-left {
			width:100%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: left;
		}

		.student_info_table {
			border-collapse: collapse;
			margin: 0 auto;
			width: 98%;
			height: auto;
			position: relative;
		}

		#student_exam_info{
			width:100%;
			box-sizing: border-box;
			height: auto;

		}

		.exam_info-left {
			width:100%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: left;
		}

		.mark_info_table{
			border-collapse: collapse;
			margin: 0 auto;
			margin-top:10px;
			width: 98%;
			height: auto;
			position: relative;
		}

		.subject_info_table{
			border-collapse: collapse;
			margin: 0 auto;
			width: 100%;
			height: auto;
			position: relative;
		}

		.td_center{
			text-align: center;
			padding: 1px;
			padding-top: 3px;
			padding-bottom: 3px;
			font-weight: 500;
		}

		.td_right{
			text-align: right;
			padding: 1px;
			padding-top: 3px;
			padding-bottom: 3px;
			font-weight: 500;
		}
		.td_left{
			text-align: left;
			padding: 1px;
			padding-top: 3px;
			padding-bottom: 3px;
			font-weight: 500;
		}

		.td_backgroud{
			background-color: #d9e6f2;
			font-weight: bold;
		}

		.signature_info{
			width:100%;
			box-sizing: border-box;
			height: 100px;
			bottom: 30px;
			position: absolute;
		}
		.signature_left {
			width:33%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: center;
			vertical-align: bottom;
		}
		.signature_middle {
			width:34%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: center;
			vertical-align: bottom;
		}
		.signature_right {
			width:33%;
			float:left;
			height: 100%;
			box-sizing: border-box;
			text-align: center;
			vertical-align: bottom;
		}

	</style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->


<?php
$i = 0;
foreach ($Result as $row):
?>
	<section class="sheet padding-5mm">

		<!-- Write HTML just like a web page -->
		<div class="main_content">
			<div class="body_content">
				<div id="header">
					<div class="logo">
						<div class="frame">
							<img class="img" src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $row['photo']; ?>" width="120px" height="130px" />
						</div>
					</div>
					<div class="header-left">
						<h3 style="margin-top: 20px;"><?php echo $school_info[0]['school_name']; ?></h3>
						<span><?php echo $school_info[0]['address']; ?></span>
						<div class="log_frame">
							<img class="img" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info[0]['picture']; ?>" width="80px" height="90px" />
						</div>
						<span style="letter-spacing: 3px;line-height: 20px;
                          border-bottom: 2px solid #009933;">
							Progress Report
						</span>
					</div>
					<div class="header-right">
						<table class="gpa_table">
							<tr>
								<th>Range</th>
								<th>Grade</th>
								<th>GPA</th>
							</tr>
							<tr>
								<td>80-100</td>
								<td>A+</td>
								<td>5.0</td>
							</tr>
							<tr>
								<td>70-79</td>
								<td>A</td>
								<td>4.0</td>
							</tr>
							<tr>
								<td>60-69</td>
								<td>A-</td>
								<td>3.5</td>
							</tr>
							<tr>
								<td>50–59</td>
								<td>B</td>
								<td>3.0</td>
							</tr>
							<tr>
								<td>40–49</td>
								<td>C</td>
								<td>2.0</td>
							</tr>
							<tr>
								<td>33–39</td>
								<td>D</td>
								<td>1.0</td>
							</tr>
							<tr>
								<td>0–32</td>
								<td>F</td>
								<td>0.0</td>
							</tr>
						</table>
					</div>

				</div>

				<div class="student_info">
					<div class="student_info-left">
						<table class="student_info_table" style="border-style : hidden!important;">
							<tr>
								<td style="border: none;" class="td_left" width="15%">Name of Student</td>
								<td style="border: none;" class="td_center" width="5%">:</b></td>
								<td style="border: none;" class="td_left" colspan="4"  width="80%"><b><?php echo $row['student_name']; ?></b></td>
							</tr>
							<tr>
								<td style="border: none;" class="td_left" width="15%">Father's Name</td>
								<td style="border: none;" class="td_center" width="5%">:</b></td>
								<td style="border: none;" class="td_left" colspan="4"  width="80%"><b><?php echo $row['father_name']; ?></b></td>
							</tr>
							<tr>
								<td style="border: none;" class="td_left" width="15%">Mother's Name</td>
								<td style="border: none;" class="td_center" width="5%">:</b></td>
								<td style="border: none;" class="td_left" colspan="4" width="80%"><b><?php echo $row['mother_name']; ?></b></td>
							</tr>
							<tr>
								<td style="border: none;" class="td_left" width="15%">Student ID</td>
								<td style="border: none;" class="td_center" width="5%">:</b></td>
								<td style="border: none;" class="td_left" width="30%"><b>&nbsp;<?php echo $row['student_code']; ?></b></td>

								<td style="border: none;" class="td_left" width="15%">Exam</td>
								<td style="border: none;" class="td_center" width="5%">:</b></td>
								<td style="border: none;" class="td_left" width="30%"><b><?php echo $exam[0]['name']; ?></b></td>
							</tr>

							<tr>
								<td style="border: none;" class="td_left" width="15%">Roll No.</td>
								<td style="border: none;" class="td_center" width="5%">:</b></td>
								<td style="border: none;" class="td_left" width="30%"><b><?php echo $row['roll_no']; ?></b></td>

								<td style="border: none;" class="td_left" width="15%">Year</td>
								<td style="border: none;" class="td_center" width="5%">:</b></td>
								<td style="border: none;" class="td_left" width="30%"><b><?php echo $exam[0]['year']; ?></b></td>
							</tr>


							<tr>
								<td style="border: none;" class="td_left" width="15%">Class</td>
								<td style="border: none;" class="td_center" width="5%">:</b></td>
								<td style="border: none;" class="td_left" width="30%"><b>&nbsp;<?php echo $class_name . '-' . $section_name . '-' . $shift_name; ?></b></td>

								<td style="border: none;" class="td_left" width="15%">Group</td>
								<td style="border: none;" class="td_center" width="5%">:</b></td>
								<td style="border: none;" class="td_left" width="30%"><b><?php echo $group_name; ?></b></td>
							</tr>
						</table>
					</div>
				</div>

				<div class="student_exam_info">
					<div class="exam_info-left">
						<table class="mark_info_table">
							<tr>
<!--								<th class="td_center td_backgroud" rowspan="2">CODE</th>-->
								<th class="td_center td_backgroud" rowspan="2">SUBJECT</th>
								<th class="td_center td_backgroud" colspan="10"><span><?php echo $exam[0]['name']; ?></th>
							</tr>
							<tr>
								<th class="td_center td_backgroud">Full <br> Marks</th>
								<th class="td_center td_backgroud">Highest<br> Marks</th>
								<th class="td_center td_backgroud"><?php echo $main_marking_heads->class_test; ?></th>
								<th class="td_center td_backgroud"><?php echo $main_marking_heads->written; ?></th>
								<th class="td_center td_backgroud"><?php echo $main_marking_heads->mcq; ?></th>
								<th class="td_center td_backgroud"><?php echo $main_marking_heads->practical; ?></th>
								<th class="td_center td_backgroud">Total<br> Marks</th>
								<th class="td_center td_backgroud">Letter <br>Grade</th>
								<th class="td_center td_backgroud">Grade<br> Point</th>
							</tr>

							<?php
							$j = 0;
							$is_merge_row_number = 0;
							$already_merge_for = array();
							foreach ($Result[$i]['result'] as $result_row):
								?>
								<tr <?php if ($result_row['is_optional'] == '1') { ?> style="background-color: #c6ecd9;" <?php } ?>>
<!--									<td class="td_center">-->
<!--										&nbsp;--><?php //echo $result_row['subject_code']; ?><!--<br>-->
<!--									</td>-->

									<td style="text-align:left;">
										&nbsp;<?php
										    $optional_text = "";
										    $uncountable_text = "";
										    if ($result_row['is_optional'] == '1') { $optional_text = " (4th)"; }
										    if ($result_row['subject_type'] == 'UNC') { $uncountable_text = " (UC)"; }
										    echo $result_row['subject_name'] . $optional_text  . $uncountable_text;
										 ?>
									</td>

									<td  class="td_center" style="font-weight: 600;">&nbsp;
										<?php
										echo round($result_row['credit']);
										?>
									</td>

									<td class="td_center">
										<?php echo round($highest_marks[$result_row['subject_code']], 2); ?>
									</td>

									<td  class="td_center">&nbsp;
										<?php
										echo round($result_row['class_test'], 2);
										?>
									</td>
									<td  class="td_center">&nbsp;
										<?php
										echo round($result_row['written'], 2);
										?>
									</td>
									<td  class="td_center">&nbsp;
										<?php
										echo round($result_row['objective'], 2);
										?>
									</td>
									<td  class="td_center">&nbsp;
										<?php
										echo round($result_row['practical'], 2);
										?>
									</td>
									<td  class="td_center" style="font-weight: 600;">&nbsp;
										<?php
										echo round($result_row['calculable_total_obtain'], 2);
										?>
									</td>

									<?php if ($result_row['merge_code'] > 0 && array_search($result_row['subject_id'], $already_merge_for) == '') { ?>
										<td style="vertical-align: middle; font-weight: 600;"  class="td_center" rowspan="2">
											<?php
											echo $result_row['alpha_gpa'];
											?>
										</td>
									<?php } ?>


									<?php
									if($result_row['is_absent'] == '1'){
										?>
										<td colspan="2" class="td_center" style="vertical-align: middle; font-weight: 600;" >
											Absent
										</td>
										<?php
									}else{
										?>
										<?php if ($result_row['merge_code'] == 0) { ?>
											<td class="td_center" style="vertical-align: middle; font-weight: 600;" >
												<?php
												echo $result_row['alpha_gpa'];
												?>
											</td>
										<?php } ?>

										<?php
										if ($result_row['merge_code'] > 0 && array_search($result_row['subject_id'], $already_merge_for) == '') {
											$subject_id = $result_row['subject_id'];
											$merge_subject_id = $result_row['merge_subject_id'];
											$already_merge_for[] = $subject_id;
											$already_merge_for[] = $merge_subject_id;
											?>
											<td style="vertical-align: middle; font-weight: 600;"   class="td_center" rowspan="2">
												<?php echo round($result_row['numeric_gpa'], 2); ?>
											</td>
										<?php } ?>



										<?php if ($result_row['merge_code'] == 0) { ?>
											<td class="td_center" style="vertical-align: middle; font-weight: 600;" >
												<?php

												echo round($result_row['numeric_gpa'], 2);
												?>
											</td>
										<?php } ?>
										<?php
									}
									?>



								</tr>

								<?php
								$j++;
							endforeach;
							?>

							<tr style="background-color: #d9e6f2; color: #003300;">
								<td style="text-align:right; font-weight:bold;">Total Exam Marks&nbsp;</td>
								<td style="text-align:center; font-weight:bold;">
									<?php
									echo round($row['total_credit'], 2);
									?>
								</td>

								<td style="text-align:center; font-weight:bold;" colspan="5">
									Obtained Marks & GPA
								</td>
								<td style="text-align:center; font-weight:bold;">
									<?php
									echo round($row['total_obtain_mark'], 2);
									?>
								</td>
								<td style="text-align:center; font-weight:bold;">
									<?php
									echo $row['c_alpha_gpa_with_optional'];
									?>
								</td>
								<td style="text-align:center; font-weight:bold;">
									<?php
									echo round($row['gpa_with_optional'], 2);
									?>
								</td>
							</tr>


						</table>
					</div>
				</div>

				<div class="summary_info">
					<div class="summery_info-left">
						<table class="student_info_table">
							<tr>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">
									Result Status
								</th>
								<th class="td_center" style="color:green;padding-left:5px;font-weight:bold;">
									<?php
									if($row['c_alpha_gpa_with_optional'] == 'F') {
										echo 'Failed';
									}else{
										echo 'Passed';
									}
									 ?>
								</th>
							</tr>

							<tr>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">
									Class Position
								</th>
								<th class="td_center" style="padding-left:5px;font-weight:bold;">
									<?php echo $row['class_position']; ?>
								</th>
							</tr>

							<tr>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">
									GPA (Without 4th)
								</th>
								<th class="td_center" style="padding-left:5px;font-weight:bold;">
									<?php
									echo round($row['gpa_without_optional'], 2);
									?>
								</th>
							</tr>

							<tr>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">
									Failed Subject (S)
								</th>
								<th class="td_center" style="padding-left:5px;font-weight:bold;">
									<?php
									echo $row['number_of_failed_subject'];
									?>
								</th>
							</tr>

							<tr>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">
									Working Days
								</th>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">

								</th>
							</tr>

							<tr>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">
									Total Present
								</th>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">

								</th>
							</tr>

							<tr>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">
									Total Absent
								</th>
								<th class="td_left" style="padding-left:5px;font-weight:bold;">

								</th>
							</tr>

						</table>
					</div>


					<div class="summery_info-middle">
						<table class="student_info_table">
							<tr>
								<td style="width:50%;font-weight:bold;">
									<table class="student_info_table">
										<tr>
											<th class="td_center" colspan="2">
												<b>Moral & Behavior Evaluation</b>
											</th>
										</tr>
										<tr>
											<th style="width:20% !important;font-weight:600;" class="td_center">
												&nbsp;
											</th>
											<th style="font-weight:600;">
												Excelent
											</th>
										</tr>

										<tr>
											<th style="width:20% !important;font-weight:600;" class="td_center">
												&nbsp;
											</th>
											<th style="font-weight:600;">
												Good
											</th>
										</tr>

										<tr>
											<th style="width:20% !important;font-weight:600;" class="td_center">
												&nbsp;
											</th>
											<th style="font-weight:600;">
												Average
											</th>
										</tr>

										<tr>
											<th style="width:20% !important;font-weight:600;" class="td_center">
												&nbsp;
											</th>
											<th style="font-weight:600;">
												Poor
											</th>
										</tr>
									</table>
								</td>
								<td style="width:50%">
									<table class="student_info_table">
										<tr>
											<th style="font-weight:600;" class="td_center" colspan="2">
												<b>Co-Curricular Activities</b>
											</th>
										</tr>
										<tr>
											<th style="width:20% !important;font-weight:600;" class="td_center">
												&nbsp;
											</th>
											<th style="font-weight:600;">
												Sports
											</th>
										</tr>

										<tr>
											<th style="width:20% !important;font-weight:600;" class="td_center">
												&nbsp;
											</th>
											<th style="font-weight:600;">
												Cultural Function
											</th>
										</tr>

										<tr>
											<th style="width:20% !important;font-weight:600;" class="td_center">
												&nbsp;
											</th>
											<th style="font-weight:600;">
												Scout/BNCC
											</th>
										</tr>

										<tr>
											<th style="width:20% !important;font-weight:600;" class="td_center">
												&nbsp;
											</th>
											<th style="font-weight:600;">
												Math Olympiad
											</th>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="height:60px; vertical-align:top;">
									<b><u>Comments:</u></b>
								</td>
							</tr>
						</table>

					</div>

					<div class="summery_info-right1">
						<table style="border:0px;">
							<tr>
								<td>
									<img style="position: relative !important;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/QR_CODE/<?php echo $row['qr_code']; ?>" width="120px" height="130px" />
								</td>
							</tr>
						</table>
					</div>
				</div>


				<div class="signature_info">
					<div class="signature_left">
						<?php  if (isset($signature['L'])) {
							if ($signature['L']['is_use'] == '1') { ?>

								<img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['L']['location']; ?>" height="60" width="75"/>
								<br>................................<br>
								<span><?php echo $signature['L']['level']; ?></span>

							<?php }
						} ?>&nbsp;
					</div>
					<div class="signature_middle">
						<?php  if (isset($signature['M'])) {
							if ($signature['M']['is_use'] == '1') { ?>
								<img  style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['M']['location']; ?>"  height="60" width="75"/>
								<br>................................<br>
								<span><?php echo $signature['M']['level']; ?></span>
							<?php }
						} ?>
						&nbsp;
					</div>
					<div class="signature_right">
						<?php  if (isset($signature['R'])) {
							if ($signature['R']['is_use'] == '1') { ?>
								<img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['R']['location']; ?>" height="60" width="75"/>
								<br>................................<br>
								<span><?php echo $signature['R']['level']; ?></span>
							<?php }
						} ?>&nbsp;
					</div>
				</div>
				<span style="font-size: 12px;
    float: left;
    bottom: 30px;
    margin-left: -125px;
    position: absolute;
    width: 100%;"><b>Powered By: School360 </b>&nbsp;</span>


			</div>
		</div>
	</section>
	<?php
	$i++;
endforeach;
 ?>

</body>

</html>
