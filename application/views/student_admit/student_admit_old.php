<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $title; ?></title>

    <style>
        table, td, th {
            border: 0px solid black;
            text-align: left;

        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        .box {
            border: 1px solid black;
        }

        th, td {
            padding: 0px;
        }

        .page_breack {
            page-break-after: always;

        }

    </style>
</head>
<body>
<?php
if ($is_pdf_request != "Download") {
    ?>
    <script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript">

        function PrintElem(elem) {
            Popup($(elem).html());
        }

        function Popup(data) {
            var mywindow = window.open('', 'my div', 'height=400,width=600');
            mywindow.document.write('<html><head><title>my div</title>');
            /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            mywindow.print();
            mywindow.close();

            return true;
        }

    </script>
    <table width="100%" style="color:#<?php echo $admit_card_text_color; ?>;" align="right" bgcolor="#<?php echo $admit_card_background_color; ?>" cellpadding="0" cellspacing="0" id="box-table-a"
           summary="Employee Pay Sheet">
        <tr>
            <td>
                <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>students/print_admit" method="post"
                      enctype="multipart/form-data">
                    <input type="hidden" name="txtClass" value="<?php echo $class_id; ?>">
                    <input type="hidden" name="txtSection" value="<?php echo $section_id; ?>">
                    <input type="hidden" name="txtGroup" value="<?php echo $group; ?>">
                    <input type="hidden" name="txtExam" value="<?php echo $exam; ?>">
                    <input type="hidden" name="txtStudent" value="<?php echo $student_id; ?>">
                    <input type="hidden" name="is_pdf_request" value="Download">
                    <input type="button" class="button" value="Print" onclick="PrintElem('#mydiv')"/>
                    <input type="submit" class="submit" value="Download PDF">
                </form>
            </td>
        </tr>
    </table>
    <?php
}
?>
<div id="mydiv">
    <?php
    $i = 1;
    foreach ($list as $row):
        ?>
        <table class="box" bgcolor="#<?php echo $admit_card_background_color; ?>" align="center" style="margin-bottom:10px; margin-top:10px;color:#<?php echo $admit_card_text_color; ?>;">
            <tr>
                <td>
                    <?php
                    if ($row['photo'] == '') {
                        $image = "default.jpg";
                    } else {
                        $image = $row['photo'];
                    }
                    ?>
                    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $image; ?>" align="center"
                         style="width: 130px; height: 150px;">
                </td>
                <td colspan="3" style="text-align: center; padding-right: 50px;">
                    <span style="font-size: 16px;">
                    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/admit-logo.png" align="center">
                    </span><br>
                     <span style="font-size: 14px;">EIIN: <?php echo $school_info[0]['eiin_number']; ?></span><br>
                    <span style="font-size: 14px;"><b><?php echo $exam; ?></b></span><br>
                    <span style="font-size: 15px;"><b>Admit Card</b></span><br>

                    <span style="font-size: 14px;"><?php echo $school_info[0]['web_address']; ?></span>
                </td>
            </tr>

            <tr>
                <td class="box" width="20%" style="text-align: right;">
                    Student ID:&nbsp;
                </td>
                <td class="box" width="50%">
                    <b>&nbsp;<?php echo $row['student_code']; ?></b>
                </td>
                <td class="box" width="15%" style="text-align: right;">
                    Class:&nbsp;
                </td>
                <td class="box" width="15%">
                    <b>&nbsp;<?php echo $row['class_name']; ?></b>
                </td>
            </tr>

            <tr>
                <td class="box" style="text-align: right;">
                    Name:&nbsp;
                </td>
                <td class="box">
                    <b>&nbsp;<?php echo $row['name']; ?></b>
                </td>
                <td class="box" style="text-align: right;">
                    Section:&nbsp;
                </td>
                <td class="box">
                    <b>&nbsp;<?php echo $row['section_name']; ?></b>
                </td>
            </tr>
            <tr>
                <td class="box" style="text-align: right;">
                    Father's Name:&nbsp;
                </td>
                <td class="box">
                    <b>&nbsp;<?php echo $row['father_name']; ?></b>
                </td>
                <td class="box" style="text-align: right;">
                    Roll No.:&nbsp;
                </td>
                <td  class="box">
                    <b>&nbsp;<?php echo $row['roll_no']; ?></b>
                </td>
            </tr>
            <tr>
                <td class="box" style="text-align: right;">
                    Mother's Name:&nbsp;
                </td>
                <td  class="box">
                    <b>&nbsp;<?php echo $row['mother_name']; ?></b>
                </td>
                <td class="box" style="text-align: right;">
                   Shift:&nbsp;
                </td>
                <td class="box">
                    <b>&nbsp;<?php echo $row['shift_name']; ?></b>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <b> &nbsp;N.B.: </b> Student without admit card would not be allowed to participate in the exam. List
                    of student and routine will be published on website.<br>
                    <span style="font-size: 11px;"><b>&nbsp;Powered by: School360</b></span>
                </td>
                <td style="text-align: right;" colspan="2">
                     <br>
                      .......................................<br>
                      <b>Principal Signature &nbsp;</b>
                </td>
            </tr>
        </table>
		<br>

        <?php
        if ($i % 3 == 0) {
            echo '<div class="page_breack">&nbsp;</div>';
        }
        $i++; endforeach;
    ?>
</div>
</body>
</html>
