<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title><?php echo $title; ?></title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/paper.css">


<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_css/bootstrap-colorpicker.min.css">
<script src="<?php echo base_url(); ?>core_media/report_css/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>core_media/report_css/bootstrap-colorpicker.js"></script>


  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>
@media print
{
    .no-print, .no-print *
    {
        display: none !important;
    }
}
  @page { size: A4 }
  .main_content {
    width: 100%;
	  height: 31%;
    box-sizing: border-box;
	border: 6px <?php echo $admit_design->main_first_border_color; ?> solid;
	background-color: <?php echo $admit_design->background_color; ?>;
  }


  .body_content{
    width: 100%;
	height: 100%;
    box-sizing: border-box;
	border: 6px <?php echo $admit_design->main_second_border_color; ?> solid;
  }

    #header{
	 width:100%;
	 box-sizing: border-box;
	 height: 128px;
	 background-color: <?php echo $admit_design->header_color; ?>;
	}
	.header-left {
		width:60%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
		line-height: 1;
	}
	.logo {
		width:20%;
		float:left;
		height: 100%;
		box-sizing: border-box;
        text-align: center;
	}
	.header-right {
		width:20%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
	}
	.clr{ clear:both;}


   .frame{
        width: 100%;
        height: 100%;
        margin: auto;
        position: relative;
    }
    .log_frame{
        width: 80%;
        height: 75px;
        margin: auto;
        position: relative;
    }
    .img{
        max-height: 75%;
        max-width: 60%;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
    }

	.gpa_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 90%;
	  height: auto;
	  position: relative;
	  top:10px;
	}

	table, td, th {
	  border: 1px solid black;
    font-size: 14px;
    padding: 2px;
	}

	#student_info{
	 width:100%;
	 box-sizing: border-box;
	 height: 200px;

	}

	.student_info-left {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;
    margin-top: 40px !important;
    margin-bottom: -10px;
	}

	.student_info_table {
	  border-collapse: collapse;
	  margin: 0 auto;
	  width: 98%;
	  height: auto;
	  position: relative;
	}

	#student_exam_info{
	 width:100%;
	 box-sizing: border-box;
	 height: auto;

	}

  #signature_info{
    width:100%;
   	box-sizing: border-box;
   	height: 100px;
  }


  .signature_left {
		width:33%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
    vertical-align: bottom;
	}
	.signature_middle {
		width:34%;
		float:left;
		height: 100%;
		box-sizing: border-box;
    text-align: center;
    vertical-align: bottom;
	}
	.signature_right {
		width:33%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: center;
    vertical-align: bottom;
	}


	.exam_info-left {
		width:100%;
		float:left;
		height: 100%;
		box-sizing: border-box;
		text-align: left;

	}

	.mark_info_table{
	  border-collapse: collapse;
	  margin: 0 auto;
	  margin-top:10px;
	  width: 98%;
	  height: auto;
	  position: relative;
	}

	.td_center{
	  text-align: center;
	}

.icon-bar {
  position: fixed;
  top: 40%;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}

.icon-bar a {
  display: block;
  text-align: center;
  padding: 10px;
  transition: all 0.3s ease;
  color: white;
  font-size: 13px;
  text-decoration:none;
  width:150px;
}

.icon-bar a:hover {
  background-color: #000;
}

.facebook {
  background: #3B5998;
  color: white;
}

.twitter {
  background: #55ACEE;
  color: white;
}

.google {
  background: #dd4b39;
  color: white;
  font-size: 9px;
}

.linkedin {
  background: #007bb5;
  color: white;
  font-size: 9px;
}

.youtube {
  background: #bb0000;
  color: white;
}

.save_button {
	background-color: #4CAF50; /* Green */
	border: none;
	color: white;
	padding: 12px 28px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	border-radius: 5px;
	margin: 4px 2px;
	cursor: pointer;
}


  </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4" style="color: <?php echo $admit_design->text_color; ?>">

<form target="_blank" action="<?php echo base_url(); ?>admit_designs/index" method="post">
	<div class="icon-bar no-print">
	  <a href="#" id="background-color-picker" class="facebook">Background Color</i></a>
	  <a href="#" id="header-color-picker" class="twitter">Header Color</a>
	  <a href="#" id="text-color-picker" class="google">Text Color</a>
	  <a href="#" id="1st-border-color-picker" class="linkedin">Main 1st Border</i></a>
	  <a href="#" id="2nd-border-color-picker" class="youtube">Main 2nd Border</a>
	  <input type="submit" class="save_button" value="Save Your Data">
	</div>
	<input type="hidden" required name="id" value="<?php echo $admit_design->id; ?>">
	<input type="hidden" required name="design_type" value="<?php echo $admit_design->design_type; ?>">
	<input type="hidden" id="background_color" value="<?php echo $admit_design->background_color; ?>" name="background_color">
	<input type="hidden" id="header_color" value="<?php echo $admit_design->header_color; ?>" name="header_color">
	<input type="hidden" id="text_color" value="<?php echo $admit_design->text_color; ?>" name="text_color">
	<input type="hidden" id="main_first_border_color" value="<?php echo $admit_design->main_first_border_color; ?>" name="main_first_border_color">
	<input type="hidden" id="main_second_border_color" value="<?php echo $admit_design->main_second_border_color; ?>" name="main_second_border_color">
</form>

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
    <section class="sheet padding-5mm">
  <?php
  $i = 1;
  foreach ($list as $row):
      ?>


    <!-- Write HTML just like a web page -->

    <div class="main_content" id="main_content">
	    <div class="body_content" id="body_content">
		   <div id="header" class="header">
			 <div class="logo">
					<div class="frame">
            <?php
            if ($row['photo'] == '' || $row['photo'] == 'default.png') {
            ?>
            <img  class="img" src="<?php echo base_url(); ?>/core_media/images/default.png" align="center"
                 width="120px" height="130px">
            <?php
            } else {
              ?>
              <img class="img"  src="<?php echo base_url() . MEDIA_FOLDER; ?>/student/<?php echo $row['photo']; ?>" align="center"
                    width="120px" height="130px">
              <?php
            }
            ?>
					</div>
			 </div>
			 <div class="header-left">
			    <h3 style="margin-bottom: 5px;margin-top: 7px;"><?php echo $school_info[0]['school_name']; ?></h3>
  				<span><?php echo $school_info[0]['address']; ?></span><br>
          <span><?php echo $school_info[0]['web_address']; ?></span>
				<div class="log_frame">
				    <img class="img" style="margin-top: 5px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/logos/<?php echo $school_info[0]['picture']; ?>" width="60px" height="90px" />
				</div>
				<span><u>ADMIT CARD<u></span>
			 </div>

			 <div class="clr"></div>
			</div>

			<div class="student_info">
			   <div class="student_info-left">
			     <table class="student_info_table">
				  <tr>
					<td colspan="2">Name : <b><?php echo $row['name']; ?></b></td>

          <td colspan="2">Exam Name : <b><?php echo $exam; ?></b></td>
				  </tr>
				  <tr>
            <td width="25%">Student ID : <b><?php echo $row['student_code']; ?></b></td>
            <td width="25%">Class : <b><?php echo $row['class_name']; ?></b></td>
          <td width="25%">Section : <b><?php echo $row['section_name']; ?></b></td>
					<td width="25%">Group : <b><?php echo $row['group_name']; ?></b></td>

				  </tr>
				  <tr>
          <td width="25%">Shift : <b><?php echo $row['shift_name']; ?></b></td>
					<td width="25%">Roll No. : <b><?php echo $row['roll_no']; ?></b></td>
          <td width="25%">Year : <b><?php echo $year; ?></b></td>
          <td width="25%">Mobile : <b><?php echo $row['guardian_mobile']; ?></b></td>
				  </tr>
				  </table>
			   </div>

			</div>

			<!-- <div class="student_exam_info">
			   <div class="exam_info-left">
					<table class="mark_info_table">
					  <tr>
						<th class="td_center td_backgroud">
              <p>
                N.B.:  Student without admit card would not be allowed to participate in the exam. List
                of student and routine will be published on website.
              </p>
            </th>
					  </tr>
					</table>
				</div>
			</div> -->



      <div class="signature_info">
            <div class="signature_left">
                 <?php  if (isset($signature['L'])) {
                if ($signature['L']['is_use'] == '1') { ?>

                   <img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['L']['location']; ?>" height="60" width="75"/>
                    <br>................................<br>
                   <span><?php echo $signature['L']['level']; ?></span>

                 <?php }
            } ?>&nbsp;
            </div>
            <div class="signature_middle">
              <?php  if (isset($signature['M'])) {
                if ($signature['M']['is_use'] == '1') { ?>
                <img  style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['M']['location']; ?>"  height="60" width="75"/>
                 <br>................................<br>
                 <span><?php echo $signature['M']['level']; ?></span>
              <?php }
            } ?>
            &nbsp;
           </div>
           <div class="signature_right">
             <?php  if (isset($signature['R'])) {
                if ($signature['R']['is_use'] == '1') { ?>
               <img style="margin-bottom: -25px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/signature/<?php echo $signature['R']['location']; ?>" height="60" width="75"/>
                <br>................................<br>
                <span><?php echo $signature['R']['level']; ?></span>
             <?php }
            } ?>&nbsp;
          </div>

      </div>
      <span style="font-size:12px; float:right;"><b>Powered By: School360 </b>&nbsp;</span>
	    </div>
	</div>

  <?php
  if ($i % 3 != 0) {
      echo '<br>';
  }
  ?>

<?php
if ($i % 3 == 0) {
      echo '</section><section class="sheet padding-5mm">';
  }
$i++; endforeach;
?>


<script type="text/javascript">
    $('#background-color-picker,#header-color-picker,#text-color-picker,#1st-border-color-picker,#2nd-border-color-picker').colorpicker();
    $('#background-color-picker').colorpicker().on('changeColor', function() {
        $('.main_content').css('background-color',$(this).colorpicker('getValue', '#ffffff') );
		document.getElementById("background_color").value = $(this).colorpicker('getValue', '#ffffff');
    });
	$('#header-color-picker').colorpicker().on('changeColor', function() {
        $('.header').css('background-color',$(this).colorpicker('getValue', '#ffffff') );
		document.getElementById("header_color").value = $(this).colorpicker('getValue', '#ffffff');
    });
	$('#text-color-picker').colorpicker().on('changeColor', function() {
        document.body.style.color = $(this).colorpicker('getValue', '#ffffff');
		document.getElementById("text_color").value = $(this).colorpicker('getValue', '#ffffff');
    });
	$('#1st-border-color-picker').colorpicker().on('changeColor', function() {
	    $(".main_content").css("border-color", $(this).colorpicker('getValue', '#ffffff'));
		document.getElementById("main_first_border_color").value = $(this).colorpicker('getValue', '#ffffff');
    });
	$('#2nd-border-color-picker').colorpicker().on('changeColor', function() {
	    //alert($(this).colorpicker('getValue', '#ffffff'));
		$(".body_content").css("border-color", $(this).colorpicker('getValue', '#ffffff'));
		document.getElementById("main_second_border_color").value = $(this).colorpicker('getValue', '#ffffff');
    });
</script>


</body>

</html>
