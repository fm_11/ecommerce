<script>
function show_off_roll_section(view_type){
    if(view_type == 'A'){
        document.getElementById("from_roll_section").style.display = "none";
        document.getElementById("to_roll_section").style.display = "none";
    }
    if(view_type == 'S'){
        document.getElementById("from_roll_section").style.display = "";
        document.getElementById("to_roll_section").style.display = "";
    }
}
</script>
        <form name="addForm" class="cmxform" id="commentForm"  target="_blank" action="<?php echo base_url(); ?>print_admit/index" method="post"
              enctype="multipart/form-data">
              <div class="form-row">
                    <div class="form-group col-md-4">
                      <label>Exam</label>
                      <select class="js-example-basic-single w-100" name="txtExam" required="1">
                          <option value="">-- Please Select --</option>
                          <?php
                          $i = 0;
                          if (count($exam)) {
                              foreach ($exam as $list) {
                                  $i++; ?>
                                  <option
                                      value="<?php echo $list['id']; ?>"><?php echo $list['name'].' ('.$list['year'].')'; ?></option>
                                  <?php
                              }
                          }
                          ?>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
                      <select class="js-example-basic-single w-100" name="class_shift_section_id" id="class_shift_section_id" required>
                          <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                          <?php
                          $i = 0;
                          if (count($class_section_shift_marge_list)) {
                              foreach ($class_section_shift_marge_list as $list) {
                                  $i++; ?>
                                  <option
                                      value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>">
                                  <?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
                                 </option>
                                  <?php
                              }
                          }
                          ?>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label>Group</label>
                      <select class="js-example-basic-single w-100" name="txtGroup" required="1">
                          <option value="">-- Please Select --</option>
                          <?php
                          $i = 0;
                          if (count($group)) {
                              foreach ($group as $list) {
                                  $i++; ?>
                                  <option
                                      value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                  <?php
                              }
                          }
                          ?>
                      </select>
                    </div>
              </div>


              <div class="form-row">
                <div class="form-group col-md-4">
                   <label>Type</label>
                   <select  class="js-example-basic-single w-100" onchange="show_off_roll_section(this.value)" name="view_type" required="1">
                      <option value="A">All Student</option>
                      <option value="S">Selected Roll</option>
                   </select>
                 </div>

                <div class="form-group col-md-4">
                   <div id="from_roll_section" style="display:none;">
                      <label>From Roll</label>
                      <input type="text" autocomplete="off"  class="form-control"  name="from_roll" id="from_roll"/>
                      <span>Use only number. (Example: 1,2,3 etc.)</span>
                   </div>
                </div>

                <div class="form-group col-md-4">
                   <div id="to_roll_section" style="display:none;">
                      <label>To Roll</label>
                      <input type="text" autocomplete="off"  class="form-control"  name="to_roll" id="to_roll"/>
                       <span>Use only number. (Example: 1,2,3 etc.)</span>
                   </div>
                </div>
              </div>
          <div class="btn-group float-right">
              <!-- <input type="submit" name="is_pdf_request" class="btn btn-light" value="Download"> -->
              <input type="submit" name="Preview" class="btn btn-primary" value="Preview & Download">
          </div>
        </form>
