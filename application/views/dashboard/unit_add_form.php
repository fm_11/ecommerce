<?php
if($action == 'edit'){
?>


<h2>Update Unit</h2>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard/unit_info_edit" method="post">
    <label>Unit Name</label>
    <input type="text" autocomplete="off"  name="txtUnitName" class="smallInput wide" required="1" value="<?php echo $unit_info->unit_name; ?>">


    <label>Address</label>
    <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="txtAddress"><?php echo $unit_info->address;  ?></textarea>

    <br>
    <br>
    <input type="hidden" name="id" value="<?php echo $unit_info->id; ?>">
    <input type="submit" class="submit" value="Update">
    <input type="reset" class="submit" value="Reset">
</form>
<br />

<div class="clear"></div><br />


<?php
}
else{
?>

<h2>Add New Unit</h2>
<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard/unit_add" method="post">
    <label>Unit Name</label>
    <input type="text" autocomplete="off"  name="txtUnitName" class="smallInput wide" required="1">

    

    <label>Address</label>
    <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="txtAddress"></textarea>

    <br>
    <br>
    <input type="submit" class="submit" value="Submit">
    <input type="reset" class="submit" value="Reset">
</form>
<br />

<div class="clear"></div><br />

<?php
}
?>
