<style>
	.custom_text_cls{
		font-weight: bold !important;
		color: #000000 !important;
	}
	.class_wise_student_border{
		border: 6px <?php echo $dashboard_color_info->class_wise_student_border_color; ?> solid !important;
	}
	.gender_wise_student_border{
		border: 6px <?php echo $dashboard_color_info->gender_wise_wise_border_color; ?> solid !important;
	}
	.income_vs_expense_border{
		border: 6px <?php echo $dashboard_color_info->income_vs_expense_border_color; ?> solid !important;
	}
	.last_7_days_border{
		border: 6px <?php echo $dashboard_color_info->last_7_days_collection_border_color; ?> solid !important;
	}
</style>

<?php
$session_user = $this->session->userdata('user_info');
$logo_name = PUBPATH . MEDIA_FOLDER . '/logos/' . $session_user[0]->contact_info[0]['picture'];
if(file_exists($logo_name)){
	//echo 88;die;
	$logo_name = base_url() . MEDIA_FOLDER . '/logos/' . $session_user[0]->contact_info[0]['picture'];
}else{
	$logo_name = base_url() . 'core_media/admin_v3/images/school360-logo.png';
}
$teacher_image = "core_media/images/teacher-big.png";
if (isset($dashboard_teacher_info)) {
	$teacher_image = MEDIA_FOLDER . "/teacher/" . $dashboard_teacher_info->photo_location;
}

?>

<style>
.background_color{
	border:15px #4cae4c solid;
}
</style>


<div class="row  mt-3">
					 <div class="col-xl-3 d-flex grid-margin stretch-card">
						 <div class="card">
							 <div class="card-body py-4" style="border:15px #76C1FA  solid;">
								 <div class="d-flex flex-wrap justify-content-between">
										 <h2 class="mb-0 extra-large-title mt-1">Total Products </h2>
									 <!-- <div class="badge badge-pill badge-danger">-15%</div> -->
								 </div>

								 <div class="d-lg-flex align-items-baseline justify-content-between mt-3">
									 <div class="d-flex align-items-center">
										 <div class="icon-bg-square">
											 <i class="ion ion ion-ios-basket menu-icon"></i>
										 </div>
										 <h1 class="text-dark font-weight-bold"><?php echo $total_product; ?></h1>
									 </div>
								 </div>
								 <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold">Read more</button>
							 </div>
						 </div>
					 </div>
					 <div class="col-xl-3 d-flex grid-margin stretch-card">
						 <div class="card">
							 <div class="card-body py-4" style="border:15px #a7896a solid;">
								 <div class="d-flex flex-wrap justify-content-between">
										 <h2 class="mb-0 extra-large-title mt-1">Total Customers </h2>
									 <!-- <div class="badge badge-pill badge-success">51%</div> -->
								 </div>

								 <div class="d-lg-flex align-items-baseline justify-content-between mt-3">
										 <div class="d-flex align-items-center">
											 <div class="icon-bg-square">
												 <i class="fa fa-user-o"></i>
											 </div>
											 <h1 class="text-dark font-weight-bold"><?php echo $total_customer; ?></h1>
										 </div>
								 </div>
								 <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold">Read more</button>
							 </div>
						 </div>
					 </div>
					 <div class="col-xl-3 d-flex grid-margin stretch-card">
						 <div class="card">
							 <div class="card-body py-4" style="border:15px #4d8264 solid;">
								 <div class="d-flex flex-wrap justify-content-between">
										 <h2 class="mb-0 extra-large-title mt-1">Total Categories</h2>
									 <!-- <div class="badge badge-pill badge-success">34%</div> -->
								 </div>

								 <div class="d-lg-flex align-items-baseline justify-content-between mt-3">
										 <div class="d-flex align-items-center">
												 <div class="icon-bg-square">
													 <i class="ion ion ion-ios-basket menu-icon"></i>
												 </div>
												 <h1 class="text-dark font-weight-bold"><?php echo $total_category; ?></h1>
											 </div>
								 </div>
								 <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold">Read more</button>
							 </div>
						 </div>
					 </div>
					 <div class="col-xl-3 d-flex grid-margin stretch-card">
						 <div class="card">
							 <div class="card-body py-4" style="border:15px #4f57b9 solid;">
								 <div class="d-flex flex-wrap justify-content-between">
										 <h2 class="mb-0 extra-large-title mt-1">Total Sales</h2>
									 <!-- <div class="badge badge-pill badge-success">34%</div> -->
								 </div>

								 <div class="d-lg-flex align-items-baseline justify-content-between mt-3">
										 <div class="d-flex align-items-center">
												 <div class="icon-bg-square">
													 <i class="fa fa-dollar"></i>
												 </div>
												 <h1 class="text-dark font-weight-bold"><?php echo $total_sales->total; ?></h1>
											 </div>
								 </div>
								 <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold">Read more</button>
							 </div>
						 </div>
					 </div>
				 </div>



				 <div class="row  mt-3">
				 					 <div class="col-xl-3 d-flex grid-margin stretch-card">
				 						 <div class="card">
				 							 <div class="card-body py-4" style="border:15px #b3b3ff solid;">
				 								 <div class="d-flex flex-wrap justify-content-between">
				 										 <h2 class="mb-0 extra-large-title mt-1">Total Orders </h2>
				 									 <!-- <div class="badge badge-pill badge-danger">-15%</div> -->
				 								 </div>

				 								 <div class="d-lg-flex align-items-baseline justify-content-between mt-3">
				 									 <div class="d-flex align-items-center">
				 										 <div class="icon-bg-square">
				 											 <i class="ion ion-ios-briefcase"></i>
				 										 </div>
				 										 <h1 class="text-dark font-weight-bold"><?php echo $total_order; ?></h1>
				 									 </div>
				 								 </div>
				 								 <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold">Read more</button>
				 							 </div>
				 						 </div>
				 					 </div>
				 					 <div class="col-xl-3 d-flex grid-margin stretch-card">
				 						 <div class="card">
				 							 <div class="card-body py-4" style="border:15px #ecb3ff solid;">
				 								 <div class="d-flex flex-wrap justify-content-between">
				 										 <h2 class="mb-0 extra-large-title mt-1">Pending Orders </h2>
				 									 <!-- <div class="badge badge-pill badge-success">51%</div> -->
				 								 </div>

				 								 <div class="d-lg-flex align-items-baseline justify-content-between mt-3">
				 										 <div class="d-flex align-items-center">
				 											 <div class="icon-bg-square">
				 												 <i class="ion ion-ios-briefcase"></i>
				 											 </div>
				 											 <h1 class="text-dark font-weight-bold"><?php echo $total_pending_order; ?></h1>
				 										 </div>
				 								 </div>
				 								 <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold">Read more</button>
				 							 </div>
				 						 </div>
				 					 </div>
				 					 <div class="col-xl-3 d-flex grid-margin stretch-card">
				 						 <div class="card">
				 							 <div class="card-body py-4" style="border:15px #ffc299 solid;">
				 								 <div class="d-flex flex-wrap justify-content-between">
				 										 <h2 class="mb-0 extra-large-title mt-1">Processing Orders</h2>
				 									 <!-- <div class="badge badge-pill badge-success">34%</div> -->
				 								 </div>

				 								 <div class="d-lg-flex align-items-baseline justify-content-between mt-3">
				 										 <div class="d-flex align-items-center">
				 												 <div class="icon-bg-square">
				 													 <i class="ion ion-ios-briefcase"></i>
				 												 </div>
				 												 <h1 class="text-dark font-weight-bold"><?php echo $total_processing_order; ?></h1>
				 											 </div>
				 								 </div>
				 								 <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold">Read more</button>
				 							 </div>
				 						 </div>
				 					 </div>
				 					 <div class="col-xl-3 d-flex grid-margin stretch-card">
				 						 <div class="card">
				 							 <div class="card-body py-4" style="border:15px #009900 solid;">
				 								 <div class="d-flex flex-wrap justify-content-between">
				 										 <h2 class="mb-0 extra-large-title mt-1">Delivered Orders</h2>
				 									 <!-- <div class="badge badge-pill badge-success">34%</div> -->
				 								 </div>

				 								 <div class="d-lg-flex align-items-baseline justify-content-between mt-3">
				 										 <div class="d-flex align-items-center">
				 												 <div class="icon-bg-square">
				 													 <i class="ion ion-ios-briefcase"></i>
				 												 </div>
				 												 <h1 class="text-dark font-weight-bold"><?php echo $total_delivered_order; ?></h1>
				 											 </div>
				 								 </div>
				 								 <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold">Read more</button>
				 							 </div>
				 						 </div>
				 					 </div>
				 				 </div>



					 				 <div class="row  mt-3">
					 				 					 <div class="col-xl-3 d-flex grid-margin stretch-card">
					 				 						 <div class="card">
					 				 							 <div class="card-body py-4" style="border:15px #F36368 solid;">
					 				 								 <div class="d-flex flex-wrap justify-content-between">
					 				 										 <h2 class="mb-0 extra-large-title mt-1">Canceled Orders </h2>
					 				 									 <!-- <div class="badge badge-pill badge-danger">-15%</div> -->
					 				 								 </div>

					 				 								 <div class="d-lg-flex align-items-baseline justify-content-between mt-3">
					 				 									 <div class="d-flex align-items-center">
					 				 										 <div class="icon-bg-square">
					 				 											 <i class="ion ion-ios-briefcase"></i>
					 				 										 </div>
					 				 										 <h1 class="text-dark font-weight-bold"><?php echo $total_canceled_order; ?></h1>
					 				 									 </div>
					 				 								 </div>
					 				 								 <button type="button" class="btn btn-link text-secondary p-0 mt-4 font-weight-bold">Read more</button>
					 				 							 </div>
					 				 						 </div>
					 				 					 </div>



					 				 				 </div>
<!--
<div class="row">
	<div class="col-xl-7 d-flex grid-margin stretch-card">
		<div class="card">
			<div class="card-body py-6 class_wise_student_border">
				<h5 class="card-subtitle-lg"><?php echo $this->lang->line('class') . ' ' . $this->lang->line('wise') . ' ' . $this->lang->line('student'); ?></h5>
				<input type="hidden" id="students_chart_level" value="<?php echo $students_chart_level; ?>">
				<input type="hidden" id="students_chart_color"  value="<?php echo $students_chart_color; ?>">
				<input type="hidden" id="students_chart_value"  value="<?php echo $students_chart_value; ?>">
				<canvas id="order-by-location"></canvas>
			</div>
		</div>
	</div>

	<div class="col-lg-5 grid-margin stretch-card">
		<div class="card gender_wise_student_border">
			<div class="card-body">
				<input type="hidden" id="male_student" value="<?php echo $male_student; ?>">
				<input type="hidden" id="female_student" value="<?php echo $female_student; ?>">
				<input type="hidden" id="other_gender_student" value="<?php echo $other_gender_student; ?>">

				<h4 class="card-title">Gender Wise Student</h4>
				<div id="morris-donut-example"></div>
			</div>
		</div>
	</div>

</div>


<div class="row">

	<?php if (isset($general_config->income_vs_expense_graph) && $general_config->income_vs_expense_graph == '1') { ?>
		<div class="col-lg-6 grid-margin stretch-card">
			<div class="card income_vs_expense_border">
				<div class="card-body">
					<input type="hidden" id="i_vs_e_income_data" value="<?php echo rtrim($i_vs_e_income_data, ','); ?>">
					<input type="hidden" id="i_vs_e_exense_data" value="<?php echo rtrim($i_vs_e_exense_data, ','); ?>">
					<h4 class="card-title"><?php echo $this->lang->line('income') . ' vs ' . $this->lang->line('expense'); ?> (<?php echo date('Y'); ?>)</h4>
					<canvas id="areachart-multi"></canvas>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if (isset($general_config->is_last_7_days_collection_graph) && $general_config->is_last_7_days_collection_graph == '1') { ?>
		<div class="col-xl-6 d-flex grid-margin stretch-card">
			<div class="card">
				<div class="card-body py-6 last_7_days_border">
					<input type="hidden" id="last_7_days_graph_value" value="<?php echo $last_7_days_graph_value; ?>">
					<textarea id="last_7_days_graph_level" style="display:none;"><?php echo $last_7_days_graph_level; ?></textarea>
					<h4 class="card-title"><?php echo $this->lang->line('last_7_days_collection'); ?></h4>
					<canvas id="barChart"></canvas>
				</div>
			</div>
		</div>

	<?php } ?>

</div> -->
