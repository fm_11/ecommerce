<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/js/jquery-latest.min.js"></script>
<script>
    $(document).ready(function() {
        var str = location.href.toLowerCase();
        $(".sub-header li a").each(function() {
            if (str.indexOf(this.href.toLowerCase()) > -1) {
                $("a.current").removeClass("current");
                $(this).closest('a').addClass('current');
            }
        });
    })

</script>


<ul class="sub-header">
    <li><a href="<?php echo base_url(); ?>dashboard/notice_list"><span>Notice</span></a></li>
    <li><a href="<?php echo base_url(); ?>dashboard/managing_committee_list"><span>Managing Committee</span></a></li>
    <li><a href="<?php echo base_url(); ?>dashboard/all_headmasters_list"><span>All Headmasters</span></a></li>
    <li><a href="<?php echo base_url(); ?>dashboard/administration_list"><span>Administration</span></a></li>
    <li><a href="<?php echo base_url(); ?>dashboard/success_student_list"><span>Success Students</span></a></li>
    <li><a href="<?php echo base_url(); ?>dashboard/get_contact_message"><span>Contact Message</span></a></li>
    <li><a href="<?php echo base_url(); ?>dashboard/get_slide_images"><span>Slide</span></a></li>
    <li><a href="<?php echo base_url(); ?>dashboard/get_digital_display"><span>Digital Contents</span></a></li>
    <li><a href="<?php echo base_url(); ?>dashboard/document_list"><span>Document</span></a></li>
</ul>

