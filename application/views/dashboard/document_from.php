
    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard/document_add" method="post" enctype="multipart/form-data">
        <label>Title</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtTitle" required="1"/>

        <label>Date</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtDate" value="<?php echo date('Y-m-d') ?>"  required="1"/>

        <label>File</label>
        <input type="file" name="txtFile" required="1" class="smallInput"> * File Format -> PDF , DOC , DOCX ,JPEG , JPG and PNG.

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form><br />

<div class="clear"></div><br />
