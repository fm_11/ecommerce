<script type="text/javascript">
    function deleteConfirm() {
        var result = confirm("Are you sure to delete?");
        if (result == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>

<h2>
    <a class="button_grey_round" style="margin-bottom: 5px;"
       href="<?php echo base_url(); ?>dashboard/managing_committee_add"><span>Add New Member</span></a>
</h2>
<table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th width="50" scope="col">SL</th>
        <th width="200" scope="col">Name</th>
        <th width="200" scope="col">Post</th>
        <th width="200" scope="col">Mobile</th>
        <th width="200" scope="col">Photo</th>
        <th width="100" scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($managing_committee as $row):
        $i++;
        ?>
        <tr>

        <tr>
            <td width="34" style="vertical-align:middle">
                <?php echo $i; ?>
            </td>
            <td style="vertical-align:middle"><?php echo $row['name']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['post']; ?></td>
            <td style="vertical-align:middle"><?php echo $row['mobile']; ?></td>
            <td style="vertical-align:middle">
                <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/managing_committee/<?php echo $row['photo_location'] ?>" height="120"
                     width="150">
            </td>

            <td style="vertical-align:middle">
                <a href="<?php echo base_url(); ?>dashboard/managing_committee_edit/<?php echo $row['id']; ?>"
                   class="edit_icon" title="Edit"></a>
                <a href="<?php echo base_url(); ?>dashboard/managing_committee_delete/<?php echo $row['id']; ?>"
                   onclick="return deleteConfirm()" class="delete_icon" title="Delete"></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>