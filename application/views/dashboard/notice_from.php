<?php
if($action == 'edit'){
    ?>
    <h2>Update Teacher Information</h2>
    <form id="edit" action="<?php echo base_url(); ?>index.php/photo_gallerys/gallery_photo_edit" method="post" enctype="multipart/form-data">
        <label>Title</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtTitle" value="<?php echo $photo_gallery_info[0]['title']; ?>"/>
        <label>Event Name</label>
        <select class="smallInput" name="txtEventName" required="1">
            <option value="">-- Please Select --</option>
            <?php
            $i = 0;
            if (count($events)) {
                foreach ($events as $list) {
                    $i++;
                    ?>
                    <option
                        value="<?php echo $list['id']; ?>"<?php if($list['id']==$photo_gallery_info[0]['event_id']){echo 'selected';} ?>><?php echo $list['event_name']; ?></option>
                <?php
                }
            }
            ?>
        </select>

        <label>Photo</label>
        <input type="file" name="txtPhoto" class="smallInput">

        <label>Remarks</label>
        <textarea id="wysiwyg" class="smallInput wide" rows="7" cols="30" name="txtRemarks"><?php echo $photo_gallery_info[0]['remarks']; ?></textarea>

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form><br />



<?php
}else{
    ?>


    <h2>Please Input All Correct Information</h2>
    <form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>dashboard/notice_add" method="post" enctype="multipart/form-data">
        <label>Title</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtTitle" required="1"/>

        <label>Date</label>
        <input type="text" autocomplete="off"  class="smallInput wide" name="txtDate" value="<?php echo date('Y-m-d') ?>"  required="1"/>

        <label>File</label>
        <input type="file" name="txtFile" required="1" class="smallInput"> * File Format -> PDF , DOC , DOCX ,JPEG , JPG and PNG.

        <br>
        <br>
        <input type="submit" class="submit" value="Submit">
        <input type="reset" class="submit" value="Reset">
    </form><br />

<?php
}
?>

<div class="clear"></div><br />
