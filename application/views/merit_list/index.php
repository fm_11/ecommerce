<script>
	function getExamByYear(year,type){
		//alert(year);
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		}
		else
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			{
				//alert(xmlhttp.responseText);
				if(type == 'cw'){
					document.getElementById("exam_id_for_class").innerHTML = xmlhttp.responseText;
				}
				if(type == 'sw'){
					document.getElementById("exam_id_for_shift").innerHTML = xmlhttp.responseText;
				}
				if(type == 'secw'){
					document.getElementById("exam_id_for_section").innerHTML = xmlhttp.responseText;
				}
				if(type == 'gw'){
					document.getElementById("exam_id_for_group").innerHTML = xmlhttp.responseText;
				}
			}
		}
		xmlhttp.open("GET", "<?php echo base_url(); ?>exams/getExamByYear?year=" + year, true);
		xmlhttp.send();
	}
</script>

<?php
$merit_report_report_type = $this->session->userdata('merit_report_report_type');

if($merit_report_report_type == ''){
	$merit_report_report_type = 'cw';
}
?>

		<ul class="nav nav-pills nav-pills-success" id="pills-tab" role="tablist">
			<li class="nav-item">
				<a class="nav-link <?php if($merit_report_report_type == 'cw'){ echo 'active'; } ?>" id="pills-home-tab" data-toggle="pill" href="#pills-Asset" role="tab" aria-controls="pills-Asset" aria-selected="<?php if($merit_report_report_type == 'cw'){ echo 'true'; }else{ echo 'false'; } ?>">Class Wise</a>
			</li>
			<li class="nav-item">
				<a class="nav-link <?php if($merit_report_report_type == 'sw'){ echo 'active'; } ?>" id="pills-Liabilities-tab" data-toggle="pill" href="#pills-Liabilities" role="tab" aria-controls="pills-Liabilities" aria-selected="<?php if($merit_report_report_type == 'sw'){ echo 'true'; }else{ echo 'false'; } ?>">Shift Wise</a>
			</li>
			<li class="nav-item">
				<a class="nav-link <?php if($merit_report_report_type == 'secw'){ echo 'active'; } ?>" id="pills-Income-tab" data-toggle="pill" href="#pills-Income" role="tab" aria-controls="pills-Income" aria-selected="<?php if($merit_report_report_type == 'secw'){ echo 'true'; }else{ echo 'false'; } ?>">Section Wise</a>
			</li>
			<li class="nav-item">
				<a class="nav-link <?php if($merit_report_report_type == 'gw'){ echo 'active'; } ?>" id="pills-Expenses-tab" data-toggle="pill" href="#pills-Expenses" role="tab" aria-controls="pills-Expenses" aria-selected="<?php if($merit_report_report_type == 'gw'){ echo 'true'; }else{ echo 'false'; } ?>">Group Wise</a>
			</li>
		</ul>
		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade <?php if($merit_report_report_type == 'cw'){ echo 'show active'; } ?>" id="pills-Asset" role="tabpanel" aria-labelledby="pills-Asset-tab">
				<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>merit_lists/index" method="post">
					<div class="form-row col-md-12">
						<div class="form-group col-md-3">
							<label>Year</label>
							<select class="js-example-basic-single" style="width: 100%" onchange="getExamByYear(this.value,'cw')" name="year" id="year" required="1">
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($years)) {
									foreach ($years as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['value']; ?>"
											<?php if (isset($year)) {
												if ($year == $list['value']) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['text']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>
						<div class="form-group col-md-3">
							<label><?php echo $this->lang->line('exam'); ?></label>
							<select name="exam_id" id="exam_id_for_class" style="width: 100%"  class="js-example-basic-single w-100" required="required">
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php for ($A = 0; $A < count($exam_list); $A++) { ?>
									<option value="<?php echo $exam_list[$A]['id']; ?>"
										<?php if (isset($exam_id)) {
											if ($exam_id == $exam_list[$A]['id']) {
												echo 'selected';
											}
										} ?>><?php echo $exam_list[$A]['name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-md-3">
							<label for="class"><?php echo $this->lang->line('class'); ?></label>
							<select class="js-example-basic-single w-100" style="width: 100%"
									name="class_id" id="class_id" required>
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($class_list)) {
									foreach ($class_list as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['id']; ?>"
											<?php if (isset($class_id)) {
												if ($class_id == $list['id']) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['name']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>
					</div>

					<div class="btn-group float-right">
						<input type="hidden" value="cw" name="report_type">
						<input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
						<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
						<input type="submit" class="btn btn-primary" value="View Report">
					</div>
				</form>
			</div>
			<div class="tab-pane fade <?php if($merit_report_report_type == 'sw'){ echo 'show active'; } ?>" id="pills-Liabilities" role="tabpanel" aria-labelledby="pills-Liabilities-tab">
				<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>merit_lists/index" method="post">
					<div class="form-row col-md-12">
						<div class="form-group col-md-3">
							<label>Year</label>
							<select class="js-example-basic-single" style="width: 100%" onchange="getExamByYear(this.value,'sw')" name="year" id="year" required="1">
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($years)) {
									foreach ($years as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['value']; ?>"
											<?php if (isset($year)) {
												if ($year == $list['value']) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['text']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>
						<div class="form-group col-md-3">
							<label><?php echo $this->lang->line('exam'); ?></label>
							<select name="exam_id" id="exam_id_for_shift" style="width: 100%"  class="js-example-basic-single w-100" required="required">
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php for ($A = 0; $A < count($exam_list); $A++) { ?>
									<option value="<?php echo $exam_list[$A]['id']; ?>"
										<?php if (isset($exam_id)) {
											if ($exam_id == $exam_list[$A]['id']) {
												echo 'selected';
											}
										} ?>><?php echo $exam_list[$A]['name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-md-3">
							<label for="class"><?php echo $this->lang->line('class'); ?></label>
							<select class="js-example-basic-single w-100" style="width: 100%"
									name="class_id" id="class_id" required>
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($class_list)) {
									foreach ($class_list as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['id']; ?>"
											<?php if (isset($class_id)) {
												if ($class_id == $list['id']) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['name']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>

						<div class="form-group col-md-3">
							<label for="class"><?php echo $this->lang->line('shift'); ?></label>
							<select class="js-example-basic-single w-100" style="width: 100%"
									name="shift_id" id="shift_id" required>
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($shift_list)) {
									foreach ($shift_list as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['id']; ?>"
											<?php if (isset($shift_id)) {
												if ($shift_id == $list['id']) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['name']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>
					</div>

					<div class="btn-group float-right">
						<input type="hidden" value="sw" name="report_type">
						<input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
						<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
						<input type="submit" class="btn btn-primary" value="View Report">
					</div>
				</form>
			</div>
			<div class="tab-pane fade <?php if($merit_report_report_type == 'secw'){ echo 'show active'; } ?>" id="pills-Income" role="tabpanel" aria-labelledby="pills-Income-tab">
				<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>merit_lists/index" method="post">
					<div class="form-row col-md-12">
						<div class="form-group col-md-3">
							<label>Year</label>
							<select class="js-example-basic-single" style="width: 100%" onchange="getExamByYear(this.value,'secw')" name="year" id="year" required="1">
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($years)) {
									foreach ($years as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['value']; ?>"
											<?php if (isset($year)) {
												if ($year == $list['value']) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['text']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>
						<div class="form-group col-md-3">
							<label><?php echo $this->lang->line('exam'); ?></label>
							<select name="exam_id" id="exam_id_for_section" style="width: 100%"  class="js-example-basic-single w-100" required="required">
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php for ($A = 0; $A < count($exam_list); $A++) { ?>
									<option value="<?php echo $exam_list[$A]['id']; ?>"
										<?php if (isset($exam_id)) {
											if ($exam_id == $exam_list[$A]['id']) {
												echo 'selected';
											}
										} ?>><?php echo $exam_list[$A]['name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-md-3">
							<label for="class"><?php echo $this->lang->line('class') . ' / ' . $this->lang->line('shift') . ' / ' . $this->lang->line('section'); ?></label>
							<select class="js-example-basic-single w-100" style="width: 100%"
									name="class_shift_section_id" id="class_shift_section_id" required>
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($class_section_shift_marge_list)) {
									foreach ($class_section_shift_marge_list as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id']; ?>"
											<?php if (isset($class_shift_section_id)) {
												if ($class_shift_section_id == ($list['class_id'] . '-' . $list['shift_id'] . '-' . $list['section_id'])) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['class_name'] . '-' . $list['shift_name'] . '-'. $list['section_name']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>
					</div>

					<div class="btn-group float-right">
						<input type="hidden" value="secw" name="report_type">
						<input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
						<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
						<input type="submit" class="btn btn-primary" value="View Report">
					</div>
				</form>
			</div>
			<div class="tab-pane fade <?php if($merit_report_report_type == 'gw'){ echo 'show active'; } ?>" id="pills-Expenses" role="tabpanel" aria-labelledby="pills-Expenses-tab">
				<form name="addForm" class="cmxform" id="commentForm"  action="<?php echo base_url(); ?>merit_lists/index" method="post">
					<div class="form-row col-md-12">
						<div class="form-group col-md-3">
							<label>Year</label>
							<select class="js-example-basic-single" style="width: 100%" onchange="getExamByYear(this.value,'gw')" name="year" id="year" required="1">
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($years)) {
									foreach ($years as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['value']; ?>"
											<?php if (isset($year)) {
												if ($year == $list['value']) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['text']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>
						<div class="form-group col-md-3">
							<label><?php echo $this->lang->line('exam'); ?></label>
							<select name="exam_id" id="exam_id_for_group" style="width: 100%"  class="js-example-basic-single w-100" required="required">
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php for ($A = 0; $A < count($exam_list); $A++) { ?>
									<option value="<?php echo $exam_list[$A]['id']; ?>"
										<?php if (isset($exam_id)) {
											if ($exam_id == $exam_list[$A]['id']) {
												echo 'selected';
											}
										} ?>><?php echo $exam_list[$A]['name']; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-md-3">
							<label><?php echo $this->lang->line('class'); ?></label>
							<select class="js-example-basic-single w-100" style="width: 100%"
									name="class_id" id="class_id" required>
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($class_list)) {
									foreach ($class_list as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['id']; ?>"
											<?php if (isset($class_id)) {
												if ($class_id == $list['id']) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['name']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>

						<div class="form-group col-md-3">
							<label><?php echo $this->lang->line('group'); ?></label>
							<select class="js-example-basic-single w-100" style="width: 100%"
									name="group_id" id="group_id" required>
								<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
								<?php
								$i = 0;
								if (count($group_list)) {
									foreach ($group_list as $list) {
										$i++; ?>
										<option
											value="<?php echo $list['id']; ?>"
											<?php if (isset($group_id)) {
												if ($group_id == $list['id']) {
													echo 'selected';
												}
											} ?>>
											<?php echo $list['name']; ?>
										</option>
										<?php
									}
								}
								?>
							</select>
						</div>
					</div>

					<div class="btn-group float-right">
						<input type="hidden" value="gw" name="report_type">
						<input type="button" class="btn btn-secondary" onclick="printDiv('printableArea')" value="Print Result"/>
						<input type="submit" class="btn btn-success" name="pdf_download" value="PDF Download"/>
						<input type="submit" class="btn btn-primary" value="View Report">
					</div>
				</form>
			</div>
		</div>


<?php
if(isset($merit_data)){
	echo $report;
}
?>
