<?php
if (isset($is_pdf) && $is_pdf == 1) {
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="<?php echo base_url(); ?>core_media/report_pdf/solaiman-lipi/font.css">
	<style>

		body {
			font-family: 'SolaimanLipi', Arial, sans-serif !important;
		}
		table, td, th {
			border: 1px solid black;
			font-size: 11px;
			padding-left: 3px !important;
			padding-right: 3px !important;
			padding: 0px;

		}
		table {
			border-collapse: collapse;
		}
		.h_td{
			font-weight: bold !important;
			text-align: center;
		}
	</style>

	<?php
} ?>

<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table width="100%" id="sortable-table-1" class="table">
		<thead>
		<tr>
			<td colspan="7"  class="h_td" style="text-align:center; line-height:1.5;">
				<b style="font-size: 18px;"><?php echo $HeaderInfo[0]['school_name']; ?></b><br>
				<span style="font-size: 13px;">
					<?php echo $HeaderInfo[0]['address']; ?><br>
				    <?php echo $title; ?></b>
				</span>
			</td>
		</tr>
		<tr>
			<td scope="col" style="text-align:center;" class="h_td"><b>Student ID</b></td>
			<td scope="col" style="text-align:center;" class="h_td"><b>Roll No.</b></td>
			<td scope="col" style="text-align:center;" class="h_td"><b>Name</b></td>
			<td scope="col" style="text-align:center;" class="h_td"><b>Total Marks</b></td>
			<td scope="col" style="text-align:center;" class="h_td"><b>Grade</b></td>
			<td scope="col" style="text-align:center;" class="h_td"><b>GPA</b></td>
			<td scope="col" style="text-align:center;" class="h_td">
				<b>
					<?php
					if($report_type == 'cw'){
						echo 'Class Merit';
					}else if($report_type == 'sw'){
						echo 'Shift Merit';
					}else if($report_type == 'secw'){
						echo 'Section Merit';
					}else if($report_type == 'gw'){
						echo 'Group Merit';
					}
					?>

				</b>
			</td>
		</tr>
		</thead>
		<tbody>

		<?php
		foreach ($merit_data as $arow):
			?>
			<tr>
				<td align="center">
					<?php
					echo $arow['student_code']; ?>
				</td>
				<td align="center">
					<?php
					echo $arow['roll_no']; ?>
				</td>
				<td align="left">
					<?php
					echo $arow['name']; ?>
				</td>
				<td align="center">
					<?php
					echo round($arow['total_obtain_mark'],2); ?>
				</td>
				<td align="center">
					<?php
					echo $arow['c_alpha_gpa_with_optional']; ?>
				</td>
				<td align="center">
					<?php
					echo round($arow['gpa_with_optional'],2); ?>
				</td>
				<td align="center">
					<?php
					if($report_type == 'cw'){
						echo $arow['class_position'];
					}else if($report_type == 'sw'){
						echo $arow['shift_position'];
					}else if($report_type == 'secw'){
						echo $arow['section_position'];
					}else if($report_type == 'gw'){
						echo $arow['group_position'];
					}
					?>
				</td>
			</tr>
		<?php
		endforeach; ?>
		</tbody>
	</table>
</div>
