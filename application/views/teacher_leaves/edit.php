<form name="addForm" class="cmxform" id="commentForm"   action="<?php echo base_url(); ?>teacher_leaves/edit" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-3">
			<label>Teacher/Staff</label>
			<select class="js-example-basic-multiple w-100" name="teacher_id" required="1">
				<option value="">-- Please Select --</option>
				<?php
				$i = 0;
				if (count($teachers)) {
					foreach ($teachers as $list) {
						$i++;
						?>
						<option
							value="<?php echo $list['id']; ?>" <?php if($list['id'] == $leave_info->teacher_id){ echo 'selected'; } ?>><?php echo $list['name'] . ' (' . $list['teacher_code'] . ')'; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Leave Type</label>
			<select class="js-example-basic-multiple w-100" name="leave_type_id" required="1">
				<option value="">-- Please Select --</option>
				<?php
				$i = 0;
				if (count($leave_types)) {
					foreach ($leave_types as $list) {
						$i++;
						?>
						<option
							value="<?php echo $list['id']; ?>" <?php if($list['id'] == $leave_info->leave_type_id){ echo 'selected'; } ?>><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="Date">From Date</label>
			<div class="input-group date">
				<input type="text" value="<?php echo $leave_info->date_from; ?>" autocomplete="off"
					   name="date_from" id="date_from" required class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
			</div>
		</div>
		<div class="form-group col-md-3">
			<label for="Date">To Date</label>
			<div class="input-group date">
				<input type="text" value="<?php echo $leave_info->date_to; ?>" autocomplete="off"
					   name="date_to" id="date_to" required class="form-control">
				<span class="input-group-text input-group-append input-group-addon">
                       <i class="simple-icon-calendar"></i>
                   </span>
			</div>
		</div>
	</div>

	<div class="form-row">
		<div class="form-group col-md-12">
			<label for="txtRemarks">Reason</label>
			<textarea rows="5" cols="50" required class="form-control" name="reason"><?php echo $leave_info->reason; ?></textarea>
		</div>
	</div>

	<input type="hidden" name="id" value="<?php echo $leave_info->id; ?>">


	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>
</form>
