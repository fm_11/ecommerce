<div class="table-sorter-wrapper col-lg-12 table-responsive">
	<table id="sortable-table-1" class="table">
		<thead>
		<tr>
			<th scope="col">Class</th>
			<th scope="col">Data List</th>
			<th scope="col">Actions</th>
		</tr>
		</thead>
		<tbody>
		<?php
		foreach ($class_list as $row):
			?>

			<tr>
				<td><?php echo $row['name']; ?></td>
				<td>
					<?php
					  echo $row['data_list'];
					?>
				</td>
				<td>
					<a href="<?php echo base_url(); ?>main_marking_heads/edit/<?php echo $row['id']; ?>"
					   class="btn btn-warning btn-xs mb-1" title="Edit">Edit</a>
				</td>
			</tr>

		<?php endforeach; ?>
		</tbody>
	</table>
</div>
