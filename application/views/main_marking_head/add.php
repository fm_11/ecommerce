<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>main_marking_heads/add" method="post" enctype="multipart/form-data">

	<div class="form-row">
		<div class="form-group col-md-6">
			<label for="class">Classes</label><br>
			<select class="js-example-basic-multiple w-100" multiple name="class_id[]" id="class_id" required="1">
				<?php
				if (count($class_list)) {
					foreach ($class_list as $list) {
						?>
						<option value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
						<?php
					}
				}
				?>
			</select>
		</div>
	</div>


	<div class="form-row">
    <div class="form-group col-md-3">
      <label>Creative or Written</label><br>
      <input type="text" autocomplete="off"  name="written" required class="form-control" value="">
    </div>

    <div class="form-group col-md-3">
      <label>Objetive</label><br>
      <input type="text" autocomplete="off"  name="mcq" required class="form-control" value="">
    </div>

    <div class="form-group col-md-3">
      <label>Practical</label><br>
        <input type="text" autocomplete="off"  name="practical" required class="form-control" value="">
    </div>

    <div class="form-group col-md-3">
      <label>Class Test/Other</label><br>
      <input type="text" autocomplete="off"  name="class_test" required class="form-control" value="">
    </div>
  </div>


  <div class="float-right">
     <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
  </div>

</form>
