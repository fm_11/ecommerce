<form name="addForm" class="cmxform" id="commentForm" action="<?php echo base_url(); ?>main_marking_heads/edit" method="post" enctype="multipart/form-data">
	<div class="form-row">
		<div class="form-group col-md-3">
			<label>Creative or Written</label><br>
			<input type="text" autocomplete="off"  name="written" required class="form-control" value="<?php if(!empty($heads)){ echo $heads->written; } ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Objetive</label><br>
			<input type="text" autocomplete="off"  name="mcq" required class="form-control" value="<?php if(!empty($heads)){ echo $heads->mcq; } ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Practical</label><br>
			<input type="text" autocomplete="off"  name="practical" required class="form-control" value="<?php if(!empty($heads)){ echo $heads->practical; } ?>">
		</div>

		<div class="form-group col-md-3">
			<label>Class Test/Other</label><br>
			<input type="text" autocomplete="off"  name="class_test" required class="form-control" value="<?php if(!empty($heads)){ echo $heads->class_test; } ?>">
		</div>
	</div>

	<input type="hidden" autocomplete="off"  name="class_id" required class="form-control" value="<?php echo $class_id; ?>">


	<div class="float-right">
		<input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('update'); ?>">
	</div>

</form>
