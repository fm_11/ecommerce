<script type="text/javascript">
    function get_fee_allocation_form_by_year_class(){
		var class_id = document.getElementById('class_id').value;
		var group_id = document.getElementById('group_id').value;
		var year = document.getElementById('year').value;
		var category_id = document.getElementById('category_id').value;
        debugger;
        if(class_id != "" && group_id != "" && category_id != "" && year != ""){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			}
			else
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					document.getElementById("allocation_form").innerHTML = xmlhttp.responseText;
					$(".js-example-basic-multiple").select2();
				}
			}
			xmlhttp.open("GET", "<?php echo base_url(); ?>fee_allocations/get_fee_allocation_form_by_year_class?class_id=" + class_id + '&&group_id=' + group_id + '&&category_id=' + category_id + '&&year=' + year, true);
			xmlhttp.send();
        }
    }

    function collectMonthDisableForAllMonth(rowId) {
		var isSelect = document.getElementById("all_month_" + rowId);
		if (isSelect.checked == true){
			document.getElementById("collection_month_" + rowId).disabled = true;
			document.getElementById("is_selected_" + rowId).checked = true;
		} else {
			document.getElementById("collection_month_" + rowId).disabled = false;
		}
	}

	function collectMonthDisable(rowId) {
		var isSelect = document.getElementById("is_selected_" + rowId);
		if (isSelect.checked == true){
			document.getElementById("collection_month_" + rowId).disabled = false;
		} else {
			document.getElementById("collection_month_" + rowId).disabled = true;
		}
	}

	function checkAllocationData() {
    	var totalRow = document.getElementById("loop_time").value;
    	var i = 0;
    	var total_selected = 0;
    	while (i < totalRow){
			var isSelect = document.getElementById("is_selected_" + i);
			if (isSelect.checked == true){
				total_selected++;
				var collection_month = document.getElementById("collection_month_" + i).value;
				if(collection_month == ''){
					var isAllSelect = document.getElementById("all_month_" + i);
					if (isAllSelect.checked == false){
						alert("Please set month for collection");
						document.getElementById("collection_month_" + i).focus();
						return false;
					}
				}
			}
			i++;
		}
		if(total_selected == 0){
			alert("Please select some category");
			return false;
		}
        return true;
	}

</script>

    <form name="addForm" class="cmxform" id="commentForm"  onsubmit="return checkAllocationData()" action="<?php echo base_url(); ?>fee_allocations/add" method="post">
        <div class="form-row">
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('year'); ?></label>
                <select  class="js-example-basic-single w-100" name="year" id="year" onchange="get_fee_allocation_form_by_year_class()"  required="1">
                    <option value="<?php echo date('Y') - 1; ?>"><?php echo date('Y') - 1; ?></option>
                    <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>
                    <option value="<?php echo date('Y') + 1; ?>"><?php echo date('Y') + 1; ?></option>
                </select>
            </div>
            <div class="form-group col-md-3">
                <label><?php echo $this->lang->line('class'); ?></label>
                <select  class="js-example-basic-single w-100" onchange="get_fee_allocation_form_by_year_class()" id="class_id" name="class_id" required="1">
                    <option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
                    <?php
                    $i = 0;
                    if (count($class)) {
                        foreach ($class as $list) {
                            $i++; ?>
                            <option
                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
			<div class="form-group col-md-3">
				<label><?php echo $this->lang->line('group'); ?></label><br>
				<select class="js-example-basic-single w-100" onchange="get_fee_allocation_form_by_year_class()"  name="group_id" id="group_id" required="1">
					<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					<?php
					$i = 0;
					if (count($groups)) {
						foreach ($groups as $list) {
							$i++; ?>
							<option
									value="<?php echo $list['id']; ?>"
									<?php if(isset($group_id)){ if ($group_id == $list['id']) {
										echo 'selected';
									}} ?>><?php echo $list['name']; ?></option>
							<?php
						}
					}
					?>
				</select>
			</div>
			<div class="form-group col-md-3">
				<label>Category</label><br>
				<select class="js-example-basic-single w-100" onchange="get_fee_allocation_form_by_year_class()" name="student_category_id" id="category_id" required="1">
					<option value="">-- <?php echo $this->lang->line('please_select'); ?> --</option>
					<?php
					if (count($categories)) {
						foreach ($categories as $list) {
							?>
							<option
									value="<?php echo $list['id']; ?>"
									<?php if(isset($category_id)){ if ($category_id == $list['id']) {
										echo 'selected';
									}} ?>><?php echo $list['name']; ?></option>
							<?php
						}
					}
					?>
				</select>
			</div>
        </div>

	    <div id="allocation_form">

        </div>

        <div class="float-right">
            <input class="btn btn-light" type="reset" value="<?php echo $this->lang->line('cancel'); ?>">
            <input class="btn btn-primary" type="submit" value="<?php echo $this->lang->line('submit'); ?>">
        </div>
    </form>
