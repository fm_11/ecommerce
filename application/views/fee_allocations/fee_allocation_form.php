

<div class="table-responsive-sm">
    <table class="table table-hover">
 <tr>
        <th scope="col"><?php echo $this->lang->line('sl'); ?></th>
        <th scope="col"><?php echo $this->lang->line('sub').' '.$this->lang->line('category'); ?></th>
        <th scope="col"><?php echo $this->lang->line('category'); ?></th>
        <th scope="col"><?php echo $this->lang->line('non-resident').' '.$this->lang->line('amount'); ?></th>
        <th scope="col"><?php echo $this->lang->line('resident').' '.$this->lang->line('amount'); ?></th>
	    <th scope="col" style="text-align: center;">
			Collected Month <br>
			(Check For All Month)
		</th>
		<th scope="col">#</th>
    </tr>

    <?php
    $i = 0;
    if (isset($allocated_data)) {
        ?>
        <?php
        foreach ($allocated_data as $row):
            ?>
            <tr>
                <td>
                    <?php echo $i + 1; ?>
                </td>
                <td>
                    <?php echo $row['name']; ?>
                    
                    <input type="hidden" name="sub_category_id_<?php echo $i; ?>"
                           value="<?php echo $row['id']; ?>">
                </td>
                <td>
                     <?php echo $row['category_name']; ?>
                    <input type="hidden" name="category_id_<?php echo $i; ?>"
                           value="<?php echo $row['category_id']; ?>">
                </td>
                
                <td>
                    <input type="text" autocomplete="off"  class="form-control" style="text-align: right;padding: 10px; width: 100px;"
                           name="amount_<?php echo $i; ?>"
                           value="<?php if($row['amount'] != ''){ echo $row['amount']; }else{ echo 0; } ?>">
                </td>
                
                 <td>
                    <input type="text" autocomplete="off"  class="form-control" style="text-align: right;padding: 10px; width: 100px;"
                           name="resident_amount_<?php echo $i; ?>"
                           value="<?php if($row['resident_amount'] != ''){ echo $row['resident_amount']; }else{ echo 0; } ?>">
                </td>

				<td style="min-width: 230px;">
					<input type="checkbox" onclick="collectMonthDisableForAllMonth('<?php echo $i; ?>')" <?php if(count(explode(",", $row['already_selected_month'])) == 12){ echo 'checked'; } ?> name="all_month_<?php echo $i; ?>" id="all_month_<?php echo $i; ?>">
					<select <?php if($row['is_select'] ==  '' || count(explode(",", $row['already_selected_month'])) == 12){ echo 'disabled'; } ?>  class="js-example-basic-multiple" style="padding: 12px;min-width: 220px;" multiple name="collection_month_<?php echo $i; ?>[]" id="collection_month_<?php echo $i; ?>">
						<?php
						$month_no = 1;
						while ($month_no <= 12) {
							$dateObj = DateTime::createFromFormat('!m', $month_no); ?>
							<option value="<?php echo $month_no; ?>" <?php if (in_array($month_no, explode(",", $row['already_selected_month']))) { ?> selected="selected" <?php } ?>><?php echo $dateObj->format('F'); ?></option>
							<?php
							$month_no++;
						}
						?>
					</select>
				</td>
                
                <td style="vertical-align:middle">
                    <input type="checkbox" onclick="collectMonthDisable('<?php echo $i; ?>')" <?php if($row['is_select'] !=  ''){ echo 'checked'; } ?> name="is_selected_<?php echo $i; ?>" id="is_selected_<?php echo $i; ?>">
                </td>
             
                
            </tr>
        <?php $i++; endforeach;
    } ?>

</table>
</div>
<input type="hidden" class="input-text-short" id="loop_time" name="loop_time"
           value="<?php echo $i; ?>"/>
