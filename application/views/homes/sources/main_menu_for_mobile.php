<ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav>
    <li><a href="<?php echo  base_url(); ?>homes/index">Home</a></li>
    <li class="uk-parent">
        <a href="#">Academic</a>
        <ul class="uk-nav-sub">
            <li><a href="<?php echo  base_url(); ?>homes/getTeacherInfo">Teacher Information</a></li>
            <li><a href="<?php echo  base_url(); ?>homes/getMcMemberInfo">Managing Committee</a></li>
            <li><a href="<?php echo  base_url(); ?>contents/getStudentInformation">Student Information</a></li>
            <li><a href="<?php echo  base_url(); ?>homes/getStaffInfo">Staff Information</a></li>
            <li><a href="<?php echo  base_url(); ?>contents/getAllHeadTeacherInfo">All Headmaster's</a></li>
            <li><a href="<?php echo  base_url(); ?>contents/getAllSuccessStudentsInfo">Success Student's</a></li>
        </ul>
    </li>

    <li><a href="<?php echo  base_url(); ?>homes/getAllNoticeInfo">Notice</a></li>

    <li class="uk-parent">
        <a href="#">Result</a>
        <ul class="uk-nav-sub">
            <li>
                <a href="<?php echo  base_url(); ?>contents/getResult">Result Archive</a>
            </li>
            <li>
                <a href="<?php echo  base_url(); ?>homes/getAllUploadedResultInfo">Result Download</a>
            </li>
        </ul>
    </li>

    <li class="uk-parent">
        <a href="#">Admission</a>
        <ul class="uk-nav-sub">
            <li>
                <a href="<?php echo  base_url(); ?>contents/ApplyOnline" target="_blank">Apply Online</a>
            </li>
            <li>
                <a href="<?php echo  base_url(); ?>contents/getAdmitCard" target="_blank">Admit Card</a>
            </li>
            <li>
                <a href="<?php echo  base_url(); ?>contents/getAdmissionProcessInfo">Admission Process</a>
            </li>
            <li>
                <a href="<?php echo  base_url(); ?>contents/getAdmissionForm">Admission Form Download</a>
            </li>
            <li>
                <a href="<?php echo  base_url(); ?>contents/getAdmissionContact">Admission Contacts</a>
            </li>
        </ul>
    </li>

    <li class="uk-parent">
        <a href="#">About Us</a>
        <ul class="uk-nav-sub">
            <li>
                <a href="<?php echo  base_url(); ?>homes/getAboutInfo">About</a>
            </li>
            <li>
                <a href="<?php echo  base_url(); ?>homes/getHistoryInfo">History</a>
            </li>
        </ul>
    </li>

    <li><a href="<?php echo  base_url(); ?>homes/getContactInfo">Contact</a></li>
    <li><a href="<?php echo  base_url(); ?>homes/getDashboard">Dashboard</a></li>
</ul>