<div class="uk-width-large-2-4 midsection">
    <h3> Photo Gallery</h3>
    <hr/>

    <p>
        <a href="<?php echo base_url(); ?>homes/getPhotoGallery">Last 100 Image</a>|
        <?php
        $i = 0;
        foreach ($photo_event as $row):
            $i++;
            ?>
            <a href="<?php echo base_url(); ?>homes/getPhotoGallery/<?php echo $row['id']; ?>"><?php echo $row['event_name']; ?></a>|
        <?php endforeach; ?>
    </p>

    <!-- Galary section -->
    <section class="portfolio-agileinfo" id="portfolio">
        <div class="container">
            <span class="fa fa-star-o" aria-hidden="true"></span>
            <div id="grid-gallery" class="grid-gallery">
                <section class="grid-wrap">
                    <ul class="grid">
                        <li class="grid-sizer"></li><!-- for Masonry column width -->

                        <?php
                        foreach ($photos as $row):
                        ?>
                        <li data-aos="zoom-in">
                            <figure>
                                <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/gallery/<?php echo $row['photo_location']; ?>" alt="<?php echo $row['title']; ?>" class="img-responsive"/>
                            </figure>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </section><!-- // grid-wrap -->
                <section class="slideshow">
                    <ul>
                        <?php
                        foreach ($photos as $row):
                        ?>
                        <li>
                            <figure>
                                <figcaption>
                                    <h3><?php echo $row['title']; ?></h3>
                                </figcaption>
                                <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/gallery/<?php echo $row['photo_location']; ?>" alt="img01" class="img-responsive"/>
                            </figure>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <nav>
                        <span class="icon nav-prev"></span>
                        <span class="icon nav-next"></span>
                        <span class="icon nav-close"></span>
                    </nav>
                </section><!-- // slideshow -->
            </div><!-- // grid-gallery -->
        </div>
    </section>
    <!-- /Galary section -->
</div>

<!-- js for Galary -->
<link href="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/css/portfolio.css" rel="stylesheet" type="text/css" media="all" />
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/modernizr.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/classie2.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/font_end/js/cbpGridGallery.js"></script>
<script>
    new CBPGridGallery( document.getElementById( 'grid-gallery' ) );
</script>
<!-- /js for Galary -->