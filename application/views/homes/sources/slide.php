<div class="uk-grid uk-grid-small uk-margin-small-bottom" data-uk-grid-margin>


    <!--slide Start-->
    <div class="uk-width-large-3-4">

        <div class="uk-slidenav-position" data-uk-slideshow="autoplay:true">
            <ul class="uk-slideshow uk-overlay-active slideimg">
                <?php
                $i = 0;
                $slideshow_item = "";
                foreach ($slide_images as $row):
                    $slideshow_item .= '<li data-uk-slideshow-item="' . $i . '"><a href=""></a></li>'
                    ?>
                    <li>
                        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/images/slide/<?php echo $row['photo_location']; ?>"
                             height="auto"
                             alt="<?php echo $row['title']; ?>">
                    </li>
                    <?php $i++; endforeach; ?>
            </ul>

            <ul class="uk-dotnav uk-dotnav-contrast uk-float-right uk-position-bottom uk-flex-center">
                <?php echo $slideshow_item; ?>
            </ul>

        </div>
    </div>
    <!--slide complete-->

    <!--2nd section start-->
    <div class="uk-width-large-1-4">

        <div class="uk-panel uk-panel-box event">
            <p align="center">Notice / Events</p>
            <ul class="uk-list">

                <?php
                foreach ($home_page_notice as $row):
                    ?>
                    <li>
                        <a href="<?php echo base_url() . MEDIA_FOLDER; ?>/notice/<?php echo $row['url']; ?>"><?php echo $row['title']; ?>
                            <br/>
                            <span><i class="uk-icon-calendar"></i> <?php echo date("d-m-Y", strtotime($row['date'])); ?>
                                </a></li>
                <?php endforeach; ?>
            </ul>
        </div>

    </div>
    <!-- 2nd section delete-->

</div>