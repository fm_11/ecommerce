<script>
    $(function () {
        $("#date").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>

<div class="uk-width-large-2-4 midsection">
    <h3>Teacher Timekeeping</h3>
    <hr/>
    <div class="uk-margin">
        <form class="uk-form" action="<?php echo base_url(); ?>homes/getTeacherTimekeepingInfo" method="post">

            <?php
            $date = $this->session->userdata('date');
            $login_status = $this->session->userdata('login_status');
            if ($date != '') {
                $date = $date;
            } else {
                $date = "";
            }
            if ($login_status != '') {
                $login_status = $login_status;
            } else {
                $login_status = "";
            }
            ?>
            <fieldset data-uk-margin>
                <select class="uk-width-large-1-4" name="login_status" id="login_status">
                    <option value="">--Login Status--</option>
                    <option value="P" <?php if ($login_status == 'P') {
                        echo 'selected';
                    } ?>>Present
                    </option>
                    <option value="A" <?php if ($login_status == 'A') {
                        echo 'selected';
                    } ?>>Absent
                    </option>
                    <option value="L" <?php if ($login_status == 'L') {
                        echo 'selected';
                    } ?>>Leave
                    </option>
                </select>


                <input type="text" autocomplete="off"  class="uk-width-large-1-4" name="date" id="date" size="15" placeholder="YYYY-mm-dd"
                       value="<?php echo $date; ?>"/>

                <input type="submit" name="Search" class="uk-button uk-width-large-1-5" value="Search"/>
            </fieldset>
        </form>
    </div>
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
            <tr>
                <th>SL</th>
                <th>Name</th>
                <th>Index Number</th>
                <th>Post</th>
                <th>Date</th>
                <th>Login Status</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = (int)$this->uri->segment(3);
            foreach ($teacher_timekeeping_info as $row):
                $i++;
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['teacher_index_no']; ?></td>
                    <td><?php echo $row['post_name']; ?></td>
                    <td><?php echo $row['date']; ?></td>
                    <td>
                        <?php
                        if ($row['login_status'] == 'A') {
                            echo 'Absent';
                        } else if ($row['login_status'] == 'P') {
                            echo 'Present';
                        } else if ($row['login_status'] == 'L') {
                            echo 'Leave';
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <!--  PAGINATION START -->
    <div class="pagination uk-text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
    <!--  PAGINATION END-->

</div>

<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/all.css">
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/jquery.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/core.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/widget.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/calender/datepicker.js"></script>



