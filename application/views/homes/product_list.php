<!-- Products -->
 <section class="padding-top-40 padding-bottom-60">
   <div class="container">
     <div class="row">
       <!-- Products -->
       <div class="col-md-12">

         <!-- Short List -->
         <div class="short-lst">
           <h2>Product List</h2>
         </div>

         <!-- Items -->
         <div class="item-col-4">

           <?php
           $i = 0;
           foreach ($products as $row):
             $i++;
             ?>
           <!-- Product -->
           <div class="product">
             <article>
               <img class="img-responsive" src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $row['feature_image_path'];?>" alt="" >
               <!-- Content -->
               <a href="<?php echo base_url(); ?>homes/details/<?php echo $row['id'];?>" class="tittle"><?php echo $row['product_name']; ?></a>
               <!-- Reviews -->
               <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i>
                 <i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star"></i>
                </p>
               <div class="price">৳ <?php echo $row['standard_price'];?></div>
               <a href="<?php echo base_url(); ?>cart/addToCart/<?php echo $row['id'];?>" class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
           </div>

          <?php endforeach; ?>



         </div>


       </div>
       <div class="float-right">
         <?php echo $links;?>
       </div>
     </div>
   </div>
 </section>
