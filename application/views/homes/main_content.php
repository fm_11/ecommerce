
    <!-- Weekly Featured -->
    <section class="padding-bottom-60">
      <div class="container">

        <!-- heading -->
        <div class="heading">
          <h2>Hot Deals</h2>
          <hr>
        </div>
        <!-- Items Slider -->
        <div class="item-slide-4 with-nav">
            <?php foreach ($hot_deal_product_list as  $value): ?>
          <!-- Product -->
          <div class="product">
            <article> <img class="img-responsive" src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $value['feature_image_path'];?>" alt="" >
              <!-- Content -->
              <span class="tag">Latop</span> <a href="<?php echo base_url(); ?>homes/details/<?php echo $value['id'];?>" class="tittle"><?php echo $value['product_name'];?></a>
              <!-- Reviews -->
              <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
              <div class="price"><?php echo $value['standard_price'];?> </div>
              <a href="<?php echo base_url(); ?>cart/addToCart/<?php echo $value['id'];?>" class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
          </div>
            <?php endforeach; ?>
        </div>
      </div>
    </section>

    <!-- Latest Products -->
    <section class="padding-bottom-60">
      <div class="container">

        <!-- heading -->
        <div class="heading">
          <h2>Latest Products</h2>
          <hr>
        </div>
        <!-- Items Slider -->
        <div class="item-slide-4 with-nav">
        <?php foreach ($all_product_list as  $value): ?>
          <div class="product">
            <article> <img class="img-responsive" src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $value['feature_image_path'];?>" alt="" >

              <span class="tag">Latop</span> <a href="<?php echo base_url(); ?>homes/details/<?php echo $value['id'];?>" class="tittle"><?php echo $value['product_name'];?></a>

              <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
              <div class="price"><?php echo $value['standard_price'];?> </div>
              <a href="<?php echo base_url(); ?>cart/addToCart/<?php echo $value['id'];?>" class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
          </div>

        <?php endforeach; ?>

        </div>
      </div>
    </section>

    <!-- Shipping Info -->
    <section class="shipping-info">
      <div class="container">
        <ul>

          <!-- Free Shipping -->
          <li>
            <div class="media-left"> <i class="flaticon-delivery-truck-1"></i> </div>
            <div class="media-body">
              <h5>Free Shipping (For Dhaka)</h5>
              <span>On order over Tk 100</span></div>
          </li>
          <!-- Money Return -->
          <li>
            <div class="media-left"> <i class="flaticon-arrows"></i> </div>
            <div class="media-body">
              <h5>Money Return</h5>
              <span>30 Days money return</span></div>
          </li>
          <!-- Support 24/7 -->
          <li>
            <div class="media-left"> <i class="flaticon-operator"></i> </div>
            <div class="media-body">
              <h5>Support 24/7</h5>
              <span>Hotline: +880179-664004</span></div>
          </li>
          <!-- Safe Payment -->
          <li>
            <div class="media-left"> <i class="flaticon-business"></i> </div>
            <div class="media-body">
              <h5>Safe Payment</h5>
              <span>Cash payment</span></div>
          </li>
        </ul>
      </div>
    </section>
