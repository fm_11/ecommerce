
    <!-- Ship Process -->
    <div class="ship-process padding-top-30 padding-bottom-30">
      <div class="container">
        <ul class="row">

          <!-- Step 1 -->
          <li class="col-sm-3">
            <div class="media-left"> <i class="fa fa-check"></i> </div>
            <div class="media-body"> <span>Step 1</span>
              <h6>Shopping Cart</h6>
            </div>
          </li>

          <!-- Step 2 -->
          <li class="col-sm-3">
            <div class="media-left"> <i class="fa fa-check"></i> </div>
            <div class="media-body"> <span>Step 2</span>
              <h6>Payment Methods</h6>
            </div>
          </li>

          <!-- Step 3 -->
          <li class="col-sm-3 current">
            <div class="media-left"> <i class="flaticon-delivery-truck"></i> </div>
            <div class="media-body"> <span>Step 3</span>
              <h6>Delivery Methods</h6>
            </div>
          </li>

          <!-- Step 4 -->
          <li class="col-sm-3">
            <div class="media-left"> <i class="fa fa-check"></i> </div>
            <div class="media-body"> <span>Step 4</span>
              <h6>Confirmation</h6>
            </div>
          </li>
        </ul>
      </div>
    </div>

    <!-- Payout Method -->
    <section class="padding-bottom-60">
      <div class="container">
        <!-- Payout Method -->
        <div class="pay-method">
          <div class="row">
            <?php if(!empty($error_msg)){ ?>
        <div class="col-md-12">
            <div class="alert alert-danger"><?php echo $error_msg; ?></div>
        </div>
        <?php } ?>
            <div class="col-md-6">

              <!-- Your information -->
              <div class="heading">
                <h2>Your information</h2>
                <hr>
              </div>
              <form method="post" action="<?php echo base_url(); ?>checkout/placeOrder">
                <div class="row">
                  <div class="col-sm-12">
                    <label> Name <span style="color:red">*</span>
                      <input class="form-control" type="text" name="name" required>
                    </label>
                  </div>

                  <div class="col-sm-12">
                    <label> Address <span style="color:red">*</span>
                      <input class="form-control" type="text" name="address" required>
                    </label>
                  </div>

                  <div class="col-sm-6">
                    <label> Contact Number <span style="color:red">*</span>
                      <input class="form-control" type="number" name="phone" required>
                    </label>
                  </div>

                  <div class="col-sm-6">
                    <label> Email <span style="color:red">*</span>
                      <input class="form-control" type="email" name="email" required>
                    </label>
                  </div>

                </div>

            </div>

            <!-- Select Your Transportation -->
            <div class="col-md-6">
              <div class="heading">
                <h2>Select Your Transportation</h2>
                <hr>
              </div>
              <div class="transportation">
                <div class="row">

                  <!-- Free Delivery -->
                  <div class="col-sm-6">
                    <div class="charges">
                      <h6>Free Delivery</h6>
                      <br>
                      <span>Minimum Order</span> <span class="deli-charges"> ৳1000 </span></div>
                  </div>

                  <!-- Free Delivery -->
                  <div class="col-sm-6">
                    <div class="charges select">
                      <h6>Fast Delivery</h6>
                      <br>
                      <span>In Dhaka</span> <span class="deli-charges"> City </span> </div>
                  </div>
                  <!-- Expert Delivery -->
                  <div class="col-sm-6">
                    <div class="charges">
                      <h6>Paid Delivery</h6>
                      <br>
                      <span>24 - 48 Hours</span> <span class="deli-charges"> ৳100 </span> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Button -->
        <div class="pro-btn">
          <a href="<?php echo base_url(); ?>homes/all_product" class="btn-round btn-light">
            Continue Shopping</a>
            <button type="submit" class="btn-round">Go Confirmation</button> </div>
      </div>
      </form>
    </section>
