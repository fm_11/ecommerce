            <!-- Products -->
            <section class="padding-top-40 padding-bottom-60">
               <div class="container">
                  <div class="row">
                     <!-- Shop Side Bar -->
                     <div class="col-md-3">
                        <div class="shop-side-bar">
                           <!-- Categories -->
                           <h6>Categories</h6>
                           <div class="checkbox checkbox-primary">
                              <ul>
                                <?php foreach ($all_sub_category_list as $value): ?>
                                 <li>
                                    <label for="cate1"> <?php echo $value['name']; ?> </label>
                                 </li>
                                 <?php endforeach; ?>
                              </ul>
                           </div>

                        </div>
                     </div>
                     <!-- Products -->
                     <div class="col-md-9">
                        <div class="product-detail">
                           <div class="product">
                              <div class="row">
                                 <!-- Slider Thumb -->
                                 <div class="col-xs-5">
                                    <article class="slider-item on-nav">
                                              <div id="slider" class="flexslider">
                                                <ul class="slides">

                                                  <?php if($product_details->image_path_1 != ''){ ?>
                                                    <li>
                                                      <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $product_details->image_path_1;?>" alt="">
                                                    </li>
                                                 <?php } ?>

                                                 <?php if($product_details->image_path_2 != ''){ ?>
                                                   <li>
                                                     <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $product_details->image_path_2;?>" alt="">
                                                   </li>
                                                <?php } ?>

                                                <?php if($product_details->image_path_3 != ''){ ?>
                                                  <li>
                                                    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $product_details->image_path_3;?>" alt="">
                                                  </li>
                                               <?php } ?>

                                               <?php if($product_details->image_path_4 != ''){ ?>
                                                 <li>
                                                   <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $product_details->image_path_4;?>" alt="">
                                                 </li>
                                              <?php } ?>


                                                </ul>
                                              </div>
                                              <div id="carousel" class="flexslider">
                                                <ul class="slides">
                                                  <?php if($product_details->image_path_1 != ''){ ?>
                                                    <li>
                                                      <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $product_details->image_path_1;?>" alt="">
                                                    </li>
                                                 <?php } ?>

                                                 <?php if($product_details->image_path_2 != ''){ ?>
                                                   <li>
                                                     <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $product_details->image_path_2;?>" alt="">
                                                   </li>
                                                <?php } ?>

                                                <?php if($product_details->image_path_3 != ''){ ?>
                                                  <li>
                                                    <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $product_details->image_path_3;?>" alt="">
                                                  </li>
                                               <?php } ?>

                                               <?php if($product_details->image_path_4 != ''){ ?>
                                                 <li>
                                                   <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $product_details->image_path_4;?>" alt="">
                                                 </li>
                                              <?php } ?>
                                                </ul>
                                              </div>
                                    </article>
                                 </div>
                                 <!-- Item Content -->
                                 <div class="col-xs-7">
                                    <h5><?php echo $product_details->product_name; ?></h5>
                                    <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                    <div class="row">
                                       <div class="col-sm-6"><span class="price">৳ <?php echo $product_details->standard_price; ?> </span></div>
                                       <div class="col-sm-6">
                                          <p>Availability: <span class="in-stock">In stock</span></p>
                                       </div>
                                    </div>
                                    <!-- List Details -->
                                    <ul class="bullet-round-list">
                                      <?php echo $product_details->short_description; ?>
                                    </ul>

                                    <!-- Quinty -->
                                    <form method="post" action="<?php echo base_url(); ?>cart/addToCart/<?php echo $product_details->id;?>">
                                      <div class="quinty">
                                         <input type="number" name="quantity" required value="1">
                                      </div>
                                      <button type="submit" class="btn-round"><i class="icon-basket-loaded margin-right-5"></i> Add to Cart</a>
                                   </form>
                                 </div>
                              </div>
                           </div>
                           <!-- Details Tab Section-->
                           <div class="item-tabs-sec">
                              <!-- Nav tabs -->
                              <ul class="nav" role="tablist">
                                 <li role="presentation" class="active"><a href="#pro-detil"  role="tab" data-toggle="tab">Product Details</a></li>

                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                 <div role="tabpanel" class="tab-pane fade in active" id="pro-detil">
                                    <!-- List Details -->
                                  <?php echo $product_details->description; ?>
                                 </div>
                                 <div role="tabpanel" class="tab-pane fade" id="cus-rev"></div>
                                 <div role="tabpanel" class="tab-pane fade" id="ship"></div>
                              </div>
                           </div>
                        </div>
                        <!-- Related Products -->
                        <section class="padding-top-30 padding-bottom-0">
                           <!-- heading -->
                           <div class="heading">
                              <h2>Related Products</h2>
                              <hr>
                           </div>
                           <!-- Items Slider -->
                           <div class="item-slide-4 with-nav">
                              <!-- Product -->

                              <?php
                              foreach ($related_product_list as $row):
                                ?>
                              <div class="product">
                                 <article>
                                    <img class="img-responsive" src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $row['image_path_1'];?>" alt="" >
                                    <!-- Content -->
                                    <a href="<?php echo base_url(); ?>homes/details/<?php echo $row['id'];?>" class="tittle"><?php echo $row['product_name']; ?></a>
                                    <!-- Reviews -->
                                    <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                                    <div class="price">৳ <?php echo $row['standard_price'];?> </div>
                                    <a href="<?php echo base_url(); ?>cart/addToCart/<?php echo $row['id'];?>" class="cart-btn"><i class="icon-basket-loaded"></i></a>
                                 </article>
                              </div>
                             <?php endforeach; ?>
                           </div>
                        </section>
                     </div>
                  </div>
               </div>
            </section>
