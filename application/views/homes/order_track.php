<br><br>
<?php if(empty($orders)){ ?>
  <!-- Oder-success -->
  <section>
    <div class="container">
      <!-- Payout Method -->

      <div class="order-success">
        <i style="color: red !important;border: 2px solid red !important;" class="fa fa-frown-o"></i>
        <h6>Sorry! No data found</h6>
        <a href="<?php echo base_url(); ?>homes/all_product" class="btn-round">Return to Shop</a> </div>
    </div>
  </section>


<?php }else{ ?>
<table class="table table-bordered" style="width: 60%;margin-bottom: 100px;margin:0px auto;">
    <tbody>
            <tr>
              <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold" colspan="3">Customer Information</td>
            </tr>
            <tr>
              <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> Name</td>
              <td colspan="2" style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> <?php echo $customer_info->name; ?></td>
            </tr>
            <tr>
                <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> Phone</td>
                <td colspan="2" style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold">  <?php echo $customer_info->phone; ?></td>
            </tr>
            <tr>
                <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> Email</td>
                <td  colspan="2" style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold">  <?php echo $customer_info->email; ?></td>
            </tr>

            <tr>
                <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> Shipping Address</td>
                <td  colspan="2" style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> <?php echo $customer_info->address; ?> </td>
            </tr>

            <tr>
              <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold" colspan="3">Order Details</td>
            </tr>

            <tr>
                <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> Status</td>
                <td colspan="2" style="background: red;color: white;padding:10px !important;text-align:left;font-size:16px;font-weight:bold">
                  <?php
                  if($customer_info->status == 'P'){
                    echo 'Pending';
                  }else if($customer_info->status == 'PRO'){
                    echo 'Processing';
                  }else if($customer_info->status == 'D'){
                    echo 'Delivered';
                  }else{
                    echo 'Canceled';
                  }
                   ?>
                </td>
            </tr>

            <tr>
              <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold" colspan="3">Order Items</td>
            </tr>


            <tr>
                <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> Product Name</td>
                <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> Quantity </td>
                <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> Amount </td>
            </tr>

            <?php
            foreach ($orders as $row):
                ?>

                <tr>
                    <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold">
                      <img style="height:80px;width:80px;" src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $row['image_path_1'];?>" alt="">
                       <?php echo $row['product_name']; ?>
                     </td>
                    <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold">  <?php echo $row['quantity']; ?> </td>
                    <td style="padding:10px !important;text-align:right;font-size:16px;font-weight:bold">  <?php echo $row['sub_total']; ?> </td>
                </tr>

            <?php endforeach; ?>

            <tr>
                <td style="padding:10px !important;text-align:left;font-size:16px;font-weight:bold"> Total</td>
                <td  colspan="2" style="padding:10px !important;text-align:right;font-size:16px;font-weight:bold"> <?php echo $customer_info->total; ?> </td>
            </tr>






    </tbody>



</table>
<?php } ?>
