<!Doctype html>
<html class="no-js" lang="en">

<!-- Mirrored from event-theme.com/themes/html/smarttech/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 09 Aug 2020 16:47:29 GMT -->
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="M_Adnan" />
<!-- Document Title -->
<title>Online Shopping In Bangladesh - Rockdeals.com.bd</title>

<!-- Favicon -->
<link rel="shortcut icon" href="<?php echo base_url() . MEDIA_FOLDER; ?>/website/images/favicon.html" type="image/x-icon">
<link rel="icon" href="<?php echo base_url() . MEDIA_FOLDER; ?>/website/images/favicon.html" type="image/x-icon">

<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . MEDIA_FOLDER; ?>/website/rs-plugin/css/settings.css" media="screen" />

<!-- StyleSheets -->
<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/website/css/ionicons.min.css">
<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/website/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/website/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/website/css/main.css">
<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/website/css/style.css">
<link rel="stylesheet" href="<?php echo base_url() . MEDIA_FOLDER; ?>/website/css/responsive.css">

<!-- Fonts Online -->
<link href="https://fonts.googleapis.com/css?family=Lato:100i,300,400,700,900" rel="stylesheet">

<!-- JavaScripts -->
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/js/vendors/modernizr.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>

<!-- Page Wrapper -->
<div id="wrap" class="layout-3">

  <!-- Top bar -->
  <div class="top-bar">
    <div class="container">
      <p>Welcome to Rock Deails BD</p>
      <div class="right-sec">
        <div class="social-top">
          <a href="https://www.facebook.com/Rockdealsbd/"><i class="fa fa-facebook"></i></a>
        </div>
      </div>
    </div>
  </div>

  <!-- Header -->
  <header>
    <div class="container">
      <div class="logo"> <a href="<?php echo base_url(); ?>homes"><img src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/images/logo-3.png" alt="" ></a> </div>
      <?php
        $category_id = $this->session->userdata('category_id');
        $product_name = $this->session->userdata('product_name');
      ?>
      <div class="search-cate">
        <form action="<?php echo base_url(); ?>homes/all_product" method="post">
            <select name="category_id" class="selectpicker">
              <option value="all"> All Categories</option>
              <?php foreach ($all_category_list as  $value): ?>
                    <option value="<?php echo $value['id'];?>" <?php if (isset($category_id)) {
                  if ($category_id == $value['id']) {
                      echo 'selected';
                  }
              } ?>> <?php echo $value['name'];?></option>
              <?php endforeach; ?>
            </select>
            <input type="text" name="product_name" value="<?php echo $product_name; ?>" placeholder="Search entire store here...">
            <button class="submit" type="submit"><i class="icon-magnifier"></i></button>
          </form>
      </div>

      <!-- Cart Part -->
      <!-- Cart Part -->
     <ul class="nav navbar-right cart-pop">
       <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="itm-cont"><?php echo count($cart_list);?></span> <i class="flaticon-shopping-bag"></i> <strong>My Cart</strong> <br>
         <span><?php echo count($cart_list);?> item(s) - <?php echo array_sum(array_column($cart_list,'subtotal'));?></span></a>
         <ul class="dropdown-menu">
           <?php foreach ($cart_list as  $value): ?>
             <li>
               <div class="media-left"> <a href="#." class="thumb"> <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $value['image'];?>" class="img-responsive" alt="" > </a> </div>
               <div class="media-body"> <a href="#." class="tittle"><?php echo $value['name'];?></a> <span><?php echo $value['price'];?> x <?php echo $value['qty'];?></span> </div>
             </li>
           <?php endforeach; ?>

           <!-- <li>
             <div class="media-left"> <a href="#." class="thumb"> <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/images/item-img-1-2.jpg" class="img-responsive" alt="" > </a> </div>
             <div class="media-body"> <a href="#." class="tittle">Funda Para Ebook 7" full HD</a> <span>250 x 1</span> </div>
           </li> -->
           <li class="btn-cart"> <a href="<?php echo base_url(); ?>cart" class="btn-round">View Cart</a> </li>
         </ul>
       </li>
     </ul>
    </div>

    <!-- Nav -->
    <nav class="navbar ownmenu">
      <div class="container">

        <!-- Categories -->
        <div class="cate-lst"> <a  data-toggle="collapse" class="cate-style" href="#cater"><i class="fa fa-list-ul"></i> Our Categories </a>
          <div class="cate-bar-in">
            <div id="cater" class="collapse">
              <ul>
                <?php foreach ($all_category_list as $value): ?>
                 <li class="sub-menu">
                   <a href="<?php echo base_url(); ?>homes/all_product?category_id=<?php echo $value['id'];?>"><?php echo $value['name'];?></a>
                   <ul>
                     <?php foreach ($value['sub_category'] as $key): ?>
                         <li><a href="<?php echo base_url(); ?>homes/all_product?category_id=<?php echo $value['id'];?>&sub_category_id=<?php echo $key['id'];?>"><?php echo $key['name'];?></a></li>
                     <?php endforeach; ?>
                   </ul>
                 </li>
               <?php endforeach; ?>
                </ul>
            </div>
          </div>
        </div>

        <!-- Navbar Header -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-open-btn" aria-expanded="false"> <span><i class="fa fa-navicon"></i></span> </button>
        </div>
        <!-- NAV -->
        <div class="collapse navbar-collapse" id="nav-open-btn">
          <ul class="nav">
            <li <?php if($this->uri->segment(1) == 'homes' && ($this->uri->segment(2) == 'index' || $this->uri->segment(2) == '')){ echo 'class="active"'; } ?>><a href="<?php echo base_url(); ?>homes">Home </a></li>
            <li <?php if($this->uri->segment(1) == 'homes' && $this->uri->segment(2) == 'all_product'){ echo 'class="active"'; } ?>><a href="<?php echo base_url(); ?>homes/all_product"> All Product </a></li>
            <!--<li class="dropdown"> <a href="index.html" class="dropdown-toggle" data-toggle="dropdown">Pages </a>
              <ul class="dropdown-menu multi-level animated-2s fadeInUpHalf">
                <li><a href="About.html"> About </a></li>
                <li><a href="LoginForm.html"> Login Form </a></li>
                <li><a href="GridProducts_3Columns.html"> Products 3 Columns </a></li>
                <li><a href="GridProducts_4Columns.html"> Products 4 Columns </a></li>
                <li><a href="ListProducts.html"> List Products </a></li>
                <li><a href="Product-Details.html"> Product Details </a></li>
                <li><a href="ShoppingCart.html"> Shopping Cart</a></li>
                <li><a href="PaymentMethods.html"> Payment Methods </a></li>
                <li><a href="DeliveryMethods.html"> Delivery Methods</a></li>
                <li><a href="Confirmation.html"> Confirmation </a></li>
                <li><a href="CheckoutSuccessful.html"> Checkout Successful </a></li>
                <li><a href="Error404.html"> Error404 </a></li>
                <li><a href="contact.html"> Contact </a></li>
                <li class="dropdown-submenu"><a href="#."> Dropdown Level </a>
                  <ul class="dropdown-menu animated-2s fadeInRight">
                    <li><a href="#.">Level 1</a></li>
                  </ul>
                </li>
              </ul>
            </li>!-->

            <li>
               <a  data-toggle="modal" data-target="#myModal" href="javascript:void">
                 <span style="padding: 8px;background: #f74b16;border-radius: 10px;">ORDER TRACKING</span>
               </a>
            </li>
          </ul>
        </div>

        <!-- NAV RIGHT -->
        <div class="nav-right"> <span class="call-mun"><i class="fa fa-phone"></i> <strong>Hotline:</strong> +880179-664004</span> </div>
      </div>
    </nav>
  </header>
