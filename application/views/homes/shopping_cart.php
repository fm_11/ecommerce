
<script>
// Update item quantity
function updateCartItem(obj, rowid){
    $.get("<?php echo base_url('cart/updateItemQty/'); ?>", {rowid:rowid, qty:obj.value}, function(resp){
        if(resp == 'ok'){
            location.reload();
        }else{
            alert('Cart update failed, please try again.');
        }
    });
}
</script>
  <!-- Content -->

    <!-- Ship Process -->
    <div class="ship-process padding-top-30 padding-bottom-30">
      <div class="container">
        <ul class="row">

          <!-- Step 1 -->
          <li class="col-sm-3 current">
            <div class="media-left"> <i class="flaticon-shopping"></i> </div>
            <div class="media-body"> <span>Step 1</span>
              <h6>Shopping Cart</h6>
            </div>
          </li>

          <!-- Step 2 -->
          <li class="col-sm-3">
            <div class="media-left"> <i class="flaticon-business"></i> </div>
            <div class="media-body"> <span>Step 2</span>
              <h6>Payment Methods</h6>
            </div>
          </li>

          <!-- Step 3 -->
          <li class="col-sm-3">
            <div class="media-left"> <i class="flaticon-delivery-truck"></i> </div>
            <div class="media-body"> <span>Step 3</span>
              <h6>Delivery Methods</h6>
            </div>
          </li>

          <!-- Step 4 -->
          <li class="col-sm-3">
            <div class="media-left"> <i class="fa fa-check"></i> </div>
            <div class="media-body"> <span>Step 4</span>
              <h6>Confirmation</h6>
            </div>
          </li>
        </ul>
      </div>
    </div>

    <!-- Shopping Cart -->
    <section class="shopping-cart padding-bottom-60">
      <div class="container">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">SL</th>
              <th>Items</th>
              <th class="text-center">Price</th>
              <th class="text-center">Quantity</th>
              <th class="text-center">Total Price </th>
              <th>&nbsp; </th>
            </tr>
          </thead>
          <tbody>
           <?php $grand_total=0;  $i = 1; foreach ($cart as $value): ?>
             <tr>
               <td>
                   <?php echo $i; ?>
               </td>
               <td><div class="media">
                   <div class="media-left"> <a href="#."> <img class="img-responsive" src="<?php echo base_url() . MEDIA_FOLDER; ?>/product/<?php echo $value['image'];?>" alt="" > </a> </div>
                   <div class="media-body">
                     <p><?php echo $value['name'];?></p>
                   </div>
                 </div></td>
               <td class="text-center padding-top-60"><?php echo $value['price'];?></td>
               <td class="text-center"><!-- Quinty -->

                 <div class="quinty padding-top-20">
                   <input type="number" value="<?php echo $value['qty'];?>" onchange="updateCartItem(this, '<?php echo $value["rowid"]; ?>')">
                 </div></td>
               <td class="text-center padding-top-60"><?php echo $value['subtotal'];?></td>
               <td class="text-center padding-top-60"><a href="#." class="remove" onclick="return confirm('Are you sure to delete item?')?window.location.href='<?php echo base_url('cart/removeItem/'.$value["rowid"]); ?>':false;" ><i class="fa fa-close"></i></a></td>
             </tr>
             <?php $grand_total=$grand_total+$value['subtotal'];   $i++;?>
           <?php endforeach; ?>

            <!-- <tr>
              <td><div class="media">
                  <div class="media-left"> <a href="#."> <img class="img-responsive" src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/images/item-img-1-2.jpg" alt="" > </a> </div>
                  <div class="media-body">
                    <p>E-book Reader Lector De Libros
                      Digitales 7''</p>
                  </div>
                </div></td>
              <td class="text-center padding-top-60">$200.00</td>
              <td class="text-center"><div class="quinty padding-top-20">
                  <input type="number" value="02">
                </div></td>
              <td class="text-center padding-top-60">$400.00</td>
              <td class="text-center padding-top-60"><a href="#." class="remove"><i class="fa fa-close"></i></a></td>
            </tr> -->
          </tbody>
        </table>

        <!-- Promotion -->
        <div class="promo">


          <!-- Grand total -->
          <div class="g-totel">
            <h5>Grand total: <span><?php echo $grand_total;?></span></h5>

          </div>
        </div>

        <!-- Button -->
        <div class="pro-btn"> <a href="<?php echo base_url(); ?>homes/all_product" class="btn-round btn-light">Continue Shopping</a> <a href="<?php echo base_url(); ?>homes/delivery_methods" class="btn-round">Go Payment Methods</a> </div>
      </div>
    </section>
