 <hr>
<!-- End Content -->
  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">

        <!-- Contact -->
        <div class="col-md-4">
          <h4>Contact Rock Deals BD!</h4>
          <p>Address: Ka27/G, Shahid Abdul Aziz Sharak, Jagannathpur, Vatara.
Dhaka, Bangladesh 1229</p>
          <p>Phone: +880179664004</p>
          <p>Email: support@rockdeals.com.bd</p>
           <div class="social-links"> <a href="https://www.facebook.com/Rockdealsbd/"><i class="fa fa-facebook"></i></a>
					</div>

        </div>

        <!-- Categories -->
        <div class="col-md-3">
          <h4>Categories</h4>
          <ul class="links-footer">
						<?php foreach ($all_category_list as $value): ?>
							 <li><a href="<?php echo base_url(); ?>homes/all_product?category_id=<?php echo $value['id'];?>"><?php echo $value['name'];?></a></li>
						<?php endforeach; ?>

            <!-- <li><a href="#."> TV & Video</a></li>
            <li><a href="#."> Camera, Photo & Video</a></li>
            <li><a href="#."> Cell Phones & Accessories</a></li>
            <li><a href="#."> Headphones</a></li>
            <li><a href="#."> Video Games</a></li>
            <li><a href="#."> Bluetooth & Wireless</a></li> -->
          </ul>
        </div>

        <!-- Categories -->
        <div class="col-md-3">
          <h4>Customer Services</h4>
          <ul class="links-footer">
            <li><a href="#.">Shipping & Returns</a></li>
            <li><a href="#."> Secure Shopping</a></li>
            <li><a href="#."> International Shipping</a></li>
            <li><a href="#."> Affiliates</a></li>
            <li><a href="#."> Contact </a></li>
          </ul>
        </div>

        <!-- Categories -->
        <div class="col-md-2">
          <h4>Information</h4>
          <ul class="links-footer">
            <li><a href="#.">Our Blog</a></li>
            <li><a href="#."> About Our Shop</a></li>
            <li><a href="#."> Secure Shopping</a></li>
            <li><a href="#."> Delivery infomation</a></li>
            <li><a href="#."> Store Locations</a></li>
            <li><a href="#."> FAQs</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <!-- Rights -->
  <div class="rights">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <p>Copyright © 2020 <a href="www.rockdeals.com.bd" class="ri-li"> Rock Deals BD </a></p>
        </div>
        <div class="col-sm-6 text-right"> <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/images/card-icon.png" alt=""> </div>
      </div>
    </div>
  </div>

  <!-- End Footer -->

  <!-- GO TO TOP  -->
  <a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a>
  <!-- GO TO TOP End -->
</div>
<!-- End Page Wrapper -->

<!-- JavaScripts -->
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/js/vendors/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/js/vendors/wow.min.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/js/vendors/bootstrap.min.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/js/vendors/own-menu.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/js/vendors/jquery.sticky.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/js/vendors/owl.carousel.min.js"></script>

<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/rs-plugin/js/jquery.tp.t.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/rs-plugin/js/jquery.tp.min.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/js/main.js"></script>
<script src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/js/vendors/jquery.nouislider.min.js"></script>
<script>
jQuery(document).ready(function($) {

  //  Price Filter ( noUiSlider Plugin)
    $("#price-range").noUiSlider({
    range: {
      'min': [ 0 ],
      'max': [ 1000 ]
    },
    start: [40, 940],
        connect:true,
        serialization:{
            lower: [
        $.Link({
          target: $("#price-min")
        })
      ],
      upper: [
        $.Link({
          target: $("#price-max")
        })
      ],
      format: {
      // Set formatting
        decimals: 2,
        prefix: '$'
      }
        }
  })
})

</script>
</body>

<!-- Mirrored from event-theme.com/themes/html/smarttech/index-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 09 Aug 2020 16:48:09 GMT -->
</html>
