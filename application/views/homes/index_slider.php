<!-- Slid Sec -->
<section class="slid-sec with-bg-wide" >
	<!-- Main Slider Start -->
	<div class="tp-banner-container">
		<div class="tp-banner-full">
			<ul>

				<?php
          $i = 0;
          foreach ($slide_images as $row):
        ?>
				<!-- SLIDE  -->
				<li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/images/trans-bg.png"  alt="slider"  data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat">

					<!-- LAYER NR. 1 -->
					<div class="tp-caption sft tp-resizeme"
																	 data-x="left" data-hoffset="780"
																	 data-y="center" data-voffset="-100"
																	 data-speed="800"
																	 data-start="800"
																	 data-easing="Power3.easeInOut"
																	 data-splitin="none"
																	 data-splitout="none"
																	 data-elementdelay="0.03"
																	 data-endelementdelay="0.4"
																	 data-endspeed="300"
																	 style="z-index: 5; font-size:24px; font-weight:500; color:#888888;  max-width: auto; max-height: auto; white-space: nowrap;">
																			<?php echo $row['title']; ?>
																 </div>

					<!-- LAYER NR. 2 -->
					<div class="tp-caption sfr tp-resizeme"
																	 data-x="left" data-hoffset="780"
																	 data-y="center" data-voffset="-40"
																	 data-speed="800"
																	 data-start="1000"
																	 data-easing="Power3.easeInOut"
																	 data-splitin="chars"
																	 data-splitout="none"
																	 data-elementdelay="0.03"
																	 data-endelementdelay="0.1"
																	 data-endspeed="300"
																	 style="z-index: 6; font-size:36px; color:#f74b16; font-weight:800; white-space: nowrap;">
																	 	<?php echo $row['text_1']; ?>
																  </div>

					<!-- LAYER NR. 1 -->

					<div class="tp-caption sfl tp-resizeme"
																	 data-x="left" data-hoffset="780"
																	 data-y="center" data-voffset="15"
																	 data-speed="800"
																	 data-start="800"
																	 data-easing="Power3.easeInOut"
																	 data-splitin="chars"
																	 data-splitout="none"
																	 data-elementdelay="0.03"
																	 data-endelementdelay="0.4"
																	 data-endspeed="300"
																	 style="z-index: 5; font-size:20px; font-weight:normal; color:#888888;
																	   max-width: auto; max-height: auto; white-space: nowrap;">
																		 <?php echo $row['text_2']; ?>
																	  </div>

						<!-- LAYER NR. 1 -->
					<div class="tp-caption sfl tp-resizeme"
																	 data-x="left" data-hoffset="300"
																	 data-y="center" data-voffset="0"
																	 data-speed="800"
																	 data-start="1300"
																	 data-easing="Power3.easeInOut"
																	 data-elementdelay="0.1"
																	 data-endelementdelay="0.1"
																	 data-endspeed="300"
																	 data-scrolloffset="0"
																	 style="z-index: 1;">
		        <img src="<?php echo base_url() . MEDIA_FOLDER; ?>/website/images/slide/<?php echo $row['photo_location']; ?>" alt="" > </div>

					<!-- LAYER NR. 4 -->
					<div class="tp-caption lfb tp-resizeme"
																	 data-x="left" data-hoffset="780"
																	 data-y="center" data-voffset="90"
																	 data-speed="800"
																	 data-start="1300"
																	 data-easing="Power3.easeInOut"
																	 data-elementdelay="0.1"
																	 data-endelementdelay="0.1"
																	 data-endspeed="300"
																	 data-scrolloffset="0"
																	 style="z-index: 10;"><a href="<?php echo base_url(); ?>homes/all_product" class="btn-round">Shop Now</a> </div>
				</li>

				<?php endforeach; ?>

			</ul>
		</div>
	</div>
</section>
