<?php

class Student extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    //student ID create
	public function getStudentCode($class_id, $group_id, $year, $id_segment_year)
	{
		$config_info = $this->db->query("SELECT * FROM `tbl_student_code_config`")->result_array();
		if (empty($config_info)) {
			$sdata['exception'] = "Data not found for student auto ID configuration";
			$this->session->set_userdata($sdata);
			redirect('dashboard/index');
		}

		$segment_1 = $config_info[0]['segment_1'];
		$segment_2 = $config_info[0]['segment_2'];
		$segment_3 = $config_info[0]['segment_3'];
		$segment_4 = $config_info[0]['segment_4'];
		$auto_inc_code_length = $config_info[0]['auto_inc_code_length'];
		$auto_increament_option = $config_info[0]['auto_increament_option'];
		$code_separator = $config_info[0]['code_separator'];

		$generating_code = "";
		$separator = "";

		//code_separator
		if ($code_separator != 'NA') {
			$separator = $code_separator;
		}

		//segment_1
		if ($segment_1 != 'NA') {
			$generating_code .= $segment_1 . $separator;
		}

		//segment_2
		if ($segment_2 == 'YF') {
			$generating_code .= $id_segment_year . $separator;
		} else {
			$generating_code .= substr($id_segment_year, -2) . $separator;
		}

		//segment_3
		if ($segment_3 == 'CS') {
			$class_info = $this->db->query("SELECT student_code_short_form FROM `tbl_class` WHERE id = '$class_id'")->result_array();
			$generating_code .= $class_info[0]['student_code_short_form'] . $separator;
		}


		if ($segment_4 == 'Y') {
			$group_info = $this->db->query("SELECT code FROM `tbl_student_group` WHERE id = '$group_id'")->result_array();
			$generating_code = $class_info[0]['student_code_short_form'] . $separator . substr($id_segment_year, -2) . $separator . $group_info[0]['code'] . $separator;
		}

		//auto_increament_option
		$where = "";
		if ($auto_increament_option == 'CW') {
			$where = "class_id = '$class_id' AND year = '$year'";
		} elseif ($auto_increament_option == 'FSW') {
			$where = "year = '$year'";
		}

		if ($segment_4 == 'Y') {
			$where .= " AND `group` = '$group_id'";
		}

		$total_student = $this->db->query("SELECT count(id) as total FROM `tbl_student` WHERE $where")->result_array();
		$total_student = $total_student[0]['total'] + 1;

		//$increament_num = str_pad($total_student, $auto_inc_code_length, '0', STR_PAD_LEFT);
		//$generating_code .= $increament_num;
		$validated_generating_code = $this->verify_student_code($total_student, $auto_inc_code_length, $generating_code);
		return $validated_generating_code;
	}

	public function verify_student_code($total_student, $auto_inc_code_length, $generating_code_pre)
	{
		$increament_num = str_pad($total_student, $auto_inc_code_length, '0', STR_PAD_LEFT);
		$generating_code = $generating_code_pre . $increament_num;
		$student_info = $this->db->query("SELECT id FROM `tbl_student` WHERE student_code = '$generating_code'")->result_array();
		if (!empty($student_info)) {
			//  echo 55;
			//  die;
			return $this->verify_student_code($total_student + 1, $auto_inc_code_length, $generating_code_pre);
		} else {
			return $generating_code;
		}
	}
	//student ID create

    public function get_student_info_by_student_code($student_code)
    {
        return $this->db->where('student_code', $student_code)->get('tbl_student')->row();
    }

    public function get_all_uploaded_result($limit, $offset, $value = '')
    {
        $this->db->select('tbl_student_uploaded_result.*');
        $this->db->from('tbl_student_uploaded_result');
        if (isset($value) && !empty($value)) {
            $this->db->where('tbl_student_uploaded_result.status', $value['status']);
        }
        $this->db->order_by("tbl_student_uploaded_result.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }




    public function position_search($positions, $field, $value)
    {
        foreach ($positions as $key => $position) {
            if ($position[$field] === $value) {
                return $key;
            }
        }
        return false;
    }


    public function result_all_student_after_process($exam_id, $class_id, $section_id, $group, $shift_id, $student_id, $roll_where = '')
    {
        //echo $roll_where; die;
        $where = "";
        if ($student_id != 0) {
            $where = " AND rp.`student_id` = '$student_id' ";
        }
        if($roll_where != '' && $student_id == 0){
			$where = " AND rp.`roll_no` IN($roll_where) ";
		}

        $result_process = $this->db->query("SELECT s.`id`,s.`name`,s.`photo`,s.`qr_code`,s.`father_name`,
											s.`mother_name`,s.`student_code`,s.`date_of_birth`,
											rp.`id` AS result_id,
											rp.`roll_no`,
											rp.`total_obtain_mark`,
											rp.`gpa_with_optional`,
											rp.`gpa_without_optional`,
											rp.`c_alpha_gpa_with_optional`,
											rp.`c_alpha_gpa_without_optional`,
											rp.`average_percentage_for_final`,
											rp.`average_mark_for_final`,
											rp.`calculable_subject`,
											rp.`calculable_total_grade_point`,
											rp.`class_position`,
											rp.`shift_position`,
											rp.`section_position`,
											rp.`group_position`,
											rp.`number_of_failed_subject`,
											rp.`is_annual_exam`,
											rp.`grand_average_mark`,
											rp.`grand_average_cgpa`,
											rp.`grand_average_grade`,
											rp.`total_credit`
							FROM `tbl_result_process` AS rp
							INNER JOIN `tbl_student` AS s ON s.`id` = rp.`student_id`
							WHERE rp.`exam_id` = '$exam_id' AND rp.`class_id` = '$class_id'
							AND rp.`section_id` = '$section_id' AND rp.`group` = '$group'
							 AND rp.`shift` = '$shift_id' $where
							ORDER BY ABS(rp.`roll_no`) ASC")->result_array();


//        echo '<pre>';
//         print_r($result_process); die;

        if (empty($result_process)) {
            return null;
        }


        $result_data = array();
        $i = 0;
        foreach ($result_process as $row):
        $result_data[$i]['student_id'] = $row['id'];
        $result_data[$i]['result_id'] = $row['result_id'];
        $result_data[$i]['student_name'] = $row['name'];
        $result_data[$i]['student_code'] = $row['student_code'];
        $result_data[$i]['father_name'] = $row['father_name'];
        $result_data[$i]['mother_name'] = $row['mother_name'];
        $result_data[$i]['date_of_birth'] = $row['date_of_birth'];
        $result_data[$i]['photo'] = $row['photo'];
        $result_data[$i]['qr_code'] = $row['qr_code'];
        $result_data[$i]['roll_no'] = $row['roll_no'];
        $result_data[$i]['total_obtain_mark'] = $row['total_obtain_mark'];
        $result_data[$i]['total_credit'] = $row['total_credit'];
        $result_data[$i]['gpa_with_optional'] = $row['gpa_with_optional'];
        $result_data[$i]['number_of_failed_subject'] = $row['number_of_failed_subject'];
        $result_data[$i]['class_position'] =  $row['class_position'];
		$result_data[$i]['shift_position'] =  $row['shift_position'];
		$result_data[$i]['section_position'] =  $row['section_position'];
		$result_data[$i]['group_position'] =  $row['group_position'];
        $result_data[$i]['grand_average_mark'] =  $row['grand_average_mark'];
        $result_data[$i]['average_percentage_for_final'] =  $row['average_percentage_for_final'];
        $result_data[$i]['average_mark_for_final'] =  $row['average_mark_for_final'];
        $result_data[$i]['gpa_without_optional'] = $row['gpa_without_optional'];
        $result_data[$i]['c_alpha_gpa_with_optional'] = $row['c_alpha_gpa_with_optional'];
        $result_data[$i]['c_alpha_gpa_without_optional'] = $row['c_alpha_gpa_without_optional'];
        $result_data[$i]['is_annual_exam'] = $row['is_annual_exam'];

        $student_id = $row['id'];

        $result_process_details = $this->db->query("SELECT * FROM `tbl_result_process_details` AS rp
WHERE rp.`exam_id` = '$exam_id' AND rp.`student_id` = '$student_id' AND rp.`class_id` = '$class_id' AND rp.`section_id` = '$section_id' AND rp.`group` = '$group' AND
rp.`shift` = '$shift_id' ORDER BY rp.`merge_code` = 0 ,rp.`merge_code`,rp.`order_number` ASC")->result_array();



        $total_class_test_marks = 0;
		$total_written_marks = 0;
		$total_objective_marks = 0;
		$total_practical_marks = 0;

        foreach ($result_process_details as $details_row):

                     $sub = $details_row['subject_id'];
        $stu_id = $row['id'];
        $extra_head = $this->db->query("SELECT exa.*,exh.name as head_name FROM `tbl_class_wise_extra_head_amount` as exa
                             INNER JOIN tbl_exam_marking_extra_head as exh on exa.head_id = exh.id WHERE exa.class_id = '$class_id' ORDER BY exh.id ASC")->result_array();

        if (!empty($extra_head)) {
            $extra_head_mount = $this->db->query("SELECT * FROM `tbl_student_wise_extra_mark` WHERE exam_id = '$exam_id' AND student_id = '$stu_id' AND subject_id = '$sub'")->result_array();
            if (!empty($extra_head_mount)) {
                foreach ($extra_head as $col_row):
                                foreach ($extra_head_mount as $mark_row):
                                    if ($mark_row['head_id'] == $col_row['head_id']) {
                                        $result_data[$i]['result'][$details_row['subject_code']]['extra_mark'][$col_row['head_id']]['mark'] =  $mark_row['mark'];
                                        $result_data[$i]['result'][$details_row['subject_code']]['extra_mark'][$col_row['head_id']]['subject_id'] =  $mark_row['subject_id'];
                                    }
                endforeach;
                endforeach;
            } else {
                foreach ($extra_head as $col_row):
                                    $result_data[$i]['result'][$details_row['subject_code']]['extra_mark'][$col_row['head_id']]['mark'] =  0;
                $result_data[$i]['result'][$details_row['subject_code']]['extra_mark'][$col_row['head_id']]['subject_id'] = $details_row['subject_id'];
                endforeach;
            }
        }

        $result_data[$i]['result'][$details_row['subject_code']]['subject_id'] = $details_row['subject_id'];
        $result_data[$i]['result'][$details_row['subject_code']]['subject_name'] = $details_row['subject_name'];
        $result_data[$i]['result'][$details_row['subject_code']]['subject_code'] = $details_row['subject_code'];
        $result_data[$i]['result'][$details_row['subject_code']]['credit'] = $details_row['credit'];
        $result_data[$i]['result'][$details_row['subject_code']]['written'] = $details_row['written'];
        $result_data[$i]['result'][$details_row['subject_code']]['class_test'] = $details_row['class_test'];
        $total_class_test_marks += $details_row['class_test'];
		$total_written_marks += $details_row['written'];
		$total_objective_marks += $details_row['objective'];
		$total_practical_marks += $details_row['practical'];
        $result_data[$i]['result'][$details_row['subject_code']]['objective'] = $details_row['objective'];
        $result_data[$i]['result'][$details_row['subject_code']]['practical'] = $details_row['practical'];
        $result_data[$i]['result'][$details_row['subject_code']]['calculable_total_obtain'] = $details_row['calculable_total_obtain'];
        $result_data[$i]['result'][$details_row['subject_code']]['total_number_get_from_other_exam'] = $details_row['total_number_get_from_other_exam'];
        $result_data[$i]['result'][$details_row['subject_code']]['total_obtain'] = $details_row['total_obtain'];
        $result_data[$i]['result'][$details_row['subject_code']]['alpha_gpa'] = $details_row['alpha_gpa'];
        $result_data[$i]['result'][$details_row['subject_code']]['numeric_gpa'] = $details_row['numeric_gpa'];
        $result_data[$i]['result'][$details_row['subject_code']]['is_optional'] = $details_row['is_optional'];
		$result_data[$i]['result'][$details_row['subject_code']]['is_absent'] = $details_row['is_absent'];
		$result_data[$i]['result'][$details_row['subject_code']]['merge_code'] = $details_row['merge_code'];
		$result_data[$i]['result'][$details_row['subject_code']]['merge_subject_id'] = $details_row['merge_subject_id'];
		$result_data[$i]['result'][$details_row['subject_code']]['subject_type'] = $details_row['subject_type'];
        endforeach;
        $result_data[$i]['total_class_test_marks'] = $total_class_test_marks;
		$result_data[$i]['total_written_marks'] = $total_written_marks;
		$result_data[$i]['total_objective_marks'] = $total_objective_marks;
		$result_data[$i]['total_practical_marks'] = $total_practical_marks;
        $i++;
        endforeach;
        //echo '<pre>';
        // print_r($result_data); die;
        return $result_data;
    }


    public function result_by_student_after_process($student_id, $exam_id)
    {
        //echo $student_id.'/'.$exam_id; die;
        $data = array();
        $data['result_process_info'] = $this->db->query("SELECT rp.*,s.`name`,s.`student_code`,s.`roll_no`,s.`father_name`,s.`mother_name`,s.`date_of_birth`,
c.`name` AS class_name,sc.`name` AS section_name,sg.`name` AS group_name
FROM `tbl_result_process` AS rp
INNER JOIN `tbl_student` AS s ON s.`id` = rp.`student_id`
LEFT JOIN `tbl_class` AS c ON c.`id` = rp.`class_id`
LEFT JOIN `tbl_section` AS sc ON sc.`id` = rp.`section_id`
LEFT JOIN `tbl_student_group` AS sg ON sg.`id` = rp.`group`
WHERE rp.`exam_id` = '$exam_id' AND rp.`student_id` = '$student_id'")->result_array();
        if (empty($data['result_process_info'])) {
            //echo 77; die;
            return null;
        } else {
            $result_id = $data['result_process_info'][0]['id'];
            $data['result_process_details'] = $this->db->query("SELECT * FROM `tbl_result_process_details` WHERE `result_id` = '$result_id' AND `exam_id` = '$exam_id' AND `student_id` = '$student_id' ORDER BY `order_number`")->result_array();
            if (empty($data['result_process_details'])) {
                return null;
            }
            return $data;
            //echo '<pre>';
            //print_r($data['result_process_details']); die;
        }
    }




    public function getGrandResultForStudent($student_id)
    {
        $year = 2017;
        $student_info = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code` FROM `tbl_student` AS s
WHERE s.`id` = '$student_id'")->result_array();

        $exam_list = $this->db->query("SELECT * FROM `tbl_exam` order by id")->result_array();

        $rinfo = array();

        $j = 0;
        while ($j < count($student_info)) {
            $rinfo[$j]['id'] = $student_info[$j]['id'];
            $rinfo[$j]['name'] = $student_info[$j]['name'];
            $rinfo[$j]['roll_no'] = $student_info[$j]['roll_no'];
            $rinfo[$j]['student_code'] = $student_info[$j]['student_code'];

            $student_id = $student_info[$j]['id'];
            $total_mark = 0;
            $total_average_mark = 0;

            $k = 0;
            while ($k < count($exam_list)) {
                $exam_id = $exam_list[$k]['id'];
                $exam_name = $exam_list[$k]['name'];
                $mark_info = $this->db->query("SELECT * FROM `tbl_grand_exam_result` WHERE student_id = '$student_id' AND exam_id = '$exam_id' AND year = '$year'")->result_array();



                $result_info = array();
                if (empty($mark_info)) {
                    $result_info['total_obtain_mark'] = 0;
                    $result_info['average_mark'] = 0;
                    $result_info['exam_id'] = $exam_id;
                    $result_info['exam_name'] = $exam_name;
                } else {
                    $result_info['total_obtain_mark'] = $mark_info[0]['total_obtain_marks'];
                    $result_info['average_mark'] =  $mark_info[0]['average_mark'];
                    $total_mark += $mark_info[0]['total_obtain_marks'];
                    $total_average_mark += $mark_info[0]['average_mark'];
                    $result_info['exam_id'] = $exam_id;
                    $result_info['exam_name'] = $exam_name;
                }
                $rinfo[$j]['marks_info'][$k] = $result_info;
                $k++;
            }
            $rinfo[$j]['total_mark'] =  $total_mark;
            $rinfo[$j]['total_average_mark'] =  number_format($total_average_mark, 2);
            $rinfo[$j]['cgpa'] =  number_format($this->number_to_num_grade($total_average_mark, 100), 2);
            $rinfo[$j]['grade'] = $this->number_to_alfa_grade($total_average_mark, 100);


            $j++;
        }


        // echo '<pre>';
        // print_r($rinfo);
        // die;
        return $rinfo;
    }




    //result

    public function getGrandResult($data)
    {
        $class_id = $data['class_id'];
        $section_id = $data['section_id'];
        $group = $data['group'];
        $shift_id = $data['shift_id'];
        $year = $data['year'];

        if ($year == date('Y')) {
            $student_info = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code` FROM `tbl_student` AS s
                WHERE s.`class_id` = '$class_id' AND  s.`shift_id` = '$shift_id' AND s.`section_id` = '$section_id'
                AND s.`group` = '$group' AND s.year = '$year' AND s.`status` = 1 ORDER BY s.roll_no")->result_array();
        } else {
            $student_info = $this->db->query("SELECT s.id,mt.name,s.student_code,mt.old_roll_no as roll_no FROM tbl_student_data_migrate_history AS mt
                                                INNER JOIN tbl_student AS s ON s.id = mt.student_id
                                                 WHERE mt.old_year = '$year' AND mt.old_class_id = '$class_id' AND mt.old_section_id = '$section_id'
                                                 AND mt.`old_group` = '$group' AND  mt.`old_shift` = '$shift_id' ORDER BY mt.old_roll_no")->result_array();
        }


        //  $student_info = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code` FROM `tbl_student` AS s
        //WHERE s.`class_id` = '$class_id' AND  s.`shift_id` = '$shift_id' AND s.`section_id` = '$section_id'
        //AND s.`group` = '$group' AND s.year = '$year' AND s.`status` = 1 ORDER BY s.roll_no")->result_array();

        $exam_list = $this->db->query("SELECT * FROM `tbl_exam` WHERE is_applicable_for_final_calcultion = 1 AND year = '$year' order by exam_order")->result_array();
        //  echo '<pre>';
        // print_r($exam_list);
        // die;

        $rinfo = array();

        $j = 0;
        while ($j < count($student_info)) {
            $rinfo[$j]['id'] = $student_info[$j]['id'];
            $rinfo[$j]['name'] = $student_info[$j]['name'];
            $rinfo[$j]['roll_no'] = $student_info[$j]['roll_no'];
            $rinfo[$j]['student_code'] = $student_info[$j]['student_code'];

            $student_id = $student_info[$j]['id'];
            $total_mark = 0;
            $total_average_mark = 0;

            $k = 0;
            while ($k < count($exam_list)) {
                $exam_id = $exam_list[$k]['id'];
                $exam_name = $exam_list[$k]['name'];
                $is_annual_exam =  $exam_list[$k]['is_annual_exam'];
                $annual_position = "";
                $mark_info = $this->db->query("SELECT * FROM `tbl_result_process` WHERE student_id = '$student_id' AND exam_id = '$exam_id' AND year = '$year'")->result_array();
                //echo $this->db->last_query();
                //  echo '<pre>';
                //  print_r($mark_info);
                //  die;

                $result_info = array();
                if (empty($mark_info)) {
                    $result_info['total_obtain_mark'] = 0;
                    $result_info['average_mark'] = 0;
                    $result_info['exam_id'] = $exam_id;
                    $result_info['exam_name'] = $exam_name;
                } else {
                    $result_info['total_obtain_mark'] = $mark_info[0]['total_obtain_mark'];
                    $result_info['average_mark'] =  $mark_info[0]['average_mark'];
                    $total_mark += $mark_info[0]['total_obtain_mark'];
                    $total_average_mark += $mark_info[0]['average_mark'];
                    $result_info['exam_id'] = $exam_id;
                    $result_info['exam_name'] = $exam_name;
                    if ($is_annual_exam == 1) {
                        $annual_position =  $mark_info[0]['annual_position'];
                    }
                }
                $rinfo[$j]['marks_info'][$k] = $result_info;
                $k++;
            }
            $rinfo[$j]['total_mark'] =  $total_mark;
            $rinfo[$j]['total_average_mark'] =  number_format($total_average_mark, 4);
            $rinfo[$j]['cgpa'] =  number_format($this->number_to_num_grade($total_average_mark, 100), 4);
            $rinfo[$j]['grade'] = $this->number_to_alfa_grade($total_average_mark, 100);
            $rinfo[$j]['annual_position'] =  $annual_position;

            $j++;
        }

        return $rinfo;
        //echo '<pre>';
      //print_r($rinfo);
      //die;
    }

    public function number_to_num_grade($Number, $Credit)
    {
        $Value = ($Number / $Credit);
        if (($Value >= '0.8')) {
            return 5;
        } elseif (($Value >= '0.7' && $Value < '0.8')) {
            return 4;
        } elseif (($Value >= '0.6' && $Value < '0.7')) {
            return 3.5;
        } elseif (($Value >= '0.5' && $Value < '0.6')) {
            return 3;
        } elseif (($Value >= '0.4' && $Value < '0.5')) {
            return 2;
        } elseif (($Value >= '0.33' && $Value < '0.4')) {
            return 1;
        } elseif (($Value < '0.33')) {
            return 0;
        }
    }

    public function number_to_alfa_grade($Number, $Credit)
    {
        $Value = ($Number / $Credit);
        if (($Value >= '0.8')) {
            return 'A+';
        } elseif (($Value >= '0.7' && $Value < '0.8')) {
            return "A";
        } elseif (($Value >= '0.6' && $Value < '0.7')) {
            return "A-";
        } elseif (($Value >= '0.5' && $Value < '0.6')) {
            return "B";
        } elseif (($Value >= '0.4' && $Value < '0.5')) {
            return "C";
        } elseif (($Value >= '0.33' && $Value < '0.4')) {
            return "D";
        } elseif (($Value < '0.33')) {
            return "F";
        }
    }


    //result


    public function get_all_exam($limit, $offset, $value = '')
    {
        $this->db->select('tbl_exam.*,t.name as exam_type');
        $this->db->from('tbl_exam');
        $this->db->join('tbl_exam_type AS t', 'tbl_exam.exam_type_id=t.id', 'left');
        $this->db->order_by("tbl_exam.exam_order", "asc");
        $this->db->order_by("tbl_exam.year", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_uploaded_routine($limit, $offset, $value = '')
    {
        $this->db->select('r.*,c.name as class_name,s.name as section_name');
        $this->db->from('tbl_student_class_routine as r');
        $this->db->join('tbl_class AS c', 'r.class_id=c.id', 'left');
        $this->db->join('tbl_section AS s', 'r.section_id=s.id', 'left');
        if (isset($value) && !empty($value)  && $value['class_id'] != '') {
            $this->db->where('r.class_id', $value['class_id']);
        }
        if (isset($value) && !empty($value) && $value['section_id'] != '') {
            $this->db->where('r.section_id', $value['section_id']);
        }
        $this->db->order_by("r.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_uploaded_syllabus($limit, $offset, $value = '')
    {
        $this->db->select('s.*,c.name as class_name');
        $this->db->from('tbl_syllabus as s');
        $this->db->join('tbl_class AS c', 's.class_id=c.id', 'left');
        if (isset($value) && !empty($value)  && $value['class_id'] != '') {
            $this->db->where('s.class_id', $value['class_id']);
        }
        if (isset($value) && !empty($value) && $value['year'] != '') {
            $this->db->where('s.year', $value['year']);
        }
        $this->db->order_by("s.page_no", "asc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

	public function getStudentCategory()
	{
		return $this->db->get('tbl_student_category')->result_array();
	}


    public function get_all_student_list($limit, $offset, $value = '')
    {
        //echo '<pre>';
        //print_r($value); die;
        $this->db->select('s.*,c.name as class_name,sc.name as section_name,cat.name as category_name,
        g.name as group_name,sf.name as shift_name,bg.name as blood_group,r.name as religion_name');
        $this->db->from('tbl_student as s');
        $this->db->join('tbl_class AS c', 's.class_id=c.id', 'left');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id', 'left');
        $this->db->join('tbl_shift AS sf', 's.shift_id=sf.id', 'left');
        $this->db->join('tbl_student_group AS g', 's.group=g.id', 'left');
        $this->db->join('tbl_blood_group AS bg', 's.blood_group_id=bg.id', 'left');
		$this->db->join('tbl_religion AS r', 's.religion=r.id', 'left');
		$this->db->join('tbl_student_category AS cat', 's.category_id=cat.id', 'left');
        if (isset($value) && !empty($value) && isset($value['class_id'])  && $value['class_id'] != '') {
            $this->db->where('s.class_id', $value['class_id']);
        }
		if (isset($value) && !empty($value) && isset($value['shift_id']) && $value['shift_id'] != '') {
			$this->db->where('s.shift_id', $value['shift_id']);
		}
        if (isset($value) && !empty($value) && isset($value['section_id']) && $value['section_id'] != '') {
            $this->db->where('s.section_id', $value['section_id']);
        }
        if (isset($value) && !empty($value) && isset($value['group']) && $value['group'] != '') {
            $this->db->where('s.group', $value['group']);
        }
        if (isset($value) && !empty($value) && isset($value['roll']) && $value['roll'] != '') {
            $this->db->like('s.roll_no', $value['roll']);
            $this->db->or_like('s.student_code', $value['roll']);
            $this->db->or_like('s.name', $value['roll']);
        }

        if (isset($value) && !empty($value) && isset($value['religion']) && $value['religion'] != '') {
            $this->db->where('s.religion', $value['religion']);
        }
        if (isset($value) && !empty($value) && isset($value['gender']) && $value['gender'] != '') {
            $this->db->where('s.gender', $value['gender']);
        }
        if (isset($value) && !empty($value) && isset($value['blood_group_id']) && $value['blood_group_id'] != '') {
            $this->db->where('s.blood_group_id', $value['blood_group_id']);
        }

        if (isset($value) && !empty($value) && isset($value['shift_id']) && $value['shift_id'] != '') {
            $this->db->where('s.shift_id', $value['shift_id']);
        }

        if (isset($value) && !empty($value) && isset($value['student_status']) && $value['student_status'] != '') {
            $this->db->where('s.status', $value['student_status']);
        }

        $this->db->where('s.is_deleted', 0);
        $this->db->order_by("c.name", "asc");
        $this->db->order_by("ABS(s.roll_no)", "asc");
        $this->db->order_by("sc.name", "asc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_student_info_by_student_id($id)
    {
        $this->db->select('s.*,c.name as class_name,ac.name as admitted_class_name,
        c.name_bangla as class_name_bangla,sc.name as section_name,sf.name as shift_name,
        g.name as group_name,bg.name as blood_group,r.name as religion_name');
        $this->db->from('tbl_student as s');
        $this->db->join('tbl_class AS c', 's.class_id=c.id', 'left');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id', 'left');
		$this->db->join('tbl_shift AS sf', 's.shift_id=sf.id', 'left');
        $this->db->join('tbl_student_group AS g', 's.group=g.id', 'left');
        $this->db->join('tbl_blood_group AS bg', 's.blood_group_id=bg.id', 'left');
		$this->db->join('tbl_class AS ac', 's.admitted_class_id=ac.id', 'left');
		$this->db->join('tbl_religion AS r', 's.religion=r.id', 'left');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function routine_upload($data)
    {
        return $this->db->insert('tbl_student_class_routine', $data);
    }

    public function add_upload_result($data)
    {
        return $this->db->insert('tbl_student_uploaded_result', $data);
    }

    public function update_uploaded_result_status($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_student_uploaded_result', $data);
    }

    public function update_exam_status($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_exam', $data);
    }


    public function get_uploaded_result_info_by_result_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_student_uploaded_result')->result_array();
    }

    public function get_uploaded_syllabus_info_by_syllabus_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_syllabus')->result_array();
    }

    public function get_uploaded_routine_info_by_routine_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_student_class_routine')->result_array();
    }


    public function delete_uploaded_result_info_by_result_id($id)
    {
        return $this->db->delete('tbl_student_uploaded_result', array('id' => $id));
    }


    public function delete_uploaded_syllabus_info_by_syllabus_id($id)
    {
        return $this->db->delete('tbl_syllabus', array('id' => $id));
    }


    public function delete_uploaded_routine_info_by_routine_id($id)
    {
        return $this->db->delete('tbl_student_class_routine', array('id' => $id));
    }


    public function getStudentTimeKeepingInformation($limit, $offset, $value = '')
    {
        $this->db->select('s.*,c.name as class_name,sc.name as section_name,sl.login_status,sl.date,g.name as group_name');
        $this->db->from('tbl_student as s');
        $this->db->join('tbl_student_logins AS sl', 's.id=sl.student_id', 'left');
        $this->db->join('tbl_class AS c', 's.class_id=c.id', 'left');
        $this->db->join('tbl_section AS sc', 's.section_id=sc.id', 'left');
        $this->db->join('tbl_student_group AS g', 's.group=g.id', 'left');
        if (isset($value) && !empty($value)  && $value['class_id'] != '') {
            $this->db->where('s.class_id', $value['class_id']);
        }
        if (isset($value) && !empty($value) && $value['section_id'] != '') {
            $this->db->where('s.section_id', $value['section_id']);
        }
        if (isset($value) && !empty($value) && $value['group'] != '') {
            $this->db->where('s.group', $value['group']);
        }
        if (isset($value) && !empty($value) && $value['date'] != '') {
            $this->db->where('sl.date', $value['date']);
        }
        $this->db->where('s.status', 1);
        $this->db->order_by("sl.date", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_applied_student($limit, $offset, $value = '')
    {
        $this->db->select('a.*,c.name as class_name');
        $this->db->from('tbl_admission as a');
        $this->db->join('tbl_class AS c', 'a.class_id=c.id', 'left');
        if (isset($value) && !empty($value)  && $value['class_id'] != '') {
            $this->db->where('a.class_id', $value['class_id']);
        }
        if (isset($value) && !empty($value)  && $value['pin'] != '') {
            $this->db->where('a.pin', $value['pin']);
        }
        if (isset($value) && !empty($value) && $value['year'] != '') {
            $this->db->where('a.year', $value['year']);
        }
        $this->db->order_by("a.date_of_application", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
}
