<?php

class Salary_type extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_salary_type()
    {
        $this->db->select('*');
        $this->db->from('tbl_salary_types');
        $this->db->order_by("report_order", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_other_allow_deduc_type()
    {
      $this->db->select('*');
      $this->db->from('tbl_other_allow_deduc_types');
      $this->db->order_by("report_order", "asc");
      $query = $this->db->get();
      return $query->result_array();
    }

    public function get_other_allow_deduc_type_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_other_allow_deduc_types')->row();
    }

    public function get_salary_type_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_salary_types')->row();
    }
}
