<?php

class Home extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }
	
	function get_all_MC_member($limit, $offset, $value = ''){
		$this->db->select('tbl_managing_committee.*');
        $this->db->from('tbl_managing_committee');
		$this->db->order_by("tbl_managing_committee.id", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
	}
	
	function get_all_staff($limit, $offset, $value = ''){
		$this->db->select('si.*,tp.name AS post_name,ts.name AS section_name');
        $this->db->from('tbl_staff_info as si');
        $this->db->join('tbl_teacher_post AS tp', 'tp.id = si.post','left');
        $this->db->join('tbl_teacher_section AS ts', 'ts.id = si.section','left');
		$this->db->order_by("si.id", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
	}
}

?>