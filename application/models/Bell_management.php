<?php

class Bell_management extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }

	 function get_all_bell_list($limit, $offset, $value = ''){
        $this->db->select('b.*');
		$this->db->from('tbl_school_bell as b');       
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
}
