<?php

class E_commerce extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }

//Basic_info

    function get_all_basic_info($limit, $offset, $value = '')
    {
        $this->db->select('b.*');
        $this->db->from('tbl_basic_info as b');

        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('b.name', $value['name']);
        }
        $this->db->order_by("b.id", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add_basic_info($data)
    {
        return $this->db->insert('tbl_basic_info', $data);
    }

    public function get_basic_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_basic_info')->row();
    }

    //Category

    public function get_category_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('tbl_category as s');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        $this->db->order_by("s.order_no", "asc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_category($data)
    {
        return $this->db->insert('tbl_category', $data);
    }


    function edit_category($data, $id)
    {
        return $this->db->update('tbl_category', $data, array('id' => $id));
    }


    function read_category($id)
    {
        return $this->db->get_where('tbl_category', array('id' => $id))->row();
    }

    function delete_category($id)
    {
        return $this->db->delete('tbl_category', array('id' => $id));
    }

    public function checkifexist_category_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM tbl_category where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_category_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM tbl_category where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    //Sub Category

    public function get_all_sub_category($limit, $offset, $value = '')
  	{
  		$this->db->select('sc.*,c.name as category_name');
  		$this->db->from('tbl_sub_category as sc');
  		$this->db->join('tbl_category AS c', 'sc.category_id=c.id');
  		$this->db->order_by("sc.order_no", "asc");
  		if (isset($limit) && $limit > 0) {
  			$this->db->limit($limit, $offset);
  		}
  		$query = $this->db->get();
  		return $query->result_array();
  	}

    function add_subcategory($data)
    {
        return $this->db->insert('tbl_sub_category', $data);
    }


    function edit_subcategory($data, $id)
    {
        return $this->db->update('tbl_sub_category', $data, array('id' => $id));
    }


    function read_subcategory($id)
    {
        return $this->db->get_where('tbl_sub_category', array('id' => $id))->row();
    }

    function delete_subcategory($id)
    {
        return $this->db->delete('tbl_sub_category', array('id' => $id));
    }

    public function checkifexist_subcategory_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM tbl_sub_category where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_subcategory_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM tbl_sub_category where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    //Delivery_info

    public function get_delivery_info_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('tbl_delivery_info as s');

        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_delivery_info($data)
    {
        return $this->db->insert('tbl_delivery_info', $data);
    }

    function read_delivery_info($id)
    {
        return $this->db->get_where('tbl_delivery_info', array('id' => $id))->row();
    }

    function delete_delivery_info($id)
    {
        return $this->db->delete('tbl_delivery_info', array('id' => $id));
    }

    //Mail Configuration

    public function get_mail_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('tbl_mail_configuration as s');

        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function read_mail_info($id)
    {
        return $this->db->get_where('tbl_mail_configuration', array('id' => $id))->row();
    }
    function delete_mail_info($id)
    {
        return $this->db->delete('tbl_mail_configuration', array('id' => $id));
    }

    //Customer_code

    public function get_customer_code_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('s.*');
        $this->db->from('tbl_customer_code as s');

        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function read_customer_code_info($id)
    {
        return $this->db->get_where('tbl_customer_code', array('id' => $id))->row();
    }

    function delete_customer_code_info($id)
    {
        return $this->db->delete('tbl_customer_code', array('id' => $id));
    }
    function get_all_category_list_by_type($type){
      $data   = array();
      if($type=='h')//hot deal
      {
        $data   = $this->db->get_where('tbl_category', array('is_hot_deal' => 1))->result_array();
      }elseif ($type=='m') //main menu
      {
        $data   = $this->db->get_where('tbl_category', array('is_main_menu' => 1))->result_array();
      }else{
        $data   = $this->db->get('tbl_category')->result_array();
      }

      foreach( $data as $key=>$each ){

          $data[$key]['sub_category']   = $this->db->where('category_id', $each['id'])->get('tbl_sub_category')->result_array();
      }

      return $data;
  }
  public function get_all_product_info_list()
  {
    $data = $this->db->query("SELECT * FROM `tbl_product_info`")->result_array();
    return $data;
  }

  public function get_all_product_sub_category_list()
  {
    $data = $this->db->query("SELECT * FROM `tbl_sub_category`")->result_array();
    return $data;
  }

  }

  ?>
