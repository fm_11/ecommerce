<?php

class Message extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
        date_default_timezone_set('Asia/Dhaka');
    }

	public function get_all_voice_audio_list($limit, $offset, $value = '')
	{
		$this->db->select('a.*');
		$this->db->from('tbl_audio_sms as a');
		$this->db->order_by("a.id", "desc");
		if (isset($limit) && $limit > 0) {
			$this->db->limit($limit, $offset);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	function get_all_voice_sms_request_list($limit, $offset, $value = ''){
		$this->db->select('r.*,a.title');
		$this->db->from('tbl_voice_sms_request as r');
		$this->db->join('tbl_audio_sms AS a', 'a.id=r.audio_id');
		$this->db->order_by("r.id", "desc");
		if (isset($limit) && $limit > 0) {
			$this->db->limit($limit, $offset);
		}
		$query = $this->db->get();
		return $query->result_array();
	}


    public function getSmsBalance()
    {
        $data = array();
        $message_config = $this->db->query("SELECT * FROM tbl_message_config")->result_array();
        if (!empty($message_config)) {
            if ($message_config[0]['user_id'] != '') {
                //echo 555;die;
                $url = "http://api.smscpanel.net/g_api.php?token=". $message_config[0]['api_key'] ."&balance";
                $ch = curl_init(); // Initialize cURL
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_ENCODING, '');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
                $smsresult = curl_exec($ch);
                $data['sms_balance_amount'] = $smsresult;
            } else {
                $api_url = $message_config[0]['server_url'] . "/miscapi/" . $message_config[0]['api_key'] . "/getBalance";
                $ch = curl_init($api_url);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $result = curl_exec($ch);
                curl_close($ch);
                $rdata = explode(":", $result);
                //echo '<pre>';
                // print_r($rdata);
                //die;
                $data['sms_quantity'] = ($rdata[1] / .50);
                $data['sms_balance_amount'] =  $rdata[1];
            }
        } else {
            $data['sms_balance_amount'] = "Not Configured";
            $data['sms_quantity'] = "Not Configured";
        }
        return $data;
    }

	public function getAvailableSmsBalance(){
		$total_in = $this->db->query("SELECT SUM(`quantity`) AS total FROM `tbl_sms_balance_transaction` 
                                     WHERE `type` = 'I'")->row();

		$total_out = $this->db->query("SELECT SUM(`quantity`) AS total FROM `tbl_sms_balance_transaction` 
                                     WHERE `type` = 'O'")->row();
		if($total_in->total == ''){
			$total_in->total = 0;
		}
		if($total_out->total == ''){
			$total_out->total = 0;
		}
		return $total_in->total - $total_out->total;
	}

    public function get_student_info_for_result($class_id,$shift_id,$section_id,$group_id,$exam_id)
    {
        $result = $this->db->query("SELECT r.`student_id`,s.`name`,s.`father_name`,s.`guardian_mobile`,
s.`student_code`,r.`roll_no`,r.`c_alpha_gpa_with_optional` FROM `tbl_result_process` AS r
INNER JOIN `tbl_student` AS s ON s.`id` = r.`student_id`
WHERE r.`class_id` = $class_id AND r.`shift` = $shift_id AND r.`section_id` = $section_id 
AND r.`group` = $group_id AND r.`exam_id` = $exam_id ORDER BY ABS(r.`roll_no`);")->result_array();
        return $result;
    }

    public function get_all_phone_book($limit, $offset, $value = '')
    {
        $this->db->select('ph.*,c.name as category_name');
        $this->db->from('tbl_message_contact_list as ph');
        $this->db->join('tbl_message_category AS c', 'ph.category_id=c.id');

        $this->db->order_by("ph.id", "asc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_all_teachers()
    {
        $this->db->select('tbl_teacher.*,tp.name as post_name,ts.name as section_name');
        $this->db->from('tbl_teacher');
        $this->db->join('tbl_teacher_post AS tp', 'tbl_teacher.post=tp.id');
        $this->db->join('tbl_teacher_section AS ts', 'tbl_teacher.section=ts.id');
        $this->db->where('tbl_teacher.status', 1);
        $this->db->order_by("tbl_teacher.id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }


    public function single_sms_send($sms_info)
    {
        $message_config = $this->db->query("SELECT * FROM tbl_message_config")->result_array();
        if (empty($message_config)) {
            $sdata['exception'] = "Message panel configuration not found to database.";
            $this->session->set_userdata($sdata);
            redirect("dashboard/index");
        }

        $server_url = $message_config[0]['server_url'];
        $api_key = $message_config[0]['api_key'];
        $form_number = $message_config[0]['form_number'];


        $contact = $sms_info['number'];
        $from = $form_number;
        $sms_text = $sms_info['message'];
        //echo $sms_text;
        //die;

        $ch = "$server_url/smsapi?api_key=$api_key&type=text&contacts=$contact&senderid=$from&msg=" . urlencode($sms_text);
        ///echo $ch;
        // die;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $ch);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        //print $result;
        //die;

        $cdata = array();
        $cdata['sms_shoot_id'] = $result;
        $cdata['date'] = date('Y-m-d H:i:s');
        if ($this->db->insert('tbl_message_sms_shoot', $cdata)) {
            return true;
        }
    }


    public function sms_send($sms_info)
    {
        $message_config = $this->db->query("SELECT * FROM tbl_message_config")->result_array();
        if (empty($message_config)) {
            $sdata['exception'] = "Message panel configuration not found to database.";
            $this->session->set_userdata($sdata);
            redirect("dashboard/index");
        }
		$contact = $sms_info['numbers'];
		$sms_text = $sms_info['message'];
		$message = $sms_text;

        //balance calculation
		$number_array = explode(",", $contact);
		$total_send_number = count($number_array);
		$checkLanguage = mb_detect_encoding($message);
		$stringLength = mb_strlen($message, 'utf8');
		if($checkLanguage == 'UTF-8'){
			$totalSMS = ceil($stringLength / 67);
		}else{
			$totalSMS = ceil($stringLength / 160);
		}

		$sms_balance = $this->getAvailableSmsBalance();
        if($sms_balance <= $totalSMS){
			$sdata['exception'] = "There is not enough balance in your account";
			$this->session->set_userdata($sdata);
			redirect("dashboard/index");
		}

        $server_url = $message_config[0]['server_url'];
        //$api_key = $message_config[0]['api_key'];
		$api_key = "839c6f33f5db3afec7490f950818f6ca";


		$to = $contact;
		$token = $api_key;
	   // echo $message; die;

		$url = $server_url;


		$data= array(
		'to'=>"$to",
		'message'=>"$message",
		'token'=>"$token"
		); // Add parameters in key value
		$ch = curl_init(); // Initialize cURL
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$smsresult = curl_exec($ch);
//             echo '<pre>';
//             print_r($smsresult); die;
		$cdata = array();
		$cdata['sms_shoot_id'] = $smsresult;
		$cdata['history_id'] = $sms_info['history_id'];
		$cdata['numbers'] = $to;
		$cdata['date'] = date('Y-m-d H:i:s');
		if ($this->db->insert('tbl_message_sms_shoot', $cdata)) {
			$insert_id = $this->db->insert_id();
			//balance update
			$t_data = array();
			$t_data['type'] = 'O';
			$t_data['quantity'] = $totalSMS * $total_send_number;
			$t_data['date'] = date('Y-m-d');
			$t_data['reference_id'] = $insert_id;
			$this->db->insert('tbl_sms_balance_transaction', $t_data);

			return true;
		}

    }

    public function smsResponseDataSave($response,$template_id,$send_from,$numbers_with_person_id){
		$decoded_response = json_decode($response, true);
//		echo '<pre>';
//		print_r($decoded_response);
//		die;
		$total_message_length = 0;
		$response_data_array = array();
		$i = 0;
		$csms_id = 0;
		foreach ($decoded_response['smsinfo'] as $row){
			$response_data_array[$i]['date_time'] = date('Y-m-d H:i:s');
			$response_data_array[$i]['msisdn'] = $row['msisdn'];
			$response_data_array[$i]['sms_body'] = $row['sms_body'];
			$response_data_array[$i]['sms_type'] = $row['sms_type'];
			$response_data_array[$i]['stakeholder'] = "SPATEINON";
			$response_data_array[$i]['sms_status'] = $row['sms_status'];
			$response_data_array[$i]['status_message'] = $row['status_message'];
			$response_data_array[$i]['csms_id'] = $row['csms_id'];
			$csms_id = $row['csms_id'];
			$response_data_array[$i]['reference_id'] = $row['reference_id'];
			$response_data_array[$i]['sender_type'] = $send_from;

			$message_length = $this->smsCount($row['sms_body']);
			if(strtolower($row['sms_status']) != 'success'){
				$trim_number = trim($row['msisdn']);
			}else{
				if(strlen(trim($row['msisdn'])) > 11){
					$trim_number = substr(trim($row['msisdn']),strlen(trim($row['msisdn'])) - 11);
				}else{
					$trim_number = trim($row['msisdn']);
				}
				$total_message_length += $message_length;
			}

			if(isset($numbers_with_person_id[$trim_number]['student_id'])){
				$response_data_array[$i]['student_id'] = $numbers_with_person_id[$trim_number]['student_id'];
			}else{
				$response_data_array[$i]['student_id'] = 0;
			}

			if(isset($numbers_with_person_id[$trim_number]['teacher_id'])){
				$response_data_array[$i]['teacher_id'] = $numbers_with_person_id[$trim_number]['teacher_id'];
			}else{
				$response_data_array[$i]['teacher_id'] = 0;
			}

			$response_data_array[$i]['template_id'] = $template_id;
			$response_data_array[$i]['number_of_sms'] = $message_length;
			$i++;
		}
		$this->db->insert_batch('tbl_sms_response', $response_data_array);

		//balance deduction
		if($total_message_length > 0){
			$t_data = array();
			$t_data['type'] = 'O';
			$t_data['quantity'] = $total_message_length;
			$t_data['date'] = date('Y-m-d');
			$t_data['csms_id'] = $csms_id;
			$this->db->insert('tbl_sms_balance_transaction', $t_data);
		}
		//balance deduction

		$return_data = array();
		$return_data['status'] = $decoded_response['status'];
		$return_data['status_code'] = $decoded_response['status_code'];
		$return_data['error_message'] = $decoded_response['error_message'];
		return $return_data;
    }


	public function dynamicSMSSend($messageData, $send_from, $numbers_with_person_id,$total_num_of_sms_for_dynamic_send){
//		echo '<pre>';
//		print_r($messageData);
//		die;
    	$this->validateSMSBalance(0,$total_num_of_sms_for_dynamic_send);
    	$response = $this->dynamicSms($messageData);
		return $this->smsResponseDataSave($response,0,$send_from,$numbers_with_person_id);
	}

	public function bulkSMSSend($numbers, $template_id, $template_body, $send_from, $numbers_with_person_id){
		$message_length = $this->smsCount($template_body);
    	$this->validateSMSBalance(count($numbers),$message_length);
    	$batchCsmsId = $this->generateSMSTransactionID(); // csms id must be unique
		$response = $this->bulkSmsSendBySSL($numbers, $template_body, $batchCsmsId);
		return $this->smsResponseDataSave($response,$template_id,$send_from,$numbers_with_person_id);
	}


	public function validateSMSBalance($total_send_number,$message_length){
		$message_config = $this->db->query("SELECT * FROM tbl_message_config")->result_array();
		if (empty($message_config)) {
			$sdata['exception'] = "Message panel configuration not found to database.";
			$this->session->set_userdata($sdata);
			redirect("dashboard/index");
		}
		$sms_balance = $this->getAvailableSmsBalance();
		if($total_send_number > 0){
			$total_message_length = $message_length * $total_send_number;
		}else{
			$total_message_length = $message_length;
		}

		if($sms_balance <= $total_message_length){
			$sdata['exception'] = "There is not enough balance in your account";
			$this->session->set_userdata($sdata);
			redirect("dashboard/index");
		}
		return true;
	}

	function smsCount($sms_text){
		$checkLanguage = mb_detect_encoding($sms_text);
		$stringLength = mb_strlen($sms_text, 'utf8');
		if($checkLanguage == 'UTF-8'){
			$message_length = ceil($stringLength / 67);
		}else{
			$message_length = ceil($stringLength / 160);
		}
		return $message_length;
	}

	function dynamicSms($messageData)
	{
		$params = array(
			"api_token" => "SPATEi-3a28c396-65a5-4312-a2a3-3926ea524e6b",
			"sid" => "SPATEINON",
			"sms" => $messageData,
		);
		$params = json_encode($params);
		$url = "https://smsplus.sslwireless.com/api/v3/send-sms/dynamic";
		return $this->callSSLSMSApi($url, $params);
	}

    public function bulkSmsSendBySSL($msisdns, $messageBody, $batchCsmsId){
		$params = array(
			"api_token" => "SPATEi-3a28c396-65a5-4312-a2a3-3926ea524e6b",
			"sid" => "SPATEINON",
			"msisdn" => $msisdns,
			"sms" => $messageBody,
			"batch_csms_id" => $batchCsmsId
		);
		$url = "https://smsplus.sslwireless.com/api/v3/send-sms/bulk";
		$params = json_encode($params);
		return $this->callSSLSMSApi($url, $params);
	}


	function callSSLSMSApi($url, $params)
	{
		$ch = curl_init(); // Initialize cURL
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($params),
			'accept:application/json'
		));

		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	function generateSMSTransactionID(){
		$code = "";
		$total_send_sms = count($this->db->query("SELECT id FROM tbl_sms_response")->result_array());
		$code .= $total_send_sms;
		$code .= $this->generateRandomString(6);
		return  $code;
	}

	function generateRandomString($length = 2) {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}



	public function DoCommonInsert($data, $tableName)
    {
        /* echo '<pre>';
        print_r($data);
        echo $tableName;
        die(); */
        $this->db->set($data);
        $insertresult = $this->db->insert($tableName);
        if ($insertresult) {
            $return['status'] = 1;
        } else {
            $return['status'] = 0;
        }
        return $return;
    }


    //////// edit by dip (07-06-17)////////////////
    public function GetStudentInfoByBirthday()
    {
        /* SELECT tbl_class.name,tbl_section.name,tbl_student_group.name,tbl_student.id,tbl_student.name,tbl_student.roll_no,tbl_student.gender,tbl_student.guardian_mobile FROM
        tbl_student AS a INNER JOIN tbl_class AS b ON b.id=a.class_id
INNER JOIN tbl_section AS c ON c.id=a.section_id
INNER JOIN tbl_student_group AS d ON d.id=a.group WHERE date_of_birth='1992-10-02' */

        $date = date('m-d');
        $this->db->select('tbl_class.id AS classId,tbl_class.name as class,tbl_section.id AS sectionId,tbl_section.name as section,tbl_student_group.id AS groupId,tbl_student_group.name as groupName,tbl_student.id,tbl_student.name,tbl_student.roll_no,tbl_student.gender,tbl_student.guardian_mobile,tbl_student.student_code');
        $this->db->from('tbl_student');
        $this->db->join('tbl_class', 'tbl_class.id = tbl_student.class_id');
        $this->db->join('tbl_section', 'tbl_section.id = tbl_student.section_id');
        $this->db->join('tbl_student_group', 'tbl_student_group.id = tbl_student.group');
        $this->db->where("date_of_birth LIKE '%" . $date . "%'");
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            $studentData = $result->result_array();
            /* echo '<pre>';
            print_r($studentData);
            die(); */
            $return['status'] = 1;
            $return['studentData'] = $studentData;
        } else {
            $return['status'] = 0;
        }
        return $return;
    }
    //////// edit by dip (07-06-17)////////////////
    //////// edit by dip (09-06-17)////////////////
    public function GetTeacherInfoByBirthday()
    {
        $date = date('m-d');
        //SELECT id,NAME, FROM tbl_teacher WHERE date_of_birth LIKE '%06-08%'
        $this->db->select('id,name,mobile,gender,subject');
        $this->db->from('tbl_teacher');
        $this->db->where("date_of_birth LIKE '%" . $date . "%'");
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            $studentData = $result->result_array();
            /* echo '<pre>';
            print_r($studentData);
            die(); */
            $return['status'] = 1;
            $return['teacherData'] = $studentData;
        } else {
            $return['status'] = 0;
        }
        return $return;
    }
    //////// edit by dip (09-06-17)////////////////
    //////// edit by dip (11-06-17)////////////////
    public function GetCategoryInfo($CategoryType)
    {
        $where = array(
            'category_id' => $CategoryType
        );
        $this->db->select('id,contact_name,contact_number');
        $this->db->from('tbl_message_contact_list');
        $this->db->where($where);
        $result = $this->db->get();
        if ($result->num_rows($result) > 0) {
            $CategoryData = $result->result_array();
            /* echo '<pre>';
            print_r($studentData);
            die(); */
            $return['status'] = 1;
            $return['CategoryData'] = $CategoryData;
        } else {
            $return['status'] = 0;
        }
        $Categorywhere = array(
            'id' => $CategoryType
        );
        $this->db->select('name');
        $this->db->from('tbl_message_category');
        $this->db->where($Categorywhere);
        $result = $this->db->get();
        $Categoryname = $result->row_array();

        $return['name'] = $Categoryname['name'];
        $return['category_id'] = $CategoryType;
        return $return;
    }

    public function GetGuardianSMSReport($FromDate, $ToDate)
    {
        $this->db->select('c.name AS studentName,a.mobile_number,a.date,d.name AS className,e.name AS sectionName,f.name AS groupName,g.template_name,b.template_body');
        $this->db->from('tbl_message_to_guardian_student_list AS a');
        $this->db->join('tbl_message_to_guardian AS b', 'b.id=a.to_guardian_message_id');
        $this->db->join('tbl_student AS c', 'c.id=a.student_id');
        $this->db->join('tbl_class AS d', 'd.id=b.class_id');
        $this->db->join('tbl_section AS e', 'e.id=b.section_id');
        $this->db->join('tbl_student_group AS f', 'f.id=b.group');
        $this->db->join('tbl_message_template AS g', 'g.id=b.template_id');
        $this->db->where("a.date BETWEEN '" . $FromDate . "' AND '" . $ToDate . "'");
        $result = $this->db->get();
        if ($result->num_rows($result) > 0) {
            $Data = $result->result_array();
            $return['status'] = 1;
            $return['GuardianSMSReport'] = $Data;
        } else {
            $return['status'] = 0;
        }
        return $return;

        /* echo '<pre>';
        print_r($CategoryData);
        die(); */
    }

    public function GetDetailsSMSReport($FromDate, $ToDate)
    {
        /* SELECT  FROM tbl_message_to_guardian_student_list AS a
INNER JOIN tbl_student AS b ON b.id=a.student_id
INNER JOIN tbl_message_to_guardian AS c ON c.id=a.to_guardian_message_id
INNER JOIN tbl_message_template AS d ON d.id=c.template_id */
        /* SELECT  FROM tbl_message_to_teacher_list AS a
        INNER JOIN tbl_teacher AS b ON b.id=a.teacher_id
        INNER JOIN tbl_message_to_teacher AS c ON c.id=a.to_teacher_message_id
        INNER JOIN tbl_message_template AS d ON d.id=c.template_id */
        $return = array();
        $i = 0;
        $this->db->select('b.name,a.mobile_number,a.date,d.template_name,c.template_body');
        $this->db->from('tbl_message_to_guardian_student_list AS a');
        $this->db->join('tbl_student AS b', 'b.id=a.student_id');
        $this->db->join('tbl_message_to_guardian AS c', 'c.id=a.to_guardian_message_id');
        $this->db->join('tbl_message_template AS d', 'd.id=c.template_id');
        $this->db->where("a.date BETWEEN '" . $FromDate . "' AND '" . $ToDate . "'");
        $result = $this->db->get();
        if ($result->num_rows($result) > 0) {
            $guardianData = $result->result_array();
            foreach ($guardianData as $Data) {
                $return[$i]['messageName'] = 'Message To Guardian';
                $return[$i]['ReciverName'] = $Data['name'];
                $return[$i]['Number'] = $Data['mobile_number'];
                $return[$i]['template_name'] = $Data['template_name'];
                $return[$i]['template_body'] = $Data['template_body'];
                $return[$i]['date'] = $Data['date'];
                $lenth = strlen($Data['template_body']);
                //$lenth = 320;
                if ($lenth <= 160) {
                    $count = 1;
                } else {
                    $count = ceil($lenth / 169);
                };
                $return[$i]['numberOfSMS'] = $count;
                $i++;
            }
        }
        $this->db->select('b.name,a.mobile,a.date,d.template_name,c.template_body');
        $this->db->from('tbl_message_to_teacher_list AS a');
        $this->db->join('tbl_teacher AS b', 'b.id=a.teacher_id');
        $this->db->join('tbl_message_to_teacher AS c', 'c.id=a.to_teacher_message_id');
        $this->db->join('tbl_message_template AS d', 'd.id=c.template_id');
        $this->db->where("a.date BETWEEN '" . $FromDate . "' AND '" . $ToDate . "'");
        $result = $this->db->get();
        if ($result->num_rows($result) > 0) {
            $teacherData = $result->result_array();
            foreach ($teacherData as $Data) {
                $return[$i]['messageName'] = 'Message To Teacher';
                $return[$i]['ReciverName'] = $Data['name'];
                $return[$i]['Number'] = $Data['mobile'];
                $return[$i]['template_name'] = $Data['template_name'];
                $return[$i]['template_body'] = $Data['template_body'];
                $return[$i]['date'] = $Data['date'];
                $lenth = strlen($Data['template_body']);
                //$lenth = 320;
                if ($lenth <= 160) {
                    $count = 1;
                } else {
                    $count = ceil($lenth / 169);
                };
                $return[$i]['numberOfSMS'] = $count;
                $i++;
            }
        }
        /* SELECT FROM tbl_message_to_phonebook_list AS a
INNER JOIN  tbl_message_contact_list AS b ON b.category_id=a.smsReciver_id
INNER JOIN tbl_message_to_phonebook AS c ON c.id=a.to_message_for_birthday_id
INNER JOIN tbl_message_category AS d ON d.id=c.categoryId
INNER JOIN tbl_message_template AS e ON e.id=c.template_id */
        $this->db->select('b.contact_name,a.mobile_number,a.date,d.name,e.template_name,c.template_body');
        $this->db->from('tbl_message_to_phonebook_list AS a');
        $this->db->join('tbl_message_contact_list AS b', 'b.category_id=a.smsReciver_id');
        $this->db->join('tbl_message_to_phonebook AS c', 'c.id=a.to_message_for_birthday_id');
        $this->db->join('tbl_message_category AS d', 'd.id=c.categoryId');
        $this->db->join('tbl_message_template AS e', 'e.id=c.template_id');
        $this->db->where("a.date BETWEEN '" . $FromDate . "' AND '" . $ToDate . "'");
        $result = $this->db->get();
        if ($result->num_rows($result) > 0) {
            $phonebookData = $result->result_array();
            foreach ($phonebookData as $Data) {
                $return[$i]['messageName'] = 'Message To ' . $Data['name'];
                $return[$i]['ReciverName'] = $Data['contact_name'];
                $return[$i]['Number'] = $Data['mobile_number'];
                $return[$i]['template_name'] = $Data['template_name'];
                $return[$i]['template_body'] = $Data['template_body'];
                $return[$i]['date'] = $Data['date'];
                $lenth = strlen($Data['template_body']);
                //$lenth = 320;
                if ($lenth <= 160) {
                    $count = 1;
                } else {
                    $count = ceil($lenth / 169);
                };
                $return[$i]['numberOfSMS'] = $count;
                $i++;
            }
        }
        /* 	SELECT  FROM tbl_message_for_birthday_to_guardian_student_list AS a
    INNER JOIN tbl_student AS b ON b.id=a.student_id
    INNER JOIN tbl_message_for_birthday AS c ON c.id=a.to_message_for_birthday_id
    INNER JOIN tbl_message_template AS d ON d.id=c.template_id */
        $this->db->select('b.name,a.mobile_number,a.date,d.template_name,c.template_body');
        $this->db->from('tbl_message_for_birthday_to_guardian_student_list AS a');
        $this->db->join('tbl_student AS b', 'b.id=a.student_id');
        $this->db->join('tbl_message_for_birthday AS c', 'c.id=a.to_message_for_birthday_id');
        $this->db->join('tbl_message_template AS d', 'd.id=c.template_id');
        $this->db->where("a.date BETWEEN '" . $FromDate . "' AND '" . $ToDate . "'");
        $result = $this->db->get();
        if ($result->num_rows($result) > 0) {
            $birthdayStudentData = $result->result_array();
            foreach ($birthdayStudentData as $Data) {
                $return[$i]['messageName'] = 'Message To Student Birthday';
                $return[$i]['ReciverName'] = $Data['name'];
                $return[$i]['Number'] = $Data['mobile_number'];
                $return[$i]['template_name'] = $Data['template_name'];
                $return[$i]['template_body'] = $Data['template_body'];
                $return[$i]['date'] = $Data['date'];
                $lenth = strlen($Data['template_body']);
                //$lenth = 320;
                if ($lenth <= 160) {
                    $count = 1;
                } else {
                    $count = ceil($lenth / 169);
                };
                $return[$i]['numberOfSMS'] = $count;
                $i++;
            }
        }
        /* 	SELECT  FROM tbl_message_for_birthday_to_teacher AS a
    INNER JOIN tbl_teacher AS b ON b.id=a.teacher_id
    INNER JOIN tbl_message_for_birthday AS c ON c.id=a.to_message_for_birthday_id
    INNER JOIN tbl_message_template AS d ON d.id=c.template_id */
        $this->db->select('b.name,a.mobile_number,a.date,d.template_name,c.template_body');
        $this->db->from('tbl_message_for_birthday_to_teacher AS a');
        $this->db->join('tbl_teacher AS b', 'b.id=a.teacher_id');
        $this->db->join('tbl_message_for_birthday AS c', 'c.id=a.to_message_for_birthday_id');
        $this->db->join('tbl_message_template AS d', 'd.id=c.template_id');
        $this->db->where("a.date BETWEEN '" . $FromDate . "' AND '" . $ToDate . "'");
        $result = $this->db->get();
        if ($result->num_rows($result) > 0) {
            $birthdayStudentData = $result->result_array();
            foreach ($birthdayStudentData as $Data) {
                $return[$i]['messageName'] = 'Message To Teacher Birthday';
                $return[$i]['ReciverName'] = $Data['name'];
                $return[$i]['Number'] = $Data['mobile_number'];
                $return[$i]['template_name'] = $Data['template_name'];
                $return[$i]['template_body'] = $Data['template_body'];
                $return[$i]['date'] = $Data['date'];
                $lenth = strlen($Data['template_body']);
                //$lenth = 320;
                if ($lenth <= 160) {
                    $count = 1;
                } else {
                    $count = ceil($lenth / 169);
                };
                $return[$i]['numberOfSMS'] = $count;
                $i++;
            }
        }
        if (!empty($return)) {
            $returndata['status'] = 1;
            $returndata['report'] = $return;
        } else {
            $returndata['status'] = 0;
        }
        /* echo '<pre>';
        print_r($returndata);
        die(); */
        return $returndata;
    }

    //////// edit by dip (11-06-17)////////////////
}
