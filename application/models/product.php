<?php

class Product extends CI_Model
{

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }


    public function get_all_orders($limit, $offset, $value = '')
    {
      $this->db->select('o.*,c.name as customer_name,c.phone,c.email,c.address');
      $this->db->from('tbl_orders as o');
      $this->db->join('tbl_customer AS c', 'o.customer_id=c.id', 'left');
      if (isset($value) && !empty($value) && isset($value['order_code']) && $value['order_code'] != '') {
         $this->db->like('o.order_code', $value['order_code']);
      }

      if (isset($value) && !empty($value) && isset($value['id']) && $value['id'] != '') {
          $this->db->where('o.id', $value['id']);
      }

      $this->db->order_by("o.id", "desc");
      if (isset($limit) && $limit > 0)
          $this->db->limit($limit, $offset);
      $query = $this->db->get();
      return $query->result_array();
    }






    function get_all_product_info_list($limit, $offset, $value = '')
    {
        $this->db->select('p.*');
        $this->db->from('tbl_product_info as p');
        // if (isset($value['tailor_id']) && !empty($value) && $value['tailor_id'] != '') {
        //     $this->db->where('t.id', $value['tailor_id']);
        // }
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('p.product_name', $value['name']);
            $this->db->or_like('p.product_code', $value['name']);
        }
        if (isset($value) && !empty($value) && isset($value['hot_deal']) && $value['hot_deal'] != '') {
            $this->db->where('p.hot_deal', $value['hot_deal']);
        }

        if (isset($value) && !empty($value) && isset($value['category_id']) && $value['category_id'] != '') {
           $category_id = $value['category_id'];
           $sub_query = "SELECT `product_info_id` FROM `tbl_product_category_details`
                          WHERE `category_id` = $category_id";
           $this->db->where("id IN ($sub_query)");
        }


        if (isset($value) && !empty($value) && isset($value['sub_category_id']) && $value['sub_category_id'] != '') {
           $sub_category_id = $value['sub_category_id'];
           $sub_query = "SELECT `product_info_id` FROM `tbl_product_category_details`
                          WHERE `sub_category_id` = $sub_category_id";

           $this->db->where("id IN ($sub_query)");
        }

        if (isset($value) && !empty($value) && isset($value['product_name']) && $value['product_name'] != '') {
           $this->db->like('p.product_name', $value['product_name']);
        }



        $this->db->order_by("p.id", "desc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_product_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_product_info')->row();
    }
    public function get_category_id_by_sub_category_id($id)
    {
        $info = $this->db->where('id', $id)->get('tbl_sub_category')->row();
        return $info->category_id;
    }
    public function update_product_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_product_info', $data);
    }

    public function add_product_info($data)
    {
        return $this->db->insert('tbl_product_info', $data);
    }

    function delete_product_info_by_product_id($id)
    {
        return $this->db->delete('tbl_product_info', array('id' => $id));
    }
    function delete_product_category_details_by_product_id($id)
    {
        return $this->db->delete('tbl_product_category_details', array('product_info_id' => $id));
    }
    function get_product_category_list(){
      $data   = array();

      $this->db->select('*');
      $this->db->from('tbl_category');
      $this->db->order_by("order_no", "asc");
      $data = $this->db->get()->result_array();

    //  $data   = $this->db->get('tbl_category')->result_array();
      foreach( $data as $key=>$each ){
        $this->db->select('*');
        $this->db->from('tbl_sub_category');
        $this->db->where('category_id', $each['id']);
        $this->db->order_by("order_no", "asc");
        $data[$key]['sub_category'] = $this->db->get()->result_array();
          //$data[$key]['sub_category']   = $this->db->where('category_id', $each['id'])->get('tbl_sub_category')->result_array();
      }

      return $data;
  }
  function get_product_category_list_by_product_id($id){
    $data   = array();
    $this->db->select('*');
    $this->db->from('tbl_category');
    $this->db->order_by("order_no", "asc");
    $data = $this->db->get()->result_array();

  //  $data   = $this->db->get('tbl_category')->result_array();
    foreach( $data as $key=>$each ){
      $cat_id=$each['id'];
       $sub_list=$this->db->query("SELECT * FROM (
               SELECT DISTINCT s.`name`,s.`id`,0 AS active,s.order_no  FROM tbl_sub_category AS s
               WHERE s.`category_id`=$cat_id AND s.id NOT IN(SELECT `sub_category_id` FROM tbl_product_category_details WHERE product_info_id=$id)
               UNION
               SELECT s.`name`,s.`id`,(CASE WHEN p.`id` IS NULL THEN 0 ELSE 1 END) AS active,s.order_no  FROM tbl_sub_category AS s
               inner JOIN `tbl_product_category_details` AS p ON s.`id`=p.`sub_category_id`
               WHERE s.`category_id`=$cat_id AND p.`product_info_id`=$id
             ) AS t ORDER BY t.order_no")->result_array();

     $data[$key]['sub_category']=$sub_list;
     // echo "<pre>";
     // print_r($sub_list);
     // die;
    }

  return $data;
}
public function checkifexist_product_info_by_name($name)
{
  $name = mysql_real_escape_string($name);
  $count = $this->db->query("SELECT id FROM tbl_product_info where product_name = '$name'")->num_rows();
  if((int)$count==0){return 0;}else{return 1;}
}
public function checkifexist_update_product_info_by_name($name,$id)
{
  $count = $this->db->query("SELECT id FROM tbl_product_info where product_name = '$name' and id !='$id'")->num_rows();
  if((int)$count==0){return 0;}else{return 1;}
}
public function get_all_product_info()
{
  $result=$this->db->query("SELECT id,CONCAT(`product_name`,'(',`product_code`,')') AS name FROM tbl_product_info")->result_array();
  return $result;
}
public function checkifexist_product_info_dependency_by_product_id($id)
{
  $count = $this->db->where('product_id', $id)->get('tbl_order_items')->num_rows();

  if((int)$count==0){return 0;}else{return 1;}
}
}
