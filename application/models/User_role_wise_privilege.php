<?php

class User_role_wise_privilege extends CI_Model
{

    var $title = '';
    var $content = '';
    var $date = '';

    function __construct()
    {
        $this->load->database();
        parent::__construct();
    }

    /**
     * Generates a list of user wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To Generate a list of user wise privileges
     * @access  :   public
     * @param   :   int $offset, int $limit
     * @return  :   array
     */
    function get_list($offset, $limit)
    {
        $query = $this->db->get('user_role_wise_privileges', $offset, $limit);
        return $query->result();
    }

    function get_role_name_by_id($role_id)
    {
        return $this->db->query("SELECT `role_name` FROM `user_roles` WHERE `id` = '$role_id'")->row()->role_name;
    }

    /**
     * Counts number of rows of  user wise privileges table
     * @author  :   Amlan Chowdhury
     * @uses    :   To count number of rows of  user wise privileges table
     * @access  :   public
     * @return  :   int
     */
    function row_count()
    {
        return $this->db->count_all_results('user_role_wise_privileges');
    }

    /**
     * Adds data to user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To add data to user role wise privileges
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    function add($data)
    {
        if (isset($data['resources'])) {
            //echo '-------'.$data['role_id'].'=======';echo '<pre>';print_r($data);echo '</pre>';//die('model die');

            $this->db->trans_start();
            $this->db->delete('user_role_wise_privileges', array('role_id' => $data['role_id']));
            //$this->insert_rows('user_role_wise_privileges', $data['column_names'],$data['column_rows']);
            $this->db->insert_batch('user_role_wise_privileges', $data['resources']);
            $this->db->trans_complete();

            return $this->db->trans_status();
        } else {
            $this->db->delete('user_role_wise_privileges', array('role_id' => $data['role_id']));
            return true;
        }
    }

    /**
     * Updates data of user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To update data of user roles wise privileges
     * @access  :   public
     * @param   :   array $data
     * @return  :   boolean
     */
    function edit($data)
    {
        return $this->db->update('user_role_wise_privileges', $data, array('id' => $data['id']));
    }

    /**
     * Reads data of specific user role wise privilege
     * @author  :   Amlan Chowdhury
     * @uses    :   To  read data of specific user role wise privilege
     * @access  :   public
     * @param   :   int $user_role_wise_privileges_id
     * @return  :   boolean
     */
    function read($user_role_wise_privileges_id)
    {
        return $this->db->get_where('user_role_wise_privileges', array('id' => $user_role_wise_privileges_id))->result();
    }

    /**
     * Gets data of user role wise privilege by role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   array
     */
    function get_by_role_id($role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by role id,controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by role id, controller name and action
     * @access  :   public
     * @param   :   int $role_id, string $controller, string $action
     * @return  :   array
     */
    function get_by_role_id_controller_action($role_id, $controller, $action)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id, 'controller' => $controller, 'action' => $action));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by controller name and action
     * @access  :   public
     * @param   :   string $controller, string $action
     * @return  :   array
     */
    function get_by_controller_action($controller, $action)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('controller' => $controller, 'action' => $action));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privilege by controller name ,action and role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privilege by controller name, action and role id
     * @access  :   public
     * @param   :   string $controller, string $action, int $role_id
     * @return  :   array
     */
    function get_by_controller_action_role($controller, $action, $role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('controller' => $controller, 'action' => $action, 'role_id' => $role_id));
        return $query->result_array();
    }

    /**
     * Gets data of user role wise privileged resourses by controller role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  get data of user role wise privileged resourses by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   array
     */
    function get_privileged_resources($role_id)
    {
        $query = $this->db->get_where('user_role_wise_privileges', array('role_id' => $role_id));
        {
            $amlan_field_officers_reports_ccdas = array();
            $results = $query->result_array();
            foreach ($results as $result) {
                if ($result['controller'] == 'amlan_field_officers_reports') {
                    $result['id'] += 1000;
                    $result['controller'] = 'amlan_field_officers_reports_ccdas';
                    array_push($amlan_field_officers_reports_ccdas, $result);
                }
            }
            foreach ($amlan_field_officers_reports_ccdas as $amlan_field_officers_reports_ccda) {
                array_push($results, $amlan_field_officers_reports_ccda);
            }
            return $results;
        }
        return $query->result_array();
    }

    /**
     * Deletes data of specific user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of specific user role wise privileges
     * @access  :   public
     * @param   :   int $user_role_wise_privileges_id
     * @return  :   boolean
     */
    function delete($user_role_wise_privileges_id)
    {
        return $this->db->delete('user_role_wise_privileges', array('id' => $user_role_wise_privileges_id));
    }

    /**
     * Deletes data of user role wise privileges by role id ,controller name and action
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of user role wise privileges by role id ,controller name and action
     * @access  :   public
     * @param   :   int $role_id,string $controller, string $action
     * @return  :   boolean
     */
    function delete_by_role_id_controller_action($role_id, $controller, $action)
    {
        return $this->db->delete('user_role_wise_privileges', array('role_id' => $role_id, 'controller' => $controller, 'action' => $action));
    }

    /**
     * Deletes data of specific user role wise privileges by role id
     * @author  :   Amlan Chowdhury
     * @uses    :   To  delete data of specific user role wise privileges by role id
     * @access  :   public
     * @param   :   int $role_id
     * @return  :   boolean
     */
    function delete_by_role_id($role_id)
    {
        return $this->db->delete('user_role_wise_privileges', array('role_id' => $role_id));
    }

    /**
     * Checks permission of user role wise privileges
     * @author  :   Amlan Chowdhury
     * @uses    :   To  check permission of user role wise privileges
     * @access  :   public
     * @param   :   int $role_id, string $controller,string $action
     * @return  :   boolean
     */
    function check_permission($role_id, $controller, $action)
    {
        $controller = strtolower($controller);
        $action = strtolower($action);

        if ($this->is_globally_allowed_action($controller, $action)) { //if action is globally allowed, access is granted
            return true;
        } else {
            $check = $this->db->query("SELECT `id` FROM `user_role_wise_privileges` WHERE `controller` = '$controller' AND `action` = '$action' AND `role_id` = '$role_id'")->result_array();
            return !empty($check) ? true : false;
        }
    }

    /**
     * Checks if the action is globally dis-allowed or not
     * @author  :   Anis Alamgir
     * @uses    :   To check if the action is globally dis-allowed or not
     * @access  :   public
     * @param   :   string $controller,string $action
     * @return  :   boolean
     * @wiki    : http://203.188.255.195/tracker/projects/microfin360/wiki/Manage_User_Role#Only-For-Super-Admin
     */
    function is_globally_disallowed_action($controller, $action)
    {
        return isset($actions[$controller][$action]) ? true : false;
    }

    /**
     * Checks if the action is globally allowed or not
     * @author  :   Amlan Chowdhury
     * @uses    :   To check if the action is globally allowed or not
     * @access  :   public
     * @param   :   string $controller,string $action
     * @return  :   boolean
     */
    function is_globally_allowed_action($controller, $action)
    {
        $actions = array();
        $actions['login']['index'] = 1;
		$actions['login']['download_zone'] = 1;
        $actions['login']['student'] = 1;
        $actions['login']['student_guardian_login_authentication'] = 1;
        $actions['login']['student_guardian_logout'] = 1;
        $actions['login']['forgot_password'] = 1;
        $actions['login']['student_forgot_password'] = 1;
        $actions['login']['authentication'] = 1;
        $actions['login']['logout'] = 1;
        $actions['login']['denied'] = 1;
		$actions['login']['change_password'] = 1;
		$actions['students']['getStudentByClassShiftSection'] = 1;
		$actions['students']['getStudentInformationById'] = 1;
		$actions['service_bridge']['getNumberOfStudent'] = 1;
		$actions['service_bridge']['smsBalanceRecharge'] = 1;
		$actions['fee_collections']['fee_collect_process_from_student_portal'] = 1;
        return isset($actions[$controller][$action]);
    }


    function get_all_resources_array()
    {
        define("VIEW", "View");
        define("ADD", "Add");
        define("EDIT", "Edit");
        define("DELETE", "Delete");
        //initializing
        $group_id = 0;
        $tmp = array();
        //Organization management group


        $group_name = 'Dashboard';
        $subgroup_name = 'Dashboard';

        $entity_name = 'Dashboard';
        $controller = 'dashboard';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $group_name = 'Configuration';
        $subgroup_name = 'General Settings';

        $entity_name = 'Institute Setup';
        $controller = 'institute_setup';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'update');

        $entity_name = 'Global Configuration';
        $controller = 'global_configs';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'index');

        $entity_name = 'General Configuration';
        $controller = 'config_generals';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'index , view');

        $entity_name = 'Main Marking Head';
        $controller = 'main_marking_heads';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'index');

        $entity_name = 'Signature';
        $controller = 'signature';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'index');
        

        $entity_name = 'Student ID Configuration';
        $controller = 'students';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'student_code_config');
        

        $entity_name = 'Class';
        $controller = 'classes';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Section';
        $controller = 'sections';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Group';
        $controller = 'groups';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Subject';
        $controller = 'subjects';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Grade Point Configuration';
        $controller = 'result_grades';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','data_save'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');
       

        $entity_name = 'Mark Sheet Design';
        $controller = 'marksheet_designs';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','get_configuration_by_year_class'));


        $entity_name = 'Admit Design';
        $controller = 'admit_designs';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'index');

        $group_name = 'Configuration';
        $subgroup_name = 'Student Fees';

        $entity_name = 'User Wise Asset Head Configuration';
        $controller = 'user_wise_income_heads';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'index');

        $entity_name = 'Fee Type Configuration';
        $controller = 'fee_type_configs';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');

        $entity_name = 'Waiver Setting';
        $controller = 'waiver_settings';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


        //User management
		$group_name = 'User Management';
		$subgroup_name = 'User Management';

		$entity_name = 'User Information';
        $controller = 'users';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'User Roles';
        $controller = 'user_roles';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


        #Student
        $group_name = 'Student';
        $subgroup_name = 'Admission';

        $entity_name = 'Student Information';
        $controller = 'students';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','updateMsgStatusStudentStatus'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Bulk Admission';
		$controller = 'students';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('bulk_add','bulk_add_data_save'));

        $entity_name = 'Excel Admission';
        $controller = 'student_batch_adds';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','excel_data_save'));

		$entity_name = 'Student Photo Upload';
		$controller = 'students';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('student_photo_upload','student_photo_upload_data_save'));

		$entity_name = 'Multiple Student Update';
		$controller = 'multiple_students_update';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','data_save'));



        #Teacher/Staff

        $group_name = 'Teacher/Staff';
        $subgroup_name = 'Settings';

        $entity_name = 'Section';
        $controller = 'teacher_sections';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Designation';
        $controller = 'teacher_post';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Pay Code Information';
		$controller = 'pay_codes';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


        $entity_name = 'Teacher/Staff';
        $controller = 'teachers';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Teacher Add By Excel';
		$controller = 'teacher_batch_adds';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','excel_data_save'));


		#Attendance
        $group_name = 'Attendance';
        $subgroup_name = 'Attendance';

        $entity_name = 'Holiday';
        $controller = 'holidays';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','batch_add','batch_add_data_save'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Teacher/Staff Attendance Settings';
		$controller = 'teacher_attendance_settings';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');

		$entity_name = 'Leave Type';
		$controller = 'leave_types';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Student Timekeeping';
		$controller = 'student_timekeepings';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','add_student_login_data'));

		$entity_name = 'Teacher/Staff Attendance';
		$controller = 'teacher_staff_logins';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','data_save'));

		$entity_name = 'Teacher/Staff Leave';
		$controller = 'teacher_leaves';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Movement Register';
		$controller = 'movement_registers';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


		#Student Fees
        $group_name = 'Student Fees';
        $subgroup_name = 'Settings';

        $entity_name = 'Fee Category';
        $controller = 'fee_categories';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Sub Category';
        $controller = 'fee_sub_categories';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Fees Allocation';
        $controller = 'fee_allocations';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','get_fee_allocation_form_by_year_class'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Fees Remove';
        $controller = 'student_fee_removes';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','fee_remove_data_save'));

        $entity_name = 'Fees Waiver';
        $controller = 'student_fee_waivers';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','student_fee_waiver_data_save','student_fee_waiver_from_total_amount_add','student_fee_waiver_from_total_amount_data_save'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Student Scholarship';
        $controller = 'student_scholarships';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','student_scholarship_data_save'));


        $entity_name = 'Fees Collection';
        $controller = 'fee_collections';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','getCollectionSheet','processCollectionSheet','quick_add','qucik_fee_collect'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Fee Receipt';
		$controller = 'student_fees';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('getStudentReceipt','getStudentReceipt'));


		$entity_name = 'Special Care Fees Collection';
        $controller = 'special_care_fees';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Play Traunt Fines';
        $controller = 'play_truant_fines';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');

        $entity_name = 'Absent Fine';
        $controller = 'student_absent_fines';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');

        #Basic Accounts

        $group_name = 'Basic Accounts';
        $subgroup_name = 'Settings';

        $entity_name = 'Accounts Head';
        $controller = 'chart_of_accounts';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');


        $entity_name = 'Asset Category';
        $controller = 'asset_categories';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Liabilities Category';
        $controller = 'liabilities_categories';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Income Category';
        $controller = 'income_categories';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Expense Category';
        $controller = 'expense_categories';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


        $entity_name = 'Income Deposit';
        $controller = 'income_deposits';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','deposit_details_view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Expense';
        $controller = 'expense_details';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','expense_details_view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Fund Transfer';
        $controller = 'fund_transfers';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');


        #Result
        $group_name = 'Result';
        $subgroup_name = 'Result';

        $entity_name = 'Exam Type';
        $controller = 'exam_types';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Exam';
        $controller = 'exams';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','updateMsgStatusExamStatus'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','get_exam_list_for_combined','getExamByYear','get_exam_list_for_combined_edit'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Class Wise Subject';
        $controller = 'class_wise_subjects';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');

        $entity_name = 'Subject Assign';
        $controller = 'subject_assigns';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','bulk_subject_assign','student_subject_assign_data_save'));

        $entity_name = 'Marking';
        $controller = 'markings';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','marking_data_save','markingDataUpload','generateExcelDataForMarking','getSubjectWiseMarkingConfig','marking_data_process'));

        $entity_name = 'Result Process';
        $controller = 'result_processes';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('index','result_process_data_save_new','resultProcessByStudent'));

        #Accessories

        $group_name = 'Accessories';
        $subgroup_name = 'Accessories';

        $entity_name = 'Admit Card Print';
        $controller = 'print_admit';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'Seat Plan Print';
        $controller = 'print_seatplan';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'ID Card Print';
        $controller = 'id_cards';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'Testimonial';
        $controller = 'testimonials';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','generateSerialNo'));

        $entity_name = 'Transfer Certificate';
        $controller = 'transfer_certificates';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','view'));
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','generateSerialNo'));

        $entity_name = 'Certification Letter';
        $controller = 'prottayans';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'Teacher ID Card';
        $controller = 'teacher_id_cards';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        #SMS
		$group_name = 'SMS';
		$subgroup_name = 'SMS';

		$entity_name = 'SMS Template';
		$controller = 'templates';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Category';
		$controller = 'phonebook_categories';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Phone Book';
		$controller = 'phone_books';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, array('add','validate_mobile_number'));
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

		$entity_name = 'Audio Upload';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'audio_message_upload');

		$subgroup_name = 'SMS Send';

		$entity_name = 'Voice SMS send';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'voice_sms_send');

		$entity_name = 'Absent SMS send';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'absent_sms_send_form');

		$entity_name = 'Guardian SMS send';
		$controller = 'sms_sends';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'to_guardian_sms_send');
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'guardian_sms_send_action');

		$entity_name = 'Teacher SMS send';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'to_teacher_sms_send_form');

		$entity_name = 'Admission SMS Send';
		$controller = 'sms_sends';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'to_guardian_sms_send');

		$entity_name = 'Result Notify';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('to_result_notify,to_result_notify_sms_send'));

		$entity_name = 'Phone Book';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'view_category_phoneBook_list');

		$entity_name = 'Excel';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'to_excel_file');

		$entity_name = 'Birthday';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'SendBirthDaySMS');


        #Routine

        $group_name = 'Routine';
        $subgroup_name = 'Class Routine';

        $entity_name = 'Period';
        $controller = 'class_periods';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $subgroup_name = 'Exam Routine';

        $entity_name = 'Routine Session';
        $controller = 'exam_sessions';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, ADD, 'add');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, EDIT, 'edit');
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, DELETE, 'delete');

        $entity_name = 'Routine';
        $controller = 'exam_routines';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

        $entity_name = 'Routine View';
        $controller = 'exam_routine_reports';
        $tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');


        //report start

		$group_name = 'Student Report';
		$subgroup_name = 'Student Report';

		$entity_name = 'Student Information Report';
		$controller = 'student_reports';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('get_student_attendance_sheet','get_class_name','get_period_name','get_section_name','get_shift_name','get_group_name','get_month_name_by_id'));

		$entity_name = 'Student Summary Report';
		$controller = 'student_summery_reports';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Resident/Non-Resident Student';
		$controller = 'report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'resident_non_resident_student');

		$entity_name = 'Special Care Student';
		$controller = 'report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'special_care_non_special_care_student');

		#Attendance
		$group_name = 'Attendance Report';
		$subgroup_name = 'Attendance Report';

		$entity_name = 'Attendance Blank Sheet';
		$controller = 'student_attendance_sheets';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Student Attendance Details';
		$controller = 'student_attendance_details';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Student Attendance Details Summary';
		$controller = 'student_attendance_details_summery';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','getStudentAttendanceDetailsSummeryReport','dateDiffInDays'));

		$entity_name = 'Login/logout Report';
		$controller = 'login_logout_reports';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Teacher/Staff Absentees';
		$controller = 'report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'teacher_staff_report_absentee');

		$entity_name = 'Student Absentees Report';
		$controller = 'report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'student_report_absentee');

		$entity_name = 'Person Wise Login/Logout Report';
		$controller = 'report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'person_wise_login_logout_report');

		$entity_name = 'Teacher Attendance Details Report';
		$controller = 'report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'getTeacherAttendanceDetailReport');

		$entity_name = 'Student Attendance Details Summary Report';
		$controller = 'report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'getStudentAttendanceDetailsSummeryReport');

		#Result

		$group_name = 'Result Report';
		$subgroup_name = 'Result Report';

		$entity_name = 'Marking Blank Sheet';
		$controller = 'mark_input_blank_sheets';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Mark Sheet';
		$controller = 'mark_sheets';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Combined Mark Sheet';
		$controller = 'combined_mark_sheet';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','get_only_combined_exam_by_year'));

		$entity_name = 'Grand Final Report';
		$controller = 'progress_report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','get_only_annual_exam_by_year'));

		$entity_name = 'Unassigned Marks';
		$controller = 'unassigned_marks_reports';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Merit List';
		$controller = 'merit_lists';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Tabulation Sheet';
		$controller = 'tabulation_sheets';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','ajax_exam_by_year'));

		$entity_name = 'Short Tabulation Sheet';
		$controller = 'result_short_tebulations';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','ajax_exam_by_year'));

		$entity_name = 'Result Summary Report';
		$controller = 'report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'studentResultSummeryReport');

		$entity_name = 'Failed List';
		$controller = 'failed_lists';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		#Student Fees

		$group_name = 'Student Fees Report';
		$subgroup_name = 'Student Fees Report';

		$entity_name = 'Due List';
		$controller = 'fee_due_lists';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'student_fee_due_list');

		$entity_name = 'Student Fee Status Details';
		$controller = 'student_fee_status_details';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Collection Summary';
		$controller = 'collection_summery';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Collection Details';
		$controller = 'collection_details_reports';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Special Care Fees Collection';
		$controller = 'report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'getSpecialCareCollection');


		#Accounts

		$group_name = 'Accounts Report';
		$subgroup_name = 'Accounts Report';

		$entity_name = 'Trial Balance';
		$controller = 'trial_balance';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('index','report_vew_post'));

		$entity_name = 'Short Trial Balance';
		$controller = 'trial_balance';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, array('short_trial_balance','report_vew_post'));

		$entity_name = 'Balance Sheet';
		$controller = 'balance_sheets';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Income vs Expense Report';
		$controller = 'income_vs_expense';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Income Report';
		$controller = 'income_reports';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Expense Report';
		$controller = 'expense_reports';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Fund Transfer Report';
		$controller = 'fund_transfer_report';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');

		$entity_name = 'Fund Transfer Statement';
		$controller = 'fund_transfer_statements';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');



		$group_name = 'SMS Report';
		$subgroup_name = 'SMS Report';

		$entity_name = 'Guardian SMS Report';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'GetGuardianSMSReport');

		$entity_name = 'SMS Details Report';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'GetDetailsSMSReport');

		$entity_name = 'SMS Summary Report';
		$controller = 'messages';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'SMSSummaryReport');

		$entity_name = 'Recharge Histories';
		$controller = 'recharge_histories';
		$tmp = $this->create_resource($tmp, $group_name, $subgroup_name, $entity_name, $controller, VIEW, 'index');


		return $tmp;
    }

    function create_resource(&$tmp, $group_name, $subgroup_name, $entity_name, $controller, $action_title, $actions = null)
    {
        if (is_null($actions)) {
            $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][0]['name'] = strtolower($action_title);
        } elseif (is_string($actions)) {
            $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][0]['name'] = $actions;
        } elseif (is_array($actions)) {
            foreach ($actions as $key => $row) {
                $tmp[$group_name][$subgroup_name][$entity_name][$controller][$action_title][$key]['name'] = $row;
            }
        }

        return $tmp;
    }

}
