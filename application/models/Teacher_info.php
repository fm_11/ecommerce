<?php

class Teacher_info extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function generateTeacherCode(){
		$code = "";
		$code .= substr(date('Y'), -2);
		$total_teacher = count($this->db->query("SELECT id FROM tbl_teacher")->result_array());
		return $code.str_pad(($total_teacher + 1), 4, '0', STR_PAD_LEFT);
	}

    public function get_all_teachers($limit, $offset, $value = '')
    {
        $this->db->select('tbl_teacher.*,tp.name as post_name,ts.name as section_name,
        c.name as category_name,r.name as religion_name');
        $this->db->from('tbl_teacher');
        $this->db->join('tbl_teacher_post AS tp', 'tbl_teacher.post=tp.id', 'left');
        $this->db->join('tbl_teacher_section AS ts', 'tbl_teacher.section=ts.id', 'left');
		$this->db->join('tbl_teacher_category AS c', 'tbl_teacher.category_id=c.id', 'left');
		$this->db->join('tbl_religion AS r', 'tbl_teacher.religion=r.id', 'left');
        if (isset($value) && !empty($value)) {
            $this->db->where('tbl_teacher.status', $value['status']);
        }
        $this->db->order_by("tp.shorting_order", "asc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_teacher_status($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_teacher', $data);
    }

    public function add_teacher_info($data)
    {
        return $this->db->insert('tbl_teacher', $data);
    }

    public function get_teacher_photo_info_by_teacher_id($id)
    {
        $this->db->select('tbl_teacher.*,tp.name as post_name,ts.name as section_name,bg.name as blood_group');
        $this->db->from('tbl_teacher');
        $this->db->join('tbl_teacher_post AS tp', 'tbl_teacher.post=tp.id', 'left');
        $this->db->join('tbl_teacher_section AS ts', 'tbl_teacher.section=ts.id', 'left');
        $this->db->join('tbl_blood_group AS bg', 'tbl_teacher.blood_group_id=bg.id', 'left');
        $this->db->where('tbl_teacher.id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_staff_photo_info_by_staff_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_staff_info')->result_array();
    }

    public function delete_teacher_info_by_teacher_id($id)
    {
        return $this->db->delete('tbl_teacher', array('id' => $id));
    }

    public function delete_staff_info_by_staff_id($id)
    {
        return $this->db->delete('tbl_staff_info', array('id' => $id));
    }

    public function update_staff_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_staff_info', $data);
    }
}
