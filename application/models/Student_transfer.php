<?php

class Student_transfer extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    

    function get_all_transfer_list($limit, $offset, $value = ''){
        //echo '<pre>';
        //print_r($value); die;
        $this->db->select('s.name,s.student_code,t.*,oc.name as old_class_name,nc.name as new_class_name,
		                   osc.name as old_section_name,nsc.name as new_section_name,
						   og.name as old_group_name,ng.name as new_group_name,
						   osf.name as old_shift_name,nsf.name as new_shift_name');
        $this->db->from('tbl_student_transfer as t');
		$this->db->join('tbl_student AS s', 's.id=t.student_id');
		
        $this->db->join('tbl_class AS oc', 'oc.id=t.old_class_id','left');
		$this->db->join('tbl_class AS nc', 'nc.id=t.new_class_id','left');
		
        $this->db->join('tbl_section AS osc', 'osc.id=t.old_section_id','left');
		$this->db->join('tbl_section AS nsc', 'nsc.id=t.new_section_id','left');
		
		$this->db->join('tbl_student_group AS og', 'og.id=t.old_group_id','left');
		$this->db->join('tbl_student_group AS ng', 'ng.id=t.new_group_id','left');
		
        $this->db->join('tbl_shift AS osf', 'osf.id=t.old_shift_id','left');
		$this->db->join('tbl_shift AS nsf', 'nsf.id=t.new_shift_id','left');
				    
        $this->db->order_by("t.id", "desc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
}

?>