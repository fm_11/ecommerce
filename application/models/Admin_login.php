<?php

class Admin_login extends CI_Model
{
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library(array('session'));
    }

    public function getGroupList(){
    		$group_list = $this->db->query("SELECT * FROM tbl_student_group  AS g
                        ORDER BY -g.`order` DESC;")->result_array();
    		return $group_list;
    }

  	public function getClassListByNameIndex()
  	{
  		$returnList = array();
  		$religions = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
  		foreach ($religions as $religion) {
  			$returnList[trim(strtolower($religion['name']))]['id'] = $religion['id'];
  			$returnList[trim(strtolower($religion['name']))]['name'] = $religion['name'];
  			$returnList[trim(strtolower($religion['name']))]['name_bangla'] = $religion['name_bangla'];
  		}
  		return $returnList;
  	}

	public function getReligionList()
	{
		$returnList = array();
		$religions = $this->db->query("SELECT * FROM `tbl_religion`")->result_array();
		foreach ($religions as $religion) {
			$returnList[$religion['id']]['id'] = $religion['id'];
			$returnList[$religion['id']]['name'] = $religion['name'];
		}
		return $returnList;
	}


    function getExamDetailsById($id){
		return $this->db->where('id', $id)->get('tbl_exam')->row();
	}

    public function getMarkSheetDesignConfig($class_id,$year,$type){
		$configs = $this->db->query("SELECT * FROM `tbl_marksheet_design`
									WHERE `class_id` = '$class_id' AND `year` = '$year' AND `type` = '$type'")->result_array();
		$data = array();
		if(!empty($configs)){
			$data['show_student_image'] = $configs[0]['show_student_image'];
			$data['show_watermark'] = $configs[0]['show_watermark'];
			$data['marksheet_header'] = $configs[0]['marksheet_header'];
			$data['merit_position'] = $configs[0]['merit_position'];
			$data['template_category'] = $configs[0]['template_category'];
		}
		return $data;
	}

    public function getClassWiseSubjectMarkingHead($class_id, $exam_type_id){
		$configs = $this->db->query("SELECT * FROM tbl_class_wise_subject_marking_head WHERE class_id = '$class_id'
                                      AND exam_type_id = '$exam_type_id'")->result_array();
		$data = array();
		if(!empty($configs)){
			$data['is_class_test_allow'] = $configs[0]['is_class_test_allow'];
			$data['is_written_allow'] = $configs[0]['is_written_allow'];
			$data['is_objective_allow'] = $configs[0]['is_objective_allow'];
			$data['is_practical_allow'] = $configs[0]['is_practical_allow'];
		}else{
			$data['is_class_test_allow'] = 0;
			$data['is_written_allow'] = 0;
			$data['is_objective_allow'] = 0;
			$data['is_practical_allow'] = 0;
		}
		return $data;
	}

    public function class_section_shift_marge_list()
    {
        $class_wise_section = array();
        $i = 0;
        $classes = $this->all_class();
        foreach ($classes as $class) {
            $class_id = $class['id'];
            $all_shift = $this->all_shift();
            foreach ($all_shift as $shift) {
                $shift_id = $shift['id'];
                $sections = $this->all_section();
                foreach ($sections as $section) {
                    $section_id = $section['id'];
                    $students = $this->db->query("SELECT id FROM tbl_student WHERE class_id = '$class_id' AND shift_id = '$shift_id' AND section_id = '$section_id' AND status = 1")->result_array();
                    if (!empty($students)) {
                        $class_wise_section[$i]['class_id'] = $class_id;
                        $class_wise_section[$i]['class_name'] = $class['name'];
                        $class_wise_section[$i]['shift_id'] = $shift_id;
                        $class_wise_section[$i]['shift_name'] = $shift['name'];
                        $class_wise_section[$i]['section_id'] = $section_id;
                        $class_wise_section[$i]['section_name'] = $section['name'];
                        $i++;
                    }
                }
            }
        }
        return $class_wise_section;
    }

	public function all_class_section_shift_marge_list()
	{
		$class_wise_section = array();
		$i = 0;
		$classes = $this->all_class();
		foreach ($classes as $class) {
			$class_id = $class['id'];
			$all_shift = $this->all_shift_by_class($class_id);
			foreach ($all_shift as $shift) {
				$shift_id = $shift['id'];
				$sections = $this->all_section_by_class($class_id);
				foreach ($sections as $section) {
					$section_id = $section['id'];
					$class_wise_section[$i]['class_id'] = $class_id;
					$class_wise_section[$i]['class_name'] = $class['name'];
					$class_wise_section[$i]['shift_id'] = $shift_id;
					$class_wise_section[$i]['shift_name'] = $shift['name'];
					$class_wise_section[$i]['section_id'] = $section_id;
					$class_wise_section[$i]['section_name'] = $section['name'];
					$i++;
				}
			}
		}
		return $class_wise_section;
	}

    public function getClassAndGroupWiseSubjectList($class_id)
    {
      $groups = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
      $returnGroupList = array();
      foreach ($groups as $group) {
         $group_id = $group['id'];
         $subjects = $this->db->query("SELECT c.`subject_id` FROM `tbl_class_wise_subject` AS c
                          WHERE c.`class_id` = '$class_id' AND FIND_IN_SET('$group_id',group_list) <> 0 GROUP BY c.`subject_id`")->result_array();
         $i = 0;
         foreach ($subjects as $subject) {
           $returnGroupList[$group_id][$i] = $subject['subject_id'];
           $i++;
         }
      }
      return $returnGroupList;
    }

	public function getClassAndGroupWiseSubjectListWithoutChoosable($class_id)
	{
		$groups = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$returnGroupList = array();
		foreach ($groups as $group) {
			$group_id = $group['id'];
			$subjects = $this->db->query("SELECT c.`subject_id`,c.`subject_type` FROM `tbl_class_wise_subject` AS c
                          WHERE c.`class_id` = '$class_id' AND c.`subject_type` != 'CHO' AND FIND_IN_SET('$group_id',group_list) <> 0 GROUP BY c.`subject_id`")->result_array();
			$i = 0;
			foreach ($subjects as $subject) {
				$returnGroupList[$group_id][$i]['subject_id'] = $subject['subject_id'];
				$returnGroupList[$group_id][$i]['subject_type'] = $subject['subject_type'];
				$i++;
			}
		}
		return $returnGroupList;
	}

	public function getClassAndGroupWiseChoosableSubjectList($class_id,$group_id)
	{
		$returnSubjectList = array();
		$subjects = $this->db->query("SELECT c.`subject_id`,c.`subject_type`,s.`name` FROM `tbl_class_wise_subject` AS c
						  LEFT JOIN `tbl_subject` AS s ON s.`id` = c.`subject_id`
                          WHERE c.`class_id` = '$class_id' AND c.`subject_type` = 'CHO'
                          AND FIND_IN_SET('$group_id',group_list) <> 0 GROUP BY c.`subject_id`")->result_array();
		$i = 0;
		foreach ($subjects as $subject) {
			$returnSubjectList[$i]['subject_id'] = $subject['subject_id'];
			$returnSubjectList[$i]['name'] = $subject['name'];
			$returnSubjectList[$i]['subject_type'] = $subject['subject_type'];
			$i++;
		}
		return $returnSubjectList;
	}

    public function getClassAndGroupAndExamTypeWiseSubjectList($class_id, $exam_type_id)
    {
      $groups = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
      $returnGroupList = array();
      foreach ($groups as $group) {
         $group_id = $group['id'];
         $subjects = $this->db->query("SELECT c.`subject_id` FROM `tbl_class_wise_subject` AS c
                          WHERE c.`exam_type_id` = '$exam_type_id' AND c.`class_id` = '$class_id' AND FIND_IN_SET('$group_id',group_list) <> 0 GROUP BY c.`subject_id`")->result_array();
         $i = 0;
         foreach ($subjects as $subject) {
           $returnGroupList[$group_id][$i] = $subject['subject_id'];
           $i++;
         }
      }
      return $returnGroupList;
    }


	public function getClassGroupAndExamTypeWiseSubjectList($class_id, $exam_type_id,$group_id)
	{
		$returnGroupList = array();
		$subjects = $this->db->query("SELECT c.`subject_id`,s.`name`,s.`code` FROM `tbl_class_wise_subject` AS c
							INNER JOIN `tbl_subject` AS s ON s.`id` = c.`subject_id`
                            WHERE c.`exam_type_id` = '$exam_type_id' AND c.`class_id` = '$class_id'
                            AND FIND_IN_SET('$group_id',group_list) <> 0 GROUP BY c.`subject_id` ORDER BY s.`code`")->result_array();
		$i = 0;
		foreach ($subjects as $subject) {
			$returnGroupList[$i]['subject_id'] = $subject['subject_id'];
			$returnGroupList[$i]['name'] = $subject['name'];
			$returnGroupList[$i]['code'] = $subject['code'];
			$i++;
		}
		return $returnGroupList;
	}

	public function getSubjectTypeByClassExamTypeSubject($class_id,$subject_id){
		$subject_info = $this->db->query("SELECT `subject_type` FROM `tbl_class_wise_subject`
                    WHERE `class_id` = $class_id AND `subject_id` = $subject_id;")->row();
		return $subject_info;
	}

    public function getCurrentSystemYear()
    {
        $config = $this->db->query("SELECT software_current_year FROM tbl_config")->row();
        if (empty($config)) {
            $sdata['exception'] = "Please Contact to technical team for initial configuration add";
            $this->session->set_userdata($sdata);
            redirect("dashboard/index");
        }
        $software_current_year = $config->software_current_year;
        if ($software_current_year == '') {
            $sdata['exception'] =  "Please update initial configuration";
            $this->session->set_userdata($sdata);
            redirect("global_configs/index");
        }
        return $software_current_year;
    }


    public function getStudentListByYear($year, $class_id='', $shift_id='', $section_id='',$group_id='')
    {
        $config = $this->db->query("SELECT software_start_year,software_current_year FROM tbl_config")->row();
        $data = array();
        if (empty($config)) {
            $data['status'] = "Please Contact to technical team for initial configuration add";
            return $data;
        }
        $software_start_year = $config->software_start_year;
        $software_current_year = $config->software_current_year;
        if ($software_start_year == '' || $software_current_year == '') {
            $data['status'] = "Please update initial configuration";
            return $data;
        }
        $software_start_year = $config->software_start_year;
        $software_current_year = $config->software_current_year;

        $class_where = '';
        $shift_where = '';
        $section_where = '';
        $group_where = '';

        $class_where_old = '';
        $section_where_old = '';
        $shift_where_old = '';
        $group_where_old = '';

        if ($class_id != '' && $class_id > 0) {
            $class_where = " AND s.`class_id` = $class_id ";
            $class_where_old = " AND mt.`old_class_id` = $class_id ";
        }

        if ($section_id != '' && $section_id > 0) {
            $section_where = " AND s.`section_id` = $section_id ";
            $section_where_old =  " AND mt.`old_section_id` = $section_id ";
        }

        if ($shift_id != '' && $shift_id > 0) {
            $shift_where = " AND s.`shift_id` = $shift_id ";
            $shift_where_old = " AND mt.`old_shift` = $shift_id ";
        }

        if ($group_id != '' && $group_id > 0) {
            $group_where = " AND s.`group` = $group_id ";
            $group_where_old = " AND mt.`old_group` = $group_id ";
        }

        if ($software_current_year == $year) {
            $students = $this->db->query("SELECT s.id,s.name,s.roll_no,s.student_code,s.class_id,s.section_id,s.shift_id,s.group FROM tbl_student AS s WHERE
              s.status = '1' $class_where $section_where $shift_where $group_where ORDER BY ABS(s.roll_no)")->result_array();
        } else {
            $students = $this->db->query("SELECT s.id,s.student_code,mt.name,mt.old_roll_no as roll_no
                               FROM tbl_student_data_migrate_history AS mt
                               INNER JOIN tbl_student AS s ON s.id = mt.student_id
                               WHERE mt.old_year = '$year' $class_where_old
                               $section_where_old $shift_where_old $group_where_old ORDER BY ABS(mt.old_roll_no)")->result_array();
        }
        $data['status'] = 'success';
        $data['students'] = $students;
        return $data;
    }

    public function getYearList($num_of_extra_year_add, $num_of_extra_year_excluded)
    {
        $config = $this->db->query("SELECT software_start_year,software_current_year FROM tbl_config")->row();
        if (empty($config)) {
            $sdata['exception'] = "Please Contact to technical team for initial configuration add";
            $this->session->set_userdata($sdata);
            redirect("dashboard/index");
        }
        $software_start_year = $config->software_start_year;
        $software_current_year = $config->software_current_year;
        if ($software_start_year == '' || $software_current_year == '') {
            $sdata['exception'] = "Please update initial configuration";
            $this->session->set_userdata($sdata);
            redirect("global_configs/index");
        }
        $software_start_year = $config->software_start_year - $num_of_extra_year_excluded;
        $software_current_year = $config->software_current_year + $num_of_extra_year_add;
        $years = array();
        $i = 0;
        while ($software_start_year <= $software_current_year) {
            $years[$i]['text'] = $software_start_year;
            $years[$i]['value'] = $software_start_year;
            if ($software_start_year == $software_current_year) {
                $years[$i]['is_selected'] = 1;
            } else {
                $years[$i]['is_selected'] = 0;
            }
            $i++;
            $software_start_year++;
        }
//        echo '<pre>';
//        print_r($years);
//        die;
        return $years;
    }


    function get_all_success_student($limit, $offset, $value = ''){
		$this->db->select('tbl_success_students.*');
		$this->db->from('tbl_success_students');

		$this->db->order_by("tbl_success_students.id", "desc");
		if (isset($limit) && $limit > 0) {
			$this->db->limit($limit, $offset);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	function get_all_donor_list($limit, $offset, $value = ''){
		$this->db->select('tbl_donor_lists.*');
		$this->db->from('tbl_donor_lists');

		if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
			$this->db->like('tbl_donor_lists.member_code', $value['name']);
			$this->db->or_like('tbl_donor_lists.name', $value['name']);
		}

		if (isset($value) && !empty($value) && isset($value['present_district']) && $value['present_district'] != '') {
			$this->db->like('tbl_donor_lists.present_district', $value['present_district']);
			$this->db->or_like('tbl_donor_lists.present_thana', $value['present_district']);
		}

		if (isset($value) && !empty($value) && isset($value['permanent_district']) && $value['permanent_district'] != '') {
			$this->db->like('tbl_donor_lists.permanent_district', $value['permanent_district']);
		}

		if (isset($value) && !empty($value) && isset($value['mobile']) && $value['mobile'] != '') {
			$this->db->like('tbl_donor_lists.permanent_district', $value['mobile']);
		}

		if (isset($value) && !empty($value) && isset($value['donation_type']) && $value['donation_type'] != '') {
			$this->db->where('tbl_donor_lists.donation_type', $value['donation_type']);
		}

		$this->db->order_by("tbl_donor_lists.member_code", "asc");
		if (isset($limit) && $limit > 0) {
			$this->db->limit($limit, $offset);
		}
		$query = $this->db->get();
		return $query->result_array();
	}


    function get_all_testimonial($limit, $offset, $value = ''){
		$this->db->select('tbl_testimonial.*,q.name as exam_name,g.name as group_name,b.name as board_name');
		$this->db->from('tbl_testimonial');
		$this->db->join('tbl_educational_qualification AS q', 'tbl_testimonial.qua_id=q.id', 'left');
		$this->db->join('tbl_student_group AS g', 'tbl_testimonial.group_id=g.id', 'left');
		$this->db->join('tbl_board AS b', 'tbl_testimonial.board_id=b.id', 'left');

		if (isset($value) && !empty($value) && isset($value['id']) && $value['id'] != '') {
			$this->db->where('tbl_testimonial.id', $value['id']);
		}
		if (isset($value) && !empty($value) && isset($value['sl_no']) && $value['sl_no'] != '') {
			$this->db->like('tbl_testimonial.sl_no', $value['sl_no']);
			$this->db->or_like('tbl_testimonial.name', $value['sl_no']);
		}
		$this->db->order_by("tbl_testimonial.id", "desc");
		if (isset($limit) && $limit > 0) {
			$this->db->limit($limit, $offset);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	function get_all_transfer_certificate($limit, $offset, $value = ''){
		$this->db->select('tbl_transfer_certificate.*');
		$this->db->from('tbl_transfer_certificate');
		if (isset($value) && !empty($value) && isset($value['id']) && $value['id'] != '') {
			$this->db->where('tbl_transfer_certificate.id', $value['id']);
		}
		if (isset($value) && !empty($value) && isset($value['sl_no']) && $value['sl_no'] != '') {
			$this->db->like('tbl_transfer_certificate.sl_no', $value['sl_no']);
			$this->db->or_like('tbl_transfer_certificate.name', $value['sl_no']);
		}
		$this->db->order_by("tbl_transfer_certificate.id", "desc");
		if (isset($limit) && $limit > 0) {
			$this->db->limit($limit, $offset);
		}
		$query = $this->db->get();
		return $query->result_array();
	}


	public function getUserWiseAssetHead($user_id)
    {
        return $this->db->query("SELECT * FROM tbl_user_wise_asset_head WHERE user_id = $user_id")->result_array();
    }

    public function getAssetHeadWiseUser($asset_category_id)
    {
        return $this->db->query("SELECT * FROM tbl_user_wise_asset_head WHERE asset_category_id = $asset_category_id")->result_array();
    }

    public function all_class()
    {
        return $this->db->query("SELECT * FROM tbl_class")->result_array();
    }

    public function getSignatureByAccessCode($accessCode)
    {
        $rows = $this->db->query("SELECT * FROM tbl_signature where `access_code` = '$accessCode'")->result_array();
        $signatures = array();
        foreach ($rows as $row) {
            $signatures[$row['position']]['is_use'] = $row['is_use'];
            $signatures[$row['position']]['level'] = $row['level'];
            $signatures[$row['position']]['location'] = $row['location'];
        }
        return $signatures;
    }


    public function get_main_marking_head($class_id)
    {
        return $this->db->query("SELECT * FROM tbl_exam_main_marking_head WHERE class_id = $class_id;")->row();
    }

	public function get_online_payment_gateway_setting()
	{
		return $this->db->query("SELECT * FROM tbl_online_payment_gateway_setup")->row();
	}

	public function get_payslip_setting()
	{
		return $this->db->query("SELECT * FROM tbl_payslip_setting")->row();
	}


    public function all_section()
    {
        return $this->db->query("SELECT * FROM tbl_section")->result_array();
    }

	public function all_section_by_class($class_id)
	{
		return $this->db->query("SELECT s.`id`,s.`name` FROM `tbl_class_section_mappings` AS m
INNER JOIN `tbl_section` AS s ON s.`id` = m.`section_id`
WHERE m.`class_id` = $class_id;")->result_array();
	}

	public function all_shift_by_class($class_id)
	{
		return $this->db->query("SELECT s.`id`,s.`name` FROM `tbl_class_shift_mappings` AS m
INNER JOIN `tbl_shift` AS s ON s.`id` = m.`shift_id`
WHERE m.`class_id` = $class_id;")->result_array();
	}

    public function all_shift()
    {
        return $this->db->query("SELECT * FROM tbl_shift")->result_array();
    }

    public function get_global_config()
    {
        $config_info = $this->db->query("SELECT * FROM `tbl_config`")->result_array();
        return $config_info;
    }


    public function qr_code_generate($data, $name)
    {
        $path = FCPATH . '/' . MEDIA_FOLDER . '/QR_CODE';
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $this->load->library('ciqrcode');
        $config['cacheable']	= true; //boolean, the default is true
        $config['cachedir']		= ''; //string, the default is application/cache/
        $config['errorlog']		= ''; //string, the default is application/logs/
        $config['quality']		= true; //boolean, the default is true
        $config['size']			= ''; //interger, the default is 1024
        $config['black']		= array(224,255,255); // array, default is array(255,255,255)
        $config['white']		= array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);

        $params['data'] = $data;
        $params['level'] = 'H';
        $params['size'] = 10;
        $params['savename'] = $path.'/'.$name;
        $this->ciqrcode->generate($params);
        return true;
    }

    public function random_password($length)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    public function generateNumberlist($num_string, $number)
    {
        $message_config = $this->db->query("SELECT * FROM tbl_message_config")->result_array();
        if (empty($message_config)) {
            $sdata['exception'] = "Message panel configuration not found to database.";
            $this->session->set_userdata($sdata);
            redirect("dashboard/index");
        }

        $user_id = $message_config[0]['user_id'];
        if ($this->validate_mobile_number($number)) {
            if ($user_id != '') {
                if ($num_string == "") {
                    $num_string .= $number;
                } else {
                    $num_string .= "," . $number;
                }
            } else {
                if ($num_string == "") {
                    $num_string .= "88" . $number;
                } else {
                    $num_string .= "+88" . $number;
                }
            }
        }

        return $num_string;
    }

    public function validate_mobile_number($phoneNumber)
    {
        if (!empty($phoneNumber)) { // phone number is not empty
            if (preg_match('/^\d{11}$/', $phoneNumber)) { // phone number is valid
                return true;
            // your other code here
            } else { // phone number is not valid
                return false;
            }
        } else { // phone number is empty
            return false;
        }
    }

    public function fetReportHeader()
    {
        return $this->db->query("SELECT * FROM `tbl_contact_info`")->result_array();
    }

    public function get_year_drop_down_list($software_start_year)
    {
        $year_data = array();
        while ($software_start_year <= (date('Y') + 1)) {
            $year_data['value'] = $software_start_year;
            $year_data['text'] = $software_start_year;
            $software_start_year++;
        }
        return $year_data;
    }


    public function get_history_info()
    {
        return $this->db->get('tbl_history_info')->row();
    }

    public function get_rules_and_regulation_info()
    {
        return $this->db->get('tbl_rules_and_regulation')->row();
    }

    public function get_history_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_history_info')->row();
    }

    public function get_rules_and_regulation_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_rules_and_regulation')->row();
    }

    public function history_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_history_info', $data);
    }

    public function rules_and_regulation_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_rules_and_regulation', $data);
    }

    public function add_contact_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_admission_contact', $data);
    }

    public function add_process_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_admission_process', $data);
    }

    public function get_about_info()
    {
        return $this->db->get('tbl_about_info')->row();
    }


    public function get_contact_info()
    {
        return $this->db->get('tbl_contact_info')->row();
    }

    public function get_founder_info()
    {
        return $this->db->get('tbl_founder_info')->row();
    }

    public function update_founder_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_founder_info', $data);
    }


    public function get_home_page_news_info($limit, $offset, $value = '')
    {
        $this->db->select('hpn.*');
        $this->db->from('tbl_home_page_news AS hpn');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_home_page_breaking_news_info()
    {
        return $this->db->get('tbl_home_page_breaking_news')->row();
    }

    public function get_about_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_about_info')->row();
    }

    public function get_home_page_news_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_home_page_news')->row();
    }

    public function get_home_page_breaking_news_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_home_page_breaking_news')->row();
    }

    public function about_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_about_info', $data);
    }

    public function home_page_news_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_home_page_news', $data);
    }

    public function home_page_breaking_news_info_update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_home_page_breaking_news', $data);
    }

    public function get_all_unit()
    {
        return $this->db->get('tbl_unit')->result_array();
    }

    public function add_unit($data)
    {
        return $this->db->insert('tbl_unit', $data);
    }

    public function delete_unit_by_unit_id($id)
    {
        return $this->db->delete('tbl_unit', array('id' => $id));
    }

    public function get_unit_info_by_unit_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_unit')->row();
    }

    public function update_unit_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_unit', $data);
    }

    public function get_all_notice($limit, $offset, $value = '')
    {
        $this->db->select('n.*');
        $this->db->from('tbl_notice AS n');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('n.id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function get_all_document($limit, $offset, $value = '')
    {
        $this->db->select('d.*');
        $this->db->from('tbl_document AS d');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('d.id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_contact_message($limit, $offset, $value = '')
    {
        $this->db->select('c.*');
        $this->db->from('tbl_contact AS c');
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('c.id', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add_document($data)
    {
        return $this->db->insert('tbl_document', $data);
    }

    public function add_notice($data)
    {
        return $this->db->insert('tbl_notice', $data);
    }

    public function add_admission_form($data)
    {
        return $this->db->insert('tbl_admission_form', $data);
    }



    public function get_document_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_document')->result_array();
    }


    public function get_notice_info_by_id($id)
    {
        return $this->db->where('id', $id)->get('tbl_notice')->result_array();
    }


    public function delete_document_info_by_id($id)
    {
        return $this->db->delete('tbl_document', array('id' => $id));
    }



    public function delete_notice_info_by_id($id)
    {
        return $this->db->delete('tbl_notice', array('id' => $id));
    }



    public function update_contact_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_contact_info', $data);
    }

    public function getAllDigitalContent($limit, $offset, $value = '')
    {
        $this->db->select('dc.*');
        $this->db->from('tbl_digital_contents as dc');
        $this->db->order_by("dc.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_basic_info()
    {
        return $this->db->get('tbl_basic_info')->row();
    }

    public function update_basic_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_basic_info', $data);
    }

    public function get_mail_info()
    {
        return $this->db->get('tbl_mail_configuration')->row();
    }

    public function update_mail_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_mail_configuration', $data);
    }

    public function get_delivery_info()
    {
        return $this->db->get('tbl_delivery_info')->row();
    }

    public function update_delivery_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_delivery_info', $data);
    }
}
