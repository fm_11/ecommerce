<?php


class User_role extends CI_Model
{

    function __construct()
    {
        $this->load->database();
        parent::__construct();
    }


    function get_list()
    {
        return $this->db->query("SELECT * FROM `user_roles` ORDER BY `id`")->result();
    }

    function add($data)
    {
        return $this->db->insert('user_roles', $data);
    }


    function edit($data, $id)
    {
        return $this->db->update('user_roles', $data, array('id' => $id));
    }


    function read($role_id)
    {
        return $this->db->get_where('user_roles', array('id' => $role_id))->row();
    }

    function delete($role_id)
    {
        return $this->db->delete('user_roles', array('id' => $role_id));
    }

}
