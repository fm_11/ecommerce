<?php

class Lab_info extends CI_Model
{

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all_computers($limit, $offset, $value = '')
    {
//        echo '<pre>';
//        print_r($value);
//        die;
        $this->db->select('tbl_pc.*,l.name as lab_name');
        $this->db->from('tbl_pc');
        $this->db->join('tbl_lab AS l', 'tbl_pc.lab_id=l.id');
        if (isset($value) && !empty($value) && $value['lab_id'] != '') {
            $this->db->where('tbl_pc.lab_id', $value['lab_id']);
        }
        $this->db->order_by("tbl_pc.id", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

	
	function get_all_lab($limit, $offset, $value = '')
    {
//        echo '<pre>';
//        print_r($value);
//        die;
        $this->db->select('tbl_lab.*');
        $this->db->from('tbl_lab');   
        $this->db->order_by("tbl_lab.id", "asc");
        if (isset($limit) && $limit > 0)
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_employee_status($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_employees', $data);
    }


    public function update_employee_info($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_employees', $data);
    }

    public function add_employee_info($data)
    {
        return $this->db->insert('tbl_employees', $data);
    }

    function get_employee_photo_info_by_employee_id($id)
    {
        $this->db->select('tbl_employees.*,tp.name as post_name,ts.name as section_name');
        $this->db->from('tbl_employees');
        $this->db->join('tbl_employees_post AS tp', 'tbl_employees.post=tp.id');
        $this->db->join('tbl_employees_section AS ts', 'tbl_employees.section=ts.id');
        $this->db->where('tbl_employees.id', $id);
        $query = $this->db->get();
        return $query->result_array();

    }

   
    function delete_employee_info_by_employee_id($id)
    {
        return $this->db->delete('tbl_employees', array('id' => $id));
    }

   

    
}

?>