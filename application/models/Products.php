<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Model{

    function __construct() {
        $this->proTable = 'products';
        $this->custTable = 'customers';
        $this->ordTable = 'orders';
        $this->ordItemsTable = 'order_items';
    }

    /*
     * Fetch products data from the database
     * @param id returns a single record if specified, otherwise all records
     */
    public function getRows($id = ''){
        $this->db->select('*');
        $this->db->from($this->proTable);
        $this->db->where('status', '1');
        if($id){
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('name', 'asc');
            $query = $this->db->get();
            $result = $query->result_array();
        }

        // Return fetched data
        return !empty($result)?$result:false;
    }

    /*
     * Fetch order data from the database
     * @param id returns a single record of the specified ID
     */
    public function getOrder($id){
        $this->db->select('o.*, c.name, c.email, c.phone, c.address');
        $this->db->from('tbl_orders as o');
        $this->db->join('tbl_customers as c', 'c.id = o.customer_id', 'left');
        $this->db->where('o.id', $id);
        $query = $this->db->get();
        $result = $query->row_array();

        // Get order items
        $this->db->select('i.*, p.product_name, p.standard_price');
        $this->db->from('tbl_order_items as i');
        $this->db->join('tbl_product_info as p', 'p.id = i.product_id', 'left');
        $this->db->where('i.id', $id);
        $query2 = $this->db->get();
        $result['items'] = ($query2->num_rows() > 0)?$query2->result_array():array();

        // Return fetched data
        return !empty($result)?$result:false;
    }

    /*
     * Insert customer data in the database
     * @param data array
     */
    public function insertCustomer($data){
        // Insert customer data
        $insert = $this->db->insert("tbl_customers", $data);

        // Return the status
        return $insert?$this->db->insert_id():false;
    }

    /*
     * Insert order data in the database
     * @param data array
     */
    public function insertOrder($data){
        // Add created and modified date if not included
        if(!array_key_exists("created_date", $data)){
            $data['created_date'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("modified_date", $data)){
            $data['modified_date'] = date("Y-m-d H:i:s");
        }

        // Insert order data
        $insert = $this->db->insert("tbl_orders", $data);

        // Return the status
        return $insert?$this->db->insert_id():false;
    }

    /*
     * Insert order items data in the database
     * @param data array
     */
    public function insertOrderItems($data = array()) {

        // Insert order items
        $insert = $this->db->insert_batch('tbl_order_items', $data);

        // Return the status
        return $insert?true:false;
    }

}
