<?php

class Inventory extends CI_Model
{

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}


	public function get_all_sub_category($limit, $offset, $value = '')
	{
		$this->db->select('sc.*,c.category_name');
		$this->db->from('inventory_sub_category as sc');
		$this->db->join('inventory_category AS c', 'sc.category_id=c.id');
		$this->db->order_by("sc.id", "desc");
		if (isset($limit) && $limit > 0) {
			$this->db->limit($limit, $offset);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	    public function get_item_list($limit, $offset, $value = '')
    {
      // print_r($value);
      // die();
        $this->db->select('c.category_name as cname,s.name as sname,w.name as wname,i.*');
        $this->db->from('inventory_item as i');
        $this->db->join('`inventory_category` AS c', 'i.`category_id`=c.`id`', 'left');
        $this->db->join('`inventory_sub_category` AS s', 'i.`sub_category_id`=s.`id`', 'left');
        $this->db->join('`inventory_ware_house` AS w', 'i.`ware_house_id`=w.`id`', 'left');
        if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
            $this->db->like('s.name', $value['name']);
        }
        $this->db->order_by("s.id", "desc");
        if (isset($limit) && $limit > 0) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_item($data)
    {
        return $this->db->insert('inventory_item', $data);
    }


    function edit_item($data, $id)
    {
        return $this->db->update('inventory_item', $data, array('id' => $id));
    }


    function read_item($id)
    {
        return $this->db->get_where('inventory_item', array('id' => $id))->row();
    }

    function delete_item($id)
    {
        return $this->db->delete('inventory_item', array('id' => $id));
    }

    public function checkifexist_item_by_name($name)
    {
      $count = $this->db->query("SELECT id FROM inventory_item where name = '$name'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }
    public function checkifexist_update_item_by_name($name,$id)
    {
      $count = $this->db->query("SELECT id FROM inventory_item where name = '$name' and id !='$id'")->num_rows();
      if((int)$count==0){return 0;}else{return 1;}
    }

    function get_catgory_list()
    {
        return $this->db->query("SELECT * FROM `inventory_category` ")->result_array();
    }

    function get_subcategory_by_category($category_id)
    {
        return $this->db->query("SELECT id,name FROM `inventory_sub_category` where category_id= $category_id")->result_array();
    }

    function get_warehouse_list()
    {
        return $this->db->query("SELECT * FROM `inventory_ware_house` ")->result_array();
    }


	public function get_all_suppliers($limit, $offset, $value = '')
	{
		$this->db->select('m.*');
		$this->db->from('`tbl_supplier AS m');
		if (isset($value) && !empty($value) && isset($value['name']) && $value['name'] != '') {
			$this->db->like('m.name', $value['name']);
		}
		$this->db->order_by("m.id", "desc");
		if (isset($limit) && $limit > 0) {
			$this->db->limit($limit, $offset);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	function add_suppliers($data)
	{
		return $this->db->insert('tbl_supplier', $data);
	}

	function edit_suppliers($data, $id)
	{
		return $this->db->update('tbl_supplier', $data, array('id' => $id));
	}
	function read_suppliers($vendor_id)
	{
		return $this->db->get_where('tbl_supplier', array('id' => $vendor_id))->row();
	}
	function delete_suppliers($vendor_id)
	{
		return $this->db->delete('tbl_supplier', array('id' => $vendor_id));
	}
	public function checkifexist_suppliers_by_mobile($mobile)
	{
		$count = $this->db->query("SELECT id FROM tbl_supplier where mobile = '$mobile'")->num_rows();
		if((int)$count==0){return 0;}else{return 1;}
	}
	public function checkifexist_update_suppliers_by_mobile($mobile,$id)
	{
		$count = $this->db->query("SELECT id FROM tbl_supplier where mobile = '$mobile' and id !='$id'")->num_rows();
		if((int)$count==0){return 0;}else{return 1;}
	}

}
