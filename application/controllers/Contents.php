<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contents extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Home', 'Teacher_info', 'Admin_login', 'Timekeeping', 'Student'));
        $this->load->library('session');
    }

    function getAllHeadTeacherInfo()
    {
        $data['title'] = 'All Headmasters';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['heading_msg'] = 'All Headmasters';
        $data['all_headmasters'] = $this->db->query("SELECT * FROM tbl_all_headmasters ORDER BY id ASC")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/all_headmasters_list', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getAllSuccessStudentsInfo()
    {
        $data['title'] = 'Success Students';
        $data['heading_msg'] = 'Success Students';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['students'] = $this->db->query("SELECT * FROM tbl_success_students ORDER BY id ASC")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/all_success_student_list', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getSuccessStudentsInfoByID($id)
    {
        $data['title'] = 'Success Students';
        $data['heading_msg'] = 'Success Students';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['students'] = $this->db->query("SELECT * FROM tbl_success_students WHERE id = '$id'")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/success_student_full_info', $data, true);
        $data['counter'] = $this->load->view('homes/sources/counter', $data, true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getClassRoutineInfo()
    {
        $data = array();
        $data['title'] = 'Class Routine';
        $data['heading_msg'] = 'Class Routine';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('contents/getClassRoutineInfo/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Student->get_all_uploaded_routine(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['student_routine'] = $this->Student->get_all_uploaded_routine(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/routine_list', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }


    function getStudentInformation()
    {
        $data = array();
        $data['title'] = 'Student Information';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        if ($_POST) {
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $roll = "";
            $religion = "";
            $gender = "";
            $blood_group_id = "";
            if ($class_id != '' || $section_id != '' || $group != '' || $roll != '' || $religion != '' || $blood_group_id != '' || $gender != '') {
                $sdata['roll'] = $roll;
                $sdata['class_id'] = $class_id;
                $sdata['section_id'] = $section_id;
                $sdata['group'] = $group;
                $sdata['religion'] = $religion;
                $sdata['gender'] = $gender;
                $sdata['student_status'] = 1;
                $sdata['blood_group_id'] = $blood_group_id;
                $this->session->set_userdata($sdata);
                $cond['roll'] = $roll;
                $cond['class_id'] = $class_id;
                $cond['section_id'] = $section_id;
                $cond['group'] = $group;
                $cond['religion'] = $religion;
                $cond['gender'] = $gender;
                $cond['blood_group_id'] = $blood_group_id;
                $cond['blood_group_id'] = $blood_group_id;
                $cond['student_status'] = 1;
            }
        } else {
            $roll = $this->session->userdata('roll');
            $class_id = $this->session->userdata('class_id');
            $section_id = $this->session->userdata('section_id');
            $group = $this->session->userdata('group');
            $gender = $this->session->userdata('gender');
            $religion = $this->session->userdata('religion');
            $blood_group_id = $this->session->userdata('blood_group_id');
            $student_status = $this->session->userdata('student_status');
            $cond['roll'] = $roll;
            $cond['class_id'] = $class_id;
            $cond['section_id'] = $section_id;
            $cond['group'] = $group;
            $cond['religion'] = $religion;
            $cond['gender'] = $gender;
            $cond['blood_group_id'] = $blood_group_id;
            $cond['student_status'] = $student_status;
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('contents/getStudentInformation/');
        $config['per_page'] = 25;
        $config['total_rows'] = count($this->Student->get_all_student_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['student_info'] = $this->Student->get_all_student_list(25, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
        $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
        $data['group_list'] = $this->Admin_login->getGroupList();
        $data['main_content'] = $this->load->view('contents/student_info', $data, true);
        $data['counter'] = $this->counter();
        $data['is_open_slide'] = 0;
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function counter()
    {
        $data = array();
        $data['online'] = 2;
        $data['day_value'] = 5;
        $data['yesterday_value'] = 3;
        $data['week_value'] = 150;
        $data['month_value'] = 500;
        $data['year_value'] = 1002;
        $data['all_value'] = 1002;
        $data['record_value'] = 50;
        $data['record_date'] = date('Y-m-d');
        return $data;
    }


    function getStudentInfoByStudentID($id)
    {
        $data = array();
        $data['title'] = 'Student Profile';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['student_info'] = $this->Student->get_student_info_by_student_id($id);
        $data['subject_list_by_student'] = $this->db->query("SELECT
  su.*,
  s.`is_optional`
FROM
  `tbl_student_wise_subject` AS s
  LEFT JOIN `tbl_subject` AS su
    ON su.`id` = s.`subject_id`
WHERE s.`student_id` = '$id' ")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/student_profile', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }


    function getStudentTimeKeepingInformation()
    {
        $data = array();
        $data['title'] = 'Student Timekeeping Information';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        if ($_POST) {
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $date = $this->input->post("date");
            if ($class_id != '' || $section_id != '' || $group != '' || $date != '') {
                $sdata['class_id'] = $class_id;
                $sdata['section_id'] = $section_id;
                $sdata['group'] = $group;
                $sdata['date'] = $date;
                $this->session->set_userdata($sdata);
                $cond['class_id'] = $class_id;
                $cond['section_id'] = $section_id;
                $cond['group'] = $group;
                $cond['date'] = $date;
            }
        } else {
            $cond['date'] = date('Y-m-d');
            $cond['class_id'] = '';
            $cond['section_id'] = '';
            $cond['group'] = '';
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('contents/getStudentTimeKeepingInformation/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student->getStudentTimeKeepingInformation(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['student_login_info'] = $this->Student->getStudentTimeKeepingInformation(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
        $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
        $data['group_list'] = $this->Admin_login->getGroupList();
        $data['main_content'] = $this->load->view('contents/student_login_info', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getAdmissionProcessInfo()
    {
        $data['title'] = 'Admission Process';
        $data['heading_msg'] = 'Admission Process';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['add_process'] = $this->db->query("SELECT * FROM tbl_admission_process")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/add_process', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }


    function getAdmissionContact()
    {
        $data['title'] = 'Admission Contact';
        $data['heading_msg'] = 'Admission Contact';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['add_contact'] = $this->db->query("SELECT * FROM tbl_admission_contact")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/add_contact', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getTeacherPostStatusInfo()
    {
        $data['title'] = 'Teacher Post Status';
        $data['heading_msg'] = 'Teacher Post Status';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['post_status_teacher'] = $this->db->query("SELECT
  IF(h.NoOfPost IS NULL, 0, h.NoOfPost) AS NoOfExistPost,
  p.* 
FROM
  `tbl_teacher_post` AS p 
  LEFT JOIN 
    (SELECT 
      COUNT(`id`) AS NoOfPost,
      `post` 
    FROM
      `tbl_teacher` 
    GROUP BY `post`) AS h 
    ON h.`post` = p.`id` WHERE p.`is_teacher` = '1' ORDER BY p.`shorting_order` ASC")->result_array();
        $data['post_status_staff'] = $this->db->query("SELECT
  IF(h.NoOfPost IS NULL, 0, h.NoOfPost) AS NoOfExistPost,
  p.*
FROM
  `tbl_teacher_post` AS p
  LEFT JOIN
    (SELECT
      COUNT(`id`) AS NoOfPost,
      `post`
    FROM
      `tbl_staff_info`
    GROUP BY `post`) AS h
    ON h.`post` = p.`id`
    WHERE p.`is_teacher` = '0' ORDER BY p.`shorting_order` ASC")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/teacher_post_status', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getAdmissionForm()
    {
        $data['title'] = 'Admission Form';
        $data['heading_msg'] = 'Admission Form';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['add_form'] = $this->db->query("SELECT af.*,c.`name` AS class_name FROM `tbl_admission_form` AS af
LEFT JOIN `tbl_class` AS c ON af.`class_id` = c.`id`")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['slide'] = $this->load->view('homes/sources/slide', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/add_form', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getStaffTimeKeepingInformation()
    {
        $data = array();
        $data['title'] = 'Staff Timekeeping Information';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        if ($_POST) {
            $date = $this->input->post("date");
            if ($date != '') {
                $sdata['date'] = $date;
                $this->session->set_userdata($sdata);
                $cond['date'] = $date;
            }
        } else {
            $cond['date'] = date('Y-m-d');
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('contents/getStaffTimeKeepingInformation/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Timekeeping->get_all_staff_timekeeping_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['staff_login_info'] = $this->Timekeeping->get_all_staff_timekeeping_list(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/staff_login_info', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getAcademicHoliday()
    {
        $data = array();
        $data['title'] = 'Academic Holiday';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        if ($_POST) {
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");
            if ($from_date != '') {
                $sdata['from_date'] = $from_date;
                $sdata['to_date'] = $to_date;
                $this->session->set_userdata($sdata);
                $cond['from_date'] = $from_date;
                $cond['to_date'] = $to_date;
            }
        } else {
            $cond['from_date'] = "";
            $cond['to_date'] = "";
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('contents/getAcademicHoliday/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Timekeeping->get_all_holiday_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['holiday_info'] = $this->Timekeeping->get_all_holiday_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
        $data['main_content'] = $this->load->view('contents/holiday_info', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }


    function getAllDigitalContent()
    {
        $data = array();
        $data['title'] = 'Digital Contents';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('contents/getAllDigitalContent/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Admin_login->getAllDigitalContent(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['digital_content_info'] = $this->Admin_login->getAllDigitalContent(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['president_message'] = $this->db->query("SELECT * FROM tbl_president_message")->result_array();
//        echo '<pre>';
//        print_r($data['digital_content_info']);
//        die;
        $data['main_content'] = $this->load->view('contents/digital_content_info', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function getSyllabus()
    {
        $data = array();
        $data['title'] = 'Syllabus';
        $data['eiin_number'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['administration_members'] = $this->db->query("SELECT * FROM tbl_administration_member")->result_array();
        $data['home_page_news'] = $this->db->query("SELECT * FROM tbl_home_page_news")->result_array();
        $data['home_latest_news'] = $this->db->query("SELECT * FROM tbl_home_page_breaking_news")->result_array();
        $data['home_page_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 5")->result_array();
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['main_menu'] = $this->load->view('homes/sources/main_menu', '', true);
        $data['main_content'] = $this->load->view('contents/all_class_list', $data, true);
        $data['counter'] = $this->counter();
        $data['contact_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $data['main_menu_for_mobile'] = $this->load->view('homes/sources/main_menu_for_mobile', '', true);
        $data['is_open_slide'] = 0;
        $data['digital_contents'] = $this->db->query("SELECT * FROM tbl_digital_contents ORDER BY id DESC LIMIT 3")->result_array();
        $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $this->load->view('homes/index', $data);
    }

    function openSyllabus($class_id)
    {
        $data = array();
        $data['title'] = 'Syllabus';
        $year = date('Y');
        $data['year'] = $year;
        $data['pages'] = $this->db->query("SELECT s.* FROM `tbl_syllabus` AS s
 WHERE s.`class_id` = '$class_id' AND s.`year` = '$year' ORDER BY s.`page_no` ASC")->result_array();
        $data['class_name'] = $this->db->query("SELECT * FROM `tbl_class` WHERE id = '$class_id'")->result_array();
        $this->load->view('contents/syllabus_book', $data);
    }

    function ApplyOnline()
    {
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/student/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg|png';
            $config['max_size'] = '6000';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "ADD_Student_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/student/" . $new_photo_name)) {
                        $year = $this->input->post('txtYear', true);
                        $get_last_pin = $this->db->query("SELECT MAX(a.`pin`) AS last_pin FROM `tbl_admission` AS a WHERE a.`year` = '$year'")->result_array();
                        $last_pin = $get_last_pin[0]['last_pin'];
                        if ($last_pin != '') {
                            $last_pin = $last_pin + 1;
                        } else {
                            $last_pin = $year . '1001';
                        }
                        $data = array();
                        $data['year'] = $year;
                        $data['admit_mobile'] = $this->input->post('txtAdmitMobile', true);
                        $data['password'] = md5($this->input->post('txtPassword', true));
                        $data['name'] = $this->input->post('txtStudentName', true);
                        $data['father_name'] = $this->input->post('txtFathersName', true);
                        $data['father_nid'] = $this->input->post('txtFathersNID', true);
                        $data['mother_name'] = $this->input->post('txtMothersName', true);
                        $data['mother_nid'] = $this->input->post('txtMothersNID', true);
                        $data['date_of_birth'] = $this->input->post('txtDOBYear', true) . '-' . $this->input->post('txtDOBMonth', true) . '-' . $this->input->post('txtDOBDay', true);
                        $data['gender'] = $this->input->post('txtGender', true);
                        $data['religion'] = $this->input->post('txtReligion', true);
                        $data['guardian_mobile'] = $this->input->post('txtGuardianMobile', true);
                        $data['present_address'] = $this->input->post('txtPresentAddress', true);
                        $data['permanent_address'] = $this->input->post('txtPermanentAddress', true);
                        $data['photo'] = $new_photo_name;
                        $data['class_id'] = $this->input->post('txtClass', true);
                        $data['group'] = $this->input->post('txtGroup', true);
                        $data['last_school_name'] = $this->input->post('txtLastSchoolName', true);
                        $data['last_result'] = $this->input->post('txtLastResult', true);
                        $data['date_of_application'] = date('Y-m-d');
                        $data['pin'] = $last_pin;
                        $this->db->insert('tbl_admission', $data);
                        $sdata['message'] = "You are Successfully Syllabus Uploaded !";
                        $this->session->set_userdata($sdata);
                        $data['title'] = 'Apply Online';
                        $data['main_content'] = $this->load->view('contents/admission_success_page', $data, true);
                        $this->load->view('contents/admission_index', $data);
                    } else {
                        $sdata['exception'] = "Sorry Apply Failed !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("students/ApplyOnline");
                    }
                } else {
                    $sdata['exception'] = "Sorry Apply Failed !";
                    $this->session->set_userdata($sdata);
                    redirect("students/ApplyOnline");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Apply Online';
            $year = date('Y');
            $data['year'] = $year;
            $data['class'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['group'] = $this->Admin_login->getGroupList();
            $data['main_content'] = $this->load->view('contents/admission_form', $data, true);
            $this->load->view('contents/admission_index', $data);
        }
    }

    function getAdmitCard()
    {
        if ($_POST) {
            $data = array();
            $pin = $this->input->post('txtPIN', true);
            $password = md5($this->input->post('txtPassword', true));
            $data['admit_card_status'] = $this->db->query("SELECT a.*,c.`name` AS class_name FROM `tbl_admission` AS a
LEFT JOIN `tbl_class` AS c ON c.`id` = a.`class_id`
WHERE a.`pin` = '$pin'
AND a.`password` = '$password' AND a.`payment_status` = '1'")->result_array();
            if (!empty($data['admit_card_status'])) {
                $status = 1;
            } else {
                $status = 0;
            }
            $data['status'] = $status;
            $data['title'] = 'Admit Card';
            $data['main_content'] = $this->load->view('contents/admit_card', $data, true);
            $this->load->view('contents/admission_index', $data);
        } else {
            $data = array();
            $data['title'] = 'Admit Card';
            $data['main_content'] = $this->load->view('contents/admit_card_form', $data, true);
            $this->load->view('contents/admission_index', $data);
        }
    }

    function getResult2017()
    {
        if ($_POST) {
            $student_id = $this->input->post('student_id', true);

            $student_info = $this->db->query("SELECT s.* FROM `tbl_student` AS s WHERE s.`student_code` = '$student_id'")->result_array();
            if (empty($student_info)) {
                $data = array();
                $data['title'] = 'Result Archive';
                $sdata['exception'] = "Sorry this Student Not Found !";
                $this->session->set_userdata($sdata);
                $data['main_content'] = $this->load->view('contents/result_archive_form_2017', $data, true);
                $this->load->view('contents/admission_index', $data);
            } else {
                //echo '<pre>';
                // print_r($student_info);
                // die;
                $data = array();
                $data['title'] = 'Result Archive 2017';
                $student_id = $student_info[0]['id'];
                $data['studnet_info'] = $this->db->query("SELECT s.*,c.`name` AS class_name,sc.`name` AS section_name,g.`name` AS group_name FROM tbl_student AS s 
LEFT JOIN tbl_class AS c ON c.`id` = s.`class_id`
LEFT JOIN `tbl_section` AS sc ON sc.`id` = s.`section_id`
LEFT JOIN `tbl_student_group` AS g ON g.`id` = s.`group`
WHERE s.`id` = '$student_id'")->result_array();
                $data['rdata'] = $this->Student->getGrandResultForStudent($student_id);
                $data['main_content'] = $this->load->view('contents/result_view_2017', $data, true);
                $this->load->view('contents/admission_index', $data);
            }

        } else {
            $data = array();
            $data['title'] = 'Result Archive 2017';
            $data['main_content'] = $this->load->view('contents/result_archive_form_2017', $data, true);
            $this->load->view('contents/admission_index', $data);
        }
    }


    function getResult()
    {
        if ($_POST) {
            $year = $this->input->post('year', true);
            $exam = $this->input->post('exam', true);
            $class = $this->input->post('class', true);
            $group = $this->input->post('group', true);
            $shift = $this->input->post('shift', true);
            $section = $this->input->post('section', true);
            $roll = $this->input->post('roll', true);

            $student_info = $this->db->query("SELECT s.`id` FROM `tbl_student` AS s WHERE s.`class_id` = '$class' AND s.`section_id` = '$section' AND s.`roll_no` = '$roll' AND
s.`group` = '$group' AND s.`shift_id` = '$shift' AND  s.`status` = 1")->result_array();
//echo '<pre>';
//print_r($student_info);
//die;
            if (empty($student_info)) {
                $data = array();
                $data['title'] = 'Result Archive';
                $sdata['exception'] = "Sorry This Student Not Found !";
                $this->session->set_userdata($sdata);
                $data['class'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
                $data['section'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
                $data['group'] = $this->Admin_login->getGroupList();
                $data['shift'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
                $data['main_content'] = $this->load->view('contents/result_archive_form', $data, true);
                $this->load->view('contents/admission_index', $data);
            } else {
                $data = array();
                $data['title'] = 'Result Archive';
                $student_id = $student_info[0]['id'];
                $data['group'] = $group;
                $data['exam'] = $this->db->query("SELECT * FROM `tbl_exam` AS e WHERE e.`id` = '$exam' AND e.`year` = '$year' ")->result_array();
                $data['Result'] = $this->Student->result_by_student_after_process($student_id, $exam);

               // echo '<pre>';
               // print_r($data['Result']);
               // die;
 
                if(empty($data['Result'])){
                    $data = array();
                    $sdata['exception'] = "Sorry result info. not found for this student!";
                    $this->session->set_userdata($sdata);
                    redirect("contents/getResult");
                }

                $exam_year_class_id = $data['Result']['result_process_info'][0]['class_id'];
                $exam_year_section_id = $data['Result']['result_process_info'][0]['section_id'];
                $exam_year_group = $data['Result']['result_process_info'][0]['group'];
                $exam_year_shift = $data['Result']['result_process_info'][0]['shift'];
                $gpa_with_optional = $data['Result']['result_process_info'][0]['gpa_with_optional'];
                $total_obtain_mark = $data['Result']['result_process_info'][0]['total_obtain_mark'];

                $data['max_total_obtain_mark'] = $this->db->query("SELECT MAX(`total_obtain_mark`) AS max_total_obtain_mark
 FROM `tbl_result_process` WHERE  exam_id = '$exam' AND class_id = '$exam_year_class_id' AND `section_id` = '$exam_year_section_id' AND `group` = '$exam_year_group' AND `shift` = '$exam_year_shift' ")->result_array();

                if ($gpa_with_optional != '0.00') {
                    $positions = $this->db->query("SELECT id,student_id,total_obtain_mark,gpa_with_optional FROM tbl_result_process
	WHERE exam_id = '$exam' AND class_id = '$exam_year_class_id' AND `section_id` = '$exam_year_section_id' AND `group` = '$exam_year_group' AND `shift` = '$exam_year_shift' 
	AND total_obtain_mark >= '$total_obtain_mark'
	ORDER BY total_obtain_mark DESC, gpa_with_optional DESC;")->result_array();
                    $data['position'] = count($positions);
                } else {
                    $data['position'] = 'N/A';
                }

                $data['main_content'] = $this->load->view('contents/result_view', $data, true);
                $this->load->view('contents/admission_index', $data);
            }

        } else {
            $data = array();
            $data['title'] = 'Result Archive';
            $data['class'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group'] = $this->Admin_login->getGroupList();
            $data['shift'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
            $data['main_content'] = $this->load->view('contents/result_archive_form', $data, true);
            $this->load->view('contents/admission_index', $data);
        }
    }

    public function ajax_exam_by_year()
    {
        $year = $_GET['year'];
        $data = array();
        $data['exam_list'] = $this->db->query("SELECT * FROM `tbl_exam` AS e WHERE e.`year` = '$year' AND e.`status` = '1'")->result_array();
        $this->load->view('contents/ajax_exam_by_year', $data);
    }

}
