<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Checkout extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('common/insert_model', 'common/custom_methods_model', 'products','E_commerce'));
        $this->load->library('cart', 'form_validation');
        $this->load->helper('form');
        date_default_timezone_set('Asia/Dhaka');
      
        $this->notification = array();
    }


    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['phone'] = $_POST['phone'];
            $data['email'] = $_POST['email'];
            $data['address'] = $_POST['address'];
            $data['password'] = $_POST['password'];
            $this->db->insert("tbl_customers", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('customers/index');
        }
        $data = array();
        $data['title'] = 'Add Customer Information';
        $data['heading_msg'] = "Add Customer Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('customers/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['phone'] = $_POST['phone'];
            $data['email'] = $_POST['email'];
            $data['address'] = $_POST['address'];
            $data['password'] = $_POST['password'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_customers', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('customers/index');
        }
        $data = array();
        $data['title'] = 'Update Customer Information';
        $data['heading_msg'] = "Update Customer Information";
        $data['is_show_button'] = "index";
        $data['customers'] = $this->db->query("SELECT * FROM `tbl_customers` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('customers/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
        $this->db->delete('tbl_customers', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('customers/index');
    }

    ///

    function index(){
        $data = array();
        $data['cart'] = $this->cart->contents();
        $data['cart_list'] = $this->cart->contents();
        $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
        $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
        $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");

        $data['index_header'] = $this->load->view('homes/index_header', $data, true);
        $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
        $this->load->view('homes/delivery_methods', $data);
        // Redirect if the cart is empty
        if($this->cart->total_items() <= 0){
            redirect('products/');
        }

        $custData = $data = array();

        // If order request is submitted
        $submit = $this->input->post('placeOrder');
        if(isset($submit)){
            // Form field validation rules
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');

            // Prepare customer data
            $custData = array(
                'name'     => strip_tags($this->input->post('name')),
                'email'     => strip_tags($this->input->post('email')),
                'phone'     => strip_tags($this->input->post('phone')),
                'address'=> strip_tags($this->input->post('address'))
            );

            // Validate submitted form data
            if($this->form_validation->run() == true){
                // Insert customer data
                $insert = $this->products->insertCustomer($custData);

                // Check customer data insert status
                if($insert){
                    // Insert order
                    $order = $this->placeOrder($insert);

                    // If the order submission is successful
                    if($order){
                        $sdata['message'] = $this->lang->line('add_success_message');
                        $this->session->set_userdata($sdata);
                        redirect($this->controller.'/orderSuccess/'.$order);
                    }else{
                        $sdata['message'] = $this->lang->line('add_error_message');
                    }
                }else{
                    $sdata['message'] = $this->lang->line('add_error_message');
                }
            }
        }

        // Customer data
        $data['custData'] = $custData;

        // Retrieve cart data from the session
        $data['cartItems'] = $this->cart->contents();

        // Pass products data to the view
        $this->load->view($this->controller.'/index', $data);
    }


    function generateSMSTransactionID(){
      $total_collection = count($this->db->query("SELECT id FROM tbl_orders")->result_array());
      $code = $this->generateRandomString(4);
      $code .= $total_collection;
      return $code;
    }

    function generateRandomString($length = 3) {
      $characters = '0123456789';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    function placeOrder(){
      if($_POST){

        if($this->cart->total_items() <= 0){
            $data['exception'] = "Please add to cart some item";
            $this->session->set_userdata($data);
            redirect('checkout/orderError');
        }

        //customer
        $customer = array();
        $customer['name'] = $this->input->post("name");
        $customer['address'] = $this->input->post("address");
        $customer['phone'] = $this->input->post("phone");
        $customer['email'] = $this->input->post("email");
        $this->db->insert('tbl_customer', $customer);
        $custID = $this->db->insert_id();
        //customer

          $order_code = $this->generateSMSTransactionID();

          // Insert order data
          $ordData = array(
              'order_code' => $order_code,
              'customer_id' => $custID,
              'total' => $this->cart->total()
          );
          $insertOrder = $this->products->insertOrder($ordData);

          if($insertOrder){
              // Retrieve cart data from the session
              $cartItems = $this->cart->contents();

              // Cart items
              $ordItemData = array();
              $i=0;
              foreach($cartItems as $item){
                  $ordItemData[$i]['order_id']     = $insertOrder;
                  $ordItemData[$i]['product_id']     = $item['id'];
                  $ordItemData[$i]['quantity']     = $item['qty'];
                  $ordItemData[$i]['sub_total']     = $item["subtotal"];
                  $i++;
              }

              if(!empty($ordItemData)){
                  // Insert order items
                  $insertOrderItems = $this->products->insertOrderItems($ordItemData);
                  if($insertOrderItems){
                      // Remove items from the cart
                      $this->cart->destroy();
                      // Return order ID
                      redirect('checkout/orderSuccess/' . $order_code);
                  }
              }
          }
          redirect('checkout/orderError');
      }
    }

    function orderSuccess($order_code){
        $data = array();
        $data['cart_list'] = $this->cart->contents();
        $data['is_slide_open'] = 0;
        $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
        $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
        $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
        $data['index_header'] = $this->load->view('homes/index_header', $data, true);
        $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
        $data['order_code'] = $order_code;
        $data['main_content'] = $this->load->view('homes/checkout_successful', $data, true);
        $this->load->view('homes/index', $data);
    }

    function orderError(){
        $data = array();
        $data['cart_list'] = $this->cart->contents();
        $data['is_slide_open'] = 0;
        $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
        $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
        $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
        $data['index_header'] = $this->load->view('homes/index_header', $data, true);
        $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
        $data['main_content'] = $this->load->view('homes/checkout_error', $data, true);
        $this->load->view('homes/index', $data);
    }
}
