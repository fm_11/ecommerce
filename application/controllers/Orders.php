<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Orders extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login', 'Message','Product'));
		$this->load->library('session');
		$this->notification = array();
	}

	public function index()
	{
		$cond = array();
		$data = array();
		if ($_POST) {
			$order_code = $this->input->post("order_code");
			$sdata['order_code'] = $order_code;
			$this->session->set_userdata($sdata);

			$cond['order_code'] = $order_code;
		} else {
			$order_code = $this->session->userdata('order_code');
			$cond['order_code'] = $order_code;
		}
		$row_per_page = 20;
		$data['title'] = "Orders";
		$data['heading_msg'] = "Orders";
	
		$this->load->library('pagination');
		$config['base_url'] = site_url('orders/index/');
		$config['per_page'] = $row_per_page;
		$config['total_rows'] = count($this->Product->get_all_orders(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['fees'] = $this->Product->get_all_orders($row_per_page, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
		$data['fee_module_type'] = $config[0]['fee_module_type'];
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('orders/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

  public function view_invoice($id)
  {
     	$data['title'] = "Order Invoice";
      $data['heading_msg'] = "Order Invoice";
      $Info = $this->Admin_login->fetReportHeader();
      $data['HeaderInfo'] = $Info;
      $cond['id'] = $id;
      $data['orders'] = $this->Product->get_all_orders(0, 0, $cond);
      $data['order_details'] = $this->db->query("SELECT oi.*,p.`product_name`,p.`feature_image_path` FROM `tbl_order_items` AS oi
INNER JOIN `tbl_product_info` AS p ON p.`id` = oi.`product_id`
WHERE oi.`order_id` = $id;")->result_array();
      // echo '<pre>';
      // print_r($data['orders']);
      // die;
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('orders/invoice', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
  }

  public function order_status_change($id,$status)
  {
    $data = array();
    $data['id'] = $id;
    $data['status'] = $status;
    $this->db->where('id', $data['id']);
    $this->db->update('tbl_orders', $data);
    $sdata['message'] = $this->lang->line('add_success_message');
    $this->session->set_userdata($sdata);
    redirect("orders/index");
  }

}
