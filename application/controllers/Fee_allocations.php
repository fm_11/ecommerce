<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Fee_allocations extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Message', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('fees') . ' ' . $this->lang->line('allocation');
        $data['heading_msg'] =  $this->lang->line('fees') . ' ' . $this->lang->line('allocation');
        $data['is_show_button'] = "add";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('fee_allocations/index/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_allocated_fee(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['fees'] = $this->Student_fee->get_all_allocated_fee(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('fee_allocations/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function get_fee_allocation_form_by_year_class()
    {
        $data = array();
        $data['year'] = $this->input->get('year', true);
        $data['class_id'] = $this->input->get('class_id', true);
		$data['group_id'] = $this->input->get('group_id', true);
		$data['category_id'] = $this->input->get('category_id', true);
        $class_id = $data['class_id'];
		$group_id = $data['group_id'];
        $year = $data['year'];
        $category_id = $data['category_id'];
        $allocated_data = $this->db->query("SELECT sc.*,c.name AS category_name,a.resident_amount,a.amount,a.id AS is_select
           FROM tbl_fee_sub_category AS sc
        INNER JOIN tbl_fee_category AS c ON c.id = sc.category_id
        LEFT JOIN tbl_fee_allocate AS a ON (a.sub_category_id = sc.id AND a.class_id = $class_id AND a.student_category_id = $category_id AND a.group_id = $group_id AND a.year = '$year')
         ORDER BY sc.`order` = 0, sc.`order`;")->result_array();

//        echo '<pre>';
//        print_r($allocated_data);
//        die;


		$allocated_data_array = array();
		$i = 0;
        foreach ($allocated_data as $row){
			$allocated_data_array[$i]['id'] = $row['id'];
			$allocated_data_array[$i]['category_id'] = $row['category_id'];
			$allocated_data_array[$i]['name'] = $row['name'];
			$allocated_data_array[$i]['is_waiver_applicable'] = $row['is_waiver_applicable'];
			$allocated_data_array[$i]['is_student_wise_fee'] = $row['is_student_wise_fee'];
			$allocated_data_array[$i]['remarks'] = $row['remarks'];
			$allocated_data_array[$i]['category_name'] = $row['category_name'];
			$allocated_data_array[$i]['resident_amount'] = $row['resident_amount'];
			$allocated_data_array[$i]['amount'] = $row['amount'];
			$allocated_data_array[$i]['is_select'] = $row['is_select'];
			if($row['is_select'] != ''){
				$allocate_id =  $row['is_select'];
				$already_selected_month = $this->db->query("SELECT GROUP_CONCAT(DISTINCT CONCAT(`collected_month`) 
ORDER BY `collected_month` ASC) AS already_selected_month FROM `tbl_fee_allocate_details` WHERE `allocate_id` = $allocate_id;")->row();
				//echo '<pre>';
				//echo $this->db->last_query();
				//print_r($already_selected_month);
				//die;
				$allocated_data_array[$i]['already_selected_month'] = $already_selected_month->already_selected_month;
			}else{
				$allocated_data_array[$i]['already_selected_month'] = '';
			}
			$i++;
		}

       //echo '<pre>';
        //print_r($allocated_data_array);
       // die;

		$data['allocated_data'] = $allocated_data_array;

        $this->load->view('fee_allocations/fee_allocation_form', $data);
    }


    public function add()
    {
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $loop_time = $this->input->post("loop_time");
            $class_id = $this->input->post("class_id");
			$group_id = $this->input->post("group_id");
			$student_category_id = $this->input->post("student_category_id");
            $year = $this->input->post("year");
            $i = 0;

			$this->db->delete('tbl_fee_allocate_details', array('class_id' => $class_id, 'group_id' => $group_id, 'student_category_id' => $student_category_id, 'year' => $year));
			$this->db->delete('tbl_fee_allocate', array('class_id' => $class_id, 'group_id' => $group_id, 'student_category_id' => $student_category_id, 'year' => $year));

            while ($i < $loop_time) {
                if ($this->input->post("is_selected_" . $i)) {
                    $data = array();
                    $data['class_id'] = $class_id;
					$data['group_id'] = $group_id;
					$data['student_category_id'] = $student_category_id;
                    $data['category_id'] = $_POST['category_id_' . $i];
                    $data['sub_category_id'] = $_POST['sub_category_id_' . $i];
                    $data['year'] = $year;
                    $data['amount'] = $_POST['amount_'. $i];
                    $data['resident_amount'] = $_POST['resident_amount_' . $i];
					$this->db->insert("tbl_fee_allocate", $data);
					$allocate_id = $this->db->insert_id();
					$details_data = array();

					if($this->input->post("all_month_" . $i)){
						$number_of_month = 12;
					}else{
						$number_of_month = count($_POST['collection_month_' . $i]);
					}

					//echo $number_of_month;die;

                    $start = 0;
                    while ($start < $number_of_month){
						$details_data[$start]['allocate_id'] = $allocate_id;
						$details_data[$start]['class_id'] = $class_id;
						$details_data[$start]['group_id'] = $group_id;
						$details_data[$start]['student_category_id'] = $student_category_id;

						if($this->input->post("all_month_" . $i)){
							$details_data[$start]['collected_month'] = $start + 1;
							$details_data[$start]['payment_due_date'] =	$data['year'] . '-' . ($start + 1) . '-01';
						}else{
							$details_data[$start]['collected_month'] = $_POST['collection_month_' . $i][$start];
							$details_data[$start]['payment_due_date'] =	$data['year'] . '-' . $_POST['collection_month_' . $i][$start] . '-01';
						}

						$details_data[$start]['year'] = $data['year'];
						$start++;
					}
					$this->db->insert_batch('tbl_fee_allocate_details', $details_data);
                }
                $i++;
            }

            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('fee_allocations/add');
        }
        $data = array();
        $data['title'] =  $this->lang->line('fees') . ' ' . $this->lang->line('allocation');
        $data['heading_msg'] =  $this->lang->line('fees') . ' ' . $this->lang->line('allocation');
        $data['is_show_button'] = "index";
        $data['action'] = '';
		$data['categories'] = $this->Student->getStudentCategory();
		$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['class'] = $this->db->query("SELECT * FROM tbl_class ORDER BY student_code_short_form")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('fee_allocations/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
		$this->db->delete('tbl_fee_allocate_details', array('allocate_id' => $id));
        $this->db->delete('tbl_fee_allocate', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("fee_allocations/index");
    }
}
