<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Exams extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Student'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Exam';
        $data['heading_msg'] = "Exam";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('exams/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Student->get_all_exam(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['exams'] = $this->Student->get_all_exam(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/exam_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function updateMsgStatusExamStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }
        $this->Student->update_exam_status($data);
        if ($status == 0) {
            echo '<a class="badge badge-success mb-1" title="Published" href="#" onclick="msgStatusUpdate(' . $id . ',1)">Published</a>';
        } else {
            echo '<a class="badge badge-warning mb-1" title="Unpublished" href="#" onclick="msgStatusUpdate(' . $id . ',0)">Unpublished</a>';
        }
    }


    public function add()
    {
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;

            $data = array();
            $data['name'] = $_POST['name'];
            $data['exam_type_id'] = $_POST['exam_type_id'];
            $data['year'] = $_POST['year'];
            $data['exam_order'] = $_POST['exam_order'];
            $data['is_applicable_for_final_calcultion'] = $_POST['is_applicable_for_final_calcultion'];
            $data['percentage_of_grand_result'] = $_POST['percentage_of_grand_result'];
            $data['is_annual_exam'] = $_POST['is_annual_exam'];
            $data['is_combined_result'] = $_POST['is_combined_result'];
            $data['combined_percentage'] = $_POST['combined_percentage'];
            $data['status'] = 0;
            $this->db->insert("tbl_exam", $data);

            $exam_id = $this->db->insert_id();
            if ($_POST['is_combined_result'] == '1') {
                $total_row = $_POST['total_row'];
                $i = 0;
                while ($i < $total_row) {
                    if ($_POST['is_use_' . $i] == '1') {
                        $c_data = array();
                        $c_data['exam_id'] = $exam_id;
                        $c_data['percentage'] = $_POST['percentage_' . $i];
                        $c_data['combined_exam_id'] = $_POST['combined_exam_id_' . $i];
                        $this->db->insert("tbl_exam_combined_result", $c_data);
                    }
                    $i++;
                }
            }

            $sdata['message'] =  $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('exams/add');
        }
        $data = array();
        $data['title'] = 'Add Exam';
        $data['heading_msg'] = "Add Exam";
        $data['is_show_button'] = "index";
        $data['exam_types'] = $this->db->query("SELECT * FROM tbl_exam_type")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/exam_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id=null)
    {
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['exam_type_id'] = $_POST['exam_type_id'];
            $data['year'] = $_POST['year'];
            $data['exam_order'] = $_POST['exam_order'];
            $data['is_applicable_for_final_calcultion'] = $_POST['is_applicable_for_final_calcultion'];
            $data['percentage_of_grand_result'] = $_POST['percentage_of_grand_result'];
            $data['is_annual_exam'] = $_POST['is_annual_exam'];
            $data['is_combined_result'] = $_POST['is_combined_result'];
            $data['combined_percentage'] = $_POST['combined_percentage'];

            $this->db->where('id', $_POST['id']);
            $this->db->update('tbl_exam', $data);

            $exam_id = $_POST['id'];
            $this->db->delete('tbl_exam_combined_result', array('exam_id' => $exam_id));


            if ($_POST['is_combined_result'] == '1') {
                $total_row = $_POST['total_row'];
                $i = 0;
                while ($i < $total_row) {
                    if ($_POST['is_use_' . $i] == '1') {
                        $c_data = array();
                        $c_data['exam_id'] = $exam_id;
                        $c_data['percentage'] = $_POST['percentage_' . $i];
                        $c_data['combined_exam_id'] = $_POST['combined_exam_id_' . $i];
                        $this->db->insert("tbl_exam_combined_result", $c_data);
                    }
                    $i++;
                }
            }

            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('exams/index');
        }
        $data = array();
        $data['title'] = 'Exam';
        $data['heading_msg'] = "Exam";
        $data['is_show_button'] = "index";
        $data['exam_types'] = $this->db->query("SELECT * FROM tbl_exam_type")->result_array();
        $data['exams'] = $this->db->query("SELECT * FROM tbl_exam WHERE id = '$id'")->row();
        $year = $data['exams']->year;
        $data['combined_exam_list'] = $this->db->query("SELECT er.`combined_exam_id`,
          er.`percentage`,e.* FROM `tbl_exam` AS e
LEFT JOIN `tbl_exam_combined_result` AS er ON er.`combined_exam_id` = e.`id` AND er.`exam_id` = '$id'
WHERE e.`year` = '$year' AND e.`id` != '$id'")->result_array();

        // echo '<pre>';
        // print_r($data['combined_exam_list']);
        // die;
        $data['combined_result'] = $this->load->view('students/combined_exam_list', $data, true);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/exam_edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function get_exam_list_for_combined()
    {
        $year = $_GET['year'];
        $data = array();
        $data['combined_exam_list'] = $this->db->query("SELECT * FROM `tbl_exam`
          WHERE `year` = '$year' ORDER BY `exam_order`")->result_array();
        $this->load->view('students/combined_exam_list_for_add', $data);
    }

    public function getExamByYear()
    {
        $year = $_GET['year'];
        $data = array();
        $data['exam_list'] = $this->db->query("SELECT * FROM `tbl_exam` AS e WHERE e.`year` = '$year' ORDER BY exam_order")->result_array();
        $this->load->view('exams/exam_list_by_year', $data);
    }

    public function get_exam_list_for_combined_edit()
    {
        $year = $_GET['year'];
        $exam_id = $_GET['exam_id'];
        //echo $exam_id;
        //  die;
        $data = array();
        $data['combined_exam_list'] = $this->db->query("SELECT er.`combined_exam_id`,
          er.`percentage`,e.* FROM `tbl_exam` AS e
LEFT JOIN `tbl_exam_combined_result` AS er ON er.`combined_exam_id` = e.`id` AND er.`exam_id` = '$exam_id'
WHERE e.`year` = '$year' AND e.`id` != '$exam_id'")->result_array();
        // echo '<pre>';
        // print_r($data['combined_exam_list']);
        // die;
        $this->load->view('students/combined_exam_list_for_add', $data);
    }

    public function delete($id)
    {
        $marking_info = $this->db->where('exam_id', $id)->get('tbl_result_process')->result_array();
        if (!empty($marking_info)) {
            $sdata['exception'] = "Student Exam Result Depended Data Found !";
            $this->session->set_userdata($sdata);
            redirect("exams/index");
        }
        $this->db->query("DELETE FROM tbl_exam_combined_result WHERE `exam_id` = '$id'");
        $this->db->query("DELETE FROM tbl_exam WHERE `id` = '$id'");
        $sdata['message'] = "Exam Deleted Successfully.";
        $this->session->set_userdata($sdata);
        redirect('exams/index');
    }
}
