
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Exam_routines extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Admin_login'));

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	function index(){
		$data = array();
		if($_POST){
			$exam_id = $this->input->post("exam_id");
			$class_id = $this->input->post("class_id");
			$group_id = $this->input->post("group_id");
			$data['exam_id'] = $exam_id;
			$data['class_id'] = $class_id;
			$data['group_id'] = $group_id;

			$examInfo = $this->Admin_login->getExamDetailsById($exam_id);
			$exam_type_id = $examInfo->exam_type_id;
			$subject_list = $this->Admin_login->getClassGroupAndExamTypeWiseSubjectList($class_id, $exam_type_id,$group_id);
			if(empty($subject_list)){
				$sdata['exception'] = "Subject not configure for this class and exam";
				$this->session->set_userdata($sdata);
				redirect("exam_routines/index");
			}

			$customSubjectList = array();
			$i = 0;
			foreach ($subject_list as $subject){
				$customSubjectList[$i]['subject_id'] = $subject['subject_id'];
				$customSubjectList[$i]['name'] = $subject['name'];
				$customSubjectList[$i]['code'] = $subject['code'];
				$subject_id = $subject['subject_id'];
				$subject_routine = $this->db->query("SELECT * FROM tbl_exam_routine 
                                   WHERE `exam_id` = $exam_id AND `class_id` = $class_id AND `subject_id` = $subject_id")->row();
				if(empty($subject_routine)){
					$customSubjectList[$i]['room_no'] = '';
					$customSubjectList[$i]['date'] = '';
					$customSubjectList[$i]['session_id'] = '';
				}else{
					$customSubjectList[$i]['room_no'] = $subject_routine->room_id;
					$customSubjectList[$i]['date'] = $subject_routine->date;
					$customSubjectList[$i]['session_id'] = $subject_routine->exam_session_id;
				}
				$i++;
			}
//			echo '<pre>';
//			print_r($customSubjectList);
//			die;
			$data['subject_list'] = $customSubjectList;

			$data['exam_sessions'] = $this->db->query("SELECT * FROM tbl_exam_session")->result_array();
			$data['rooms'] = $this->db->query("SELECT * FROM tbl_room_no")->result_array();
			$data['process_list'] = $this->load->view('exam_routines/process_list', $data, true);
		}
		$data['title'] = 'Exam Routine';
		$data['heading_msg'] = "Exam Routine";
		$data['exam_sessions'] = $this->db->query("SELECT * FROM tbl_exam_session")->result_array();
		$data['classes'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
		$data['GroupList'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$current_system_year = $this->Admin_login->getCurrentSystemYear();
		$data['ExamList'] = $this->db->query("SELECT * FROM tbl_exam WHERE year = $current_system_year")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('exam_routines/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}


	function checkRoomNoByDate(){
		$room_id = $this->input->get('room_id', true);
		$session_id = $this->input->get('session_id', true);
		$date = $this->input->get('date', true);
		$checkRoom = $this->db->query("SELECT id FROM `tbl_exam_routine` AS er
					    	WHERE er.`date` = '$date' AND er.`room_id` = $room_id AND er.`exam_session_id` = $session_id")->result_array();
		if(empty($checkRoom)){
			echo '0';
		}else{
			echo '1';
		}
	}

	function routine_data_save(){
		if($_POST){
			$exam_id = $this->input->post("exam_id");
			$class_id = $this->input->post("class_id");
			$group_id = $this->input->post("group_id");
			$total_subject = $this->input->post("total_subject");
			$i = 0;
			while ($i < $total_subject){
				$subject_id = $this->input->post("subject_id_" . $i);
				$this->db->query("DELETE FROM `tbl_exam_routine` WHERE `exam_id` = '$exam_id' 
                              AND `class_id` = '$class_id' AND `subject_id` = '$subject_id'");

				$data = array();
				$data['exam_id'] = $exam_id;
				$data['class_id'] = $class_id;
				$data['group_id'] = $group_id;
				$data['subject_id'] = $this->input->post("subject_id_" . $i);
				$data['date'] =  $this->input->post("date_" . $i);
				$data['exam_session_id'] = $this->input->post("session_id_" . $i);
				$data['room_id'] = $this->input->post("room_no_id_" . $i);
				$this->db->insert("tbl_exam_routine", $data);
				$i++;
			}
			$sdata['message'] =  $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect("exam_routines/index");
		}
	}
}
