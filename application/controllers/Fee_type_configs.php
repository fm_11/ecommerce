<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fee_type_configs extends CI_Controller
{

     public $SOFTWARE_START_YEAR = '';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Student_fee', 'Admin_login'));

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    function index()
    {
        $data = array();
        if ($_POST) {
            $year = $_POST['year'];
        } else {
            $year = date('Y');
        }
        $data['title'] = 'Fee Type Configuration';
        $data['heading_msg'] = "Fee Type Configuration";
        $data['year'] = $year;
        $data['is_show_button'] = "add";
        $data['annual_fee_types'] = $this->db->query("SELECT * FROM `tbl_fee_type` WHERE fee_type = 'A' AND `year` = '$year'")->result_array();
        $data['tri_annual_fee_types'] = $this->db->query("SELECT * FROM `tbl_fee_type` WHERE fee_type = 'T' AND `year` = '$year'")->result_array();
        $data['quarterly_fee_types'] = $this->db->query("SELECT * FROM `tbl_fee_type` WHERE fee_type = 'Q' AND `year` = '$year'")->result_array();
        $data['monthly_fee_types'] = $this->db->query("SELECT * FROM `tbl_fee_type` WHERE fee_type = 'M' AND `year` = '$year'")->result_array();
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('fee_type_configs/fee_type_view', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function add()
    {
        if ($_POST) {
            $year = $_POST['year'];
            $j = 0;
            $data = array();
            $data[$j]['year'] = $year;
            $data[$j]['month'] = $_POST['annual_fee_type'];
            $data[$j]['fee_type'] = "A";
            $j = $j + 1;
            if (count($_POST['tri_annual_fee_type']) != 3) {
                $sdata['exception'] = "Sorry Tri-Annual fee type only 3 month allow";
                $this->session->set_userdata($sdata);
                redirect("fee_type_configs/index");
            } else {
                $i = 0;
                while ($i < count($_POST['tri_annual_fee_type'])) {
                    $data[$j]['year'] = $year;
                    $data[$j]['month'] = $_POST['tri_annual_fee_type'][$i];
                    $data[$j]['fee_type'] = "T";
                    $i++;
                    $j++;
                }
            }

            if (count($_POST['quarterly_fee_type']) != 4) {
                $sdata['exception'] = "Sorry quarterly fee type only 4 month allow";
                $this->session->set_userdata($sdata);
                redirect("fee_type_configs/index");
            } else {
                $i = 0;
                while ($i < count($_POST['quarterly_fee_type'])) {
                    $data[$j]['year'] = $year;
                    $data[$j]['month'] = $_POST['quarterly_fee_type'][$i];
                    $data[$j]['fee_type'] = "Q";
                    $i++;
                    $j++;
                }
            }

            if (count($_POST['monthly_fee_type']) != 12) {
                $sdata['exception'] = "Sorry monthly fee type only 12 month allow";
                $this->session->set_userdata($sdata);
                redirect("fee_type_configs/index");
            } else {
                $i = 0;
                while ($i < count($_POST['monthly_fee_type'])) {
                    $data[$j]['year'] = $year;
                    $data[$j]['month'] = $_POST['monthly_fee_type'][$i];
                    $data[$j]['fee_type'] = "M";
                    $i++;
                    $j++;
                }
            }
            if ($this->db->delete('tbl_fee_type', array('year' => $year))) {
                $this->db->insert_batch('tbl_fee_type', $data);
                $sdata['message'] = "Data added successfully !";
                $this->session->set_userdata($sdata);
                redirect("fee_type_configs/index");
            } else {
                $sdata['exception'] = "Sorry Data doesn't saved !";
                $this->session->set_userdata($sdata);
                redirect("fee_type_configs/index");
            }
        } else {
            $data = array();
            $data['title'] = 'Fee Type Configuration';
            $data['heading_msg'] = "Fee Type Configuration";
            $data['is_show_button'] = "index";
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('fee_type_configs/fee_type_config', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

}
