<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Resident_students extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student', 'Message', 'Admin_login', 'common/custom_methods_model', 'Student_fee'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		//echo '<pre>'; print_r($user_info); die;
		$this->SOFTWARE_START_YEAR = $user_info[0]->config_info[0]['software_start_year'];
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Resident Student';
		$data['heading_msg'] = "Resident Student";
		if ($_POST) {
			$year = $this->input->post("year");
			$group_id = $this->input->post("group_id");
			$data['year'] = $year;
			$class_shift_section_id = $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];
			$data['class_id'] = $class_id;
			$data['shift_id'] = $shift_id;
			$data['section_id'] = $section_id;
			$data['group_id'] = $group_id;
			$data['year'] = $year;
			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['student_info'] = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code`,sl.`year`,sl.`id` AS already_save  FROM `tbl_student` AS s
LEFT JOIN `tbl_student_resident_info` AS sl ON sl.`student_id` = s.`id` AND sl.`year` = '$year'
WHERE s.`class_id` = '$class_id' AND  s.`shift_id` = '$shift_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group_id' ORDER BY ABS(s.roll_no);")->result_array();
			$data['student_table'] = $this->load->view('resident_students/student_allocate_table', $data, true);
		}
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['groups'] = $this->Admin_login->getGroupList();
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['maincontent'] = $this->load->view('resident_students/index', $data, true);
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function resident_data_save()
	{
		$year = $this->input->post("year");
		$loop_time = $this->input->post("loop_time");
		$class_id = $this->input->post("class_id");
		$section_id = $this->input->post("section_id");
		$group_id = $this->input->post("group_id");
		$shift_id = $this->input->post("shift_id");
		$this->db->delete('tbl_student_resident_info', array('class_id' => $class_id, 'section_id' => $section_id, 'group' => $group_id, 'shift_id' => $shift_id, 'year' => $year));
		$i = 1;
		while ($i <= $loop_time) {
			if ($this->input->post("is_allow_" . $i)) {
				$data = array();
				$data['student_id'] = $this->input->post("student_id_" . $i);
				$data['year'] = $year;
				$data['class_id'] = $class_id;
				$data['section_id'] = $section_id;
				$data['group'] = $group_id;
				$data['shift_id'] = $shift_id;
				$data['date'] = date('Y-m-d');
				$this->db->insert('tbl_student_resident_info', $data);
			}
			$i++;
		}
		$sdata['message'] = $this->lang->line('add_success_message');
		$this->session->set_userdata($sdata);
		redirect("resident_students/index");
	}

}
