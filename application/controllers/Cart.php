<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cart extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('common/insert_model', 'common/custom_methods_model', 'products','E_commerce'));
        $this->load->library('session', 'cart');
        date_default_timezone_set('Asia/Dhaka');
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['cart'] = $this->cart->contents();
        $data['cart_list'] = $this->cart->contents();
        // echo '<pre>';
        // print_r($data['cart_list']);
        // die;
        $data['is_slide_open'] = 0;
        $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
        $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
        $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
        $data['index_header'] = $this->load->view('homes/index_header', $data, true);
        $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
        $data['main_content'] = $this->load->view('homes/shopping_cart', $data, true);
        $this->load->view('homes/index', $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['phone'] = $_POST['phone'];
            $data['email'] = $_POST['email'];
            $data['address'] = $_POST['address'];
            $data['password'] = $_POST['password'];
            $this->db->insert("tbl_customers", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('customers/index');
        }
        $data = array();
        $data['title'] = 'Add Customer Information';
        $data['heading_msg'] = "Add Customer Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('customers/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['phone'] = $_POST['phone'];
            $data['email'] = $_POST['email'];
            $data['address'] = $_POST['address'];
            $data['password'] = $_POST['password'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_customers', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('customers/index');
        }
        $data = array();
        $data['title'] = 'Update Customer Information';
        $data['heading_msg'] = "Update Customer Information";
        $data['is_show_button'] = "index";
        $data['customers'] = $this->db->query("SELECT * FROM `tbl_customers` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('customers/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
        $this->db->delete('tbl_customers', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('customers/index');
    }

    function addToCart($id)
    {
        //echo 666;die;
        $quantity = 1;
        if($_POST){
          $quantity = $_POST['quantity'];
        }
        // echo $quantity;
        // die;
        $data = array();
        $product = $this->db->query("SELECT * FROM `tbl_product_info` WHERE id = '$id'")->row_array();
        // echo '<pre>';
        // print_r($product);
        // die;
        // Add product to the cart
        $data = array(
            'id'    => $product['id'],
            'qty'    => $quantity,
            'price'    => $product['standard_price'],
            'name'    => $product['product_name'],
            'image' => $product['image_path_1']
        );
        $this->cart->insert($data);
        $cart = $this->cart->contents();

         // print_r($cart);
         // die;
        redirect('cart/index');
    }

    function updateItemQty(){
        $update = 0;

        // Get cart item info
        $rowid = $this->input->get('rowid');
        $qty = $this->input->get('qty');

        // Update item in the cart
        if(!empty($rowid) && !empty($qty)){
            $data = array(
                'rowid' => $rowid,
                'qty'   => $qty
            );
            $update = $this->cart->update($data);
        }

        // Return response
        echo $update?'ok':'err';
    }

    function removeItem($rowid){
        // Remove item from cart
        $remove = $this->cart->remove($rowid);

        // Redirect to the cart page
        redirect('cart/index');
    }
}
