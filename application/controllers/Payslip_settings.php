<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Payslip_settings extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}


	public function index()
	{
		if ($_POST) {
			$data = array();
			$data['id'] = $_POST['id'];
			$data['asset_head_id'] = $_POST['asset_head_id'];
			$data['collected_user'] = $_POST['collected_user'];
			$this->db->where('id', $data['id']);
			$this->db->update('tbl_payslip_setting', $data);
			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect('payslip_settings/index');
		}
		$data = array();
		$data['title'] = "Payslip Settings";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['config'] = $this->Admin_login->get_payslip_setting();
		if(empty($data['config'])){
			$sdata['exception'] = "Initial configuration not found";
			$this->session->set_userdata($sdata);
			redirect('dashboard/index');
		}
		// echo '<pre>';
		// print_r($data['heads']);
		// die;
		$data['asset_category'] = $this->db->query("SELECT * FROM tbl_asset_category")->result_array();
		$data['maincontent'] = $this->load->view('payslip_settings/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}

