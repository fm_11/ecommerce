<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Birthday_sms extends CI_Controller
{

    public $SOFTWARE_START_YEAR = '';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function SendBirthDaySMS()
    {
        $data = array();
        if ($_POST) {

            $ReceiverType = $this->input->post('ReceiverType', true);
            if ($ReceiverType == 'Student') {
                $data['teacherSMSStatus'] = 0;
                $data['studentSMSStatus'] = 1;
                $student_inforeturn = $this->Message->GetStudentInfoByBirthday();
                if ($student_inforeturn['status'] == 1) {
                    $data['studentInfoStatus'] = 1;
                    $data['studentInfo'] = $student_inforeturn['studentData'];
                } else {
                    $data['studentInfoStatus'] = 0;
                }
            } elseif ($ReceiverType == 'Teacher') {
                $data['teacherSMSStatus'] = 1;
                $data['studentSMSStatus'] = 0;
                $teacher_inforeturn = $this->Message->GetTeacherInfoByBirthday();
                if ($teacher_inforeturn['status'] == 1) {
                    $data['teacherInfoStatus'] = 1;
                    $data['teacherInfo'] = $teacher_inforeturn['teacherData'];
                } else {
                    $data['teacherInfoStatus'] = 0;
                }
            }

            $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        }
        $data['title'] = 'Send Birth Day SMS';
        $data['heading_msg'] = "Send Birth Day SMS";
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
        $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('birthday_sms/SendBirthDaySMS', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
