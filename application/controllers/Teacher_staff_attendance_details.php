<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Teacher_staff_attendance_details extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Students_report', 'Student', 'Timekeeping'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Teacher/Staff Attendance Details';
		$data['heading_msg'] = 'Teacher/Staff Attendance Details';
		if ($_POST) {
			$year = $this->input->post("year");
			$month = $this->input->post("month");
			$data['year'] = $year;
			$data['month'] = $month;
			$form_date = $year . "-" . $month . "-01";
			$to_date = $year . "-" . $month . "-" . cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$data['number_of_days'] = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$cond = array();
			$cond['form_date'] = $form_date;
			$cond['to_date'] = $to_date;
			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;
			$cond['number_of_days'] = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$data['time_keeping_info'] = $this->Timekeeping->get_teacher_staff_attendance_details($cond);
//			echo '<pre>';
//			print_r($data['time_keeping_info']);
//			die;
			$data['report'] = $this->load->view('teacher_staff_attendance_details/report', $data, true);
			$this->load->view('report_content/report_main_content', $data);
		}else{
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('teacher_staff_attendance_details/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}
}
