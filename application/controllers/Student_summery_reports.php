<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Student_summery_reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student','Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = "Student Summery Report";
        $data['heading_msg'] = "Student Summery Report";
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;
            $this->load->library('numbertowords');
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['address'] = $SchoolInfo[0]['address'];
            $data['HeaderInfo'] = $Info;
            $year = $this->input->post("year");
            $report_type = $this->input->post("report_type");

            $data['year'] = $year;
            $data['report_type'] = $report_type;

            if ($report_type == 'cw') {
                $data['idata'] = $this->db->query("SELECT COUNT(s.`id`) AS total_student, c.`name` AS class_name FROM `tbl_student` AS s
  INNER JOIN `tbl_class` AS c ON c.`id` = s.`class_id`
  WHERE s.`status` = 1 AND s.`year` = '$year'
  GROUP BY s.`class_id`")->result_array();
                $data['report'] = $this->load->view('student_summery_reports/class_wise_report', $data, true);
            } elseif ($report_type == 'sw') {
                $data['idata'] = $this->db->query("SELECT COUNT(s.`id`) AS total_student,
                c.`name` AS class_name, sc.`name` AS section_name,
s.`class_id`, s.`section_id` FROM `tbl_student` AS s
INNER JOIN `tbl_class` AS c ON c.`id` = s.`class_id`
INNER JOIN `tbl_section` AS sc ON sc.`id` = s.`section_id`
WHERE s.`status` = 1 AND s.`year` = '$year'
GROUP BY s.`class_id`,s.`section_id`")->result_array();
                $data['report'] = $this->load->view('student_summery_reports/section_wise_report', $data, true);
            } elseif ($report_type == 'csw') {
                $data['idata'] = $this->db->query("SELECT COUNT(s.`id`) AS total_student,
                 sf.`name`  AS shift_name,
c.`name` AS class_name, sc.`name` AS section_name,
s.`class_id`, s.`section_id` FROM `tbl_student` AS s
INNER JOIN `tbl_class` AS c ON c.`id` = s.`class_id`
INNER JOIN `tbl_section` AS sc ON sc.`id` = s.`section_id`
INNER JOIN `tbl_shift` AS sf ON sf.`id` = s.`shift_id`
WHERE s.`status` = 1 AND s.`year` = '$year'
GROUP BY s.`shift_id`,s.`class_id`,s.`section_id`")->result_array();
                $data['report'] = $this->load->view('student_summery_reports/shift_wise_report', $data, true);
            }
        }
        if (isset($_POST['pdf_download'])) {
            $data['is_pdf'] = 1;
            //Dom PDF
            $this->load->library('mydompdf');
            if ($report_type == 'cw') {
                $html = $this->load->view('student_summery_reports/class_wise_report', $data, true);
            } elseif ($report_type == 'sw') {
                $html = $this->load->view('student_summery_reports/section_wise_report', $data, true);
            } elseif ($report_type == 'csw') {
                $html = $this->load->view('student_summery_reports/shift_wise_report', $data, true);
            }
			$this->mydompdf->createPDF($html, 'StudentSummeryReport', true, 'A4', 'portrait');
        //Dom PDF
        } else {
            $data['years'] = $this->Admin_login->getYearList(0, 0);
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_summery_reports/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
