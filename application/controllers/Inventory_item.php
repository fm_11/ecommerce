<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Inventory_item extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message','Inventory','common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = "Inventory Item";
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $sdata['name'] = $name;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
        } else {
            $name = $this->session->userdata('name');
            $cond['name'] = $name;
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('inventory_item/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Inventory->get_item_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['item'] = $this->Inventory->get_item_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('inventory_item/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $name = $this->input->post('name');
            $code = $this->input->post('code');
            $category_id = $this->input->post('category_id');
            $sub_category_id = $this->input->post('sub_category_id');
            $ware_house_id = $this->input->post('ware_house_id');
            $opening_quantity = $this->input->post('opening_quantity');
            $date = date('Y-m-d');
            $sales_price = $this->input->post('sales_price');
            $data['name']=$name;
            $data['code']=$code;
            $data['category_id']=$category_id;
            $data['sub_category_id']=$sub_category_id;
            $data['ware_house_id']=$ware_house_id;
            $data['opening_quantity']=$opening_quantity;
            $data['date'] = $date;
            $data['sales_price']=$sales_price;
            if ($sales_price<=0) {
              $sdata['exception'] = "This value '".$sales_price."' is not acceptable.";
              $this->session->set_userdata($sdata);
              redirect("inventory_item/add");
            }
            if($this->Inventory->checkifexist_item_by_name($name))
            {
              $sdata['exception'] = "This name '".$name."' already exist.";
              $this->session->set_userdata($sdata);
              redirect("inventory_item/add");
            }
            if ($this->Inventory->add_item($data)) {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("inventory_item/index");
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
                redirect("inventory_item/add");
            }
        }
        $data = array();
		$data['title'] = "Inventory Item";
        $data['action'] = 'add';
	    $data['is_show_button'] = "index";
        $data['category'] = $this->Inventory->get_catgory_list();
        $data['subcategory'] = $this->Inventory->get_subcategory_by_category(0);
        $data['warehouse'] = $this->Inventory->get_warehouse_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('inventory_item/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          $data = array();
          $data['id']=$id;
          $data['name']=$this->input->post('name');
          $data['code']=$this->input->post('code');
          $data['category_id']=$this->input->post('category_id');
          $data['sub_category_id']=$this->input->post('sub_category_id');
          $data['ware_house_id']=$this->input->post('ware_house_id');
          $data['opening_quantity']=$this->input->post('opening_quantity');
          $data['date'] = $this->input->post('date');
          $data['sales_price']=$this->input->post('sales_price');
          if ($data['sales_price'] <0 ) {
             $sdata['exception'] = "This value '" . $data['sales_price'] . "' is not acceptable.";
             $this->session->set_userdata($sdata);
             redirect("inventory_item/edit/".$id);
          }
          if($this->Inventory->checkifexist_update_item_by_name($data['name'],$id))
          {
            $sdata['exception'] = "This name '(".$data['name'].")' already exist.";
            $this->session->set_userdata($sdata);
            redirect("inventory_item/edit/".$id);
          }
            if ($this->Inventory->edit_item($data, $id)) {
                $sdata['message'] = $this->lang->line('edit_success_message');
                $this->session->set_userdata($sdata);
                redirect("inventory_item/index");
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
                redirect("inventory_item/edit");
            }
        }
        $data = array();
		$data['title'] = "Inventory Item";
        $data['action'] = 'edit/' . $id;
        $data['is_show_button'] = "index";
        $data['row_data'] = $this->Inventory->read_item($id);
        $data['category'] = $this->Inventory->get_catgory_list();
        $data['subcategory'] = $this->Inventory->get_subcategory_by_category($data['row_data']->category_id);
        $data['warehouse'] = $this->Inventory->get_warehouse_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('inventory_item/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->Inventory->delete_item($id);
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("inventory_item/index");
    }
    public function ajax_subcategory_by_category()
    {
        $category_id = $_GET['category_id'];
        $data["dropdown_list"]= $this->Inventory->get_subcategory_by_category($category_id);
        $this->load->view('inventory_item/dropdown_list', $data);
    }
}
