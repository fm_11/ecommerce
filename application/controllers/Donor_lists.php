<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Donor_lists extends CI_Controller
{
  public $SOFTWARE_START_YEAR = '';

  public function __construct()
  {
      parent::__construct();
      $this->load->database();
      $this->load->model(array('Admin_login'));
      $this->load->library('session');
      date_default_timezone_set('Asia/Dhaka');
      $user_info = $this->session->userdata('user_info');

      if (empty($user_info)) {
          $sdata = array();
          $sdata['exception'] = "Please Login Vaild User !";
          $this->session->set_userdata($sdata);
          redirect("login/index");
      }
      $this->notification = array();
  }

  public function index()
  {
    $data = array();
    $cond = array();
	  if ($_POST) {
		  $name = $this->input->post("name");
		  $present_district = $this->input->post("present_district");
		  $permanent_district = $this->input->post("permanent_district");
		  $mobile = $this->input->post("mobile");
		  $donation_type = $this->input->post("donation_type");

		  $sdata['name'] = $name;
		  $sdata['present_district'] = $present_district;
		  $sdata['permanent_district'] = $permanent_district;
		  $sdata['mobile'] = $mobile;
		  $sdata['donation_type'] = $donation_type;
		  $this->session->set_userdata($sdata);

		  $cond['name'] = $name;
		  $cond['present_district'] = $present_district;
		  $cond['permanent_district'] = $permanent_district;
		  $cond['mobile'] = $mobile;
		  $cond['donation_type'] = $donation_type;
	  } else {
		  $name = $this->session->userdata('name');
		  $present_district = $this->session->userdata('present_district');
		  $permanent_district = $this->session->userdata('permanent_district');
		  $mobile = $this->session->userdata('mobile');
		  $donation_type = $this->session->userdata('donation_type');

		  $cond['name'] = $name;
		  $cond['present_district'] = $present_district;
		  $cond['permanent_district'] = $permanent_district;
		  $cond['mobile'] = $mobile;
		  $cond['donation_type'] = $donation_type;
	  }

    $data['title'] = 'Donor List Information';
    $data['heading_msg'] = "Donor List Information";
    $data['is_show_button'] = "add";
    $this->load->library('pagination');
    $config['base_url'] = site_url('donor_lists/index/');
    $config['per_page'] = 20;
    $config['total_rows'] = count($this->Admin_login->get_all_donor_list(0, 0, $cond));
    $this->pagination->initialize($config);
    $data['donor_lists'] = $this->Admin_login->get_all_donor_list(20, (int)$this->uri->segment(3), $cond);
    $data['counter'] = (int)$this->uri->segment(3);
    $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
    $data['maincontent'] = $this->load->view('donor_lists/index', $data, true);
    $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
  }

  public function add()
  {
      if ($_POST) {
          $data = array();
          $data['name'] = $_POST['name'];
		  $data['member_code'] = $this->generateMemberCode();
          $data['father_spouse_name'] = $_POST['father_spouse_name'];
          $data['present_address'] = $_POST['present_address'];
          $data['mobile1'] = $_POST['mobile1'];
          $data['mobile2'] = $_POST['mobile2'];
          $data['present_ward'] = $_POST['present_ward'];
          $data['present_post_office'] = $_POST['present_post_office'];
          $data['present_thana'] = $_POST['present_thana'];
          $data['present_district'] = $_POST['present_district'];
          $data['donation_type'] = $_POST['donation_type'];
          $data['amount'] = $_POST['amount'];
          $data['permanent_address'] = $_POST['permanent_address'];
          $data['permanent_ward'] = $_POST['permanent_ward'];
          $data['permanent_post_office'] = $_POST['permanent_post_office'];
          $data['permanent_thana'] = $_POST['permanent_thana'];
          $data['permanent_district'] = $_POST['permanent_district'];

          $this->db->insert("tbl_donor_lists", $data);
          $sdata['message'] = $this->lang->line('add_success_message');
          $this->session->set_userdata($sdata);
          redirect('donor_lists/index');
      }
      $data = array();
      $data['title'] = 'Add Donor Information';
      $data['heading_msg'] = "Add Donor Information";
      $data['is_show_button'] = "index";
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('donor_lists/add', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
  }

	function generateMemberCode(){
		$code = "";
		$total_member = count($this->db->query("SELECT id FROM tbl_donor_lists")->result_array());
		return $code.str_pad(($total_member + 1), 4, '0', STR_PAD_LEFT);
	}

  public function edit($id = null)
  {
      if ($_POST) {
          $data = array();
          $data['id'] = $_POST['id'];
          $data['name'] = $_POST['name'];
          $data['father_spouse_name'] = $_POST['father_spouse_name'];
          $data['present_address'] = $_POST['present_address'];
          $data['mobile1'] = $_POST['mobile1'];
          $data['mobile2'] = $_POST['mobile2'];
          $data['present_ward'] = $_POST['present_ward'];
          $data['present_post_office'] = $_POST['present_post_office'];
          $data['present_thana'] = $_POST['present_thana'];
          $data['present_district'] = $_POST['present_district'];
		  $data['donation_type'] = $_POST['donation_type'];
          $data['amount'] = $_POST['amount'];
          $data['permanent_address'] = $_POST['permanent_address'];
          $data['permanent_ward'] = $_POST['permanent_ward'];
          $data['permanent_post_office'] = $_POST['permanent_post_office'];
          $data['permanent_thana'] = $_POST['permanent_thana'];
          $data['permanent_district'] = $_POST['permanent_district'];
          $this->db->where('id', $data['id']);
          $this->db->update('tbl_donor_lists', $data);
          $sdata['message'] = $this->lang->line('edit_success_message');
          $this->session->set_userdata($sdata);
          redirect('donor_lists/index');
      }
      $data = array();
      $data['title'] = 'Update Donor Information';
      $data['heading_msg'] = "Update Donor Information";
      $data['is_show_button'] = "index";
      $data['donor_lists'] = $this->db->query("SELECT * FROM `tbl_donor_lists` WHERE id = '$id'")->result_array();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('donor_lists/edit', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
  }

  public function delete($id)
  {
      $this->db->delete('tbl_donor_lists', array('id' => $id));
      $sdata['message'] = $this->lang->line('delete_sucess_message');
      $this->session->set_userdata($sdata);
      redirect('donor_lists/index');
  }

}

 ?>
