
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Movement_registers extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Timekeeping', 'Admin_login'));
		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}


	public function index()
	{
		$data = array();
		$cond = array();
		if ($_POST) {
			$name = $this->input->post("name");
			$from_date = $this->input->post("from_date");
			$to_date = $this->input->post("to_date");

			$sdata['from_date'] = $from_date;
			$sdata['to_date'] = $to_date;
			$sdata['name'] = $name;
			$this->session->set_userdata($sdata);

			$cond['name'] = $name;
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
		} else {
			$name = $this->session->userdata('name');
			$from_date = $this->session->userdata('from_date');
			$to_date = $this->session->userdata('to_date');
			$cond['name'] = $name;
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
		}
		$data['title'] = "Movement Register";
		$data['heading_msg'] = "Movement Register";
		$this->load->library('pagination');
		$config['base_url'] = site_url('movement_registers/index/');
		$config['per_page'] = 20;
		$config['total_rows'] = count($this->Timekeeping->get_all_movement_register_list(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['records'] = $this->Timekeeping->get_all_movement_register_list(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['is_show_button'] = "add";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('movement_registers/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add()
	{
		if ($_POST) {
			$person = explode("-", $this->input->post('person_id'));
			$person_id = $person[1];
			$person_type = $person[0];
			$data = array();
			$data['teacher_id'] = $this->input->post('teacher_id');
			$data['from_date'] = $this->input->post('from_date');
			$data['to_date'] = $this->input->post('to_date');
			$data['start_time'] = $this->input->post('outTime');
			$data['end_time'] = $this->input->post('inTime');
			$data['location'] = $this->input->post('txtLocation');
			$data['purpose'] = $this->input->post('txtPurpose');
			$data['attendance_auto'] = $this->input->post('txtAutoAttendance');
			if ($this->db->insert('tbl_teacher_staff_movement_registers', $data)) {
				if ($data['attendance_auto'] == 'Y') {
					if ($this->add_login_logout_data($data)) {
						$sdata['message'] =  $this->lang->line('add_success_message');
						$this->session->set_userdata($sdata);
						redirect("movement_registers/add");
					} else {
						$sdata['exception'] =  $this->lang->line('add_error_message') . " (Movement Register data Added but auto login/logout failed)";
						$this->session->set_userdata($sdata);
						redirect("movement_registers/add");
					}
				} else {
					$sdata['message'] =  $this->lang->line('add_success_message');
					$this->session->set_userdata($sdata);
					redirect("movement_registers/add");
				}
			} else {
				$sdata['exception'] = $this->lang->line('add_error_message');
					$this->session->set_userdata($sdata);
				redirect("movement_registers/add");
			}
		}
		$data = array();
		$data['title'] = 'Movement Register';
		$data['is_show_button'] = 'index';
		$data['heading_msg'] = "Movement Register";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['teachers'] = $this->db->query("SELECT id,name,teacher_code FROM `tbl_teacher` ORDER BY id")->result_array();
	    $data['maincontent'] = $this->load->view('movement_registers/add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add_login_logout_data($data)
	{
		$teacher_id = $data['teacher_id'];
		$teacher_info = $this->db->where('id', $teacher_id)->get('tbl_teacher')->result_array();
		$process_code = $teacher_info[0]['process_code'];

		$teacher_attendance_info = $this->db->where('teacher_id', $teacher_id)->get('tbl_teacher_attendance_settings')->row();
		if(empty($teacher_attendance_info)){
			return false;
		}

		$begin = new DateTime($data['from_date']);
		$end   = new DateTime($data['to_date']);

		for($i = $begin; $i <= $end; $i->modify('+1 day')){
			$ldata = array();
			$ldata['teacher_id'] = $teacher_id;
			$ldata['login_time'] = $teacher_attendance_info->in_time;
			$ldata['logout_time'] =  $teacher_attendance_info->out_time;
			$ldata['expected_login_time'] = $teacher_attendance_info->in_time;
			$ldata['expected_logout_time'] = $teacher_attendance_info->out_time;
			$ldata['process_code'] = $process_code;
			$ldata['date'] = $i->format("Y-m-d");
			$ldata['is_manual_login'] = '1';
			$ldata['flexible_min_for_late'] = $teacher_attendance_info->flexible_min_for_late;
			$this->db->insert('tbl_teacher_staff_logins', $ldata);
			return true;
		}
	}

	public function delete($id)
	{
		$this->db->delete('tbl_ba_income_category', array('id' => $id));
		$sdata['message'] = $this->lang->line('delete_success_message');
		$this->session->set_userdata($sdata);
		redirect("income_categories/index");
	}
}
