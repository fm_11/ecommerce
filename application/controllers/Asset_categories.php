<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Asset_categories extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Account', 'Admin_login'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = "Asset Categories";
        $data['heading_msg'] = "Asset Categories";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['is_show_button'] = "add";
        $data['asset_category'] = $this->db->query("SELECT * FROM tbl_asset_category")->result_array();
        $data['maincontent'] = $this->load->view('asset_categories/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['code'] = $_POST['code'];
            $data['opening_balance'] = $_POST['opening_balance'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->insert("tbl_asset_category", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('asset_categories/add');
        }
        $data = array();
        $data['title'] = "Asset Categories";
        $data['heading_msg'] = "Asset Categories";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('asset_categories/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $id = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['code'] = $_POST['code'];
            $data['opening_balance'] = $_POST['opening_balance'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->where('id', $id);
            $this->db->update('tbl_asset_category', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('asset_categories/index');
        }
        $data = array();
        $data['title'] = "Asset Categories";
        $data['heading_msg'] = "Asset Categories";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['asset_category_info'] = $this->db->query("SELECT * FROM tbl_asset_category WHERE `id` = '$id'")->result_array();
        $data['maincontent'] = $this->load->view('asset_categories/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->db->delete('tbl_asset_category', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("asset_categories/index");
    }
}
