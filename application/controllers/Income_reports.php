<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Income_reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $this->load->library('numbertowords');
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");
            $income_category_id = $this->input->post("income_category_id");
            $cost_center_id = $this->input->post("cost_center_id");

            $data['from_date'] = $this->input->post("from_date");
            $data['to_date'] = $this->input->post("to_date");
            $data['income_category_id'] = $this->input->post("income_category_id");
            $data['cost_center_id'] = $this->input->post("cost_center_id");

            $category_where = "";
            if ($income_category_id != 'all') {
                $category_where = " AND dd.`income_category_id` = '$income_category_id' ";
            }

            $cost_center_where = "";
            if ($cost_center_id != 'all') {
                $cost_center_where = " AND dd.`cost_center_id` = '$cost_center_id' ";
            }

            $data['idata'] = $this->db->query("SELECT dd.*,ic.`name` AS category_name, dm.`name` AS deposit_method_name
          ,cc.`name` as cost_center_name,d.`date`
          FROM `tbl_ba_deposit_details` AS dd
INNER JOIN `tbl_ba_income_category` AS ic ON dd.`income_category_id` = ic.`id`
INNER JOIN `tbl_ba_deposit_method` AS dm ON dd.`deposit_method_id` = dm.`id`
INNER JOIN `tbl_cost_center` AS cc ON dd.`cost_center_id` = cc.`id`
INNER JOIN `tbl_ba_deposit` AS d ON d.`id` = dd.`deposit_id`
WHERE `deposit_id` IN (SELECT id FROM `tbl_ba_deposit` WHERE `date` BETWEEN '$from_date' AND '$to_date') $cost_center_where $category_where")->result_array();
            $data['report'] = $this->load->view('income_reports/generate_basic_acc_income_report', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            $data['is_pdf'] = 1;
            //Dom PDF
            $this->load->library('mydompdf');
            $html = $this->load->view('income_reports/generate_basic_acc_income_report', $data, true);
            $this->mydompdf->createPDF($html, 'IncomeReport', true);
        //Dom PDF
        } else {
            $data['title'] = $this->lang->line('income') . ' ' . $this->lang->line('report');
            $data['heading_msg'] = $this->lang->line('income') . ' ' . $this->lang->line('report');
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['income_category'] = $this->db->query("SELECT * FROM `tbl_ba_income_category`")->result_array();
            $data['cost_center'] = $this->db->query("SELECT * FROM `tbl_cost_center`")->result_array();
            $data['maincontent'] = $this->load->view('income_reports/basic_acc_income_report', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
