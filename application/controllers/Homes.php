<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Homes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','E_commerce','Product'));
      //  $this->load->library('session');
        $this->load->library('session', 'cart');
    }


	function counter(){
		$data = array();
		$data['online'] = 2;
		$data['day_value'] = 5;
		$data['yesterday_value'] = 3;
		$data['week_value'] = 150;
		$data['month_value'] = 500;
		$data['year_value'] = 1002;
		$data['all_value'] = 1002;
		$data['record_value'] = 50;
		$data['record_date'] = date('Y-m-d');
		return $data;
	}


   public function order_track()
   {
      $order_no = $this->input->post('order_no');
      $data = array();
      $data['customer_info'] = $this->db->query("SELECT o.*,c.`name`,c.`address`,c.`phone`,c.`email` FROM `tbl_orders` AS o
INNER JOIN `tbl_customer` AS c ON c.`id` = o.`customer_id`
WHERE `order_code` = '$order_no'")->row();
      if(empty($data['customer_info'])){
        $data['orders'] = array();
      }else{
        $order_id = $data['customer_info']->id;
        $data['orders'] = $this->db->query("SELECT o.*,p.`product_name`,p.`image_path_1` FROM `tbl_order_items` AS o
LEFT JOIN `tbl_product_info` AS p ON p.`id` = o.`product_id` WHERE o.`order_id` = $order_id;")->result_array();
      }
      $data['cart_list'] = $this->cart->contents();
      $data['is_slide_open'] = 0;
      $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
      $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
      $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $data['main_content'] = $this->load->view('homes/order_track', $data, true);
      $this->load->view('homes/index', $data);
   }


    public function index() {
        $data = array();
        $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
        $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
        $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
      //  $data['digital_content'] = $this->load->view('homes/sources/digital_content', $data, true);
        $data['cart_list'] = $this->cart->contents();
        $cond = array();
        $data['all_product_list'] = $this->Product->get_all_product_info_list(16, 0, $cond);

        $cond = array();
        $cond['hot_deal'] = 1;
        $data['hot_deal_product_list']= $this->Product->get_all_product_info_list(10, 0, $cond);
        $data['is_slide_open'] = 1;
        $data['slide_images'] = $this->db->query("SELECT * FROM tbl_slide_images WHERE is_view = '1' ORDER BY id DESC")->result_array();
        $data['index_header'] = $this->load->view('homes/index_header', $data, true);
        $data['index_slider'] = $this->load->view('homes/index_slider', $data, true);
        $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
        $data['main_content'] = $this->load->view('homes/main_content', $data, true);
        $this->load->view('homes/index', $data);
    }


    function contact(){
      $data = array();
      $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
      $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
      $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
      $data['cart_list'] = $this->cart->contents();
      $data['is_slide_open'] = 0;
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['index_slider'] = $this->load->view('homes/index_slider', $data, true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $data['main_content'] = $this->load->view('homes/contact', $data);
      $this->load->view('homes/index', $data);
    }


    public function all_product()
    {
      //echo $_GET['category_id'];
    //  die;
      $data = array();
      $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
      $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
      $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");

      $data['cart_list'] = $this->cart->contents();
      $data['is_slide_open'] = 0;

      $cond = array();
      if($_POST){
        $category_id = $this->input->post("category_id");
  			$product_name = $this->input->post("product_name");
        $sdata = array();
        if($category_id != 'all'){
            $sdata['category_id'] = $category_id;
            $cond['category_id'] = $category_id;
        }else{
          $sdata['category_id'] = '';
        }
        if($product_name != ''){
            $sdata['product_name'] = $product_name;
            $cond['product_name'] = $product_name;
        }else{
            $sdata['product_name'] = '';
        }
  			$this->session->set_userdata($sdata);
      }else{
        if(isset($_GET['sub_category_id'])){
          $cond['sub_category_id'] = $_GET['sub_category_id'];
        }else{
          if(isset($_GET['category_id'])){
            $cond['category_id'] = $_GET['category_id'];
          }else{
            $category_id = $this->session->userdata('category_id');
            $product_name = $this->session->userdata('product_name');
            if($category_id != 'all'){
                $cond['category_id'] = $category_id;
            }
            if($product_name != ''){
                $cond['product_name'] = $product_name;
            }
          }
        }
      }

      $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      $this->load->library('pagination');
      $config['base_url'] = base_url() . "homes/all_product/";
      $config['per_page'] = 20;
      $config["uri_segment"] = 3;
      $config['total_rows'] = count($this->Product->get_all_product_info_list(0, 0, $cond));
      if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['first_url'] = $config['base_url'].'?'.http_build_query($_GET);
      $this->pagination->initialize($config);
      $data['products'] = $this->Product->get_all_product_info_list($config['per_page'], $page, $cond);

      // echo '<pre>';
      // print_r($data['products']);
      // die;
      // echo $this->db->last_query();
      // die;
      $data['counter'] = $page;
      $data['links'] = $this->pagination->create_links();
      $data['all_sub_category_list']=$this->E_commerce->get_all_product_sub_category_list();

      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['index_slider'] = $this->load->view('homes/index_slider', $data, true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $data['main_content'] = $this->load->view('homes/product_list', $data, true);
      $this->load->view('homes/index', $data);
    }


    public function details($id)
    {
      $data = array();
      $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
      $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
      $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
      $data['cart_list'] = $this->cart->contents();
      $data['is_slide_open'] = 0;
      $data['product_details'] = $this->Product->get_product_info_by_id($id);
      // echo '<pre>';
      // print_r($data['product_details']);
      // die;
      $data['all_sub_category_list'] = $this->E_commerce->get_all_product_sub_category_list();
      $data['related_product_list'] = $this->db->query("SELECT p.* FROM tbl_product_info AS p
INNER JOIN `tbl_product_category_details` AS pd ON pd.`product_info_id` = p.`id`
WHERE pd.`sub_category_id` = $id;")->result_array();
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['index_slider'] = $this->load->view('homes/index_slider', $data, true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $data['main_content'] = $this->load->view('homes/product_list_details', $data, true);
      $this->load->view('homes/index', $data);
    }



    function AboutInfo(){
        $data = array();
        $data['cart_list'] = $this->cart->contents();
        $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
        $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
        $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");

        $data['index_header'] = $this->load->view('homes/index_header', $data, true);
        $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
        $this->load->view('homes/about', $data);
    }


    function product_details(){
      $data = array();
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/product_details', $data);
    }
    function list_products(){
      $data = array();
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/list_products', $data);
    }
    function shopping_cart(){
      $data = array();
      $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
      $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
      $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/shopping_cart', $data);
    }
    function payment_methods(){
      $data = array();
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/payment_methods', $data);
    }
    function confirmation(){
      $data = array();
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/confirmation', $data);
    }


    function delivery_methods(){
      $data = array();
      $data['cart_list'] = $this->cart->contents();
      $data['is_slide_open'] = 0;
      $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
      $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
      $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);
      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $data['main_content'] = $this->load->view('homes/delivery_methods', $data, true);
      $this->load->view('homes/index', $data);
    }


    function checkout_successful(){
      $data = array();
      $data['cart_list'] = $this->cart->contents();
      $data['main_menu_category_list']=$this->E_commerce->get_all_category_list_by_type("m");
      $data['hot_deal_category_list']=$this->E_commerce->get_all_category_list_by_type("h");
      $data['all_category_list']=$this->E_commerce->get_all_category_list_by_type("all");
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/checkout_successful', $data);
    }
    function blog(){
      $data = array();
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/blog', $data);
    }
    function blog_details(){
      $data = array();
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/blog_details', $data);
    }
    function login_form(){
      $data = array();
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/login_form', $data);
    }
    function Error404(){
      $data = array();
      $data['index_header'] = $this->load->view('homes/index_header', $data, true);

      $data['index_footer'] = $this->load->view('homes/index_footer', $data, true);
      $this->load->view('homes/error404', $data);
    }

}
