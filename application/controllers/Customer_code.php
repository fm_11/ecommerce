<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Customer_code extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message','E_commerce','common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('customer_code');
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('customer_code/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->E_commerce->get_customer_code_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['customer'] = $this->E_commerce->get_customer_code_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('customer_code/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['customer_header_code'] = $_POST['customer_header_code'];
            $data['customer_footer_code'] = $_POST['customer_footer_code'];
            $this->db->insert("tbl_customer_code", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('customer_code/index');
        }
        $data = array();
		    $data['title'] = $this->lang->line('customer_code');
        $data['action'] = 'add';
	      $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('customer_code/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          $data = array();
          $data['id'] = $_POST['id'];
          $data['customer_header_code'] = $_POST['customer_header_code'];
          $data['customer_footer_code'] = $_POST['customer_footer_code'];
          $this->db->where('id', $data['id']);
          $this->db->update('tbl_customer_code', $data);
          $sdata['message'] = $this->lang->line('add_success_message');
          $this->session->set_userdata($sdata);
          redirect('customer_code/index');
        }
        $data = array();
		    $data['title'] = $this->lang->line('customer_code');
        $data['action'] = 'edit/' . $id;
        $data['is_show_button'] = "index";
        $data['row_data'] = $this->E_commerce->read_customer_code_info($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('customer_code/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->E_commerce->delete_customer_code_info($id);
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("customer_code/index");
    }
}
