<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Teacher_staff_logins extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Teacher_info', 'Timekeeping','Message','Admin_login'));
		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}


	public function index()
	{
		$data = array();
		if($_POST){
			$category_id = $this->input->post("category_id");
			$date = $this->input->post("date");

			$cdata['category_id'] = $category_id;
			$cdata['date'] = $date;
			$this->session->set_userdata($cdata);

			$data['category_id'] = $category_id;
			$data['date'] = $date;
			$teachers = $this->db->query("SELECT t.`id`,t.`name`,t.`teacher_code`,t.`process_code`,
p.`name` AS teacher_post, l.`login_time`, l.`logout_time`
FROM `tbl_teacher` AS t 
LEFT JOIN `tbl_teacher_post` AS p ON p.`id` = t.`post`
LEFT JOIN `tbl_teacher_staff_logins` AS l ON l.`teacher_id` = t.`id` AND l.`date` = '$date'
WHERE t.`status` = '1' AND t.`category_id` = '$category_id' ORDER BY p.`shorting_order` ASC;")->result_array();

			if(empty($teachers)){
				$sdata['exception'] = "Data Not Found";
				$this->session->set_userdata($sdata);
				redirect("teacher_staff_logins/index");
			}

			$teacher_setting_data =  $this->Timekeeping->getAttendanceTimeForAllTeacherStaff();

			$teachers_array = array();
			$i = 0;
			foreach ($teachers as $row){
				$teachers_array[$i]['id'] = $row['id'];
				$teachers_array[$i]['name'] = $row['name'];
				$teachers_array[$i]['teacher_code'] = $row['teacher_code'];
				$teachers_array[$i]['teacher_post'] = $row['teacher_post'];
				$teachers_array[$i]['process_code'] = $row['process_code'];
				$teachers_array[$i]['login_time'] = $row['login_time'];
				$teachers_array[$i]['logout_time'] = $row['logout_time'];

				if(isset($teacher_setting_data[$row['id']])){
					$teachers_array[$i]['expected_login_time'] = $teacher_setting_data[$row['id']]['in_time'];
				}else{
					$teachers_array[$i]['expected_login_time'] = '';
				}

				if(isset($teacher_setting_data[$row['id']])){
					$teachers_array[$i]['expected_logout_time'] = $teacher_setting_data[$row['id']]['out_time'];
				}else{
					$teachers_array[$i]['expected_logout_time'] = '';
				}

				if(isset($teacher_setting_data[$row['id']])){
					$teachers_array[$i]['flexible_min_for_late'] = $teacher_setting_data[$row['id']]['flexible_min_for_late'];
				}else{
					$teachers_array[$i]['flexible_min_for_late'] = 0;
				}
				$i++;
			}

//			echo '<pre>';
//			print_r($teachers_array);
//			die;

			$data['teachers'] = $teachers_array;
			$data['process_table'] = $this->load->view('teacher_staff_logins/process_table', $data, true);
		}
		$data['title'] = "Teacher/Staff Attendance";
		$data['heading_msg'] = "Teacher/Staff Attendance";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['categories'] = $this->db->query("SELECT * FROM tbl_teacher_category;")->result_array();
		$data['maincontent'] = $this->load->view('teacher_staff_logins/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	function data_save(){
		if($_POST){
			$total_row = $this->input->post("total_row");
			$date = $this->input->post("date");

			$is_present_sms = $this->input->post("is_present_sms");
			$is_absent_sms = $this->input->post("is_absent_sms");
			$present_msg = "";
			if (isset($is_present_sms) && $is_present_sms == 'on') {
				$present = $this->db->query("SELECT `template_body` FROM `tbl_message_template` WHERE `is_teacher_present_sms` = 1")->row();
				if (empty($present)) {
					$sdata['exception'] = "Please set present sms template first.";
					$this->session->set_userdata($sdata);
					redirect("teacher_staff_logins/index");
				}
				$present_msg = $present->template_body;
			}
			$absent_msg = "";
			if (isset($is_absent_sms) && $is_absent_sms == 'on') {
				$absent = $this->db->query("SELECT `template_body` FROM `tbl_message_template` WHERE `is_teacher_absent_sms` = 1")->row();
				if (empty($absent)) {
					$sdata['exception'] = "Please set absent sms template first.";
					$this->session->set_userdata($sdata);
					redirect("teacher_staff_logins/index");
				}
				$absent_msg = $absent->template_body;
			}


			$data = array();
			$i = 0;
			$row_no = 0;
			while ($i < $total_row){
				$is_absent = $this->input->post("is_absent_" . $i);
				$teacher_id = $this->input->post("teacher_id_" . $i);
				$this->db->delete('tbl_teacher_staff_logins', array('teacher_id' => $teacher_id, 'date' => $date));
				if($is_absent == ''){
					$data[$row_no]['date'] = $date;
					$data[$row_no]['teacher_id'] =  $teacher_id;
					$data[$row_no]['process_code'] =  $this->input->post("process_code_" . $i);
					$data[$row_no]['login_time'] = $this->input->post("login_time_" . $i);
					$data[$row_no]['logout_time'] = $this->input->post("logout_time_" . $i);
					$data[$row_no]['expected_login_time'] = $this->input->post("expected_login_time_" . $i);
					$data[$row_no]['expected_logout_time'] = $this->input->post("expected_logout_time_" . $i);
					$data[$row_no]['flexible_min_for_late'] =  $this->input->post("flexible_min_for_late_" . $i);
					$time1 = strtotime($data[$row_no]['expected_login_time']);
					$time2 = strtotime($data[$row_no]['login_time']);
					$time_def = ($time2-$time1)/60;
					$data[$row_no]['late_min'] = $time_def;
					$data[$row_no]['is_manual_login'] = '1';
					$row_no++;

					//present sms send
					if (isset($is_present_sms) && $is_present_sms == 'on' && $present_msg != '') {
						$teacher_info = $this->db->query("SELECT * FROM `tbl_teacher` WHERE `id` = $teacher_id")->row();
						if($this->Admin_login->validate_mobile_number($teacher_info->mobile)){
							$sms_info = array();
							$sms_info['message'] = "Date: " . $date . ", Name: " . $teacher_info->name . "::" . $present_msg;
							$sms_info['numbers'] =  $teacher_info->mobile;
							$this->Message->sms_send($sms_info);
						}
					}
					//present sms send
				}

				//absent sms send
				if (isset($is_absent_sms) && $is_absent_sms == 'on' && $absent_msg != '') {
					$teacher_info = $this->db->query("SELECT * FROM `tbl_teacher` WHERE `id` = $teacher_id")->row();
					if($this->Admin_login->validate_mobile_number($teacher_info->mobile)){
						$sms_info = array();
						$sms_info['message'] = "Date: " . $date . ", Name: " . $teacher_info->name . "::" . $absent_msg;
						$sms_info['numbers'] =  $teacher_info->mobile;
						$this->Message->sms_send($sms_info);
					}
				}
				//absent sms send

				$i++;
			}
			if($row_no > 0){
				$this->db->insert_batch('tbl_teacher_staff_logins', $data);
			}
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect("teacher_staff_logins/index");
		}
	}
}
