<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Mark_input_blank_sheets extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student','Admin_login'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Mark Input Blank Sheet';
		$data['heading_msg'] = "Mark Input Blank Sheet";
		if ($_POST) {
			$exam_id = $_POST['exam_id'];
			$class_shift_section_id = $_POST['class_shift_section_id'];
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];
			$group_id = $_POST['group_id'];
			$subject_id = $_POST['subject_id'];
			$year = $_POST['year'];

			$data['posted_exam_id'] = $exam_id;
			$data['posted_class_shift_section_id'] = $class_shift_section_id;
			$data['posted_group_id'] = $group_id;
			$data['posted_subject_id'] = $subject_id;
			$data['posted_year'] = $year;

			$cdata['m_year'] = $year;
			$cdata['m_exam_id'] = $exam_id;
			$cdata['m_class_shift_section_id'] = $class_shift_section_id;
			$cdata['m_group_id'] = $group_id;
			$cdata['m_subject_id'] = $subject_id;
			$this->session->set_userdata($cdata);

			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$Info = array();
			$Info['school_name'] = $SchoolInfo[0]['school_name'];
			$Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
			$data['HeaderInfo'] = $Info;

			$class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
			$data['class_name'] = $class_info[0]['name'];

			$shift_info = $this->db->query("SELECT name FROM `tbl_shift` WHERE id ='$shift_id'")->result_array();
			$data['shift_name'] = $shift_info[0]['name'];

			$data['exam_info'] = $this->db->query("SELECT * FROM `tbl_exam` WHERE id = '$exam_id'")->result_array();
			$exam_year = $data['exam_info'][0]['year'];

			if ($subject_id != '') {
				$data['subject_info'] = $this->db->query("SELECT * FROM `tbl_subject` WHERE id = '$subject_id'")->result_array();
			} else {
				$data['subject_info'] = null;
			}

			$group_where = "";
			if ($group_id != 'all') {
				$group_info = $this->db->query("SELECT name FROM `tbl_student_group` WHERE id ='$group_id'")->result_array();
				$data['group_name'] = $group_info[0]['name'];
				$group_where = " AND s.`group` = '$group_id' ";
			} else {
				$data['group_name'] = "All";
			}

			$section_where = "";
			if ($subject_id != 'all') {
				$section_info = $this->db->query("SELECT name FROM `tbl_section` WHERE id ='$section_id'")->result_array();
				$data['section_name'] = $section_info[0]['name'];
				$section_where = " AND s.`section_id` = '$section_id' ";
			} else {
				$data['section_name'] = "All";
			}

			$subject_where = "";
			if ($subject_id != '') {
				$subject_where = " AND s.`id` IN(SELECT st.student_id FROM `tbl_student_wise_subject` AS st WHERE st.year = '$exam_year' AND st.subject_id = '$subject_id') ";
			}

			$data['info'] = $this->db->query("SELECT * FROM `tbl_student` AS s WHERE s.`class_id` = '$class_id'
                            $group_where  $section_where
                            AND  s.`shift_id` = '$shift_id' AND  s.`status` = 1 AND s.`year` = '$exam_year'
                            $subject_where
                            ORDER BY ABS(s.`roll_no`) ASC")->result_array();
			//echo '<pre>';
			//print_r($data['info']); die;
			$data['is_excel'] = 0;
			if (isset($_POST['excel_download'])) {
				$data['is_excel'] = 1;
			}


			$data['report'] = $this->load->view('mark_input_blank_sheets/mark_input_blank_sheet_table', $data, true);
		}
		if (isset($_POST['excel_download'])) {
			$this->load->view('mark_input_blank_sheets/mark_input_blank_sheet_table_for_excel', $data);
			//die;
		} elseif  (isset($_POST['pdf_download'])) {
			$data['is_pdf'] = 1;
			//Dom PDF
			$this->load->library('mydompdf');
			$html = $this->load->view('mark_input_blank_sheets/mark_input_blank_sheet_table', $data, true);
			$this->mydompdf->createPDF($html, 'StudentSummeryReport', true, 'A4', 'portrait');
			//Dom PDF
		} else {

			$data['m_exam_id'] = $this->session->userdata('m_exam_id');
			$data['class_shift_section_id'] = $this->session->userdata('m_class_shift_section_id');
			$data['m_group_id'] = $this->session->userdata('m_group_id');
			$data['m_subject_id'] = $this->session->userdata('m_subject_id');
			$data['year'] = $this->session->userdata('m_year');

			$class_id = '';
			if ($data['class_shift_section_id'] != '') {
				$class_shift_section_arr = explode("-", $data['class_shift_section_id']);
				$class_id  = $class_shift_section_arr[0];
				// $shift_id = $class_shift_section_arr[1];
				// $section_id = $class_shift_section_arr[2];
			}

			if ($class_id != '' && $data['m_exam_id'] != '') {
				$m_class_id = $class_id;
				$m_exam_id = $data['m_exam_id'];
				$exam_info =  $this->db->query("SELECT * FROM tbl_exam WHERE id = '$m_exam_id'")->result_array();
				$exam_type_id = 0;
				if (!empty($exam_info)) {
					$exam_type_id = $exam_info[0]['exam_type_id'];
				}

				//  echo $exam_type_id;
				//  die;
				$data['subjects'] = $this->db->query("SELECT
        c.`subject_id`,
        s.`name`, s.`code`
        FROM
        `tbl_class_wise_subject` AS c
        LEFT JOIN `tbl_subject` AS s
        ON s.`id` = c.`subject_id`
        WHERE c.`exam_type_id` = '$exam_type_id' AND c.`class_id` = '$m_class_id' GROUP BY c.`subject_id` ORDER BY c.`order_number`")->result_array();
			} else {
				$data['subjects'] = array();
			}

			if ($data['m_exam_id'] != '') {
				$year = $data['year'];
				$data['ExamList'] = $this->db->query("SELECT * FROM `tbl_exam` WHERE `year` = '$year' ORDER BY exam_order")->result_array();
			} else {
				$data['ExamList'] = array();
			}


			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['GroupList'] = $this->Admin_login->getGroupList();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('mark_input_blank_sheets/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}
}
