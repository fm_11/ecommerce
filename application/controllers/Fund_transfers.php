<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Fund_transfers extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Account', 'Admin_login'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
		$data = array();
		$cond = array();
		if ($_POST) {
			$from_date = $this->input->post("from_date");
			$to_date = $this->input->post("to_date");

			$sdata['from_date'] = $from_date;
			$sdata['to_date'] = $to_date;

			$this->session->set_userdata($sdata);
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
		} else {
			$from_date = $this->session->userdata('from_date');
			$to_date = $this->session->userdata('to_date');
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
		}
        $data['title'] = $this->lang->line('fund') . ' ' . $this->lang->line('transfer');
        $data['heading_msg'] = $this->lang->line('fund') . ' ' . $this->lang->line('transfer');
        $this->load->library('pagination');
        $config['base_url'] = site_url('fund_transfers/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Account->get_all_fund_transfer_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['tdata'] = $this->Account->get_all_fund_transfer_info(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('fund_transfers/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            if ($_POST['from_asset_category_id'] == $_POST['to_asset_category_id']) {
                $sdata['exception'] = $this->lang->line('add_error_message') . " (Fund transfer is not possible in the same deposit method.)";
                $this->session->set_userdata($sdata);
                redirect('fund_transfers/add');
            }
            $data['from_asset_category_id'] = $_POST['from_asset_category_id'];
            $data['to_asset_category_id'] = $_POST['to_asset_category_id'];
            $data['date'] = $_POST['date'];
            $data['amount'] = $_POST['amount'];
            $data['description'] = $_POST['description'];
            $data['entry_date'] = date('Y-m-d H:i:s');
            $user_info = $this->session->userdata('user_info');
            $data['user_id'] = $user_info[0]->id;
            $this->db->insert("tbl_ba_fund_transfer", $data);
            $reference_id =  $this->db->insert_id();

            //asset entry
            $user_by_from_asset_head = $this->Admin_login->getAssetHeadWiseUser($_POST['from_asset_category_id']);
            $transaction_from_user_id = 0;
            if (!empty($user_by_from_asset_head)) {
                $transaction_from_user_id =  $user_by_from_asset_head[0]['user_id'];
            }

            $assetdata = array();
            $assetdata['user_id'] = $transaction_from_user_id;
            $assetdata['asset_id'] = $_POST['from_asset_category_id'];
            $assetdata['amount'] = $_POST['amount'];
            $assetdata['type'] = 'O';
            $assetdata['from_transaction_type'] = 'FT';
            $assetdata['date'] = $_POST['date'];
            $assetdata['reference_id'] = $reference_id;
            $assetdata['remarks'] = 'Fund Transfer (From)';
            $assetdata['added_by'] = $user_info[0]->id;
            $this->db->insert('tbl_user_wise_asset_transaction', $assetdata);

            $user_by_to_asset_head = $this->Admin_login->getAssetHeadWiseUser($_POST['to_asset_category_id']);
            $transaction_to_user_id = 0;
            if (!empty($user_by_to_asset_head)) {
                $transaction_to_user_id =  $user_by_to_asset_head[0]['user_id'];
            }
            $assetdata = array();
            $assetdata['user_id'] = $transaction_to_user_id;
            $assetdata['asset_id'] = $_POST['to_asset_category_id'];
            $assetdata['amount'] = $_POST['amount'];
            $assetdata['type'] = 'I';
            $assetdata['from_transaction_type'] = 'FT';
            $assetdata['date'] = $_POST['date'];
            $assetdata['reference_id'] = $reference_id;
            $assetdata['remarks'] = 'Fund Transfer (To)';
            $assetdata['added_by'] = $user_info[0]->id;
            $this->db->insert('tbl_user_wise_asset_transaction', $assetdata);
            //asset entry


            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('fund_transfers/add');
        }
        $data = array();
        $data['title'] = $this->lang->line('fund') . ' ' . $this->lang->line('transfer') . ' ' . $this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('fund') . ' ' . $this->lang->line('transfer') . ' ' . $this->lang->line('add');
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['asset_category'] = $this->db->query("SELECT * FROM tbl_asset_category")->result_array();
        $data['maincontent'] = $this->load->view('fund_transfers/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function view($id)
    {
        $this->load->library('numbertowords');
        $data = array();
        $data['title'] = 'Fund Transfer Voucher';
        $data['heading_msg'] = "Fund Transfer Voucher";
        $data['school_info'] = $this->Admin_login->get_contact_info();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['fund_transfer_info'] = $this->db->query("SELECT acf.`name` AS from_asset_head, act.`name` AS to_asset_head, u.`name` AS user_name,
ft.* FROM `tbl_ba_fund_transfer` AS ft INNER JOIN `tbl_asset_category` AS acf ON acf.`id` = ft.`from_asset_category_id`
INNER JOIN `tbl_asset_category` AS act ON act.`id` = ft.`to_asset_category_id`
LEFT JOIN `tbl_user` AS u ON u.`id` = ft.`user_id`
WHERE ft.`id` = $id")->result_array();
        // echo '<pre>';
        // print_r($data['deposit_info']);
        //    print_r($data['deposit_details_info']);
        //  die;
        $this->load->view('fund_transfers/view', $data);
    }

    public function delete($id)
    {
        $this->db->delete('tbl_ba_fund_transfer', array('id' => $id));
        $this->db->delete('tbl_user_wise_asset_transaction', array('reference_id' => $id,'from_transaction_type' => 'FT'));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("fund_transfers/index");
    }
}
