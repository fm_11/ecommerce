<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class MenuSwitcher extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function switchMenu($menuType = "", $menuTypeColor = "")
    {
        //echo $menuTypeColor;
        //sdie;
        $menuType = ($menuType != "") ? $menuType : "top";
        $this->session->set_userdata('site_menu', $menuType);

        $menuTypeColor = ($menuTypeColor != "") ? $menuTypeColor : "dark";
        $this->session->set_userdata('site_menu_color', $menuTypeColor);
        redirect($_SERVER['HTTP_REFERER']);
    }
}
