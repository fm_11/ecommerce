<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Waiver_settings extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = "Waiver Setting";
        $data['heading_msg'] = "Waiver  Setting";
        $data['is_show_button'] = "add";
        $data['waiver_info'] = $this->db->query("SELECT * FROM tbl_fee_waiver_setting")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('waiver_settings/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
       if($_POST){
         $year = $_POST['year'];
         $waiver_info = $this->db->query("SELECT id FROM tbl_fee_waiver_setting WHERE year = '$year'")->result_array();
         if(!empty($waiver_info)){
           $sdata['exception'] = "Already configuration exists for this year";
           $this->session->set_userdata($sdata);
           redirect('waiver_settings/index');
         }
         $data = array();
         $data['year'] = $_POST['year'];
         $data['applicable_month'] = $_POST['applicable_month'];
         $data['applicable_month_for_scholarship'] = $_POST['applicable_month_for_scholarship'];
         $this->db->insert("tbl_fee_waiver_setting", $data);
         $sdata['message'] = $this->lang->line('add_success_message');
         $this->session->set_userdata($sdata);
         redirect('waiver_settings/index');
       }else{
         $data = array();
         $data['title'] = "Waiver Setting";
         $data['heading_msg'] = "Waiver  Setting";
         $data['is_show_button'] = "index";
         $data['years'] = $this->Admin_login->getYearList(0, 0);
         $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
         $data['maincontent'] = $this->load->view('waiver_settings/add', $data, true);
         $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
       }
    }

    public function edit($id='')
    {
       if($_POST){
         $data = array();
         $data['id'] = $_POST['id'];
         $data['year'] = $_POST['year'];
         $data['applicable_month'] = $_POST['applicable_month'];
         $data['applicable_month_for_scholarship'] = $_POST['applicable_month_for_scholarship'];
         $this->db->where('id', $data['id']);
         $this->db->update('tbl_fee_waiver_setting', $data);
         $sdata['message'] = $this->lang->line('edit_success_message');
         $this->session->set_userdata($sdata);
         redirect('waiver_settings/index');
       }else{
         $data = array();
         $data['title'] = "Waiver Setting";
         $data['heading_msg'] = "Waiver  Setting";
         $data['is_show_button'] = "index";
         $data['waiver_info'] = $this->db->query("SELECT * FROM tbl_fee_waiver_setting WHERE id = '$id'")->result_array();
         $data['years'] = $this->Admin_login->getYearList(0, 0);
         $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
         $data['maincontent'] = $this->load->view('waiver_settings/edit', $data, true);
         $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
       }
    }

    public function delete($id)
    {
        $waiver_info = $this->db->query("SELECT year FROM tbl_fee_waiver_setting WHERE id = '$id'")->result_array();
        $year = $waiver_info[0]['year'];
        $collection_info = $this->db->query("SELECT id FROM tbl_fee_collection WHERE year = '$year'")->result_array();
        // echo '<pre>';
        // print_r($collection_info);
        // die;
        if(!empty($collection_info)){
          $sdata['exception'] = "Already atudent fees collection exists for this year";
          $this->session->set_userdata($sdata);
          redirect('waiver_settings/index');
        }
        $this->db->delete('tbl_fee_waiver_setting', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("waiver_settings/index");
    }

}
