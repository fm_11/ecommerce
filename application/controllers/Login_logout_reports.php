
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login_logout_reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('login') . '/' . $this->lang->line('logout') . ' ' . $this->lang->line('report');
        $data['heading_msg'] = $this->lang->line('login') . '/' . $this->lang->line('logout') . ' ' . $this->lang->line('report');
        if ($_POST) {
            $date = $_POST['date'];
            $cond = array();
            $cond['date'] = $date;
			$data['time_keeping_info'] = $this->Timekeeping->get_all_teacher_staff_timekeeping_list(0, 0, $cond);
//            echo '<pre>';
//            print_r($data['time_keeping_info']);
//            die;
            $data['date'] = $date;
            $SchoolInfo = $this->db->query("SELECT `school_name`,`eiin_number`,`address` FROM `tbl_contact_info`")->result_array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
			$Info['address'] = $SchoolInfo[0]['address'];
            $data['HeaderInfo'] = $Info;
            $data['report_view'] = $this->load->view('login_logout_reports/report', $data, true);
        }

        if (isset($_POST['pdf_download'])) {
            $data['is_pdf'] = 1;
            //Dom PDF
            $this->load->library('mydompdf');
            $html = $this->load->view('login_logout_reports/report', $data, true);
            $this->mydompdf->createPDF($html, 'login_logout_report', true, 'A4', 'portrait');
            //Dom PDF
        }
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('login_logout_reports/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
