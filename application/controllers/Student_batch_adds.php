<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Student_batch_adds extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student', 'Message','Admin_login','common/custom_methods_model','Student_fee'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		//For Excel Read
		require_once APPPATH . 'third_party/PHPExcel.php';
		$this->excel = new PHPExcel();
		//For Excel Read


		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		//echo '<pre>'; print_r($user_info); die;
		$this->SOFTWARE_START_YEAR = $user_info[0]->config_info[0]['software_start_year'];
		$this->notification = array();
	}


	public function index()
	{
		$data = array();
		if ($_POST) {
			$class_shift_section_id = $this->input->post('class_shift_section_id');
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$data['class_shift_section_id'] = $class_shift_section_id;
			$class_id = $class_shift_section_arr[0];
			$shift = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$data['class_id'] = $class_id;
			$data['shift_id'] = $shift;
			$data['section_id'] = $section_id;


			$group = $this->input->post('txtGroup', true);
			$data['group_id'] = $group;

			$category_id = $this->input->post('category_id', true);
			$data['category_id'] = $category_id;

			$year = $this->input->post('txtYear', true);
			$data['year'] = $year;

			$id_segment_year = $this->input->post('txtIdSegmentYear', true);
			$data['id_segment_year'] = $id_segment_year;

			$isAutoID = $this->input->post('isAutoID', true);
			$data['isAutoID'] = $isAutoID;

			$DataTypeFormat = $this->input->post('DataTypeFormat', true);
			$data['DataTypeFormat'] = $DataTypeFormat;
			//echo $DataTypeFormat;
			//die;
			$photo_name = "default.png";

			$IsCellNum = 0;
			if ($isAutoID == 'Y') {
				$IsCellNum = 1;
			}
			//For Excel Upload
			$configUpload['upload_path'] = FCPATH . 'uploads/excel/';
			$configUpload['allowed_types'] = 'xls|xlsx|csv';
			$configUpload['max_size'] = '15000';
			$this->load->library('upload', $configUpload);
			$this->upload->do_upload('txtFile');
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name']; //uploded file name
			$extension = $upload_data['file_ext'];    // uploded file extension
			//echo FCPATH;
			//echo '<pre>'; print_r($_FILES); die;
			if ($extension == '.xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
			} else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
			}

			//Set to read only
			$objReader->setReadDataOnly(true);
			//Load excel file
			$objPHPExcel = $objReader->load(FCPATH . 'uploads/excel/' . $file_name);
			$totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			//loop from first data untill last data
			$returnDataForShow = array();
			for ($i = 2; $i <= $totalrows; $i++) {
				if ($DataTypeFormat == 'SF') {
					$roll_no = trim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");
					$name = trim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue(), " ");
					if ($roll_no == '' || $name == '') {
						break;
					}

					$gender = trim($objWorksheet->getCellByColumnAndRow(2, $i)->getValue(), " ");
					$religion = trim($objWorksheet->getCellByColumnAndRow(3, $i)->getValue(), " ");
					$father_name = trim($objWorksheet->getCellByColumnAndRow(4, $i)->getValue(), " ");
					$mother_name = trim($objWorksheet->getCellByColumnAndRow(5, $i)->getValue(), " ");
					$guardian_mobile = trim($objWorksheet->getCellByColumnAndRow(6, $i)->getValue(), " ");

					if ($gender == 'Male') {
						$gender = 'M';
					} else {
						$gender = 'F';
					}

					$returnDataForShow[$i]['roll_no'] = $roll_no;
					$returnDataForShow[$i]['name'] = $name;
					$returnDataForShow[$i]['gender'] = $gender;
					$returnDataForShow[$i]['religion'] = $religion;
					$returnDataForShow[$i]['father_name'] = $father_name;
					$returnDataForShow[$i]['mother_name'] = $mother_name;
					$returnDataForShow[$i]['guardian_mobile'] = $guardian_mobile;

				} else {
					$name = ltrim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");
					if ($name == '') {
						break;
					}
					$name_bangla = trim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue());
					$process_code = trim($objWorksheet->getCellByColumnAndRow(2, $i)->getValue()); //Excel Column 1

					$roll_no = trim($objWorksheet->getCellByColumnAndRow(4 - $IsCellNum, $i)->getValue()); //Excel Column 3
					//  echo $roll_no;
					//  die;
					if ($isAutoID == 'Y') {
						$student_code = $this->getStudentCode($class_id, $group, $year, $id_segment_year);
					} else {
						$student_code = trim($objWorksheet->getCellByColumnAndRow(3, $i)->getValue()); //Excel Column 2
					}
					//echo $student_code;
					//die;
					$reg_no = trim($objWorksheet->getCellByColumnAndRow(5 - $IsCellNum, $i)->getValue()); //Excel Column 4
					$date_of_birth = $this->date_format_from_excel_date(trim($objWorksheet->getCellByColumnAndRow(6 - $IsCellNum, $i)->getValue()));
					$gender = trim($objWorksheet->getCellByColumnAndRow(7 - $IsCellNum, $i)->getValue());
					$date_of_admission = $this->date_format_from_excel_date(trim($objWorksheet->getCellByColumnAndRow(8 - $IsCellNum, $i)->getValue()));
					$father_name = trim($objWorksheet->getCellByColumnAndRow(9 - $IsCellNum, $i)->getValue());
					$father_nid = trim($objWorksheet->getCellByColumnAndRow(10 - $IsCellNum, $i)->getValue());
					$mother_name = trim($objWorksheet->getCellByColumnAndRow(11 - $IsCellNum, $i)->getValue());
					$mother_nid = trim($objWorksheet->getCellByColumnAndRow(12 - $IsCellNum, $i)->getValue());
					$present_address = trim($objWorksheet->getCellByColumnAndRow(13 - $IsCellNum, $i)->getValue());
					$permanent_address = trim($objWorksheet->getCellByColumnAndRow(14 - $IsCellNum, $i)->getValue());

					$guardian_mobile = trim($objWorksheet->getCellByColumnAndRow(15 - $IsCellNum, $i)->getValue());
					$second_guardian_mobile = trim($objWorksheet->getCellByColumnAndRow(16 - $IsCellNum, $i)->getValue());
					$student_mobile = trim($objWorksheet->getCellByColumnAndRow(17 - $IsCellNum, $i)->getValue());

					$birth_certificate_no = trim($objWorksheet->getCellByColumnAndRow(18 - $IsCellNum, $i)->getValue());
					$email = trim($objWorksheet->getCellByColumnAndRow(19 - $IsCellNum, $i)->getValue());
					$religion = trim($objWorksheet->getCellByColumnAndRow(20 - $IsCellNum, $i)->getValue());

					$blood_group_id = $this->get_blood_group_id_by_name(trim($objWorksheet->getCellByColumnAndRow(21 - $IsCellNum, $i)->getValue()));
					$allowed_for_timekeeping = trim($objWorksheet->getCellByColumnAndRow(22 - $IsCellNum, $i)->getValue());
					if ($allowed_for_timekeeping == "") {
						$allowed_for_timekeeping = 0;
					}
					$syn_date = $this->date_format_from_excel_date(trim($objWorksheet->getCellByColumnAndRow(22 - $IsCellNum, $i)->getValue()));


					//QR Code
					$qr_code_name = "s_" . (time() + $i) . "_" . date('Y-m-d') . '.png';
					$qr_code_data = "Name: " . $name . ', Student ID: ' . $student_code . ', Date of Birth: ' . $date_of_birth;
					if ($this->Admin_login->qr_code_generate($qr_code_data, $qr_code_name) != true) {
						$sdata['exception'] =  $this->lang->line('edit_error_message') . " , Error: QR Code generate failed ! Error Row Number: " . ($i - 1);
						$this->session->set_userdata($sdata);
						redirect("student_batch_adds/index");
					}
					//QR Code End
					$st_password = md5('123456');
					$data = array('name' => $name, 'name_bangla' => $name_bangla, 'class_id' => $class_id, 'section_id' => $section_id, 'group' => $group,'category_id' => $category_id, 'shift_id' => $shift,
						'process_code' => $process_code, 'birth_certificate_no' => $birth_certificate_no,'student_code' => $student_code,
						'roll_no' => $roll_no, 'reg_no' => $reg_no, 'date_of_birth' => $date_of_birth, 'gender' => $gender,
						'date_of_admission' => $date_of_admission, 'father_name' => $father_name, 'father_nid' => $father_nid,
						'mother_name' => $mother_name, 'mother_nid' => $mother_nid, 'present_address' => $present_address,
						'permanent_address' => $permanent_address, 'guardian_mobile' => $guardian_mobile, 'second_guardian_mobile' => $second_guardian_mobile, 'student_mobile' => $student_mobile,
						'password' => $st_password,'email' => $email, 'religion' => $religion, 'blood_group_id' => $blood_group_id,
						'allowed_for_timekeeping' => $allowed_for_timekeeping, 'syn_date' => $syn_date, 'year' => $year ,'photo' => $photo_name ,'qr_code' => $qr_code_name , 'data_entry_date' => date('Y-m-d'));

					$this->db->insert('tbl_student', $data);
				}
			}
			//For Excel Upload

			$filedel = PUBPATH . 'uploads/excel/' . $file_name;
			if (unlink($filedel)) {
				if ($DataTypeFormat != 'SF') {
					$sdata['message'] = $this->lang->line('add_success_message');
					$this->session->set_userdata($sdata);
					redirect("student_batch_adds/index");
				}
			}

//			echo '<pre>';
//			print_r($returnDataForShow);
//			die;

			$data['religions'] = $this->Admin_login->getReligionList();
			$data['returnDataForShow'] = $returnDataForShow;
			$data['data_table'] = $this->load->view('student_batch_adds/data_table', $data, true);
		}

		$data['title'] = $this->lang->line('student_add');
		$data['action'] = '';
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['categories'] = $this->Student->getStudentCategory();
		$data['class_section_shift_marge_list'] = $this->Admin_login->all_class_section_shift_marge_list();
		$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('student_batch_adds/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function excel_data_save(){
		if($_POST){
			$number_of_rows = $this->input->post('number_of_rows');
			$class_id = $this->input->post('class_id');
			$shift_id =  $this->input->post('shift_id');
			$section_id = $this->input->post('section_id');
			$group_id = $this->input->post('group_id');
			$category_id = $this->input->post('category_id');
			$year  = $this->input->post('year');
			$id_segment_year  = $this->input->post('id_segment_year');
			$i = 0;
			while ($i < $number_of_rows){
				$roll = $this->input->post('roll_' . $i);
				if($roll != ''){
					$data = array();
					$data['name_bangla'] = $this->input->post('name_bangla_' . $i, true);
					$data['name'] = $this->input->post('name_english_' . $i, true);
					$data['class_id'] = $class_id;
					$data['roll_no'] = $roll;
					$data['year'] = $year;
					$data['section_id'] = $section_id;
					$data['group'] = $group_id;
					$data['category_id'] = $category_id;
					$data['shift_id'] = $shift_id;
					$data['gender'] = $this->input->post('gender_' . $i, true);
					$data['religion'] = $this->input->post('religion_' . $i, true);
					$data['father_name'] = $this->input->post('fathers_name_' . $i, true);
					$data['mother_name'] = $this->input->post('mothers_name_' . $i, true);
					$data['guardian_mobile'] = $this->input->post('mobile_' . $i, true);
					$data['date_of_birth'] = NULL;
					$data['date_of_admission'] = NULL;
					$data['syn_date'] = NULL;
					$data['password'] = md5('123456');
					$data['student_code'] = $this->Student->getStudentCode($class_id, $group_id, $year, $id_segment_year);
					//QR Code
					$new_photo_and_qr_code_name = "s_" . time() . $i . "_" . date('Y-m-d');
					$qr_code_name = $new_photo_and_qr_code_name . '.png';
					$qr_code_data = "Name: " . $this->input->post('name_english_' . $i, true) . ', Student ID: ' . $data['student_code'] . ', Date of Birth: Not Found';
					if ($this->Admin_login->qr_code_generate($qr_code_data, $qr_code_name) != true) {
						$sdata['exception'] = $this->lang->line('add_error_message') . ", Error: QR Code generate failed ! ";
						$this->session->set_userdata($sdata);
						redirect("student_batch_adds/index");
					}
					$data['qr_code'] = $qr_code_name;
					//QR Code End

					$this->db->insert('tbl_student', $data);
				}
				$i++;
			}
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect("student_batch_adds/index");
		}
	}
}
