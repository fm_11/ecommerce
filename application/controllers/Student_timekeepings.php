
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Student_timekeepings extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Student', 'Message', 'Admin_login', 'Timekeeping','common/custom_methods_model'));
		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function add()
	{
		$data = array();
		if($_POST){
			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];
			$date = $this->input->post("date");
			$period_id = $this->input->post("period_id");

			$sdata['period_id'] = $period_id;
			$sdata['class_shift_section_id'] = $class_shift_section_id;
			$sdata['date'] = $date;
			$this->session->set_userdata($sdata);

			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['date'] = $date;
			$data['period_id'] = $period_id;

			$data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['shift_id'] = $shift_id;

			$data['student_info'] = $this->db->query("SELECT s.*,sl.`date`,sl.`login_status` AS process_status
FROM `tbl_student` AS s
LEFT JOIN `tbl_student_logins` AS sl ON (sl.`student_id` = s.`id` AND sl.`date` = '$date' AND sl.`period_id` = '$period_id')
WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' 
AND s.`shift_id` = '$shift_id' ORDER BY ABS(s.`roll_no`)")->result_array();
			$data['student_timekeeping_data'] = $this->load->view('student_timekeepings/student_timekeeping_form', $data, true);
		}

		$data['title'] = 'Student Timekeeping';
		$date = date('Y-m-d');
		$data['date'] = $date;
		$data['heading_msg'] = "Student Manual Login";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['class_periods'] = $this->db->query("SELECT * FROM tbl_class_period")->result_array();
		$data['maincontent'] = $this->load->view('student_timekeepings/add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add_student_login_data()
	{
		$user_info = $this->session->userdata('user_info');
		$user_id = $user_info[0]->id;
		$is_present_sms = $this->input->post("is_present_sms");
		$is_absent_sms = $this->input->post("is_absent_sms");
		$is_leave_sms = $this->input->post("is_leave_sms");
		if (isset($is_present_sms) && $is_present_sms == 'on') {
			$present = $this->db->query("SELECT `template_body` FROM `tbl_message_template` WHERE `is_present_sms` = 1")->row();
			if (empty($present)) {
				$sdata['exception'] = "Please present sms template set first.";
				$this->session->set_userdata($sdata);
				redirect("student_timekeepings/add");
			}
			$present_msg = $present->template_body;
		}

		if (isset($is_absent_sms) && $is_absent_sms == 'on') {
			$absent = $this->db->query("SELECT `template_body` FROM `tbl_message_template` WHERE `is_absent_sms` = 1")->row();
			if (empty($absent)) {
				$sdata['exception'] = "Please absent sms template template set first.";
				$this->session->set_userdata($sdata);
				redirect("student_timekeepings/add");
			}
			$absent_msg = $absent->template_body;
		}
		//echo $absent_msg; die;
		if (isset($is_leave_sms) && $is_leave_sms == 'on') {
			$leave = $this->db->query("SELECT `template_body` FROM `tbl_message_template` WHERE `is_leave_sms` = 1")->row();
			if (empty($leave)) {
				$sdata['exception'] = "Please leave sms template set first.";
				$this->session->set_userdata($sdata);
				redirect("student_timekeepings/add");
			}
			$leave_msg = $leave->template_body;
		}
		$date = $this->input->post("date");
		$period_id = $this->input->post("period_id");
		$class_id = $this->input->post("class_id");
		$section_id = $this->input->post("section_id");
		$shift_id = $this->input->post("shift_id");
		$loop_time = $this->input->post("loop_time");
		$i = 1;
		$total_present_sms = 0;
		$total_absent_sms = 0;
		$total_leave_sms = 0;
		$messageData = array();
		$sms_index = 0;
		$total_num_of_sms_for_dynamic_send = 0;
		$numbers_with_student_id = array();
		$send_from = 'AS';
		$newDate = date("d-m-Y", strtotime($date));
		while ($i <= $loop_time) {
			$data = array();
			$data['student_id'] = $this->input->post("student_id_" . $i);
			$data['period_id'] = $period_id;
			$data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['shift_id'] = $shift_id;
			$data['group_id'] = $this->input->post("group_id_" . $i);
			$data['date'] = $date;
			$data['added_by'] = $user_id;
			$data['added_on'] = date('Y-m-d H:i:s');
			$data['login_status'] = $this->input->post("login_status_" . $i);
			$number = $this->input->post("guardian_mobile_" . $i);
			if ($this->Admin_login->validate_mobile_number($number)) {
				if (isset($is_present_sms) && $is_present_sms == 'on' && $data['login_status'] == 'P') {
					$message_body =  $newDate . ", Name: " . $this->input->post("name_" . $i) . "::" .$present_msg;
					$messageData[$sms_index]['msisdn'] = $number;
					$messageData[$sms_index]['text'] = $message_body;
					$total_num_of_sms_for_dynamic_send += $this->Message->smsCount($message_body);
					$messageData[$sms_index]['csms_id'] = $this->Message->generateSMSTransactionID();
					$sms_index++;
					$total_present_sms++;
				}
				if (isset($is_absent_sms) && $is_absent_sms == 'on' && $data['login_status'] == 'A') {
					$message_body = $newDate . ", Name :" . $this->input->post("name_" . $i) . "::" .$absent_msg;
					$messageData[$sms_index]['msisdn'] = $number;
					$messageData[$sms_index]['text'] = $message_body;
					$total_num_of_sms_for_dynamic_send += $this->Message->smsCount($message_body);
					$messageData[$sms_index]['csms_id'] = $this->Message->generateSMSTransactionID();
					$sms_index++;
					$total_absent_sms += 1;
				}
				if (isset($is_leave_sms) && $is_leave_sms == 'on' && $data['login_status'] == 'L') {
					$message_body = $newDate . ", Name :" . $this->input->post("name_" . $i) . "::" .$leave_msg;
					$messageData[$sms_index]['msisdn'] = $number;
					$messageData[$sms_index]['text'] = $message_body;
					$total_num_of_sms_for_dynamic_send += $this->Message->smsCount($message_body);
					$messageData[$sms_index]['csms_id'] = $this->Message->generateSMSTransactionID();
					$sms_index++;
					$total_leave_sms += 1;
				}
				$numbers_with_student_id[$number]['student_id'] = $data['student_id'];
				$numbers_with_student_id[$number]['number'] = $number;
			}
			$this->db->delete('tbl_student_logins', array('student_id' => $data['student_id'], 'date' => $date, 'period_id' => $period_id));
			$this->db->insert('tbl_student_logins', $data);
			$i++;
		}
		$this->Message->dynamicSMSSend($messageData, $send_from,$numbers_with_student_id,$total_num_of_sms_for_dynamic_send);
		$sdata['message'] = "Student Login Info. Successfully Updated & " . $total_present_sms . " Present, " . $total_absent_sms . " Absent and " . $total_leave_sms . " Leave SMS Send.";
		$this->session->set_userdata($sdata);
		redirect("student_timekeepings/add");
	}
}
