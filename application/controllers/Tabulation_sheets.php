
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Tabulation_sheets extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login', 'Student'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['is_pdf'] = 0;
		$data['title'] = "Tabulation Sheet";
		$data['heading_msg'] = "Tabulation Sheet";
		if ($_POST) {
			$exam_id = $this->input->post("exam");
			$group = $this->input->post("group");
			$year = $this->input->post("year");
			$report_type = $this->input->post("report_type");

			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];


			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;

			$data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['group'] = $group;
			$data['shift_id'] = $shift_id;
			$data['year'] = $year;
			$data['exam_id'] = $exam_id;
			$data['report_type'] = $report_type;
			$data['class_shift_section_id'] = $class_shift_section_id;


			$class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
			$data['class_name'] = $class_info[0]['name'];

			$section_info = $this->db->query("SELECT name FROM tbl_section WHERE id ='$section_id'")->result_array();
			$data['section_name'] = $section_info[0]['name'];

			$group_info = $this->db->query("SELECT name FROM tbl_student_group WHERE id ='$group'")->result_array();
			$data['group_name'] = $group_info[0]['name'];

			$shift_info = $this->db->query("SELECT name FROM tbl_shift WHERE id ='$shift_id'")->result_array();
			$data['shift_name'] = $shift_info[0]['name'];

			$exam_info = $this->db->query("SELECT name,year,exam_type_id FROM tbl_exam WHERE id ='$exam_id'")->result_array();
			$data['exam_name'] = $exam_info[0]['name'];
			$data['exam_year'] = $exam_info[0]['year'];
			$exam_type_id =  $exam_info[0]['exam_type_id'];


			$data['result_data'] = $this->Student->result_all_student_after_process($exam_id, $class_id, $section_id, $group, $shift_id, 0);
			if (empty($data['result_data'])) {
				$data = array();
				$sdata['exception'] = "Sorry result info. not found. Please process result first";
				$this->session->set_userdata($sdata);
				redirect("tabulation_sheets/index");
			}

			$data['main_marking_heads'] = $this->Admin_login->get_main_marking_head($class_id);
      if(empty($data['main_marking_heads'])){
        $sdata['exception'] = "Please set class wise marking head";
        $this->session->set_userdata($sdata);
        redirect('main_marking_heads/edit/' . $class_id);
      }

			if($report_type != 'A4'){
				$data['subject_list'] = $this->db->query("SELECT s.`name`,s.`code`,cs.* FROM `tbl_class_wise_subject` AS cs
				INNER JOIN `tbl_subject` AS s ON s.`id` = cs.`subject_id`
				WHERE cs.`class_id` = '$class_id' AND FIND_IN_SET('$group', `group_list`) > 0 GROUP BY cs.`subject_id` ORDER BY cs.`order_number` ASC")->result_array();
				$colspan = 0;
				foreach ($data['subject_list'] as $subject_row){
					if($subject_row['is_written_allow'] == '1'){
						$colspan += 1;
					}
					if($subject_row['is_objective_allow'] == '1'){
						$colspan += 1;
					}
					if($subject_row['is_practical_allow'] == '1'){
						$colspan += 1;
					}
					if($subject_row['is_class_test_allow'] == '1'){
						$colspan += 1;
					}
					$colspan += 2;
				}
				$data['colspan'] = $colspan;
			}

			if ($report_type == 'A4') {
				$data['report'] = $this->load->view('tabulation_sheets/a4_report_table', $data, true);
			}else if($report_type == 'LB'){
				if (empty($data['subject_list'])) {
					$sdata['exception'] = "Please check class wise subject and assign group.";
					$this->session->set_userdata($sdata);
					redirect("tabulation_sheets/index");
				}
				$data['report'] = $this->load->view('tabulation_sheets/legal_report_table_lb', $data, true);
			}else{
				if (empty($data['subject_list'])) {
					$sdata['exception'] = "Please check class wise subject and assign group.";
					$this->session->set_userdata($sdata);
					redirect("tabulation_sheets/index");
				}
				$data['report'] = $this->load->view('tabulation_sheets/legal_report_table_li', $data, true);
			}
			$this->load->view('report_content/report_main_content', $data);

		}else{
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['group_list'] = $this->Admin_login->getGroupList();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('tabulation_sheets/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

	public function ajax_exam_by_year()
	{
		$year = $_GET['year'];
		$data = array();
		$data['exam_list'] = $this->db->query("SELECT * FROM `tbl_exam` AS e WHERE e.`year` = '$year' AND e.`status` = '1'")->result_array();
		$this->load->view('result_short_tebulations/exam_list', $data);
	}
}
