<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Collection_details_reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Collection Details Report';
        $data['heading_msg'] = "Collection Details Report";
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $this->load->library('numbertowords');
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;
            $class_id = $this->input->post("class_id");
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");

            $data['class_id'] = $class_id;
            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
            if ($class_id == 'all') {
                $data['class_name'] = "All";
            } else {
                $class_info = $this->db->query("SELECT * FROM tbl_class WHERE id ='$class_id'")->result_array();
                $data['class_name'] = $class_info[0]['name'];
            }
            $data['rdata'] = $this->Student_fee->getCollectionDetailsStandard($data);
            $data['report'] = $this->load->view('collection_details_reports/report', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            //echo 'gggg'; die;
            // Load all views as normal
            $data['is_pdf'] = 1;
            $html = $this->load->view('collection_details_reports/report', $data, true);
            $pdfFilePath = "FeeCollectionSummeryReport.pdf";
            $this->load->library('m_pdf');
            $this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
            //generate the PDF from the given html
            $this->m_pdf->pdf->autoScriptToLang = true;
            $this->m_pdf->pdf->WriteHTML($html);
            //download it.
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
        } else {
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('collection_details_reports/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
