<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Id_cards extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Admin_login','Config_general'));

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data['title'] = 'Student ID Card Print';
        $data['heading_msg'] = "Student ID Card Print";
        if ($_POST) {
            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];

            $group = $this->input->post('txtGroup', true);
            $IsDoublePage = $this->input->post('IsDoublePage', true);
            $IsPrintFor = $this->input->post('IsPrintFor', true);
            $Part = $this->input->post('Part', true);
            $Design = $this->input->post('design', true);

            $from_roll = $this->input->post('from_roll', true);
            $to_roll = $this->input->post('to_roll', true);

            $view_type = $this->input->post('view_type', true);


            $student_id = 0;
            $st_where = "";
            if ($view_type == 'S') {
                if ($from_roll == '' || $to_roll == '') {
                    $sdata['exception'] = "From roll and To roll cannot be empty";
                    $this->session->set_userdata($sdata);
                    redirect("id_cards/index");
                }
                if ($from_roll > $to_roll) {
                    $sdata['exception'] = "From roll cannot be greater than To roll";
                    $this->session->set_userdata($sdata);
                    redirect("id_cards/index");
                }

                $roll_where_in_string = "";
                while ($from_roll <= $to_roll) {
                    $roll_where_in_string .= "'" . sprintf("%02d", $from_roll)."',";
                    $from_roll++;
                }
                $roll_where_in_string_for_q = rtrim($roll_where_in_string, ',');
                //die;
                $st_where = " AND s.`roll_no` IN ($roll_where_in_string_for_q)";
            }



            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['shift'] = $shift;

            $data['title'] = 'Student ID Card';
            $data['heading_msg'] = "Student ID Card";

            $roll_where =  "";
            if ($view_type == 'S') {
                $roll_where = $st_where;
            }

            $where = " AND s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` =  '$group' AND s.`shift_id` =  '$shift'";



            $data['list'] = $this->db->query("SELECT sf.name as shift_name,sem.name as semester_name,s.guardian_mobile,b.name as blood_group_name,
            s.date_of_admission,s.`id`,LTRIM(s.`name`) AS name,s.`roll_no`,s.`father_name`,s.`mother_name`,s.`date_of_birth`,
            s.`student_code`,c.`name` AS class_name,sc.`name` AS section_name,
            g.`name` AS group_name,s.mother_name,s.father_name,s.`photo`,s.year,s.`qr_code`
            FROM `tbl_student` AS s
            LEFT JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
            LEFT JOIN `tbl_section` AS sc ON s.`section_id` = sc.`id`
            LEFT JOIN `tbl_student_group` AS g ON s.`group` = g.`id`
            LEFT JOIN `tbl_blood_group` AS b ON s.`blood_group_id` = b.`id`
            LEFT JOIN `tbl_shift` AS sf ON s.`shift_id` = sf.`id`
            LEFT JOIN `tbl_semester` AS sem ON s.`semester_id` = sem.`id`
            WHERE s.`status` = '1' $where $roll_where ORDER BY s.roll_no asc")->result_array();
            if (empty($data['list'])) {
                $sdata['exception'] = "Data Not Found";
                $this->session->set_userdata($sdata);
                redirect("id_cards/index");
            }
            // echo '<pre>';
            // print_r($data['list']);
            // die;
            $data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
            $data['signature'] = $this->Admin_login->getSignatureByAccessCode('SIC'); //SIC= student ID card


            $is_pdf_request = $this->input->post('is_pdf_request', true);
            $general_config = $this->Config_general->get_general_configurations('general');

			$data['id_card_config'] = $this->db->query("SELECT * FROM `tbl_id_card_config`")->row();
			if(empty($data['id_card_config'])){
				$sdata['exception'] = "ID Card Configuration not found";
				$this->session->set_userdata($sdata);
				redirect("id_cards/index");
			}

            if ($Part == 'F') {
                if ($Design == 'D1') {
                    $this->load->view('id_cards/normal_design_1', $data);
                }elseif($Design=='D3'){
                   $this->load->view('id_cards/normal_design_3', $data);
                } else {
                    $this->load->view('id_cards/normal_design_2', $data);
                }

                // if ($IsDoublePage == 'Y') {
                //     $data['is_pdf_request'] = $is_pdf_request;
                //     $this->load->view('id_cards/student_id_card_double_print', $data);
                // } else {
                //     if ($IsPrintFor == 'Z' && $general_config->id_card_design == '0') {
                //         $this->load->view('id_cards/student_id_card_print_for_zebra_printer_d0', $data);
                //     } elseif ($IsPrintFor == 'Z' && $general_config->id_card_design == '1') {
                //         $this->load->view('id_cards/student_id_card_print_for_zebra_printer_d1', $data);
                //     } elseif ($IsPrintFor == 'Z' && $general_config->id_card_design == '2') {
                //         $this->load->view('id_cards/student_id_card_print_for_zebra_printer_d2', $data);
                //     } else {
                //         $this->load->view('id_cards/student_id_card_print', $data);
                //     }
                // }
            } else {
				$this->load->view('id_cards/normal_design_1_back_part', $data);
              //  $this->load->view('id_cards/student_id_card_back_part_print', $data);
            }
        } else {
            $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
            $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('id_cards/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
