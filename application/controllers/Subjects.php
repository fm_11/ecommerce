<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Subjects extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Subject List';
        $data['heading_msg'] = "Subject List";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['subject'] = $this->db->query("SELECT * FROM tbl_subject ORDER BY code")->result_array();
        $data['maincontent'] = $this->load->view('subjects/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['short_name'] = $_POST['short_name'];
            $data['relational_subject_id'] = $_POST['relational_subject_id'];
            $data['code'] = $_POST['code'];
            $this->db->insert("tbl_subject", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('subjects/index');
        }
        $data = array();
        $data['title'] = 'Add Subject';
        $data['heading_msg'] = "Add Subject";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('subjects/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['short_name'] = $_POST['short_name'];
            $data['relational_subject_id'] = $_POST['relational_subject_id'];
            $data['code'] = $_POST['code'];
            $this->db->where('id', $id);
            $this->db->update('tbl_subject', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('subjects/index');
        }
        $data = array();
        $data['title'] = 'Edit Subject';
        $data['heading_msg'] = "Edit Subject";
        $data['is_show_button'] = "index";
        $data['subject'] = $this->db->query("SELECT * FROM tbl_subject WHERE `id` = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('subjects/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function subjectByClassSectionShiftAndExam()
    {
        $class_shift_section_id = $_GET['class_shift_section_id'];
        $class_shift_section_arr = explode("-", $class_shift_section_id);
        $class_id  = $class_shift_section_arr[0];
        $exam_id = $_GET['exam_id'];
        $exam_info =  $this->db->query("SELECT exam_type_id FROM tbl_exam WHERE id = '$exam_id'")->result_array();
        $exam_type_id = $exam_info[0]['exam_type_id'];
        $data = array();
        $data['subject_list'] = $this->db->query("SELECT
      c.`subject_id`,
      s.`name`, s.`code`
      FROM
      `tbl_class_wise_subject` AS c
      LEFT JOIN `tbl_subject` AS s
        ON s.`id` = c.`subject_id`
      WHERE  c.`exam_type_id` = '$exam_type_id' AND  c.`class_id` = '$class_id'
      GROUP BY c.`subject_id` ORDER BY c.`order_number`")->result_array();
        $this->load->view('subjects/subject_list_for_dropdown', $data);
    }


    public function delete($id)
    {
        $Check = $this->db->query("SELECT `id` FROM `tbl_class_wise_subject` WHERE `subject_id` = '$id' LIMIT 1")->result_array();
        if (empty($Check)) {
            $this->db->query("DELETE FROM tbl_subject WHERE `id` = '$id'");
            $sdata['message'] =  $this->lang->line('delete_success_message');
            $this->session->set_userdata($sdata);
            redirect('subjects/index');
        } else {
            $sdata['exception'] =  $this->lang->line('delete_success_message') . " (This Subject already assigned to a class)";
            $this->session->set_userdata($sdata);
            redirect('subjects/index');
        }
    }
}
