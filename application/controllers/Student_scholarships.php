<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student_scholarships extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Message', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    function index()
    {
        $data = array();
        $data['title'] = 'Student Scholarship';
        $data['heading_msg'] = "Student Scholarship";
        $data['is_show_button'] = "add";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_scholarships/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Student_fee->get_all_student_scholarship(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['list'] = $this->Student_fee->get_all_student_scholarship(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_scholarships/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function add()
    {
        $data = array();
        if ($_POST) {
            $year = $this->input->post("year");

            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];

            $group = $this->input->post("group");
            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['shift_id'] = $shift_id;
            $data['section_id'] = $section_id;
            $data['class_shift_section_id'] = $class_shift_section_id;
            $data['group'] = $group;

            $data['student_info'] = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code`,sl.`year`,sl.`id` AS already_save  FROM `tbl_student` AS s
LEFT JOIN `tbl_student_scholarship` AS sl ON sl.`student_id` = s.`id` AND sl.`year` = '$year'
WHERE s.`class_id` = '$class_id' AND s.`shift_id` = '$shift_id'
AND s.`section_id` = '$section_id' AND s.`group` = '$group' AND s.`status` = '1' ORDER BY ABS(s.roll_no)")->result_array();

            $data['scholarship_form'] = $this->load->view('student_scholarships/student_scholarship_add_form', $data, true);
        }

        $data['title'] = 'Student Scholarship';
        $data['heading_msg'] = "Student Scholarship";
        $data['is_show_button'] = "index";
        $data['years'] = $this->Admin_login->getYearList(0, 0);
        $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['group_list'] = $this->Admin_login->getGroupList();
        $data['maincontent'] = $this->load->view('student_scholarships/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);

    }

    public function student_scholarship_data_save()
    {
        $year = $this->input->post("year");
        $loop_time = $this->input->post("loop_time");
        $class_id = $this->input->post("class_id");
        $shift_id = $this->input->post("shift_id");
        $section_id = $this->input->post("section_id");
        $group = $this->input->post("group");
        $this->db->delete('tbl_student_scholarship', array('class_id' => $class_id, 'section_id' => $section_id,
         'shift_id' => $shift_id, 'group' => $group, 'year' => $year));
        $i = 1;
        while ($i <= $loop_time) {
            if ($this->input->post("is_allow_" . $i)) {
                $data = array();
                $data['student_id'] = $this->input->post("student_id_" . $i);
                $data['roll_no'] = $this->input->post("roll_no_" . $i);
                $data['year'] = $year;
                $data['class_id'] = $class_id;
                $data['shift_id'] = $shift_id;
                $data['section_id'] = $section_id;
                $data['group'] = $group;
                $data['date'] = date('Y-m-d');
                $this->db->insert('tbl_student_scholarship', $data);
            }
            $i++;
        }
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect("student_scholarships/index");
    }

}
