<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Class_section_mappings extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Account', 'Admin_login'));
		$this->load->library('session');
		date_default_timezone_set('Asia/Dhaka');
		$this->notification = array();
	}

	public function index(){
		$data = array();
		$class_list = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
		$i = 0;
		$class_array = array();
		foreach ($class_list as $class){
			$class_id = $class['id'];
			$class_array[$i]['id'] = $class_id;
			$class_array[$i]['name'] = $class['name'];

			$section_list = $this->db->query("SELECT GROUP_CONCAT(DISTINCT CONCAT(s.`name`) 
ORDER BY s.`name` DESC) AS section_list
FROM `tbl_section` AS s
INNER JOIN `tbl_class_section_mappings` AS m ON m.`section_id` = s.`id` AND m.`class_id` = $class_id;")->row();

			$class_array[$i]['sections_name'] = $section_list->section_list;

			$i++;
		}

		$data['class_list'] = $class_array;
		$data['is_show_button'] = "add";
		$data['title'] = "Class Section Mapping";
		$data['heading_msg'] = "Class Section Mapping";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('class_section_mappings/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add(){
		if($_POST){
			$classes =  $_POST['class_id'];
			$total_row =  $_POST['total_row'];
			foreach ($classes as $class_id){
				$i = 0;
				$insert_row_no = 0;
				$save_data = array();
				while ($i < $total_row){
					if(isset($_POST['selected_item_' . $i])){
						$save_data[$insert_row_no]['class_id'] = $class_id;
						$save_data[$insert_row_no]['section_id'] = $_POST['section_id_' . $i];
						$insert_row_no++;
					}
					$i++;
				}
				$this->db->query("DELETE FROM `tbl_class_section_mappings` WHERE `class_id` = $class_id;");
				$this->db->insert_batch('tbl_class_section_mappings', $save_data);
			}
			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect('class_section_mappings/add');
		}
		$section_info = $this->db->query("SELECT * FROM `tbl_section` ORDER BY `id`")->result_array();
		if (empty($section_info)) {
			$sdata['exception'] = "Please section add first";
			$this->session->set_userdata($sdata);
			redirect('sections/index');
		}
		$data['section_info'] = $section_info;
		$data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`
WHERE `id` NOT IN (SELECT `class_id` FROM `tbl_class_section_mappings` GROUP BY `class_id`)")->result_array();
		$data['is_show_button'] = "index";
		$data['title'] = "Class Section Mapping";
		$data['heading_msg'] = "Class Section Mapping";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('class_section_mappings/add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function edit($class_id=null)
	{
		if ($_POST) {
			$class_id =  $_POST['class_id'];
			$total_row =  $_POST['total_row'];
			$i = 0;
			$insert_row_no = 0;
			$save_data = array();
			while ($i < $total_row){
				if(isset($_POST['selected_item_' . $i])){
					$save_data[$insert_row_no]['class_id'] = $class_id;
					$save_data[$insert_row_no]['section_id'] = $_POST['section_id_' . $i];
					$insert_row_no++;
				}
				$i++;
			}
			$this->db->query("DELETE FROM `tbl_class_section_mappings` WHERE `class_id` = $class_id;");
			$this->db->insert_batch('tbl_class_section_mappings', $save_data);
			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect('class_section_mappings/index');
		}
		$data = array();
		$data['class_id'] = $class_id;
		$data['class_info'] = $this->db->query("SELECT * FROM `tbl_class` WHERE id = $class_id")->row();
		$section_info = $this->db->query("SELECT s.*,sm.`id` AS mapping_id FROM `tbl_section` AS s
                                  LEFT JOIN `tbl_class_section_mappings` AS sm 
                                  ON sm.`section_id` = s.`id` AND sm.`class_id` = $class_id ORDER BY s.`id`")->result_array();
		if (empty($section_info)) {
			$sdata['exception'] = "Please section add first";
			$this->session->set_userdata($sdata);
			redirect('sections/index');
		}
		$data['section_info'] = $section_info;
		$data['title'] = "Class Section Mapping";
		$data['heading_msg'] = "Class Section Mapping";
		$data['is_show_button'] = "index";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('class_section_mappings/edit', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}
