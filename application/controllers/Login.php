<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('common/insert_model', 'common/edit_model', 'common/custom_methods_model','Message'));
        $this->load->library(array('session'));
        $this->notification = array();
    }

    public function index()
    {
        //$this->update_database();
        $data = array();
        $contact_info = $this->db->query("SELECT school_name FROM tbl_contact_info")->result_array();
        $data['school_name'] = $contact_info[0]['school_name'];
        $this->load->view('login/index', $data);
    }

	public function student()
	{
		$data = array();
		$contact_info = $this->db->query("SELECT school_name FROM tbl_contact_info")->result_array();
		$data['school_name'] = $contact_info[0]['school_name'];
		$data['title'] = 'Login to Student and Guardian Panel';
		$data['heading_msg'] = "Login to Student and Guardian Panel";
		$this->load->view('student_guardian_panels/login', $data);
	}

	public function student_guardian_login_authentication()
	{
		$user_name = $this->input->post("user_name");
		$password = $this->input->post("password");
		$password = md5($password);
		$is_authenticated = $this->custom_methods_model->num_of_data("tbl_student", "WHERE student_code='$user_name' AND password='$password'");
		if ($is_authenticated > 0) {
			$student_info = $this->custom_methods_model->select_row_by_condition("tbl_student", "student_code='$user_name' AND password='$password'");
			$contact_info = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
			$student_info[0]->contact_info = $contact_info;
			$this->session->set_userdata('session_student_info', $student_info);
			redirect("student_guardian_panels/index");
		} else {
			$sdata['exception'] = "Student ID or Password did not match";
			$this->session->set_userdata($sdata);
			redirect("login/student");
		}
	}

	public function student_guardian_logout()
	{
		$this->session->unset_userdata('student_info');
		$sdata['message'] = "You are logged out";
		$this->session->set_userdata($sdata);
		redirect("login/student");
	}

	public function student_forgot_password()
	{
		if ($_POST) {
			$student_code = $this->input->post("student_code");
			$is_authenticated = $this->custom_methods_model->num_of_data("tbl_student", "WHERE student_code='$student_code'");
			if ($is_authenticated > 0) {
				$student_info = $this->custom_methods_model->select_row_by_condition("tbl_student", "student_code='$student_code'");
				if ($this->validate_mobile_number($student_info[0]->guardian_mobile)) {
					$new_pass = $this->random_password(6);
					// echo '<pre>';
					// print_r($student_info);
					// die;
					$data = array();
					$data['password'] =   md5($new_pass);
					$data['id'] = $student_info[0]->id;
					$this->db->where('id', $data['id']);
					$this->db->update('tbl_student', $data);

					$name = $student_info[0]->name;
					$sms_info = array();
					$sms_info['message'] = "Dear " . $name . ", You have successfully forgot your password. Your Password is : '" . $new_pass . "'. Thank You, School360 Team";
					$sms_info['numbers'] = "88" . $student_info[0]->guardian_mobile;
					$sms_info['is_bangla_sms'] = 0;
					$status = $this->Message->sms_send($sms_info);
					if ($status) {
						$sdata['message'] = "Your password has been send by a SMS";
						$this->session->set_userdata($sdata);
						redirect("login/student_forgot_password");
					} else {
						$sdata['exception'] = "Some error occurred.";
						$this->session->set_userdata($sdata);
						redirect("login/student_forgot_password");
					}
				} else {
					$sdata['exception'] = "Student Mobile Number Not Valid";
					$this->session->set_userdata($sdata);
					redirect("login/student_forgot_password");
				}
			} else {
				$sdata['exception'] = "Student ID did not match";
				$this->session->set_userdata($sdata);
				redirect("login/student_forgot_password");
			}
		}
		$data = array();
		$contact_info = $this->db->query("SELECT school_name FROM tbl_contact_info")->result_array();
		$data['school_name'] = $contact_info[0]['school_name'];
		$this->load->view('student_guardian_panels/forgot_password', $data);
	}


	public function download_zone()
	{
		$data = array();
		$data['title'] = "School360 Download Zone";
		$contact_info = $this->db->query("SELECT school_name FROM tbl_contact_info")->result_array();
		$data['school_name'] = $contact_info[0]['school_name'];
		$this->load->view('login/download_zone', $data);
	}

    public function update_database()
    {
        $tbl_school360_changes_exist =  $this->db->query("SHOW TABLES LIKE 'tbl_school360_changes';")->result_array();
        //echo '<pre>';
        //print_r($tbl_school360_changes_exist);
        //die;
        $where = "";
        if (!empty($tbl_school360_changes_exist)) {
            $already_executable_query = $this->db->query("SELECT query_id FROM tbl_school360_changes")->result_array();
            $already_executable_query_to_string = implode(",", array_column($already_executable_query, 'query_id'));

            // echo $already_executable_query_a;
            // print_r($already_executable_query_a);
            // die;
            $where = " AND id NOT IN ($already_executable_query_to_string)";
        }

        $changes_db = $this->load->database('changes_data_db', true);
        $all_query = $changes_db->query("SELECT * FROM tbl_changes_query WHERE is_executable = 1 $where ORDER BY id ASC;")->result_array();
        $changes_db->close();
        $this->load->database('default', true);

        $tbl_school360_changes = array();
        $already_execute_sc = array();
        if (!empty($all_query)) {
            foreach ($all_query as $row):
                $query = $row['query_string'];
            $this->db->query("$query");
            $already_execute = array();
            $already_execute['query_id'] = $row['id'];
            $already_execute['executed_db'] = SCHOOL_NAME;
            $already_execute['date_time'] =  date('Y-m-d H:i:s');
            array_push($already_execute_sc, $already_execute);

            $school360_change = array();
            $school360_change['query_id'] = $row['id'];
            $school360_change['date_time'] =  date('Y-m-d H:i:s');
            array_push($tbl_school360_changes, $school360_change);
            endforeach;
            $this->db->insert_batch('tbl_school360_changes', $tbl_school360_changes);

            $changes_db = $this->load->database('changes_data_db', true);
            $changes_db->insert_batch('tbl_already_execute_sc', $already_execute_sc);
            $changes_db->close();
            $this->load->database('default', true);
        }
        return true;
    }

    public function validate_mobile_number($phoneNumber)
    {
        if (!empty($phoneNumber)) { // phone number is not empty
            if (preg_match('/^\d{11}$/', $phoneNumber)) { // phone number is valid
                return true;
            // your other code here
            } else { // phone number is not valid
                return false;
            }
        } else { // phone number is empty
            return false;
        }
    }

    public function random_password($length)
    {
        $chars = "0123456789";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }


    public function forgot_password()
    {
        if ($_POST) {
            $user_name = $this->input->post("user_name");
            $is_authenticated = $this->custom_methods_model->num_of_data("tbl_user", "WHERE user_name='$user_name'");
            if ($is_authenticated > 0) {
                $user_info = $this->custom_methods_model->select_row_by_condition("tbl_user", "user_name='$user_name'");
                if ($this->validate_mobile_number($user_info[0]->mobile)) {
                    $new_pass = $this->random_password(8);
                    // echo $new_pass;
                    // die;
                    $mdata = array();
                    $mdata['user_id'] = $user_info[0]->id;
                    $mdata['old_pass'] = $user_info[0]->user_password;
                    $mdata['new_pass'] = md5($new_pass);
                    $mdata['date'] = date('Y-m-d H:i:s');
                    $this->db->insert("tbl_user_pass_history", $mdata);

                    $data = array();
                    $data['user_password'] =   md5($new_pass);
                    $data['id'] = $user_info[0]->id;
                    $this->db->where('id', $data['id']);
                    $this->db->update('tbl_user', $data);


                    $sms_info = array();
                    $sms_info['message'] = "You have successfully forgot your password. Your Password is : '" . $new_pass . "'. Thank You, School360° Team";
                    $sms_info['numbers'] = "88" . $user_info[0]->mobile;
                    $sms_info['is_bangla_sms'] = 0;
                    $status = $this->Message->sms_send($sms_info);

                    if ($status) {
                        $sdata['message'] = "Your password has been send by a SMS.";
                        $this->session->set_userdata($sdata);
                        redirect("login/forgot_password");
                    } else {
                        $sdata['exception'] = "Some error occurred.";
                        $this->session->set_userdata($sdata);
                        redirect("login/forgot_password");
                    }
                } else {
                    $sdata['exception'] = "User Mobile Number Not Valid!";
                    $this->session->set_userdata($sdata);
                    redirect("login/forgot_password");
                }
            } else {
                $sdata['exception'] = "User Name did not match!";
                $this->session->set_userdata($sdata);
                redirect("login/forgot_password");
            }
        }
        $data = array();
        $contact_info = $this->db->query("SELECT school_name FROM tbl_contact_info")->result_array();
        $data['school_name'] = $contact_info[0]['school_name'];
        $this->load->view('login/forgot_password', $data);
    }

    public function authentication()
    {
        $user_name = $this->input->post("user_name");
        $password = $this->input->post("password");
        //echo $password; die;
        $password = md5($password);
        $is_authenticated = $this->custom_methods_model->num_of_data("tbl_user", "WHERE user_name='$user_name' AND user_password='$password'");
        //echo $is_authenticated; die;
        if ($is_authenticated > 0) {
            $user_info = $this->custom_methods_model->select_row_by_condition("tbl_user", "user_name='$user_name' AND user_password='$password'");

            //API call
            /*$contact_info = $this->db->query("SELECT eiin_number FROM tbl_contact_info")->result_array();
            $eiin_number = $contact_info[0]['eiin_number'];
            $postdata = http_build_query(
                array(
                    'security_pin' => 12345,
                    'eiin_number' => $eiin_number
                )
            );
            $eo_domain = "http://www.deo.jamalpurdistrict.org/";
            $result = $this->getSchoolData($postdata, $eo_domain.'ServiceBridge/getTotalNewsThisMonth');
            if(empty($result)){
                $user_info[0]->notice_this_month = 0;
            }else{
                $user_info[0]->notice_this_month = $result[0]->notice_this_month;
            }*/
            $user_info[0]->notice_this_month = 0;
            //error_reporting(E_ALL);

            //End API call

            $config_info = $this->db->query("SELECT * FROM tbl_config")->result_array();
            $user_info[0]->config_info = $config_info ;

            $default_language = $config_info[0]['default_language'];
            $language = 'english';
            if ($default_language != 'E') {
                $language = 'bangla';
            }
            $this->session->set_userdata('site_lang', $language);




            //menu type
			if(isset($config_info[0]['menu_type'])){
				$menu_type = $config_info[0]['menu_type'];
				if($menu_type == 'LL'){
					$this->session->set_userdata('site_menu', 'left');
					$this->session->set_userdata('site_menu_color', 'light');
				}else if($menu_type == 'LD'){
					$this->session->set_userdata('site_menu', 'left');
					$this->session->set_userdata('site_menu_color', 'dark');
				}else{
					$this->session->set_userdata('site_menu', 'top');
					$this->session->set_userdata('site_menu_color', 'dark');
				}
			}else{
				$this->session->set_userdata('site_menu', 'top');
				$this->session->set_userdata('site_menu_color', 'dark');
			}


			//left menu color
			$left_menu_background_color = "#282F3A";
			$top_menu_show_hide_button_color = "";
			$top_bar_background_color = "";

			$dashboard_color_info = $this->db->query("SELECT `left_menu_background_color`,`top_bar_background_color`,`top_menu_show_hide_button_color` FROM tbl_dashboard_color_settings")->row();
			if(!empty($dashboard_color_info)){
				if($dashboard_color_info->left_menu_background_color != ''){
					$left_menu_background_color = $dashboard_color_info->left_menu_background_color;
				}
				if($dashboard_color_info->top_menu_show_hide_button_color != ''){
					$top_menu_show_hide_button_color = $dashboard_color_info->top_menu_show_hide_button_color;
				}
				if($dashboard_color_info->top_bar_background_color != ''){
					$top_bar_background_color = $dashboard_color_info->top_bar_background_color;
				}
			}
			$user_info[0]->left_menu_background_color = $left_menu_background_color;
			$user_info[0]->top_menu_show_hide_button_color = $top_menu_show_hide_button_color;
			$user_info[0]->top_bar_background_color = $top_bar_background_color;
			//left menu color end



            $contact_info = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
            $user_info[0]->contact_info = $contact_info;
            //	echo 555; die;
            $this->session->set_userdata('user_info', $user_info);

            //$user_inddfo = $this->session->userdata('user_info');
            //echo '<pre>';
            //print_r($user_inddfo); die;

            redirect("dashboard/index");
        } else {
            $sdata['exception'] = "User Name or Password did not match!";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }

    public function getSchoolData($postdata, $api)
    {
        $opts = array('http' =>
            array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context = stream_context_create($opts);
        $result = file_get_contents("$api", false, $context);
        return json_decode($result);
    }

    public function logout()
    {
        $this->session->unset_userdata('user_info');
        $this->session->userdata = array();
        $this->session->sess_destroy();
        $sdata['message'] = "You are logged out";
        $this->session->set_userdata($sdata);
        redirect("login/index");
    }

    public function denied()
    {
        $data['title'] = 'Access denied';
        $data['heading_msg'] = 'Access denied';
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('login/denied', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    public function change_password($user_id = null)
    {
        if ($_POST) {
            $user_id = $this->input->post("user_id");
            $password = $this->input->post("current_pass");
            $password = md5($password);
            $is_authenticated = $this->custom_methods_model->num_of_data("tbl_user", "WHERE id='$user_id' AND user_password='$password'");
            if ($is_authenticated > 0) {
                $data = array();
                $data['user_password'] = md5($this->input->post("new_pass"));
                $data['id'] = $user_id;
                $this->db->where('id', $data['id']);
                $this->db->update('tbl_user', $data);
                $sdata['message'] = "You are Successfully Password Changes.";
                $this->session->set_userdata($sdata);
                redirect("login/change_password/".$user_id);
            } else {
                $sdata['exception'] = "Sorry Current Password Does'nt Match !";
                $this->session->set_userdata($sdata);
                redirect("login/change_password/".$user_id);
            }
        } else {
            $data['title'] = 'Change Password';
            $data['heading_msg'] = "Change Password";
            $data['user_id'] = $user_id;
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('login/change_password', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }



}
