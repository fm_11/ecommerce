<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Templates extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Message', 'Admin_login'));
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = 'SMS Template';
        $data['heading_msg'] = "SMS Template";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['templateList'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        $data['maincontent'] = $this->load->view('templates/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function add()
    {
        if ($_POST) {
            $insertData['template_name'] = $_POST['templateName'];
            $insertData['template_body'] = $_POST['templateBody'];
            $insertData['is_present_sms'] = $_POST['is_present_sms'];
            $insertData['is_admission_welcome_sms'] = $_POST['is_admission_welcome_sms'];
            $insertData['is_absent_sms'] = $_POST['is_absent_sms'];
            $insertData['is_leave_sms'] = $_POST['is_leave_sms'];
            $insertData['is_bangla_sms'] = $_POST['is_bangla_sms'];
			$insertData['is_teacher_present_sms'] = $_POST['is_teacher_present_sms'];
			$insertData['is_teacher_absent_sms'] = $_POST['is_teacher_absent_sms'];
			$insertData['is_teacher_leave_sms'] = $_POST['is_teacher_leave_sms'];

            $tableName = "tbl_message_template";
            $returnData = $this->Message->DoCommonInsert($insertData, $tableName);
            if ($returnData['status'] == 1) {
                $sdata['message'] =  $this->lang->line('add_success_message');
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
            }
            $this->session->set_userdata($sdata);

            redirect('templates/add');
        }
        $data = array();
        $data['title'] = 'Create SMS Template';
        $data['heading_msg'] = "Create SMS Template";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['templateList'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        $data['maincontent'] = $this->load->view('templates/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $Check = $this->db->query("SELECT `id` FROM `tbl_message_template` WHERE `id` = '$id' LIMIT 1")->result_array();
        /* echo '<pre>';
        print_r($Check); */
        if (!empty($Check)) {
            if ($this->db->query("DELETE FROM tbl_message_template WHERE `id` = '$id'")) {
                $sdata['message'] =  $this->lang->line('delete_success_message');
            } else {
                $sdata['exception'] =  $this->lang->line('delete_error_message');
            }
            $this->session->set_userdata($sdata);
            redirect('templates/index');
        } else {
            $sdata['exception'] = $this->lang->line('delete_error_message');
            $this->session->set_userdata($sdata);
            redirect('templates/index');
        }
    }

    public function edit($id)
    {
        if ($_POST) {
            //echo '<pre>';
            //   print_r($_POST);
            //  die;
            $updateData = array();
            $updateData['template_name'] = $_POST['templateName'];
            $updateData['template_body'] = $_POST['templateBody'];
            $updateData['is_present_sms'] = $_POST['is_present_sms'];
            $updateData['is_admission_welcome_sms'] = $_POST['is_admission_welcome_sms'];
            $updateData['is_absent_sms'] = $_POST['is_absent_sms'];
            $updateData['is_leave_sms'] = $_POST['is_leave_sms'];
            $updateData['is_bangla_sms'] = $_POST['is_bangla_sms'];
			$updateData['is_teacher_present_sms'] = $_POST['is_teacher_present_sms'];
			$updateData['is_teacher_absent_sms'] = $_POST['is_teacher_absent_sms'];
			$updateData['is_teacher_leave_sms'] = $_POST['is_teacher_leave_sms'];
            $this->db->where('id', $id);
            $this->db->update('tbl_message_template', $updateData);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('templates/index');
        }
        $data = array();
        $data['title'] = 'Edit SMS Template';
        $data['heading_msg'] = "Edit SMS Template";
        $data['is_show_button'] = "index";
        $data['EditData'] = $this->db->query("SELECT * FROM tbl_message_template WHERE `id` = '$id'")->row_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['templateList'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        $data['maincontent'] = $this->load->view('templates/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
