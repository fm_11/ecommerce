<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Fee_paid_lists extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student_fee', 'Admin_login', 'Message', 'Timekeeping'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Fees Paid List';
		$data['heading_msg'] = "Fees Paid List";
		if ($_POST) {
			$this->load->library('numbertowords');
			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$year = $this->input->post('year', true);
			$group_id = $this->input->post('txtGroup', true);
			$months = $this->input->post('month', true);
			$reportType = $this->input->post('reportType', true);

			//echo  $months[0];
			//die;
			$total_num_of_month = count($months);
			//echo $total_num_of_month;
			//echo '<pre>';
			//print_r($_POST);
			//die;

			$ClassName = $this->db->query("SELECT * FROM `tbl_class` WHERE id='$class_id'")->result_array();
			$SectionName = $this->db->query("SELECT * FROM `tbl_section` WHERE id='$section_id'")->result_array();
			$SchoolInfo = $this->db->query("SELECT `school_name`,`eiin_number` FROM `tbl_contact_info`")->result_array();
			$Info = array();
			$Info['ClassName'] = $ClassName[0]['name'];
			$Info['SectionName'] = $SectionName[0]['name'];
			$Info['school_name'] = $SchoolInfo[0]['school_name'];
			$Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
			$Info['month'] = $months[0];
			$Info['year'] = $year;
			$data['HeaderInfo'] = $Info;

			$data['months'] = $months;
			$data['paid_list'] = $this->Student_fee->get_student_fee_paid_list($class_id, $section_id, $shift_id, $group_id, $months, $year);
//			echo '<pre>';
//			print_r($data['paid_list']);
//			die;
			$this->load->view('fee_paid_lists/report', $data);
		} else {
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['groupList'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['maincontent'] = $this->load->view('fee_paid_lists/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}
}

