<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Testimonials extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model(array('Message', 'Admin_login'));
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}


	public function index()
	{
		$data = array();
		$cond = array();
		if ($_POST) {
			$sl_no = $this->input->post("sl_no");
			$sdata['sl_no'] = $sl_no;
			$this->session->set_userdata($sdata);
			$cond['sl_no'] = $sl_no;
		} else {
			$sl_no = $this->session->userdata('sl_no');
			$cond['sl_no'] = $sl_no;
		}
		$data['title'] = 'Testimonial Generate';
		$data['heading_msg'] = "Testimonial Generate";
		$this->load->library('pagination');
		$config['base_url'] = site_url('testimonials/index/');
		$config['per_page'] = 20;
		$config['total_rows'] = count($this->Admin_login->get_all_testimonial(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['testimonials'] = $this->Admin_login->get_all_testimonial(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['is_show_button'] = "add";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('testimonials/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add(){
		if($_POST){
			$data = array();
			$data['student_id'] =  $this->input->post('student_id', true);
			$data['sl_no'] = $this->generateSerialNo($this->input->post('year', true));
			$data['qua_id'] =  $this->input->post('qua_id', true);
			$data['student_id'] =  $this->input->post('student_id', true);
			$data['name'] =  $this->input->post('name', true);
			$data['fathers_name'] =  $this->input->post('fathers_name', true);
			$data['mothers_name'] =  $this->input->post('mothers_name', true);
			$data['address'] =  $this->input->post('address', true);
			$data['board_id'] =  $this->input->post('board_id', true);
			$data['year'] =  $this->input->post('year', true);
			$data['group_id'] =  $this->input->post('group_id', true);
			$data['roll_no'] =  $this->input->post('roll_no', true);
			$data['reg_no'] =  $this->input->post('reg_no', true);
			$data['session'] =  $this->input->post('session', true);
			$data['gpa'] =  $this->input->post('gpa', true);
			$data['grade_point'] =  $this->input->post('grade_point', true);
			$data['date_of_birth'] =  $this->input->post('date_of_birth', true);
			$data['issue_date'] =  $this->input->post('issue_date', true);
			$this->db->insert("tbl_testimonial", $data);
			$sdata['message'] = $this->lang->line('add_success_message') . ' ( SL. No - ' . $data['sl_no'] . ')';
			$this->session->set_userdata($sdata);
			redirect('testimonials/index');
		}
		$data = array();
		$data['title'] = 'Testimonial Generate';
		$data['heading_msg'] = 'Testimonial Generate';
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['is_show_button'] = "index";
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['boards'] = $this->db->query("SELECT * FROM tbl_board")->result_array();
		$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$data['qualifications'] = $this->db->query("SELECT * FROM tbl_educational_qualification")->result_array();
		$data['maincontent'] = $this->load->view('testimonials/add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function generateSerialNo($year)
	{
		$code = "";
		$code .= $year . date('m');
		$total_purchase = count($this->db->query("SELECT id FROM tbl_testimonial")->result_array());
		return $code.str_pad(($total_purchase + 1), 8, '0', STR_PAD_LEFT);
	}

	public function view($id){
		$data = array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['is_show_button'] = "index";
		$cond = array();
		$cond['id'] = $id;
		$data['testimonial_info'] = $this->Admin_login->get_all_testimonial(0, 0, $cond);
//		echo '<pre>';
//		print_r($data['testimonial_info']);
//		die;
		$data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->row();
//				echo '<pre>';
//		print_r($data['school_info']);
//		die;
		$data['title'] = 'Testimonial (' . $data['testimonial_info'][0]['sl_no'] . ')';
		$data['heading_msg'] ='Testimonial (' . $data['testimonial_info'][0]['sl_no'] . ')';
		$this->load->view('testimonials/view', $data);
	}

}
