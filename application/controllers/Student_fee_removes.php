<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Student_fee_removes extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->load->model(array('Student_fee', 'Student' , 'Config_general' ,'Admin_login'));
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        if($_POST){
            $year = $this->input->post("year");
            $class_shift_section_id = $this->input->post("class_shift_section_id");
            $group_id = $this->input->post("group_id");
            $fee_sub_category_id = $this->input->post("fee_sub_category_id");



            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];

            $data['year'] = $year;
            $data['class_shift_section_id'] = $class_shift_section_id;
            $data['class_id'] = $class_id;
            $data['shift_id'] = $shift_id;
            $data['section_id'] = $section_id;
            $data['group_id'] = $group_id;
            $data['fee_sub_category_id'] = $fee_sub_category_id;

            $students = $this->Admin_login->getStudentListByYear($year, $class_id, $shift_id, $section_id,$group_id);
            $studentsFeeRemoveData = $this->Student_fee->getFeeRemoveData($year, $class_id, $shift_id, $section_id, $group_id, $fee_sub_category_id);

            $i = 0;
            $retrun_data = array();
            foreach ($students['students'] as $student) {
              $retrun_data[$i]['student_id'] = $student['id'];
              $retrun_data[$i]['name'] = $student['name'];
              $retrun_data[$i]['roll_no'] = $student['roll_no'];
              $retrun_data[$i]['student_code'] = $student['student_code'];
              if(isset($studentsFeeRemoveData[$student['id']])){
                 $retrun_data[$i]['is_fee_remove'] = 1;
              }else{
                  $retrun_data[$i]['is_fee_remove'] = 0;
              }

              $i++;
            }
           $data['retrun_data'] = $retrun_data;
           // echo '<pre>';
           // print_r($data['retrun_data']);
           // die;
           $data['fee_remove_data'] = $this->load->view('student_fee_removes/fee_remove_data', $data, true);
        }
        $data['title'] = "Fee Remove";
        $data['years'] = $this->Admin_login->getYearList(0, 0);
        $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
        $data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['sub_categorys'] = $this->Student_fee->getSubCategory();
        // echo '<pre>';
        // print_r($data['sub_categorys']);
        // die;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fee_removes/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function fee_remove_data_save()
    {
       if($_POST){
         $year = $this->input->post("year");
         $loop_time = $this->input->post("loop_time");
         $class_id = $this->input->post("class_id");
         $section_id = $this->input->post("section_id");
         $group_id = $this->input->post("group_id");
         $shift_id = $this->input->post("shift_id");
         $fee_sub_category_id = $this->input->post("fee_sub_category_id");
         $user_info = $this->session->userdata('user_info');
         $this->db->delete('tbl_fee_remove', array('class_id' => $class_id, 'section_id' => $section_id,
         'group_id' => $group_id, 'shift_id' => $shift_id, 'year' => $year, 'fee_sub_category_id' => $fee_sub_category_id));
         $i = 1;
         while ($i <= $loop_time) {
             if ($this->input->post("is_allow_" . $i)) {
                 $data = array();
                 $data['student_id'] = $this->input->post("student_id_" . $i);
                 $data['year'] = $year;
                 $data['class_id'] = $class_id;
                 $data['section_id'] = $section_id;
                 $data['group_id'] = $group_id;
                 $data['shift_id'] = $shift_id;
                 $data['fee_sub_category_id'] = $fee_sub_category_id;
                 $data['added_by'] = $user_info[0]->id;
                 $data['added_on'] = date('Y-m-d H:i:s');
                 $this->db->insert('tbl_fee_remove', $data);
             }
             $i++;
         }
         $sdata['message'] = $this->lang->line('add_success_message');
         $this->session->set_userdata($sdata);
         redirect("student_fee_removes/index");
       }
    }
}
