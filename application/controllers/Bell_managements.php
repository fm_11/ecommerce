<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Bell_managements extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Bell_management'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }

    public function index()
    {
        $data = array();
        $cond = array();
        $data['title'] = 'Configuration';
        $data['heading_msg'] = "Configuration";
        $this->load->library('pagination');
        $config['base_url'] = site_url('bell_managements/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Bell_management->get_all_bell_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['infos'] = $this->Bell_management->get_all_bell_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('bell_managements/bell_schedule_info_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function view_details($school_bell_id = null)
    {
        $data = array();
        $data['title'] = 'Configuration View Details';
        $data['heading_msg'] = "Configuration View Details";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['schedule_list'] = $this->db->query("SELECT * FROM `tbl_school_bell_schedule` where school_bell_id = '$school_bell_id'")->result_array();
        $data['maincontent'] = $this->load->view('bell_managements/bell_schedule_info_details', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function schedule_data_delete($id)
    {
        $this->db->delete('tbl_school_bell_schedule', array('id' => $id));
        $sdata['message'] = "Schedule Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("bell_managements/index");
    }

    public function schedule_data_edit($id)
    {
        if ($_POST) {
            $date = $this->input->post('date', true);
            $time = $this->input->post('bell_time', true);
            $data = array();
            $data['id'] = $id;
            $data['bell_time'] = date('Y-m-d H:i:s', strtotime("$date $time"));
            $data['bell_type'] = $this->input->post('bell_type', true);
            $this->db->where('id', $id);
            $this->db->update('tbl_school_bell_schedule', $data);
            $sdata['message'] = "Data Updated Successfully.";
            $this->session->set_userdata($sdata);
            redirect('bell_managements/index');
        } else {
            $data = array();
            $data['title'] = 'Schedule Update';
            $data['heading_msg'] = "Schedule Update";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['schedule_info'] = $this->db->query("SELECT * FROM `tbl_school_bell_schedule` where id = '$id'")->result_array();
            $data['maincontent'] = $this->load->view('bell_managements/schedule_info_update', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function add_schedule_data()
    {
        if ($_POST) {
            //echo '<pre>';
            //print_r($_POST);
            //die;
            $from_date = $this->input->post('from_date', true);
            $to_date = $this->input->post('to_date', true);
            //echo $from_date.'/'.date('Y-m-d');
            //die;
            if ($from_date <= date('Y-m-d')) {
                $sdata['exception'] = "Past Date Not Allowed for Configuration ! ";
                $this->session->set_userdata($sdata);
                redirect("bell_managements/index");
            }


            $days  =  $this->input->post('for_day', true);


            $data2 = array();
            $data2['from_date'] = $this->input->post('from_date', true);
            $data2['to_date'] = $this->input->post('to_date', true);
            $data2['for_day'] = implode(",", $this->input->post('for_day', true));
            $this->db->insert("tbl_school_bell", $data2);
            $insert_id = $this->db->insert_id();

            while (strtotime($from_date) <= strtotime($to_date)) {
                if (in_array(strtolower(date("D", strtotime($from_date))), $days)) {
                    //echo 	strtolower(date("D", strtotime($from_date)));
                    //echo $from_date;
                    // die;
                    for ($i = 1; $i <= 20; $i++) {
                        $bell_type = $this->input->post('bell_type_'.$i, true);
                        //echo $bell_type; die;
                        if ($bell_type != 'no') {
                            $data = array();
                            $data['date'] = $from_date;
                            $data['school_bell_id'] = $insert_id;
                            $data['for_day'] = strtolower(date("D", strtotime($from_date)));
                            $time = $this->input->post('bell_time_'.$i, true);
                            //echo  $time.'/'.date('Y-m-d H:i:s', strtotime("$from_date $time"));
                            // die;
                            $data['bell_time'] = date('Y-m-d H:i:s', strtotime("$from_date $time"));
                            $data['bell_type'] = $this->input->post('bell_type_'.$i, true);
                            $data['is_completed'] = 0;
                            $this->db->insert("tbl_school_bell_schedule", $data);
                        }
                    }
                }
                $from_date = date('Y-m-d', strtotime($from_date . "+1 days"));
            }

            $sdata['message'] = "You are Successfully Data Added ! ";
            $this->session->set_userdata($sdata);
            redirect("bell_managements/index");
        } else {
            $data = array();
            $data['title'] = 'Schedule Add';
            $data['heading_msg'] = "Schedule Add";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('bell_managements/schedule_info_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
