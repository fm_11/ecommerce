<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Student_report_absentees extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Students_report', 'Student', 'Timekeeping'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Student Absentees List';
		$data['heading_msg'] = "Student Absentees List";
		if ($_POST) {
			$group_id = $this->input->post("group_id");
			$period_id = $this->input->post("period_id");
			$date = $this->input->post("date");
			$data['date'] = $date;
			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];
			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['group_id'] = $group_id;
			$data['period_id'] = $period_id;

			$check_holiday = $this->Timekeeping->check_holiday($date);
			if(!empty($check_holiday)){
				$sdata['exception'] = $date . ", this date are holiday !";
				$this->session->set_userdata($sdata);
				redirect("student_report_absentees/index");
			}

			$data['absentee_info'] = $this->Timekeeping->getStudentReportAbsentees($date, $class_id, $shift_id, $section_id, $group_id, $period_id);

			$data['period_name'] = $this->Students_report->get_period_name($period_id);
			$data['class_name'] =$this->Students_report->get_class_name($class_id);
			$data['section_name'] =$this->Students_report->get_section_name($section_id);
			$data['shift_name'] =$this->Students_report->get_shift_name($shift_id);
			$data['group_name'] = $this->Students_report->get_group_name($group_id);


			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;
			$data['report'] = $this->load->view('student_report_absentees/report', $data, true);
		}

		if (isset($_POST['pdf_download'])) {
			$data['is_pdf'] = 1;
			//Dom PDF
			$this->load->library('mydompdf');
			$html = $this->load->view('student_report_absentees/report', $data, true);
			$this->mydompdf->createPDF($html, 'student_report_absentees', true, 'A4', 'portrait');
			//Dom PDF
		}
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['GroupList'] = $this->Admin_login->getGroupList();
		$data['period_list'] = $this->db->query("SELECT * FROM `tbl_class_period`")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('student_report_absentees/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}
