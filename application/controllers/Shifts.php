<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Shifts extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Shift Information';
        $data['heading_msg'] = "Shift Information";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['shift'] = $this->db->query("SELECT * FROM tbl_shift")->result_array();
        $data['maincontent'] = $this->load->view('shifts/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['login_time'] = $_POST['login_time'];
            $data['logout_time'] = $_POST['logout_time'];
            $data['flexible_late_minute'] = $_POST['flexible_late_minute'];
            $this->db->insert("tbl_shift", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('shifts/index');
        }
        $data = array();
        $data['title'] = 'Add Shift Information';
        $data['heading_msg'] = "Add Shift Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('shifts/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['login_time'] = $_POST['login_time'];
            $data['logout_time'] = $_POST['logout_time'];
            $data['flexible_late_minute'] = $_POST['flexible_late_minute'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_shift', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('shifts/index');
        }
        $data = array();
        $data['title'] = 'Update Shift Information';
        $data['heading_msg'] = "Update Shift Information";
        $data['is_show_button'] = "index";
        $data['shift'] = $this->db->query("SELECT * FROM `tbl_shift` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('shifts/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
        $student_info = $this->db->where('shift_id', $id)->get('tbl_student')->result_array();
        if (!empty($student_info)) {
            $sdata['exception'] = $this->lang->line('delete_error_message') . " (Student Depended Data Found !)";
            $this->session->set_userdata($sdata);
            redirect('shifts/index');
        }
        $this->db->delete('tbl_shift', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_sucess_message');
        $this->session->set_userdata($sdata);
        redirect('shifts/index');
    }
}
