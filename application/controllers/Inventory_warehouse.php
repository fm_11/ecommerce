<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Inventory_warehouse extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('warehouse').' '.$this->lang->line('list');
        $data['heading_msg'] = $this->lang->line('warehouse').' '.$this->lang->line('list');
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['warehouse'] = $this->db->query("SELECT * FROM inventory_ware_house")->result_array();
        $data['maincontent'] = $this->load->view('inventory_warehouse/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['location'] = $_POST['location'];
            $this->db->insert("inventory_ware_house", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('inventory_warehouse/index');
        }
        $data = array();
        $data['title'] = $this->lang->line('warehouse').' '.$this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('warehouse').' '.$this->lang->line('add');
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('inventory_warehouse/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['location'] = $_POST['location'];
            $this->db->where('id', $data['id']);
            $this->db->update('inventory_ware_house', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('inventory_warehouse/index');
        }
        $data = array();
        $data['title'] = $this->lang->line('warehouse').' '.$this->lang->line('edit');
        $data['heading_msg'] = $this->lang->line('warehouse').' '.$this->lang->line('edit');
        $data['is_show_button'] = "index";
        $data['warehouse'] = $this->db->query("SELECT * FROM `inventory_ware_house` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('inventory_warehouse/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
        $this->db->delete('inventory_ware_house', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('inventory_warehouse/index');
    }
}
