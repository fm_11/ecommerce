<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Exam_types extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Exam Type';
        $data['heading_msg'] = "Exam Type";
        $data['is_show_button'] = "add";
        $data['exam_types'] = $this->db->query("SELECT * FROM tbl_exam_type")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/exam_type_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['type'] = $_POST['type'];
            $this->db->insert("tbl_exam_type", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('exam_types/add');
        }
        $data = array();
        $data['title'] = 'Add Exam Type';
        $data['heading_msg'] = "Add Exam Type";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/exam_type_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $exam_info = $this->db->where('exam_type_id', $id)->get('tbl_exam')->result_array();
        if (!empty($exam_info)) {
            $sdata['exception'] = "Exam Depended Data Found !";
            $this->session->set_userdata($sdata);
            redirect("exam_types/index");
        }
        $this->db->query("DELETE FROM tbl_exam_type WHERE `id` = '$id'");
        $sdata['message'] = "Exam Type Deleted Successfully.";
        $this->session->set_userdata($sdata);
        redirect('exam_types/index');
    }
}
