

<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Resident_student_reports extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login', 'Student'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = "Resident/Non-Resident Student";
		$data['heading_msg'] = "Resident/Non-Resident Student";
		if ($_POST) {
			$is_resident = $this->input->post("is_resident");
			if ($is_resident == "Y") {
				$title_value = "Resident";
			} else {
				$title_value = "Non-Resident";
			}
		} else {
			$title_value = "Resident/Non-Resident";
		}
		$data['title'] = $title_value . ' Student List';
		$data['heading_msg'] = $title_value . " Student List";

		if ($_POST) {
			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$Info = array();
			$Info['school_name'] = $SchoolInfo[0]['school_name'];
			$Info['address'] = $SchoolInfo[0]['address'];
			$data['HeaderInfo'] = $Info;

			$is_resident = $this->input->post("is_resident");
			$year = $this->input->post("year");
			$class_shift_section_id = $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$group_id = $this->input->post("group_id");

			$data['year'] = $year;
			$data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['group_id'] = $group_id;
			$data['shift_id'] = $shift_id;
			$data['is_resident'] = $is_resident;
			$data['class_shift_section_id'] = $class_shift_section_id;

			$class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
			$data['class_name'] = $class_info[0]['name'];

			$section_info = $this->db->query("SELECT name FROM `tbl_section` WHERE id ='$section_id'")->result_array();
			$data['section_name'] = $section_info[0]['name'];

			$group_info = $this->db->query("SELECT name FROM `tbl_student_group` WHERE id ='$group_id'")->result_array();
			$data['group_name'] = $group_info[0]['name'];

			$shift_info = $this->db->query("SELECT name FROM `tbl_shift` WHERE id ='$shift_id'")->result_array();
			$data['shift_name'] = $shift_info[0]['name'];

			if ($is_resident == 'Y') {
				$data['idata'] = $this->db->query("SELECT r.*,s.`name`,s.`roll_no`,s.`student_code`,s.`guardian_mobile` FROM `tbl_student_resident_info` AS r
INNER JOIN `tbl_student` AS s ON r.`student_id` = s.`id`
WHERE r.`class_id` = '$class_id' AND r.`section_id` = '$section_id' AND r.`group` = '$group_id' AND r.`shift_id` = '$shift_id' AND r.`year` = '$year'")->result_array();
			} else {
				$data['idata'] = $this->db->query("SELECT s.`name`,s.`roll_no`,s.`student_code`,s.`guardian_mobile` FROM `tbl_student` AS s
WHERE (s.`id` NOT IN (SELECT r.`id` FROM `tbl_student_resident_info` AS r
WHERE r.`class_id` = '$class_id' AND r.`section_id` = '$section_id' AND r.`group` = '$group_id' AND r.`shift_id` = '$shift_id' AND r.`year` = '$year'))
AND s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group_id' AND s.`shift_id` = '$shift_id';")->result_array();
			}

			$data['report'] = $this->load->view('resident_student_reports/report_table', $data, true);
		}
		if (isset($_POST['pdf_download'])) {
			$data['is_pdf'] = 1;
			//Dom PDF
			$this->load->library('mydompdf');
			$html = $this->load->view('resident_student_reports/report_table', $data, true);
			$this->mydompdf->createPDF($html, 'Resident/Non-ResidentStudentReport', true, 'A4', 'portrait');
			//Dom PDF
		} else {
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['groups'] = $this->Admin_login->getGroupList();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('resident_student_reports/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}
}
