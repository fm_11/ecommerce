<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Teacher_staff_report_absentees extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Students_report', 'Student', 'Timekeeping'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Teacher/Staff Absentees List';
		$data['heading_msg'] = "Teacher/Staff Absentees List";
		if ($_POST) {
			$date = $this->input->post("date");
			$data['date'] = $date;

			$check_holiday = $this->Timekeeping->check_holiday($date);
			if(!empty($check_holiday)){
				$sdata['exception'] = $date . ", this date are holiday !";
				$this->session->set_userdata($sdata);
				redirect("teacher_staff_report_absentees/index");
			}

			$cond = array();
			$cond['date'] = $date;
			$data['absentee_info'] = $this->Timekeeping->get_teacher_staff_report_absentee($date);

			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;
			$data['report'] = $this->load->view('teacher_staff_report_absentees/report', $data, true);
		}

		if (isset($_POST['pdf_download'])) {
			$data['is_pdf'] = 1;
			//Dom PDF
			$this->load->library('mydompdf');
			$html = $this->load->view('teacher_staff_report_absentees/report', $data, true);
			$this->mydompdf->createPDF($html, 'TeacherStaffReportAbsentees', true, 'A4', 'portrait');
			//Dom PDF
		}
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('teacher_staff_report_absentees/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}
