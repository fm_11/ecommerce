<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Store_management extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('StoreManagement'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Inventory Management';
        $data['heading_msg'] = "Store Management";
        //$data['top_menu'] = $this->load->view('student_fees/student_fee_menu', '', true);
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function vendors_index()
    {
        $data = array();
        $data['title'] = 'Vendors';
        $data['heading_msg'] = "Vendors";
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['vendors_info'] = $this->db->query("SELECT * FROM inventory_vendors")->result_array();
        $data['maincontent'] = $this->load->view('storeManagement/vendors_info_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add_vendors()
    {
        if ($_POST) {
            $data = array();
            $data['company_name'] = $_POST['company_name'];
            $data['company_phone'] = $_POST['company_phone'];
            $data['company_email'] = $_POST['company_email'];
            $data['country'] = $_POST['country'];
            $data['status'] = $_POST['status'];
            $data['city'] = $_POST['city'];
            $data['contact_person_name'] = $_POST['contact_person_name'];
            $data['contact_person_address'] = $_POST['contact_person_address'];
            $data['contact_person_phone'] = $_POST['contact_person_phone'];
            $data['bank_name'] = $_POST['contact_person_bank_name'];
            $data['branch_name'] = $_POST['contact_person_bank_branch_name'];
            $data['account_no'] = $_POST['contact_person_bank_account_no'];
            $data['IFSC_code'] = $_POST['IFSC_code'];
            $return = $this->StoreManagement->addDataInTable("inventory_vendors", $data);
            if ($return) {
                $sdata['message'] = "Data Added Successfully.";
            } else {
                $sdata['message'] = "Error";
            }

            $this->session->set_userdata($sdata);
            redirect('/Store_management/vendors_index');
        }
        $data = array();
        $data['title'] = 'Add Vendors';
        $data['heading_msg'] = "Add Vendors";
        $data['action'] = '';
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['maincontent'] = $this->load->view('storeManagement/vendore_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function inventory_category_Index()
    {
        $data = array();
        $data['title'] = 'Category';
        $data['heading_msg'] = "Category";
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['category_info'] = $this->db->query("SELECT * FROM inventory_category")->result_array();
        $data['maincontent'] = $this->load->view('storeManagement/category_info_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add_category()
    {
        if ($_POST) {
            $data = array();
            $data['code'] = $_POST['code'];
            $data['category_name'] = $_POST['category_name'];
            $return = $this->StoreManagement->addDataInTable("inventory_category", $data);
            if ($return) {
                $sdata['message'] = "Data Added Successfully.";
            } else {
                $sdata['exception'] = "Data Doesn't Added";
            }

            $this->session->set_userdata($sdata);
            redirect('/Store_management/inventory_category_Index');
        }
        $data = array();
        $data['title'] = 'Add Category';
        $data['heading_msg'] = "Add Category";
        $data['action'] = '';
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['maincontent'] = $this->load->view('storeManagement/category_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function inventory_sub_category_Index()
    {
        $data = array();
        $data['title'] = 'Sub Category';
        $data['heading_msg'] = "Sub Category";
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['sub_category_info'] = $this->db->query("SELECT s.*,c.`category_name`,c.`code` AS category_code FROM `inventory_sub_category` AS s
LEFT JOIN `inventory_category` AS c ON s.`category_id` = c.`id`")->result_array();
        $data['maincontent'] = $this->load->view('storeManagement/sub_category_info_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add_sub_category()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['code'] = $_POST['code'];
            $data['category_id'] = $_POST['category_id'];
            $return = $this->StoreManagement->addDataInTable("inventory_sub_category", $data);
            if ($return) {
                $sdata['message'] = "Data Added Successfully.";
            } else {
                $sdata['exception'] = "Data Doesn't Added";
            }

            $this->session->set_userdata($sdata);
            redirect('/Store_management/inventory_sub_category_Index');
        }
        $data = array();
        $data['title'] = 'Add Sub Category';
        $data['heading_msg'] = "Add Sub Category";
        $data['action'] = '';
        $data['category_info'] = $this->db->query("SELECT * FROM inventory_category")->result_array();
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['maincontent'] = $this->load->view('storeManagement/sub_category_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function inventory_ware_house_Index()
    {
        $data = array();
        $data['title'] = 'Warehouse Information';
        $data['heading_msg'] = "Warehouse Information";
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['warehouse_info'] = $this->db->query("SELECT * FROM `inventory_ware_house`")->result_array();
        $data['maincontent'] = $this->load->view('storeManagement/warehouse_info_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function add_warehouse_info()
    {
        if ($_POST) {
            $data = array();
            $data['location'] = $_POST['location'];
            $data['name'] = $_POST['name'];
            $return = $this->StoreManagement->addDataInTable("inventory_ware_house", $data);
            if ($return) {
                $sdata['message'] = "Data Added Successfully.";
            } else {
                $sdata['exception'] = "Data Doesn't Added";
            }

            $this->session->set_userdata($sdata);
            redirect('/Store_management/inventory_ware_house_Index');
        }
        $data = array();
        $data['title'] = 'Add Category';
        $data['heading_msg'] = "Add Category";
        $data['action'] = '';
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['maincontent'] = $this->load->view('storeManagement/warehouse_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function inventory_item_index()
    {
        $data = array();
        $data['title'] = 'Inventory Item';
        $data['heading_msg'] = "Inventory Item";
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['inventory_item_info'] = $this->db->query("SELECT i.`id`,i.`quantity`,i.`name`,c.`category_name`,s.`name` AS sub_category_name,
CONCAT(w.`name`, '(', w.`location`, ')') AS warehouse, u.`unit_name`
FROM `inventory_item` AS i 
LEFT JOIN `inventory_category` AS c ON c.`id` = i.`category_id`
LEFT JOIN `inventory_sub_category` AS s ON s.`id` = i.`sub_category_id`
LEFT JOIN `inventory_ware_house` AS w ON w.`id` = i.`ware_house_id`
LEFT JOIN `item_unit` AS u ON u.`id` = i.`unit_id`")->result_array();
        $data['maincontent'] = $this->load->view('storeManagement/inventory_item_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);

    }

    function inventory_item_in_index(){
        $data = array();
        $data['title'] = 'Inventory Item In';
        $data['heading_msg'] = "Inventory Item In";
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['item_info'] = $this->db->query("SELECT ii.`name`,i.`id`,i.`quantity`,i.`warrenty_from`,i.`warrenty_to`,
CONCAT(w.`name`, '(', w.`location`, ')') AS warehouse, u.`unit_name`
FROM `inventory_item_in` AS i
LEFT JOIN `inventory_item` AS ii ON ii.`id` = i.`item_id`
LEFT JOIN `inventory_vendors` AS v ON v.`id` = i.`vendor_id`
LEFT JOIN `inventory_ware_house` AS w ON w.`id` = i.`ware_house_id`
LEFT JOIN `item_unit` AS u ON u.`id` = i.`unit_id`")->result_array();
        $data['maincontent'] = $this->load->view('storeManagement/inventory_item_in_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function getSubCategoryByCategoryId(){
        $category_id = $this->input->get('category_id', true);
        $data = array();
        $data['sub_category'] = $this->db->query("SELECT id,name,code FROM inventory_sub_category WHERE `category_id` = '$category_id'")->result_array();
        $this->load->view('storeManagement/sub_category_list_for_combo', $data);
    }

    function getItemBySubCategoryId(){
        $sub_category_id = $this->input->get('sub_category_id', true);
        $data = array();
        $data['inventory_item'] = $this->db->query("SELECT id,name,quantity FROM inventory_item WHERE `sub_category_id` = '$sub_category_id'")->result_array();
        $this->load->view('storeManagement/inventory_item_list_for_combo', $data);
    }

    public function add_inventory_item()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['category_id'] = $_POST['category_id'];
            $data['sub_category_id'] = $_POST['sub_category_id'];
            $data['ware_house_id'] = $_POST['ware_house_id'];
            $data['quantity'] = $_POST['quantity'];
            $data['unit_id'] = $_POST['unit_id'];
            $data['date'] = date('Y-m-d');
            $return = $this->StoreManagement->addDataInTable("inventory_item", $data);
            if ($return) {
                $sdata['message'] = "Data Added Successfully.";
            } else {
                $sdata['message'] = "Error";
            }
            $this->session->set_userdata($sdata);
            redirect('Store_management/inventory_item_index');
        }
        $data = array();
        $data['title'] = 'Add Inventory Item';
        $data['heading_msg'] = "Add Inventory Item";
        $data['action'] = '';
        $data['category_info'] = $this->db->query("SELECT * FROM inventory_category")->result_array();
        $data['sub_category_info'] = $this->db->query("SELECT * FROM inventory_sub_category")->result_array();
        $data['ware_house'] = $this->db->query("SELECT * FROM inventory_ware_house")->result_array();
        $data['item_unit'] = $this->db->query("SELECT * FROM item_unit")->result_array();
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['maincontent'] = $this->load->view('storeManagement/inventory_item_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    function add_inventory_item_in(){
        if ($_POST) {
            $data = array();
            $data['item_id'] = $_POST['item_id'];
            $data['category_id'] = $_POST['category_id'];
            $data['sub_category_id'] = $_POST['sub_category_id'];
            $data['ware_house_id'] = $_POST['ware_house_id'];
            $data['vendor_id'] = $_POST['vendor_id'];
            $data['quantity'] = $_POST['quantity'];
            $data['price'] = $_POST['price'];
            $data['unit_id'] = $_POST['unit_id'];
            $data['warrenty_from'] = $_POST['warrenty_from'];
            $data['warrenty_to'] = $_POST['warrenty_to'];
            $data['remarks'] = $_POST['remarks'];
            $data['date'] = date('Y-m-d');
            $return = $this->StoreManagement->addDataInTable("inventory_item_in", $data);
            if ($return) {
                $sdata['message'] = "Data Added Successfully.";
            } else {
                $sdata['message'] = "Error";
            }
            $this->session->set_userdata($sdata);
            redirect('Store_management/inventory_item_in_index');
        }
        $data = array();
        $data['title'] = 'In a Inventory Item';
        $data['heading_msg'] = "In a Inventory Item";
        $data['action'] = '';
        $data['category_info'] = $this->db->query("SELECT * FROM inventory_category")->result_array();
        $data['vendor_info'] = $this->db->query("SELECT * FROM inventory_vendors")->result_array();
        $data['ware_house'] = $this->db->query("SELECT * FROM inventory_ware_house")->result_array();
        $data['item_unit'] = $this->db->query("SELECT * FROM item_unit")->result_array();
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['maincontent'] = $this->load->view('storeManagement/in_a_inventory_item', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function inventory_scrapped_goods_index(){
        $data = array();
        $data['title'] = 'Inventory Scrapped Goods';
        $data['heading_msg'] = "Inventory Scrapped Goods";
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['scrapped_goods_info'] = $this->db->query("SELECT i.*,ii.`name`,u.`unit_name`
FROM `inventory_scrapped_goods` AS i
LEFT JOIN `inventory_item` AS ii ON ii.`id` = i.`item_id`
LEFT JOIN `item_unit` AS u ON u.`id` = i.`unit_id`")->result_array();
        $data['maincontent'] = $this->load->view('storeManagement/inventory_scrapped_goods_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function add_scrapped_item(){
        if ($_POST) {
            $data = array();
            $data['item_id'] = $_POST['item_id'];
            $data['category_id'] = $_POST['category_id'];
            $data['sub_category_id'] = $_POST['sub_category_id'];
            $data['quantity'] = $_POST['quantity'];
            $data['unit_id'] = $_POST['unit_id'];
            $data['scrapped_date'] = $_POST['scrapped_date'];
            $data['reason'] = $_POST['reason'];
            $data['date'] = date('Y-m-d');
            $return = $this->StoreManagement->addDataInTable("inventory_scrapped_goods", $data);
            if ($return) {
                $sdata['message'] = "Data Added Successfully.";
            } else {
                $sdata['message'] = "Error";
            }
            $this->session->set_userdata($sdata);
            redirect('Store_management/inventory_scrapped_goods_index');
        }
        $data = array();
        $data['title'] = 'In a Inventory Item';
        $data['heading_msg'] = "In a Inventory Item";
        $data['action'] = '';
        $data['category_info'] = $this->db->query("SELECT * FROM inventory_category")->result_array();
        $data['ware_house'] = $this->db->query("SELECT * FROM inventory_ware_house")->result_array();
        $data['item_unit'] = $this->db->query("SELECT * FROM item_unit")->result_array();
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['maincontent'] = $this->load->view('storeManagement/scrapped_item_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function stock_register_index(){
        $data = array();
        $data['title'] = 'Stock Register';
        $data['heading_msg'] = "Stock Register";
        $data['top_menu'] = $this->load->view('storeManagement/store_management_menu', '', true);
        $data['scrapped_goods_info'] = $this->db->query("SELECT i.`id`,i.`name`,c.`category_name`,sc.`name` AS sub_category_name,i.`quantity` AS opening_quantity,
(SELECT SUM(ii.`quantity`) FROM `inventory_item_in` AS ii WHERE i.`id` = ii.`item_id` GROUP BY ii.`item_id`) AS in_quantity,
(SELECT SUM(isg.`quantity`) FROM `inventory_scrapped_goods` AS isg WHERE isg.`item_id` = i.`id` GROUP BY isg.`item_id`) AS scrapped_goods_quantity
FROM `inventory_item` AS i
LEFT JOIN `inventory_category` AS c ON c.`id` = i.`category_id`
LEFT JOIN `inventory_sub_category` AS sc ON sc.`id` = i.`sub_category_id`")->result_array();
        $data['maincontent'] = $this->load->view('storeManagement/stock_register', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


}
