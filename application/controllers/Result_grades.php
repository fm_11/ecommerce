<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Result_grades extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('grade_point') . ' ' . $this->lang->line('configuration');
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['maincontent'] = $this->load->view('result_grades/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function add()
    {
        $data = array();
        $data['title'] =  $this->lang->line('grade_point') . ' ' . $this->lang->line('configuration');
        $data['heading_msg'] =  $this->lang->line('grade_point') . ' ' . $this->lang->line('configuration');
        $data['is_show_button'] = "index";
        $data['classes'] = $this->Admin_login->all_class();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('result_grades/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function data_save($action_type)
    {
        if ($_POST) {
            $class_array = $_POST['class_id'];
            if ($action_type == 'a') {
                $total_class = count($_POST['class_id']);
            } else {
                $total_class = 1;
            }

            $total_grade_row = $_POST['total_submited_row'];


            $i = 0;
            while ($i < $total_class) {
                //echo $action_type;
                //die;
                if ($action_type == 'a') {
                    $class_id = $class_array[$i];
                } else {
                    $class_id = $_POST['class_id'];
                }


                $this->db->delete('tbl_result_grade_setup', array('class_id' => $class_id));


                $grade_data = array();
                $j = 0;
                while ($j < $total_grade_row) {
                    $grade_data[$j]['class_id'] = $class_id;
                    $grade_data[$j]['start_range'] = $_POST['start_range_'. $j];
                    $grade_data[$j]['end_range'] = $_POST['end_range_'. $j];
                    $grade_data[$j]['numeric_gpa'] =  $_POST['numeric_gpa_'. $j];
                    $grade_data[$j]['alpha_gpa'] =  $_POST['alpha_gpa_'. $j];
                    $j++;
                }
                //  echo '<pre>';
                //  print_r($grade_data);


                $this->db->insert_batch('tbl_result_grade_setup', $grade_data);
                //  die;

                $i++;
            }




            //die;
            if ($action_type == 'a') {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect('result_grades/add');
            } else {
                $sdata['message'] = $this->lang->line('edit_success_message');
                $this->session->set_userdata($sdata);
                redirect('result_grades/index');
            }
        }
    }

    public function edit($class_id)
    {
        $check_data = $this->db->query("SELECT * FROM tbl_result_grade_setup where class_id = $class_id  ORDER BY `start_range` DESC")->result_array();
        if (empty($check_data)) {
            $sdata['exception'] = $this->lang->line('edit_error_message') . ' (No data found in this class, please add)';
            $this->session->set_userdata($sdata);
            redirect('result_grades/add');
        }
        $data = array();
        $data['title'] =  $this->lang->line('grade_point') . ' ' . $this->lang->line('configuration');
        $data['heading_msg'] =  $this->lang->line('grade_point') . ' ' . $this->lang->line('configuration');
        $data['is_show_button'] = "index";
        $data['grade_data'] = $check_data;
        $data['classes'] = $this->Admin_login->all_class();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('result_grades/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($class_id)
    {
        $this->db->delete('tbl_result_grade_setup', array('class_id' => $class_id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("result_grades/index");
    }
}
