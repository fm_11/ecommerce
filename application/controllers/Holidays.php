<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Holidays extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Timekeeping', 'Admin_login'));
		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$cond = array();
		if($_POST){
			$holiday_type_id = $this->input->post("holiday_type_id");
			$from_date = $this->input->post("from_date");
			$to_date = $this->input->post("to_date");

			$cond['holiday_type_id'] = $holiday_type_id;
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;

			$sdata['holiday_type_id'] = $holiday_type_id;
			$sdata['from_date'] = $from_date;
			$sdata['to_date'] = $to_date;
			$this->session->set_userdata($sdata);
		}else{
			$holiday_type_id = $this->session->userdata('holiday_type_id');
			$from_date = $this->session->userdata('from_date');
			$to_date = $this->session->userdata('to_date');

			$cond['holiday_type_id'] = $holiday_type_id;
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
		}
		$data['title'] = 'Holiday';
		$data['heading_msg'] = "Holiday";
		$this->load->library('pagination');
		$config['base_url'] = site_url('holidays/index/');
		$config['per_page'] = 20;
		$data['is_show_button'] = "add";
		$config['total_rows'] = count($this->Timekeeping->get_all_holiday_list(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['holidays'] = $this->Timekeeping->get_all_holiday_list(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['holiday_types'] = $this->db->query("SELECT * FROM `tbl_holiday_type`")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('holidays/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}


	public function add()
	{
		if ($_POST) {
			$date = $this->input->post("txtDate");
			$is_already_holiday = $this->db->query("SELECT id FROM tbl_holiday WHERE date = '$date'")->result_array();
			if (!empty($is_already_holiday)) {
				$sdata['exception'] = "Already added this date for holiday!";
				$this->session->set_userdata($sdata);
				redirect("holidays/add");
			}
			$data = array();
			$data['holiday_type'] = $this->input->post("txtHolidayType");
			$data['date'] = $date;
			$data['remarks'] = $this->input->post("txtRemarks");
			$this->db->insert('tbl_holiday', $data);
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect('holidays/add');
		} else {
			$data = array();
			$data['title'] = 'Holiday';
			$date = date('Y-m-d');
			$data['date'] = $date;
			$data['is_show_button'] = "index";
			$data['heading_msg'] = "Holiday Add";
			$data['holiday_types'] = $this->db->query("SELECT * FROM `tbl_holiday_type`")->result_array();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('holidays/add', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

	public function edit($id = null)
	{
		if ($_POST) {
			$data = array();
			$data['id'] = $this->input->post('id', true);
			$data['holiday_type'] = $this->input->post("txtHolidayType");
			$data['date'] = $this->input->post("txtDate");
			$data['remarks'] = $this->input->post("txtRemarks");
			$this->db->where('id', $data['id']);
			$this->db->update('tbl_holiday', $data);
			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect("holidays/index");
		} else {
			$data = array();
			$data['title'] = 'Holiday';
			$data['heading_msg'] = "Holiday";
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['is_show_button'] = "index";
			$data['holiday_types'] = $this->db->query("SELECT * FROM `tbl_holiday_type`")->result_array();
			$data['holiday_info'] = $this->db->query("SELECT * FROM tbl_holiday WHERE id='$id'")->result_array();
			$data['maincontent'] = $this->load->view('holidays/edit', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

	public function delete($id)
	{
		$this->db->delete('tbl_holiday', array('id' => $id));
		$sdata['message'] = $this->lang->line('delete_success_message');
		$this->session->set_userdata($sdata);
		redirect("holidays/index");
	}

	public function batch_add_data_save()
	{
		if ($_POST) {
			//echo '<pre>';
			// print_r($_POST);
			// die;
			$year = $this->input->post("year");
			$month = $this->input->post("month");
			$num_of_days = $this->input->post("num_of_days");

			$this->db->query("DELETE FROM `tbl_holiday` WHERE MONTH(`date`) = '$month' AND YEAR(`date`) = '$year'");

			//echo '<pre>';
			// print_r($tt);
			//die;

			$i = 1;
			while ($i <= $num_of_days) {
				if ($this->input->post("is_allow_" . $i)) {
					$data = array();
					$data['holiday_type'] = $this->input->post("txtHolidayType_" . $i);
					$data['date'] = $this->input->post("date_" . $i);
					$data['remarks'] = "Batch Holiday Add";
					$this->db->insert('tbl_holiday', $data);
				}
				$i++;
			}
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect("holidays/index");
		}
	}

	public function batch_add()
	{
		$data = array();
		$data['holiday_types'] = $this->db->query("SELECT * FROM `tbl_holiday_type`")->result_array();
		if ($_POST) {
			$data['month'] = $this->input->post("month");
			$data['year'] = $this->input->post("year");
			$month = $data['month'];
			$year = $data['year'];
			$data['num_of_days'] = cal_days_in_month(CAL_GREGORIAN, $data['month'], $data['year']);
			// echo $data['num_of_days']; die;
			$data['holidays'] = $this->db->query("SELECT * FROM `tbl_holiday` WHERE
                              MONTH(`date`) = '$month' AND YEAR(`date`) = '$year'")->result_array();
			//echo '<pre>';
			//print_r($data['holidays']);
			// die;
			$data['process_table'] = $this->load->view('holidays/holiday_batch_add_form_table', $data, true);
		}
		$data['title'] = 'Holiday';
		$data['is_show_button'] = "index";
		$data['heading_msg'] = "Holiday";
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('holidays/holiday_batch_add_form', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}



}
