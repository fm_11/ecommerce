<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class President_messages extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

public function index()
{
    $data = array();
    $data['title'] = 'President Message';
    $data['heading_msg'] = "President Message";
    $data['message'] = $this->db->where('id', '1')->get('tbl_president_message')->row();
	$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
    $data['maincontent'] = $this->load->view('president_messages/index', $data, true);
    $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
}

public function edit()
{
    $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
    $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
    $sContent = strip_tags(stripslashes($_POST['txtMessage']), $allowedTags);

    $file_name = $_FILES['txtPhoto']['name'];
    if ($file_name != '') {
        $president_photo_info = $this->db->where('id', '1')->get('tbl_president_message')->result_array();
        $old_file = $president_photo_info[0]['location'];
        if (!empty($old_file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/headteacher/' . $old_file;
            unlink($filedel);
        }
        $this->load->library('upload');
        $config['upload_path'] = MEDIA_FOLDER . '/headteacher/';
        $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $this->upload->initialize($config);
        $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
        foreach ($_FILES as $field => $file) {
            if ($file['error'] == 0) {
                $new_photo_name = "president_" . time() . "_" . date('Y-m-d') . "." . $extension;
                if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/headteacher/" . $new_photo_name)) {
                    $data = array();
                    $data['id'] = $this->input->post('id', true);
                    $data['title'] = $this->input->post('txtTitle', true);
                    $data['short_message'] = $sContent;
                    $data['long_message'] = $sContent;
                    $data['location'] = $new_photo_name;
                    $this->db->where('id', $data['id']);
                    $this->db->update('tbl_president_message', $data);
                    $sdata['message'] = $this->lang->line('edit_success_message');
                    $this->session->set_userdata($sdata);
                    redirect("president_messages/index");
                } else {
                    $sdata['exception'] = "Sorry President Message Doesn't Updated !" . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("president_messages/edit");
                }
            } else {
                $sdata['exception'] = "Sorry President Message Doesn't Updated  !" . $this->upload->display_errors();
                $this->session->set_userdata($sdata);
                redirect("president_messages/edit");
            }
        }
    } else {
        $data = array();
        $data['id'] = $this->input->post('id', true);
        $data['title'] = $this->input->post('txtTitle', true);
        $data['short_message'] = $sContent;
        $data['long_message'] = $sContent;
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_president_message', $data);
        $sdata['message'] = $this->lang->line('edit_success_message');
        $this->session->set_userdata($sdata);
        redirect("president_messages/index");
    }
}

}

?>
