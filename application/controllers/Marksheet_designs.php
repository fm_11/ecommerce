
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Marksheet_designs extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('common/insert_model', 'common/custom_methods_model','Admin_login'));
		$this->load->library('session');
		date_default_timezone_set('Asia/Dhaka');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}


	public function index()
	{
		if ($_POST) {
			$year = $this->input->post("year");
			$class_id = $this->input->post("class_id");
			$type = $this->input->post("type");
			$this->db->delete('tbl_marksheet_design', array('class_id' => $class_id, 'year' => $year, 'type' => $type));

			$data = array();
			$data['year'] = $year;
			$data['class_id'] = $class_id;
			$data['show_student_image'] = $_POST['show_student_image'];
			$data['marksheet_header'] = $_POST['marksheet_header'];
			$data['show_watermark'] = $_POST['show_watermark'];
			$data['merit_position'] = $_POST['merit_position'];
			$data['type'] = $_POST['type'];
			$data['template_category'] = $_POST['template_category'];
			$this->db->insert("tbl_marksheet_design", $data);

			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect('marksheet_designs/index');
		}
		$data = array();
		$data['title'] = "Marksheet Design";
		$data['heading_msg'] = "Marksheet Design";
		$data['is_show_button'] = "index";
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['class'] = $this->db->query("SELECT * FROM tbl_class ORDER BY student_code_short_form")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('marksheet_designs/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}


	public function get_configuration_by_year_class()
	{
		$data = array();
		$data['year'] = $this->input->get('year', true);
		$data['class_id'] = $this->input->get('class_id', true);
		$data['type'] = $this->input->get('type', true);
		$class_id = $data['class_id'];
		$year = $data['year'];
		$type = $data['type'];
		$data['configuration_data'] = $this->db->query("SELECT * FROM `tbl_marksheet_design`
         WHERE `class_id` = '$class_id' AND `year` = '$year' AND  `type` = '$type'")->result_array();
		$this->load->view('marksheet_designs/marksheet_design_table', $data);
	}


}

?>
