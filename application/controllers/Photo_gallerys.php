<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Photo_gallerys extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Photo_gallery'));
        $this->load->library('session');
        //$this->checkpermission();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Photo Gallery';
        $data['heading_msg'] = "Media Library";
        $data['top_menu'] = $this->load->view('photo_gallerys/photo_gallery_menu', '', true);
        //$data['maincontent'] = $this->load->view('photo_gallerys/gallery_event_list', '', true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function gallery_event_list()
    {
        $data = array();
        $data['title'] = 'Gallery Event';
        $data['heading_msg'] = "Gallery Event";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('photo_gallerys/gallery_event_list/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Photo_gallery->get_all_gallery_events(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['events'] = $this->Photo_gallery->get_all_gallery_events(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);

        // echo '<pre>';
        // print_r($data['events']);
        // die;

        $data['top_menu'] = $this->load->view('photo_gallerys/photo_gallery_menu', '', true);
        $data['maincontent'] = $this->load->view('photo_gallerys/gallery_event_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function gallery_event_add()
    {
        if ($_POST) {
            $data = array();
            $data['event_name'] = $this->input->post('txtEventName', true);
            $data['remarks'] = $this->input->post('txtRemarks', true);
            $this->Photo_gallery->add_gallery_event($data);
            $sdata['message'] = "You are Successfully Event Added ! ";
            $this->session->set_userdata($sdata);
            redirect("photo_gallerys/gallery_event_add");
        } else {
            $data = array();
            $data['title'] = 'Add New Event';
            $data['heading_msg'] = "Gallery Event";
            $data['top_menu'] = $this->load->view('photo_gallerys/photo_gallery_menu', '', true);
            $data['action'] = '';
            $data['maincontent'] = $this->load->view('photo_gallerys/gallery_event_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function gallery_event_edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['event_name'] = $this->input->post('txtEventName', true);
            $data['remarks'] = $this->input->post('txtRemarks', true);
            $this->Photo_gallery->update_gallery_event($data);
            $sdata['message'] = "You are Successfully Updated ! ";
            $this->session->set_userdata($sdata);
            redirect("photo_gallerys/gallery_event_list");
        } else {
            $data = array();
            $data['title'] = 'Update Event';
            $data['heading_msg'] = "Gallery Event";
            $data['top_menu'] = $this->load->view('photo_gallerys/photo_gallery_menu', '', true);
            $data['action'] = 'edit';
            $data['events_info'] = $this->Photo_gallery->get_event_info_by_event_id($id);
            $data['maincontent'] = $this->load->view('photo_gallerys/gallery_event_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function gallery_event_delete($id)
    {
        $this->Photo_gallery->delete_gallery_event_by_event_id($id);
        $sdata['message'] = "Event Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("photo_gallerys/gallery_event_list");
    }

    public function photo_gallery_list()
    {
        $data = array();
        $data['title'] = 'Photo Gallery';
        $data['heading_msg'] = "Media Library";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('photo_gallerys/photo_gallery_list/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Photo_gallery->get_all_gallery_photos(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['all_photos'] = $this->Photo_gallery->get_all_gallery_photos(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['top_menu'] = $this->load->view('photo_gallerys/photo_gallery_menu', '', true);
        $data['maincontent'] = $this->load->view('photo_gallerys/photo_gallery_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function gallery_photo_add()
    {
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/gallery/';
            $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size'] = '5000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "PG_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/gallery/" . $new_photo_name)) {
                        $data = array();
                        $data['event_id'] = $this->input->post('txtEventName', true);
                        $data['title'] = $this->input->post('txtTitle', true);
                        $data['photo_location'] = $new_photo_name;
                        $data['remarks'] = $this->input->post('txtRemarks', true);
                        $data['upload_date'] = date('Y-m-d');
                        $this->Photo_gallery->add_gallery_photo($data);
                        $sdata['message'] = "You are Successfully Photo Added ! ";
                        $this->session->set_userdata($sdata);
                        redirect("photo_gallerys/gallery_photo_add");
                    } else {
                        $sdata['exception'] = "Sorry Member Doesn't Added !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("photo_gallerys/gallery_photo_add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Photo Does't Upload !" . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("photo_gallerys/gallery_photo_add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Add New Photo';
            $data['heading_msg'] = "Media Library";
            $data['action'] = '';
            $data['events'] = $this->db->query("SELECT * FROM tbl_photo_event")->result_array();
            $data['top_menu'] = $this->load->view('photo_gallerys/photo_gallery_menu', '', true);
            $data['maincontent'] = $this->load->view('photo_gallerys/gallery_photo_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function gallery_photo_delete($id)
    {
        $gallery_photo_info = $this->Photo_gallery->get_gallery_photo_info_by_photo_id($id);
        $file = $gallery_photo_info[0]['photo_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/gallery/' . $file;
            if (unlink($filedel)) {
                $this->Photo_gallery->delete_gallery_photo_by_photo_id($id);
            } else {
                $this->Photo_gallery->delete_gallery_photo_by_photo_id($id);
            }
        } else {
            $this->Photo_gallery->delete_gallery_photo_by_photo_id($id);
        }

        $sdata['message'] = "Gallery Photo Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("photo_gallerys/photo_gallery_list");
    }


    public function checkpermission()
    {
        $user_info = $this->session->userdata('user_info');
        //echo '<pre>'; print_r($user_info); die();
        if (!empty($user_info[0])) {

        } else {
            $sdata['message'] = "You are not logged in!";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }

    public function get_all_gallery_event_for_fontend($unit_id)
    {
        $data = array();
        $data['title'] = 'Photo Gallery';
        $data['heading_msg'] = "Photo Gallery";
        $data['digital_displays_menu'] = $this->Digital_display->get_all_digital_display_sub_menu();
        $data['all_units'] = $this->Photo_gallery->get_all_unit_for_fontend();
        $data['traning_list'] = $this->Training_manage->get_all_training_programs(0, 0, 0);
        $data['home_page_news'] = $this->Home_model->get_all_home_page_news();
        $data['unit_id'] = $unit_id;
        $data['webroot_tmp'] = base_url() . "webroot/tmpl/";
        $data['left_menu'] = $this->load->view('templates/sources/home_page_left_menu', $data, true);
        $data['right_menu'] = $this->load->view('templates/sources/home_page_right_menu', '', true);
        $data['main_content'] = $this->load->view('templates/sources/photo_gallery/photo_gallery_event', $data, true);
        $this->template->write_view('content', 'home', $data);
        $this->template->render();
    }

    public function get_gallery_image_by_event_id()
    {
        $data = array();
        $data['title'] = 'Photo Gallery';
        $data['heading_msg'] = "Photo Gallery";
        $data['digital_displays_menu'] = $this->Digital_display->get_all_digital_display_sub_menu();
        $data['all_units'] = $this->Photo_gallery->get_all_unit_for_fontend();
        $data['traning_list'] = $this->Training_manage->get_all_training_programs(0, 0, 0);
        $data['webroot_tmp'] = base_url() . "webroot/tmpl/";
        $data['event_id'] = $this->uri->segment(3);
        $data['unit_id'] = $this->uri->segment(4);
        $data['images'] = $this->Photo_gallery->get_all_images_by_unit_and_event_id($data);
        $data['home_page_news'] = $this->Home_model->get_all_home_page_news();
        $data['left_menu'] = $this->load->view('templates/sources/home_page_left_menu', $data, true);
        $data['right_menu'] = $this->load->view('templates/sources/home_page_right_menu', '', true);
        $data['main_content'] = $this->load->view('templates/sources/photo_gallery/photo_gallery', $data, true);
        $this->template->write_view('content', 'home', $data);
        $this->template->render();
    }

}
