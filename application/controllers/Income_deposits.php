<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Income_deposits extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Account', 'Admin_login'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $cond = array();
        if ($_POST) {
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");

            $sdata['from_date'] = $from_date;
            $sdata['to_date'] = $to_date;

            $this->session->set_userdata($sdata);
            $cond['from_date'] = $from_date;
            $cond['to_date'] = $to_date;
        } else {
            $from_date = $this->session->userdata('from_date');
            $to_date = $this->session->userdata('to_date');
            $cond['from_date'] = $from_date;
            $cond['to_date'] = $to_date;
        }
        $data['title'] = $this->lang->line('income') . ' ' . $this->lang->line('deposit');
        $data['heading_msg'] = $this->lang->line('income') . ' ' . $this->lang->line('deposit');
        $this->load->library('pagination');
        $config['base_url'] = site_url('income_deposits/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Account->get_all_deposit(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['deposits'] = $this->Account->get_all_deposit(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('income_deposits/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        $user_info = $this->session->userdata('user_info');
        //echo $user_info[0]->id;
        //  die;
        $user_asset_head = $this->Admin_login->getUserWiseAssetHead($user_info[0]->id);
        // echo '<pre>';
        // print_r($user_asset_head);
        // die;
        if (empty($user_asset_head)) {
            $sdata['exception'] = "Please set asset head for this user";
            $this->session->set_userdata($sdata);
            redirect("user_wise_income_heads/index");
        }
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $mdata = array();
            $mdata['date'] = $this->input->post("date");
            $mdata['income_voucher_id'] = $this->input->post("income_voucher_id");
            $mdata['income_total_amount'] = $this->input->post("income_total_amount");
            $mdata['cost_center_id'] = $this->input->post("cost_center_id");
            $mdata['add_time'] = date("Y-m-d H:i:s");
            $user_info = $this->session->userdata('user_info');
            $mdata['user_id'] = $user_info[0]->id;

            if ($this->db->insert('tbl_ba_deposit', $mdata)) {
                $insert_id = $this->db->insert_id();
                $loop_time = $this->input->post("num_of_row");
                $i = 0;
                while ($i < $loop_time) {
                    $data = array();
                    $data['deposit_id'] = $insert_id;
                    $data['income_category_id'] = $this->input->post("income_category_id_" . $i);
                    $data['income_description'] = $this->input->post("income_description_" . $i);
                    $data['deposit_method_id'] = $this->input->post("deposit_method_id_" . $i);
                    $data['amount'] = $this->input->post("amount_" . $i);
                    $data['cost_center_id'] = $this->input->post("cost_center_id");
                    $this->db->insert('tbl_ba_deposit_details', $data);
                    $i++;
                }

                //asset entry
                $assetdata = array();
                $assetdata['user_id'] = $user_asset_head[0]['user_id'];
                $assetdata['asset_id'] = $user_asset_head[0]['asset_category_id'];
                $assetdata['amount'] = $this->input->post("income_total_amount");
                $assetdata['type'] = 'I';
                $assetdata['from_transaction_type'] = 'OI';
                $assetdata['date'] = $this->input->post("date");
                $assetdata['reference_id'] = $insert_id;
                $assetdata['added_by'] = $user_info[0]->id;
                $assetdata['remarks'] = 'From Income (' . $this->input->post("income_voucher_id") . ')';
                $this->db->insert('tbl_user_wise_asset_transaction', $assetdata);
                //asset entry

                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("income_deposits/add");
            }
        }
        $data = array();
        $data['title'] = $this->lang->line('income') . ' ' . $this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('income') . ' ' . $this->lang->line('add');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['is_show_button'] = "index";
        $data['income_category'] = $this->db->query("SELECT * FROM tbl_ba_income_category")->result_array();
        $data['deposit_method'] = $this->db->query("SELECT * FROM tbl_ba_deposit_method")->result_array();
        $data['cost_center'] = $this->db->query("SELECT * FROM tbl_cost_center")->result_array();
        $data['maincontent'] = $this->load->view('income_deposits/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function deposit_details_view($id)
    {
        $this->load->library('numbertowords');
        $data = array();
        $data['title'] = 'DEPOSIT VIEW';
        $data['heading_msg'] = "DEPOSIT VIEW";
        $data['school_info'] = $this->Admin_login->get_contact_info();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['deposit_info'] = $this->db->query("SELECT tbl_ba_deposit.*,cc.name as cost_center FROM tbl_ba_deposit
           left join tbl_cost_center as cc on cc.id = tbl_ba_deposit.cost_center_id
           WHERE tbl_ba_deposit.id = '$id'")->result_array();
        $data['deposit_details_info'] = $this->db->query("SELECT dd.*,c.name as deposit_category_name,dm.name as deposit_method_name
                                        FROM tbl_ba_deposit_details as dd
                                        left join tbl_ba_income_category as c on c.id = dd.income_category_id
                                        left join tbl_ba_deposit_method as dm on dm.id = dd.deposit_method_id
                                        WHERE dd.deposit_id = '$id'")->result_array();
        // echo '<pre>';
        // print_r($data['deposit_info']);
        //    print_r($data['deposit_details_info']);
        //  die;
        $this->load->view('income_deposits/deposit_details_view', $data);
    }

    public function delete($id)
    {
        if ($this->db->delete('tbl_ba_deposit_details', array('deposit_id' => $id))) {
            $this->db->delete('tbl_ba_deposit', array('id' => $id));
            $this->db->delete('tbl_user_wise_asset_transaction', array('reference_id' => $id,'type' => 'I','from_transaction_type' => 'OI'));
            $sdata['message'] = $this->lang->line('delete_success_message');
            $this->session->set_userdata($sdata);
            redirect("income_deposits/index");
        }
    }
}
