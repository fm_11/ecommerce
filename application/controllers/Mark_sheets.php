<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mark_sheets extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Student', 'Message','Admin_login','common/custom_methods_model'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        if ($_POST) {
            $year = $this->input->post('year', true);
            $exam = $this->input->post('exam', true);

            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class  = $class_shift_section_arr[0];
            $shift = $class_shift_section_arr[1];
            $section = $class_shift_section_arr[2];

            $group = $this->input->post('group', true);
            $roll = $this->input->post('roll', true);
			$view_type = $this->input->post('view_type', true);
			$from_roll = $this->input->post('from_roll', true);
			$to_roll = $this->input->post('to_roll', true);

			$student_id = 0;
			$st_where = "";
			if ($view_type == 'S') {
				if ($from_roll == '' || $to_roll == '') {
					$sdata['exception'] = "From roll and To roll cannot be empty";
					$this->session->set_userdata($sdata);
					redirect("id_cards/index");
				}
				if ($from_roll > $to_roll) {
					$sdata['exception'] = "From roll cannot be greater than To roll";
					$this->session->set_userdata($sdata);
					redirect("id_cards/index");
				}

				$roll_where_in_string = "";
				while ($from_roll <= $to_roll) {
					$roll_where_in_string .= "'" . $from_roll ."',";
					$from_roll++;
				}
				$roll_where_in_string_for_q = rtrim($roll_where_in_string, ',');
				//die;
				$st_where = "$roll_where_in_string_for_q";
			}

            $total_working_days = $this->input->post('total_working_days', true);

			$roll_where =  "";
			if ($view_type == 'S') {
				$roll_where = $st_where;
			}


            $result_info = $this->Student->result_all_student_after_process($exam, $class, $section, $group, $shift, $student_id, $roll_where);
//               echo '<pre>';
//              print_r($result_info); die;

            if (empty($result_info)) {
                $sdata['exception'] = "Sorry result not found !";
                $this->session->set_userdata($sdata);
                redirect("mark_sheets/index");
            } else {
                $data = array();

                $highest_mark = $this->db->query("SELECT s.`subject_code`,MAX(s.`total_obtain`) as highest_mark,s.`student_id` FROM `tbl_result_process_details` AS s
                WHERE s.exam_id = '$exam' AND s.`class_id` = '$class' AND s.`section_id` = '$section' AND
                 s.`group` = '$group' AND s.`shift` = '$shift' GROUP BY s.`subject_code`")->result_array();

                $highest_marks = array();
                foreach ($highest_mark as $row):
                   $highest_marks[$row['subject_code']] = $row['highest_mark'];
                endforeach;

                $data['highest_marks'] = $highest_marks;


                $data['title'] = 'Mark Sheet';
                $data['school_info'] = $this->db->query("SELECT * FROM `tbl_contact_info`")->result_array();
			    // echo '<pre>';
               // print_r( $data['school_info']); die;
                $data['exam'] = $this->db->query("SELECT * FROM `tbl_exam` AS e WHERE e.`id` = '$exam' AND e.`year` = '$year'")->result_array();
                $exam_type_id = $data['exam'][0]['exam_type_id'];
                $data['is_annual_exam'] = $data['exam'][0]['is_annual_exam'];
                //echo '<pre>';
                //print_r($data['exam']);
                //die;
                $data['Result'] = $result_info;

                $class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class'")->result_array();
                $data['class_name'] = $class_info[0]['name'];

                $section_info = $this->db->query("SELECT name FROM tbl_section WHERE id ='$section'")->result_array();
                $data['section_name'] = $section_info[0]['name'];

                $group_info = $this->db->query("SELECT name FROM tbl_student_group WHERE id ='$group'")->result_array();
                $data['group_name'] = $group_info[0]['name'];

                $shift_info = $this->db->query("SELECT name FROM tbl_shift WHERE id ='$shift'")->result_array();
                $data['shift_name'] = $shift_info[0]['name'];
                $data['total_working_days'] = $total_working_days;

                $section_where = "";
                $place_show_will_be_marksheet = 1;
                $marksheet_design = 'N';
                $is_process_bangla_english_separate = 1;
                $add_extra_mark_with_total_obtained = 0;
                $calculable_total_mark_percentage = 0;
                $admit_card_text_color = "000000";
                $admit_card_background_color = "FFFFFF";

                $tbl_config = $this->db->query("SELECT place_show_will_be_marksheet,marksheet_design,is_process_bangla_english_separate,
                      how_to_generate_position,add_extra_mark_with_total_obtained,calculable_total_mark_percentage FROM tbl_config")->result_array();
                if (!empty($tbl_config)) {
                    $place_show_will_be_marksheet = $tbl_config[0]['place_show_will_be_marksheet'];
                    $marksheet_design = $tbl_config[0]['marksheet_design'];
                    $is_process_bangla_english_separate =  $tbl_config[0]['is_process_bangla_english_separate'];
                    $how_to_generate_position = $tbl_config[0]['how_to_generate_position'];
                    $add_extra_mark_with_total_obtained = $tbl_config[0]['add_extra_mark_with_total_obtained'];
                    $calculable_total_mark_percentage = $tbl_config[0]['calculable_total_mark_percentage'];
                } else {
                    $how_to_generate_position = 'CS';
                }
                $data['place_show_will_be_marksheet'] = $place_show_will_be_marksheet;
                $data['is_process_bangla_english_separate'] = $is_process_bangla_english_separate;
                $data['add_extra_mark_with_total_obtained'] = $add_extra_mark_with_total_obtained;
                $data['calculable_total_mark_percentage'] = $calculable_total_mark_percentage;
                $data['admit_card_text_color'] = $admit_card_text_color;
                $data['admit_card_background_color'] = $admit_card_background_color;

                $data['main_marking_heads'] = $this->Admin_login->get_main_marking_head($class);
                if(empty($data['main_marking_heads'])){
                  $sdata['exception'] = "Please set class wise marking head";
            			$this->session->set_userdata($sdata);
            			redirect('main_marking_heads/edit/' . $class);
                }


                if ($how_to_generate_position == 'CS') {
                    $section_where = " AND `section_id` = '$section' ";
                }

				$data['signature'] = $this->Admin_login->getSignatureByAccessCode('MS'); //MS= Marksheet


                $data['max_total_obtain_mark'] = $this->db->query("SELECT `total_obtain_mark` AS max_total_obtain_mark
  FROM `tbl_result_process` WHERE  exam_id = '$exam' AND class_id = '$class' AND `group` = '$group' AND `shift` = '$shift' AND `class_position` = '1' $section_where")->result_array();

                if (empty($data['max_total_obtain_mark'])) {
                    $data['max_total_obtain_mark'][0]['max_total_obtain_mark'] = 0;
                }

                $data['extra_head'] = $this->db->query("SELECT exa.*,exh.name as head_name FROM `tbl_class_wise_extra_head_amount` as exa
                             INNER JOIN tbl_exam_marking_extra_head as exh on exa.head_id = exh.id WHERE exa.class_id = '$class' AND exa.exam_type_id = '$exam_type_id' ORDER BY exh.id ASC")->result_array();

                if ($add_extra_mark_with_total_obtained == 0) {
                    $data['extra_head_colspan'] = count($data['extra_head']);
                } else {
                    $data['extra_head_colspan'] = 0;
                    $extra_head_value_for_total = array();
                    foreach ($data['extra_head'] as $exh_row):
                $head_id = $exh_row['head_id'];
                    // echo $head_id.'/';
                    $extra_head_value_for_total[$head_id] = $this->getExtraHeadMarkForTotalMark($class, $section, $group, $shift, $exam, $head_id);
                    endforeach;
                    $data['extra_head_value_for_total'] = $extra_head_value_for_total;
                }


                //	echo '<pre>';
                //print_r($data['extra_head']);
                //	die;


                if (isset($_POST['pdf_download'])) {
                    //echo 'gggg'; die;
                    // Load all views as normal
                    $data['is_pdf'] = 1;
                    if ($marksheet_design == 'D') {
                        $html = $this->load->view('mark_sheets/mark_sheet_view_for_different_calculable_amount', $data, true);
                    } elseif ($marksheet_design == 'D2') {
                        $html = $this->load->view('mark_sheets/mark_sheet_view_details_view_2', $data, true);
                    } elseif ($marksheet_design == 'D3') {
                        $html = $this->load->view('mark_sheets/mark_sheet_view_details_view_3', $data, true);
                    } elseif ($marksheet_design == 'D4') {
                        $html = $this->load->view('mark_sheets/mark_sheet_view_details_view_4', $data, true);
                    } else {
                        $html = $this->load->view('mark_sheets/mark_sheet_view', $data, true);
                    }
                    $this->load->library('m_pdf');
                    $this->m_pdf->pdf->AddPage(
                        'P', // L - landscape, P - portrait
                        '',
                        '',
                        '',
                        '',
                        5, // margin_left
                        5, // margin right
                        5, // margin top
                        5, // margin bottom
                        5, // margin header
                        5
                    ); // margin footer
                    $pdfFilePath = "MarkSheet.pdf";
                    //generate the PDF from the given html
                    $this->m_pdf->pdf->autoScriptToLang = true;
                    $this->m_pdf->pdf->WriteHTML($html);
                    //download it.
                    $this->m_pdf->pdf->Output($pdfFilePath, "D");
                } else {
                    $data['is_pdf'] = 0;
//                    if ($marksheet_design == 'D') {
//                        $data['extra_head'] = $this->db->query("SELECT exa.*,exh.name as head_name FROM `tbl_class_wise_extra_head_amount` as exa
//                             INNER JOIN tbl_exam_marking_extra_head as exh on exa.head_id = exh.id WHERE exa.class_id = '$class' AND exa.exam_type_id = '$exam_type_id' ORDER BY exh.id ASC")->result_array();
//
//                        $this->load->view('mark_sheets/mark_sheet_view_for_different_calculable_amount', $data);
//                    } elseif ($marksheet_design == 'D2') {
//                        $this->load->view('mark_sheets/mark_sheet_view_details_view_2', $data);
//                    } elseif ($marksheet_design == 'D3') {
//                        $this->load->view('mark_sheets/mark_sheet_view_details_view_3', $data);
//                    } elseif ($marksheet_design == 'D4') {
//                        $this->load->view('mark_sheets/mark_sheet_view_details_view_4', $data);
//                    } else {
//                        $this->load->view('mark_sheets/mark_sheet_view', $data);
//                    }
					$this->load->view('mark_sheets/mark_sheet_view', $data);
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Mark Sheet Archive';
            $data['heading_msg'] = "Mark Sheet Archive";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['years'] = $this->Admin_login->getYearList(0, 0);
            $data['group'] = $this->Admin_login->getGroupList();
            $data['maincontent'] = $this->load->view('mark_sheets/mark_sheet_index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
