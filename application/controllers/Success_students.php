<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Success_students extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
		$data['title'] = 'Success Students';
		$data['heading_msg'] = 'Success Students';
		$data['is_show_button'] = "add";
		$cond = array();
		$this->load->library('pagination');
		$config['base_url'] = site_url('success_students/index/');
		$config['per_page'] = 20;
		$config['total_rows'] = count($this->Admin_login->get_all_success_student(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['success_student'] = $this->Admin_login->get_all_success_student(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['is_show_button'] = "add";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('success_students/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('txtName', true);
            $data['year_of_passing'] = $this->input->post('txtYearPassing', true);
            $data['edu_qua'] = $this->input->post('txtEduQua', true);
            $data['org'] = $this->input->post('txtOrg', true);
            $data['position'] = $this->input->post('txtPosition', true);
            $data['mobile'] = $this->input->post('txtMobile', true);
            $data['email'] = $this->input->post('txtEmail', true);
            $data['address'] = $this->input->post('txtAddress', true);
            $this->db->insert('tbl_success_students', $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect("success_students/add");
        } else {
            $data = array();
            $data['title'] = 'Success Students';
            $data['heading_msg'] = "Success Students";
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['action'] = '';
            $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
            $data['maincontent'] = $this->load->view('success_students/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

	public function edit($id=null)
	{
		if ($_POST) {
			$data = array();
			$data['id'] = $this->input->post('id', true);
			$data['name'] = $this->input->post('txtName', true);
			$data['year_of_passing'] = $this->input->post('txtYearPassing', true);
			$data['edu_qua'] = $this->input->post('txtEduQua', true);
			$data['org'] = $this->input->post('txtOrg', true);
			$data['position'] = $this->input->post('txtPosition', true);
			$data['mobile'] = $this->input->post('txtMobile', true);
			$data['email'] = $this->input->post('txtEmail', true);
			$data['address'] = $this->input->post('txtAddress', true);
			$this->db->where('id', $data['id']);
			$this->db->update('tbl_success_students', $data);
			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect("success_students/index");
		} else {
			$data = array();
			$data['title'] = 'Success Students';
			$data['heading_msg'] = "Success Students";
			$data['is_show_button'] = "index";
			$data['student_info'] = $this->db->query("SELECT * FROM tbl_success_students WHERE id = $id")->row();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
			$data['maincontent'] = $this->load->view('success_students/edit', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

    public function delete($id)
    {
        $this->db->delete('tbl_success_students', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("success_students/index");
    }

}
