<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Managing_committees extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

	public function index()
	{
		$data['title'] = 'Managing Committee';
		$data['heading_msg'] = 'Managing Committee';
		$data['is_show_button'] = "add";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['managing_committee'] = $this->db->query("SELECT * FROM tbl_managing_committee ORDER BY id ASC")->result_array();
		$data['maincontent'] = $this->load->view('managing_committees/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add()
	{
		if ($_POST) {
			$this->load->library('upload');
			$config['upload_path'] = MEDIA_FOLDER . '/managing_committee/';
			$config['allowed_types'] = 'png|JPEG|jpeg|jpg';
			$config['max_height'] = '400';
			$config['max_width'] = '400';
			$config['max_size'] = '2000';
			$this->upload->initialize($config);
			$extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
			foreach ($_FILES as $field => $file) {
				if ($file['error'] == 0) {
					$new_photo_name = "MC_" . time() . "_" . date('Y-m-d') . "." . $extension;
					if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/managing_committee/" . $new_photo_name)) {
						$data = array();
						$data['name'] = $this->input->post('txtName', true);
						$data['post'] = $this->input->post('txtPost', true);
						$data['mobile'] = $this->input->post('txtMobile', true);
						$data['photo_location'] = $new_photo_name;
						$this->db->insert('tbl_managing_committee', $data);
						$sdata['message'] = $this->lang->line('add_success_message');
						$this->session->set_userdata($sdata);
						redirect("managing_committees/add");
					} else {
						$sdata['exception'] = "Sorry Member Doesn't Added !" . $this->upload->display_errors();
						$this->session->set_userdata($sdata);
						redirect("managing_committees/add");
					}
				} else {
					$sdata['exception'] = "Sorry Member Doesn't Upload !";
					$this->session->set_userdata($sdata);
					redirect("managing_committees/add");
				}
			}
		} else {
			$data = array();
			$data['title'] = 'Managing Committee';
			$data['heading_msg'] = "Add New Member";
			$data['is_show_button'] = "index";
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('managing_committees/add', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

	public function edit($id = null)
	{
		if ($_POST) {
			$file = $_FILES["txtPhoto"]['name'];
			if ($file != '') {
				$mc_photo_info = $this->db->where('id', $this->input->post('id', true))->get('tbl_managing_committee')->result_array();
				$old_file = $mc_photo_info[0]['photo_location'];
				if (!empty($old_file)) {
					$filedel = PUBPATH . MEDIA_FOLDER . '/managing_committee/' . $old_file;
					unlink($filedel);
				}
				$this->load->library('upload');
				$config['upload_path'] = MEDIA_FOLDER . '/managing_committee/';
				$config['allowed_types'] = 'jpg|png|JPEG|jpeg';
				$config['max_size'] = '6000';
				$config['max_width'] = '4000';
				$config['max_height'] = '4000';
				$this->upload->initialize($config);
				$extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
				foreach ($_FILES as $field => $file) {
					if ($file['error'] == 0) {
						$new_photo_name = "MC_" . time() . "_" . date('Y-m-d') . "." . $extension;
						if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/managing_committee/" . $new_photo_name)) {
							$data = array();
							$data['id'] = $this->input->post('id', true);
							$data['name'] = $this->input->post('txtName', true);
							$data['post'] = $this->input->post('txtPost', true);
							$data['mobile'] = $this->input->post('txtMobile', true);
							$data['photo_location'] = $new_photo_name;
							$this->db->where('id', $data['id']);
							$this->db->update('tbl_managing_committee', $data);
							$sdata['message'] = "You are Successfully Managing Committee Information Updated ! ";
							$this->session->set_userdata($sdata);
							redirect("managing_committees/index");
						} else {
							$sdata['exception'] = "Sorry Managing Committee Doesn't Updated !" . $this->upload->display_errors();
							$this->session->set_userdata($sdata);
							redirect("managing_committees/index");
						}
					} else {
						$sdata['exception'] = "Sorry Photo Does't Upload !";
						$this->session->set_userdata($sdata);
						redirect("managing_committees/index");
					}
				}
			} else {
				$data = array();
				$data['id'] = $this->input->post('id', true);
				$data['name'] = $this->input->post('txtName', true);
				$data['post'] = $this->input->post('txtPost', true);
				$data['mobile'] = $this->input->post('txtMobile', true);
				$this->db->where('id', $data['id']);
				$this->db->update('tbl_managing_committee', $data);
				$sdata['message'] = $this->lang->line('edit_success_message');
				$this->session->set_userdata($sdata);
				redirect("managing_committees/index");
			}
		} else {
			$data = array();
			$data['title'] = 'Update Managing Committee Information';
			$data['heading_msg'] = "Update Managing Committee Information";
			$data['is_show_button'] = "index";
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['members'] = $this->db->query("SELECT * FROM tbl_managing_committee WHERE id = '$id'")->result_array();
			$data['maincontent'] = $this->load->view('managing_committees/edit', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

	public function delete($id)
	{
		$member_photo_info = $this->db->where('id', $id)->get('tbl_managing_committee')->result_array();
		$file = $member_photo_info[0]['photo_location'];
		if (!empty($file)) {
			$filedel = PUBPATH . MEDIA_FOLDER . '/managing_committee/' . $file;
			if (unlink($filedel)) {
				$this->db->delete('tbl_managing_committee', array('id' => $id));
			} else {
				$this->db->delete('tbl_managing_committee', array('id' => $id));
			}
		} else {
			$this->db->delete('tbl_managing_committee', array('id' => $id));
		}

		$sdata['message'] = $this->lang->line('delete_success_message');
		$this->session->set_userdata($sdata);
		redirect("managing_committees/index");
	}


}
