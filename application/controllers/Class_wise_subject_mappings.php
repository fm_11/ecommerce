<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Class_wise_subject_mappings extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Account', 'Admin_login'));
		$this->load->library('session');
		date_default_timezone_set('Asia/Dhaka');
		$this->notification = array();
	}

	public function index(){
		$data = array();
		$data['title'] = "Class Wise Subject Mapping";
		$data['heading_msg'] = "Class Wise Subject Mapping";

		$class_list = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
		$i = 0;
		$class_array = array();
		foreach ($class_list as $class){
			$class_id = $class['id'];
			$class_array[$i]['id'] = $class_id;
			$class_array[$i]['name'] = $class['name'];

			$subject_list = $this->db->query("SELECT GROUP_CONCAT(DISTINCT CONCAT(s.`name`) 
ORDER BY s.`name` DESC) AS subject_list
FROM `tbl_subject` AS s
INNER JOIN `tbl_class_wise_subject_mappings` AS m ON m.`subject_id` = s.`id` AND m.`class_id` = $class_id;")->row();

			$class_array[$i]['subjects_name'] = $subject_list->subject_list;

			$i++;
		}

		$data['class_list'] = $class_array;


		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('class_wise_subject_mappings/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function edit($class_id=null)
	{
		if ($_POST) {
			$class_id =  $_POST['class_id'];
			$total_row =  $_POST['total_row'];
			$i = 0;
			$insert_row_no = 0;
			$save_data = array();
			while ($i < $total_row){
				if(isset($_POST['selected_item_' . $i])){
					$save_data[$insert_row_no]['class_id'] = $class_id;
					$save_data[$insert_row_no]['subject_id'] = $_POST['subject_id_' . $i];
					$save_data[$insert_row_no]['subject_type'] = $_POST['subject_type_' . $i];
					$save_data[$insert_row_no]['group_list'] = implode(",", $_POST['groups_' . $i]);
					$insert_row_no++;
				}
				$i++;
			}
			$this->db->query("DELETE FROM `tbl_class_wise_subject_mappings` WHERE `class_id` = $class_id;");
			$this->db->insert_batch('tbl_class_wise_subject_mappings', $save_data);
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect('class_wise_subject_mappings/index');
		}
		$data = array();
		$data['class_id'] = $class_id;
		$data['class_info'] = $this->db->query("SELECT * FROM `tbl_class` WHERE id = $class_id")->row();
		$data['subject_list'] = $this->db->query("SELECT s.`id`,s.`name`,m.`subject_id`,m.`group_list`,m.`subject_type` FROM `tbl_subject` AS s
								LEFT JOIN `tbl_class_wise_subject_mappings` AS m ON m.`subject_id` = s.`id` AND m.`class_id` = $class_id
								ORDER BY s.`name`")->result_array();

		if (empty($data['subject_list'])) {
			$sdata['exception'] = "Please section add first";
			$this->session->set_userdata($sdata);
			redirect('subjects/index');
		}
		$data['groups'] = $this->Admin_login->getGroupList();
		$data['title'] = "Class Wise Subject Mapping";
		$data['heading_msg'] = "Class Wise Subject Mapping";
		$data['is_show_button'] = "index";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('class_wise_subject_mappings/edit', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	function data_copy(){
		if($_POST){
			$from_class_id = $_POST['from_class_id'];
			$to_class_id = $_POST['to_class_id'];
			$getData = $this->db->query("SELECT * FROM `tbl_class_wise_subject_mappings` WHERE `class_id` = $from_class_id;")->result_array();
			$save_data = array();
			$insert_row_no = 0;
			foreach ($getData as $row){
				$save_data[$insert_row_no]['class_id'] = $to_class_id;
				$save_data[$insert_row_no]['subject_id'] = $row['subject_id'];
				$save_data[$insert_row_no]['subject_type'] = $row['subject_type'];
				$save_data[$insert_row_no]['group_list'] = $row['group_list'];
				$insert_row_no++;
			}
			$this->db->query("DELETE FROM `tbl_class_wise_subject_mappings` WHERE `class_id` = $to_class_id;");
			$this->db->insert_batch('tbl_class_wise_subject_mappings', $save_data);
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect('class_wise_subject_mappings/index');
		}
	}

	function checkAlreadyAssignSubject(){
		$class_id = $this->input->get('class_id', true);
		$check = $this->db->query("SELECT id FROM `tbl_class_wise_subject_mappings` WHERE `class_id` = $class_id;")->result_array();
		if(empty($check)){
			echo '1';
		}else{
			echo '0';
		}
	}
}
