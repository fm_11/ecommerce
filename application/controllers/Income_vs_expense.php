<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Income_vs_expense extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Income vs Expense';
        $data['heading_msg'] = 'Income vs Expense';
        if ($_POST) {
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];

            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");

            $data['HeaderInfo'] = $Info;

            $data['from_date'] = $this->input->post("from_date");
            $data['to_date'] = $this->input->post("to_date");
            $data['class_id'] = 'all';


            $data['deposit_info'] = $this->db->query("SELECT SUM(dd.`amount`) AS total_amount, ic.`name` AS income_cat_name FROM `tbl_ba_deposit_details` AS dd
INNER JOIN `tbl_ba_deposit` AS d ON d.`id` = dd.`deposit_id`
INNER JOIN `tbl_ba_income_category` AS ic ON ic.`id` = dd.`income_category_id`
WHERE d.`date` BETWEEN '$from_date' AND '$to_date'
GROUP BY dd.`income_category_id`")->result_array();


            $data['student_fee_details'] = $this->Student_fee->getCollectionDetailsStandard($data);

            $special_care_fee_details = $this->db->query("SELECT SUM(c.`amount`) AS total_amount FROM
             `tbl_monthly_special_care_fee` AS c WHERE c.`date` BETWEEN '$from_date' AND '$to_date'")->result_array();
            if (empty($special_care_fee_details)) {
                $data['special_care_fee_details'] =  0;
            } else {
                $data['special_care_fee_details'] =  $special_care_fee_details[0]['total_amount'];
            }

            $data['expense_details'] = $this->db->query("SELECT SUM(e.`amount`) AS total_amount, ec.`name` AS expense_cat_name FROM `tbl_ba_expense_details` AS e
INNER JOIN `tbl_ba_expense` AS ex ON ex.`id` = e.`expense_id`
INNER JOIN `tbl_ba_expense_category` AS ec ON ec.`id` = e.`expense_category_id`
WHERE ex.`date` BETWEEN '$from_date' AND '$to_date'
GROUP BY e.`expense_category_id`")->result_array();

            $data['report'] = $this->load->view('income_vs_expense/report', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            $data['is_pdf'] = 1;
            //Dom PDF
            $this->load->library('mydompdf');
            $html = $this->load->view('income_vs_expense/report', $data, true);
            $this->mydompdf->createPDF($html, 'Income_vs_Expense', true);
        //Dom PDF
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('income_vs_expense/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
