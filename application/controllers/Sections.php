<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sections extends CI_Controller
{

     public $SOFTWARE_START_YEAR = '';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = 'Student Section';
        $data['heading_msg'] = "Student Section";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['sections'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
        $data['maincontent'] = $this->load->view('sections/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $this->db->insert("tbl_section", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('sections/index');
        }
        $data = array();
        $data['title'] = 'Add Section';
        $data['heading_msg'] = "Add Section";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('sections/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_section', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('sections/index');
        }
        $data = array();
        $data['title'] = 'Update Section Information';
        $data['heading_msg'] = "Update Section Information";
        $data['is_show_button'] = "index";
        $data['sections'] = $this->db->query("SELECT * FROM `tbl_section` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('sections/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $class_info = $this->db->where('section_id', $id)->get('tbl_student')->result_array();
        if (!empty($class_info)) {
            $sdata['exception'] =  $this->lang->line('delete_error_message') . " (Student Depended Data Found)";
            $this->session->set_userdata($sdata);
            redirect("sections/index");
        }
        $this->db->query("DELETE FROM tbl_section WHERE `id` = '$id'");
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('sections/index');

    }
}
