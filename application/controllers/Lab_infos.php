<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lab_infos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set("Asia/Dhaka");
        $this->load->model(array('Lab_info', 'Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }

    public function index()
    {
        $data['title'] = 'Lab Manager Dashboard';
        $data['heading_msg'] = 'Lab Manager Dashboard';
        $data['lab'] = $this->db->query("SELECT * FROM tbl_lab")->result_array();
        $data['top_menu'] = $this->load->view('lab_infos/lab_menu', '', true);
        $data['maincontent'] = $this->load->view('lab_infos/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function getOnComputer()
    {
        $title = 'On';
        $data = array();
        $data['title'] = $title . ' Computer List';
        $data['heading_msg'] = $title . " Computer List";
        $current_time = date('Y-m-d H:i:s');

        $time = strtotime($current_time);
        $time = $time - (2 * 60);
        $old_time = date("Y-m-d H:i:s", $time);

        //echo $current_time.'/'.$old_time.'/'; die;
        $data['on_pc'] = $this->db->query("SELECT s.*,pc.`serial`,l.`name` AS lab_name FROM `tbl_on_of_status` AS s
LEFT JOIN `tbl_pc` AS pc ON pc.`id` = s.`pc_id`
LEFT JOIN `tbl_lab` AS l ON l.`id` = pc.`lab_id`
WHERE `time` BETWEEN '$old_time' AND '$current_time'
GROUP BY s.`pc_id`")->result_array();

        $data['top_menu'] = $this->load->view('lab_infos/lab_menu', '', true);
        $data['maincontent'] = $this->load->view('lab_infos/on_computer_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function getCurrentValue()
    {
        $data = array();
        $data['lab_id'] = $this->input->get('lab_id', true);
        if ($data['lab_id'] != '') {
            $total_lab = count($this->Lab_info->get_all_lab(0, 0, $data));
            $total_pc = $this->Lab_info->get_all_computers(0, 0, $data);

            $i = 0;
            $all_pc_id = "";
            while ($i < count($total_pc)) {
                if ($i == 0) {
                    $all_pc_id .= $total_pc[$i]['id'];
                } else {
                    $all_pc_id .= (',' . $total_pc[$i]['id']);
                }
                $i++;
            }

//        echo $all_pc_id;
//        die;

//        echo '<pre>';
//        print_r($total_pc);
//        die;

            $current_time = date('Y-m-d H:i:s');
            $time = strtotime($current_time);
            $time = $time - (1 * 60);
            $old_time = date("Y-m-d H:i:s", $time);

            if ($all_pc_id == "") {
                $on_pc = 0;
            } else {
                $on_pc = $this->db->query("SELECT s.id,s.`pc_id` FROM `tbl_on_of_status` AS s
WHERE s.`time` BETWEEN '$old_time' AND '$current_time' AND 
s.`pc_id` IN ($all_pc_id)
GROUP BY s.`pc_id`")->num_rows();
            }

            $return_array = array();
            $return_array['total_lab'] = $total_lab;
            $return_array['total_pc'] = count($total_pc);
            $return_array['on_pc'] = $on_pc;
            $return_array['off_pc'] = count($total_pc) - $on_pc;
            $return_array['old_time'] = $old_time;
            $return_array['current_time'] = $current_time;
            echo json_encode($return_array);
        }else{
            echo json_encode(array());
        }
    }

    public function pc_info_index()
    {
        $title = 'Computer';
        $data = array();
        $data['title'] = $title . ' Information';
        $data['heading_msg'] = $title . " Information";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('lab_infos/pc_info_index/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Lab_info->get_all_computers(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['computers'] = $this->Lab_info->get_all_computers(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['top_menu'] = $this->load->view('lab_infos/lab_menu', '', true);
        $data['maincontent'] = $this->load->view('lab_infos/computer_info_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    function computer_add()
    {
        if ($_POST) {
            $data = array();
            $data['serial'] = $this->input->post('serial', true);
            $data['mac_address'] = $this->input->post('mac_address', true);
            $data['processor_id'] = $this->input->post('processor_id', true);
            $data['lab_id'] = $this->input->post('lab_id', true);
            $this->db->insert('tbl_pc', $data);
            $sdata['message'] = "You are Successfully Computer Information Added ! ";
            $this->session->set_userdata($sdata);
            redirect("lab_infos/computer_add");
        } else {
            $data = array();
            $data['title'] = 'Computer Information Add';
            $data['heading_msg'] = "Add New Computer Information";
            $data['action'] = '';
            $data['lab'] = $this->db->query("SELECT id,name FROM tbl_lab")->result_array();
            $data['top_menu'] = $this->load->view('lab_infos/lab_menu', '', true);
            $data['maincontent'] = $this->load->view('lab_infos/computer_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    function computer_edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['serial'] = $this->input->post('serial', true);
            $data['mac_address'] = $this->input->post('mac_address', true);
            $data['processor_id'] = $this->input->post('processor_id', true);
            $data['lab_id'] = $this->input->post('lab_id', true);
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_pc', $data);
            $sdata['message'] = "You are Successfully Computer Information Updated ! ";
            $this->session->set_userdata($sdata);
            redirect("lab_infos/pc_info_index");
        } else {
            $data = array();
            $data['title'] = 'Computer Information';
            $data['heading_msg'] = "Update Computer Information";
            $data['action'] = 'edit';
            $data['lab'] = $this->db->query("SELECT * FROM tbl_lab")->result_array();
            $data['pc_info'] = $this->db->query("SELECT * FROM tbl_pc WHERE id = '$id'")->result_array();
            $data['top_menu'] = $this->load->view('lab_infos/lab_menu', '', true);
            $data['maincontent'] = $this->load->view('lab_infos/computer_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function computer_delete($id)
    {
        $this->db->delete('tbl_pc', array('id' => $id));
        $sdata['message'] = "Computer Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("lab_infos/pc_info_index");
    }


    /////


    public function get_all_lab()
    {
        $title = 'Lab';
        $data = array();
        $data['title'] = $title . ' Information';
        $data['heading_msg'] = $title . " Information";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('lab_infos/get_all_lab/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Lab_info->get_all_lab(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['labs'] = $this->Lab_info->get_all_lab(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['top_menu'] = $this->load->view('lab_infos/lab_menu', '', true);
        $data['maincontent'] = $this->load->view('lab_infos/lab_info', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function lab_delete($id)
    {
        $this->db->delete('tbl_lab', array('id' => $id));
        $sdata['message'] = "Lab Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("lab_infos/get_all_lab");
    }


    function lab_add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('name', true);
            $data['num_of_computer'] = $this->input->post('num_of_computer', true);
            $this->db->insert('tbl_lab', $data);
            $sdata['message'] = "You are Successfully Lab Added.";
            $this->session->set_userdata($sdata);
            redirect("lab_infos/lab_add");
        } else {
            $data = array();
            $data['action'] = "";
            $data['title'] = 'Lab Information Add';
            $data['heading_msg'] = "Lab Information Add";
            $data['top_menu'] = $this->load->view('lab_infos/lab_menu', '', true);
            $data['maincontent'] = $this->load->view('lab_infos/lab_info_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    function lab_edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('name', true);
            $data['num_of_computer'] = $this->input->post('num_of_computer', true);
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_lab', $data);
            $sdata['message'] = "You are Successfully Lab Updated.";
            $this->session->set_userdata($sdata);
            redirect("lab_infos/get_all_lab");
        } else {
            $data = array();
            $data['action'] = "edit";
            $data['title'] = 'Lab Information Update';
            $data['heading_msg'] = "Lab Information Update";
            $data['lab_info'] = $this->db->query("SELECT * FROM tbl_lab WHERE id = '$id'")->result_array();
            $data['top_menu'] = $this->load->view('lab_infos/lab_menu', '', true);
            $data['maincontent'] = $this->load->view('lab_infos/lab_info_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    /////
}
