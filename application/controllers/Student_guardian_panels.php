<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Student_guardian_panels extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login', 'Timekeeping', 'Student', 'Student_fee', 'Student_guardian_panel', 'common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
		$this->load->library('session');

		$student_info = $this->session->userdata('session_student_info');
//        echo '<pre>';
//        print_r($student_info); die;
		if (empty($student_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Valid Student !";
			$this->session->set_userdata($sdata);
			redirect("login/student");
		}

	}

	function fees_payment(){
		$student_info = $this->session->userdata('session_student_info');
		$id = $student_info[0]->id;
		$data = array();
		$data['student_info'] = $this->Student->get_student_info_by_student_id($id);
		//		echo '<pre>';
//		print_r($data['student_info']);
//		die;
		$data['title'] = 'Fees Payment';
		$data['heading_msg'] = "Fees Payment";
		$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
		$data['maincontent'] = $this->load->view('student_guardian_panels/fees_payment_option', $data, true);
		$this->load->view('student_guardian_panels/index', $data);
	}


	public function fee_payment_data_save()
	{
		if ($_POST) {
			$date = new DateTime();
			$posted_receipt_no = $date->getTimestamp();
			$checkIfExists = $this->db->query("SELECT id FROM `tbl_fee_collection`
                             WHERE receipt_no = '$posted_receipt_no'")->result_array();
			if (!empty($checkIfExists)) {
				$date = new DateTime();
				$posted_receipt_no = $date->getTimestamp();
			}

			$class_id = $this->input->post('class_id', true);
			$shift_id = $this->input->post('shift_id', true);
			$section_id = $this->input->post('section_id', true);
			$student_category_id = $this->input->post('student_category_id', true);
			$group_id = $this->input->post('group_id', true);
			$student_id = $this->input->post('student_id', true);
			$manual_collection_date = $this->input->post('manual_collection_date', true);
			$year = $this->input->post('year', true);

			$waiver_setting_info_check = $this->db->query("SELECT id FROM `tbl_fee_waiver_setting` WHERE year = '$year'")->result_array();
			if (empty($waiver_setting_info_check)) {
				$sdata['exception'] = "Please add waiver configuration first";
				$this->session->set_userdata($sdata);
				redirect("student_guardian_panels/fees_payment");
			}


			$collection_month = $this->input->post('collection_month', true);
			$mode_of_pay = "B";
			$num_of_row = $this->input->post('num_of_row', true);
//			echo '<pre>';
//			print_r($_POST);
//			die;
			//echo $num_of_row; die;


			$is_resident_transaction = 0;
			$is_resident_info = $this->db->query("SELECT id FROM tbl_student_resident_info WHERE student_id = '$student_id' AND year = '$year'")->result_array();
			if (!empty($is_resident_info)) {
				$is_resident_transaction = 1;
			}


			$transaction_id_for_payment = $this->generateSMSTransactionID();

			$mdata = array();
			$mdata['class_id'] = $class_id;
			$mdata['section_id'] = $section_id;
			$mdata['group_id'] = $group_id;
			$mdata['shift_id'] = $shift_id;
			$mdata['student_category_id'] = $student_category_id;
			$mdata['student_id'] = $student_id;
			$mdata['year'] = $year;
			$mdata['month'] = $collection_month; //its collection month
			$mdata['mode_of_pay'] = $mode_of_pay;
			$mdata['receipt_no'] = $posted_receipt_no;
			$mdata['date'] = $manual_collection_date;
			$mdata['is_resident_transaction'] = $is_resident_transaction;
			$mdata['total_discount'] = $this->input->post('total_discount', true);
			$mdata['total_paid_amount'] = $this->input->post('total_paid_amount', true);
			$mdata['transaction_id'] = $transaction_id_for_payment;
			$mdata['entry_date'] = date('Y-m-d H:i:s');
			$mdata['do_you_collect_late_fee'] = 'N';

			if ($this->db->insert('tbl_fee_collection_draft_for_opt', $mdata)) {
				$collection_id = $this->db->insert_id();
				//echo $collection_id;die;
				$i = 1;
				while ($i <= $num_of_row) {
					$cdata = array();
					$cdata['collection_id'] = $collection_id;
					$cdata['class_id'] = $class_id;
					$cdata['group_id'] = $group_id;
					$cdata['shift_id'] = $shift_id;
					$cdata['student_category_id'] = $student_category_id;
					$cdata['section_id'] = $section_id;
					$cdata['student_id'] = $student_id;
					$cdata['month'] = $this->input->post('month_' . $i);
					$cdata['year'] = $year;
					$cdata['category_id'] = $this->input->post('category_id_' . $i);
					$cdata['sub_category_id'] = $this->input->post('sub_category_id_' . $i);
					$cdata['waiver_amount'] = $this->input->post('waiver_amount_' . $i);
					$cdata['actual_amount'] = $this->input->post('actual_amount_' . $i);
					$cdata['discount_amount'] = $this->input->post('discount_amount_' . $i);
					$cdata['paid_amount'] = $this->input->post('paid_amount_' . $i);
					$cdata['due_amount'] = 0;
					$cdata['entry_date'] = date('Y-m-d H:i:s');
					$this->db->insert('tbl_fee_collection_details_draft_for_opt', $cdata);
					$i++;
				}
			}


			//service charge calculation
			$service_charge_calculation = $this->calculate_service_charge($this->input->post('total_paid_amount', true));
			//service charge calculation

			//transport fee save end
			$this->session->set_userdata('transaction_id_for_payment', $transaction_id_for_payment);
			//$this->process_fees_collection_for_main_table('bkash_api_test');
			$this->payment_validate($service_charge_calculation['total_paid_amount'],$transaction_id_for_payment);
		}
	}

	public function process_fees_collection_for_main_table($payment_type){
		$transaction_id_for_payment = $this->session->userdata('transaction_id_for_payment');
		$this->payment_data_save($transaction_id_for_payment,$payment_type);
		$this->session->unset_userdata('transaction_id_for_payment');
		$sdata['message'] = "You are successfully payment completed";
		$this->session->set_userdata($sdata);
		redirect("student_guardian_panels/fees_payment");

	}

	public function data_send_to_family_portal($service_charge_percentage,$service_charge_amount,$amount,$transaction_id,$receipt_no,$payment_type){
		$family_db = $this->load->database('family_db', true);
		$data = array();
		$data['school_code'] = SCHOOL_NAME;
		$data['transaction_id'] = $transaction_id;
		$data['service_charge_percentage'] = $service_charge_percentage;
		$data['service_charge_amount'] = $service_charge_amount;
		$data['amount'] = $amount;
		$data['total_paid_amount'] = $amount + $service_charge_amount;
		$data['receipt_no'] = $receipt_no;
		$data['payment_type'] = $payment_type;
		$data['date_time'] = date('Y-m-d H:i:s');
		$family_db->insert('tbl_student_online_payment', $data);
		$family_db->close();
		$this->load->database('default', true);
		return true;
	}

	public function calculate_service_charge($amount){
		$service_charge_percentage = 2;
		$service_charge_amount = ceil($amount * $service_charge_percentage / 100);
		$total_paid_amount = $amount + $service_charge_amount;
		$data = array();
		$data['service_charge_percentage'] = $service_charge_percentage;
		$data['service_charge_amount'] = $service_charge_amount;
		$data['total_paid_amount'] = $total_paid_amount;
		return $data;
	}

	public function payment_data_save($transaction_id_for_payment,$payment_type){

		$collection_info = $this->db->query("SELECT * FROM `tbl_fee_collection_draft_for_opt` 
                            WHERE `transaction_id` = '$transaction_id_for_payment'")->row();

		$draft_collection_id = $collection_info->id;
		$collection_details = $this->db->query("SELECT * FROM `tbl_fee_collection_details_draft_for_opt` 
                            WHERE `collection_id` = $draft_collection_id")->result_array();

		$payment_gateway_config = $this->Admin_login->get_online_payment_gateway_setting();

		$mdata = array();
		$mdata['class_id'] = $collection_info->class_id;
		$mdata['section_id'] = $collection_info->section_id;
		$mdata['group_id'] = $collection_info->group_id;
		$mdata['shift_id'] = $collection_info->shift_id;
		$mdata['student_category_id'] = $collection_info->student_category_id;
		$mdata['student_id'] = $collection_info->student_id;
		$mdata['year'] = $collection_info->year;
		$mdata['month'] = $collection_info->month;
		$mdata['mode_of_pay'] = $collection_info->mode_of_pay;
		$mdata['receipt_no'] = $collection_info->receipt_no;
		$mdata['date'] = $collection_info->date;
		$mdata['is_resident_transaction'] = $collection_info->is_resident_transaction;
		$mdata['total_discount'] = $collection_info->total_discount;
		$mdata['total_paid_amount'] = $collection_info->total_paid_amount;
		$mdata['entry_date'] = $collection_info->entry_date;
		$mdata['do_you_collect_late_fee'] = $collection_info->do_you_collect_late_fee;
		$mdata['payment_type'] = "$payment_type";
		$mdata['transaction_id'] = $transaction_id_for_payment;
		$mdata['is_online_payment'] = '1';

		//service charge calculation
		$service_charge_calculation = $this->calculate_service_charge($collection_info->total_paid_amount);
		$mdata['service_charge_percentage'] = $service_charge_calculation['service_charge_percentage'];
		$mdata['service_charge_amount'] = $service_charge_calculation['service_charge_amount'];
		//service charge calculation
		$mdata['is_payslip'] = '0';
		$mdata['collection_status'] = '1';
		$mdata['user_id'] = $payment_gateway_config->user_id_for_payment;

		if ($this->db->insert('tbl_fee_collection', $mdata)) {
			$collection_id = $this->db->insert_id();
			$i = 0;
			foreach ($collection_details as $row) {
				$cdata = array();
				$cdata['collection_id'] = $collection_id;
				$cdata['class_id'] = $row['class_id'];
				$cdata['group_id'] = $row['group_id'];
				$cdata['shift_id'] = $row['shift_id'];
				$cdata['student_category_id'] = $row['student_category_id'];
				$cdata['section_id'] =  $row['section_id'];
				$cdata['student_id'] = $row['student_id'];
				$cdata['month'] = $row['month'];
				$cdata['year'] = $row['year'];
				$cdata['category_id'] = $row['category_id'];
				$cdata['sub_category_id'] = $row['sub_category_id'];
				$cdata['waiver_amount'] = $row['waiver_amount'];
				$cdata['actual_amount'] = $row['actual_amount'];
				$cdata['discount_amount'] = $row['discount_amount'];
				$cdata['paid_amount'] = $row['paid_amount'];
				$cdata['entry_date'] = $row['entry_date'];
				$cdata['collection_status'] = '1';
				$cdata['due_amount'] = 0;
				$this->db->insert('tbl_fee_collection_details', $cdata);
				$i++;
			}

			//asset entry
			$assetdata = array();
			$assetdata['user_id'] = $payment_gateway_config->user_id_for_payment;
			$assetdata['asset_id'] = $payment_gateway_config->asset_head_id_for_student_payment;
			$assetdata['amount'] = $collection_info->total_paid_amount;
			$assetdata['type'] = 'I';
			$assetdata['from_transaction_type'] = 'SF';
			$assetdata['date'] = $collection_info->date;
			$assetdata['reference_id'] = $collection_id;
			$assetdata['added_by'] = $payment_gateway_config->user_id_for_payment;
			$assetdata['remarks'] = 'From Online Student Fees (' . $collection_info->receipt_no . ')';
			$this->db->insert('tbl_user_wise_asset_transaction', $assetdata);
			//asset entry

			$this->db->query("DELETE FROM tbl_fee_collection_details_draft_for_opt WHERE `collection_id` = $draft_collection_id");
			$this->db->query("DELETE FROM tbl_fee_collection_draft_for_opt WHERE `id` = $draft_collection_id");
		}

		$this->data_send_to_family_portal($service_charge_calculation['service_charge_percentage'],$service_charge_calculation['service_charge_amount'],$collection_info->total_paid_amount,$transaction_id_for_payment,$collection_info->receipt_no,$payment_type);

		return true;
	}


	public function payment_validate($amount,$transaction_id_for_payment)
	{
		$return_url = base_url() . 'student_guardian_panels/payment_return';
		$Unique_ID = 'STX' . $transaction_id_for_payment;
		//  echo $Unique_ID; die;

		$ch = curl_init();
		$options = array( 'Merchant_Username'=>'scthree', 'Merchant_password'=>'eNETfqJ9ZP7n');
		$uniq_transaction_key = $Unique_ID.uniqid();//Given By Shurjumukhi Limited

		$clientIP = $_SERVER['REMOTE_ADDR'];

		$xml_data = 'spdata=<?xml version="1.0" encoding="utf-8"?>
                                      <shurjoPay><merchantName>'.$options['Merchant_Username'].'</merchantName>
                                      <merchantPass>'.$options['Merchant_password'].'</merchantPass>
                                      <userIP>'.$clientIP.'</userIP>
                                      <uniqID>'.$uniq_transaction_key.'</uniqID>
                                      <totalAmount>'.$amount.'</totalAmount>
                                      <paymentOption>shurjopay</paymentOption>
                                      <returnURL>' . $return_url . '</returnURL></shurjoPay>';


		$url = "https://shurjopay.com/sp-data.php";
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
		curl_setopt($ch,CURLOPT_POSTFIELDS,$xml_data);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		print_r($response);
		curl_close ($ch);
	}

	public function payment_return()
	{
		if(count($_POST)>0) {
			$response_encrypted = $_POST['spdata'];
			$student_info = $this->session->userdata('session_student_info');
			$student_id = $student_info[0]->id;

			$fp = fopen('return.txt', 'a');
			$e = $response_encrypted."\n";
			fwrite($fp,$response_encrypted);
			fclose($fp);

			$response_decrypted = file_get_contents("https://shurjopay.com/merchant/decrypt.php?data=".$response_encrypted);
			$data= simplexml_load_string($response_decrypted) or die("Error: Cannot create object");
			// echo '<pre>';
			// print_r($data);
			// die;
			$fp = fopen('return.txt', 'a');
			$d = $response_decrypted."\n";
			fwrite($fp,$response_decrypted);
			fclose($fp);

			$tx_id = $data->txID;
			$bank_tx_id = $data->bankTxID;
			$bank_status = $data->bankTxStatus;
			$sp_code = $data->spCode;
			$sp_code_des = $data->spCodeDes;
			$sp_payment_option = $data->paymentOption;

			$transaction_id = $tx_id;
			$amount = $data->txnAmount;


			switch($sp_code) {
				case '000':
					$res = array('status'=>true,'msg'=>'Action Successful');
					break;
				case '001':
					$res = array('status'=>false,'msg'=>'Action Failed');
					break;
			}

			if(!isset($res)){
				$res['status'] = false;
			}

			$msg = "Not actual message found from API";
			if(isset($res) && isset($res['msg'])){
				$msg = $res['msg'];
			}

			if($res['status']) {
				$data = array();
				$data['student_id'] = $student_id;
				$data['amount'] = $amount;
				$data['transaction_id'] = "$tx_id";
				$data['payment_option'] = "$sp_payment_option";
				$data['msg'] = $msg;
				$data['date_time'] = date('Y-m-d H:i:s');
				$this->db->insert('tbl_gateway_payment_history', $data);

				$payment_type = $sp_payment_option;

				$this->process_fees_collection_for_main_table($payment_type);
			} else {
				//echo "Fail - " . $tx_id; //header("location:".$failed_url."?status=failed&msg=".$res['msg']."&id=".$tx_id);
				//die();
				$data = array();
				$data['student_id'] = $student_id;
				$data['amount'] = $amount;
				$data['transaction_id'] = "$tx_id";
				$data['msg'] = $msg;
				$data['date_time'] = date('Y-m-d H:i:s');
				$this->db->insert('tbl_gateway_payment_error', $data);

				$transaction_id_for_payment = $this->session->userdata('transaction_id_for_payment');
				$collection_info = $this->db->query("SELECT id FROM `tbl_fee_collection_draft_for_opt` 
                            WHERE `transaction_id` = '$transaction_id_for_payment'")->row();
				$draft_collection_id = $collection_info->id;
				$this->db->query("DELETE FROM tbl_fee_collection_details_draft_for_opt WHERE `collection_id` = $draft_collection_id;");
				$this->db->query("DELETE FROM tbl_fee_collection_draft_for_opt WHERE `id` = $draft_collection_id;");
				$this->session->unset_userdata('transaction_id_for_payment');

				$sdata['exception'] = "Your payment could not be made. (" . $msg . ")";
				$this->session->set_userdata($sdata);
				redirect("student_guardian_panels/fees_payment");
			}
		}
	}



	function generateSMSTransactionID(){
		$date = new DateTime();
		$code = $date->getTimestamp();
		$total_collection = count($this->db->query("SELECT id FROM tbl_fee_collection")->result_array());
		$code .= $total_collection;
		$code .= $this->generateRandomString(5);
		return  $code;
	}

	function generateRandomString($length = 2) {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}



	public function index()
	{
		$student_info = $this->session->userdata('session_student_info');
		$student_id = $student_info[0]->id;
		$from_date = date('Y-m').'-01';
		$to_date = date('Y-m').'-31';
		$data = array();
		$data['active_menu'] = 'dashboard';
		$data['title'] = 'Student Dashboard';
		$data['heading_msg'] = "Student Dashboard";
		$data['present'] = count($this->db->query("SELECT * FROM `tbl_student_logins` AS l WHERE l.`student_id` = $student_id AND l.`login_status` = 'P' AND
(l.`date` BETWEEN '$from_date' AND '$to_date')")->result_array());
		$data['absent'] = count($this->db->query("SELECT * FROM `tbl_student_logins` AS l WHERE l.`student_id` = $student_id AND l.`login_status` = 'A' AND
(l.`date` BETWEEN '$from_date' AND '$to_date')")->result_array());
		$data['leave'] = count($this->db->query("SELECT * FROM `tbl_student_logins` AS l WHERE l.`student_id` = $student_id AND l.`login_status` = 'L' AND
(l.`date` BETWEEN '$from_date' AND '$to_date')")->result_array());

		$data['chart_level'] = 'Present' . ',' . 'Absent' . ',' . 'Leave';
		$data['chart_color'] = '#009900' . ',' . '#ff0000' . ',' . '#e6b800';
		$data['chart_value'] = $data['present'] . ',' . $data['absent'] . ',' . $data['leave'];
		$data['dashboard_notice'] = $this->db->query("SELECT * FROM tbl_notice ORDER BY id DESC LIMIT 6")->result_array();
		$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
		$data['maincontent'] = $this->load->view('student_guardian_panels/dashboard', $data, true);
		$this->load->view('student_guardian_panels/index', $data);
	}

	function timekeeping_report(){
		$data = array();
		if($_POST){
			$month = $this->input->post('month', true);
			$year = $this->input->post('year', true);
			$period_id = $this->input->post("period_id");
			$data['month'] = $month;
			$data['year'] = $year;
			$data['period_id'] = $period_id;
			$student_id = $this->input->post('student_id', true);
			$from_date = $year.'-'.$month.'-01';
			$to_date = $year.'-'.$month.'-31';
			$data['timekeeping_info'] = $this->db->query("SELECT * FROM `tbl_student_logins` AS l
 WHERE l.`student_id` = $student_id AND l.`period_id` = $period_id AND
(l.`date` BETWEEN '$from_date' AND '$to_date')  ORDER BY l.`date`")->result_array();
		}
		$student_info = $this->session->userdata('session_student_info');
		$data['student_id'] = $student_info[0]->id;
		$data['active_menu'] = 'timekeeping';
		$data['title'] = 'Student Attendance Report';
		$data['heading_msg'] = "Student Attendance Report";
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['period_list'] = $this->db->query("SELECT * FROM `tbl_class_period`")->result_array();
		$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
		$data['maincontent'] = $this->load->view('student_guardian_panels/timekeeping_report_form', $data, true);
		$this->load->view('student_guardian_panels/index', $data);
	}

	function get_student_fees_info(){
		$data = array();
		$data['title'] = 'Student Fees Information';
		$data['heading_msg'] = "Student Fees Information";
		if($_POST){
			$SchoolInfo = $this->db->query("SELECT * FROM `tbl_contact_info`")->result_array();
			$Info = array();
			$Info['school_name'] = $SchoolInfo[0]['school_name'];
			$Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
			$data['HeaderInfo'] = $Info;
			$student_info = $this->session->userdata('session_student_info');
			$student_id = $student_info[0]->id;
			$data['student_id'] = $student_id;
			$data['student_info'] = $this->db->query("SELECT s.`name`,s.`roll_no`,c.`name` AS class_name,sf.`name` AS shift_name FROM `tbl_student` AS s
LEFT JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
LEFT JOIN `tbl_shift` AS sf ON s.`shift_id` = sf.`id`
WHERE s.`id` = '$student_id'")->result_array();
			$data['year'] = $this->input->post('year', true);
			$fees_para_data = array();
			$fees_para_data['year'] = $data['year'];
			$fees_para_data['student_id'] = $data['student_id'];
			$data['sub_category'] = $this->db->query("SELECT * FROM tbl_fee_sub_category ORDER BY id")->result_array();
			$data['rdata'] = $this->Student_fee->getStudentFeeStatusDetailsStandard($fees_para_data);
			$data['report'] = $this->load->view('student_fee_status_details/report', $data, true);
		}

		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$student_info = $this->session->userdata('session_student_info');
		$data['student_id'] = $student_info[0]->id;
		$data['active_menu'] = 'student_fees';
		$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
		$data['maincontent'] = $this->load->view('student_guardian_panels/get_student_fees_info', $data, true);
		$this->load->view('student_guardian_panels/index', $data);
	}

	function get_student_info()
	{
		$student_info = $this->session->userdata('session_student_info');
		$id = $student_info[0]->id;
		$data = array();
		$data['title'] = 'Student Profile';
		$data['heading_msg'] = "Student Profile";
		$data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
		$data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
		$data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
		$data['group_list'] = $this->Admin_login->getGroupList();
		$data['student_info_by_id'] = $this->Student->get_student_info_by_student_id($id);
		$year = $data['student_info_by_id'][0]['year'];
//        echo '<pre>';
//        print_r($data['student_info_by_id']);
//        die;

		$data['result_info_for_current_year'] = $this->db->query("SELECT e.`name`,r.`gpa_with_optional`,r.`c_alpha_gpa_with_optional`,
r.`total_obtain_mark`,r.`total_credit`,r.`class_position` FROM `tbl_result_process` AS r 
INNER JOIN `tbl_exam` AS e ON e.`id` = r.`exam_id`
WHERE r.`student_id` = $id AND r.`year` = $year")->result_array();

		$data['subject_list_by_student'] = $this->db->query("SELECT
  su.*,
  s.`is_optional`
FROM
  `tbl_student_wise_subject` AS s
  LEFT JOIN `tbl_subject` AS su
    ON su.`id` = s.`subject_id`
WHERE s.`student_id` = '$id' AND s.`year` = '$year'")->result_array();
		$fees_para_data = array();
		$fees_para_data['class_id'] = $data['student_info_by_id'][0]['class_id'];
		$fees_para_data['group_id'] =  $data['student_info_by_id'][0]['group'];
		$fees_para_data['section_id'] =  $data['student_info_by_id'][0]['section_id'];
		$fees_para_data['student_id'] =  $data['student_info_by_id'][0]['id'];
		$fees_para_data['year'] =  $data['student_info_by_id'][0]['year'];
		$data['sub_category'] = $this->db->query("SELECT * FROM tbl_fee_sub_category ORDER BY id")->result_array();
		$data['rdata'] = $this->Student_fee->getStudentFeeStatusDetailsStandard($fees_para_data);
		$data['fess_data_report'] = $this->load->view('student_fee_status_details/report', $data, true);
		$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
		$data['maincontent'] = $this->load->view('student_guardian_panels/student_profile', $data, true);
		$this->load->view('student_guardian_panels/index', $data);
	}


	function get_student_leave_info()
	{
		$data = array();
		$data['active_menu'] = 'leave_info';
		$data['title'] = 'Student Leave';
		$data['heading_msg'] = "Student Leave";
		$cond = array();
		$this->load->library('pagination');
		$config['base_url'] = site_url('student_guardian_panels/get_student_leave_info/');
		$config['per_page'] = 20;
		$config['total_rows'] = count($this->Timekeeping->get_all_student_leave_list(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['leaves'] = $this->Timekeeping->get_all_student_leave_list(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
		$data['maincontent'] = $this->load->view('student_guardian_panels/student_leave_list', $data, true);
		$this->load->view('student_guardian_panels/index', $data);
	}

	function get_student_leave_application()
	{
		if ($_POST) {
			$student_id = $this->input->post('student_id', true);
			$date_from = $this->input->post('txtFromDate', true);
			$date_to = $this->input->post('txtToDate', true);
			$login_info = $this->db->query("SELECT id FROM tbl_logins WHERE person_id = '$student_id' AND person_type = 'S'
AND (date BETWEEN '$date_from' AND '$date_to')")->result_array();
			if (!empty($login_info)) {
				$sdata['exception'] = "Login Information Found for this student !";
				$this->session->set_userdata($sdata);
				redirect("student_guardian_panels/get_student_leave_application");
			}
			$data = array();
			$data['student_id'] = $student_id;
			$data['class_id'] = $this->input->post('class_id', true);
			$data['section_id'] = $this->input->post('section_id', true);
			$data['group'] = $this->input->post('group', true);
			$data['date_from'] = $date_from;
			$data['date_to'] = $date_to;
			$data['status'] = 0;
			$data['syn_date'] = date('Y-m-d');
			$data['reason'] = $this->input->post('txtReason', true);
			$this->db->insert('tbl_student_leave_applications', $data);
			$sdata['message'] = "You are Successfully Added Leave Application";
			$this->session->set_userdata($sdata);
			redirect("student_guardian_panels/get_student_leave_application");
		}
		$student_info = $this->session->userdata('session_student_info');
		$data = array();
		$data['active_menu'] = 'leave_info';
		$data['title'] = 'Student Leave';
		$data['heading_msg'] = "Student Leave";
		$data['student_id'] = $student_info[0]->id;
		$data['name'] = $student_info[0]->name;
		$data['student_code'] = $student_info[0]->student_code;
		$data['class_id'] = $student_info[0]->class_id;
		$data['section_id'] = $student_info[0]->section_id;
		$data['group'] = $student_info[0]->group;
		$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
		$data['maincontent'] = $this->load->view('student_guardian_panels/student_leave_add', $data, true);
		$this->load->view('student_guardian_panels/index', $data);
	}


	function get_all_notice()
	{
		$data = array();
		$data['title'] = 'Notice';
		$data['active_menu'] = 'message';
		$cond = array();
		$this->load->library('pagination');
		$config['base_url'] = site_url('student_guardian_panels/get_all_notice');
		$config['per_page'] = 20;
		$config['total_rows'] = count($this->Admin_login->get_all_notice(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['notices'] = $this->Admin_login->get_all_notice(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
		$data['maincontent'] = $this->load->view('student_guardian_panels/notice_list', $data, true);
		$this->load->view('student_guardian_panels/index', $data);
	}


	function change_password()
	{
		if ($_POST) {
			$student_id = $this->input->post("student_id");
			$password = $this->input->post("current_pass");
			$password = md5($password);
			$is_authenticated = $this->custom_methods_model->num_of_data("tbl_student", " WHERE id='$student_id' AND password='$password'");

			if ($is_authenticated > 0) {
				$data = array();
				$data['password'] = md5($this->input->post("new_pass"));
				$data['id'] = $student_id;
				$this->db->where('id', $data['id']);
				$this->db->update('tbl_student', $data);
				$sdata['message'] = "You are Successfully Password Changes";
				$this->session->set_userdata($sdata);
				redirect("student_guardian_panels/change_password");
			} else {
				$sdata['exception'] = "Sorry Current Password Does'nt Match !";
				$this->session->set_userdata($sdata);
				redirect("student_guardian_panels/change_password");
			}
		} else {
			$data = array();
			$data['title'] = 'Change Password';
			$data['heading_msg'] = "Change Password";
			$student_info = $this->session->userdata('session_student_info');
			$data['student_id'] = $student_info[0]->id;
			$data['name'] = $student_info[0]->name;
			$data['student_code'] = $student_info[0]->student_code;
			$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
			$data['maincontent'] = $this->load->view('student_guardian_panels/change_password', $data, true);
			$this->load->view('student_guardian_panels/index', $data);
		}
	}


}


?>
