<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Class_wise_subjects extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student','Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Exam Mark Configuration';
        $data['heading_msg'] = "Exam Mark Configuration";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $ClassList = $this->db->query("SELECT * FROM `tbl_class` ORDER BY `student_code_short_form`;")->result_array();
        $ExamTypeList = $this->db->query("SELECT * FROM `tbl_exam_type`")->result_array();
        if (empty($ExamTypeList)) {
            $sdata['exception'] = "Please add exam type first.";
            $this->session->set_userdata($sdata);
            redirect('exam_types/index');
        }
        $data['ExamTypeList'] = $ExamTypeList;
        $subject_info = array();
        for ($i = 0; $i < count($ClassList); $i++) {
            $ClassID = $ClassList[$i]['id'];

            foreach ($ExamTypeList as $exam_type_row) {
                $exam_type_id = $exam_type_row['id'];
                $subject_info[$i]['class_name'] = $ClassList[$i]['name'];
                $subject_info[$i]['class_id'] = $ClassList[$i]['id'];
                $subject_info[$i]['exam_type_'.$exam_type_id]['exam_type_id'] = $exam_type_id;
                $subject_info[$i]['exam_type_'.$exam_type_id]['exam_type_name'] = $exam_type_row['name'];
                $info = $this->db->query("SELECT s.`name` AS subject_name, c.* FROM `tbl_class_wise_subject` AS c
					  LEFT JOIN `tbl_subject` AS s ON s.`id` = c.`subject_id`
					WHERE c.`class_id` = '$ClassID' AND c.`exam_type_id` = '$exam_type_id' ORDER BY s.`name` ")->result_array();

                if (!empty($info)) {
                    $MyString = '';
                    for ($k = 0; $k < count($info); $k++) {
                        $MyString = $MyString . ', ' . $info[$k]['subject_name'].' (' . $info[$k]['credit'] . ')';
                    }
                    $MyString = substr($MyString, 2);
                    $subject_info[$i]['exam_type_'.$exam_type_id]['subject_name'] = $MyString;
                } else {
                    $subject_info[$i]['exam_type_'.$exam_type_id]['subject_name'] = 'N/A';
                }
            }
        }
        // echo "<pre>"; print_r($subject_info); die();
        $data['subject_info'] = $subject_info;
		$data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
        $data['maincontent'] = $this->load->view('class_wise_subjects/class_wise_subject_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($class_id, $exam_type_id)
    {
        if ($_POST) {
//        	echo '<pre>';
//        	print_r($_POST);
//        	die;
            //echo $class_id.'/'.$exam_type_id; die;
            $loop_time = $this->input->post("loop_time");
            if ($loop_time > 0) {
                $this->db->query("DELETE FROM tbl_class_wise_subject WHERE `class_id` = '$class_id' AND `exam_type_id` = '$exam_type_id'");
                $i = 0;
                while ($i < $loop_time) {
                    if ($this->input->post("is_allow_" . $i)) {
                        $data = array();
                        $data['subject_id'] = $_POST['subject_id_' . $i];
                        $data['credit'] = $_POST['credit_' . $i];
                        $data['exam_type_id'] = $exam_type_id;
                        $data['class_id'] = $class_id;

                        if ($_POST['class_test_' . $i] == '') {
                            $data['class_test'] = 0;
                        } else {
                            $data['class_test'] = $_POST['class_test_' . $i];
                        }

                        if ($_POST['written_' . $i] == '') {
                            $data['written'] = 0;
                        } else {
                            $data['written'] = $_POST['written_' . $i];
                        }


                        if ($_POST['objective_' . $i] == '') {
                            $data['objective'] = 0;
                        } else {
                            $data['objective'] = $_POST['objective_' . $i];
                        }

                        if ($_POST['practical_' . $i] == '') {
                            $data['practical'] = 0;
                        } else {
                            $data['practical'] = $_POST['practical_' . $i];
                        }

                        $data['order_number'] = $_POST['orderNumber_' . $i];
                        $data['merge_code'] = $_POST['merge_code_' . $i];
                        $data['is_class_test_allow'] = $_POST['isClassTestAllow_' . $i];
                        $data['is_written_allow'] = $_POST['isWrittenAllow_' . $i];
                        $data['is_objective_allow'] = $_POST['isObjectiveAllow_' . $i];
                        $data['is_practical_allow'] = $_POST['isPracticalAllow_' . $i];
                        if (empty($_POST['groups_' . $i])) {
                            $sdata['exception'] = "Minimum one group must be given";
                            $this->session->set_userdata($sdata);
                            redirect('class_wise_subjects/index');
                        }
                        $data['group_list'] = implode(",", $_POST['groups_' . $i]);

                        $data['class_test_pass_marks'] = $_POST['class_test_pass_marks_' . $i];
                        $data['written_pass_marks'] = $_POST['written_pass_marks_' . $i];
                        $data['objective_pass_marks'] = $_POST['objective_pass_marks_' . $i];
                        $data['practical_pass_marks'] = $_POST['practical_pass_marks_' . $i];

						$data['acceptance_written'] = $_POST['acceptance_written_' . $i];
						$data['acceptance_objective'] = $_POST['acceptance_objective_' . $i];
						$data['acceptance_practical'] = $_POST['acceptance_practical_' . $i];
						$data['acceptance_class_test'] = $_POST['acceptance_class_test_' . $i];

						$data['subject_type'] = $_POST['subject_type_' . $i];

                        //echo '<pre>';
                        //print_r($data); die;
                        $this->db->insert("tbl_class_wise_subject", $data);
                    }
                    $i++;
                }

                //class wise marking head config
				$this->db->query("DELETE FROM tbl_class_wise_subject_marking_head
                                 WHERE `class_id` = '$class_id' AND `exam_type_id` = '$exam_type_id'");

                $marking_head_data = array();
				$marking_head_data['class_id'] = $class_id;
				$marking_head_data['exam_type_id'] = $exam_type_id;

                if(isset($_POST['is_class_test_allow_for_main_head'])){
					$marking_head_data['is_class_test_allow'] = '1';
				}else{
					$marking_head_data['is_class_test_allow'] = '0';
				}

				if(isset($_POST['is_written_allow_for_main_head'])){
					$marking_head_data['is_written_allow'] = '1';
				}else{
					$marking_head_data['is_written_allow'] = '0';
				}

				if(isset($_POST['is_objective_allow_for_main_head'])){
					$marking_head_data['is_objective_allow'] = '1';
				}else{
					$marking_head_data['is_objective_allow'] = '0';
				}

				if(isset($_POST['is_practical_allow_for_main_head'])){
					$marking_head_data['is_practical_allow'] = '1';
				}else{
					$marking_head_data['is_practical_allow'] = '0';
				}
				$this->db->insert("tbl_class_wise_subject_marking_head", $marking_head_data);
				//end class wise marking head config

                $sdata['message'] = "Class Wise Subject Added Successfully.";
                $this->session->set_userdata($sdata);
                redirect('class_wise_subjects/index');
            } else {
                $sdata['message'] = "No Subject Added.";
                $this->session->set_userdata($sdata);
                redirect('class_wise_subjects/index');
            }
        }
        $data = array();
        $data['class'] = $class_id;
        $data['exam_type_id'] = $exam_type_id;
        $class_info = $this->db->query("SELECT * FROM `tbl_class` WHERE `id` = '$class_id'")->result_array();
        $exam_type_info = $this->db->query("SELECT * FROM `tbl_exam_type` WHERE `id` = '$exam_type_id'")->result_array();
        $data['title'] = 'Select Subjects for ' . $class_info[0]['name']. '(' . $exam_type_info[0]['name'] . ')';
        $data['heading_msg'] = 'Select Subjects for ' . $class_info[0]['name']. '(' . $exam_type_info[0]['name'] . ')';
        $data['subject'] = $this->getClassAndExamTypeWiseSubject($class_id, $exam_type_id);


		if (empty($data['subject'])) {
			$sdata['exception'] = "Please complete class subject mapping first";
			$this->session->set_userdata($sdata);
			redirect('class_wise_subject_mappings/index#basic_config');
		}

	    	$data['class_wise_marking_heads'] = $this->Admin_login->getClassWiseSubjectMarkingHead($class_id, $exam_type_id);
//        echo '<pre>';
//        print_r($data['subject']); die;
        $data['is_show_button'] = "index";

        $data['main_marking_heads'] = $this->Admin_login->get_main_marking_head($class_id);
        if(empty($data['main_marking_heads'])){
          $sdata['exception'] = "Please set class wise marking head";
    			$this->session->set_userdata($sdata);
    			redirect('main_marking_heads/edit/' . $class_id);
        }

        $data['groups'] = $this->Admin_login->getGroupList();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('class_wise_subjects/class_wise_subject_edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    function getClassAndExamTypeWiseSubject($class_id, $exam_type_id){
		return $this->db->query("SELECT s.*, IF(h.subject_id IS NULL, '0', '1') AS is_checked, h.credit AS credit,
			h.`class_test`,h.`written`,h.`objective`,h.`practical`,h.`order_number`,h.`group_list`,h.`merge_code`,
			h.`class_test_pass_marks`,h.`written_pass_marks`,h.`objective_pass_marks`,h.`practical_pass_marks`,
			h.`is_class_test_allow`,h.`is_written_allow`,h.`is_objective_allow`,h.`is_practical_allow`,
			h.`acceptance_written`,h.`acceptance_objective`,h.`acceptance_practical`,h.`acceptance_class_test`,
			sm.`subject_type`,sm.`group_list` AS mapping_group_list FROM
			`tbl_subject` AS s
			LEFT JOIN `tbl_class_wise_subject_mappings` AS sm ON sm.`subject_id` = s.`id`
			LEFT JOIN (SELECT * FROM `tbl_class_wise_subject` AS c
			WHERE c.`class_id` = $class_id
			AND c.`exam_type_id` = $exam_type_id) AS h ON h.subject_id = s.`id`
			WHERE s.`id` IN (SELECT `subject_id` FROM `tbl_class_wise_subject_mappings`
			 WHERE `class_id` = $class_id) GROUP BY s.`id` ORDER BY s.`name`; ")->result_array();
	}

    function checkAlreadyConfiguredConfig(){
		$to_class_id = $this->input->get('to_class_id', true);
		$to_exam_type_id = $this->input->get('to_exam_type_id', true);
		$check = $this->db->query("SELECT id FROM `tbl_class_wise_subject` WHERE `class_id` = $to_class_id AND `exam_type_id` = $to_exam_type_id;")->result_array();
		if(empty($check)){
			echo '1';
		}else{
			echo '0';
		}
	}

	function data_copy(){
    	if($_POST){
			$from_class_id = $this->input->post('from_class_id', true);
			$from_exam_type_id = $this->input->post('from_exam_type_id', true);
			$to_class_id = $this->input->post('to_class_id', true);
			$to_exam_type_id = $this->input->post('to_exam_type_id', true);

			$subjectList = $this->getClassAndExamTypeWiseSubject($from_class_id, $from_exam_type_id);

			$subjectIdList = '';
			foreach ($subjectList as $subject){
				$subjectIdList .= $subject['id'] . ',';
			}
			$subjectIdList = rtrim($subjectIdList, ',');
			$checkSubjectBetweenTwoClass = $this->db->query("SELECT `id`,`subject_id` FROM `tbl_class_wise_subject_mappings` WHERE `class_id` = $to_class_id
            AND `subject_id` IN ($subjectIdList) GROUP BY `subject_id`;")->result_array();

			if(count($checkSubjectBetweenTwoClass) != count($subjectList)){
				$sdata['exception'] = "Two classes all subjects are not same. Please check class and subject mapping";
				$this->session->set_userdata($sdata);
				redirect('class_wise_subjects/index#result_part');
			}else{

				//data ready and save
				$insert_row_no = 0;
				$save_data = array();

				foreach ($subjectList as $subjectRow){
					if($subjectRow['is_checked'] == '1'){
						$save_data[$insert_row_no]['subject_id'] = $subjectRow['id'];
						$save_data[$insert_row_no]['exam_type_id'] = $to_exam_type_id;
						$save_data[$insert_row_no]['class_id'] = $to_class_id;
						$save_data[$insert_row_no]['credit'] = $subjectRow['credit'];
						$save_data[$insert_row_no]['class_test'] = $subjectRow['class_test'];
						$save_data[$insert_row_no]['written'] = $subjectRow['written'];
						$save_data[$insert_row_no]['objective'] = $subjectRow['objective'];
						$save_data[$insert_row_no]['practical'] = $subjectRow['practical'];
						$save_data[$insert_row_no]['is_class_test_allow'] = $subjectRow['is_class_test_allow'];
						$save_data[$insert_row_no]['is_written_allow'] = $subjectRow['is_written_allow'];
						$save_data[$insert_row_no]['is_objective_allow'] = $subjectRow['is_objective_allow'];
						$save_data[$insert_row_no]['is_practical_allow'] = $subjectRow['is_practical_allow'];
						$save_data[$insert_row_no]['order_number'] = $subjectRow['order_number'];
						$save_data[$insert_row_no]['group_list'] = $subjectRow['group_list'];
						$save_data[$insert_row_no]['merge_code'] = $subjectRow['merge_code'];
						$save_data[$insert_row_no]['class_test_pass_marks'] = $subjectRow['class_test_pass_marks'];
						$save_data[$insert_row_no]['written_pass_marks'] = $subjectRow['written_pass_marks'];
						$save_data[$insert_row_no]['objective_pass_marks'] = $subjectRow['objective_pass_marks'];
						$save_data[$insert_row_no]['practical_pass_marks'] = $subjectRow['practical_pass_marks'];
						$save_data[$insert_row_no]['acceptance_written'] = $subjectRow['acceptance_written'];
						$save_data[$insert_row_no]['acceptance_objective'] = $subjectRow['acceptance_objective'];
						$save_data[$insert_row_no]['acceptance_practical'] = $subjectRow['acceptance_practical'];
						$save_data[$insert_row_no]['acceptance_class_test'] = $subjectRow['acceptance_class_test'];
						$save_data[$insert_row_no]['subject_type'] = $subjectRow['subject_type'];
						$insert_row_no++;
					}
				}
//				echo '<pre>';
//				print_r($save_data);
//				die;
                $this->db->insert_batch('tbl_class_wise_subject', $save_data);
				$sdata['message'] = $this->lang->line('add_success_message');
				$this->session->set_userdata($sdata);
				redirect('class_wise_subjects/index#result_part');

				//end data ready and save
			}
		}
	}
}
