<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Balance_sheets extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Account', 'Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Balance Sheet';
        $data['heading_msg'] = 'Balance Sheet';
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $this->load->library('numbertowords');
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;
            $date = $this->input->post("date");

            $data['date'] = $this->input->post("date");

            $assetData = $this->Account->getAllAssetHeadWithOpeningHead();
            $incomeData = $this->Account->getAllIncomeDataForBalanceSheet($date);
            $expenseData = $this->Account->getAllExpenseDataForBalanceSheet($date);
            $return_data = array();
            $i = 0;
            foreach ($assetData as $asset) {
                $return_data[$i]['asset_id'] = $asset['asset_id'];
                $return_data[$i]['name'] = $asset['name'];
                $return_data[$i]['opening_balance'] = $asset['opening_balance'];
                if (isset($incomeData[$asset['asset_id']])) {
                    $return_data[$i]['total_income'] = $incomeData[$asset['asset_id']]['total_amount'];
                } else {
                    $return_data[$i]['total_income'] = 0;
                }

                if (isset($expenseData[$asset['asset_id']])) {
                    $return_data[$i]['total_expense'] = $expenseData[$asset['asset_id']]['total_amount'];
                } else {
                    $return_data[$i]['total_expense'] = 0;
                }

                $return_data[$i]['balance'] = ($return_data[$i]['opening_balance'] + $return_data[$i]['total_income']) - $return_data[$i]['total_expense'];
                $i++;
            }


            $data['liabilities'] = $this->db->query("SELECT * FROM `tbl_liabilities_category`")->result_array();

            $data['idata'] = $return_data;
            // echo '<pre>';
            // print_r($return_data);
            // die;
            $data['report'] = $this->load->view('balance_sheets/report', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            $data['is_pdf'] = 1;
            //Dom PDF
            $this->load->library('mydompdf');
            $html = $this->load->view('balance_sheets/report', $data, true);
            $this->mydompdf->createPDF($html, 'BalanceSheet', true);
        //Dom PDF
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('balance_sheets/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
