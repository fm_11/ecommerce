<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Inventory_subcategory extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Inventory'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
		$data = array();
		$data['title'] = $this->lang->line('sub').' '.$this->lang->line('category').' '.$this->lang->line('list');
		$data['heading_msg'] = $this->lang->line('sub').' '.$this->lang->line('category').' '.$this->lang->line('list');
		$data['is_show_button'] = "add";
		$cond = array();
		$this->load->library('pagination');
		$config['base_url'] = site_url('inventory_subcategory/index/');
		$config['per_page'] = 20;
		$config['total_rows'] = count($this->Inventory->get_all_sub_category(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['subcategory'] = $this->Inventory->get_all_sub_category(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('inventory_subcategory/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['category_id'] = $_POST['category_id'];
            $data['code'] = $_POST['code'];
            $this->db->insert("inventory_sub_category", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('inventory_subcategory/add');
        }
        $data = array();
        $data['title'] = $this->lang->line('sub').' '.$this->lang->line('category').' '.$this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('sub').' '.$this->lang->line('category').' '.$this->lang->line('add');
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['category'] = $this->db->query("SELECT * FROM inventory_category ")->result_array();
        $data['maincontent'] = $this->load->view('inventory_subcategory/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['category_id'] = $_POST['category_id'];
            $data['code'] = $_POST['code'];
            $this->db->where('id', $data['id']);
            $this->db->update('inventory_sub_category', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('inventory_subcategory/index');
        }
        $data = array();
        $data['title'] = $this->lang->line('sub').' '.$this->lang->line('category').' '.$this->lang->line('edit');
        $data['heading_msg'] = $this->lang->line('sub').' '.$this->lang->line('category').' '.$this->lang->line('edit');
        $data['is_show_button'] = "index";
        $data['category'] = $this->db->query("SELECT * FROM `inventory_category`")->result_array();
        $data['sub_category'] = $this->db->query("SELECT * FROM `inventory_sub_category` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('inventory_subcategory/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
        $this->db->delete('inventory_sub_category', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_sucess_message');
        $this->session->set_userdata($sdata);
        redirect('inventory_subcategory/index');
    }
}
