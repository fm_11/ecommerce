<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Fee_sub_categories extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Message', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Fee Sub Category';
        $data['heading_msg'] = "Fee Sub Category";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['fee_sub_category'] = $this->db->query("SELECT fs.*,fc.`name` AS fee_category_name FROM `tbl_fee_sub_category` AS fs
        LEFT JOIN `tbl_fee_category` AS fc ON fs.`category_id` = fc.`id` ORDER BY fs.`order` = 0, fs.`order`;")->result_array();
        $data['maincontent'] = $this->load->view('fee_sub_categories/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
        	$name = $_POST['name'];
			$check = $this->db->query("SELECT id FROM tbl_fee_sub_category WHERE `name` = '$name'")->result_array();
            if(!empty($check)){
				$sdata['exception'] = $this->lang->line('add_error_message') . ' (Data already exits)';
				$this->session->set_userdata($sdata);
				redirect('fee_sub_categories/add');
			}
			$data = array();
            $data['name'] = $_POST['name'];
            $data['category_id'] = $_POST['category_id'];
            $data['is_waiver_applicable'] = $_POST['is_waiver_applicable'];
			$data['order'] = $_POST['order'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->insert("tbl_fee_sub_category", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('fee_sub_categories/index');
        }
        $data = array();
        $data['title'] = 'Add Fee Sub Category';
        $data['heading_msg'] = "Add Fee Sub Category";
        $data['is_show_button'] = "index";
        $data['action'] = '';
        $data['fee_category_info'] = $this->db->query("SELECT * FROM tbl_fee_category")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('fee_sub_categories/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $id = $_POST['id'];
            $fee_waivers = $this->db->query("SELECT id FROM tbl_student_fee_waiver WHERE fees_category_id = '$id'")->result_array();
            if(!empty($fee_waivers) && $_POST['is_waiver_applicable'] == '0'){
              $sdata['exception'] = "Waiver are found with this category. So you can't remove, Is Waiver Applicable = No";
              $this->session->set_userdata($sdata);
              redirect('fee_sub_categories/index');
            }
            $data['name'] = $_POST['name'];
            $data['category_id'] = $_POST['category_id'];
			$data['order'] = $_POST['order'];
            $data['is_waiver_applicable'] = $_POST['is_waiver_applicable'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->where('id', $id);
            $this->db->update('tbl_fee_sub_category', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('fee_sub_categories/index');
        }
        $data = array();
        $data['title'] = 'Update Fee Sub Category';
        $data['heading_msg'] = "Update Fee Sub Category";
        $data['is_show_button'] = "index";
        $data['action'] = 'edit';
        $data['fee_category_info'] = $this->db->query("SELECT * FROM tbl_fee_category")->result_array();
        $data['fee_sub_category_info'] = $this->db->query("SELECT * FROM tbl_fee_sub_category WHERE `id` = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('fee_sub_categories/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $fee_collection = $this->db->query("SELECT id FROM tbl_fee_collection_details WHERE sub_category_id = '$id'")->result_array();
        if(!empty($fee_collection)){
          $sdata['exception'] = "Fees collection found with this category. So you can't delete this.";
          $this->session->set_userdata($sdata);
          redirect('fee_sub_categories/index');
        }
        $this->db->delete('tbl_fee_sub_category', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("fee_sub_categories/index");
    }
}
