<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Exam_routine_reports extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login', 'Student'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['is_pdf'] = 0;
		$data['title'] = "Exam Routine";
		$data['heading_msg'] = "Exam Routine";
		if ($_POST) {
			$exam_id = $this->input->post("exam_id");
			$year = $this->input->post("year");

			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;

			$data['year'] = $year;
			$data['exam_id'] = $exam_id;

			$exam_info = $this->db->query("SELECT name,year,exam_type_id FROM tbl_exam WHERE id ='$exam_id'")->result_array();
			$data['exam_name'] = $exam_info[0]['name'];
			$data['exam_year'] = $exam_info[0]['year'];

			$exam_date = $this->db->query("SELECT  MIN(`date`) AS firstExamDate, MAX(`date`) AS LastExamDate 
                                 FROM `tbl_exam_routine` WHERE `exam_id` = $exam_id;")->row();

			if ($exam_date->firstExamDate == '' && $exam_date->LastExamDate == '') {
				$sdata['exception'] = "Please routine generate first";
				$this->session->set_userdata($sdata);
				redirect("exam_routine_reports/index");
			}

			$routine_data = array();
			$classes = $this->db->query("SELECT * FROM tbl_class ORDER BY ABS(student_code_short_form)")->result_array();
			$data['classes'] = $classes;
			$exam_sessions = $this->db->query("SELECT * FROM tbl_exam_session ORDER BY id")->result_array();
			$data['exam_sessions'] = $exam_sessions;
			$index = 0;
            $exam_start_date = $exam_date->firstExamDate;
			while (strtotime($exam_start_date) <= strtotime($exam_date->LastExamDate)) {
				$routine_data[$index]['date'] = $exam_start_date;

				foreach ($classes as $class){
					$class_id = $class['id'];
					foreach ($exam_sessions as $exam_session){
						$exam_session_id = $exam_session['id'];
						$routine_data[$index][$class_id][$exam_session_id]['class_name'] = $class['name'];
						$routine_data[$index][$class_id][$exam_session_id]['session_name'] = $exam_session['name'];
						$subjectCheck = $this->db->query("SELECT r.`subject_id`,s.`name`,s.`code` FROM `tbl_exam_routine` AS r
										INNER JOIN `tbl_subject` AS s ON s.`id` = r.`subject_id`
										WHERE r.`exam_id` = $exam_id AND r.`class_id` = $class_id 
										AND r.`date` = '$exam_start_date' AND r.`exam_session_id` = $exam_session_id")->row();

						if(empty($subjectCheck)){
							$routine_data[$index][$class_id][$exam_session_id]['subject_name'] = "-";
							$routine_data[$index][$class_id][$exam_session_id]['subject_code'] = "-";
						}else{
							$routine_data[$index][$class_id][$exam_session_id]['subject_name'] = $subjectCheck->name;
							$routine_data[$index][$class_id][$exam_session_id]['subject_code'] = $subjectCheck->code;
						}
					}
				}

				$index++;
				$exam_start_date = date ("Y-m-d", strtotime("+1 day", strtotime($exam_start_date)));
			}
//			echo '<pre>';
//			print_r($routine_data);
//			die;
			$data['routine_data'] = $routine_data;
			$data['report'] = $this->load->view('exam_routine_reports/routine_view', $data, true);
			$this->load->view('report_content/report_main_content', $data);
		} else {
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('exam_routine_reports/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

}
