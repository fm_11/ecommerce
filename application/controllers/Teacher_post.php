<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Teacher_post extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Teacher_info'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('teachers') . ' ' . $this->lang->line('designation');
        $data['heading_msg'] = $this->lang->line('teachers') . ' ' . $this->lang->line('designation');
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['post'] = $this->db->query("SELECT * FROM tbl_teacher_post ORDER BY shorting_order ASC")->result_array();
        $data['maincontent'] = $this->load->view('teacher_post/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('txtPost', true);
            $data['num_of_post'] = $this->input->post('num_of_post', true);
            $data['shorting_order'] = $this->input->post('shorting_order', true);
            $data['is_teacher'] = $this->input->post('is_teacher', true);
            $this->db->insert('tbl_teacher_post', $data);
            $sdata['message'] =  $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect("teacher_post/add");
        } else {
            $data = array();
            $data['title'] = $this->lang->line('teachers') . ' ' . $this->lang->line('designation') . ' ' . $this->lang->line('add');
            $data['heading_msg'] = $this->lang->line('teachers') . ' ' . $this->lang->line('designation') . ' ' . $this->lang->line('add');
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['is_show_button'] = "index";
            $data['maincontent'] = $this->load->view('teacher_post/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function delete($id)
    {
        $this->db->delete('tbl_teacher_post', array('id' => $id));
        $sdata['message'] =  $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("teacher_post/index");
    }


    public function edit($id=null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('txtPost', true);
            $data['shorting_order'] = $this->input->post('shorting_order', true);
            $data['num_of_post'] = $this->input->post('num_of_post', true);
            $data['is_teacher'] = $this->input->post('is_teacher', true);
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_teacher_post', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("teacher_post/index");
        } else {
            $data = array();
            $data['title'] =  $this->lang->line('teachers') . ' ' . $this->lang->line('designation') . ' ' . $this->lang->line('update');
            $data['heading_msg'] = $this->lang->line('teachers') . ' ' . $this->lang->line('designation') . ' ' . $this->lang->line('update');
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['is_show_button'] = "index";
            $data['post'] = $this->db->query("SELECT * FROM tbl_teacher_post WHERE id='$id'")->result_array();
            $data['maincontent'] = $this->load->view('teacher_post/edit', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
