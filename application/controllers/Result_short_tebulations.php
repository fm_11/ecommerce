
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Result_short_tebulations extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['is_pdf'] = 0;
        $data['title'] = "Result Short Tebulation";
        $data['heading_msg'] = "Result Short Tebulation";
        if ($_POST) {
            $exam_id = $this->input->post("exam");
            $group = $this->input->post("group");
            $year = $this->input->post("year");

            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];


            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['address'] = $SchoolInfo[0]['address'];
            $data['HeaderInfo'] = $Info;

            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['shift_id'] = $shift_id;
            $data['year'] = $year;
            $data['exam_id'] = $exam_id;
            $data['class_shift_section_id'] = $class_shift_section_id;


            $class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
            $data['class_name'] = $class_info[0]['name'];

            $section_info = $this->db->query("SELECT name FROM tbl_section WHERE id ='$section_id'")->result_array();
            $data['section_name'] = $section_info[0]['name'];

            $group_info = $this->db->query("SELECT name FROM tbl_student_group WHERE id ='$group'")->result_array();
            $data['group_name'] = $group_info[0]['name'];

            $shift_info = $this->db->query("SELECT name FROM tbl_shift WHERE id ='$shift_id'")->result_array();
            $data['shift_name'] = $shift_info[0]['name'];

            $exam_info = $this->db->query("SELECT name,year,exam_type_id FROM tbl_exam WHERE id ='$exam_id'")->result_array();
            $data['exam_name'] = $exam_info[0]['name'];
            $data['exam_year'] = $exam_info[0]['year'];
            $exam_type_id =  $exam_info[0]['exam_type_id'];


            $data['result_data'] = $this->Student->result_all_student_after_process($exam_id, $class_id, $section_id, $group, $shift_id, 0);
            //echo '<pre>';
            //print_r($data['result_data']);
            //die;
            if (empty($data['result_data'])) {
                $data = array();
                $sdata['exception'] = "Sorry result info. not found. Please process result first";
                $this->session->set_userdata($sdata);
                redirect("result_short_tebulations/index");
            }

            $data['subject_list'] = $this->db->query("SELECT s.`name`,s.`code`,cs.* FROM `tbl_class_wise_subject` AS cs
  INNER JOIN `tbl_subject` AS s ON s.`id` = cs.`subject_id`
  WHERE cs.`class_id` = '$class_id' AND cs.`exam_type_id` = '$exam_type_id' AND FIND_IN_SET('$group', `group_list`) > 0
  GROUP BY cs.`subject_id` ORDER BY cs.`order_number` ASC")->result_array();

            //echo '<pre>';
            //print_r($data['subject_list']); die;

            if (empty($data['subject_list'])) {
                $sdata['exception'] = "Please check class wise subject and assign group.";
                $this->session->set_userdata($sdata);
                redirect("result_short_tebulations/index");
            }

            //echo $this->db->last_query();
            //echo '<pre>';
            //print_r($data['subject_list']);  die;
            $this->load->view('result_short_tebulations/tabulation_sheet', $data);
        } else {
            $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
            $data['years'] = $this->Admin_login->getYearList(0, 0);
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('result_short_tebulations/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function ajax_exam_by_year()
    {
        $year = $_GET['year'];
        $data = array();
        $data['exam_list'] = $this->db->query("SELECT * FROM `tbl_exam` AS e WHERE e.`year` = '$year' AND e.`status` = '1'")->result_array();
        $this->load->view('result_short_tebulations/exam_list', $data);
    }
}
