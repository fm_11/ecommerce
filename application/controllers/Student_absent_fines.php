<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student_absent_fines extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Message', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    function index()
    {
        $data = array();
        $data['title'] = 'Student Absent Fine';
        $data['heading_msg'] = 'Student Absent Fine';
        $data['is_show_button'] = "add";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_absent_fines/index/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_student_absent_fine(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['students'] = $this->Student_fee->get_all_student_absent_fine(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_absent_fines/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function add()
    {
        if ($_POST) {
            $data = array();
            $data['title'] = 'Student Absent Fine';
            $data['heading_msg'] = "Student Absent Fine";
            $year = $this->input->post("year");
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['student_info'] = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code`,af.`year`,af.`amount`
            FROM `tbl_student` AS s
LEFT JOIN `tbl_fee_absent_fine` AS af ON af.`student_id` = s.`id` AND af.`year` = '$year'
WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group' AND s.`status` = 1 ORDER BY s.roll_no")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/student_absent_fine_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Student Absent Fine';
            $data['heading_msg'] = "Student Absent Fine";
            $data['is_show_button'] = "index";
            $data['action'] = '';
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_absent_fines/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
