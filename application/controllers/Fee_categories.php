<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fee_categories extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login','Message','Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('fees'). ' ' . $this->lang->line('category');
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['fee_category'] = $this->db->query("SELECT * FROM tbl_fee_category")->result_array();
        $data['maincontent'] = $this->load->view('fee_categories/fee_category_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['receipt_no_prefix'] = $_POST['receipt_no_prefix'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->insert("tbl_fee_category", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('fee_categories/index');
        }
        $data = array();
        $data['title'] = $this->lang->line('fees'). ' ' . $this->lang->line('category') . ' ' . $this->lang->line('add');
        $data['action'] = '';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('fee_categories/fee_category_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $id = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['receipt_no_prefix'] = $_POST['receipt_no_prefix'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->where('id', $id);
            $this->db->update('tbl_fee_category', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('fee_categories/index');
        }
        $data = array();
        $data['title'] =  $this->lang->line('fees'). ' ' . $this->lang->line('category') . ' ' . $this->lang->line('update');
        $data['action'] = 'edit';
        $data['is_show_button'] = "index";
        $data['fee_category_info'] = $this->db->query("SELECT * FROM tbl_fee_category WHERE `id` = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('fee_categories/fee_category_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->db->delete('tbl_fee_category', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("fee_categories/index");
    }

}
