<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Student_fee_status_details extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index($student_id = '', $year = '',$class_shift_section_id = '',$group_id = '')
    {
        $data = array();
        $data['title'] = 'Student Fee Status Details';
        $data['heading_msg'] = "Student Fee Status Details";
        $data['students'] = array();

        if(!$_POST && $student_id != '' && $class_shift_section_id != '' && $year != '' && $group_id != ''){
          $this->load->library('numbertowords');
          $SchoolInfo = $this->Admin_login->fetReportHeader();
          $Info = array();
          $Info['school_name'] = $SchoolInfo[0]['school_name'];
          $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
          $data['HeaderInfo'] = $Info;

          $class_shift_section_arr = explode("-", $class_shift_section_id);
          $class_id  = $class_shift_section_arr[0];
          $shift_id = $class_shift_section_arr[1];
          $section_id = $class_shift_section_arr[2];

          $student_list = $this->Admin_login->getStudentListByYear($year, $class_id, $shift_id, $section_id,$group_id);
          if ($student_list['status'] != 'success') {
              $sdata['exception'] = $student_list['status'];
              $this->session->set_userdata($sdata);
              redirect("student_fee_status_details/index");
          }
          $data['students'] = $student_list['students'];
          $data['class_shift_section_id'] = $class_shift_section_id;
          $data['class_id'] = $class_id;
          $data['group_id'] = $group_id;
          $data['section_id'] = $section_id;
          $data['student_id'] = $student_id;
          $data['year'] = $year;

          $data['student_info'] = $this->db->query("SELECT s.`name`,s.`roll_no`,c.`name` AS class_name,sf.`name` AS shift_name FROM `tbl_student` AS s
LEFT JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
LEFT JOIN `tbl_shift` AS sf ON s.`shift_id` = sf.`id`
WHERE s.`id` = '$student_id'")->result_array();
          $data['sub_category'] = $this->db->query("SELECT * FROM tbl_fee_sub_category ORDER BY id")->result_array();
          $data['rdata'] = $this->Student_fee->getStudentFeeStatusDetailsStandard($data);
//           echo '<pre>';
//           print_r($data['rdata']);
//           die;
          $data['report'] = $this->load->view('student_fee_status_details/report', $data, true);
        }

        if ($_POST) {
            $this->load->library('numbertowords');
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;


            $student_id = $this->input->post("student_id");
            $year = $this->input->post("year");
            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];
            $group_id =  $this->input->post("group_id");

            $student_list = $this->Admin_login->getStudentListByYear($year, $class_id, $shift_id, $section_id,$group_id);
            if ($student_list['status'] != 'success') {
                $sdata['exception'] = $student_list['status'];
                $this->session->set_userdata($sdata);
                redirect("student_fee_status_details/index");
            }
            $data['students'] = $student_list['students'];
            $data['class_shift_section_id'] = $class_shift_section_id;
            $data['class_id'] = $class_id;
            $data['group_id'] = $group_id;
            $data['section_id'] = $section_id;
            $data['student_id'] = $student_id;
            $data['year'] = $year;

            $data['student_info'] = $this->db->query("SELECT s.`name`,s.`roll_no`,c.`name` AS class_name,sf.`name` AS shift_name FROM `tbl_student` AS s
LEFT JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
LEFT JOIN `tbl_shift` AS sf ON s.`shift_id` = sf.`id`
WHERE s.`id` = '$student_id'")->result_array();
            $data['sub_category'] = $this->db->query("SELECT * FROM tbl_fee_sub_category ORDER BY id")->result_array();
            $data['rdata'] = $this->Student_fee->getStudentFeeStatusDetailsStandard($data);
            //echo '<pre>';
            //print_r($data['rdata']);
            // die;
            $data['report'] = $this->load->view('student_fee_status_details/report', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
          $data['is_pdf'] = 1;
          //Dom PDF
          $this->load->library('mydompdf');
          $html = $this->load->view('student_fee_status_details/report', $data, true);
          $this->mydompdf->createPDF($html, 'StudentFeeStatusDetailsReport', true, 'A4', 'landscape');
        }
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
        $data['years'] = $this->Admin_login->getYearList(0, 0);
        $data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['maincontent'] = $this->load->view('student_fee_status_details/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
