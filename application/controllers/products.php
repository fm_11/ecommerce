<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Products extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Message','Admin_login','common/custom_methods_model','Student_fee','Product'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        require_once APPPATH . 'third_party/PHPExcel.php';
        $this->excel = new PHPExcel();
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->SOFTWARE_START_YEAR = $user_info[0]->config_info[0]['software_start_year'];
        $this->notification = array();
    }
    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('product_list');

        // $class_id = '';
        // $shift_id = '';
        // $section_id = '';
        // if ($_POST) {
        //     $roll = $this->input->post("roll");
        //     $group = $this->input->post("group");
        //     $sdata['roll'] = $roll;
        //     $sdata['class_shift_section_id'] = $class_shift_section_id;
        // } else {
        //     $blood_group_id = $this->session->userdata('blood_group_id');
        //     $student_status = $this->session->userdata('student_status');
        //
        //     $cond['roll'] = $roll;
        //     $cond['class_id'] = $class_id;
        //
        // }
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('products/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Product->get_all_product_info_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['products'] = $this->Product->get_all_product_info_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
        $data['maincontent'] = $this->load->view('products/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


   public function my_file_upload($filename, $type)
   {
      $ext = explode('.',$_FILES[$filename]['name']);
      $new_file_name = $type . time() . "_" . date('Y-m-d'). '.' .end($ext);

      $this->load->library('upload');
      $config['upload_path'] = MEDIA_FOLDER . '/product/';

      // print_r($config['upload_path']);
      // die;
      $config['allowed_types'] = 'jpg|png|JPEG|jpeg|JPG';
      $config['max_size'] = '6000';
      $config['max_width'] = '4000';
      $config['max_height'] = '4000';
      $this->upload->initialize($config);
      $extension = strtolower(substr(strrchr($_FILES[$filename]['name'], '.'), 1));;
      if (move_uploaded_file($_FILES[$filename]["tmp_name"], MEDIA_FOLDER . "/product/" . $new_file_name)) {
           return $new_file_name;
      } else {
          $sdata['exception'] = $this->lang->line('add_error_message') . ", Error: Sorry Product Doesn't Added !" . $this->upload->display_errors();
          $this->session->set_userdata($sdata);
          return '0';
      }
   }
   public function image_unlink($old_file)
   {
     if (!empty($old_file)) {
         $filedel = PUBPATH .MEDIA_FOLDER . '/product/' . $old_file;
         // print_r($filedel);
         // die();
         unlink($filedel);
     }
   }
    public function add()
    {
        if ($_POST) {

          $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
          $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
          $sContentDescription = strip_tags(stripslashes($_POST['description']), $allowedTags);


          $photo_name1 = "";$photo_name2="";$photo_name3="";$photo_name4="";$feature_image_name="";
          $file1 = $_FILES["txtPhoto1"]['name'];
          $file2 = $_FILES["txtPhoto2"]['name'];
          $file3 = $_FILES["txtPhoto3"]['name'];
          $file4 = $_FILES["txtPhoto4"]['name'];
          $feature_image_file = $_FILES["txtFeaturePhoto"]['name'];
          //echo $feature_image_file;die;

          if($this->Product->checkifexist_product_info_by_name($_POST['product_name']))
          {
            $sdata['exception'] = "This product name '".$_POST['product_name']."' already exist.";
            $this->session->set_userdata($sdata);
            redirect("products/add");
          }


          if($feature_image_file!='')
          {
            $feature_image_name=$this->my_file_upload("txtFeaturePhoto","feature_img_");
            if($feature_image_name=='0')
            {
              $sdata['exception'] = "Can't save feature image";
              $this->session->set_userdata($sdata);
              redirect('products/add');
            }
          }

          if($file1!='')
          {
            $photo_name1=$this->my_file_upload("txtPhoto1","image_1_");
            if($photo_name1=='0')
            {
              $sdata['exception'] = "Can't save image 1";
              $this->session->set_userdata($sdata);
              redirect('products/add');
            }
          }
          if($file2!='')
          {
            $photo_name2=$this->my_file_upload("txtPhoto2","image_2_");
            if($photo_name2=='0')
            {
              $sdata['exception'] = "Can't save image 2";
              $this->session->set_userdata($sdata);
              redirect('products/add');
            }
          }

          if($file3!='')
          {
            $photo_name3=$this->my_file_upload("txtPhoto3","image_3_");
            if($photo_name3=='0')
            {
              $sdata['exception'] = "Can't save image 3";
              $this->session->set_userdata($sdata);
              redirect('products/add');
            }
          }

          if($file4!='')
          {
            $photo_name4=$this->my_file_upload("txtPhoto4","image_4_");
            if($photo_name4=='0')
            {
              $sdata['exception'] = "Can't save image 4";
              $this->session->set_userdata($sdata);
              redirect('products/add');
            }
          }
             $data = array();
            $data['product_name'] = $_POST['product_name'];
			      $data['product_code'] = $_POST['product_code'];
            $data['phone_number'] = $_POST['phone_number'];
            $data['bikash_number'] = $_POST['bikash_number'];
            $data['hot_deal'] = $_POST['is_hot_deal'];
            $data['regular_price'] = $_POST['regular_price'];
            $data['standard_price'] = $_POST['standard_price'];
            $data['description'] = $sContentDescription;
            $data['short_description'] = $_POST['short_description'];
            $data['is_publish'] =1;
            $data['publish_date'] = date('Y-m-d');
            $data['feature_image_path'] = $feature_image_name;
            $data['image_path_1'] = $photo_name1;
            $data['image_path_2'] = $photo_name2;
            $data['image_path_3'] = $photo_name3;
            $data['image_path_4'] = $photo_name4;
           if($this->Product->add_product_info($data))
          {

               $insert_id = $this->db->insert_id();
               $loop_time = $this->input->post("sub_category_list");
               $i = 0;
               while ($i <count($loop_time) ) {
                     $data = array();
                     $data['product_info_id'] = $insert_id;
                     $data['category_id'] = $this->Product->get_category_id_by_sub_category_id($loop_time[$i]);
                     $data['sub_category_id'] = $loop_time[$i];
                     $this->db->insert('tbl_product_category_details', $data);
                     $i++;
               }
               $sdata['message'] = $this->lang->line('add_success_message');

           }else{
             $sdata['exception'] = $this->lang->line('add_error_message') . ", Error: Sorry Product Doesn't Added !";
           }

            $this->session->set_userdata($sdata);
            redirect('products/add');
        }
        $data = array();
        $data['title'] = $this->lang->line('products') . ' ' . $this->lang->line('add');
        $data['is_show_button'] = "index";
        $data['products'] = $this->Product->get_product_category_list();

        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('products/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

	public function edit($id=null)
	{
    if ($_POST) {
           // echo '<pre>';
           // 	print_r($_POST);
           // 	die;
      $photo_name1 = "";$photo_name2="";$photo_name3="";$photo_name4="";$feature_image_name="";
      $file1 = $_FILES["txtPhoto1"]['name'];
      $file2 = $_FILES["txtPhoto2"]['name'];
      $file3 = $_FILES["txtPhoto3"]['name'];
      $file4 = $_FILES["txtPhoto4"]['name'];
      $feature_image_file = $_FILES["txtFeaturePhoto"]['name'];
      if($this->Product->checkifexist_update_product_info_by_name($_POST['product_name'],$id))
      {
        $sdata['exception'] = "This name '".$_POST['product_name']."' already exist.";
        $this->session->set_userdata($sdata);
        redirect("products/edit/".$id);
      }
      $data = array();

      if($feature_image_file!='')
      {
        $feature_image_name=$this->my_file_upload("txtFeaturePhoto","feature_img_");
        if($feature_image_name=='0')
        {
          $sdata['exception'] = "Can't save feature image";
          $this->session->set_userdata($sdata);
          redirect('products/add');
        }
        $data['feature_image_path'] = $feature_image_name;
        $this->image_unlink($_POST['oldFeaturePhoto']);
      }

      if($file1!='')
      {
        $photo_name1=$this->my_file_upload("txtPhoto1","image_1_");
        if($photo_name1=='0')
        {
          redirect('products/edit/'.$id);
        }
        $data['image_path_1'] = $photo_name1;
        $this->image_unlink($_POST['oldtxtPhoto1']);
      }
      if($file2!='')
      {
        $photo_name2=$this->my_file_upload("txtPhoto2","image_2_");
        if($photo_name2=='0')
        {
          redirect('products/edit/'.$id);
        }
        $data['image_path_2'] = $photo_name2;
        $this->image_unlink($_POST['oldtxtPhoto2']);
      }

      if($file3!='')
      {
        $photo_name3=$this->my_file_upload("txtPhoto3","image_3_");
        if($photo_name3=='0')
        {
          redirect('products/edit/'.$id);
        }
        $data['image_path_3'] = $photo_name3;
        $this->image_unlink($_POST['oldtxtPhoto3']);

      }

      if($file4!='')
      {
        $photo_name4=$this->my_file_upload("txtPhoto4","image_4_");
        if($photo_name4=='0')
        {
            redirect('products/edit/'.$id);
        }
        $data['image_path_4'] = $photo_name4;
        $this->image_unlink($_POST['oldtxtPhoto4']);
      }
        $data['id'] = $id;
        $data['product_name'] = $_POST['product_name'];
        $data['product_code'] = $_POST['product_code'];
        $data['phone_number'] = $_POST['phone_number'];
        $data['bikash_number'] = $_POST['bikash_number'];
        $data['hot_deal'] = $_POST['is_hot_deal'];
        $data['short_description'] = $_POST['short_description'];
        $data['description'] = $_POST['description'];
        $data['regular_price'] = $_POST['regular_price'];
        $data['standard_price'] = $_POST['standard_price'];
        $data['description'] = $_POST['description'];
        $data['is_publish'] =1;
        $data['publish_date'] = date('Y-m-d');

          // print_r($data);
          // die;
        $this->Product->update_product_info($data);
                //  if()
                // {
          $this->Product->delete_product_category_details_by_product_id($id);
          $insert_id = $id;
          $loop_time = $this->input->post("sub_category_list");
          $i = 0;
           while ($i <count($loop_time) ) {
               $data = array();
               $data['product_info_id'] = $insert_id;
               $data['category_id'] = $this->Product->get_category_id_by_sub_category_id($loop_time[$i]);
               $data['sub_category_id'] =$loop_time[$i] ;
               $this->db->insert('tbl_product_category_details', $data);
               $i++;
          }
           $sdata['message'] = $this->lang->line('edit_success_message');

       // }else{
       //   $sdata['exception'] = $this->lang->line('edit_error_message');
       // }

        $this->session->set_userdata($sdata);
        redirect('products/index');
    }
		$data = array();
		$data['title'] = $this->lang->line('products') . ' ' . $this->lang->line('edit');
		$data['is_show_button'] = "index";
    $data['products'] = $this->Product->get_product_info_by_id($id);
    $data['sub_category'] = $this->Product->get_product_category_list_by_product_id($id);
    // echo '<pre>';
    // print_r($data['sub_category']);
    // die;
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('products/edit', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

    public function delete($id)
    {
      if($this->Product->checkifexist_product_info_dependency_by_product_id($id))
      {
        $sdata['exception'] = "This item cannot be delete due to order dependency.";
        $this->session->set_userdata($sdata);
        redirect("products/index/");
      }
        $product_info = $this->Product->get_product_info_by_id($id);
        if (empty($product_info)) {
            $sdata['exception'] = $this->lang->line('delete_error_message') . " Error: Product data not available!";
            $this->session->set_userdata($sdata);
            redirect("products/index");
        }
       $this->Product->delete_product_category_details_by_product_id($id);
       $this->Product->delete_product_info_by_product_id($id);
// print_r($product_info->image_path_1);
// die;
        $this->image_unlink($product_info->image_path_1);
        $this->image_unlink($product_info->image_path_2);
        $this->image_unlink($product_info->image_path_3);
        $this->image_unlink($product_info->image_path_4);
        $this->image_unlink($product_info->feature_image_path);

        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('products/index');
    }
}
