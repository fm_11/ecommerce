<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Admin_logins extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $this->load->view('admin_logins/index');
    }

    public function set_langulage($language_short_name)
    {
        $this->session->unset_userdata('language');

        $arraydata = array(
            'language'  => $language_short_name
          );
        $this->session->set_userdata($arraydata);

        redirect("dashboard/index");
    }

    public function module_dropdown()
    {
        $this->db->select('*');
        $this->db->where('is_allowed', 1);
        $query = $this->db->get('tbl_module');
        $modules = $query->result();
        $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode($modules));
    }

    public function all_content_info()
    {
        $data = array();
        $data['title'] = 'All Content Information';
        $data['heading_msg'] = "All Content Information";
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        //$data['maincontent'] = $this->load->view('photo_gallerys/gallery_event_list', '', true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function history_info()
    {
        $data = array();
        $data['title'] = 'History Information';
        $data['heading_msg'] = "History Information";
        $data['history_info'] = $this->Admin_login->get_history_info();
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $data['maincontent'] = $this->load->view('admin_logins/history_info', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function rules_and_regulation_info()
    {
        $data = array();
        $data['title'] = 'Rules and Regulation';
        $data['heading_msg'] = "Rules and Regulation";
        $data['rules_and_regulation_info'] = $this->Admin_login->get_rules_and_regulation_info();
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $data['maincontent'] = $this->load->view('admin_logins/rules_and_regulation_info', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function update_rules_and_regulation_info($id = null)
    {
        if ($_POST) {
            $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
            $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
            $sContent = strip_tags(stripslashes($_POST['txtRules']), $allowedTags);
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['title'] = $this->input->post('txtTitle', true);
            $data['rules'] = $sContent;
            $this->Admin_login->rules_and_regulation_info_update($data);
            $sdata['message'] = "You are Successfully Rules and Regulation Info. Updated.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/rules_and_regulation_info");
        } else {
            $data = array();
            $data['title'] = 'Update Rules and Regulation';
            $data['heading_msg'] = "Update Rules and Regulation";
            $data['rules_and_regulation_info'] = $this->Admin_login->get_rules_and_regulation_by_id($id);
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('admin_logins/rules_and_regulation_edit_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function update_history_info($id = null)
    {
        if ($_POST) {
            $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
            $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
            $sContent = strip_tags(stripslashes($_POST['txtDescription']), $allowedTags);
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['title'] = $this->input->post('txtTitle', true);
            $data['description'] = $sContent;
            $this->Admin_login->history_info_update($data);
            $sdata['message'] = "You are Successfully History Info. Updated.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/history_info");
        } else {
            $data = array();
            $data['title'] = 'Update History Information';
            $data['heading_msg'] = "Update History Information";
            $data['history_info'] = $this->Admin_login->get_history_info_by_id($id);
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('admin_logins/history_info_edit_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function about_info()
    {
        $data = array();
        $data['title'] = 'About Information';
        $data['heading_msg'] = "About Information";
        $data['about_info'] = $this->Admin_login->get_about_info();
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $data['maincontent'] = $this->load->view('admin_logins/about_info', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function founder_info()
    {
        $data = array();
        $data['title'] = 'Founder Information';
        $data['heading_msg'] = "Founder Information";
        $data['founder_info'] = $this->Admin_login->get_founder_info();
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $data['maincontent'] = $this->load->view('admin_logins/founder_info', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }




    public function home_page_news_info()
    {
        $data = array();
        $data['title'] = 'Home Page News';
        $data['heading_msg'] = "Home Page News";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('admin_logins/home_page_news_info/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Admin_login->get_home_page_news_info(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['news_info'] = $this->Admin_login->get_home_page_news_info(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $data['maincontent'] = $this->load->view('admin_logins/home_page_news_info', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function home_page_news_add()
    {
        if ($_POST) {
            $data = array();
            $data['text_color'] = "#" . $this->input->post('txtTextColor', true);
            $data['news'] = $this->input->post('txtNews', true);
            $this->db->insert('tbl_home_page_news', $data);
            $sdata['message'] = "You are Successfully Added Home Page News Info.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/home_page_news_info");
        } else {
            $data = array();
            $data['title'] = 'Home Page News Information';
            $data['heading_msg'] = "Home Page News Information";
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('admin_logins/home_page_news_info_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function home_page_breaking_news_info()
    {
        $data = array();
        $data['title'] = 'Home Page Welcome Message';
        $data['heading_msg'] = "Home Page Welcome Message";
        $data['breaking_news_info'] = $this->Admin_login->get_home_page_breaking_news_info();
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $data['maincontent'] = $this->load->view('admin_logins/home_page_breaking_news_info', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function update_home_page_news_info($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['text_color'] = "#" . $this->input->post('txtTextColor', true);
            $data['news'] = $this->input->post('txtNews', true);
            $this->Admin_login->home_page_news_info_update($data);
            $sdata['message'] = "You are Successfully Home Page News Info. Updated.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/home_page_news_info");
        } else {
            $data = array();
            $data['title'] = 'Update Home Page News Information';
            $data['heading_msg'] = "Update Home Page News Information";
            $data['news_info'] = $this->Admin_login->get_home_page_news_info_by_id($id);
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('admin_logins/home_page_news_info_edit_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function delete_home_page_news_info($id)
    {
        $this->db->delete('tbl_home_page_news', array('id' => $id));
        $sdata['message'] = "Home Page News Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("admin_logins/home_page_news_info");
    }


    public function update_home_page_breaking_news_info($id = null)
    {
        if ($_POST) {
            $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
            $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
            $sContent = strip_tags(stripslashes($_POST['txtNews']), $allowedTags);
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['breaking_news'] = $sContent;
            $this->Admin_login->home_page_breaking_news_info_update($data);
            $sdata['message'] = "You are Successfully Home Page Welcome Message Updated.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/home_page_breaking_news_info");
        } else {
            $data = array();
            $data['title'] = 'Update Home Page Welcome Message';
            $data['heading_msg'] = "Update Home Welcome Message";
            $data['breaking_news_info'] = $this->Admin_login->get_home_page_breaking_news_info_by_id($id);
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('admin_logins/home_page_breaking_news_info_edit_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function update_about_info($id = null)
    {
        if ($_POST) {
            $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
            $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
            $sContent = strip_tags(stripslashes($_POST['txtDescription']), $allowedTags);
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['title'] = $this->input->post('txtTitle', true);
            $data['description'] = $sContent;
            $this->Admin_login->about_info_update($data);
            $sdata['message'] = "You are Successfully About Info. Updated.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/about_info");
        } else {
            $data = array();
            $data['title'] = 'Update About Information';
            $data['heading_msg'] = "Update About Information";
            $data['about_info'] = $this->Admin_login->get_about_info_by_id($id);
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('admin_logins/about_info_edit_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }



    public function update_founder_info()
    {
        $file_name = $_FILES['txtPhoto']['name'];
        if ($file_name != '') {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/headteacher/';
            $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size'] = '5000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Contact_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/headteacher/" . $new_photo_name)) {
                        $data = array();
                        $data['id'] = $this->input->post('id', true);
                        $data['name'] = $this->input->post('name', true);
                        $data['years_of_active'] = $this->input->post('years_of_active', true);
                        $data['born'] = $this->input->post('born', true);
                        $data['address'] = $this->input->post('address', true);
                        $data['edu_qua'] = $this->input->post('edu_qua', true);
                        $data['occupation'] = $this->input->post('occupation', true);
                        $data['picture'] = $new_photo_name;
                        $this->Admin_login->update_founder_info($data);
                        $sdata['message'] = "You are Successfully Updated Founder Info ! ";
                        $this->session->set_userdata($sdata);
                        redirect("admin_logins/founder_info");
                    } else {
                        $sdata['exception'] = "Sorry Founder Info. Doesn't Updated !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("admin_logins/founder_info");
                    }
                } else {
                    $sdata['exception'] = "Sorry Founder Info. Doesn't Updated  !" . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("admin_logins/founder_info");
                }
            }
        } else {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('name', true);
            $data['years_of_active'] = $this->input->post('years_of_active', true);
            $data['born'] = $this->input->post('born', true);
            $data['address'] = $this->input->post('address', true);
            $data['edu_qua'] = $this->input->post('edu_qua', true);
            $data['occupation'] = $this->input->post('occupation', true);
            $this->Admin_login->update_founder_info($data);
            $sdata['message'] = "You are Successfully Updated Founder Info ! ";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/founder_info");
        }
    }



    
    public function president_message_info()
    {
        $data = array();
        $data['title'] = 'President Message';
        $data['heading_msg'] = "President Message";
        $data['message'] = $this->db->where('id', '1')->get('tbl_president_message')->row();
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $data['maincontent'] = $this->load->view('admin_logins/president_message', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function update_president_message_info()
    {
        $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
        $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
        $sContent = strip_tags(stripslashes($_POST['txtMessage']), $allowedTags);

        $file_name = $_FILES['txtPhoto']['name'];
        if ($file_name != '') {
            $president_photo_info = $this->db->where('id', '1')->get('tbl_president_message')->result_array();
            $old_file = $president_photo_info[0]['location'];
            if (!empty($old_file)) {
                $filedel = PUBPATH . MEDIA_FOLDER . '/headteacher/' . $old_file;
                unlink($filedel);
            }
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/headteacher/';
            $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size'] = '5000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "President_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/headteacher/" . $new_photo_name)) {
                        $data = array();
                        $data['id'] = $this->input->post('id', true);
                        $data['title'] = $this->input->post('txtTitle', true);
                        $data['short_message'] = $sContent;
                        $data['long_message'] = $sContent;
                        $data['location'] = $new_photo_name;
                        $this->db->where('id', $data['id']);
                        $this->db->update('tbl_president_message', $data);
                        $sdata['message'] = "You are Successfully Updated President Message.";
                        $this->session->set_userdata($sdata);
                        redirect("admin_logins/president_message_info");
                    } else {
                        $sdata['exception'] = "Sorry President Message Doesn't Updated !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("admin_logins/president_message_info");
                    }
                } else {
                    $sdata['exception'] = "Sorry President Message Doesn't Updated  !" . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("admin_logins/president_message_info");
                }
            }
        } else {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['title'] = $this->input->post('txtTitle', true);
            $data['short_message'] = $sContent;
            $data['long_message'] = $sContent;
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_president_message', $data);
            $sdata['message'] = "You are Successfully Updated President Message.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/president_message_info");
        }
    }

    public function head_message_info()
    {
        $data = array();
        $data['title'] = 'Head Teacher Message';
        $data['heading_msg'] = "Head Teacher Message";
        $data['message'] = $this->db->where('id', '1')->get('tbl_headmasters_message')->row();
        $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
        $data['maincontent'] = $this->load->view('admin_logins/head_message', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function update_head_message_info()
    {
        $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
        $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
        $sContent = strip_tags(stripslashes($_POST['txtMessage']), $allowedTags);

        $file_name = $_FILES['txtPhoto']['name'];
        if ($file_name != '') {
            $head_photo_info = $this->db->where('id', '1')->get('tbl_headmasters_message')->result_array();
            $old_file = $head_photo_info[0]['location'];
            if (!empty($old_file)) {
                $filedel = PUBPATH . MEDIA_FOLDER .'/headteacher/' . $old_file;
                unlink($filedel);
            }
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/headteacher/';
            $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size'] = '5000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Head_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/headteacher/" . $new_photo_name)) {
                        $data = array();
                        $data['id'] = $this->input->post('id', true);
                        $data['title'] = $this->input->post('txtTitle', true);
                        $data['short_message'] = $sContent;
                        $data['long_message'] = $sContent;
                        $data['location'] = $new_photo_name;
                        $this->db->where('id', $data['id']);
                        $this->db->update('tbl_headmasters_message', $data);
                        $sdata['message'] = "You are Successfully Updated Head Teacher Message.";
                        $this->session->set_userdata($sdata);
                        redirect("admin_logins/head_message_info");
                    } else {
                        $sdata['exception'] = "Sorry Head Teacher Message Doesn't Updated !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("admin_logins/head_message_info");
                    }
                } else {
                    $sdata['exception'] = "Sorry Head Teacher Message Doesn't Updated  !" . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("admin_logins/head_message_info");
                }
            }
        } else {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['title'] = $this->input->post('txtTitle', true);
            $data['short_message'] = $sContent;
            $data['long_message'] = $sContent;
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_headmasters_message', $data);
            $sdata['message'] = "You are Successfully Updated Head Teacher Message.";
            $this->session->set_userdata($sdata);
            redirect("admin_logins/head_message_info");
        }
    }
}
