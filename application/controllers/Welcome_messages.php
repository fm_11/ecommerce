<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Welcome_messages extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login','common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Home Page Welcome Message';
        $data['heading_msg'] = "Home Page Welcome Message";
        $data['breaking_news_info'] = $this->Admin_login->get_home_page_breaking_news_info();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('welcome_messages/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $allowedTags = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
            $allowedTags .= '<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
            $sContent = strip_tags(stripslashes($_POST['txtNews']), $allowedTags);
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['breaking_news'] = $sContent;
            $this->Admin_login->home_page_breaking_news_info_update($data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("welcome_messages/index");
        } else {
            $data = array();
            $data['title'] = 'Update Home Page Welcome Message';
            $data['heading_msg'] = "Update Home Welcome Message";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['breaking_news_info'] = $this->Admin_login->get_home_page_breaking_news_info_by_id($id);
            $data['top_menu'] = $this->load->view('admin_logins/all_content_menu', '', true);
            $data['maincontent'] = $this->load->view('welcome_messages/edit', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

  }

