<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Print_admit extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login'));
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        if ($_POST) {
            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];

            $group = $this->input->post('txtGroup', true);
            $exam_id = $this->input->post('txtExam', true);
            $view_type = $this->input->post('view_type', true);

            $from_roll = $this->input->post('from_roll', true);
            $to_roll = $this->input->post('to_roll', true);

            $student_id = 0;
            $st_where = "";
            if ($view_type == 'S') {
                if ($from_roll == '' || $to_roll == '') {
                    $sdata['exception'] = "From roll and To roll cannot be empty";
                    $this->session->set_userdata($sdata);
                    redirect("print_admit/index");
                }
                if ($from_roll > $to_roll) {
                    $sdata['exception'] = "From roll cannot be greater than To roll";
                    $this->session->set_userdata($sdata);
                    redirect("print_admit/index");
                }

                $roll_where_in_string = "";
                while ($from_roll <= $to_roll) {
                    $roll_where_in_string .= "'" . $from_roll ."',";
                    $from_roll++;
                }
                $roll_where_in_string_for_q = rtrim($roll_where_in_string, ',');
                //die;
                $st_where = " AND s.`roll_no` IN ($roll_where_in_string_for_q)";
            }
            //echo $exam;
            //die;
            $roll_where =  "";
            if ($view_type == 'S') {
                $roll_where = $st_where;
            }

            $where = "  s.`status` = 1 AND s.`class_id` = '$class_id' AND s.`section_id` = '$section_id'
            AND s.`group` =  '$group' AND s.`shift_id` =  '$shift_id' $roll_where";

            $exam_info = $this->db->query("SELECT * FROM tbl_exam WHERE id = '$exam_id'")->result_array();
            // echo $where;
            // die;

            $data = array();
            $data['exam'] = $exam_info[0]['name'];
            $data['year'] = $exam_info[0]['year'];
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;


            $data['title'] = 'Student Admit Card';
            $data['heading_msg'] = "Student Admit Card";
            $data['list'] = $this->db->query("SELECT s.`id`,s.`name`,s.`roll_no`,s.`guardian_mobile`,s.`student_code`,s.father_name,s.mother_name,c.`name` AS class_name,sc.`name` AS section_name, g.`name` AS group_name,sf.`name` as shift_name,
    s.`photo`
    FROM `tbl_student` AS s
    LEFT JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
    LEFT JOIN `tbl_section` AS sc ON s.`section_id` = sc.`id`
    LEFT JOIN `tbl_student_group` AS g ON s.`group` = g.`id`
    LEFT JOIN `tbl_shift` AS sf ON s.`shift_id` = sf.`id`
    WHERE $where;")->result_array();

            if(empty($data['list'])){
				$sdata['exception'] = "Student not found";
				$this->session->set_userdata($sdata);
				redirect("print_admit/index");
			}

            // echo '<pre>';
            // print_r($data['list']);
            // echo $this->db->last_query();
            // die;

			$data['admit_design']  = $this->db->query("SELECT * FROM `tbl_admit_card_design`")->row();

            $data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
            $data['signature'] = $this->Admin_login->getSignatureByAccessCode('AC'); //AC= admit card
            // echo '<pre>';
            // print_r($data['signature']);
            // die;

            $is_pdf_request = $this->input->post('is_pdf_request', true);

            if (isset($is_pdf_request) && $is_pdf_request == 'Download') {
                //echo 'gggg'; die;
                $data['is_pdf_request'] = $is_pdf_request;
                $this->load->library('mydompdf');
                $html = $this->load->view('student_admit/student_admit_card_print', $data, true);
                $this->mydompdf->createPDF($html, 'AdmitCard', true);
            } else {
                $data['is_pdf_request'] = $is_pdf_request;
                if($data['admit_design']->design_type == 'WIR'){
					$this->load->view('student_admit/student_admit_card_print_wir', $data);
				}else{
					$data['routine_info'] = $this->db->query("SELECT s.`name` as subject_name,s.`code`,sn.`name`,sn.`header`,
											sn.`start_time`,sn.`end_time`,r.`date`
											FROM `tbl_exam_routine` AS r 
											INNER JOIN `tbl_subject` AS s ON s.`id` = r.`subject_id`
											INNER JOIN `tbl_exam_session` AS sn ON sn.`id` = r.`exam_session_id`
											WHERE r.`class_id` = $class_id AND r.`exam_id` = $exam_id AND r.`group_id` = $group")->result_array();
					$this->load->view('student_admit/student_admit_card_print_wr', $data);
				}
            }
        } else {
            $data = array();
            $data['title'] = 'Student Admit Card Print';
            $data['heading_msg'] = "Student Admit Card Print";
            $software_current_year = $this->Admin_login->getCurrentSystemYear();
            $data['exam'] = $this->db->query("SELECT * FROM tbl_exam WHERE year = '$software_current_year'")->result_array();
            $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
            $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
            $data['students'] = $this->db->query("SELECT id,name,student_code FROM tbl_student WHERE status = 1")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_admit/student_admit_card_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
