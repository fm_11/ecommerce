<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function studentFailedList()
    {
        $data = array();
        $data['title'] = 'Failed Student List';
        $data['heading_msg'] = "Failed Student List";
        if ($_POST) {
            $exam_id = $this->input->post("exam");
            $class_id = $this->input->post("class_id");

            $SchoolInfo = $this->fetReportHeader();
            //echo '<pre>';
            //print_r($SchoolInfo); die;
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['address'] = $SchoolInfo[0]['address'];
            $data['HeaderInfo'] = $Info;

            $data['class_id'] = $class_id;
            $data['exam_id'] = $exam_id;


            $class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
            $data['class_name'] = $class_info[0]['name'];

            $exam_info = $this->db->query("SELECT name,year FROM tbl_exam WHERE id ='$exam_id'")->result_array();
            $data['exam_name'] = $exam_info[0]['name'];
            $data['exam_year'] = $exam_info[0]['year'];

            $data['subjects'] = $this->db->query("SELECT
  c.`subject_id`,
  s.`name`, s.`code`
FROM
  `tbl_class_wise_subject` AS c
  LEFT JOIN `tbl_subject` AS s
    ON s.`id` = c.`subject_id`
WHERE c.`class_id` = '$class_id' GROUP BY c.`subject_id` ORDER BY c.`order_number`")->result_array();
            //echo '<pre>';
            //print_r($data['groups']);
            //die;
            $result_data = array();
            $i = 0;
            foreach ($data['subjects'] as $row):
                $subject_id = $row['subject_id'];
            $subject_wise_failed =  $this->db->query("SELECT sc.`name` as section_name,GROUP_CONCAT('[',rp.`roll_no`,'] ') AS result FROM `tbl_result_process_details` AS r
                INNER JOIN `tbl_result_process` AS rp ON rp.`id` = r.`result_id`
                INNER JOIN `tbl_section` AS sc ON sc.`id` = rp.`section_id`
                WHERE r.`class_id` = '$class_id' AND r.`exam_id` = '$exam_id' AND r.`subject_id` = '$subject_id' AND r.`alpha_gpa` = 'F'
                GROUP BY r.`section_id`")->result_array();
            $result_data[$i]['subject_name'] = $row['name'];
            $result_data[$i]['code'] = $row['code'];
            $result_data[$i]['subject_id'] = $row['subject_id'];
            if (!empty($subject_wise_failed)) {
                $j = 0;
                foreach ($subject_wise_failed as $r_row):
                        $br = "";
                if ($j != 0) {
                    $br = "<br>";
                }
                $result_data[$i]['failed_list'][$j] = $br . $r_row['section_name'] . " : " . $r_row['result']. " = " . count(explode(",", $r_row['result']));
                $j++;
                endforeach;
            } else {
                $result_data[$i]['failed_list'][0] = "No student has failed";
            }
            $i++;
            endforeach;

            $data['result_data'] = $result_data;




            if (empty($data['result_data'])) {
                $data = array();
                $sdata['exception'] = "Sorry result info. not found. Please process result first";
                $this->session->set_userdata($sdata);
                redirect("report/studentFailedList");
            }

            $data['report'] = $this->load->view('report/student_failed_list_table', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            //echo 'gggg'; die;
            // Load all views as normal
            $data['is_pdf'] = 1;
            $html = $this->load->view('report/student_failed_list_table', $data, true);
            $pdfFilePath = "FailedStudentList.pdf";
            $this->load->library('m_pdf');
            $this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
            //generate the PDF from the given html
            $this->m_pdf->pdf->autoScriptToLang = true;
            $this->m_pdf->pdf->WriteHTML($html);
            //download it.
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['maincontent'] = $this->load->view('report/student_failed_list', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function studentResultSummeryReport()
    {
        $data = array();
        $data['title'] = 'Student Result Summery';
        $data['heading_msg'] = "Student Result Summery";
        if ($_POST) {
            $exam_id = $this->input->post("exam");
            $class_id = $this->input->post("class_id");

            $SchoolInfo = $this->fetReportHeader();
            //echo '<pre>';
            //print_r($SchoolInfo); die;
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['address'] = $SchoolInfo[0]['address'];
            $data['HeaderInfo'] = $Info;

            $data['class_id'] = $class_id;
            $data['exam_id'] = $exam_id;


            $class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
            $data['class_name'] = $class_info[0]['name'];

            $exam_info = $this->db->query("SELECT name,year FROM tbl_exam WHERE id ='$exam_id'")->result_array();
            $data['exam_name'] = $exam_info[0]['name'];
            $data['exam_year'] = $exam_info[0]['year'];

            $data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
            //echo '<pre>';
            //print_r($data['groups']);
            //die;
            $result_data = array();
            foreach ($data['groups'] as $row):
                $group_id = $row['id'];
            //echo $group_id.'/';
            $result_exists = $this->db->query("SELECT * FROM tbl_result_process AS r WHERE r.`class_id` = '$class_id' AND r.`group` = '$group_id' AND r.`exam_id` = '$exam_id' ")->result_array();
            if (!empty($result_exists)) {
                $result_data[$group_id]['pass'] =  $this->db->query("SELECT sc.`name` AS section_name,r.`group` AS group_id,GROUP_CONCAT(r.`roll_no`,'[',r.`gpa_with_optional`,'] ') AS result FROM `tbl_result_process` AS r
                    INNER JOIN `tbl_student` AS s ON s.`id` = r.`student_id`
                    INNER JOIN `tbl_section` AS sc ON sc.`id` = r.`section_id`
                    WHERE r.`class_id` = '$class_id' AND r.`group` = '$group_id' AND r.`exam_id` = '$exam_id' AND r.`gpa_with_optional` > 0
                    GROUP BY r.`section_id`")->result_array();

                $result_data[$group_id]['fail'] =  $this->db->query("SELECT sc.`name` AS section_name,r.`group` AS group_id,GROUP_CONCAT(r.`roll_no`,'[',r.`c_alpha_gpa_with_optional`,'(',r.`number_of_failed_subject`,')]') AS result FROM `tbl_result_process` AS r
                    INNER JOIN `tbl_student` AS s ON s.`id` = r.`student_id`
                    INNER JOIN `tbl_section` AS sc ON sc.`id` = r.`section_id`
                    WHERE r.`class_id` = '$class_id' AND r.`group` = '$group_id' AND r.`exam_id` = '$exam_id' AND r.`gpa_with_optional` <= 0
                    GROUP BY r.`section_id`")->result_array();
            }
            endforeach;

            $data['result_data'] = $result_data;




            if (empty($data['result_data'])) {
                $data = array();
                $sdata['exception'] = "Sorry result info. not found. Please process result first";
                $this->session->set_userdata($sdata);
                redirect("report/studentResultSummeryReport");
            }

            $data['report'] = $this->load->view('report/student_result_summery_table', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            //echo 'gggg'; die;
            // Load all views as normal
            $data['is_pdf'] = 1;
            $html = $this->load->view('report/student_result_summery_table', $data, true);
            $pdfFilePath = "StudentResultSummery.pdf";
            $this->load->library('m_pdf');
            $this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
            //generate the PDF from the given html
            $this->m_pdf->pdf->autoScriptToLang = true;
            $this->m_pdf->pdf->WriteHTML($html);
            //download it.
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['maincontent'] = $this->load->view('report/student_result_summery', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }




    public function studentResultReport()
    {
        if ($_POST) {
            //echo '<pre>';
            //print_r($_POST);
            //die;
            $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;

            $exam_id = $this->input->post("exam");
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $shift_id = $this->input->post("shift_id");
            $year = $this->input->post("year");

            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['shift_id'] = $shift_id;
            $data['year'] = $year;
            $data['exam_id'] = $exam_id;

            $class_info = $this->db->query("SELECT * FROM tbl_class WHERE id ='$class_id'")->result_array();
            $data['class_name'] = $class_info[0]['name'];

            $section_where = "";
            if ($section_id == 'all') {
                $data['section_name'] = "All";
            } else {
                $section_info = $this->db->query("SELECT * FROM tbl_section WHERE id ='$section_id'")->result_array();
                $data['section_name'] = $section_info[0]['name'];
                $section_where = " AND rp.section_id = '$section_id' ";
            }



            $group_where = "";
            if ($group == 'all') {
                $data['group_name'] = "All";
            } else {
                $group_info = $this->db->query("SELECT * FROM tbl_student_group WHERE id ='$group'")->result_array();
                $data['group_name'] = $group_info[0]['name'];
                $group_where = " AND rp.group = '$group' ";
            }

            $config = $this->db->query("SELECT how_to_generate_position,multiple_atudent_allow_same_position,is_calculate_cgpa_when_fail FROM `tbl_config`")->result_array();
            if (!empty($config)) {
                $how_to_generate_position = $config[0]['how_to_generate_position'];
                $multiple_atudent_allow_same_position = $config[0]['multiple_atudent_allow_same_position'];
                $is_calculate_cgpa_when_fail =  $config[0]['is_calculate_cgpa_when_fail'];
            } else {
                $how_to_generate_position = 'CS';
                $multiple_atudent_allow_same_position = 0;
                $is_calculate_cgpa_when_fail = 0;
            }

            $shift_info = $this->db->query("SELECT * FROM tbl_shift WHERE id ='$shift_id'")->result_array();
            $data['shift_name'] = $shift_info[0]['name'];

            if ($group != 'all' && $section_id != 'all') {
                $data['rdata'] = $this->db->query("SELECT s.`id`,s.`name`,s.`student_code`,rp.`roll_no`,rp.`total_obtain_mark`,rp.`gpa_without_optional`,rp.calculable_total_mark,
                rp.`gpa_with_optional`,rp.`position`,ex.total_extra_mark FROM `tbl_result_process` AS rp
                INNER JOIN `tbl_student` AS s ON s.`id` = rp.`student_id`

                left join(
                 select sum(allowable_mark) as total_extra_mark , student_id from tbl_student_wise_extra_mark
                 where exam_id = '$exam_id'
                 group by student_id
                ) ex on ex.student_id = rp.student_id

                WHERE rp.`exam_id` = '$exam_id' AND rp.`class_id` = '$class_id' AND rp.`section_id` = '$section_id' AND rp.`group` = '$group' AND rp.`shift` = '$shift_id'
                ORDER BY rp.`position` = 0, rp.`position`;")->result_array();
            //echo '<pre>';
               // print_r($data['rdata']); die;
            } else {
                $data['rdata'] = $this->db->query("SELECT s.name,s.student_code,rp.id,g.name as group_name,sc.name as section_name,rp.student_id,rp.roll_no,rp.exam_id,rp.total_obtain_mark,rp.gpa_with_optional,
               rp.gpa_without_optional,rp.position,rp.c_alpha_gpa_with_optional,rp.c_alpha_gpa_without_optional FROM tbl_result_process as rp
               INNER JOIN tbl_student AS s ON s.id = rp.student_id
               INNER JOIN tbl_student_group as g on g.id = rp.group
               INNER JOIN tbl_section as sc on sc.id = rp.section_id
            	WHERE rp.exam_id = '$exam_id' AND rp.class_id = '$class_id' AND rp.`shift` = '$shift_id' $section_where $group_where
            	ORDER BY rp.`position` = 0,rp.total_obtain_mark DESC;")->result_array();
            }
            //echo '<pre>';
            // print_r($data['rdata']);
            // die;

            $data['title'] = 'Student Result';
            $data['heading_msg'] = "Student Result";
            if ($group == 'all') {
                $positions = $data['rdata'];

                if (!empty($positions)) {
                    if ($multiple_atudent_allow_same_position == 1) {
                        $updateArray = array();
                        $same_position_count = 0;
                        for ($x = 0; $x < count($positions); $x++) {
                            if ($positions[$x]['gpa_with_optional'] == '0.00') {
                                $position = 0;
                            } else {
                                if ($x == 0) {
                                    $position = 1;
                                } else {
                                    if ($positions[$x - 1]['total_obtain_mark'] == $positions[$x]['total_obtain_mark']) {
                                        $same_position_count = $same_position_count + 1;
                                        $position = ($x + 1) - $same_position_count;
                                    //echo $positions[$x - 1]['total_obtain_mark'].'/'.$positions[$x]['total_obtain_mark'].'/'.$position.'/'.$positions[$x]['id'].'<br>';
                                    } else {
                                        if ($same_position_count > 0) {
                                            $position = ($x + 1) - $same_position_count;
                                        } else {
                                            $position = ($x + 1);
                                        }
                                    }
                                }
                            }

                            $updateArray[$positions[$x]['student_id']]['position'] = $position;
                            $updateArray[$positions[$x]['student_id']]['mark'] = $positions[$x]['total_obtain_mark'];
                        }
                    } else {
                        $updateArray = array();
                        for ($x = 0; $x < count($positions); $x++) {
                            if ($positions[$x]['gpa_with_optional'] == '0.00') {
                                $position = 0;
                            } else {
                                $position = $x + 1;
                            }

                            $updateArray[$positions[$x]['student_id']] = $position;
                        }
                    }
                }

                //echo count($positions).'/'.count($updateArray);
                //echo '<pre>';
                //print_r($updateArray);
                //die;
                $data['all_student_position'] = $updateArray;
                $data['rdata'] = $this->load->view('report/student_result_table_all', $data);
            } else {
                $data['rdata'] = $this->load->view('report/student_result_table', $data);
            }
            $this->load->view('report/rindex');
        } else {
        }
    }


    public function studentGrandResultReport()
    {
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;

            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $shift_id = $this->input->post("shift_id");
            $year = $this->input->post("year");

            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['shift_id'] = $shift_id;
            $data['year'] = $year;

            $class_info = $this->db->query("SELECT * FROM tbl_class WHERE id ='$class_id'")->result_array();
            $data['class_name'] = $class_info[0]['name'];

            $section_info = $this->db->query("SELECT * FROM tbl_section WHERE id ='$section_id'")->result_array();
            $data['section_name'] = $section_info[0]['name'];

            $group_info = $this->db->query("SELECT * FROM tbl_student_group WHERE id ='$group'")->result_array();
            $data['group_name'] = $group_info[0]['name'];

            $shift_info = $this->db->query("SELECT * FROM tbl_shift WHERE id ='$shift_id'")->result_array();
            $data['shift_name'] = $shift_info[0]['name'];

            $data['exam_list'] = $this->db->query("SELECT * FROM `tbl_exam` order by id")->result_array();
            $data['rdata'] = $this->Student->getGrandResult($data);
            //  echo '<pre>';
            //print_r($data['rdata']);
            //  die;


            $data['title'] = 'Student Grand Result';
            $data['heading_msg'] = "Student Grand Result";
            $data['rdata'] = $this->load->view('report/student_grand_result_table', $data);
            $this->load->view('report/rindex');
        } else {
            $data = array();
            $data['title'] = 'Student Grand Result';
            $data['heading_msg'] = "Student Grand Result";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['shifts'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['maincontent'] = $this->load->view('report/student_grand_result', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function index()
    {
        $data = array();
        $data['title'] = 'Report';
        $data['heading_msg'] = "Report";
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function get_report_list_for_student_fees()
    {
        $data = array();
        $data['title'] = 'Student Fees Report';
        $data['heading_msg'] = "Student Fees Report";
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('report/get_report_list_for_student_fees', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function get_report_list_for_student_info()
    {
        $data = array();
        $data['title'] = 'Student Information Report';
        $data['heading_msg'] = "Student Information Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('report/get_report_list_for_student_info', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function get_report_list_for_resut()
    {
        $data = array();
        $data['title'] = 'Student Result Report';
        $data['heading_msg'] = "Student Result Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('report/get_report_list_for_resut', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function getStudentFeeStatus()
    {
        if ($_POST) {
            $data = array();
            $data['title'] = 'Student Fee Status';
            $class_id = $this->input->post('class_id', true);
            $to_month = $this->input->post('to_month', true);
            $section_id = $this->input->post('section_id', true);
            $group = $this->input->post('group', true);
            $year = $this->input->post('year', true);

            $from_transaction_date = $this->input->post('txtFromDate', true);
            $to_transaction_date = $this->input->post('txtToDate', true);

            $ClassName = $this->db->query("SELECT * FROM `tbl_class` WHERE id='$class_id'")->result_array();
            $SectionName = $this->db->query("SELECT * FROM `tbl_section` WHERE id='$section_id'")->result_array();
            $GroupName = $this->db->query("SELECT * FROM `tbl_student_group` WHERE id='$group'")->result_array();
            $SchoolInfo = $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['ClassName'] = $ClassName[0]['name'];
            $Info['SectionName'] = $SectionName[0]['name'];
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['group_name'] = $GroupName[0]['name'];
            $Info['year'] = $year;
            $data['HeaderInfo'] = $Info;
            $data['to_month'] = $to_month;
            $data['fee_category'] = $this->db->query("SELECT * FROM `tbl_fee_category` ORDER BY id ASC")->result_array();
            $data['info'] = $this->Student_fee->get_student_fee_status_report($class_id, $section_id, $group, $year, $to_month, $from_transaction_date, $to_transaction_date);
//            echo '<pre>';
//            print_r( $data['info']);
//            die;
            $this->load->view('report/student_fee_status_report', $data);
        } else {
            $data = array();
            $data['title'] = 'Student Fee Status';
            $data['heading_msg'] = "Student Fee Status";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['maincontent'] = $this->load->view('report/student_fee_status', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function getStudentFeeReceiptForNormal()
    {
        if ($_POST) {
            $data = array();
            $data['title'] = 'Student Fee Status';
            $class_id = $this->input->post('class_id', true);
            $section_id = $this->input->post('section_id', true);
            $group = $this->input->post('group', true);
            $from_date = $this->input->post('from_date', true);
            $to_date = $this->input->post('to_date', true);
            $ClassName = $this->db->query("SELECT * FROM `tbl_class` WHERE id='$class_id'")->result_array();
            $SectionName = $this->db->query("SELECT * FROM `tbl_section` WHERE id='$section_id'")->result_array();
            $GroupName = $this->db->query("SELECT * FROM `tbl_student_group` WHERE id='$group'")->result_array();
            $SchoolInfo = $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['ClassName'] = $ClassName[0]['name'];
            $Info['SectionName'] = $SectionName[0]['name'];
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['web_address'] = $SchoolInfo[0]['web_address'];
            $Info['group_name'] = $GroupName[0]['name'];
            $Info['from_date'] = $from_date;
            $Info['to_date'] = $to_date;
            $data['Info'] = $Info;
            $data['copy_for'] = 'Student Copy';
            $data['report_data'] = $this->Student_fee->getStudentFessDataForNormalReceipt($from_date, $to_date, $class_id, $section_id);
            //	echo '<pre>';
            //print_r( $data['report_data']);
            // die;
            $this->load->view('report/student_fee_receipt_for_normal_print', $data);
        } else {
            $data = array();
            $data['title'] = 'Student Fee Receipt';
            $data['heading_msg'] = "Student Fee Receipt";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['maincontent'] = $this->load->view('report/student_fee_receipt_for_normal', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function getClassWiseCollectionSummery()
    {
        if ($_POST) {
            $data = array();
            $data['title'] = 'Student Fees Collection Summery';
            $year = $this->input->post('year', true);
            $SchoolInfo = $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['year'] = $year;
            $data['HeaderInfo'] = $Info;
            $data['fee_category'] = $this->db->query("SELECT * FROM `tbl_fee_category` ORDER BY id ASC")->result_array();
            $data['category_wise_fee_summery'] = $this->Student_fee->get_category_wise_fee_summery($year);
            $data['month_wise_fee_summery'] = $this->Student_fee->get_month_wise_fee_summery($year);
            $data['absent_play_truant_fine_summery'] = $this->Student_fee->get_absent_play_truant_fine_summery($year);
//            echo '<pre>';
//            print_r( $data['info']);
//            die;
            $this->load->view('report/class_wise_fee_summery_report', $data);
        } else {
            $data = array();
            $data['title'] = 'Student Fees Collection Summery';
            $data['heading_msg'] = "Student Fees Collection Summery";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('report/fees_collection_summery', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }




    public function fetReportHeader()
    {
        return $this->db->query("SELECT * FROM `tbl_contact_info`")->result_array();
    }


    public function get_accounts_report_list()
    {
        $data = array();
        $data['title'] = 'Finance -> Accounts Report';
        $data['heading_msg'] = "Finance -> Accounts Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('report/get_accounts_report_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function get_attendance_report_list()
    {
        $data = array();
        $data['title'] = 'Attendance Report';
        $data['heading_msg'] = "Attendance Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('report/get_attendance_report_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }




    public function student_report_absentee()
    {
        $data = array();
        if ($_POST) {
            $class_id = $_POST['class_id'];
            $section_id = $_POST['section_id'];
            $group = $_POST['group'];
            $date = $_POST['get_year'] . '-' . $_POST['get_month'] . '-' . $_POST['get_day'];

            $check_holiday = $this->Timekeeping->check_holiday($date);
            if (empty($check_holiday)) {
                $cond = array();
                $cond['date'] = $date;
                $data['absentee_info'] = $this->Timekeeping->get_student_report_absentee($date, $class_id, $section_id, $group);
//                echo '<pre>';
//                print_r($data['absentee_info']);
//                die;
                $data['date'] = $date;
                $SchoolInfo = $this->db->query("SELECT `school_name`,`eiin_number` FROM `tbl_contact_info`")->result_array();
                $class = $this->db->query("SELECT name FROM tbl_class WHERE id='$class_id'")->result_array();
                $section = $this->db->query("SELECT name FROM tbl_section WHERE id='$section_id'")->result_array();
                $Info['class'] = $class[0]['name'];
                $Info['section'] = $section[0]['name'];
                $Info['group'] = $group;
                $Info['school_name'] = $SchoolInfo[0]['school_name'];
                $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
                $data['HeaderInfo'] = $Info;
            } else {
                $sdata['exception'] = $date . ", this date are Holiday !";
                $this->session->set_userdata($sdata);
                redirect("report/teacher_staff_report_absentee");
            }
        }

        $data['title'] = 'Student Absentees List Report';
        $data['heading_msg'] = "Student Absentees List Report";
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
        $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('report/report_student_absentee', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function person_wise_login_logout_report()
    {
        $data = array();
        if ($_POST) {
            $teacher_id = $_POST['teacher_id'];
            $staff_id = $_POST['staff_id'];

            if ($teacher_id == '' && $staff_id == '') {
                $sdata['exception'] = "Please select teacher or staff!";
                $this->session->set_userdata($sdata);
                redirect("report/person_wise_login_logout_report");
            }

            if ($teacher_id == "") {
                $person_id = $staff_id;
                $person_type = "ST";
            } else {
                $person_id = $teacher_id;
                $person_type = "T";
            }

            $month = $_POST['get_month'];
            $year = $_POST['get_year'];
            $form_date = $year . "-" . $month . "-01";
            $to_date = $year . "-" . $month . "-" . cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $cond = array();
            $cond['person_type'] = $person_type;
            $cond['person_id'] = $person_id;
            $cond['form_date'] = $form_date;
            $cond['to_date'] = $to_date;

            if ($person_type == 'T') {
                $data['time_keeping_info'] = $this->Timekeeping->get_all_teacher_timekeeping_list_for_device_data(0, 0, $cond);
            } elseif ($person_type == 'ST') {
                $data['time_keeping_info'] = $this->Timekeeping->get_all_staff_timekeeping_list_for_device_data(0, 0, $cond);
            }
//            echo '<pre>';
//            print_r($data['time_keeping_info']);
//            die;
            $data['month'] = $month;
            $data['year'] = $year;
            $data['person_type'] = $person_type;
            $SchoolInfo = $this->db->query("SELECT `school_name`,`eiin_number` FROM `tbl_contact_info`")->result_array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;
        }
        $data['title'] = 'Teacher/Staff Person Wise Login/Logout Report';
        $data['heading_msg'] = "Teacher/Staff Person Wise Login/Logout Report";
        $data['teacher'] = $this->db->query("SELECT id,name,teacher_index_no FROM `tbl_teacher` ORDER BY id")->result_array();
        $data['staff'] = $this->db->query("SELECT id,name,staff_index_no FROM `tbl_staff_info`")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('report/person_wise_login_logout_report', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }



    public function getTeacherAttendanceDetailReport()
    {
        $data = array();
        if ($_POST) {
            $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;

            $data['month'] = $this->input->post("month");

            $data['year'] = $this->input->post("year");
            $data['number_of_days'] = cal_days_in_month(CAL_GREGORIAN, $data['month'], 2005);
            $pdata = array();
            $pdata['month'] = $this->input->post("month");
            $pdata['year'] = $this->input->post("year");
            $pdata['number_of_days'] = $data['number_of_days'];
            $data['title'] = 'Teacher Attendance Details Report';
            $data['heading_msg'] = "Teacher Attendance Details Report";
            $data['adata'] = $this->Timekeeping->get_teacher_attendance_details($pdata);

            if (isset($_POST['pdf_download'])) {
                //echo 'gggg'; die;
                // Load all views as normal
                $data['is_pdf'] = 1;
                $html = $this->load->view('report/teacher_attendance_details_report_table', $data, true);
                $pdfFilePath = "teacher_attendance_details_report.pdf";
                $this->load->library('m_pdf');
                $this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
                //generate the PDF from the given html
                $this->m_pdf->pdf->autoScriptToLang = true;
                $this->m_pdf->pdf->WriteHTML($html);
                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "D");
            }
            $this->load->view('report/teacher_attendance_details_report_table', $data);
        } else {
            $data['title'] = 'Teacher Attendance Details Report';
            $data['heading_msg'] = "Teacher Attendance Details Report";
            $data['years'] = $this->Admin_login->getYearList(0, 0);
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('report/teacher_attendance_details_report', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function getStudentAttendanceSummeryReport()
    {
        $data = array();
        if ($_POST) {
            $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;

            $data['month'] = $this->input->post("month");

            $data['year'] = $this->input->post("year");
            $data['number_of_days'] = cal_days_in_month(CAL_GREGORIAN, $data['month'], 2005);
            $pdata = array();
            $pdata['month'] = $this->input->post("month");
            $pdata['year'] = $this->input->post("year");
            $pdata['number_of_days'] = $data['number_of_days'];
            $data['title'] = 'Student Attendance Summery Report';
            $data['heading_msg'] = "Student Attendance Summery Report";
            $data['adata'] = $this->Timekeeping->get_student_attendance_summery($pdata);
            $data['class'] = $this->db->query("SELECT * FROM `tbl_class` ORDER BY id")->result_array();
            if (isset($_POST['pdf_download'])) {
                // Load all views as normal
                $data['is_pdf'] = 1;

                $page = $this->load->view('report/student_attendance_summery_report_table', $data, true);


                $html = '<html><head><style>
            th,td {
    border: 1px solid;
}
            </style></head>
            <body>' .
                    $page
                    . '</body></html>';

                $pdfFilePath = "student_attendance_summery_report.pdf";
                $this->load->library('m_pdf');
                $this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
                $this->m_pdf->pdf->AddPage(
                    'L', // L - landscape, P - portrait
                    '',
                    '',
                    '',
                    '',
                    10, // margin_left
                    10, // margin right
                    10, // margin top
                    10, // margin bottom
                    10, // margin header
                    10
                ); // margin footer

                //generate the PDF from the given html
                $this->m_pdf->pdf->defaultCSS = true;
                $this->m_pdf->pdf->autoScriptToLang = true;
                $this->m_pdf->pdf->WriteHTML($html);
                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "D");
            }
            $this->load->view('report/student_attendance_summery_report_table', $data);
        } else {
            $data['title'] = 'Student Attendance Summery Report';
            $data['heading_msg'] = "Student Attendance Summery Report";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('report/student_attendance_summery_report', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function dateDiffInDays($date1, $date2)
    {
        // Creates DateTime objects
        $datetime1 = date_create($date1);
        $datetime2 = date_create($date2);

        // Calculates the difference between DateTime objects
        $interval = date_diff($datetime1, $datetime2);

        // Display the result
        return  $interval->format('%a') + 1;
    }








    public function getSpecialCareCollection()
    {
        $data = array();
        $data['title'] = 'Special Care Fee Collection Summery';
        $data['heading_msg'] = "Special Care Fee Collection Summery";
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $this->load->library('numbertowords');
            $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;
            $class_id = $this->input->post("class_id");
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");

            $data['class_id'] = $class_id;
            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
            if ($class_id == 'all') {
                $data['class_name'] = "All";
                $where = "";
            } else {
                $class_info = $this->db->query("SELECT * FROM tbl_class WHERE id ='$class_id'")->result_array();
                $data['class_name'] = $class_info[0]['name'];
                $where = " AND c.`class_id` = '$class_id'";
            }

            $data['rdata'] = $this->db->query("SELECT c.*,s.`name`,s.`student_code`,cl.name as class_name
FROM `tbl_monthly_special_care_fee` AS c
INNER JOIN `tbl_student` AS s ON c.`student_id` = s.`id`
LEFT JOIN tbl_class as cl on cl.id = c.class_id
WHERE (CAST(c.`date` AS DATE) BETWEEN '$from_date' AND '$to_date') $where")->result_array();
            $data['report'] = $this->load->view('report/special_care_collection_summery_table', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            //echo 'gggg'; die;
            // Load all views as normal
            $data['is_pdf'] = 1;
            $html = $this->load->view('report/special_care_collection_summery_table', $data, true);
            $pdfFilePath = "FeeCollectionSummeryReport.pdf";
            $this->load->library('m_pdf');
            $this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
            //generate the PDF from the given html
            $this->m_pdf->pdf->autoScriptToLang = true;
            $this->m_pdf->pdf->WriteHTML($html);
            //download it.
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['students'] = $this->db->query("SELECT id,name,student_code FROM tbl_student")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $data['income_category'] = $this->db->query("SELECT * FROM `tbl_ba_income_category`")->result_array();
            $data['maincontent'] = $this->load->view('report/special_care_collection_summery', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function getResidentFeeCollection()
    {
        $data = array();
        $data['title'] = 'Resident Fee Collection Summery';
        $data['heading_msg'] = "Resident Fee Collection Summery";
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;
            $class_id = $this->input->post("class_id");
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");

            $data['class_id'] = $class_id;
            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
            if ($class_id == 'all') {
                $data['class_name'] = "All";
                $where = "";
            } else {
                $class_info = $this->db->query("SELECT * FROM tbl_class WHERE id ='$class_id'")->result_array();
                $data['class_name'] = $class_info[0]['name'];
                $where = " AND c.`class_id` = '$class_id'";
            }

            $data['rdata'] = $this->db->query("SELECT c.*,s.`name`,s.`student_code`
FROM `tbl_monthly_resident_fee` AS c
INNER JOIN `tbl_student` AS s ON c.`student_id` = s.`id`
WHERE (c.`date` BETWEEN '$from_date' AND '$to_date') $where")->result_array();
            $data['report'] = $this->load->view('report/resident_fee_collection_summery_table', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            //echo 'gggg'; die;
            // Load all views as normal
            $data['is_pdf'] = 1;
            $html = $this->load->view('report/resident_fee_collection_summery_table', $data, true);
            $pdfFilePath = "FeeCollectionSummeryReport.pdf";
            $this->load->library('m_pdf');
            $this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
            //generate the PDF from the given html
            $this->m_pdf->pdf->autoScriptToLang = true;
            $this->m_pdf->pdf->WriteHTML($html);
            //download it.
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['students'] = $this->db->query("SELECT id,name,student_code FROM tbl_student")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $data['income_category'] = $this->db->query("SELECT * FROM `tbl_ba_income_category`")->result_array();
            $data['maincontent'] = $this->load->view('report/resident_fee_collection_summery', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function special_care_non_special_care_student()
    {
        $data = array();
        if ($_POST) {
            $is_specialcare = $this->input->post("is_specialcare");
            if ($is_specialcare == "Y") {
                $title_value = "Special Care";
            } else {
                $title_value = "Non-Special Care";
            }
        } else {
            $title_value = "Special Care/Non-Special Care";
        }

        $data['title'] = $title_value . ' Student List';
        $data['heading_msg'] = $title_value . " Student List";

        if ($_POST) {
            $SchoolInfo = $this->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $data['HeaderInfo'] = $Info;

            $is_resident = $this->input->post("is_resident");
            $year = $this->input->post("year");
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $shift_id = $this->input->post("shift_id");

            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['shift_id'] = $shift_id;
            $data['is_resident'] = $is_resident;

            $class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
            $data['class_name'] = $class_info[0]['name'];

            $section_info = $this->db->query("SELECT name FROM `tbl_section` WHERE id ='$section_id'")->result_array();
            $data['section_name'] = $section_info[0]['name'];

            $group_info = $this->db->query("SELECT name FROM `tbl_student_group` WHERE id ='$group'")->result_array();
            $data['group_name'] = $group_info[0]['name'];

            $shift_info = $this->db->query("SELECT name FROM `tbl_shift` WHERE id ='$shift_id'")->result_array();
            $data['shift_name'] = $shift_info[0]['name'];

            if ($is_specialcare == 'Y') {
                $data['idata'] = $this->db->query("SELECT r.*,s.`name`,s.`roll_no`,s.`student_code`,s.`guardian_mobile` FROM `tbl_student_special_care_info` AS r
INNER JOIN `tbl_student` AS s ON r.`student_id` = s.`id`
WHERE r.`class_id` = '$class_id' AND r.`section_id` = '$section_id' AND r.`group` = '$group' AND r.`shift_id` = '$shift_id' AND r.`year` = '$year'")->result_array();
            } else {
                $data['idata'] = $this->db->query("SELECT s.`name`,s.`roll_no`,s.`student_code`,s.`guardian_mobile` FROM `tbl_student` AS s
WHERE (s.`id` NOT IN (SELECT r.`id` FROM `tbl_student_special_care_info` AS r
WHERE r.`class_id` = '$class_id' AND r.`section_id` = '$section_id' AND r.`group` = '$group' AND r.`shift_id` = '$shift_id' AND r.`year` = '$year'))
AND s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group' AND s.`shift_id` = '$shift_id';")->result_array();
            }

            $data['report'] = $this->load->view('report/special_care_student_report_table', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            //echo 'gggg'; die;
            // Load all views as normal
            $data['is_pdf'] = 1;
            $html = $this->load->view('report/special_care_student_report_table', $data, true);
            $pdfFilePath = "Special Care/Non-SpecialCareStudentReport.pdf";
            $this->load->library('m_pdf');
            $this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
            //generate the PDF from the given html
            $this->m_pdf->pdf->autoScriptToLang = true;
            $this->m_pdf->pdf->WriteHTML($html);
            //download it.
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['shifts'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['maincontent'] = $this->load->view('report/special_care_student_report', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }





    public function ajax_subject_by_class()
    {
        $class_id = $_GET['class_id'];
        $data = array();
        $data['subject_list'] = $this->db->query("SELECT
  c.`subject_id`,
  s.`name`, s.`code`
FROM
  `tbl_class_wise_subject` AS c
  LEFT JOIN `tbl_subject` AS s
    ON s.`id` = c.`subject_id`
WHERE c.`class_id` = '$class_id' GROUP BY c.`subject_id` ORDER BY c.`order_number`")->result_array();
        $this->load->view('report/ajax_subject_by_class', $data);
    }


}
