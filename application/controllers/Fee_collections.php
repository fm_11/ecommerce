<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Fee_collections extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student_fee', 'Student' , 'Config_general' ,'Admin_login', 'Message', 'Timekeeping'));
		$this->load->library('session');
		$this->notification = array();
	}

	public function index()
	{
		$cond = array();
		$data = array();
		if ($_POST) {
			$num_of_row_show = $this->input->post("num_of_row_show");
			if($num_of_row_show == ''){
				$num_of_row_show = 20;
			}
			if($num_of_row_show > 150){
				$num_of_row_show = 150;
			}
			$receipt_no = $this->input->post("receipt_no");
			$amount = $this->input->post("amount");
			$name = $this->input->post("name");
			$roll_no = $this->input->post("roll_no");
			$from_date = $this->input->post("from_date");
			$to_date = $this->input->post("to_date");
			$class_shift_section_id = $this->input->post("class_shift_section_id");
			$student_id = $this->input->post("student_id");

			$sdata['num_of_row_show'] = $num_of_row_show;
			$sdata['receipt_no'] = $receipt_no;
			$sdata['amount'] = $amount;
			$sdata['name'] = $name;
			$sdata['roll_no'] = $roll_no;
			$sdata['from_date'] = $from_date;
			$sdata['to_date'] = $to_date;
			$sdata['class_shift_section_id'] = $class_shift_section_id;
			$sdata['student_id'] = $student_id;
			$this->session->set_userdata($sdata);

			$cond['receipt_no'] = $receipt_no;
			$cond['amount'] = $amount;
			$cond['roll_no'] = $roll_no;
			$cond['name'] = $name;
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
			if ($class_shift_section_id != '') {
				$class_shift_section_arr = explode("-", $class_shift_section_id);
				$cond['class_id']  = $class_shift_section_arr[0];
				$cond['shift_id'] = $class_shift_section_arr[1];
				$cond['section_id'] = $class_shift_section_arr[2];
			}
			$cond['student_id'] = $student_id;
		} else {
			$num_of_row_show = $this->session->userdata('num_of_row_show');
			if($num_of_row_show == ''){
				$sdata['num_of_row_show'] = 20;
				$this->session->set_userdata($sdata);
			}
			$receipt_no = $this->session->userdata('receipt_no');
			$amount = $this->session->userdata('amount');
			$name = $this->session->userdata('name');
			$roll_no = $this->session->userdata('roll_no');
			$from_date = $this->session->userdata('from_date');
			$to_date = $this->session->userdata('to_date');
			$class_shift_section_id = $this->session->userdata('class_shift_section_id');
			$student_id = $this->session->userdata('student_id');
			if ($class_shift_section_id != '') {
				$class_shift_section_arr = explode("-", $class_shift_section_id);
				$cond['class_id']  = $class_shift_section_arr[0];
				$cond['shift_id'] = $class_shift_section_arr[1];
				$cond['section_id'] = $class_shift_section_arr[2];
			}
			$cond['receipt_no'] = $receipt_no;
			$cond['amount'] = $amount;
			$cond['name'] = $name;
			$cond['roll_no'] = $roll_no;
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
			$cond['student_id'] = $student_id;
		}

		if ($class_shift_section_id != '') {
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];
			$data['students'] = $this->db->query("SELECT `id`,`name`,`student_code`,`roll_no` FROM `tbl_student`
    WHERE `class_id` = '$class_id' AND `section_id` = '$section_id' AND `shift_id` = '$shift_id'
    ORDER BY `roll_no`")->result_array();
		} else {
			$data['students'] = array();
		}

		if($num_of_row_show == ''){
			$num_of_row_show = 20;
		}
		$row_per_page = $num_of_row_show;
		$cond['collection_status'] = '1';
		$data['title'] = $this->lang->line('fees'). ' ' . $this->lang->line('collection');
		$data['heading_msg'] =  $this->lang->line('fees'). ' ' . $this->lang->line('collection');
		$data['is_show_button'] = "add";
		$this->load->library('pagination');
		$config['base_url'] = site_url('fee_collections/index/');
		$config['per_page'] = $row_per_page;
		$config['total_rows'] = count($this->Student_fee->get_all_collected_fee(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['fees'] = $this->Student_fee->get_all_collected_fee($row_per_page, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
		$data['fee_module_type'] = $config[0]['fee_module_type'];
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('fee_collections/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}


	public function payslip_index()
	{
		$cond = array();
		$data = array();
		if ($_POST) {
			$num_of_row_show = $this->input->post("num_of_row_show");
			if($num_of_row_show == ''){
				$num_of_row_show = 20;
			}
			if($num_of_row_show > 150){
				$num_of_row_show = 150;
			}
			$receipt_no = $this->input->post("receipt_no");
			$amount = $this->input->post("amount");
			$name = $this->input->post("name");
			$roll_no = $this->input->post("roll_no");
			$from_date = $this->input->post("from_date");
			$to_date = $this->input->post("to_date");
			$class_shift_section_id = $this->input->post("class_shift_section_id");
			$student_id = $this->input->post("student_id");

			$sdata['num_of_row_show'] = $num_of_row_show;
			$sdata['receipt_no'] = $receipt_no;
			$sdata['amount'] = $amount;
			$sdata['name'] = $name;
			$sdata['roll_no'] = $roll_no;
			$sdata['from_date'] = $from_date;
			$sdata['to_date'] = $to_date;
			$sdata['class_shift_section_id'] = $class_shift_section_id;
			$sdata['student_id'] = $student_id;
			$this->session->set_userdata($sdata);

			$cond['receipt_no'] = $receipt_no;
			$cond['amount'] = $amount;
			$cond['roll_no'] = $roll_no;
			$cond['name'] = $name;
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
			if ($class_shift_section_id != '') {
				$class_shift_section_arr = explode("-", $class_shift_section_id);
				$cond['class_id']  = $class_shift_section_arr[0];
				$cond['shift_id'] = $class_shift_section_arr[1];
				$cond['section_id'] = $class_shift_section_arr[2];
			}
			$cond['student_id'] = $student_id;
		} else {
			$num_of_row_show = $this->session->userdata('num_of_row_show');
			if($num_of_row_show == ''){
				$sdata['num_of_row_show'] = 20;
				$this->session->set_userdata($sdata);
			}
			$receipt_no = $this->session->userdata('receipt_no');
			$amount = $this->session->userdata('amount');
			$name = $this->session->userdata('name');
			$roll_no = $this->session->userdata('roll_no');
			$from_date = $this->session->userdata('from_date');
			$to_date = $this->session->userdata('to_date');
			$class_shift_section_id = $this->session->userdata('class_shift_section_id');
			$student_id = $this->session->userdata('student_id');
			if ($class_shift_section_id != '') {
				$class_shift_section_arr = explode("-", $class_shift_section_id);
				$cond['class_id']  = $class_shift_section_arr[0];
				$cond['shift_id'] = $class_shift_section_arr[1];
				$cond['section_id'] = $class_shift_section_arr[2];
			}
			$cond['receipt_no'] = $receipt_no;
			$cond['amount'] = $amount;
			$cond['name'] = $name;
			$cond['roll_no'] = $roll_no;
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
			$cond['student_id'] = $student_id;
		}

		if ($class_shift_section_id != '') {
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];
			$data['students'] = $this->db->query("SELECT `id`,`name`,`student_code`,`roll_no` FROM `tbl_student`
    WHERE `class_id` = '$class_id' AND `section_id` = '$section_id' AND `shift_id` = '$shift_id'
    ORDER BY `roll_no`")->result_array();
		} else {
			$data['students'] = array();
		}

		if($num_of_row_show == ''){
			$num_of_row_show = 20;
		}
		$row_per_page = $num_of_row_show;
		$cond['is_payslip'] = '1';
		$cond['collection_status'] = '0';
		$data['title'] = "Payslip List";
		$data['heading_msg'] =  "Payslip List";
		$data['is_show_button'] = "add";
		$this->load->library('pagination');
		$config['base_url'] = site_url('fee_collections/payslip_index/');
		$config['per_page'] = $row_per_page;
		$config['total_rows'] = count($this->Student_fee->get_all_collected_fee(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['fees'] = $this->Student_fee->get_all_collected_fee($row_per_page, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
		$data['fee_module_type'] = $config[0]['fee_module_type'];
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('fee_collections/payslip_index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}



	public function getCollectionSheet()
	{
		$class_shift_section_id = $this->input->get("class_shift_section_id");

		$class_shift_section_arr = explode("-", $class_shift_section_id);
		$class_id  = $class_shift_section_arr[0];
		$shift_id = $class_shift_section_arr[1];
		$section_id = $class_shift_section_arr[2];

		$group_id = $this->input->get('group_id', true);
		$student_id = $this->input->get('student_id', true);
		$year = $this->input->get('year', true);
		$months = $this->input->get('month', true);
		//echo $months;
		//die;
		$collection_month = $this->input->get('collection_month', true);

		$data = array();
		$data['class_id'] = $class_id;
		$data['group_id'] = $group_id;
		$data['shift_id'] = $shift_id;
		$data['section_id'] = $section_id;
		$data['student_id'] = $student_id;
		$data['year'] = $year;
		$data['months'] = $months;
		$data['collection_month'] = $collection_month;

		$student_info = $this->db->query("SELECT `category_id` FROM `tbl_student` WHERE `id` = $student_id;")->row();
		$data['student_category_id'] = $student_info->category_id;

		$process_data = $this->processCollectionSheet($data);
		$this->load->view('fee_collections/fee_collection_sheet', $process_data);
	}


	public function processCollectionSheet($p_data)
	{
		$class_id = $p_data['class_id'];
		$group_id = $p_data['group_id'];
		$student_id = $p_data['student_id'];
		//echo $student_id; die;
		$year = $p_data['year'];
		$months = $p_data['months'];
		$collection_month = $p_data['collection_month'];

		$student_category_id = $p_data['student_category_id'];
		$data['student_category_id'] = $student_category_id;

		//echo $months;
		//  die;
		$months_array = explode(",", $months);
//		 echo '<pre>';
//		 print_r($months_array);
//		 die;


		$global_config = $this->Admin_login->get_global_config();
		if (!empty($global_config)) {
			if ($global_config[0]['show_due_amount_to_collection_sheet'] != '') {
				$show_due_amount_to_collection_sheet = $global_config[0]['show_due_amount_to_collection_sheet'];
				if ($show_due_amount_to_collection_sheet == '1') {
					//  echo $collection_month;

					$last_month_num_for_collection = $months_array[count($months_array) - 1];
					$loop_end_month = 0;
					if ($last_month_num_for_collection < $collection_month) {
						$loop_end_month = $collection_month;
					} else {
						$loop_end_month = $last_month_num_for_collection;
					}
					//  echo $loop_end_month;
					//  die;
					$month_make_loop = 0;
					$new_month_array = array();
					while ($month_make_loop < $loop_end_month) {
						$new_month_array[$month_make_loop] = ($month_make_loop + 1);
						$month_make_loop++;
					}
					$months_array = $new_month_array;
					$months = implode(", ", $new_month_array);
					//echo $months;
					//die;
					//print_r($months_array);
					//  die;
				}
			}
		}


		$i = 0;
		$data['allocated_list'] = array();
		foreach ($months_array as $collection_month){
			$get_allocated_list = $this->Student_fee->getStudentWiseAllocatedFeeCategory($class_id,$group_id,$student_category_id,$year,$student_id,$collection_month);
			foreach ($get_allocated_list as $allocated_data){
				$sub_category_id = $allocated_data['sub_category_id'];
				$data['allocated_list'][$i]['category_id'] = $allocated_data['category_id'];
				$data['allocated_list'][$i]['sub_category_id'] = $sub_category_id;
				//data process
				$waiver_amount = 0;
				if ($allocated_data['is_waiver_applicable'] == 1) {
					$waiver_info = $this->db->query("SELECT * FROM `tbl_student_fee_waiver` AS w
                    WHERE w.`student_id` = '$student_id' AND w.`fees_category_id` = '$sub_category_id'
                     AND w.`year` = '$year'")->result_array();
					if (!empty($waiver_info)) {
						$is_percentage = $waiver_info[0]['is_percentage'];
						if ($is_percentage == '0') {
							$waiver_amount = $waiver_info[0]['amount'];
						} else {
							$waiver_amount = ($allocated_data['amount'] * $waiver_info[0]['amount']) / 100;
						}
					}
				}
				$data['allocated_list'][$i]['waiver_amount'] = $waiver_amount;
				//waiver calculate end

				//add month name in sub category name
				$monthDateObj = DateTime::createFromFormat('!m', $collection_month);
				$data['allocated_list'][$i]['sub_category_name'] = $allocated_data['sub_category_name']. ', '. $monthDateObj->format('F');
				$data['allocated_list'][$i]['month'] = $collection_month;
				//add month name in sub category name

				//actual paidable amount
				$resident_info = $this->db->query("SELECT * FROM `tbl_student_resident_info` AS r
                                 WHERE r.`student_id` = '$student_id' AND r.`year` = '$year'")->result_array();
				if (!empty($resident_info)) {
					$data['allocated_list'][$i]['actual_allocated_amount_for_this_student'] = $allocated_data['resident_amount'] - $waiver_amount;
				} else {
					$data['allocated_list'][$i]['actual_allocated_amount_for_this_student'] = $allocated_data['amount'] - $waiver_amount;
				}
				//actual paidable amount

				$already_paid_info = $this->db->query("SELECT SUM(c.`paid_amount`) AS paid_amount, SUM(c.`discount_amount`) AS discount_amount 
                                     FROM `tbl_fee_collection_details` AS c
                                      WHERE c.`student_id` = '$student_id' 
                                      AND c.`sub_category_id` = '$sub_category_id' 
                                      AND c.`year` = '$year' AND c.`month` = '$collection_month'")->result_array();


//				if($sub_category_id == 3){
//					echo '<pre>';
//					print_r($already_paid_info);
//					echo $this->db->last_query();
//					die;
//				}

				if ($already_paid_info[0]['paid_amount'] > 0) {
					$data['allocated_list'][$i]['already_paid_amount'] = $already_paid_info[0]['paid_amount'];
				} else {
					$data['allocated_list'][$i]['already_paid_amount'] = 0;
				}

				//already discount

				if ($already_paid_info[0]['discount_amount'] > 0) {
					$data['allocated_list'][$i]['already_discount_amount'] = $already_paid_info[0]['discount_amount'];
				} else {
					$data['allocated_list'][$i]['already_discount_amount'] = 0;
				}
				//data process

				$i++;
			}
		}

		//Transport fee
		$transport_fees = array();
		$gen_config = $this->db->query("SELECT is_transport_fee_applicable FROM `tbl_config`")->result_array();
		if (empty($gen_config)) {
			$data['is_transport_fee_applicable'] = 0;
			$data['transport_fee_amount'] = 0;
		} else {
			$data['is_transport_fee_applicable'] = $gen_config[0]['is_transport_fee_applicable'];
			if ($data['is_transport_fee_applicable'] == '1') {
				$student_detail_info = $this->db->query("SELECT transport_fee_amount FROM `tbl_student` WHERE id = '$student_id'")->result_array();

				$k = 0;
				while ($k < count($months_array)) {
					$month = $months_array[$k];
					$transport_fees[$k]['actual_amount'] = $student_detail_info[0]['transport_fee_amount'];
					$already_paid_transport_fee = $this->db->query("SELECT * FROM `tbl_student_monthly_transport_fee`
                                                WHERE month = '$month' AND year = '$year' AND student_id = '$student_id'")->result_array();


					if (!empty($already_paid_transport_fee)) {
						$transport_fees[$k]['alreay_paid_transport_fee'] = $already_paid_transport_fee[0]['amount'];
					} else {
						$transport_fees[$k]['alreay_paid_transport_fee'] = 0;
					}
					$transport_fees[$k]['month'] = $month;
					$transport_monthDateObj = DateTime::createFromFormat('!m', $month);
					$transport_fees[$k]['title'] = 'Transport Fee '. $transport_monthDateObj->format('F') . ', '. $year;
					$transport_fees[$k]['year'] = $year;
					$k++;
				}
			}
		}
		$data['transport_fees'] = $transport_fees;
		$date = new DateTime();
		$data['receipt_no'] = $date->getTimestamp();
		$data['general_config'] = $this->Config_general->get_general_configurations('general');
		return $data;
	}

	public function add()
	{

		$user_info = $this->session->userdata('user_info');
		//echo $user_info[0]->id;
		//  die;
		$user_asset_head = $this->Admin_login->getUserWiseAssetHead($user_info[0]->id);
//         echo '<pre>';
//         print_r($user_asset_head);
//         die;
		if (empty($user_asset_head)) {
			$sdata['exception'] = "Please set asset head for this user";
			$this->session->set_userdata($sdata);
			redirect("user_wise_income_heads/index");
		}


		if ($_POST) {
			$posted_receipt_no = $this->input->post('receipt_no', true);
			$checkIfExists = $this->db->query("SELECT id FROM `tbl_fee_collection`
                             WHERE receipt_no = '$posted_receipt_no'")->result_array();
			if (!empty($checkIfExists)) {
				$sdata['exception'] = "This receipt number already exists";
				$this->session->set_userdata($sdata);
				redirect("fee_collections/index");
			}

			$class_shift_section_id = $this->input->post('class_shift_section_id');
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$data['class_shift_section_id'] = $class_shift_section_id;
			$class_id = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$group_id = $this->input->post('group_id', true);
			$student_category_id = $this->input->post('student_category_id', true);
			$student_id = $this->input->post('student_id', true);
			$manual_collection_date = $this->input->post('manual_collection_date', true);
			$year = $this->input->post('year', true);

			$waiver_setting_info_check = $this->db->query("SELECT id FROM `tbl_fee_waiver_setting` WHERE year = '$year'")->result_array();
			if (empty($waiver_setting_info_check)) {
				$sdata['exception'] = "Please add waiver configuration first";
				$this->session->set_userdata($sdata);
				redirect("waiver_settings/index");
			}

			// echo '<pre>';
			// 	print_r($_POST);
			// die;

			//echo $group_id; die;

			$cdata['class_shift_section_id'] = $class_shift_section_id;
			$cdata['c_group_id'] = $group_id;
			$cdata['c_year'] = $year;
			$cdata['c_student_id'] = $student_id;
			$this->session->set_userdata($cdata);


			$collection_month = $this->input->post('collection_month', true);
			$mode_of_pay = $this->input->post('mode_of_pay', true);
			$do_you_want_receipt = $this->input->post('do_you_want_receipt', true);
			$do_you_want_send_sms = $this->input->post('do_you_want_send_sms', true);
			$num_of_row = $this->input->post('num_of_row', true);
			$transport_fee_row = $this->input->post('transport_fee_row', true);

			$num_of_row = $num_of_row - $transport_fee_row;

			$is_resident_transaction = 0;
			$is_resident_info = $this->db->query("SELECT id FROM tbl_student_resident_info WHERE student_id = '$student_id' AND year = '$year'")->result_array();
			if (!empty($is_resident_info)) {
				$is_resident_transaction = 1;
			}

			//echo $is_resident_transaction; die;

			$guardian_mobile = "0";
			$student_info = $this->db->query("SELECT s.id,s.shift_id,s.group,s.guardian_mobile FROM tbl_student AS s WHERE s.id = '$student_id'")->result_array();
			if (!empty($student_info)) {
				$guardian_mobile = $student_info[0]['guardian_mobile'];
			}

			$mdata = array();
			$mdata['class_id'] = $class_id;
			$mdata['group_id'] = $group_id;
			$mdata['shift_id'] = $shift_id;
			$mdata['student_category_id'] = $student_category_id;
			$mdata['section_id'] = $section_id;
			$mdata['student_id'] = $student_id;
			$mdata['year'] = $year;
			$mdata['month'] = $collection_month; //its collection month
			$mdata['mode_of_pay'] = $mode_of_pay;
			$mdata['receipt_no'] = $this->input->post('receipt_no', true);
			$mdata['date'] = $manual_collection_date;
			$mdata['is_resident_transaction'] = $is_resident_transaction;
			$mdata['total_discount'] = $this->input->post('total_discount', true);
			$mdata['total_paid_amount'] = $this->input->post('total_paid_amount', true);

			if(isset($_POST['make_payslip'])){
				$mdata['is_payslip'] = '1';
				$mdata['collection_status'] = '0';
			}else{
				$mdata['is_payslip'] = '0';
				$mdata['collection_status'] = '1';
			}

			//asset amount
			$total_amount_for_asset =  $this->input->post('total_paid_amount', true);

			$user_info = $this->session->userdata('user_info');
			$mdata['user_id'] = $user_info[0]->id;

			$mdata['entry_date'] = date('Y-m-d H:i:s');
			if ($this->input->post('do_you_collect_late_fee')) {
				$mdata['do_you_collect_late_fee'] = 'Y';
			} else {
				$mdata['do_you_collect_late_fee'] = 'N';
			}
			if ($this->db->insert('tbl_fee_collection', $mdata)) {
				$collection_id = $this->db->insert_id();

				if ($this->input->post('do_you_collect_late_fee')) {
					$late_fee_data = array();
					$late_fee_data['student_id'] = $student_id;
					$late_fee_data['collection_id'] = $collection_id;
					$late_fee_data['month'] = $collection_month;
					$late_fee_data['year'] = $year;
					$late_fee_data['amount'] = $this->input->post('late_fee', true);
					//asset head  amount
					$total_amount_for_asset +=   $this->input->post('late_fee', true);
					$this->db->insert('tbl_fee_late_fine', $late_fee_data);
				}

				$i = 1;
				while ($i <= $num_of_row) {
					if ($this->input->post('is_selected_' . $i)) {
						$cdata = array();
						$cdata['collection_id'] = $collection_id;
						$cdata['class_id'] = $class_id;
						$cdata['section_id'] = $section_id;
						$cdata['group_id'] = $group_id;
						$cdata['shift_id'] = $shift_id;
						$cdata['student_category_id'] = $student_category_id;
						$cdata['student_id'] = $student_id;
						$cdata['month'] = $this->input->post('month_' . $i);
						$cdata['year'] = $year;
						$cdata['category_id'] = $this->input->post('category_id_' . $i);
						$cdata['sub_category_id'] = $this->input->post('sub_category_id_' . $i);
						$cdata['waiver_amount'] = $this->input->post('waiver_amount_' . $i);
						$cdata['actual_amount'] = $this->input->post('actual_amount_' . $i);
						$cdata['discount_amount'] = $this->input->post('discount_amount_' . $i);
						$cdata['paid_amount'] = $this->input->post('paid_amount_' . $i);
						$cdata['due_amount'] = $this->input->post('due_amount_' . $i);
						if(isset($_POST['make_payslip'])){
							$cdata['collection_status'] = '0';
						}else{
							$cdata['collection_status'] = '1';
						}
						$cdata['entry_date'] = date('Y-m-d H:i:s');
						$this->db->insert('tbl_fee_collection_details', $cdata);
					}
					$i++;
				}
			}


			//transport fee save
			$gen_config = $this->db->query("SELECT is_transport_fee_applicable FROM `tbl_config`")->result_array();
			if (empty($gen_config)) {
				$is_transport_fee_applicable = 0;
			} else {
				$is_transport_fee_applicable = $gen_config[0]['is_transport_fee_applicable'];
			}
			if ($transport_fee_row > 0 && $is_transport_fee_applicable == '1') {
				$k = 0;
				while ($k < $transport_fee_row) {
					if ($this->input->post('transport_fee_selected_' . $k)) {
						$transport_data = array();
						$transport_data['collection_id'] = $collection_id;
						$transport_data['student_id'] = $student_id;
						$transport_data['month'] = $this->input->post('transport_fee_month_' . $k);
						$transport_data['year'] = $year;
						$transport_data['amount'] = $this->input->post('paid_transport_fee_' . $k);
						//asset head  amount
						$total_amount_for_asset +=  $this->input->post('paid_transport_fee_' . $k);
						$transport_data['date'] = date('Y-m-d H:i:s');
						$this->db->insert('tbl_student_monthly_transport_fee', $transport_data);
					}
					$k++;
				}
			}

			//transport fee save end

			if(isset($_POST['submit'])) {
				if ($do_you_want_send_sms) {
					//sms send
					if ($this->Admin_login->validate_mobile_number($guardian_mobile)) {
						$contact_info = $this->db->query("SELECT * FROM `tbl_contact_info`")->result_array();
						$school_name = $contact_info[0]['school_name'];
						$total_paid_amount = $this->input->post('total_paid_amount', true);
						$msg = "Your son/daughter tuition fee has deposited Tk. $total_paid_amount. Thank you - Principal, $school_name";
						if ($total_paid_amount > 0) {
							$send_from = 'TG';
							$numbers = array();
							$numbers_with_student_id = array();
							//sms send data ready
							$number = trim($guardian_mobile);
							$numbers[] = $number;
							$numbers_with_student_id[$number]['student_id'] = $student_id;
							$numbers_with_student_id[$number]['number'] = $number;
							//sms send data ready end
							$this->Message->bulkSMSSend($numbers, 0, $msg, $send_from, $numbers_with_student_id);

						}
					}
					//sms send
				}

				$assetdata = array();
				$assetdata['user_id'] = $user_asset_head[0]['user_id'];
				$assetdata['asset_id'] = $user_asset_head[0]['asset_category_id'];
				$assetdata['amount'] = $total_amount_for_asset;
				$assetdata['type'] = 'I';
				$assetdata['from_transaction_type'] = 'SF';
				$assetdata['date'] = $manual_collection_date;
				$assetdata['reference_id'] = $collection_id;
				$assetdata['added_by'] = $user_info[0]->id;
				$assetdata['remarks'] = 'From Student Fees (' . $this->input->post('receipt_no', true) . ')';
				$this->db->insert('tbl_user_wise_asset_transaction', $assetdata);
				//asset entry
				if ($do_you_want_receipt) {
					redirect("student_fees/getStudentReceipt/" . $collection_id . "/S");
				} else {
					$sdata['message'] = "Data added successfully";
					$this->session->set_userdata($sdata);
					redirect("fee_collections/add");
				}
			}else{
				$sdata['message'] = "Payslip make successfully";
				$this->session->set_userdata($sdata);
				redirect("fee_collections/add");
			}
		} else {
			$data = array();
			$data['title'] =  $this->lang->line('fees'). ' ' . $this->lang->line('collection');
			$data['heading_msg'] =  $this->lang->line('fees'). ' ' . $this->lang->line('collection');
			$data['is_show_button'] = "index";
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();

			$data['class_shift_section_id'] = $this->session->userdata('class_shift_section_id');
			$data['c_group_id'] = $this->session->userdata('c_group_id');
			$data['c_student_id'] = $this->session->userdata('c_student_id');
			$data['c_year'] = $this->session->userdata('c_year');
			if($data['c_year'] == ''){
				$data['c_year'] = date('Y');
			}

			if ($data['class_shift_section_id'] != '') {

				$class_shift_section_id = $data['class_shift_section_id'];
				//  echo $class_shift_section_id; die;
				$class_shift_section_arr = explode("-", $class_shift_section_id);
				$data['class_shift_section_id'] = $class_shift_section_id;
				$class_id = $class_shift_section_arr[0];
				$shift_id = $class_shift_section_arr[1];
				$section_id = $class_shift_section_arr[2];

				$group_where = "";
				if($data['c_group_id'] != ''){
					$group_id = $data['c_group_id'];
					$group_where = " AND `group` = '$group_id' ";
				}

				$data['students'] = $this->db->query("SELECT id,name,student_code,roll_no FROM tbl_student
              WHERE class_id = '$class_id' AND section_id = '$section_id' AND shift_id = '$shift_id'
              AND status = '1' $group_where ORDER BY ABS(roll_no)")->result_array();
			} else {
				$data['students'] = null;
			}
			//echo $group_id; die;
			//echo '<pre>';
			//print_r($data);
			//die;
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('fee_collections/add', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

	function payslip_approve($collection_id){
		$payslip_setting = $this->Admin_login->get_payslip_setting();
		if(empty($payslip_setting)){
			$sdata['exception'] = "Please payslip setting first";
			$this->session->set_userdata($sdata);
			redirect("fee_collections/payslip_index");
		}
		$collection_info = $this->db->query("SELECT * FROM `tbl_fee_collection` 
                            WHERE `id` = $collection_id;")->row();
		$user_info = $this->session->userdata('user_info');
		if($payslip_setting->collected_user == 'PA'){
            $user_id = $user_info[0]->id;
		}else{
			$user_id = $collection_info->user_id;
		}
		//asset entry
		$assetdata = array();
		$assetdata['user_id'] = $user_id;
		$assetdata['asset_id'] = $payslip_setting->asset_head_id;
		$assetdata['amount'] = $collection_info->total_paid_amount;
		$assetdata['type'] = 'I';
		$assetdata['from_transaction_type'] = 'SF';
		$assetdata['date'] = date('Y-m-d');
		$assetdata['reference_id'] = $collection_id;
		$assetdata['added_by'] = $user_info[0]->id;
		$assetdata['remarks'] = 'From Student Fees (' . $collection_info->receipt_no . ')';
		$this->db->insert('tbl_user_wise_asset_transaction', $assetdata);

		//data update
		$c_data = array();
		$c_data['id'] = $collection_id;
		$c_data['collection_status'] = '1';
		$c_data['user_id'] = $user_id;
		$this->db->where('id', $c_data['id']);
		$this->db->update('tbl_fee_collection', $c_data);

		$cd_data = array();
		$cd_data['collection_id'] = $collection_id;
		$cd_data['collection_status'] = '1';
		$this->db->where('collection_id', $cd_data['collection_id']);
		$this->db->update('tbl_fee_collection_details', $cd_data);

		$sdata['message'] = "Payslip approved successfully";
		$this->session->set_userdata($sdata);
		redirect("fee_collections/payslip_index");
	}


	public function quick_add()
	{
		$user_info = $this->session->userdata('user_info');
		//echo $user_info[0]->id;
		//  die;
		$user_asset_head = $this->Admin_login->getUserWiseAssetHead($user_info[0]->id);
		// echo '<pre>';
		// print_r($user_asset_head);
		// die;
		if (empty($user_asset_head)) {
			$sdata['exception'] = "Please set asset head for this user";
			$this->session->set_userdata($sdata);
			redirect("user_wise_income_heads/index");
		}
		$data = array();
		if ($_POST) {
			$class_shift_section_id = $this->input->post('class_shift_section_id');
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$data['class_shift_section_id'] = $class_shift_section_id;
			$class_id = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$group_id = $this->input->post('group_id');
			//echo $gorup_id;die;
			$data['group_id'] = $group_id;

			$parameters = array();
			$parameters['class_id'] = $class_id;
			$parameters['shift_id'] = $shift_id;
			$parameters['section_id'] = $section_id;
			$parameters['group'] = $group_id;
			$parameters['student_status'] = 1;
			$data['students'] = $this->Student->get_all_student_list(0, 0, $parameters);
//			echo '<pre>';
//			print_r($data['students']);
//			die;
			$data['student_info'] = $this->load->view('fee_collections/quick_add_student_list', $data, true);
		}

		$data['title'] =  $this->lang->line('fees'). ' ' . $this->lang->line('collection');
		$data['heading_msg'] =  $this->lang->line('fees'). ' ' . $this->lang->line('collection');
		$data['is_show_button'] = "index";
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('fee_collections/quick_add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}


	public function quick_fee_collect()
	{
		//echo $this->input->post('is_new_admission');
		//  die;
		$months = $this->input->post('month');

		$class_shift_section_id = $this->input->post('class_shift_section_id');
		$class_shift_section_arr = explode("-", $class_shift_section_id);
		$data['class_shift_section_id'] = $class_shift_section_id;
		$class_id = $class_shift_section_arr[0];
		$shift_id = $class_shift_section_arr[1];
		$section_id = $class_shift_section_arr[2];

		$group_id = $this->input->post('group_id');
		$data['group_id'] = $group_id;
		$student_category_id = $this->input->post('student_category_id');
		$data['student_category_id'] = $student_category_id;
		//echo $group_id; die;

		$pdata = array();
		$pdata['class_id'] = $class_id;
		$pdata['shift_id'] = $shift_id;
		$pdata['group_id'] = $group_id;
		$pdata['student_category_id'] = $student_category_id;
		$pdata['section_id'] = $section_id;
		$pdata['student_id'] = $this->input->post('student_id');
		$pdata['year'] = date('Y');
		$pdata['months'] = implode(",", $months);
		//$months_array_for_collection = explode(",", $months);
		//echo(max($months));
		//die;
		$pdata['collection_month'] = max($months);

		$parameter_data = $this->processCollectionSheet($pdata);
		$parameter_data['is_quick_data_save'] = 1;
		$parameter_data['class_id'] =  $class_id;
		$parameter_data['section_id'] =  $section_id;
		$parameter_data['student_id'] =  $this->input->post('student_id');
		$parameter_data['year'] = date('Y');
		$parameter_data['months'] = implode(",", $months);
		$parameter_data['collection_month'] = date('m');
		$parameter_data['group_id'] = $group_id;
		$parameter_data['class_shift_section_id'] = $class_shift_section_id;
		$data['title'] =  $this->lang->line('fees'). ' ' . $this->lang->line('collection');
		$data['heading_msg'] =  $this->lang->line('fees'). ' ' . $this->lang->line('collection');
		$data['is_show_button'] = "index";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('fee_collections/fee_collection_sheet', $parameter_data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	function view_payslip($collection_id){
		$this->load->library('numbertowords');
		//echo 'fgg';
		//die;
		$data = array();
		$data['title'] = "Student Payslip";
		$data['school_info'] = $this->Admin_login->get_contact_info();
		$data['collection_sheet_info'] = $this->Student_fee->get_all_collection_sheet_info($collection_id);
		$collection_sheet_details = $this->Student_fee->get_all_collection_sheet_details_single($collection_id);
		$data['collection_sheet_details'] = $collection_sheet_details;
		$late_fee = $this->db->query("SELECT * FROM tbl_fee_late_fine WHERE collection_id = '$collection_id'")->result_array();
		// echo '<pre>';
		// print_r($collection_sheet_details);
		// die;
		if (!empty($late_fee)) {
			$data['late_fee'] = $late_fee[0]['amount'];
		} else {
			$data['late_fee'] = 0;
		}

		$config = $this->db->query("SELECT is_transport_fee_applicable FROM tbl_config")->result_array();
		$is_transport_fee_applicable = $config[0]['is_transport_fee_applicable'];
		if ($is_transport_fee_applicable == '1') {
			$data['transport_fees'] = $this->db->query("SELECT * FROM tbl_student_monthly_transport_fee WHERE collection_id = '$collection_id'")->result_array();
		} else {
			$data['transport_fees'] = null;
		}

		$data['signature'] = $this->Admin_login->getSignatureByAccessCode('FR'); //FR= Fee receipt
		$this->load->view('fee_collections/view_payslip', $data);
	}

	public function delete($id)
	{
		if ($this->db->delete('tbl_fee_collection_details', array('collection_id' => $id))) {
			$config = $this->db->query("SELECT is_transport_fee_applicable FROM tbl_config")->result_array();
			$is_transport_fee_applicable = $config[0]['is_transport_fee_applicable'];
			if ($is_transport_fee_applicable == '1') {
				$this->db->delete('tbl_student_monthly_transport_fee', array('collection_id' => $id));
			}

			$this->db->delete('tbl_fee_collection', array('id' => $id));
			$this->db->delete('tbl_user_wise_asset_transaction', array('reference_id' => $id,'type' => 'I','from_transaction_type' => 'SF'));
			$sdata['message'] = $this->lang->line('delete_success_message');
			$this->session->set_userdata($sdata);
			redirect("fee_collections/index");
		} else {
			$sdata['exception'] = "Data Doesn't Delete !";
			$this->session->set_userdata($sdata);
			redirect("fee_collections/index");
		}
	}

	//student portal collection process
	public function fee_collect_process_from_student_portal()
	{
		$months = $this->input->post('month');
		$class_id = $this->input->post('class_id');
		$shift_id = $this->input->post('shift_id');
		$section_id = $this->input->post('section_id');
		$group_id = $this->input->post('group_id');
		$student_category_id = $this->input->post('student_category_id');
		$data['group_id'] = $group_id;

		$pdata = array();
		$pdata['class_id'] = $class_id;
		$pdata['group_id'] = $group_id;
		$pdata['section_id'] = $section_id;
		$pdata['student_category_id'] = $student_category_id;
		$pdata['student_id'] = $this->input->post('student_id');
		$pdata['year'] = date('Y');
		$pdata['months'] = implode(",", $months);
		//$months_array_for_collection = explode(",", $months);
		//echo(max($months));
		//die;
		$pdata['collection_month'] = date('m');

		$parameter_data = $this->processCollectionSheet($pdata);
		$parameter_data['is_quick_data_save'] = 1;
		$parameter_data['class_id'] =  $class_id;
		$parameter_data['section_id'] =  $section_id;
		$parameter_data['student_id'] =  $this->input->post('student_id');
		$parameter_data['year'] = date('Y');
		$parameter_data['months'] = implode(",", $months);
		$parameter_data['collection_month'] = date('m');
		$parameter_data['group_id'] = $group_id;
		$parameter_data['class_id'] = $class_id;
		$parameter_data['section_id'] = $section_id;
		$parameter_data['shift_id'] = $shift_id;
		$parameter_data['student_category_id'] = $student_category_id;
		$data['title'] =  $this->lang->line('fees'). ' ' . $this->lang->line('payment');
		$data['heading_msg'] =  $this->lang->line('fees'). ' ' . $this->lang->line('payment');
		$data['is_show_button'] = "index";
		$data['payment_gateway_config'] = $this->Admin_login->get_online_payment_gateway_setting();
		$data['left_menu'] = $this->load->view('student_guardian_panels/menu', $data, true);
		$data['maincontent'] = $this->load->view('fee_collections/fee_collect_process_from_student_portal', $parameter_data, true);
		$this->load->view('student_guardian_panels/index', $data);
	}

	//student portal collection process
}
