<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Students extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Message','Admin_login','common/custom_methods_model','Student_fee'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        //For Excel Read
        require_once APPPATH . 'third_party/PHPExcel.php';
        $this->excel = new PHPExcel();
        //For Excel Read


        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        //echo '<pre>'; print_r($user_info); die;
        $this->SOFTWARE_START_YEAR = $user_info[0]->config_info[0]['software_start_year'];
        $this->notification = array();
    }



    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('student_list');
        $cond = array();
		$class_id = '';
		$shift_id = '';
		$section_id = '';
        if ($_POST) {
            $roll = $this->input->post("roll");
            $group = $this->input->post("group");
            $religion = $this->input->post("religion");
            $gender = $this->input->post("gender");
            $blood_group_id = $this->input->post("blood_group_id");
            $student_status = $this->input->post("student_status");
			$class_shift_section_id = $this->input->post("class_shift_section_id");

			if ($class_shift_section_id != '') {
				$class_shift_section_arr = explode("-", $class_shift_section_id);
				$class_id  = $class_shift_section_arr[0];
				$shift_id = $class_shift_section_arr[1];
				$section_id = $class_shift_section_arr[2];
			}

            $sdata['roll'] = $roll;
            $sdata['class_shift_section_id'] = $class_shift_section_id;
            $sdata['group'] = $group;
            $sdata['religion'] = $religion;
            $sdata['gender'] = $gender;
            $sdata['blood_group_id'] = $blood_group_id;
            $sdata['student_status'] = $student_status;
            $this->session->set_userdata($sdata);
            $cond['roll'] = $roll;
            $cond['class_id'] = $class_id;
			$cond['shift_id'] = $shift_id;
            $cond['section_id'] = $section_id;
            $cond['group'] = $group;
            $cond['religion'] = $religion;
            $cond['gender'] = $gender;
            $cond['blood_group_id'] = $blood_group_id;
            $cond['student_status'] = $student_status;
        } else {
            $roll = $this->session->userdata('roll');
            $class_shift_section_id = $this->session->userdata('class_shift_section_id');
            $group = $this->session->userdata('group');
            $gender = $this->session->userdata('gender');
            $religion = $this->session->userdata('religion');
            $blood_group_id = $this->session->userdata('blood_group_id');
            $student_status = $this->session->userdata('student_status');
            if ($student_status == '') {
                $student_status = 1;
                $cond['student_status'] = 1;
            } else {
                $cond['student_status'] = $student_status;
            }

			if ($class_shift_section_id != '') {
				$class_shift_section_arr = explode("-", $class_shift_section_id);
				$class_id  = $class_shift_section_arr[0];
				$shift_id = $class_shift_section_arr[1];
				$section_id = $class_shift_section_arr[2];
			}

            $cond['roll'] = $roll;
			$cond['class_id'] = $class_id;
			$cond['shift_id'] = $shift_id;
			$cond['section_id'] = $section_id;
            $cond['group'] = $group;
            $cond['religion'] = $religion;
            $cond['gender'] = $gender;
            $cond['blood_group_id'] = $blood_group_id;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('students/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Student->get_all_student_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['student'] = $this->Student->get_all_student_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
        $data['group_list'] = $this->Admin_login->getGroupList();
        $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
		$data['religion_list'] = $this->db->query("SELECT * FROM `tbl_religion`")->result_array();
        $data['maincontent'] = $this->load->view('students/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function bulk_add(){
    	$data = array();
    	if($_POST){
			$number_of_rows = $this->input->post('number_of_rows');
    		if($number_of_rows > 50){
				$sdata['exception'] = "Please input upto 50 rows";
				$this->session->set_userdata($sdata);
				redirect("students/bulk_add");
			}
			$class_shift_section_id = $this->input->post('class_shift_section_id');
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['class_id'] = $class_shift_section_arr[0];
			$data['shift_id'] = $class_shift_section_arr[1];
			$data['section_id'] = $class_shift_section_arr[2];
			$data['group_id'] = $this->input->post('group_id');
			$data['category_id'] = $this->input->post('category_id');
			$data['year']  = $this->input->post('year');
			$data['number_of_rows']  = $number_of_rows;
			$data['religions'] = $this->Admin_login->getReligionList();
			$data['bulk_add_list'] = $this->load->view('students/bulk_add_list', $data, true);
		}
		$data['title'] = "Student Bulk Add";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['class_section_shift_marge_list'] = $this->Admin_login->all_class_section_shift_marge_list();
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['categories'] = $this->Student->getStudentCategory();
		$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$data['maincontent'] = $this->load->view('students/bulk_add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function bulk_add_data_save(){
    	if($_POST){
    		$number_of_rows = $this->input->post('number_of_rows');
			$class_id = $this->input->post('class_id');
			$shift_id =  $this->input->post('shift_id');
			$section_id = $this->input->post('section_id');
			$group_id = $this->input->post('group_id');
			$category_id = $this->input->post('category_id');
			$year  = $this->input->post('year');
    		$i = 1;
    		while ($i <= $number_of_rows){
				$roll = $this->input->post('roll_' . $i);
				if($roll != ''){
					$data = array();
					$data['name_bangla'] = $this->input->post('name_bangla_' . $i, true);
					$data['name'] = $this->input->post('name_english_' . $i, true);
					$data['class_id'] = $class_id;
					$data['roll_no'] = $roll;
					$data['year'] = $year;
					$data['section_id'] = $section_id;
					$data['group'] = $group_id;
					$data['category_id'] = $category_id;
					$data['shift_id'] = $shift_id;
					$data['gender'] = $this->input->post('gender_' . $i, true);
					$data['religion'] = $this->input->post('religion_' . $i, true);
					$data['father_name'] = $this->input->post('fathers_name_' . $i, true);
					$data['mother_name'] = $this->input->post('mothers_name_' . $i, true);
					$data['guardian_mobile'] = $this->input->post('mobile_' . $i, true);
					$data['date_of_birth'] = NULL;
					$data['date_of_admission'] = NULL;
					$data['syn_date'] = NULL;
					$data['password'] = md5('123456');
					$data['student_code'] = $this->Student->getStudentCode($class_id, $group_id, $year, $year);
					//QR Code
					$new_photo_and_qr_code_name = "s_" . time() . $i . "_" . date('Y-m-d');
					$qr_code_name = $new_photo_and_qr_code_name . '.png';
					$qr_code_data = "Name: " . $this->input->post('name_english_' . $i, true) . ', Student ID: ' . $data['student_code'] . ', Date of Birth: Not Found';
					if ($this->Admin_login->qr_code_generate($qr_code_data, $qr_code_name) != true) {
						$sdata['exception'] = $this->lang->line('add_error_message') . ", Error: QR Code generate failed ! ";
						$this->session->set_userdata($sdata);
						redirect("students/bulk_add");
					}
					$data['qr_code'] = $qr_code_name;
					//QR Code End

					$this->db->insert('tbl_student', $data);
				}
    			$i++;
			}
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect("students/bulk_add");
		}
	}

    public function updateMsgStatusStudentStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_student', $data);
        if ($status == 0) {
        	echo '<button onclick="msgStatusUpdate(' . $id . ',1)" style="padding: 2px 10px 2px 10px !important;" class="btn btn-outline-secondary btn-sm"><i class="ti-check-box"></i></button>';
        } else {
        	echo '<button onclick="msgStatusUpdate(' . $id . ',0)" style="padding: 2px 10px 2px 10px !important;" class="btn btn-outline-danger btn-sm"><i class="ti-na"></i></button>';
        }
    }

    public function getStudentByClassShiftSection()
    {
        $class_shift_section_id = $_GET['class_shift_section_id'];
        $group_where = "";
        if(isset($_GET['group_id'])){
          $group_id = $_GET['group_id'];
          if($group_id != ''){
              $group_where = " AND `group` = '$group_id' ";
          }
        }
        $class_shift_section_id = $_GET['class_shift_section_id'];

        $data = array();
        $class_shift_section_arr = explode("-", $class_shift_section_id);
        $class_id = $class_shift_section_arr[0];
        $shift_id = $class_shift_section_arr[1];
        $section_id = $class_shift_section_arr[2];

        // echo '<pre>';
        // print_r($class_shift_section_arr);
        // die;

        $data['students'] = $this->db->query("SELECT `id`,`name`,`student_code`,`roll_no` FROM `tbl_student`
WHERE `status` = 1 AND `class_id` = '$class_id' AND `section_id` = '$section_id'
 AND `shift_id` = '$shift_id' $group_where
ORDER BY ABS(`roll_no`)")->result_array();
        $this->load->view('students/ajax_student_by_class_shift_section', $data);
    }

    public function getStudentInformationById()
    {
       $student_id = $_GET['student_id'];
       $student_info = $this->Student->get_student_info_by_student_id($student_id);
       echo json_encode($student_info);
    }


    public function getStudentByClassShiftSectionYear()
    {
        $class_shift_section_id = $_GET['class_shift_section_id'];
        $year = $_GET['year'];

        $data = array();
        $class_shift_section_arr = explode("-", $class_shift_section_id);
        $class_id = $class_shift_section_arr[0];
        $shift_id = $class_shift_section_arr[1];
        $section_id = $class_shift_section_arr[2];

        $stuldent_list = $this->Admin_login->getStudentListByYear($year, $class_id, $shift_id, $section_id,'');

        if ($stuldent_list['status'] != 'success') {
            return $data['status'];
        }
        $data['students'] = $stuldent_list['students'];
        $this->load->view('students/ajax_student_by_class_shift_section_year', $data);
    }

    public function add()
    {
        if ($_POST) {
            $photo_name = "default.png";
            $file = $_FILES["photo"]['name'];
            $data = array();
            $new_photo_and_qr_code_name = "s_" . time() . "_" . date('Y-m-d');
            if ($file != '') {
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/student/';
                $config['allowed_types'] = 'jpg|png|JPEG|jpeg|JPG';
                $config['max_size'] = '6000';
                $config['max_width'] = '4000';
                $config['max_height'] = '4000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['photo']['name'], '.'), 1));
                $IsAddedSuccesfully = 0;
                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = $new_photo_and_qr_code_name . "." . $extension;
                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], MEDIA_FOLDER . "/student/" . $new_photo_name)) {
                            $photo_name = $new_photo_name;
                        } else {
                            $sdata['exception'] = $this->lang->line('add_error_message') . ", Error: Sorry Student Doesn't Added !" . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("students/add");
                        }
                    } else {
                        $sdata['exception'] = $this->lang->line('add_error_message') . ", Error: Sorry Photo Does't Upload !";
                        $this->session->set_userdata($sdata);
                        redirect("students/add");
                    }
                }
            }

			$class_shift_section_id = $this->input->post('class_shift_section_id');
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

            $data['name'] = $this->input->post('name', true);
            $data['process_code'] = $this->input->post('process_code', true);
            $data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['shift_id'] = $shift_id;
            $data['roll_no'] = $this->input->post('roll_no', true);
            $data['year'] = $this->input->post('year', true);
            $data['group'] = $this->input->post('group', true);
			$data['category_id'] = $this->input->post('category_id', true);

            $student_id_year = $this->input->post('student_id_year', true);

            $data['student_code'] = $this->Student->getStudentCode($data['class_id'], $data['group'], $data['year'], $student_id_year);
            $data['reg_no'] = $this->input->post('reg_no', true);


            $dob_check =  $this->input->post('date_of_birth', true);
            if($dob_check == ''){
				$data['date_of_birth'] = NULL;
			}else{
				$data['date_of_birth'] = $dob_check;
			}

            $doa_check = $this->input->post('date_of_admission', true);
			if($doa_check == ''){
				$data['date_of_admission'] = NULL;
			}else{
				$data['date_of_admission'] = $doa_check;
			}

            $data['gender'] = $this->input->post('gender', true);
            $data['religion'] = $this->input->post('txtReligion', true);
            $data['blood_group_id'] = $this->input->post('blood_group_id', true);
            $data['father_name'] = $this->input->post('father_name', true);
            $data['father_nid'] = $this->input->post('father_nid', true);
            $data['mother_name'] = $this->input->post('mother_name', true);
            $data['mother_nid'] = $this->input->post('mother_nid', true);
            $data['guardian_mobile'] = $this->input->post('guardian_mobile', true);
            $data['second_guardian_mobile'] = $this->input->post('second_guardian_mobile', true);
            $data['student_mobile'] = $this->input->post('student_mobile', true);
            $data['present_address'] = $this->input->post('present_address', true);
            $data['permanent_address'] = $this->input->post('permanent_address', true);
            $data['email'] = $this->input->post('email', true);
            $data['birth_certificate_no'] = $this->input->post('birth_certificate_no', true);
			$data['admitted_class_id'] = $this->input->post('admitted_class_id', true);
            $data['photo'] = $photo_name;
            $data['allowed_for_timekeeping'] = $this->input->post('allowed_for_timekeeping', true);
			$data['syn_date'] = date("Y-m-d");
            $data['data_entry_date'] = date("Y-m-d");
			$data['password'] = md5('123456');

				//QR Code
            $qr_code_name = $new_photo_and_qr_code_name . '.png';
            $qr_code_data = "Name: " . $this->input->post('name', true) . ', Student ID: ' . $data['student_code'] . ', Date of Birth: ' . $data['date_of_birth'];
            if ($this->Admin_login->qr_code_generate($qr_code_data, $qr_code_name) != true) {
                $sdata['exception'] = $this->lang->line('add_error_message') . ", Error: QR Code generate failed ! ";
                $this->session->set_userdata($sdata);
                redirect("students/index");
            }
            $data['qr_code'] = $qr_code_name;
            //QR Code End

            $this->db->insert('tbl_student', $data);
            $IsAddedSuccesfully = 1;

            //sms send
            if ($this->Admin_login->validate_mobile_number($this->input->post('guardian_mobile', true))) {
                $msg = $this->db->query("SELECT template_body FROM `tbl_message_template` WHERE `is_admission_welcome_sms` = '1'")->result_array();
                if (!empty($msg)) {
                    $single_num = "";
                    $single_num = $this->Admin_login->generateNumberlist($single_num, $this->input->post('guardian_mobile', true));

                    $sms_info = array();
                    $sms_info['message'] = $msg[0]['template_body'];
                    $sms_info['numbers'] = $single_num;
                    $sms_info['is_bangla_sms'] = 0;
                    $status = $this->Message->sms_send($sms_info);

                    if ($status == true) {
                        $IsAddedSuccesfully = 1;
                    }
                }
            }
            //sms send
            $IsAddedSuccesfully = 1;


            if ($IsAddedSuccesfully > 0 && isset($_POST['subject_id']) && count($_POST['subject_id']) > 0) {
                $student_id = $this->db->query("SELECT
  IF (MAX(id) IS NULL, 0, MAX(id)) AS student_id
FROM
  `tbl_student` ")->result_array();
                if ($student_id[0]['student_id'] == 0) {
                    $student_id = 1;
                } else {
                    $student_id = $student_id[0]['student_id'];
                }
                $subject_id = $_POST['subject_id'];
                $optional_subject_id = 0;
                if (isset($_POST['is_optional'])) {
                    $optional_subject_id = $_POST['is_optional'];
                }
                $loop = count($subject_id);
                $i = 0;
                while ($i < $loop) {
                    $data = array();
                    $data['student_id'] = $student_id;
                    $data['subject_id'] = $subject_id[$i];
                    $data['class_id'] = $this->input->post('class_id', true);
                    $data['section_id'] = $this->input->post('section_id', true);
                    $data['group'] = $this->input->post('group', true);
                    $data['shift_id'] = $this->input->post('shift_id', true);
                    if ($optional_subject_id == $subject_id[$i]) {
                        $data['is_optional'] = '1';
                    } else {
                        $data['is_optional'] = '0';
                    }
                    $data['year'] = $this->input->post('year', true);
                    $data['data_entry_date'] = date('Y-m-d');
                    $this->db->insert('tbl_student_wise_subject', $data);
                    $i++;
                }
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("students/index");
            } else {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("students/index");
            }
        } else {
            $data = array();
            $data['action'] = 'add';
            $data['is_show_button'] = "index";
            $data['title'] =  $this->lang->line('student_add');
            $data['heading_msg'] =  $this->lang->line('student_add');
			$data['religions'] = $this->Admin_login->getReligionList();
			$data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
			$data['class_section_shift_marge_list'] = $this->Admin_login->all_class_section_shift_marge_list();
            $data['group_list'] = $this->Admin_login->getGroupList();
			$data['categories'] = $this->Student->getStudentCategory();
            $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    // function ggggg(){
    //     $student = $this->db->query("SELECT * FROM `tbl_student` where
    //     class_id = '4' AND `group` = '7' AND section_id = '3' order by id asc")->result_array();
    //    // echo '<pre>';
    //    // print_r($student);
    //     //die;
    //
    //     $i = 0;
    //     foreach ($student as $row):
    //     $i++;
    //     $db_id = $row['id'];
    //
    //
    //     $id = "15.16.67." . str_pad($i, 2, '0', STR_PAD_LEFT);
    //     $this->db->query("update tbl_student set student_code = '$id' where id = '$db_id'");
    //     endforeach;
    //
    // }

    public function edit($id = null)
    {
        if ($_POST) {
//        	echo '<pre>';
//        	print_r($_POST);
//        	die;
			$class_shift_section_id = $this->input->post('class_shift_section_id');
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$data['class_shift_section_id'] = $class_shift_section_id;
			$class_id = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];
            $file = $_FILES["photo"]['name'];
            if ($file != '') {
                $student_photo_info = $this->db->where('id', $id)->get('tbl_student')->result_array();
                $old_photo = $student_photo_info[0]['photo'];
                if (!empty($old_photo)) {
                    $filedel = PUBPATH . MEDIA_FOLDER . '/student/' . $old_photo;
                    unlink($filedel);
                }
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/student/';
                $config['allowed_types'] = 'jpg|png|JPEG|jpeg|JPG';
                $config['max_size'] = '6000';
                $config['max_width'] = '4000';
                $config['max_height'] = '4000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['photo']['name'], '.'), 1));
                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = "s_" . time() . "_" . date('Y-m-d') . "." . $extension;
                        if (move_uploaded_file($_FILES["photo"]["tmp_name"], MEDIA_FOLDER . "/student/" . $new_photo_name)) {
                            $data = array();
                            $data['id'] = $this->input->post('id', true);
                            $data['name'] = $this->input->post('name', true);
                            $data['student_code'] = $this->input->post('student_code', true);
                            $data['process_code'] = $this->input->post('process_code', true);
							$data['class_id'] = $class_id;
							$data['shift_id'] = $shift_id;
							$data['section_id'] = $section_id;
                            $data['group'] = $this->input->post('group', true);
							$data['category_id'] = $this->input->post('category_id', true);
                            $data['roll_no'] = $this->input->post('roll_no', true);
                            $data['reg_no'] = $this->input->post('reg_no', true);

							$dob_check =  $this->input->post('date_of_birth', true);
							if($dob_check == ''){
								$data['date_of_birth'] = NULL;
							}else{
								$data['date_of_birth'] = $dob_check;
							}

							$doa_check = $this->input->post('date_of_admission', true);
							if($doa_check == ''){
								$data['date_of_admission'] = NULL;
							}else{
								$data['date_of_admission'] = $doa_check;
							}

                            $data['gender'] = $this->input->post('gender', true);
                            $data['religion'] = $this->input->post('txtReligion', true);
                            $data['blood_group_id'] = $this->input->post('blood_group_id', true);
                            $data['father_name'] = $this->input->post('father_name', true);
                            $data['father_nid'] = $this->input->post('father_nid', true);
                            $data['mother_name'] = $this->input->post('mother_name', true);
                            $data['mother_nid'] = $this->input->post('mother_nid', true);
                            $data['guardian_mobile'] = $this->input->post('guardian_mobile', true);
                            $data['second_guardian_mobile'] = $this->input->post('second_guardian_mobile', true);
                            $data['student_mobile'] = $this->input->post('student_mobile', true);
                            $data['present_address'] = $this->input->post('present_address', true);
                            $data['permanent_address'] = $this->input->post('permanent_address', true);
                            $data['email'] = $this->input->post('email', true);
                            $data['birth_certificate_no'] = $this->input->post('birth_certificate_no', true);
							$data['admitted_class_id'] = $this->input->post('admitted_class_id', true);
                            $data['year'] = $this->input->post('year', true);
                            $data['allowed_for_timekeeping'] = $this->input->post('allowed_for_timekeeping', true);
                            if ($this->input->post('allowed_for_timekeeping', true) == 1) {
                                $data['syn_date'] = $this->input->post('syn_date', true);
                            }
                            $data['photo'] = $new_photo_name;
                            $this->db->where('id', $data['id']);
                            $this->db->update('tbl_student', $data);
                            $sdata['message'] = $this->lang->line('edit_success_message');
                            $this->session->set_userdata($sdata);
                            redirect("students/index");
                        } else {
                            $sdata['exception'] = $this->lang->line('edit_error_message') . ", Error: " . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("students/index");
                        }
                    } else {
                        $sdata['exception'] = $this->lang->line('edit_error_message');
                        $this->session->set_userdata($sdata);
                        redirect("students/index");
                    }
                }
            } else {
                $data = array();
                $data['id'] = $this->input->post('id', true);
                $data['name'] = $this->input->post('name', true);
                $data['student_code'] = $this->input->post('student_code', true);
                $data['process_code'] = $this->input->post('process_code', true);
				$data['class_id'] = $class_id;
				$data['shift_id'] = $shift_id;
                $data['section_id'] = $section_id;
                $data['group'] = $this->input->post('group', true);
				$data['category_id'] = $this->input->post('category_id', true);
                $data['roll_no'] = $this->input->post('roll_no', true);
                $data['reg_no'] = $this->input->post('reg_no', true);

				$dob_check =  $this->input->post('date_of_birth', true);
				if($dob_check == ''){
					$data['date_of_birth'] = NULL;
				}else{
					$data['date_of_birth'] = $dob_check;
				}

				$doa_check = $this->input->post('date_of_admission', true);
				if($doa_check == ''){
					$data['date_of_admission'] = NULL;
				}else{
					$data['date_of_admission'] = $doa_check;
				}

                $data['gender'] = $this->input->post('gender', true);
                $data['religion'] = $this->input->post('txtReligion', true);
                $data['blood_group_id'] = $this->input->post('blood_group_id', true);
                $data['father_name'] = $this->input->post('father_name', true);
                $data['father_nid'] = $this->input->post('father_nid', true);
                $data['mother_name'] = $this->input->post('mother_name', true);
                $data['mother_nid'] = $this->input->post('mother_nid', true);
                $data['guardian_mobile'] = $this->input->post('guardian_mobile', true);
                $data['present_address'] = $this->input->post('present_address', true);
                $data['permanent_address'] = $this->input->post('permanent_address', true);
                $data['email'] = $this->input->post('email', true);
                $data['year'] = $this->input->post('year', true);
                $data['birth_certificate_no'] = $this->input->post('birth_certificate_no', true);
				$data['admitted_class_id'] = $this->input->post('admitted_class_id', true);
                $data['allowed_for_timekeeping'] = $this->input->post('allowed_for_timekeeping', true);

                $this->db->where('id', $data['id']);
                $this->db->update('tbl_student', $data);
                $sdata['message'] =  $this->lang->line('edit_success_message');
                $this->session->set_userdata($sdata);
                redirect("students/index");
            }
        } else {
            $data = array();
            $data['title'] = $this->lang->line('student_update');
            $data['action'] = 'edit';
            $data['is_show_button'] = "index";
            $data['heading_msg'] =  $this->lang->line('student_update');
			$data['religions'] = $this->Admin_login->getReligionList();
			$data['categories'] = $this->Student->getStudentCategory();
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
			$data['class_section_shift_marge_list'] = $this->Admin_login->all_class_section_shift_marge_list();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
            $data['student_info_by_id'] = $this->db->query(" SELECT * FROM `tbl_student` WHERE `id` = '$id' ")->result_array();
            $year = $data['student_info_by_id'][0]['year'];
            $data['subject_list_by_student'] = $this->db->query("SELECT
  su.*,
  s.`is_optional`
FROM
  `tbl_student_wise_subject` AS s
  LEFT JOIN `tbl_subject` AS su
    ON su.`id` = s.`subject_id`
WHERE s.`student_id` = '$id' AND s.`year` = '$year'")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    //////////////////////////////end///////////////////

    public function password_reset($student_id=null)
    {
        $is_authenticated = $this->custom_methods_model->num_of_data("tbl_student", "WHERE id='$student_id'");
        if ($is_authenticated > 0) {
            $student_info = $this->custom_methods_model->select_row_by_condition("tbl_student", "id='$student_id'");

            if ($this->Admin_login->validate_mobile_number($student_info[0]->guardian_mobile)) {
                $new_pass = $this->Admin_login->random_password(6);
                // echo '<pre>';
                // print_r($student_info);
                // die;
                $data = array();
                $data['password'] =   md5($new_pass);
                $data['id'] = $student_info[0]->id;
                $this->db->where('id', $data['id']);
                $this->db->update('tbl_student', $data);

                $single_num = "";
                $single_num = $this->Admin_login->generateNumberlist($single_num, $student_info[0]->guardian_mobile);

                $name = $student_info[0]->name;
                $sms_info = array();
                $sms_info['message'] = "Dear " . $name . ", Your New Password is : '" . $new_pass . "'. Thank You, School360 Team";
                $sms_info['numbers'] = $single_num;
                $sms_info['is_bangla_sms'] = 0;
                $status = $this->Message->sms_send($sms_info);
                if ($status) {
                    $sdata['message'] = "Your (".$name.") password has been send by a SMS and New Password: <b>" . $new_pass . "</b>";
                    $this->session->set_userdata($sdata);
                    redirect("students/index");
                } else {
                    $sdata['exception'] = "Some error occurred.";
                    $this->session->set_userdata($sdata);
                    redirect("students/index");
                }
            } else {
                $sdata['exception'] = "Student Mobile Number Not Valid!";
                $this->session->set_userdata($sdata);
                redirect("students/index");
            }
        } else {
            $sdata['exception'] = "Student Id did not match!";
            $this->session->set_userdata($sdata);
            redirect("students/index");
        }
    }


    public function student_id_view($id)
    {
        $data = array();
        $data['title'] = 'Student ID Card';
        $data['heading_msg'] = "Student ID Card";
        $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
        $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
        $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
        $data['student_info_by_id'] = $this->Student->get_student_info_by_student_id($id);
        $data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $this->load->view('students/student_id_card', $data);
    }


    public function getStudentForDropdown()
    {
        $class_id = $this->input->get('class_id', true);
        $section_id = $this->input->get('section_id', true);
        $gorup_id = $this->input->get('group_id', true);
        $shift_id = $this->input->get('shift_id', true);

        $where = "";
        if ($class_id != '') {
            $where .= " AND s.class_id = '$class_id' ";
        }

        if ($section_id != '') {
            $where .= " AND s.section_id = '$section_id' ";
        }

        if ($gorup_id != '') {
            $where .= " AND s.group = '$gorup_id' ";
        }

        if ($shift_id != '') {
            $where .= " AND s.shift_id = '$shift_id' ";
        }

        if ($this->input->get('year', true) != '') {
            $year = $this->input->get('year', true);
        } else {
            $year = date('Y');
        }
        //echo $year.'/'.$where; die;
        $data = array();
        if ($year == date('Y')) {
            $data['students'] = $this->db->query("SELECT s.id,s.name,s.roll_no,s.student_code FROM tbl_student AS s WHERE s.status = '1' $where ORDER BY s.roll_no")->result_array();
        } else {
            $data['students'] = $this->db->query("SELECT s.id,s.student_code,mt.name,mt.old_roll_no FROM tbl_student_data_migrate_history AS mt
                                                INNER JOIN tbl_student AS s ON s.id = mt.student_id
                                                 WHERE mt.old_year = '$year' AND mt.old_class_id = '$class_id' AND mt.old_section_id = '$section_id' ORDER BY mt.old_roll_no")->result_array();
        }



        $this->load->view('students/student_list_for_dropdown', $data);
    }



     function createQRCode(){

         $this->Admin_login->qr_code_generate("http://school360.cloud/relief/relief_provides/add/478687998",'FO-478687998.png');
     }

    public function date_format_from_excel_date($date)
    {
        if ($date != "") {
            $split_date = str_replace('/', '-', $date);
            return date('Y-m-d', strtotime($split_date));
        } else {
            return "";
        }
    }

    public function get_blood_group_id_by_name($name)
    {
        $info = $this->db->query("SELECT id FROM tbl_blood_group WHERE name ='$name'")->result_array();
        if (!empty($info)) {
            return $info[0]['id'];
        } else {
            return 9;
        }
    }





    public function student_syllabus_list()
    {
        $data = array();
        $data['title'] = 'Student Syllabus';
        $data['heading_msg'] = "Student Syllabus Upload";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('students/student_syllabus_list/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student->get_all_uploaded_syllabus(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['student_syllabus'] = $this->Student->get_all_uploaded_syllabus(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/uploaded_syllabus_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function uploaded_syllabus_delete($id)
    {
        $uploaded_syllabus_info = $this->Student->get_uploaded_syllabus_info_by_syllabus_id($id);
        $file = $uploaded_syllabus_info[0]['location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/syllabus/' . $file;
            if (unlink($filedel)) {
                $this->Student->delete_uploaded_syllabus_info_by_syllabus_id($id);
            } else {
                $this->Student->delete_uploaded_syllabus_info_by_syllabus_id($id);
            }
        } else {
            $this->Student->delete_uploaded_syllabus_info_by_syllabus_id($id);
        }

        $sdata['message'] = "Student Uploaded Syllabus Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("students/student_syllabus_list");
    }


    public function student_syllabus_upload()
    {
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/syllabus/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
            $config['max_size'] = '60000';
            $config['max_width'] = '6000';
            $config['max_height'] = '6000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "Syllabus_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/syllabus/" . $new_file_name)) {
                        $data = array();
                        $data['class_id'] = $this->input->post('txtClass', true);
                        $data['year'] = $this->input->post('txtYear', true);
                        $data['page_no'] = $this->input->post('txtPageNum', true);
                        $data['location'] = $new_file_name;
                        $this->db->insert('tbl_syllabus', $data);
                        $sdata['message'] = "You are Successfully Syllabus Uploaded !";
                        $this->session->set_userdata($sdata);
                        redirect("students/student_syllabus_upload");
                    } else {
                        $sdata['exception'] = "Sorry Syllabus Doesn't Upload !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("students/student_syllabus_upload");
                    }
                } else {
                    $sdata['exception'] = "Sorry Syllabus Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("students/student_syllabus_upload");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Student Syllabus';
            $data['heading_msg'] = "Student Syllabus Upload";
            $data['action'] = '';
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/syllabus_upload_add_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function student_routine_list()
    {
        $data = array();
        $data['title'] = 'Class Routine';
        $data['heading_msg'] = "Class Routine Upload";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('students/student_routine_list/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Student->get_all_uploaded_routine(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['student_routine'] = $this->Student->get_all_uploaded_routine(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/uploaded_routine_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function student_routine_upload()
    {
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/routine/';
            $config['allowed_types'] = 'pdf|doc|docx|xls';
            $config['max_size'] = '60000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "Routine_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/routine/" . $new_file_name)) {
                        $data = array();
                        $data['class_id'] = $this->input->post('txtClass', true);
                        $data['section_id'] = $this->input->post('txtSection', true);
                        $data['group'] = $this->input->post('txtGroup', true);
                        $data['year'] = $this->input->post('txtYear', true);
                        $data['date'] = $this->input->post('txtDate', true);
                        $data['location'] = $new_file_name;
                        $this->Student->routine_upload($data);
                        $sdata['message'] = "You are Successfully Routine Uploaded ! ";
                        $this->session->set_userdata($sdata);
                        redirect("students/student_routine_upload");
                    } else {
                        $sdata['exception'] = "Sorry Routine Doesn't Upload !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("students/student_routine_upload");
                    }
                } else {
                    $sdata['exception'] = "Sorry Routine Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("students/student_routine_upload");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Student Routine';
            $data['heading_msg'] = "Student Routine Upload";
            $data['action'] = '';
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/routine_upload_add_from', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function uploaded_routine_delete($id)
    {
        $uploaded_routine_info = $this->Student->get_uploaded_routine_info_by_routine_id($id);
        $file = $uploaded_routine_info[0]['location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/routine/' . $file;
            if (unlink($filedel)) {
                $this->Student->delete_uploaded_routine_info_by_routine_id($id);
            } else {
                $this->Student->delete_uploaded_routine_info_by_routine_id($id);
            }
        } else {
            $this->Student->delete_uploaded_routine_info_by_routine_id($id);
        }

        $sdata['message'] = "Student Uploaded Routine Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("students/student_routine_list");
    }

    public function grace_number_index()
    {
        if ($_POST) {
            $data = array();
            $class_id = $_POST['class_id'];
            $exam_id =  $_POST['exam_id'];
            $data['class_id'] = $class_id;
            $data['exam_id'] = $exam_id;
            $data['subject'] = $this->db->query("SELECT cs.*,s.name as subject_name,s.code,g.class_test_grace,g.written_grace,g.objective_grace,g.practical_grace FROM `tbl_class_wise_subject` as cs
            INNER JOIN tbl_subject as s on s.id = cs.subject_id
            LEFT JOIN tbl_grace_number as g on g.class_id = cs.class_id and g.exam_id = '$exam_id' and g.subject_id = cs.subject_id
            WHERE cs.`class_id` = '$class_id' order by cs.order_number")->result_array();
            //echo '<pre>';
            //print_r($data['subject']);
            //die;
            $data['title'] = 'Exam Wise Grace Mark';
            $data['heading_msg'] = "Exam Wise Grace Mark";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/grace_number_process', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Exam Wise Grace Mark';
            $data['heading_msg'] = "Exam Wise Grace Mark";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['ExamList'] = $this->db->query("SELECT * FROM `tbl_exam`")->result_array();
            $data['ClassList'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['maincontent'] = $this->load->view('students/grace_number_index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function grace_number_save()
    {
        if ($_POST) {
            $class_id = $_POST['class_id'];
            $exam_id =  $_POST['exam_id'];
            $this->db->delete('tbl_grace_number', array('class_id' => $class_id, 'exam_id' => $exam_id));
            $loop_time =  $_POST['loop_time'];
            $i = 0;
            while ($i < $loop_time) {
                $data = array();
                $data['exam_id'] = $exam_id;
                $data['class_id'] = $class_id;
                $data['subject_id'] = $this->input->post("subject_id_" . $i);
                $data['class_test_grace'] = $this->input->post("class_test_grace_" . $i);
                $data['written_grace'] = $this->input->post("written_grace_" . $i);
                $data['objective_grace'] = $this->input->post("objective_grace_" . $i);
                $data['practical_grace'] = $this->input->post("practical_grace_" . $i);
                $this->db->insert('tbl_grace_number', $data);
                $i++;
            }
            $sdata['message'] = "Data Added Successfully.";
            $this->session->set_userdata($sdata);
            redirect('students/grace_number_index');
        }
    }

    public function class_wise_extra_mark_head_index()
    {
        $data = array();
        $data['title'] = 'Class Wise Extra Mark';
        $data['heading_msg'] = "Class Wise Extra Mark";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['heads'] = $this->db->query("SELECT ce.*,eh.`name` AS head_name,c.`name` AS class_name,et.`name` AS exam_type_name FROM `tbl_class_wise_extra_head_amount` AS ce
INNER JOIN `tbl_exam_marking_extra_head` AS eh ON eh.`id` = ce.`head_id`
INNER JOIN `tbl_class` AS c ON c.`id` = ce.`class_id`
INNER JOIN `tbl_exam_type` AS et ON et.`id` = ce.`exam_type_id` ORDER BY ce.`class_id`,ce.`head_id` DESC")->result_array();
        //echo '<pre>';
        //print_r($data['heads']); die;
        $data['maincontent'] = $this->load->view('students/class_wise_extra_mark_head_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function class_wise_extra_mark_head_add()
    {
        if ($_POST) {
            $class_id = $_POST['class_id'];
            $exam_type_id =  $_POST['exam_type_id'];
            $this->db->delete('tbl_class_wise_extra_head_amount', array('class_id' => $class_id, 'exam_type_id' => $exam_type_id));
            $loop_time =  $_POST['loop_time'];
            $i = 0;
            while ($i <= $loop_time) {
                if ($this->input->post("is_allow_" . $i)) {
                    $data = array();
                    $data['exam_type_id'] = $exam_type_id;
                    $data['class_id'] = $class_id;
                    $data['head_id'] = $this->input->post("head_id_" . $i);
                    $data['mark'] = $this->input->post("mark_" . $i);
                    $this->db->insert('tbl_class_wise_extra_head_amount', $data);
                }
                $i++;
            }

            $sdata['message'] = "Data Added Successfully.";
            $this->session->set_userdata($sdata);
            redirect('students/class_wise_extra_mark_head_add');
        }
        $data = array();
        $data['title'] = 'Add Class Wise Extra Mark';
        $data['heading_msg'] = "Add Class Wise Extra Mark";
        $data['class_list'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['exam_types'] = $this->db->query("SELECT * FROM tbl_exam_type")->result_array();
        $data['heads'] = $this->db->query("SELECT * FROM tbl_exam_marking_extra_head")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/class_wise_extra_mark_head_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function extra_mark_head_index()
    {
        $data = array();
        $data['title'] = 'Extra Mark Head List';
        $data['heading_msg'] = "Extra Mark Head List";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['heads'] = $this->db->query("SELECT * FROM tbl_exam_marking_extra_head")->result_array();
        $data['maincontent'] = $this->load->view('students/extra_mark_head_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function extra_mark_head_add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $this->db->insert("tbl_exam_marking_extra_head", $data);
            $sdata['message'] = "Data Added Successfully.";
            $this->session->set_userdata($sdata);
            redirect('students/extra_mark_head_index');
        }
        $data = array();
        $data['title'] = 'Add Extra Mark Head';
        $data['heading_msg'] = "Add Extra Mark Head";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/extra_mark_head_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function extra_mark_head_edit($id)
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $this->db->where('id', $id);
            $this->db->update('tbl_exam_marking_extra_head', $data);
            $sdata['message'] = "Data Updated Successfully.";
            $this->session->set_userdata($sdata);
            redirect('students/extra_mark_head_index');
        }
        $data = array();
        $data['title'] = 'Edit Extra Mark Head';
        $data['heading_msg'] = "Edit Extra Mark Head";
        $data['head'] = $this->db->query("SELECT * FROM tbl_exam_marking_extra_head WHERE `id` = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/extra_mark_head_edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function extra_mark_head_delete($id)
    {
        $Check = $this->db->query("SELECT `id` FROM `tbl_class_wise_extra_head_amount` WHERE `head_id` = '$id' LIMIT 1")->result_array();
        if (empty($Check)) {
            $this->db->query("DELETE FROM tbl_exam_marking_extra_head WHERE `id` = '$id'");
            $sdata['message'] = "Data Deleted Successfully.";
            $this->session->set_userdata($sdata);
            redirect('students/extra_mark_head_index');
        } else {
            $sdata['exception'] = "This Head already assigned to a class. You cannot Delete!";
            $this->session->set_userdata($sdata);
            redirect('students/extra_mark_head_index');
        }
    }


    public function getClassWiseSubject()
    {
        $class_id = $this->input->get('class_id', true);
        $data = array();
        $data['sub_info'] = $this->db->query("SELECT * FROM tbl_subject WHERE `id` IN (SELECT subject_id FROM tbl_class_wise_subject WHERE `class_id` = '$class_id')")->result_array();
        $this->load->view('students/subject_list', $data);
    }



    public function student_code_config()
    {
        $config_info = $this->db->query("SELECT * FROM `tbl_student_code_config`")->result_array();
        if (empty($config_info)) {
            $sdata['exception'] = "Initial data not found to database for student ID configuration";
            $this->session->set_userdata($sdata);
            redirect('students/index');
        }
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['is_auto_code_enable'] = $_POST['is_auto_code_enable'];
            $data['segment_1'] = $_POST['segment_1'];
            $data['segment_2'] = $_POST['segment_2'];
            $data['segment_3'] = $_POST['segment_3'];
            $data['segment_4'] = $_POST['segment_4'];
            $data['auto_inc_code_length'] = $_POST['auto_inc_code_length'];
            $data['auto_increament_option'] = $_POST['auto_increament_option'];
            $data['code_separator'] = $_POST['code_separator'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_student_code_config', $data);
            $sdata['message'] = "Data has been updated successfully";
            $this->session->set_userdata($sdata);
            redirect('students/student_code_config');
        }
        $data = array();
        $data['config_info'] = $config_info;
        $data['title'] = 'Student ID Configuration';
        $data['heading_msg'] = "Student ID Configuration";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/student_code_config', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function student_id_card_pdf()
    {
        $class_id = $this->input->post('txtClass', true);
        $section_id = $this->input->post('txtSection', true);
        $group = $this->input->post('txtGroup', true);
        $data = array();

        $data['title'] = 'Student ID Card';
        $data['heading_msg'] = "Student ID Card";
        $data['list'] = $this->db->query("SELECT b.name as blood_group_name,s.date_of_admission,s.`id`,s.`name`,s.`roll_no`,s.`student_code`,c.`name` AS class_name,sc.`name` AS section_name, g.`name` AS group_name,
s.`photo`
FROM `tbl_student` AS s
LEFT JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
LEFT JOIN `tbl_section` AS sc ON s.`section_id` = sc.`id`
LEFT JOIN `tbl_student_group` AS g ON s.`group` = g.`id`
LEFT JOIN `tbl_blood_group` AS b ON s.`blood_group_id` = b.`id`
WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` =  '$group' AND s.`status` = '1';")->result_array();
        $data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();
        $is_pdf_request = $this->input->post('is_pdf_request', true);
        $data['is_pdf_request'] = "N";

        //echo 'gggg'; die;
        $data['is_pdf_request'] = $is_pdf_request;
        // Load all views as normal
        $html = $this->load->view('students/student_id_card_double_print', $data, true);
        $pdfFilePath = "Student_ID_Card.pdf";
        $this->load->library('m_pdf');
        //generate the PDF from the given html
        $this->m_pdf->pdf->autoScriptToLang = true;
        $this->m_pdf->pdf->WriteHTML($html);
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");
    }







    public function print_seat_plan()
    {
        if ($_POST) {
            $class_id = $this->input->post('txtClass', true);
            $section_id = $this->input->post('txtSection', true);
            $group = $this->input->post('txtGroup', true);
            $exam = $this->input->post('txtExam', true);

            $data = array();
            $data['exam'] = $exam;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;


            $confing_info = $this->db->query("SELECT * FROM tbl_config")->result_array();
            $admit_card_text_color = "000000";
            $admit_card_background_color = "000000";
            if (!empty($confing_info)) {
                $admit_card_text_color = $confing_info[0]['admit_card_text_color'];
                $admit_card_background_color = $confing_info[0]['admit_card_background_color'];
            }

            $data['admit_card_text_color'] = $admit_card_text_color;
            $data['admit_card_background_color'] = $admit_card_background_color;


            $data['title'] = 'Exam Seat Plan Card';
            $data['heading_msg'] = "Exam Seat Plan Card";
            $data['list'] = $this->db->query("SELECT s.`id`,s.`name`,s.`roll_no`,s.`student_code`,s.father_name,s.mother_name,c.`name` AS class_name,sc.`name` AS section_name, g.`name` AS group_name,
s.`photo`
FROM `tbl_student` AS s
LEFT JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
LEFT JOIN `tbl_section` AS sc ON s.`section_id` = sc.`id`
LEFT JOIN `tbl_student_group` AS g ON s.`group` = g.`id`
WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` =  '$group' AND s.`status` = '1';")->result_array();
            $data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();

            $is_pdf_request = $this->input->post('is_pdf_request', true);

            if (isset($is_pdf_request) && $is_pdf_request == 'Download') {
                //echo 'gggg'; die;
                $data['is_pdf_request'] = $is_pdf_request;
                // Load all views as normal
                $html = $this->load->view('students/student_seat_plan_print', $data, true);
                $pdfFilePath = "Exam_Seat_Plan.pdf";
                $this->load->library('m_pdf');
                //generate the PDF from the given html
                $this->m_pdf->pdf->autoScriptToLang = true;
                $this->m_pdf->pdf->WriteHTML($html);
                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "D");
            } else {
                $data['is_pdf_request'] = $is_pdf_request;
                $this->load->view('students/student_seat_plan_print', $data);
            }
        } else {
            $data = array();
            $data['title'] = 'Exam Seat Plan Card';
            $data['heading_msg'] = "Exam Seat Plan Card";
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['exam'] = $this->db->query("SELECT * FROM tbl_exam")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/student_seat_plan_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function student_photo_upload()
    {
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;
            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section = $class_shift_section_arr[2];
            $group = $this->input->post('group', true);
            $data = array();
            $data['title'] = 'Student Photo Upload';
            $data['heading_msg'] = "Student Photo Upload";
            $data['students'] = $this->db->query("SELECT * FROM tbl_student
               WHERE class_id = '$class' AND section_id = '$section' AND `group` = '$group'
                AND `shift_id` = '$shift_id' AND status = '1' ORDER BY ABS(roll_no)")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/student_photo_upload_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Student Photo Upload';
            $data['heading_msg'] = "Student Photo Upload";
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/student_photo_upload', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function student_photo_upload_data_save()
    {
        if ($_POST) {
            //echo phpinfo();
            // echo '<pre>';
            // print_r($_FILES);
            // die;
            $config['upload_path'] = MEDIA_FOLDER . '/student/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg|JPG';
            $config['max_size'] = '204800';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $config['encrypt_name'] = true;
            $this->load->library('upload', $config);

            $loop_time = $this->input->post("loop_time");
            $i = 1;
            while ($i <= $loop_time) {
                $id = $this->input->post("id_" . $i);
                $file = $_FILES["photo_" . $i]['name'];
                if ($file != '') {
                    $student_photo_info = $this->db->where('id', $id)->get('tbl_student')->result_array();
                    //$old_photo = $student_photo_info[0]['photo'];
                    $old_photo = "";
                    if (!empty($old_photo)) {
                        $filedel = PUBPATH . MEDIA_FOLDER . '/student/' . $old_photo;
                        unlink($filedel);
                    }

                    if ($this->upload->do_upload('photo_' . $i)) {
                        $upload_info = $this->upload->data();
                        //echo '<pre>';
                        // print_r($gbr);
                        //die;
                        $data = array(
                            'id' => $id,
                            'photo' => $upload_info['file_name']
                        );
                        $this->db->where('id', $id);
                        $this->db->update('tbl_student', $data);
                    }
                }
                $i++;
            }
            $sdata['message'] = "Student Photo Successfully Uploaded.";
            $this->session->set_userdata($sdata);
            redirect("students/student_photo_upload");
        }
    }



    //Data Migration
    public function student_data_migration()
    {
        if ($_POST) {
            $from_class = $this->input->post('from_class', true);
            $to_class = $this->input->post('to_class', true);
            $from_year = $this->input->post('from_year', true);
            $to_year = $this->input->post('to_year', true);

            //if($from_class == $to_class){
            //$sdata['exception'] = "Please select two different class for data migrate.";
            // $this->session->set_userdata($sdata);
            //redirect("students/student_data_migration");
            //}

            // if($from_year == $to_year){
            //  $sdata['exception'] = "Please select two different year for data migrate.";
            // $this->session->set_userdata($sdata);
            // redirect("students/student_data_migration");
            //  }

            $data = array();
            $data['from_class'] = $from_class;
            $data['to_class'] = $to_class;
            $data['from_year'] = $from_year;
            $data['to_year'] = $to_year;
            $data['title'] = 'Student Data Migration';
            $data['heading_msg'] = "Student Data Migration";
            $data['section'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['class'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['shifts'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();

            $final_exam_id = "";
            $final_exam = $this->db->query("SELECT * FROM `tbl_exam` WHERE is_annual_exam = 1 AND year = '$from_year'")->result_array();
            if (!empty($final_exam)) {
                $final_exam_id = $final_exam[0]['id'];
            }

            $data['group_list'] = $this->Admin_login->getGroupList();

            $data['students'] = $this->db->query("SELECT s.*,r.annual_position FROM tbl_student as s
                                LEFT JOIN tbl_result_process as r ON s.id = r.student_id AND r.exam_id = '$final_exam_id'
                                WHERE s.class_id = '$from_class' AND s.status = '1' AND s.year = '$from_year' ORDER BY s.roll_no")->result_array();

            // echo '<pre>';
            // print_r($data['students']); die;

            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/student_data_migration_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Student Data Migration';
            $data['heading_msg'] = "Student Data Migration";
            $data['class'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('students/student_data_migration', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function student_data_migration_data_save()
    {
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            //  die;
            $loop_time = $this->input->post("loop_time");
            $i = 1;
            while ($i <= $loop_time) {
                if ($this->input->post("is_allow_" . $i)) {
                    $data = array();
                    $data['student_id'] = $this->input->post("id_" . $i);
                    $data['name'] = $this->input->post("name_" . $i);
                    $data['old_roll_no'] = $this->input->post("current_roll_no_" . $i);
                    $data['new_roll_no'] = $this->input->post("new_roll_no_" . $i);
                    $data['old_section_id'] = $this->input->post("current_section_id_" . $i);
                    $data['new_section_id'] = $this->input->post("new_section_id_" . $i);
                    $data['old_group'] = $this->input->post("current_group_" . $i);
                    $data['new_group'] = $this->input->post("new_group_" . $i);
                    $data['old_class_id'] = $this->input->post("from_class");
                    $data['new_class_id'] = $this->input->post("to_class");
                    $data['old_year'] = $this->input->post("from_year");
                    $data['new_year'] = $this->input->post("to_year");
                    $data['old_shift'] = $this->input->post("current_shift_" . $i);
                    $data['new_shift'] = $this->input->post("new_shift_" . $i);
                    $data['updated_status'] = $this->input->post("updated_status_" . $i);
                    $this->db->insert('tbl_student_data_migrate_history', $data);

                    $st_data = array();
                    $st_data['id'] = $this->input->post("id_" . $i);
                    $st_data['class_id'] = $this->input->post("to_class");
                    $st_data['roll_no'] = $this->input->post("new_roll_no_" . $i);
                    $st_data['section_id'] = $this->input->post("new_section_id_" . $i);
                    $st_data['group'] = $this->input->post("current_group_" . $i);
                    $st_data['shift_id'] = $this->input->post("new_shift_" . $i);
                    $st_data['status'] = $this->input->post("updated_status_" . $i);
                    $st_data['year'] = $this->input->post("to_year");
                    $this->db->where('id', $st_data['id']);
                    $this->db->update('tbl_student', $st_data);
                }
                $i++;
            }
            $sdata['message'] = "Student Data Successfully Migrate.";
            $this->session->set_userdata($sdata);
            redirect("students/student_data_migration");
        }
    }
    //Data Migration



    public function student_info_view($id)
    {
        $data = array();
        $data['title'] = 'Student Profile';
        $data['action'] = 'view';
        $data['heading_msg'] = "Student Profile";
        $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
        $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
        $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
        $data['group_list'] = $this->Admin_login->getGroupList();
        $data['student_info_by_id'] = $this->Student->get_student_info_by_student_id($id);
        $year = $data['student_info_by_id'][0]['year'];
//        echo '<pre>';
//        print_r($data['student_info_by_id']);
//        die;

		$data['result_info_for_current_year'] = $this->db->query("SELECT e.`name`,r.`gpa_with_optional`,r.`c_alpha_gpa_with_optional`,
r.`total_obtain_mark`,r.`total_credit`,r.`class_position` FROM `tbl_result_process` AS r
INNER JOIN `tbl_exam` AS e ON e.`id` = r.`exam_id`
WHERE r.`student_id` = $id AND r.`year` = $year")->result_array();

        $data['subject_list_by_student'] = $this->db->query("SELECT
  su.*,
  s.`is_optional`
FROM
  `tbl_student_wise_subject` AS s
  LEFT JOIN `tbl_subject` AS su
    ON su.`id` = s.`subject_id`
WHERE s.`student_id` = '$id' AND s.`year` = '$year'")->result_array();
		$fees_para_data = array();
		$fees_para_data['class_id'] = $data['student_info_by_id'][0]['class_id'];
		$fees_para_data['group_id'] =  $data['student_info_by_id'][0]['group'];
		$fees_para_data['section_id'] =  $data['student_info_by_id'][0]['section_id'];
		$fees_para_data['student_id'] =  $data['student_info_by_id'][0]['id'];
		$fees_para_data['year'] =  $data['student_info_by_id'][0]['year'];
		$data['sub_category'] = $this->db->query("SELECT * FROM tbl_fee_sub_category ORDER BY id")->result_array();
		$data['rdata'] = $this->Student_fee->getStudentFeeStatusDetailsStandard($fees_para_data);
		$data['fess_data_report'] = $this->load->view('student_fee_status_details/report', $data, true);
		$data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('students/student_info_view', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function student_info_delete($id)
    {
        $student_marking_info = $this->db->where('student_id', $id)->get('tbl_marking')->result_array();
        if (!empty($student_marking_info)) {
            $sdata['exception'] = "Student Marking Depended Data Found !";
            $this->session->set_userdata($sdata);
            redirect("students/index");
        }
        $student_login_info = $this->db->where(array('person_id' => $id, 'person_type' => 'S'))->get('tbl_logins')->result_array();
        if (!empty($student_login_info)) {
            $sdata['exception'] = "Student Timekeeping Depended Data Found !";
            $this->session->set_userdata($sdata);
            redirect("students/index");
        }

        $student_fees_info = $this->db->where('student_id', $id)->get('tbl_fee_collection')->result_array();
        if (!empty($student_fees_info)) {
            $sdata['exception'] = "Student Fees Depended Data Found !";
            $this->session->set_userdata($sdata);
            redirect("students/index");
        }

        $student_photo_info = $this->db->where('id', $id)->get('tbl_student')->result_array();
//        $file = $student_photo_info[0]['photo'];
//        if (!empty($file)) {
//            $filedel = PUBPATH . MEDIA_FOLDER . '/student/' . $file;
//            if (unlink($filedel)) {
//                $this->db->delete('tbl_student', array('id' => $id));
//                $this->db->delete('tbl_student_wise_subject', array('student_id' => $id));
//            } else {
//                $this->db->delete('tbl_student', array('id' => $id));
//                $this->db->delete('tbl_student_wise_subject', array('student_id' => $id));
//            }
//        } else {
//            $this->db->delete('tbl_student', array('id' => $id));
//            $this->db->delete('tbl_student_wise_subject', array('student_id' => $id));
//        }
        $data = array();
        if ($student_photo_info[0]['allowed_for_timekeeping'] == 1) {
            $data['syn_date'] = date('Y-m-d');
        }
        $data['status'] = 0;
        $data['is_deleted'] = 1;
        $this->db->where('id', $id);
        $this->db->update('tbl_student', $data);
        $sdata['message'] = "Student Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("students/index");
    }

    public function ajax_subject_by_class()
    {
        $class_id = $_GET['class_id'];
        $data = array();
        $data['type'] = 'table';
        if (isset($_GET['type'])) {
            $data['type'] = 'dropdown';
        }
        $data['subject_list'] = $this->db->query("SELECT
  c.`subject_id`,
  s.`name`, s.`code`
FROM
  `tbl_class_wise_subject` AS c
  LEFT JOIN `tbl_subject` AS s
    ON s.`id` = c.`subject_id`
WHERE c.`class_id` = '$class_id' GROUP BY c.`subject_id` ORDER BY c.`order_number`")->result_array();
        $this->load->view('students/ajax_subject_by_class', $data);
    }









    public function extra_marking_process()
    {
        //echo '<pre>';
        //print_r($_POST); die;
        $exam_id = $_POST['exam_id'];
        $class_id = $_POST['class_id'];
        $section_id = $_POST['section_id'];
        $group_id = $_POST['group_id'];
        $shift_id = $_POST['shift_id'];
        $subject_id = $_POST['subject_id'];
        $loop_time = $_POST['loop_time'];

        //exam details
        $exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$exam_id'")->result_array();
        $year = $exam_info[0]['year'];
        if ($exam_info[0]['exam_type_id'] <= 0) {
            $sdata['exception'] = "Please input first what type of exam it is.";
            $this->session->set_userdata($sdata);
            redirect('students/extra_marking_index');
        }
        $exam_type_id = $exam_info[0]['exam_type_id'];

        $subject_where = "";
        if ($subject_id != '') {
            $subject_where = " AND `subject_id` = '$subject_id' ";
        }


        $extra_head = $this->db->query("SELECT exa.*,exh.name as head_name FROM `tbl_class_wise_extra_head_amount` as exa
                             INNER JOIN tbl_exam_marking_extra_head as exh on exa.head_id = exh.id WHERE exa.class_id = '$class_id' AND exa.exam_type_id = '$exam_type_id' ORDER BY exh.id ASC")->result_array();
        $i = 0;
        while ($i < $loop_time) {
            $student_id = $this->input->post("student_id_" . $i);
            $this->db->query("DELETE FROM tbl_student_wise_extra_mark WHERE `exam_id` = '$exam_id' AND `student_id` = '$student_id' $subject_where");

            foreach ($extra_head as $col_row):
            $data = array();
            $data['student_id'] = $student_id;
            $data['exam_id'] =  $exam_id;
            $data['subject_id'] =  $subject_id;
            $data['class_id'] =  $class_id;
            $data['section_id'] =  $section_id;
            $data['group_id'] =  $group_id;
            $data['shift_id'] =  $shift_id;
            $data['mark'] = $this->input->post("mark_" . $col_row['head_id'] . '_' . $i);
            $data['allowable_percentage'] = $this->input->post("allowable_percentage_" . $col_row['head_id']);
            $data['allowable_mark'] = ($data['mark'] * $this->input->post("allowable_percentage_" . $col_row['head_id'])) / 100;
            $data['head_id'] = $this->input->post("head_id_" . $col_row['head_id'] . '_' . $i);
            $this->db->insert('tbl_student_wise_extra_mark', $data);
            endforeach;

            $i++;
        }
        $sdata['message'] = "Extra Marking Data Processed Successfully !";
        $this->session->set_userdata($sdata);
        redirect("students/extra_marking_index");
    }


    public function extra_marking_index()
    {
        if ($_POST) {
            $exam_id = $_POST['exam_id'];
            $class_id = $_POST['class_id'];
            $section_id = $_POST['section_id'];
            $group_id = $_POST['group_id'];
            $shift_id = $_POST['shift_id'];
            $subject_id = $_POST['subject_id'];

            if ($subject_id == 'NA') {
                $subject_id = null;
            }

            //exam details
            $exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$exam_id'")->result_array();
            $year = $exam_info[0]['year'];
            if ($exam_info[0]['exam_type_id'] <= 0) {
                $sdata['exception'] = "Please input first what type of exam it is.";
                $this->session->set_userdata($sdata);
                redirect('students/extra_marking_index');
            }
            $exam_type_id = $exam_info[0]['exam_type_id'];

            $data = array();
            $data['exam_id'] = $_POST['exam_id'];
            $data['class_id'] = $_POST['class_id'];
            $data['section_id'] = $_POST['section_id'];
            $data['group_id'] = $_POST['group_id'];
            $data['shift_id'] = $_POST['shift_id'];
            $data['subject_id'] = $subject_id;

            $data['title'] = 'Extra Marking';
            $data['heading_msg'] = "Extra Marking";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);

            $data['student_info'] = $this->db->query("SELECT
            s.`id`, s.`name`, s.`roll_no`, s.`reg_no`, s.`student_code`, s.`year`
            FROM
            `tbl_student` AS s
            WHERE s.`class_id` = '$class_id' AND s.`shift_id` = '$shift_id' AND s.`group` = '$group_id'
            AND s.`section_id` = '$section_id' AND s.`year` = '$year' AND s.`status` = 1 ORDER BY s.`roll_no`")->result_array();

            $where = "";
            if ($subject_id != null) {
                $where = " AND subject_id = '$subject_id' ";
            }

            $marks = $this->db->query("SELECT * from tbl_student_wise_extra_mark where exam_id = '$exam_id' $where
                             AND student_id in (SELECT s.`id` FROM `tbl_student` AS s WHERE s.`class_id` = '$class_id' AND s.`shift_id` = '$shift_id' AND s.`group` = '$group_id'
                             AND s.`section_id` = '$section_id' AND s.`status` = 1)")->result_array();

            $data['marks'] = array();
            foreach ($marks as $mark_row):
                $data['marks'][$mark_row['student_id'].'_'.$mark_row['head_id']]['student_id'] = $mark_row['student_id'];
            $data['marks'][$mark_row['student_id'].'_'.$mark_row['head_id']]['head_id'] = $mark_row['head_id'];
            $data['marks'][$mark_row['student_id'].'_'.$mark_row['head_id']]['mark'] = $mark_row['mark'];
            $data['marks'][$mark_row['student_id'].'_'.$mark_row['head_id']]['subject_id'] = $mark_row['subject_id'];
            $data['marks'][$mark_row['student_id'].'_'.$mark_row['head_id']]['exam_id'] = $mark_row['exam_id'];
            endforeach;

            //echo '<pre>';
            //print_r($data['marks']); die;

            $data['extra_head'] = $this->db->query("SELECT exa.*,exh.name as head_name FROM `tbl_class_wise_extra_head_amount` as exa
                             INNER JOIN tbl_exam_marking_extra_head as exh on exa.head_id = exh.id WHERE exa.class_id = '$class_id' AND exa.exam_type_id = '$exam_type_id' ORDER BY exh.id ASC")->result_array();

            // echo $class_id.'/'.$exam_type_id;
            // echo '<pre>';
            // print_r($data['extra_head']); die;

            if ($subject_id != null) {
                $data['ClassSubjectName'] = $this->db->query("SELECT
                  cs.`credit`, s.`name` AS SubjectName, s.`code` AS SubjectCode, c.`name` AS ClassName
                FROM
                  `tbl_class_wise_subject` AS cs
                  INNER JOIN `tbl_class` AS c
                    ON c.`id` = cs.`class_id`
                  INNER JOIN `tbl_subject` AS s
                    ON s.`id` = cs.`subject_id`
                WHERE c.`id` = '$class_id'
                  AND s.`id` = '$subject_id' AND cs.`exam_type_id` = '$exam_type_id' ")->result_array();
            } else {
                $data['ClassSubjectName'] = $this->db->query("SELECT c.name as ClassName FROM tbl_class as c WHERE c.id = '$class_id'")->result_array();
            }


            $data['maincontent'] = $this->load->view('students/extra_marking_marking_process', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Extra Marking';
            $data['heading_msg'] = "Extra Marking";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['ExamList'] = $this->db->query("SELECT * FROM `tbl_exam` ORDER BY id")->result_array();
            $data['ClassList'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['SectionList'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['GroupList'] = $this->Admin_login->getGroupList();
            $data['ShiftList'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
            $config = $this->db->query("SELECT add_extra_mark_with_total_obtained FROM `tbl_config`")->result_array();
            $data['add_extra_mark_with_total_obtained'] = $config[0]['add_extra_mark_with_total_obtained'];
            $data['maincontent'] = $this->load->view('students/extra_marking_index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }








    public function ajax_student_by_class_section()
    {
        $class_id = $_GET['class_id'];
        $section_id = $_GET['section_id'];
        $shift_id = $_GET['shift_id'];
        $data = array();
        $data['StudentList'] = $this->db->query("SELECT
  s.`id`, s.`name`, s.`roll_no`, s.`reg_no`
FROM
  `tbl_student` AS s
WHERE s.`class_id` = '$class_id'
  AND s.`section_id` = '$section_id' AND s.`shift_id` = '$shift_id' AND s.`status` = 1 ORDER BY s.`roll_no`")->result_array();
        $this->load->view('students/ajax_student_by_class_section', $data);
    }













    public function getExtraHeadMarkForTotalMark($class_id, $section_id, $group_id, $shift_id, $exam_id, $head_id)
    {
        $rows = $this->db->query("SELECT * FROM `tbl_student_wise_extra_mark` WHERE `class_id` = '$class_id' AND `section_id` = '$section_id'
AND `group_id` = '$group_id' AND `shift_id` = '$shift_id' AND `exam_id` = '$exam_id' AND `head_id` = '$head_id'")->result_array();
        $extra_head_mark = array();
        foreach ($rows as $row) {
            $extra_head_mark[$row['student_id']]['mark'] = $row['mark'];
            $extra_head_mark[$row['student_id']]['allowable_percentage'] = $row['allowable_percentage'];
            $extra_head_mark[$row['student_id']]['allowable_mark'] = $row['allowable_mark'];
            $extra_head_mark[$row['student_id']]['student_id'] = $row['student_id'];
        }
        return $extra_head_mark;
    }



    public function student_allocate_for_special_care()
    {
        if ($_POST) {
            $data = array();
            $data['title'] = 'Student Allocate for Special Care';
            $data['heading_msg'] = "Student Allocate for Special Care";
            $year = $this->input->post("year");
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $shift_id = $this->input->post("shift_id");
            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['shift_id'] = $shift_id;
            $data['group'] = $group;
            $data['student_info'] = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code`,sl.`year`,sl.`id` AS already_save  FROM `tbl_student` AS s
LEFT JOIN `tbl_student_special_care_info` AS sl ON sl.`student_id` = s.`id` AND sl.`year` = '$year'
WHERE s.`class_id` = '$class_id' AND  s.`shift_id` = '$shift_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group' ORDER BY s.roll_no")->result_array();
            $data['top_menu'] = $this->load->view('students/student_menu', $data, true);
            $data['maincontent'] = $this->load->view('students/student_allocate_for_special_care_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Student Allocate for Special Care';
            $data['heading_msg'] = "Student Allocate for Special Care";
            $data['top_menu'] = $this->load->view('students/student_menu', $data, true);
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`  ORDER BY id")->result_array();
            $data['shifts'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`  ORDER BY id")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['maincontent'] = $this->load->view('students/student_allocate_for_special_care', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function grand_exam_mark_add()
    {
        if ($_POST) {
            $data = array();
            $data['title'] = 'Student Exam Marks Process';
            $data['heading_msg'] = "Student Exam Marks Process";
            $year = $this->input->post("year");
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $exam_id = $this->input->post("exam_id");

            $exam_info = $this->db->query("SELECT * FROM tbl_exam WHERE id = '$exam_id'")->result_array();

            if (!empty($exam_info)) {
                $percentage_of_grand_result = $exam_info[0]['percentage_of_grand_result'];
            }
            $data['percentage_of_grand_result'] = $percentage_of_grand_result;

            $shift_id = $this->input->post("shift_id");
            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['shift_id'] = $shift_id;
            $data['group'] = $group;
            $data['exam_id'] = $exam_id;
            $data['student_info'] = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code`,sl.`id` AS already_save,sl.`total_marks`,sl.`total_obtain_marks`  FROM `tbl_student` AS s
LEFT JOIN `tbl_grand_exam_result` AS sl ON sl.`student_id` = s.`id` AND sl.`exam_id` = '$exam_id' AND sl.`year` = '$year'
WHERE s.`class_id` = '$class_id' AND  s.`shift_id` = '$shift_id' AND s.`section_id` = '$section_id'
AND s.`group` = '$group' AND s.year = '$year' ORDER BY s.roll_no")->result_array();
            $data['top_menu'] = $this->load->view('students/student_menu', $data, true);
            $data['maincontent'] = $this->load->view('students/grand_exam_mark_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Student Exam Marks Process';
            $data['heading_msg'] = "Student Exam Marks Process";
            $data['top_menu'] = $this->load->view('students/student_menu', $data, true);
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class` ORDER BY id")->result_array();
            $data['shifts'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section` ORDER BY id")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['exam_list'] = $this->db->query("SELECT * FROM `tbl_exam`")->result_array();
            $data['maincontent'] = $this->load->view('students/grand_exam_mark_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function grand_exam_mark_data_save()
    {
        $year = $this->input->post("year");
        $loop_time = $this->input->post("loop_time");
        $class_id = $this->input->post("class_id");
        $section_id = $this->input->post("section_id");
        $group = $this->input->post("group");
        $shift_id = $this->input->post("shift_id");
        $exam_id = $this->input->post("exam_id");
        $percentage_of_grand_result = $this->input->post("percentage_of_grand_result");
        $this->db->delete('tbl_grand_exam_result', array('class_id' => $class_id, 'section_id' => $section_id, 'group' => $group, 'shift_id' => $shift_id, 'exam_id' => $exam_id, 'year' => $year));
        $i = 1;
        while ($i <= $loop_time) {
            $data = array();
            $data['student_id'] = $this->input->post("student_id_" . $i);
            $data['total_marks'] = $this->input->post("total_marks");
            $data['total_obtain_marks'] = $this->input->post("total_obtain_marks_" . $i);
            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['shift_id'] = $shift_id;
            $data['exam_id'] = $exam_id;
            $data['date'] = date('Y-m-d');
            $data['average_mark'] = ($this->input->post("total_obtain_marks_" . $i) * $percentage_of_grand_result) / $this->input->post("total_marks");
            $this->db->insert('tbl_grand_exam_result', $data);
            $i++;
        }
        $sdata['message'] = "Data Successfully Added.";
        $this->session->set_userdata($sdata);
        redirect("students/grand_exam_mark_add");
    }


    public function student_allocate_for_special_care_data_save()
    {
        $year = $this->input->post("year");
        $loop_time = $this->input->post("loop_time");
        $class_id = $this->input->post("class_id");
        $section_id = $this->input->post("section_id");
        $group = $this->input->post("group");
        $shift_id = $this->input->post("shift_id");
        $this->db->delete('tbl_student_special_care_info', array('class_id' => $class_id, 'section_id' => $section_id, 'group' => $group, 'shift_id' => $shift_id, 'year' => $year));
        $i = 1;
        while ($i <= $loop_time) {
            if ($this->input->post("is_allow_" . $i)) {
                $data = array();
                $data['student_id'] = $this->input->post("student_id_" . $i);
                $data['year'] = $year;
                $data['class_id'] = $class_id;
                $data['section_id'] = $section_id;
                $data['group'] = $group;
                $data['shift_id'] = $shift_id;
                $data['date'] = date('Y-m-d');
                $this->db->insert('tbl_student_special_care_info', $data);
            }
            $i++;
        }
        $sdata['message'] = "Student Allocate for Special Care Data Successfully Added.";
        $this->session->set_userdata($sdata);
        redirect("students/student_allocate_for_special_care");
    }



}
