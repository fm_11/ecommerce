
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Exam_sessions extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Exam Routine Session';
		$data['heading_msg'] = "Exam Routine Session";
		$data['is_show_button'] = "add";
		$data['exam_sessions'] = $this->db->query("SELECT * FROM tbl_exam_session")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('exam_sessions/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add()
	{
		if ($_POST) {
			$data = array();
			$data['name'] = $_POST['name'];
			$data['header'] = $_POST['header'];
			$data['start_time'] = $_POST['start_time'];
			$data['end_time'] = $_POST['end_time'];
			$this->db->insert("tbl_exam_session", $data);
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect('exam_sessions/add');
		}
		$data = array();
		$data['title'] = 'Exam Routine Session';
		$data['heading_msg'] = "Exam Routine Session";
		$data['is_show_button'] = "index";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('exam_sessions/add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function edit($id=null)
	{
		if ($_POST) {
			$data = array();
			$data['id'] = $_POST['id'];
			$data['name'] = $_POST['name'];
			$data['header'] = $_POST['header'];
			$data['start_time'] = $_POST['start_time'];
			$data['end_time'] = $_POST['end_time'];
			$this->db->where('id', $id);
			$this->db->update('tbl_exam_session', $data);
			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect('exam_sessions/index');
		}
		$data = array();
		$data['title'] = 'Exam Routine Session';
		$data['heading_msg'] = "Exam Routine Session";
		$data['is_show_button'] = "index";
		$data['exam_sessions'] = $this->db->query("SELECT * FROM tbl_exam_session WHERE id = $id")->row();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('exam_sessions/edit', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function delete($id)
	{
		$exam_session_info = $this->db->where('exam_session_id', $id)->get('tbl_exam_routine')->result_array();
		if (!empty($exam_session_info)) {
			$sdata['exception'] = $this->lang->line('delete_error_message') . ", Exam Routine Depended Data Found !";
			$this->session->set_userdata($sdata);
			redirect("exam_sessions/index");
		}
		$this->db->query("DELETE FROM tbl_exam_session WHERE `id` = '$id'");
		$sdata['message'] = $this->lang->line('delete_success_message');
		$this->session->set_userdata($sdata);
		redirect('exam_sessions/index');
	}
}
