<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Sms_send_reports extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Message', 'Admin_login', 'Student'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = "SMS Send Report";
		$data['heading_msg'] = "SMS Send Report";
		if($_POST){
			$from_date = $this->input->post("from_date");
			$to_date = $this->input->post("to_date");
			$sms_from = $this->input->post("sms_from");
			$data['from_date'] = $from_date;
			$data['to_date'] = $to_date;
			$data['sms_from'] = $sms_from;

			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$Info = array();
			$Info['school_name'] = $SchoolInfo[0]['school_name'];
			$Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
			$Info['address'] = $SchoolInfo[0]['address'];
			$data['HeaderInfo'] = $Info;

			$where = "";
			if($sms_from != 'ALL'){
				$where = " AND r.`sender_type` = '$sms_from'";
			}
			$data['history_list'] = $this->db->query("SELECT s.`name` AS student_name,s.`student_code`,t.`name` AS teacher_name,t.`teacher_code`,r.* 
									FROM `tbl_sms_response` AS r 
									LEFT JOIN `tbl_student` AS s ON s.`id` = r.`student_id`
									LEFT JOIN `tbl_teacher` AS t ON t.`id` = r.`teacher_id` 
									WHERE DATE(r.`date_time`) BETWEEN '$from_date' AND '$to_date' $where;")->result_array();
			$data['report'] = $this->load->view('sms_send_reports/history_list', $data, true);
		}
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('sms_send_reports/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}
