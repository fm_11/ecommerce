<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Id_card_configs extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login'));
		$this->load->library('session');
		date_default_timezone_set('Asia/Dhaka');
		$this->notification = array();
	}


	public function index()
	{
		$data = array();
		$data['title'] = "ID Card Configuration";
		$data['heading_msg'] = "ID Card Configuration";
		$config_info = $this->db->query("SELECT * FROM `tbl_id_card_config`")->result_array();
		if (empty($config_info)) {
			$sdata['exception'] = "Initial configuration not found to database";
			$this->session->set_userdata($sdata);
			redirect('dashboard/index');
		}
		if ($_POST) {
			$data = array();
			$data['id'] = $_POST['id'];
			if(isset($_POST['submit'])){
				$data['main_color'] = '#' . $_POST['main_color'];
				$data['main_text_color'] = '#' . $_POST['main_text_color'];
				$data['secondary_text_color'] = '#' . $_POST['secondary_text_color'];
				$data['class_show'] = $_POST['class_show'];
				$data['section_show'] = $_POST['section_show'];
				$data['group_show'] = $_POST['group_show'];
				$data['shift_show'] = $_POST['shift_show'];
				$data['roll_show'] = $_POST['roll_show'];
				$data['year_show'] = $_POST['year_show'];
				$data['mobile_show'] = $_POST['mobile_show'];
				$data['fathers_name_show'] = $_POST['fathers_name_show'];
				$data['mothers_name_show'] = $_POST['mothers_name_show'];
				$data['date_of_birth_show'] = $_POST['date_of_birth_show'];
			}else{
				$data['main_color'] = "#28A739";
				$data['main_text_color'] = "#075707";
				$data['secondary_text_color'] = "#000000";
				$data['class_show'] = 1;
				$data['section_show'] = 1;
				$data['group_show'] = 1;
				$data['shift_show'] = 1;
				$data['roll_show'] = 1;
				$data['year_show'] = 1;
				$data['mobile_show'] = 1;
				$data['fathers_name_show'] = 0;
				$data['mothers_name_show'] = 0;
				$data['date_of_birth_show'] = 0;
			}

			$data['back_part_top_text'] = $_POST['back_part_top_text'];
			$data['back_part_bottom_text'] = $_POST['back_part_bottom_text'];
			$data['bar_code_value'] = $_POST['bar_code_value'];

			//QR Code
			$qr_code_name = "id_card_" . time() . "_" . date('Y-m-d') . '.png';;
			$qr_code_data = $_POST['bar_code_value'];
			if ($this->Admin_login->qr_code_generate($qr_code_data, $qr_code_name) != true) {
				$sdata['exception'] = $this->lang->line('edit_error_message') . ", Error: QR Code generate failed ! ";
				$this->session->set_userdata($sdata);
				redirect("id_card_configs/index");
			}

			$data['bar_code'] = $qr_code_name;
			//QR Code End


			$this->db->where('id', $data['id']);
			$this->db->update('tbl_id_card_config', $data);
			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect('id_card_configs/index');
		}
		$data['config_info'] = $config_info;
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('id_card_configs/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}
