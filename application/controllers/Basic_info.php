<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Basic_info extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('basic') . ' ' . $this->lang->line('information');
        $data['heading_msg'] = $this->lang->line('basic') . ' ' . $this->lang->line('information');
        $data['basic_info'] = $this->Admin_login->get_basic_info();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('basic_info/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function update()
    {
        $file_name = $_FILES['txtPhoto']['name'];
        if ($file_name != '') {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/logos/';
            $config['allowed_types'] = 'gif|jpg|png|JPEG|JPG|jpeg';
            $config['max_size'] = '5000';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Basic_info" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/logos/" . $new_photo_name)) {
                        $data = array();
                        $data['id'] = $this->input->post('id', true);
                        $data['name'] = $this->input->post('name', true);
                        $data['contact_number'] = $this->input->post('contact_number', true);
                        $data['bikash_number'] = $this->input->post('bikash_number', true);
                        $data['help_line'] = $this->input->post('help_line', true);
                        $data['logo_path'] = $new_photo_name;
                        $this->Admin_login->update_basic_info($data);
                        $sdata['message'] = $this->lang->line('edit_success_message');
                        $this->session->set_userdata($sdata);
                        redirect("basic_info/index");
                    } else {
                        $sdata['exception'] = $this->lang->line('edit_error_message') . ' (' . $this->upload->display_errors() . ')';
                        $this->session->set_userdata($sdata);
                        redirect("basic_info/index");
                    }
                } else {
                    $sdata['exception'] = $this->lang->line('edit_error_message') . ' (' . $this->upload->display_errors() . ')';
                    $this->session->set_userdata($sdata);
                    redirect("basic_info/index");
                }
            }
        } else {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('name', true);
            $data['contact_number'] = $this->input->post('contact_number', true);
            $data['bikash_number'] = $this->input->post('bikash_number', true);
            $data['help_line'] = $this->input->post('help_line', true);
            $this->Admin_login->update_basic_info($data);
            $sdata['message'] = "You are Successfully Updated Contact Info ! ";
            $this->session->set_userdata($sdata);
            redirect("basic_info/index");
        }
    }
}
