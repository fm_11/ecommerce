<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Admit_designs extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login'));
		$this->load->library('session');
		date_default_timezone_set('Asia/Dhaka');
		$this->notification = array();
	}


	public function index()
	{
		$data = array();
		$data['title'] = "Admit Design";
		$config_info = $this->db->query("SELECT * FROM `tbl_admit_card_design`")->row();
		if (empty($config_info)) {
			$sdata['exception'] = "Initial configuration not found to database";
			$this->session->set_userdata($sdata);
			redirect('dashboard/index');
		}
		if ($_POST) {
//			echo '<pre>';
//			print_r($_POST);
//			die;
			$data = array();
			$data['id'] = $_POST['id'];
			$data['design_type'] = $_POST['design_type'];
			$data['background_color'] = "#" . ltrim($_POST['background_color'],"#");
			$data['header_color'] = "#" . ltrim($_POST['header_color'],"#");
			$data['text_color'] = "#" . ltrim($_POST['text_color'],"#");
			$data['main_first_border_color'] = "#" . ltrim($_POST['main_first_border_color'],"#");
			$data['main_second_border_color'] = "#" . ltrim($_POST['main_second_border_color'],"#");
			$this->db->where('id', $data['id']);
			$this->db->update('tbl_admit_card_design', $data);
			$sdata['message'] = "Admit configuration has been updated successfully";
			$this->session->set_userdata($sdata);
			redirect('admit_designs/index');
		}
		$data = array();
		$data['config_info'] = $config_info;
		$data['title'] = "Admit Design";
		$data['heading_msg'] = "Admit Design";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('admit_designs/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}
