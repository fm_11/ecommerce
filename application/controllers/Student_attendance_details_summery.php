<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Student_attendance_details_summery extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student', 'Admin_login','Students_report','Timekeeping'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Student Attendance Details Summery';
		$data['heading_msg'] = 'Student Attendance Details Summery';
		if ($_POST) {
			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;
			$from_date = $this->input->post("FromDate");
			$to_date = $this->input->post("ToDate");

			$FromDateYear = DateTime::createFromFormat("Y-m-d", $from_date)->format("Y");
			$ToDateYear = DateTime::createFromFormat("Y-m-d", $to_date)->format("Y");
			if($FromDateYear != $ToDateYear){
				$sdata['exception'] = "Please input two dates in the same year";
				$this->session->set_userdata($sdata);
				redirect("student_attendance_details_summery/index");
			}

			$group_id = $this->input->post("group_id");
			$period_id = $this->input->post("period_id");

			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['from_date'] = $from_date;
			$data['to_date'] = $to_date;
			$data['group_id'] = $group_id;
			$data['period_id'] = $period_id;

			if($period_id != 'FM'){
				$data['period_name'] = $this->Students_report->get_period_name($period_id);
			}else{
				$data['period_name'] = "From Machine";
			}
			$data['class_name'] =$this->Students_report->get_class_name($class_id);
			$data['section_name'] =$this->Students_report->get_section_name($section_id);
			$data['shift_name'] =$this->Students_report->get_shift_name($shift_id);
			if($group_id != '' && $group_id != 'all'){
				$data['group_name'] = $this->Students_report->get_group_name($group_id);
			}else{
				$data['group_name'] = 'All';
			}

			$number_of_days = $this->dateDiffInDays($data['from_date'], $data['to_date']);
			$data['total_days'] = $number_of_days;
			//echo $total_working_days; die;
			//die;

			$pdata = array();
			$pdata['from_date'] = $this->input->post("FromDate");
			$pdata['to_date'] = $this->input->post("ToDate");
			$pdata['class_id'] = $class_id;
			$pdata['shift_id'] = $shift_id;
			$pdata['section_id'] = $section_id;
			$pdata['group_id'] = $group_id;
			$pdata['period_id'] = $period_id;
			$pdata['year'] = $FromDateYear;
			$pdata['number_of_days'] = $number_of_days;

			$data['adata'] = $this->Timekeeping->get_student_attendance_details($pdata);

			$data['report'] = $this->load->view('student_attendance_details_summery/report', $data, true);
		}
		if (isset($_POST['pdf_download'])) {
			$data['is_pdf'] = 1;
			//Dom PDF
			$this->load->library('mydompdf');
			$html = $this->load->view('student_attendance_details_summery/report', $data, true);
			$this->mydompdf->createPDF($html, 'StudentAttendanceDetailsSummery', true, 'A4', 'portrait');
			//Dom PDF
		} else {
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['GroupList'] = $this->Admin_login->getGroupList();
			$data['period_list'] = $this->db->query("SELECT * FROM `tbl_class_period`")->result_array();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('student_attendance_details_summery/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}


	public function dateDiffInDays($date1, $date2)
	{
		// Creates DateTime objects
		$datetime1 = date_create($date1);
		$datetime2 = date_create($date2);

		// Calculates the difference between DateTime objects
		$interval = date_diff($datetime1, $datetime2);

		// Display the result
		return  $interval->format('%a') + 1;
	}

	public function getStudentAttendanceDetailsSummeryReport()
	{
		$data = array();
		if ($_POST) {
			$SchoolInfo = $this->fetReportHeader();
			$Info = array();
			$Info['school_name'] = $SchoolInfo[0]['school_name'];
			$Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
			$data['HeaderInfo'] = $Info;

			$data['from_date'] = $this->input->post("FromDate");
			$data['to_date'] = $this->input->post("ToDate");
			$data['class_id'] = $this->input->post("class_id");
			$data['section_id'] = $this->input->post("section_id");

			$class_id = $data['class_id'];
			$class_info = $this->db->query("SELECT * FROM tbl_class WHERE id ='$class_id'")->result_array();
			$data['class_name'] = $class_info[0]['name'];

			$total_working_days = $this->dateDiffInDays($data['from_date'], $data['to_date']);
			$data['total_days'] = $total_working_days;
			//die;

			$pdata = array();
			$pdata['from_date'] = $this->input->post("FromDate");
			$pdata['to_date'] = $this->input->post("ToDate");
			$pdata['class_id'] = $this->input->post("class_id");
			$pdata['section_id'] = $this->input->post("section_id");
			$pdata['total_working_days'] = $total_working_days;

			$data['title'] = 'Student Attendance Details Report';
			$data['heading_msg'] = "Student Attendance Details Report";
			$data['adata'] = $this->Timekeeping->get_student_attendance_details_summery($pdata);

			if (isset($_POST['pdf_download'])) {
				//echo 'gggg'; die;
				// Load all views as normal
				$data['is_pdf'] = 1;
				$html = $this->load->view('report/student_attendance_details_summery_report_table', $data, true);
				$pdfFilePath = "student_attendance_details_summery_report.pdf";
				$this->load->library('m_pdf');
				$this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
				$this->m_pdf->pdf->AddPage(
					'L', // L - landscape, P - portrait
					'',
					'',
					'',
					'',
					10, // margin_left
					10, // margin right
					10, // margin top
					10, // margin bottom
					10, // margin header
					10
				); // margin footer

				//generate the PDF from the given html
				$this->m_pdf->pdf->defaultCSS = true;
				//generate the PDF from the given html
				$this->m_pdf->pdf->autoScriptToLang = true;
				$this->m_pdf->pdf->WriteHTML($html);
				//download it.
				$this->m_pdf->pdf->Output($pdfFilePath, "D");
				die;
			} else {
				$data['report'] = $this->load->view('report/student_attendance_details_summery_report_table', $data, true);
			}
		}
		$data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
		$data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
		$data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$data['title'] = 'Student Attendance Details Summery Report';
		$data['heading_msg'] = "Student Attendance Details Summery Report";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('report/student_attendance_details_summery_report', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}


}

