<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Leave_types extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Leave Type';
        $data['heading_msg'] = "Leave Type";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['types'] = $this->db->query("SELECT * FROM tbl_leave_type")->result_array();
        $data['maincontent'] = $this->load->view('leave_types/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['short_name'] = $_POST['short_name'];
            $this->db->insert("tbl_leave_type", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('leave_types/index');
        }
        $data = array();
        $data['title'] = 'Add Leave Type';
        $data['heading_msg'] = "Add Leave Type";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('leave_types/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['short_name'] = $_POST['short_name'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_leave_type', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('leave_types/index');
        }
        $data = array();
        $data['title'] = 'Update Leave Type Information';
        $data['heading_msg'] = "Update Leave Type Information";
        $data['is_show_button'] = "index";
        $data['types'] = $this->db->query("SELECT * FROM `tbl_leave_type` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('leave_types/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
        $leave_info = $this->db->where('leave_type_id', $id)->get('tbl_teacher_staff_leave_applications')->result_array();
        if (!empty($leave_info)) {
            $sdata['exception'] =  $this->lang->line('delete_success_message') . " (Leave Depended Data Found)";
            $this->session->set_userdata($sdata);
            redirect("leave_types/index");
        }
        $this->db->delete('tbl_leave_type', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('leave_types/index');
    }
}
