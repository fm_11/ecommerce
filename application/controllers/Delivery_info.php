<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Delivery_info extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('delivery_info');
        $data['heading_msg'] = $this->lang->line('delivery_info');
        $data['delivery_info'] = $this->Admin_login->get_delivery_info();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('delivery_info/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function update()
    {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['inside_city_cost'] = $this->input->post('inside_city_cost', true);
            $data['outside_city_cost'] = $this->input->post('outside_city_cost', true);
            $data['description_inside'] = $this->input->post('description_inside', true);
            $data['description_outside'] = $this->input->post('description_outside', true);
            $this->Admin_login->update_delivery_info($data);
            $sdata['message'] = "You are Successfully Updated Delivery Info ! ";
            $this->session->set_userdata($sdata);
            redirect("delivery_info/index");
    }
}
