
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Teacher_attendance_settings extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Teacher_info', 'Admin_login'));
		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}


	public function index()
	{
		if($_POST){
			$total_row = $this->input->post("total_row");
			$data = array();
			$i = 0;
			$row_no = 0;
			while ($i < $total_row){
				$in_time = $this->input->post("in_time_" . $i);
				$out_time = $this->input->post("out_time_" . $i);
				if($in_time != '' && $out_time != ''){
					$data[$row_no]['teacher_id'] =  $this->input->post("teacher_id_" . $i);
					$data[$row_no]['in_time'] = $in_time;
					$data[$row_no]['out_time'] = $out_time;
					$data[$row_no]['flexible_min_for_late'] =  $this->input->post("flexible_min_for_late_" . $i);
					$row_no++;
				}
				$i++;
			}
			if($row_no <= 0){
				$sdata['exception'] = $this->lang->line('add_error_message') . ' (Please select minimum input 1 row)';
				$this->session->set_userdata($sdata);
				redirect("teacher_attendance_settings/index");
			}
			$this->db->empty_table('tbl_teacher_attendance_settings');
			$this->db->insert_batch('tbl_teacher_attendance_settings', $data);
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect("teacher_attendance_settings/index");
		}
		$data = array();
		$data['title'] = "Teacher/Staff Attendance Settings";
		$data['heading_msg'] = "Teacher/Staff Attendance Settings";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$para = array();
		$para['status'] = '1';
		$data['teachers'] = $this->db->query("SELECT t.`id`,t.`name`,t.`teacher_code`,p.`name` AS teacher_post,s.`in_time`,s.`out_time`,s.`flexible_min_for_late`
FROM `tbl_teacher` AS t 
LEFT JOIN `tbl_teacher_post` AS p ON p.`id` = t.`post`
LEFT JOIN `tbl_teacher_attendance_settings` AS s ON s.`teacher_id` = t.`id`
WHERE t.`status` = '1' ORDER BY p.`shorting_order` ASC;")->result_array();
		$data['maincontent'] = $this->load->view('teacher_attendance_settings/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}
