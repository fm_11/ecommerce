<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Student_fees extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Config_general' ,'Admin_login','Message','Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = 'Finance -> Student Fees';
        $data['heading_msg'] = "Finance -> Student Fees";
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }




    public function get_fee_sub_category()
    {
        $data = array();
        $data['title'] = 'Fee Sub Category';
        $data['heading_msg'] = "Fee Sub Category";
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['fee_sub_category'] = $this->db->query("SELECT fs.*,fc.`name` AS fee_category_name
           FROM `tbl_fee_sub_category` AS fs
LEFT JOIN `tbl_fee_category` AS fc ON fs.`category_id` = fc.`id`")->result_array();
        $data['maincontent'] = $this->load->view('student_fees/fee_sub_category_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function fee_sub_category_add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['category_id'] = $_POST['category_id'];
            $data['fee_type'] = $_POST['fee_type'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->insert("tbl_fee_sub_category", $data);
            $sdata['message'] = "Data Added Successfully.";
            $this->session->set_userdata($sdata);
            redirect('student_fees/get_fee_sub_category');
        }
        $data = array();
        $data['title'] = 'Add Fee Sub Category';
        $data['heading_msg'] = "Add Fee Sub Category";
        $data['action'] = '';
        $data['fee_category_info'] = $this->db->query("SELECT * FROM tbl_fee_category")->result_array();
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/fee_sub_category_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function student_fee_sub_category_edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $id = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['category_id'] = $_POST['category_id'];
            $data['fee_type'] = $_POST['fee_type'];
            $data['remarks'] = $_POST['remarks'];
            $this->db->where('id', $id);
            $this->db->update('tbl_fee_sub_category', $data);
            $sdata['message'] = "Data Updated Successfully.";
            $this->session->set_userdata($sdata);
            redirect('student_fees/get_fee_sub_category');
        }
        $data = array();
        $data['title'] = 'Update Fee Sub Category';
        $data['heading_msg'] = "Update Fee Sub Category";
        $data['action'] = 'edit';
        $data['fee_category_info'] = $this->db->query("SELECT * FROM tbl_fee_category")->result_array();
        $data['fee_sub_category_info'] = $this->db->query("SELECT * FROM tbl_fee_sub_category WHERE `id` = '$id'")->result_array();
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/fee_sub_category_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function student_fee_sub_category_delete($id)
    {
        $this->db->delete('tbl_fee_sub_category', array('id' => $id));
        $sdata['message'] = "Data Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("student_fees/get_fee_sub_category");
    }


    public function fee_allocation_add()
    {
        if ($_POST) {
            $fee_sub_category_info = $this->Student_fee->getSubCategory();
            $loop_time = $this->input->post("loop_time");
            $class_id = $this->input->post("class_id");
            $year = $this->input->post("year");
            $i = 0;
            while ($i < $loop_time) {
                if ($this->input->post("is_selected_" . $i)) {
                    $data = array();
                    $data['class_id'] = $class_id;
                    $data['category_id'] = $_POST['category_id_'. $i];
                    $data['sub_category_id'] = $_POST['sub_category_id_'. $i];
                    $data['year'] = $year;
                    $data['amount'] = $_POST['amount_'. $i];
                    $data['resident_amount'] = $_POST['resident_amount_'. $i];
                    $data['fee_type'] = $fee_sub_category_info[$_POST['sub_category_id_'. $i]]['fee_type'];
                    $AlreadyAllocate = $this->Student_fee->checkFeeAlreadyAllocate($class_id, $_POST['category_id_'. $i], $_POST['sub_category_id_'. $i], $year);
                    if (empty($AlreadyAllocate)) {
                        $this->db->insert("tbl_fee_allocate", $data);
                    } else {
                        $data['id'] = $AlreadyAllocate[0]['id'];
                        $this->db->where('id', $data['id']);
                        $this->db->update('tbl_fee_allocate', $data);
                    }
                }
                $i++;
            }

            $sdata['message'] = "Data Added Successfully.";
            $this->session->set_userdata($sdata);
            redirect('student_fees/fee_allocation_add');
        }
        $data = array();
        $data['title'] = 'Fee Allocation';
        $data['heading_msg'] = "Fee Allocation";
        $data['action'] = '';
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/fee_allocation_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }





    public function fee_category_wise_amount_add()
    {
        if ($_POST) {
            $class_id = $this->input->post('class_id', true);
            if ($class_id == 'all') {
                $class = $this->db->query("SELECT id FROM tbl_class")->result_array();
                $i = 0;
                while ($i < count($class)) {
                    $data = array();
                    $data['class_id'] = $class[$i]['id'];
                    $data['category_id'] = $_POST['category_id'];
                    $data['year'] = $_POST['year'];
                    $data['amount'] = $_POST['amount'];
                    $data['resident_amount'] = $_POST['resident_amount'];
                    $data['remarks'] = $_POST['remarks'];
                    $AlreadyAllocate = $this->Student_fee->checkFeeAlreadyAllocateForNormal($class[$i]['id'], $_POST['category_id'], $_POST['year']);
                    if (empty($AlreadyAllocate)) {
                        $this->db->insert("tbl_fee_category_wise_amount", $data);
                    } else {
                        $data['id'] = $AlreadyAllocate[0]['id'];
                        $this->db->where('id', $data['id']);
                        $this->db->update('tbl_fee_category_wise_amount', $data);
                    }
                    $i++;
                }
            } else {
                $data = array();
                $data['class_id'] = $_POST['class_id'];
                $data['category_id'] = $_POST['category_id'];
                $data['year'] = $_POST['year'];
                $data['amount'] = $_POST['amount'];
                $data['resident_amount'] = $_POST['resident_amount'];
                $data['remarks'] = $_POST['remarks'];
                $AlreadyAllocate = $this->Student_fee->checkFeeAlreadyAllocateForNormal($_POST['class_id'], $_POST['category_id'], $_POST['year']);
                if (empty($AlreadyAllocate)) {
                    $this->db->insert("tbl_fee_category_wise_amount", $data);
                } else {
                    $data['id'] = $AlreadyAllocate[0]['id'];
                    $this->db->where('id', $data['id']);
                    $this->db->update('tbl_fee_category_wise_amount', $data);
                }
            }

            $sdata['message'] = "Data Added Successfully.";
            $this->session->set_userdata($sdata);
            redirect('student_fees/fee_category_wise_amount_add');
        } else {
            $data = array();
            $data['title'] = 'Category Wise Fee Allocation';
            $data['heading_msg'] = "Category Wise Fee Allocation";
            $data['action'] = '';
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['fee_category_info'] = $this->db->query("SELECT * FROM tbl_fee_category")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/fee_category_wise_amount_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function student_absent_fine()
    {
        $data = array();
        $data['title'] = 'Student Absent Fine';
        $data['heading_msg'] = 'Student Absent Fine';
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/student_absent_fine/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_student_absent_fine(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['students'] = $this->Student_fee->get_all_student_absent_fine(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/student_absent_fine', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function student_fee_collection_priority_index()
    {
        $data = array();
        $data['title'] = 'Fee Collection Priority Set';
        $data['heading_msg'] = 'Fee Collection Priority Set';
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/student_fee_collection_priority_index/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_fee_priority(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['data'] = $this->Student_fee->get_all_fee_priority(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/student_fee_collection_priority_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function updateUseStatusUpdate()
    {
        $is_in_use = $this->input->get('is_in_use', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($is_in_use == 1) {
            $data['is_in_use'] = 0;
        } else {
            $data['is_in_use'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_student_fee_priority', $data);
        if ($is_in_use == 0) {
            echo '<a class="approve_icon" title="Approve" href="#" onclick="useStatusUpdate(' . $id . ',1)"></a>';
        } else {
            echo '<a class="reject_icon" title="Reject" href="#" onclick="useStatusUpdate(' . $id . ',0)"></a>';
        }
    }

    public function fee_collection_priority_add()
    {
        if ($_POST) {
            $year = $this->input->post("year");

            $mdata = array();
            $mdata['year'] = $year;
            $mdata['is_in_use'] = 1;
            $mdata['name'] = $this->input->post("name");
            $mdata['date'] = date('Y-m-d');
            if ($this->db->insert('tbl_student_fee_priority', $mdata)) {
                $insert_id = $this->db->insert_id();
                $loop_time = $this->input->post("loop_time");
                $i = 1;
                while ($i <= $loop_time) {
                    if ($this->input->post("is_allow_" . $i)) {
                        $data = array();
                        $data['priority_id'] = $insert_id;
                        if ($this->input->post("is_month_" . $i) != 1) {
                            if ($this->input->post("is_absent_" . $i) != 1 && $this->input->post("is_play_truant_" . $i) != 1) {
                                $data['fee_category_id'] = $this->input->post("category_id_" . $i);
                            }
                        } else {
                            $data['month'] = $this->input->post("month_" . $i);
                        }
                        $data['priority_number'] = $this->input->post("priority_" . $i);
                        $data['percentage_of_amount'] = $this->input->post("percentage_" . $i);
                        $data['is_month'] = $this->input->post("is_month_" . $i);
                        $data['is_absent_fine'] = $this->input->post("is_absent_" . $i);
                        $data['is_play_truant_fine'] = $this->input->post("is_play_truant_" . $i);
                        $this->db->insert('tbl_student_fee_priority_details', $data);
                    }
                    $i++;
                }
                $sdata['message'] = "Fee Collection Priority Set Successfully.";
                $this->session->set_userdata($sdata);
                redirect("student_fees/student_fee_collection_priority_index");
            } else {
                $sdata['exception'] = "Fee Collection Priority Data Doesn't Save !";
                $this->session->set_userdata($sdata);
                redirect("student_fees/student_fee_collection_priority_index");
            }
        } else {
            $data = array();
            $data['title'] = 'Fee Collection Priority Set';
            $data['heading_msg'] = "Fee Collection Priority Set";
            $data['action'] = '';
            $data['fee_category'] = $this->db->query("SELECT * FROM `tbl_fee_category`")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/fee_collection_priority_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function fee_priority_view($id)
    {
        $data = array();
        $data['title'] = 'Fee Collection Priority';
        $data['heading_msg'] = "Fee Collection Priority";
        $data['action'] = '';
        $data['details'] = $this->db->query("SELECT pd.*,c.`name` AS category_name,p.`name` AS priority_name,p.`year` FROM `tbl_student_fee_priority_details` AS pd
LEFT JOIN `tbl_fee_category` AS c ON pd.`fee_category_id` = c.`id`
INNER JOIN `tbl_student_fee_priority` AS p ON pd.`priority_id` = p.`id`
WHERE pd.`priority_id` = $id ORDER BY pd.`priority_number`")->result_array();
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/fee_priority_view', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function fee_priority_setting()
    {
        if ($_POST) {
            $loop_time = $this->input->post("loop_time");
            $i = 1;
            while ($i <= $loop_time) {
                $data = array();
                $data['class_id'] = $this->input->post("class_id_" . $i);
                $data['priority_id'] = $this->input->post("priority_id_" . $i);
                $data['date'] = date('Y-m-d');
                $class_id = $this->input->post("class_id_" . $i);
                $already_exists = $this->db->query("SELECT id FROM `tbl_priority_setting` WHERE class_id = $class_id")->result_array();
                if (!empty($already_exists)) {
                    $this->db->where('class_id', $data['class_id']);
                    $this->db->update('tbl_priority_setting', $data);
                } else {
                    $this->db->insert('tbl_priority_setting', $data);
                }
                $i++;
            }
            $sdata['message'] = "You are Successfully Priority Setting Updated !";
            $this->session->set_userdata($sdata);
            redirect("student_fees/student_fee_collection_priority_index");
        } else {
            $data = array();
            $data['title'] = 'Fee Collection Priority Setting';
            $data['heading_msg'] = "Fee Collection Priority Setting";
            $data['action'] = '';
            $data['class'] = $this->db->query("SELECT c.*,s.`priority_id` FROM `tbl_class` AS c
LEFT JOIN `tbl_priority_setting` AS s ON s.`class_id` = c.`id`")->result_array();
            $data['priority'] = $this->db->query("SELECT * FROM `tbl_student_fee_priority` WHERE is_in_use = 1")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/fee_priority_setting', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function fee_priority_delete($id)
    {
        if ($this->db->delete('tbl_student_fee_priority_details', array('priority_id' => $id))) {
            $this->db->delete('tbl_student_fee_priority', array('id' => $id));
            $sdata['message'] = "Fee Collection Priority Deleted Successfully !";
            $this->session->set_userdata($sdata);
            redirect("student_fees/student_fee_collection_priority_index");
        }
    }

    public function student_play_truant_fine()
    {
        $data = array();
        $data['title'] = 'Student Play Truant Fine';
        $data['heading_msg'] = 'Student Play Truant Fine';
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/student_play_truant_fine/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_student_play_truant_fine(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['students'] = $this->Student_fee->get_all_student_play_truant_fine(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/student_play_truant_fine', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function student_absent_fine_add()
    {
        if ($_POST) {
            $data = array();
            $data['title'] = 'Student Absent Fine';
            $data['heading_msg'] = "Student Absent Fine";
            $year = $this->input->post("year");
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['student_info'] = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code`,af.`year`,af.`amount`
            FROM `tbl_student` AS s
LEFT JOIN `tbl_fee_absent_fine` AS af ON af.`student_id` = s.`id` AND af.`year` = '$year'
WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group' AND s.`status` = 1 ORDER BY s.roll_no")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/student_absent_fine_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Student Absent Fine';
            $data['heading_msg'] = "Student Absent Fine";
            $data['action'] = '';
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/student_absent_fine_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function student_auto_absent_fine_approve()
    {
        $data = array();
        if ($_POST) {
            $month = $this->input->post("month");
            $year = $this->input->post("year");
            $data['month'] = $month;
            $data['year'] = $year;
            $number_of_days = cal_days_in_month(CAL_GREGORIAN, $month, 2005);

            $total_working_days = $number_of_days;
            $data['total_days'] = $total_working_days;

            $pdata = array();
            $pdata['from_date'] = $year. '-' . $month . '-01';
            $pdata['to_date'] = $year. '-' . $month . '-' . $number_of_days;
            $pdata['class_id'] = $this->input->post("class_id");
            $pdata['section_id'] = 'all';
            $pdata['total_working_days'] = $total_working_days;
            $data['adata'] = $this->Timekeeping->get_student_attendance_details_summery($pdata);

            //echo '<pre>';
            //print_r($data['adata']);
            //die;

            $class_id = $this->input->post("class_id");
            $class_info = $this->db->query("SELECT * FROM tbl_class WHERE id ='$class_id'")->result_array();
            $data['class_name'] = $class_info[0]['name'];
            $data['class_id'] = $this->input->post("class_id");
            $data['process'] = $this->load->view('student_fees/approve_student_absent_fine_table', $data, true);
        }
        $data['title'] = 'Approve Student Absent Fine';
        $data['heading_msg'] = "Approve Student Absent Fine";
        $data['action'] = '';
        $data['class'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/approve_student_absent_fine', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function approve_absent_fine_save()
    {
        $year = $this->input->post("year");
        $loop_time = $this->input->post("loop_time");
        $class_id = $this->input->post("class_id");
        $month = $this->input->post("month");

        $this->db->delete('tbl_approve_absent_fine', array('class_id' => $class_id, 'month' => $month, 'year' => $year));

        $i = 0;
        while ($i < $loop_time) {
            $student_id = $this->input->post("student_id_" . $i);
            $data = array();
            $data['student_id'] = $student_id;
            $data['month'] = $month;
            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['amount'] = $this->input->post("amount_" . $i);
            $this->db->insert('tbl_approve_absent_fine', $data);
            $i++;
        }
        $sdata['message'] = "Student Absent Fine Data Successfully Approved.";
        $this->session->set_userdata($sdata);
        redirect("student_fees/student_auto_absent_fine_approve");
    }




    public function student_play_truant_fine_add()
    {
        if ($_POST) {
            $data = array();
            $data['title'] = 'Student Play Truant Fine';
            $data['heading_msg'] = "Student Play Truant Fine";
            $year = $this->input->post("year");
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['student_info'] = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code`,af.`year`,af.`amount` FROM `tbl_student` AS s
LEFT JOIN `tbl_fee_play_truant_fine` AS af ON af.`student_id` = s.`id` AND af.`year` = '$year'
WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group' AND s.`status` = 1 ORDER BY s.roll_no")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/student_play_truant_fine_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Student Play Truant Fine';
            $data['heading_msg'] = "Student Play Truant Fine";
            $data['action'] = '';
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/student_play_truant_fine_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function student_play_truant_fine_data_save()
    {
        $year = $this->input->post("year");
        $loop_time = $this->input->post("loop_time");
        $class_id = $this->input->post("class_id");
        $section_id = $this->input->post("section_id");
        $group = $this->input->post("group");
        $this->db->delete('tbl_fee_absent_fine', array('class_id' => $class_id, 'section_id' => $section_id, 'group' => $group, 'year' => $year));
        $i = 1;
        while ($i <= $loop_time) {
            if ($this->input->post("is_allow_" . $i)) {
                $data = array();
                $data['student_id'] = $this->input->post("student_id_" . $i);
                $data['year'] = $year;
                $data['class_id'] = $class_id;
                $data['section_id'] = $section_id;
                $data['group'] = $group;
                $data['date'] = date('Y-m-d');
                $data['amount'] = $this->input->post("amount_" . $i);
                $this->db->insert('tbl_fee_play_truant_fine', $data);
            }
            $i++;
        }
        $sdata['message'] = "Student Play Truant Fine Data Successfully Updated.";
        $this->session->set_userdata($sdata);
        redirect("student_fees/student_play_truant_fine");
    }

    public function student_absent_fine_data_save()
    {
        $year = $this->input->post("year");
        $loop_time = $this->input->post("loop_time");
        $class_id = $this->input->post("class_id");
        $section_id = $this->input->post("section_id");
        $group = $this->input->post("group");

        $i = 1;
        while ($i <= $loop_time) {
            if ($this->input->post("is_allow_" . $i)) {
                $student_id = $this->input->post("student_id_" . $i);
                $this->db->delete('tbl_fee_absent_fine', array('student_id' => $student_id, 'year' => $year));
                $data = array();
                $data['student_id'] = $student_id;
                $data['year'] = $year;
                $data['class_id'] = $class_id;
                $data['section_id'] = $section_id;
                $data['group'] = $group;
                $data['date'] = date('Y-m-d');
                $data['amount'] = $this->input->post("amount_" . $i);
                $this->db->insert('tbl_fee_absent_fine', $data);
            }
            $i++;
        }
        $sdata['message'] = "Student Absent Fine Data Successfully Updated.";
        $this->session->set_userdata($sdata);
        redirect("student_fees/student_absent_fine");
    }

    public function student_play_truant_fine_delete($id)
    {
        $this->db->delete('tbl_fee_play_truant_fine', array('id' => $id));
        $sdata['message'] = "Play Truant Fine Data Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("student_fees/student_play_truant_fine");
    }

    public function student_absent_fine_delete($id)
    {
        $this->db->delete('tbl_fee_absent_fine', array('id' => $id));
        $sdata['message'] = "Absent Fine Data Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("student_fees/student_absent_fine");
    }

    public function category_wise_amount_index()
    {
        $data = array();
        $data['title'] = 'Category Wise Fee Allocation';
        $data['heading_msg'] = "Category Wise Fee Allocation";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/category_wise_amount_index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Student_fee->get_all_allocated_fee_for_normal(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['fees'] = $this->Student_fee->get_all_allocated_fee_for_normal(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/category_wise_amount_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function student_fee_add_for_normal()
    {
        if ($_POST) {
            $data = array();
            $data['year'] = $this->input->post('year', true);
            $data['class_id'] = $this->input->post('class_id', true);
            $data['section_id'] = $this->input->post('section_id', true);
            $data['student_id'] = $this->input->post('student_id', true);
            $data['amount'] = $this->input->post('amount', true);
            $data['payment_date'] = $this->input->post('payment_date', true);


            $cdata['c_class_id'] = $data['class_id'];
            $cdata['c_section_id'] = $data['section_id'];
            $cdata['c_student_id'] = $data['student_id'];
            $this->session->set_userdata($cdata);

            $collection_sheet = $this->Student_fee->createCollectionSheet($data);
            $data['student_return_money'] = $collection_sheet['student_return_money'];
//            echo '<pre>';
//            print_r($collection_sheet);
//            die;
            if ($this->db->insert('tbl_student_fee_account', $data)) {
                $insert_id = $this->db->insert_id();
                if (isset($collection_sheet['category_wise_fee_list']) && !empty($collection_sheet['category_wise_fee_list'])) {
                    $i = 0;
                    while ($i < count($collection_sheet['category_wise_fee_list'])) {
                        $cdata = array();
                        $cdata['fee_account_id'] = $insert_id;
                        $cdata['category_id'] = $collection_sheet['category_wise_fee_list'][$i][$i]['category_id'];
                        $cdata['allocated_fee'] = $collection_sheet['category_wise_fee_list'][$i][$i]['allocated_fee'];
                        $cdata['paid_amount'] = $collection_sheet['category_wise_fee_list'][$i][$i]['paid_amount'];
                        $cdata['due_amount'] = $collection_sheet['category_wise_fee_list'][$i][$i]['due_amount'];
                        $cdata['year'] = $this->input->post('year', true);
                        $this->db->insert('tbl_category_wise_fee', $cdata);
                        $i++;
                    }
                }
                if (isset($collection_sheet['monthly_fee_list']) && !empty($collection_sheet['monthly_fee_list'])) {
                    $i = 0;
                    while ($i < count($collection_sheet['monthly_fee_list'])) {
                        $mdata = array();
                        $mdata['fee_account_id'] = $insert_id;
                        $mdata['month'] = $collection_sheet['monthly_fee_list'][$i][$i]['month'];
                        $mdata['allocated_fee'] = $collection_sheet['monthly_fee_list'][$i][$i]['allocated_fee'];
                        $mdata['paid_amount'] = $collection_sheet['monthly_fee_list'][$i][$i]['paid_amount'];
                        $mdata['due_amount'] = $collection_sheet['monthly_fee_list'][$i][$i]['due_amount'];
                        $mdata['year'] = $this->input->post('year', true);
                        $this->db->insert('tbl_month_wise_fee', $mdata);
                        $i++;
                    }
                }

                if (isset($collection_sheet['absent_and_play_truant_fine_list']) && !empty($collection_sheet['absent_and_play_truant_fine_list'])) {
                    $i = 0;
                    while ($i < count($collection_sheet['absent_and_play_truant_fine_list'])) {
                        $mdata = array();
                        $mdata['fee_account_id'] = $insert_id;
                        $mdata['is_absent_fine'] = $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['is_absent_fine'];
                        $mdata['allocated_fine'] = $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['allocated_fine'];
                        $mdata['paid_amount'] = $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['paid_amount'];
                        $mdata['due_amount'] = $collection_sheet['absent_and_play_truant_fine_list'][$i][$i]['due_amount'];
                        $mdata['year'] = $this->input->post('year', true);
                        $this->db->insert('tbl_absent_and_play_truant_fine', $mdata);
                        $i++;
                    }
                }

                $sdata['message'] = "You are Successfully Student Fee Added ! ";
                $this->session->set_userdata($sdata);
                redirect("student_fees/student_fee_add_for_normal");
            } else {
                $sdata['exception'] = "Student Fee Added failed !";
                $this->session->set_userdata($sdata);
                redirect("student_fees/student_fee_add_for_normal");
            }
        } else {
            $data = array();
            $data['title'] = 'Student Fee Collect';
            $data['heading_msg'] = "Student Fee Collect";

            $data['c_class_id'] = $this->session->userdata('c_class_id');
            $data['c_section_id'] = $this->session->userdata('c_section_id');
            $data['c_student_id'] = $this->session->userdata('c_student_id');

            if ($data['c_class_id'] != '' && $data['c_section_id'] != '') {
                $c_class_id = $data['c_class_id'];
                $c_section_id = $data['c_section_id'];
                $data['students'] = $this->db->query("SELECT id,name,student_code,roll_no FROM tbl_student WHERE class_id = '$c_class_id' AND section_id = '$c_section_id' AND status = '1'")->result_array();
            } else {
                $data['students'] = null;
            }


            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $data['maincontent'] = $this->load->view('student_fees/student_fee_add_for_normal', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function getCollectionSheetForNormal()
    {
        $data = array();
        $data['year'] = $this->input->get('year', true);
        $data['class_id'] = $this->input->get('class_id', true);
        $data['section_id'] = $this->input->get('section_id', true);
        $data['student_id'] = $this->input->get('student_id', true);
        $data['amount'] = $this->input->get('amount', true);


        $data['collection_sheet'] = $this->Student_fee->createCollectionSheet($data);
//        echo '<pre>';
//        print_r($data['collection_sheet']);
//        die;
        $date = new DateTime();
        $data['receipt_no'] = $date->getTimestamp();

        $this->load->view('student_fees/fee_collection_sheet_for_normal', $data);
    }











    public function student_scholarship_index()
    {
        $data = array();
        $data['title'] = 'Student Scholarship';
        $data['heading_msg'] = "Student Scholarship";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/student_scholarship_index/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_student_scholarship(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['list'] = $this->Student_fee->get_all_student_scholarship(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/student_scholarship_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function fee_collection_index_for_normal()
    {
        $data = array();
        $data['title'] = 'Student Fee Collection';
        $data['heading_msg'] = "Student Fee Collection";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/fee_collection_index_for_normal/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_student_fee_for_normal(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['fees'] = $this->Student_fee->get_all_student_fee_for_normal(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/fee_collection_index_for_normal', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function class_wise_monthly_fee_index()
    {
        $data = array();
        $data['title'] = 'Monthly Fee Allocation';
        $data['heading_msg'] = "Monthly Fee Allocation";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/class_wise_monthly_fee_index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Student_fee->get_all_monthly_fee_for_normal(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['fees'] = $this->Student_fee->get_all_monthly_fee_for_normal(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/class_wise_monthly_fee_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

  







    public function class_wise_fee_collect_priority_set()
    {
        if ($_POST) {
        }
        $data = array();
        $data['title'] = 'Student Fee Collection Priority Set';
        $data['heading_msg'] = "Student Fee Collection Priority Set";
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
        $data['maincontent'] = $this->load->view('student_fees/class_wise_fee_collect_priority_set', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function class_wise_monthly_fee_add()
    {
        if ($_POST) {
            $data = array();
            $data['class_id'] = $_POST['class_id'];
            $data['year'] = $_POST['year'];
            $data['amount'] = $_POST['amount'];
            $data['resident_amount'] = $_POST['resident_amount'];
            $data['remarks'] = $_POST['remarks'];

            $AlreadyAllocate = $this->Student_fee->checkFeeAlreadyAllocateClassWiseFee($_POST['class_id'], $_POST['year']);
            if (empty($AlreadyAllocate)) {
                $this->db->insert("tbl_class_wise_monthly_fee", $data);
                $sdata['message'] = "Data Added Successfully.";
                $this->session->set_userdata($sdata);
                redirect('student_fees/class_wise_monthly_fee_add');
            } else {
                $sdata['exception'] = "Already exits data for this class.";
                $this->session->set_userdata($sdata);
                redirect('student_fees/class_wise_monthly_fee_add');
            }
        } else {
            $data = array();
            $data['title'] = 'Class Wise Monthly Fee Allocation';
            $data['heading_msg'] = "Class Wise Monthly Fee Allocation";
            $data['action'] = '';
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/class_wise_monthly_fee_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }




//    function normal_fee_collection_delete($id)
//    {
//        $this->db->delete('tbl_student_fee_account', array('id' => $id));
//        $sdata['message'] = "Data Deleted Successfully !";
//        $this->session->set_userdata($sdata);
//        redirect("student_fees/fee_collection_index_for_normal");
//    }

    public function class_wise_fee_delete($id)
    {
        $this->db->delete('tbl_class_wise_monthly_fee', array('id' => $id));
        $sdata['message'] = "Data Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("student_fees/class_wise_monthly_fee_index");
    }

    public function category_wise_amount_delete($id)
    {
        $this->db->delete('tbl_fee_category_wise_amount', array('id' => $id));
        $sdata['message'] = "Data Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("student_fees/category_wise_amount_index");
    }


    public function getSubCategoryByCategoryId()
    {
        $category_id = $this->input->get('category_id', true);
        $data = array();
        $data['fee_sub_category_info'] = $this->db->query("SELECT * FROM tbl_fee_sub_category
           WHERE `category_id` = '$category_id'")->result_array();
        $this->load->view('student_fees/sub_category_list', $data);
    }


    public function fee_allocation_index()
    {
        $data = array();
        $data['title'] = 'Fee Allocation';
        $data['heading_msg'] = "Fee Allocation";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/fee_allocation_index/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_allocated_fee(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['fees'] = $this->Student_fee->get_all_allocated_fee(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/fee_allocation_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }





    public function fee_collection_index()
    {
        if ($_POST) {
            $receipt_no = $_POST['receipt_no'];
            $receipt_details = $this->db->query("SELECT * FROM `tbl_fee_collection` WHERE receipt_no = '$receipt_no'")->result_array();
            if (empty($receipt_details)) {
                $sdata['exception'] = "Collection not found by this (". $receipt_no .") receipt number !";
                $this->session->set_userdata($sdata);
                redirect("student_fees/fee_collection_index");
            } else {
                $id = $receipt_details[0]['id'];
                $this->fee_collection_delete($id);
            }
        }
        $data = array();
        $data['title'] = 'Fee Collection';
        $data['heading_msg'] = "Fee Collection";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/fee_collection_index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Student_fee->get_all_collected_fee(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['fees'] = $this->Student_fee->get_all_collected_fee(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/fee_collection_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }






    public function getSpecialCareStudentReceipt($id)
    {
        $this->load->library('numbertowords');
        $data = array();
        $data['collection_info'] = $this->db->query("SELECT * FROM `tbl_monthly_special_care_fee` WHERE `id` = '$id'")->result_array();
        $student_id = $data['collection_info'][0]['student_id'];
        $data['school_info'] = $this->Admin_login->get_contact_info();
        $data['copy_for'] = "Student Copy";
        $data['student_info'] = $this->db->query("SELECT s.`name`,s.`student_code`,s.`roll_no`,c.`name` AS class,sc.`name` AS section FROM `tbl_student` AS s
INNER JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
INNER JOIN `tbl_section` AS sc ON s.`section_id` = sc.`id`
WHERE s.`id` = '$student_id'")->result_array();
        $this->load->view('student_fees/special_care_fee_receipt', $data);
    }


    public function fee_collection_delete($id)
    {
        if ($this->db->delete('tbl_fee_collection_details', array('collection_id' => $id))) {
            $config = $this->db->query("SELECT is_transport_fee_applicable FROM tbl_config")->result_array();
            $is_transport_fee_applicable = $config[0]['is_transport_fee_applicable'];
            if ($is_transport_fee_applicable == '1') {
                $this->db->delete('tbl_student_monthly_transport_fee', array('collection_id' => $id));
            }

            $this->db->delete('tbl_fee_collection', array('id' => $id));
            $sdata['message'] = "Data Deleted Successfully !";
            $this->session->set_userdata($sdata);
            redirect("student_fees/fee_collection_index");
        } else {
            $sdata['exception'] = "Data Doesn't Delete !";
            $this->session->set_userdata($sdata);
            redirect("student_fees/fee_collection_index");
        }
    }


    public function fee_collection_add()
    {
        if ($_POST) {
            $class_id = $this->input->post('class_id', true);
            $section_id = $this->input->post('section_id', true);
            $student_id = $this->input->post('student_id', true);

            //echo '<pre>';
            //	print_r($_POST);
            //die;

            $cdata['c_class_id'] = $class_id;
            $cdata['c_section_id'] = $section_id;
            $cdata['c_student_id'] = $student_id;
            $this->session->set_userdata($cdata);

            $year = $this->input->post('year', true);
            $collection_month = $this->input->post('collection_month', true);
            $mode_of_pay = $this->input->post('mode_of_pay', true);
            $do_you_want_receipt = $this->input->post('do_you_want_receipt', true);
            $do_you_want_send_sms = $this->input->post('do_you_want_send_sms', true);
            $num_of_row = $this->input->post('num_of_row', true);
            $transport_fee_row = $this->input->post('transport_fee_row', true);

            $num_of_row = $num_of_row - $transport_fee_row;

            /*$isAlreadyPaidInfo = $this->Student_fee->check_already_fee_paid_info($student_id, $month, $year);
            if (!$isAlreadyPaidInfo) {
                $sdata['exception'] = "This payment already done of this month !";
                $this->session->set_userdata($sdata);
                redirect("student_fees/fee_collection_add");
            }*/
            $is_resident_transaction = 0;
            $is_resident_info = $this->db->query("SELECT id FROM tbl_student_resident_info WHERE student_id = '$student_id' AND year = '$year'")->result_array();
            if (!empty($is_resident_info)) {
                $is_resident_transaction = 1;
            }

            //echo $is_resident_transaction; die;
            $group_id = 0;
            $shift_id = 0;
            $guardian_mobile = "0";
            $student_info = $this->db->query("SELECT s.id,s.shift_id,s.group,s.guardian_mobile FROM tbl_student AS s WHERE s.id = '$student_id'")->result_array();
            if (!empty($student_info)) {
                $group_id = $student_info[0]['group'];
                $shift_id = $student_info[0]['shift_id'];
                $guardian_mobile = $student_info[0]['guardian_mobile'];
            }

            $mdata = array();
            $mdata['class_id'] = $class_id;
            $mdata['section_id'] = $section_id;
            $mdata['student_id'] = $student_id;
            $mdata['year'] = $year;
            $mdata['month'] = $collection_month; //its collection month
            $mdata['mode_of_pay'] = $mode_of_pay;
            $mdata['receipt_no'] = $this->input->post('receipt_no', true);
            $mdata['date'] = date('Y-m-d');
            $mdata['is_resident_transaction'] = $is_resident_transaction;
            $mdata['group_id'] = $group_id;
            $mdata['shift_id'] = $shift_id;
            $mdata['total_discount'] = $this->input->post('total_discount', true);
            $mdata['total_paid_amount'] = $this->input->post('total_paid_amount', true);
            $mdata['entry_date'] = date('Y-m-d H:i:s');
            if ($this->input->post('do_you_collect_late_fee')) {
                $mdata['do_you_collect_late_fee'] = 'Y';
            } else {
                $mdata['do_you_collect_late_fee'] = 'N';
            }
            if ($this->db->insert('tbl_fee_collection', $mdata)) {
                $collection_id = $this->db->insert_id();

                if ($this->input->post('do_you_collect_late_fee')) {
                    $late_fee_data = array();
                    $late_fee_data['student_id'] = $student_id;
                    $late_fee_data['collection_id'] = $collection_id;
                    $late_fee_data['month'] = $collection_month;
                    $late_fee_data['year'] = $year;
                    $late_fee_data['amount'] = $this->input->post('late_fee', true);
                    $this->db->insert('tbl_fee_late_fine', $late_fee_data);
                }

                $i = 1;
                while ($i <= $num_of_row) {
                    if ($this->input->post('is_selected_' . $i)) {
                        $cdata = array();
                        $cdata['collection_id'] = $collection_id;
                        $cdata['class_id'] = $class_id;
                        $cdata['section_id'] = $section_id;
                        $cdata['student_id'] = $student_id;
                        $cdata['month'] = $this->input->post('month_' . $i);
                        $cdata['year'] = $year;
                        $cdata['category_id'] = $this->input->post('category_id_' . $i);
                        $cdata['sub_category_id'] = $this->input->post('sub_category_id_' . $i);
                        $cdata['fee_type'] = $this->input->post('fee_type_id_' . $i);
                        $cdata['waiver_amount'] = $this->input->post('waiver_amount_' . $i);
                        $cdata['actual_amount'] = $this->input->post('actual_amount_' . $i);
                        $cdata['discount_amount'] = $this->input->post('discount_amount_' . $i);
                        $cdata['paid_amount'] = $this->input->post('paid_amount_' . $i);
                        $cdata['entry_date'] = date('Y-m-d H:i:s');
                        $this->db->insert('tbl_fee_collection_details', $cdata);
                    }
                    $i++;
                }
            }


            //transport fee save
            $gen_config = $this->db->query("SELECT is_transport_fee_applicable FROM `tbl_config`")->result_array();
            if (empty($gen_config)) {
                $is_transport_fee_applicable = 0;
            } else {
                $is_transport_fee_applicable = $gen_config[0]['is_transport_fee_applicable'];
            }
            if ($transport_fee_row > 0 && $is_transport_fee_applicable == '1') {
                $k = 0;
                while ($k < $transport_fee_row) {
                    if ($this->input->post('transport_fee_selected_' . $k)) {
                        $transport_data = array();
                        $transport_data['collection_id'] = $collection_id;
                        $transport_data['student_id'] = $student_id;
                        $transport_data['month'] = $this->input->post('transport_fee_month_' . $k);
                        $transport_data['year'] = $year;
                        $transport_data['amount'] = $this->input->post('paid_transport_fee_' . $k);
                        $transport_data['date'] = date('Y-m-d H:i:s');
                        $this->db->insert('tbl_student_monthly_transport_fee', $transport_data);
                    }
                    $k++;
                }
            }

            //transport fee save end

            if ($do_you_want_send_sms) {
                //sms send
                if ($this->Admin_login->validate_mobile_number($guardian_mobile)) {
                    $contact_info = $this->db->query("SELECT * FROM `tbl_contact_info`")->result_array();
                    $school_name = $contact_info[0]['school_name'];
                    $total_paid_amount = $this->input->post('total_paid_amount', true);
                    $msg = "Your son/daughter tution fee has deposited Tk. $total_paid_amount. Thank you - Principal, $school_name";
                    if ($total_paid_amount > 0) {
                        $single_num = "";
                        $single_num = $this->Admin_login->generateNumberlist($single_num, $guardian_mobile);

                        $sms_info = array();
                        $sms_info['message'] = $msg;
                        $sms_info['numbers'] = $single_num;
                        $sms_info['is_bangla_sms'] = 0;
                        $status = $this->Message->sms_send($sms_info);
                    }
                }
                //sms send
            }


            if ($do_you_want_receipt) {
                redirect("student_fees/getStudentReceipt/" . $collection_id . "/S");
            } else {
                $sdata['message'] = "Data added successfully !";
                $this->session->set_userdata($sdata);
                redirect("student_fees/fee_collection_add");
            }
        } else {
            $data = array();
            $data['title'] = 'Fee Collection';
            $data['heading_msg'] = "Fee Collection";
            $data['action'] = '';
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();

            $data['c_class_id'] = $this->session->userdata('c_class_id');
            $data['c_section_id'] = $this->session->userdata('c_section_id');
            $data['c_student_id'] = $this->session->userdata('c_student_id');

            if ($data['c_class_id'] != '' && $data['c_section_id'] != '') {
                $c_class_id = $data['c_class_id'];
                $c_section_id = $data['c_section_id'];
                $data['students'] = $this->db->query("SELECT id,name,student_code FROM tbl_student WHERE class_id = '$c_class_id' AND section_id = '$c_section_id' AND status = '1'")->result_array();
            } else {
                $data['students'] = null;
            }
            //echo '<pre>';
            //print_r($data);
            //die;

            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/fee_collection_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }






    public function getResidentFeeReceipt($student_id, $date)
    {
        $data = array();
        $data['school_info'] = $this->Admin_login->get_contact_info();
        $data['copy_for'] = "Student Copy";
        $data['student_info'] = $this->db->query("SELECT s.`name`,s.`student_code`,s.`roll_no`,c.`name` AS class,sc.`name` AS section FROM `tbl_student` AS s
INNER JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
INNER JOIN `tbl_section` AS sc ON s.`section_id` = sc.`id`
WHERE s.`id` = '$student_id'")->result_array();

        $data['collection_info'] = $this->db->query("SELECT * FROM `tbl_monthly_resident_fee` WHERE `student_id` = '$student_id' AND `date` = '$date'")->result_array();

        $this->load->view('student_fees/resident_fee_receipt', $data);
    }


    public function resident_fee_add()
    {
        if ($_POST) {
            $class_id = $this->input->post('class_id', true);
            $section_id = $this->input->post('section_id', true);
            $student_id = $this->input->post('student_id', true);

            $cdata['c_class_id'] = $class_id;
            $cdata['c_section_id'] = $section_id;
            $cdata['c_student_id'] = $student_id;
            $this->session->set_userdata($cdata);

            $data = array();
            $data['class_id'] = $this->input->post('class_id', true);
            $data['section_id'] = $this->input->post('section_id', true);
            $data['student_id'] = $this->input->post('student_id', true);
            $data['year'] = $this->input->post('year', true);
            $data['month'] = $this->input->post('month', true);
            $data['amount'] = $this->input->post('amount', true);
            $data['date'] = date('Y-m-d');
            $date = new DateTime();
            $data['receipt_no'] = $date->getTimestamp();
            $this->db->insert('tbl_monthly_resident_fee', $data);
            redirect("student_fees/getResidentFeeReceipt/" . $student_id . "/".date('Y-m-d'));
        } else {
            $data = array();
            $data['title'] = 'Resident Fee Collection';
            $data['heading_msg'] = "Resident Fee Collection";
            $data['action'] = '';
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();

            $data['c_class_id'] = $this->session->userdata('c_class_id');
            $data['c_section_id'] = $this->session->userdata('c_section_id');
            $data['c_student_id'] = $this->session->userdata('c_student_id');

            if ($data['c_class_id'] != '' && $data['c_section_id'] != '') {
                $c_class_id = $data['c_class_id'];
                $c_section_id = $data['c_section_id'];
                $data['students'] = $this->db->query("SELECT id,name,student_code FROM tbl_student WHERE class_id = '$c_class_id' AND section_id = '$c_section_id' AND status = '1'")->result_array();
            } else {
                $data['students'] = null;
            }

            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/resident_fee_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }




    public function getStudentReceipt($collection_id, $copy_for)
    {
        $this->load->library('numbertowords');
        //echo 'fgg';
        //die;
        $data = array();
        $data['title'] = "Student Receipt";
        $data['school_info'] = $this->Admin_login->get_contact_info();
        $data['collection_sheet_info'] = $this->Student_fee->get_all_collection_sheet_info($collection_id);
        $collection_sheet_details = $this->Student_fee->get_all_collection_sheet_details_single($collection_id);
		$data['collection_sheet_details'] = $collection_sheet_details;
        $late_fee = $this->db->query("SELECT * FROM tbl_fee_late_fine WHERE collection_id = '$collection_id'")->result_array();
        // echo '<pre>';
        // print_r($collection_sheet_details);
        // die;
        if (!empty($late_fee)) {
            $data['late_fee'] = $late_fee[0]['amount'];
        } else {
            $data['late_fee'] = 0;
        }

        if ($copy_for == 'S') {
            $data['copy_for'] = "Student Copy";
        } else {
            $data['copy_for'] = "School Copy";
        }

        $config = $this->db->query("SELECT is_transport_fee_applicable FROM tbl_config")->result_array();
        $is_transport_fee_applicable = $config[0]['is_transport_fee_applicable'];
        if ($is_transport_fee_applicable == '1') {
            $data['transport_fees'] = $this->db->query("SELECT * FROM tbl_student_monthly_transport_fee WHERE collection_id = '$collection_id'")->result_array();
        } else {
            $data['transport_fees'] = null;
        }

        $data['signature'] = $this->Admin_login->getSignatureByAccessCode('FR'); //FR= Fee receipt
        $general_config = $this->Config_general->get_general_configurations('general');
        if (isset($general_config->receipt_design) && $general_config->receipt_design == '0') {
            $this->load->view('student_fees/fee_receipt', $data);
        } else {
            $this->load->view('student_fees/fee_receipt_1', $data);
        }
    }

    public function getStudentByClassAndSection()
    {
        $class_id = $this->input->get('class_id', true);
        $section_id = $this->input->get('section_id', true);
        $data = array();
        $data['students'] = $this->db->query("SELECT id,name,roll_no,student_code FROM tbl_student WHERE `class_id` = '$class_id' AND section_id = '$section_id' AND status = '1' ORDER BY ABS(roll_no)")->result_array();
        $this->load->view('student_fees/student_list_for_combo', $data);
    }

    public function getStudentByClassAndSectionConsiderYear()
    {
        $class_id = $this->input->get('class_id', true);
        $section_id = $this->input->get('section_id', true);
        $year = $this->input->get('year', true);
        $data = array();
        if ($year == 2020) {
            $data['students'] = $this->db->query("SELECT id,name,roll_no,student_code FROM tbl_student WHERE `class_id` = '$class_id'
                                                 AND section_id = '$section_id' AND status = '1' ORDER BY ABS(roll_no)")->result_array();
        } else {
            $data['students'] = $this->db->query("SELECT s.id,s.student_code,mt.name,mt.old_roll_no as roll_no FROM tbl_student_data_migrate_history AS mt
                                                INNER JOIN tbl_student AS s ON s.id = mt.student_id
                                                 WHERE mt.old_year = '$year' AND mt.old_class_id = '$class_id' AND mt.old_section_id = '$section_id' ORDER BY ABS(mt.old_roll_no)")->result_array();
        }

        $this->load->view('student_fees/student_list_for_combo', $data);
    }






    public function student_fee_report()
    {
        $data = array();
        if ($_POST) {
            $student_id = $this->input->post('student_id', true);
            $class_id = $this->input->post('class_id', true);
            $section_id = $this->input->post('section_id', true);
            $year = $this->input->post('year', true);
            $month = $this->input->post('month', true);
            $ClassName = $this->db->query("SELECT * FROM `tbl_class` WHERE id='$class_id'")->result_array();
            $SectionName = $this->db->query("SELECT * FROM `tbl_section` WHERE id='$section_id'")->result_array();
            $SchoolInfo = $this->db->query("SELECT `school_name`,`eiin_number` FROM `tbl_contact_info`")->result_array();
            $Info = array();
            $Info['ClassName'] = $ClassName[0]['name'];
            $Info['SectionName'] = $SectionName[0]['name'];
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['month'] = $month;
            $Info['year'] = $year;
            $data['HeaderInfo'] = $Info;
            $data['fee_list'] = $this->Student_fee->get_student_fee_report($student_id, $class_id, $section_id, $month, $year);
        }
        $data['title'] = 'Student Fee Report';
        $data['heading_msg'] = "Student Fee Report";
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['top_menu'] = $this->load->view('student_fees/student_fee_menu', '', true);
        $data['ClassList'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
        $data['SectionList'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
        $data['maincontent'] = $this->load->view('student_fees/student_fee_report', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function view_fee_payment_history($student_id)
    {
        $data = array();
        $data['title'] = 'Fee Payment History';
        $data['heading_msg'] = "Fee Payment History";
        $cond = array();
        $cond['student_id'] = $student_id;
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fees/view_fee_payment_history/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_collected_fee(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['payment_history'] = $this->Student_fee->get_all_collected_fee(15, (int)$this->uri->segment(4), $cond);
//        echo '<pre>';
//        print_r($data['payment_history']);
//        die;
        $data['counter'] = (int)$this->uri->segment(4);
        $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
        $data['fee_module_type'] = $config[0]['fee_module_type'];
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_fees/fee_payment_history', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
