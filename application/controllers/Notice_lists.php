<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Notice_lists extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Admin_login','common/insert_model', 'common/edit_model', 'common/custom_methods_model'));
        $this->load->library(array('session'));
        $this->lang->load('message', 'english');
        date_default_timezone_set("Asia/Dhaka");
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

public function index()
{
    $data = array();
    $data['title'] = 'Notice';
    $data['heading_msg'] = "Notice";
    $data['is_show_button'] = "add";
    $cond = array();
    $this->load->library('pagination');
    $config['base_url'] = site_url('notice_lists/index/');
    $config['per_page'] = 20;
    $config['total_rows'] = count($this->Admin_login->get_all_notice(0, 0, $cond));
    $this->pagination->initialize($config);
    $data['notices'] = $this->Admin_login->get_all_notice(20, (int)$this->uri->segment(3), $cond);
    $data['counter'] = (int)$this->uri->segment(3);
	$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
    $data['maincontent'] = $this->load->view('notices/index', $data, true);
    $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
}

public function add()
{
    if ($_POST) {
        $this->load->library('upload');
        $config['upload_path'] = MEDIA_FOLDER . '/notice/';
        $config['allowed_types'] = 'pdf|doc|png|JPEG|jpeg|jpg|docx|xls';
        $config['max_size'] = '60000';
        $this->upload->initialize($config);
        $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
        foreach ($_FILES as $field => $file) {
            if ($file['error'] == 0) {
                $new_file_name = "Notice_" . time() . "_" . date('Y-m-d') . "." . $extension;
                if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/notice/" . $new_file_name)) {
                    $data = array();
                    $data['title'] = $this->input->post('txtTitle', true);
                    $data['date'] = $this->input->post('txtDate', true);
                    $data['url'] = $new_file_name;
                    $this->Admin_login->add_notice($data);
                    $sdata['message'] = $this->lang->line('add_success_message');
                    $this->session->set_userdata($sdata);
                    redirect("notice_lists/index");
                } else {
                    $sdata['exception'] = "Sorry Notice Doesn't Upload !" . $this->upload->display_errors();
                    $this->session->set_userdata($sdata);
                    redirect("notice_lists/add");
                }
            } else {
                $sdata['exception'] = "Sorry Notice Doesn't Upload !";
                $this->session->set_userdata($sdata);
                redirect("notice_lists/add");
            }
        }
    } else {
        $data = array();
        $data['title'] = 'Notice';
        $data['heading_msg'] = "Add New Notice";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['top_menu'] = $this->load->view('dashboard/dashboard_menu', '', true);
        $data['maincontent'] = $this->load->view('notices/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}

public function delete($id)
{
    $notice_info = $this->Admin_login->get_notice_info_by_id($id);

    $file = $notice_info[0]['url'];
    if (!empty($file)) {
        $filedel = PUBPATH . MEDIA_FOLDER . '/notice/' . $file;
        if (unlink($filedel)) {
            $this->Admin_login->delete_notice_info_by_id($id);
        } else {
            $this->Admin_login->delete_notice_info_by_id($id);
        }
    } else {
        $this->Admin_login->delete_notice_info_by_id($id);
    }

    $sdata['message'] = $this->lang->line('delete_success_message');
    $this->session->set_userdata($sdata);
    redirect("notice_lists/index");
}

}

?>
