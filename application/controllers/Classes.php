<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Classes extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('students') . ' ' .$this->lang->line('class');
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['class'] = $this->db->query("SELECT * FROM tbl_class ORDER BY ABS(student_code_short_form)")->result_array();
        $data['maincontent'] = $this->load->view('class/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
			$name = $_POST['name'];
			$check = $this->db->query("SELECT * FROM tbl_class WHERE name = '$name'")->row();
			if(!empty($check)){
				$sdata['exception'] = "This class is already exists";
				$this->session->set_userdata($sdata);
				redirect('classes/index');
			}
            $data = array();
            $data['name'] = $_POST['name'];
			$data['name_bangla'] = $_POST['name_bangla'];
            $data['student_code_short_form'] = $_POST['student_code_short_form'];
            $this->db->insert("tbl_class", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('classes/add');
        }
        $data = array();
        $data['title'] = $this->lang->line('class') . ' ' . $this->lang->line('add');
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('class/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

	public function edit($id=null)
	{
		if($_POST){
			$data = array();
			$data['id'] = $_POST['id'];
			$data['name'] = $_POST['name'];
			$data['name_bangla'] = $_POST['name_bangla'];
			$data['student_code_short_form'] = $_POST['student_code_short_form'];
			$this->db->where('id', $data['id']);
			$this->db->update('tbl_class', $data);
			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect('classes/index');
		}
		$data = array();
		$data['title'] = $this->lang->line('class') . ' ' . $this->lang->line('edit');
		$data['is_show_button'] = "index";
		$data['class_info'] = $this->db->query("SELECT * FROM tbl_class WHERE id = '$id'")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('class/edit', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

    public function delete($id)
    {
        $class_info = $this->db->where('class_id', $id)->get('tbl_student')->result_array();
        if (!empty($class_info)) {
            $sdata['exception'] = $this->lang->line('delete_error_message') . "Error: Student Depended Data Found !";
            $this->session->set_userdata($sdata);
            redirect("classes/index");
        }
        $this->db->query("DELETE FROM tbl_class WHERE `id` = '$id'");
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('classes/index');
    }
}
