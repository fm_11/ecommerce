<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Phonebook_lists extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function view_category_phoneBook_list()
    {
        $data = array();
        if ($_POST) {
            $CategoryType = $this->input->post('CategoryType', true);
            $CategoryInfo = $this->Message->GetCategoryInfo($CategoryType);
            $data['CategoryInfo'] = $CategoryInfo;
            /* echo '<pre>';
            print_r($CategoryInfo);
            die(); */
            $data['templates'] = $this->db->query("SELECT * FROM tbl_message_template")->result_array();
        }
        $data['title'] = 'Send SMS From Phone Book';
        $data['heading_msg'] = "Send SMS From Phone Book";
        $data['category'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('phonebook_lists/view_category_phoneBook_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
