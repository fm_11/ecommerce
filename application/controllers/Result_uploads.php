<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Result_uploads extends CI_Controller
{

     public $SOFTWARE_START_YEAR = '';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Student'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    function index()
    {
        $data = array();
        $data['title'] = 'Student Result';
        $data['heading_msg'] = "Student Result Upload";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('result_uploads/index/');
        $config['per_page'] = 10;
        $config['total_rows'] = count($this->Student->get_all_uploaded_result(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['student_results'] = $this->Student->get_all_uploaded_result(10, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('result_uploads/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function updateMsgStatusResultUploadStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }
        $this->Student->update_uploaded_result_status($data);
        if ($status == 0) {
            echo '<a class="badge badge-success mb-1" title="Publish" href="#" onclick="msgStatusUpdate(' . $id . ',1)">Publish</a>';
        } else {
            echo '<a class="badge badge-warning mb-1" title="Unpublish" href="#" onclick="msgStatusUpdate(' . $id . ',0)">Unpublish</a>';
        }
    }

    function add()
    {
        //echo MEDIA_FOLDER; die;
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/result/';
            $config['allowed_types'] = 'pdf|doc|docx|xls';
            $config['max_size'] = '60000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "Result_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/result/" . $new_file_name)) {
                        $data = array();
                        $data['result_type'] = $this->input->post('txtResultType', true);
                        $data['date'] = $this->input->post('txtDate', true);
                        $data['location'] = $new_file_name;
                        $data['status'] = 0;
                        $data['ramarks'] = $this->input->post('txtRemarks', true);
                        $this->Student->add_upload_result($data);
                        $sdata['message'] = "You are Successfully Result Uploaded ! ";
                        $this->session->set_userdata($sdata);
                        redirect("students/add");
                    } else {
                        $sdata['exception'] = "Sorry Result Doesn't Upload !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("result_uploads/add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Result Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("result_uploads/add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Student Result';
            $data['heading_msg'] = "Student Result Upload";
            $data['action'] = '';
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('result_uploads/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    function delete($id)
    {
        $uploaded_result_info = $this->Student->get_uploaded_result_info_by_result_id($id);
        $file = $uploaded_result_info[0]['location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/result/' . $file;
            if (unlink($filedel)) {
                $this->Student->delete_uploaded_result_info_by_result_id($id);
            } else {
                $this->Student->delete_uploaded_result_info_by_result_id($id);
            }
        } else {
            $this->Student->delete_uploaded_result_info_by_result_id($id);
        }

        $sdata['message'] = "Student Uploaded Result Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("result_uploads/index");
    }
  }
