<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Teacher_job_cards extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Students_report', 'Student', 'Timekeeping'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Teacher/Staff Job Card';
		$data['heading_msg'] = "Teacher/Staff Job Card";
		if ($_POST) {
			$year = $this->input->post("year");
			$month = $this->input->post("month");
			$teacher_id = $this->input->post("teacher_id");
			$data['year'] = $year;
			$data['month'] = $month;
			$data['teacher_id'] = $teacher_id;
			//echo $teacher_id; die;
            if($month <= 9){
				$month = '0' . $month;
			}
			$form_date = $year . "-" . $month . "-01";
			$to_date = $year . "-" . $month . "-" . cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$cond = array();
			$cond['teacher_id'] = $teacher_id;
			$cond['form_date'] = $form_date;
			$cond['to_date'] = $to_date;
			$cond['number_of_days'] = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$data['time_keeping_info'] = $this->Timekeeping->get_teacher_staff_attendance_details($cond);
			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;
			$data['report'] = $this->load->view('teacher_job_cards/report', $data, true);
		}

		if (isset($_POST['pdf_download'])) {
			$data['is_pdf'] = 1;
			//Dom PDF
			$this->load->library('mydompdf');
			$html = $this->load->view('teacher_job_cards/report', $data, true);
			$this->mydompdf->createPDF($html, 'TeacherStaffJobCard', true, 'A4', 'portrait');
			//Dom PDF
		}
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['teachers'] = $this->db->query("SELECT t.`id`,t.`name`,t.`teacher_code` FROM `tbl_teacher` AS t
											LEFT JOIN `tbl_teacher_post` AS p ON p.`id` = t.`post`
											WHERE t.`status` = '1'
											ORDER BY p.`shorting_order`")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('teacher_job_cards/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}
}
