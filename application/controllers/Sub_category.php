<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sub_category extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('E_commerce'));
        $this->load->library(array('session'));

        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('sub').' '.$this->lang->line('category');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $sdata['name'] = $name;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
        } else {
            $name = $this->session->userdata('name');
            $cond['name'] = $name;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('sub_category/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->E_commerce->get_all_sub_category(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['subcategory'] = $this->E_commerce->get_all_sub_category(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('sub_category/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
        $data = array();
        $name=$this->input->post('name');
        $display_sub_menu=$this->input->post('display_sub_menu');
        $data['category_id'] = $_POST['category_id'];
        $data['name']=$name;
        $data['display_sub_menu']='1';
        $data['order_no']=$this->input->post('order_no');
        if($this->E_commerce->checkifexist_subcategory_by_name($name))
        {
          $sdata['exception'] = "This name '".$name."' already exist.";
          $this->session->set_userdata($sdata);
          redirect("sub_category/add");
        }
        if ($this->E_commerce->add_subcategory($data)) {
            $sdata['message'] = "Information Successfully Added";
            $this->session->set_userdata($sdata);
            redirect("sub_category/index");
        } else {
            $sdata['exception'] = "Information could not add";
            $this->session->set_userdata($sdata);
            redirect("sub_category/add");
        }
        $this->db->insert("tbl_sub_category", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('sub_category/index');
      }
      $data = array();
      $data['heading_msg'] = $data['title'] = $this->lang->line('sub').' '.$this->lang->line('category');
      $data['action'] = 'add';
      $data['is_show_button'] = "index";
      $data['category'] = $this->db->query("SELECT * FROM tbl_category ")->result_array();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('sub_category/add', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
      if ($_POST) {
        $data = array();
        if($this->E_commerce->checkifexist_update_subcategory_by_name($_POST['name'],$_POST['id']))
        {
          $sdata['exception'] = "This name '".$_POST['name']."' already exist.";
          $this->session->set_userdata($sdata);
          redirect("sub_category/edit/".$_POST['id']);
        }
        $data['id'] = $_POST['id'];
        $data['name'] = $_POST['name'];
        $data['category_id'] = $_POST['category_id'];
        $data['order_no']=$this->input->post('order_no');
        $this->db->where('id', $data['id']);
        $this->db->update("tbl_sub_category", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('sub_category/index');
      }
      $data = array();
      $data['heading_msg'] = $data['title'] = $this->lang->line('sub').' '.$this->lang->line('category');
      $data['action'] = 'edit/' . $id;
      $data['is_show_button'] = "index";
      $data['row'] = $this->E_commerce->read_subcategory($id);
      $data['category'] = $this->db->query("SELECT * FROM `tbl_category`")->result_array();
      $data['sub_category'] = $this->db->query("SELECT * FROM `tbl_sub_category` WHERE id = '$id'")->result_array();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('sub_category/edit', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->db->delete('tbl_sub_category', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_sucess_message');
        $this->session->set_userdata($sdata);
        redirect('sub_category/index');
    }

    public function updateCaseStatus()
    {
        $status = $this->input->get('display_sub_menu', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['display_sub_menu'] = 0;
        } else {
            $data['display_sub_menu'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_sub_category', $data);
        if ($status == 0) {
            echo '<a class="deleteTag" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a class="deleteTag" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }

  }

 ?>
