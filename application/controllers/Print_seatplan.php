<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Print_seatplan extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Admin_login'));

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}


	public function index()
	{
		if ($_POST) {
			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$group = $this->input->post('txtGroup', true);
			$exam = $this->input->post('txtExam', true);

			$data = array();
			$data['exam'] = $exam;
			$data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['group'] = $group;
			$data['class_shift_section_id'] = $class_shift_section_id;


			$data['title'] = 'Exam Seat Plan Card';
			$data['heading_msg'] = "Exam Seat Plan Card";
			$data['list'] = $this->db->query("SELECT s.`id`,s.`name`,s.`roll_no`,s.`student_code`,s.father_name,s.mother_name,c.`name` AS class_name,sc.`name` AS section_name, g.`name` AS group_name,
s.`photo`
FROM `tbl_student` AS s
LEFT JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
LEFT JOIN `tbl_section` AS sc ON s.`section_id` = sc.`id`
LEFT JOIN `tbl_student_group` AS g ON s.`group` = g.`id`
WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`shift_id` = '$shift_id'
AND s.`group` =  '$group' AND s.`status` = '1' ORDER BY ABS(s.`roll_no`);")->result_array();
			$data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();

			$is_pdf_request = $this->input->post('is_pdf_request', true);

			if (isset($is_pdf_request) && $is_pdf_request == 'Download') {
				//echo 'gggg'; die;
				$data['is_pdf_request'] = $is_pdf_request;
				// Load all views as normal
				$html = $this->load->view('seat_plan/student_seat_plan_print', $data, true);
				$pdfFilePath = "Exam_Seat_Plan.pdf";
				$this->load->library('m_pdf');
				//generate the PDF from the given html
				$this->m_pdf->pdf->autoScriptToLang = true;
				$this->m_pdf->pdf->WriteHTML($html);
				//download it.
				$this->m_pdf->pdf->Output($pdfFilePath, "D");
			} else {
				$data['is_pdf_request'] = $is_pdf_request;
				$this->load->view('seat_plan/student_seat_plan_print', $data);
			}
		} else {
			$data = array();
			$data['title'] = 'Exam Seat Plan Card';
			$data['heading_msg'] = "Exam Seat Plan Card";
			$software_current_year = $this->Admin_login->getCurrentSystemYear();
			$data['exam'] = $this->db->query("SELECT * FROM tbl_exam WHERE year = '$software_current_year'")->result_array();
			$data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('seat_plan/student_seat_plan_form', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}
}
