<?php

class User_role_wise_privileges extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->model(array('User_role_wise_privilege'));
        $this->load->library(array('session'));
    }


    public function index($role_id)
    {
        if ($role_id == 1) {
            $sdata['exception'] = "Super admin has been permited for all features.";
            $this->session->set_userdata($sdata);
            redirect("user_roles/index");
        }

        if ($role_id == '') {
            $sdata['exception'] = "Please select a role.";
            $this->session->set_userdata($sdata);
            redirect("user_roles/index");
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = 'Role wise privileges [' . $this->User_role_wise_privilege->get_role_name_by_id($role_id) . ' ]';
        $data['role_privilege_resources'] = $this->User_role_wise_privilege->get_privileged_resources($role_id);
        $data['user_resources_array'] = $this->User_role_wise_privilege->get_all_resources_array();
        $data['role_id'] = $role_id;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('user_role_wise_privileges/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        $this->_prepare_validation();
        if ($_POST) {
            $data = $this->_get_posted_data();
//            echo '<pre>';
//            print_r($data);
//            die;
            //if ($this->form_validation->run() === true) {
                if ($this->User_role_wise_privilege->add($data)) {
                    $sdata['message'] = $this->lang->line('add_success_message');
                    $this->session->set_userdata($sdata);
                    redirect("user_roles/index");
                }
            //}
        }
    }

    public function _prepare_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('role_id', 'Role anme', 'trim|required|xss_clean|is_natural_no_zero');
        $this->form_validation->set_rules('data[]', 'Action', 'xss_clean');
    }

    public function _get_posted_data()
    {
        $data = array();
        $user_resources = $this->User_role_wise_privilege->get_all_resources_array();
        $i = 0;
        foreach ($user_resources as $rows1) {
            foreach ($rows1 as $rows2) {
                $entity = 0;
                foreach ($rows2 as $key3 => $rows3) {
                    foreach ($rows3 as $key4 => $rows4) {
                        foreach ($rows4 as $key5 => $rows5) {
                            foreach ($rows5 as $rows6) {
                                if (isset($_POST['data'][$entity]["$key4"]["$key5"])) {
                                    $data['resources'][$i]['controller'] = $key4;
                                    $data['resources'][$i]['action'] = $rows6['name'];
                                    $data['resources'][$i]['role_id'] = $_POST['role_id'];
                                }
                                $i++;
                            }
                        }
                    }
                    $entity++;
                }
            }
        }
        $data['role_id'] = $_POST['role_id'];
        return $data;
    }
}
