<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Teacher_sections extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Teacher Section';
        $data['heading_msg'] = "Teacher Section";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['sections'] = $this->db->query("SELECT * FROM tbl_teacher_section")->result_array();
        $data['maincontent'] = $this->load->view('teacher_sections/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $this->db->insert("tbl_teacher_section", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('teacher_sections/index');
        }
        $data = array();
        $data['title'] = 'Add Teacher Section';
        $data['heading_msg'] = "Add Teacher Section";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('teacher_sections/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_teacher_section', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('teacher_sections/index');
        }
        $data = array();
        $data['title'] = 'Update Teacher Section';
        $data['heading_msg'] = "Update Teacher Section";
        $data['is_show_button'] = "index";
        $data['sections'] = $this->db->query("SELECT * FROM `tbl_teacher_section` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('teacher_sections/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
	    $teacher_info = $this->db->where('section', $id)->get('tbl_teacher')->result_array();
	    if (!empty($teacher_info)) {
		  $sdata['exception'] =  $this->lang->line('delete_success_message') . " (Teacher Depended Data Found)";
		  $this->session->set_userdata($sdata);
		  redirect("teacher_sections/index");
	    }
        $this->db->delete('tbl_teacher_section', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('teacher_sections/index');
    }
}
