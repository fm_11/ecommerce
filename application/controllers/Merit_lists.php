
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Merit_lists extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Result Merit List';
        $data['heading_msg'] = 'Result Merit List';
        if ($_POST) {
			$report_type = $_POST['report_type'];
			$year = $_POST['year'];
			$exam_id = $_POST['exam_id'];
			$data['year'] = $year;
			$data['report_type'] = $report_type;
			$data['exam_id'] = $exam_id;

			$sdata['merit_report_report_type'] = $report_type;
			$this->session->set_userdata($sdata);

			$where = "";
			$order_by = "";
			if($report_type == 'cw'){//class wise
				$class_id = $_POST['class_id'];
				$data['class_id'] = $class_id;
				$where = " AND r.`class_id` = '$class_id' ";
				$order_by = " ORDER BY r.`class_position` = 0,r.`class_position` ";

				$class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
				$data['title'] = 'Result Merit List for ' . $class_info[0]['name'];

			}else if($report_type == 'sw'){ //shift wise
				$class_id = $_POST['class_id'];
				$shift_id = $_POST['shift_id'];
				$data['class_id'] = $class_id;
				$data['shift_id'] = $shift_id;
				$where = " AND r.`class_id` = '$class_id' AND r.`shift` = '$shift_id' ";
				$order_by = " ORDER BY r.`shift_position` = 0,r.`shift_position` ";

				$class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
				$shift_info = $this->db->query("SELECT name FROM tbl_shift WHERE id ='$shift_id'")->result_array();
				$data['title'] = 'Result Merit List for ' . $class_info[0]['name'] . '-' .  $shift_info[0]['name'];

			}else if($report_type == 'secw'){
				$class_shift_section_id = $_POST['class_shift_section_id'];
				$data['class_shift_section_id'] = $class_shift_section_id;
				$class_shift_section_arr = explode("-", $class_shift_section_id);
				$class_id  = $class_shift_section_arr[0];
				$shift_id = $class_shift_section_arr[1];
				$section_id = $class_shift_section_arr[2];
				$where = " AND r.`class_id` = '$class_id' AND r.`section_id` = '$section_id'  AND r.`shift` = '$shift_id' ";
				$order_by = " ORDER BY r.`section_position` = 0,r.`section_position` ";

				$class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
				$shift_info = $this->db->query("SELECT name FROM tbl_shift WHERE id ='$shift_id'")->result_array();
				$section_info = $this->db->query("SELECT name FROM tbl_section WHERE id ='$section_id'")->result_array();
				$data['title'] = 'Result Merit List for ' . $class_info[0]['name'] . '-' .  $shift_info[0]['name'] . '-' . $section_info[0]['name'];
			}else if($report_type == 'gw'){
				$class_id = $_POST['class_id'];
				$group_id = $_POST['group_id'];
				$data['class_id'] = $class_id;
				$data['group_id'] = $group_id;
				$where = " AND r.`class_id` = '$class_id' AND r.`group` = '$group_id' ";
				$order_by = " ORDER BY r.`group_position` = 0,r.`group_position` ";
				$class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
				$group_info = $this->db->query("SELECT name FROM tbl_student_group WHERE id ='$group_id'")->result_array();
				$data['title'] = 'Result Merit List for ' . $class_info[0]['name'] . '-' .  $group_info[0]['name'];
			}

			$data['exam_list'] = $this->db->query("SELECT * FROM `tbl_exam` AS e WHERE e.`year` = '$year' ORDER BY exam_order")->result_array();

			$data['merit_data'] =  $this->db->query("SELECT s.`name`,s.`student_code`,r.`roll_no`,r.`total_obtain_mark`,
r.`gpa_with_optional`,
r.`c_alpha_gpa_with_optional`,r.`class_position`,r.`section_position`,r.`shift_position`,r.`group_position`
 FROM `tbl_result_process` AS r
INNER JOIN `tbl_student` AS s ON s.`id` = r.`student_id`
WHERE r.`class_position` != 0 AND r.`exam_id` = '$exam_id' $where $order_by")->result_array();



			$SchoolInfo = $this->Admin_login->fetReportHeader();
            $data['HeaderInfo'] = $SchoolInfo;
            $data['report'] = $this->load->view('merit_list/report', $data, true);
        }

		if (isset($_POST['pdf_download'])) {
			$data['is_pdf'] = 1;
			//Dom PDF
			$this->load->library('mydompdf');
			$html = $this->load->view('merit_list/report', $data, true);
			$file_name = $data['title'];
			$this->mydompdf->createPDF($html, $file_name, true, 'A4', 'portrait');
			//Dom PDF
		}else{
			$data['title'] = 'Student Merit List';
			$data['heading_msg'] = "Student Merit List";
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['class_list'] = $this->db->query("SELECT * FROM `tbl_class` ORDER BY `student_code_short_form`")->result_array();
			$data['group_list'] = $this->Admin_login->getGroupList();
			$data['shift_list'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
			$data['maincontent'] = $this->load->view('merit_list/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
    }
}
