<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subject_assigns extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student','Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        if ($_POST) {
            $year = $this->input->post("year");
            $group = $this->input->post("group");
            $subject_id = $this->input->post("subject_id");

            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];

            $sdata['year'] = $year;
            $sdata['class_shift_section_id'] = $class_shift_section_id;
            $sdata['group'] = $group;
            $sdata['subject_id'] = $subject_id;
            $this->session->set_userdata($sdata);

            $data['sub_info'] = $this->db->query("SELECT * FROM tbl_subject WHERE
                 `id` IN (SELECT subject_id FROM tbl_class_wise_subject WHERE `class_id` = '$class_id')")->result_array();

            $data['year'] = $year;
            $data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['shift_id'] = $shift_id;
            $data['group'] = $group;
            $data['subject_id'] = $subject_id;
            $data['student_info'] = $this->db->query("SELECT s.`id`,s.`student_code`,s.`roll_no`,s.`section_id`,s.`shift_id`,s.`name`,ss.`id` AS already_allocate_subject_id,ss.`is_optional` FROM `tbl_student` AS s
LEFT JOIN `tbl_student_wise_subject` AS ss ON ss.`student_id` = s.`id` AND ss.`year` = '$year' AND ss.`subject_id` = '$subject_id'
WHERE s.`class_id` = '$class_id' AND s.`shift_id` = '$shift_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group' AND s.`status` = '1' ORDER BY ABS(s.roll_no)")->result_array();
            $data['student_list'] = $this->load->view('subject_assign/student_list_for_subject_allocate', $data, true);
        }

        $data['sub_info'] = array();
        $class_shift_section_id = $this->session->userdata('class_shift_section_id');
        //echo $class_shift_section_id; die;
        if(isset($class_shift_section_id)){
          if($class_shift_section_id != ''){
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $data['sub_info'] = $this->db->query("SELECT * FROM tbl_subject WHERE
                 `id` IN (SELECT subject_id FROM tbl_class_wise_subject WHERE `class_id` = '$class_id')")->result_array();
          }
        }

        $data['title'] = 'Student Wise Subject Assign';
        $data['heading_msg'] = "Student Wise Subject Assign";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
        $data['years'] = $this->Admin_login->getYearList(0, 0);
        $data['group_list'] = $this->Admin_login->getGroupList();
        $data['maincontent'] = $this->load->view('subject_assign/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

	function student_subject_assign_data_save()
	{
		//echo '<pre>';
		//print_r($_POST); die;
		$year = $this->input->post("year");
		//echo $year; die;
		$loop_time = $this->input->post("loop_time");
		$class_id = $this->input->post("class_id");
		$section_id = $this->input->post("section_id");
		$shift_id = $this->input->post("shift_id");
		$group = $this->input->post("group");
		$subject_id = $this->input->post("subject_id");
		$this->deleteStudentWiseSubject($class_id,$section_id,$shift_id,$group,$subject_id,$year);
		$subject_details = $this->Admin_login->getSubjectTypeByClassExamTypeSubject($class_id,$subject_id);
		$i = 1;
		while ($i <= $loop_time) {
			if ($this->input->post("is_allow_" . $i)) {
				$data = array();
				$data['student_id'] = $this->input->post("student_id_" . $i);
				$data['section_id'] = $this->input->post("section_id_" . $i);
				$data['shift_id'] = $this->input->post("shift_id_" . $i);
				$data['year'] = $year;
				$data['class_id'] = $class_id;
				$data['group'] = $group;
				$data['subject_id'] = $subject_id;
				$data['subject_type'] = $subject_details->subject_type;
				if ($this->input->post("is_optional_" . $i)) {
					$data['is_optional'] = '1';
				} else {
					$data['is_optional'] = '0';
				}
				$data['data_entry_date'] = date('Y-m-d');
				//echo '<pre>';
				//print_r($data); die;
				$this->db->insert('tbl_student_wise_subject', $data);
			}
			$i++;
		}
		$sdata['message'] = "Student Wise Subject Allocate Data Successfully Added.";
		$this->session->set_userdata($sdata);
		redirect("subject_assigns/index");
	}

    public function optional_subject_config(){
		$data = array();
		if ($_POST) {
			$year = $this->input->post("year");
			$group = $this->input->post("group");

			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$sdata['year'] = $year;
			$sdata['class_shift_section_id'] = $class_shift_section_id;
			$sdata['group'] = $group;
			$this->session->set_userdata($sdata);

			$data['year'] = $year;
			$data['class_id'] = $class_id;
			$data['group'] = $group;
			$data['shift_id'] = $shift_id;
			$data['section_id'] = $section_id;

			$data['subject_list'] = $this->Admin_login->getClassAndGroupWiseChoosableSubjectList($class_id,$group);
//			echo '<pre>';
//			print_r($data['subject_list']);
//			die;
			if(empty($data['subject_list'])){
				$sdata['exception'] = "Please assign choosable subject first for this class";
				$this->session->set_userdata($sdata);
				redirect("subject_assigns/optional_subject_config");
			}

			$data['student_info'] = $this->db->query("SELECT s.`id`,s.`student_code`,s.`roll_no`,
									s.`section_id`,s.`shift_id`,s.`name` FROM `tbl_student` AS s
									WHERE s.`class_id` = '$class_id' AND s.`shift_id` = '$shift_id'
									AND s.`section_id` = '$section_id' AND s.`group` = '$group'
									AND s.`status` = '1' ORDER BY ABS(s.roll_no)")->result_array();
			$data['student_list'] = $this->load->view('subject_assign/student_list_for_optional_subject_config', $data, true);
		}

		$data['title'] = '4th Subject Configuration';
		$data['heading_msg'] = '4th Subject Configuration';
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['group_list'] = $this->Admin_login->getGroupList();
		$data['maincontent'] = $this->load->view('subject_assign/optional_subject_config', $data, true);
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function single_and_multiple_assign_data_save(){
    	if($_POST){
			//echo '<pre>';
			//print_r($_POST);
			//die;
			$year = $this->input->post("year");
			$loop_time = $this->input->post("loop_time");
			$class_id = $this->input->post("class_id");
			$section_id = $this->input->post("section_id");
			$shift_id = $this->input->post("shift_id");
			$group = $this->input->post("group");
    		if(isset($_POST['SingleSaveButton'])){
				$subject_id = $this->input->post("subject_id");
				$subject_details = $this->Admin_login->getSubjectTypeByClassExamTypeSubject($class_id,$subject_id);
				$i = 1;
				while ($i <= $loop_time) {
					if ($this->input->post("is_allow_" . $i)) {
						$data = array();
						$data['student_id'] = $this->input->post("student_id_" . $i);
						$data['year'] = $year;
						$data['class_id'] = $class_id;
						$data['section_id'] = $section_id;
						$data['shift_id'] = $shift_id;
						$data['group'] = $group;
						$data['subject_id'] = $subject_id;
						$data['subject_type'] = $subject_details->subject_type;
						$assign_type = $this->input->post("assign_type");
						if ($assign_type == 'OPT') {
							$data['is_optional'] = '1';
						} else {
							$data['is_optional'] = '0';
						}
						$data['data_entry_date'] = date('Y-m-d');
						$this->deleteStudentWiseSubjectByStudent($data['student_id'],$subject_id,$year);
						$this->db->insert('tbl_student_wise_subject', $data);
					}
					$i++;
				}
				$sdata['message'] = $this->lang->line('add_success_message');
				$this->session->set_userdata($sdata);
				redirect("subject_assigns/optional_subject_config");
			}

    		//multiple
			if(isset($_POST['MultipleSaveButton'])){
				//start process for compulsory subjects
				$total_compulsory_subjects = count($_POST['compulsory_subjects']);
				$compulsory_sl = 0;
				while ($compulsory_sl < $total_compulsory_subjects){
                    $subject_id = $_POST['compulsory_subjects'][$compulsory_sl];
					$subject_details = $this->Admin_login->getSubjectTypeByClassExamTypeSubject($class_id,$subject_id);
					//save data
					$i = 1;
					while ($i <= $loop_time) {
						if ($this->input->post("is_allow_" . $i)) {
							$data = array();
							$data['student_id'] = $this->input->post("student_id_" . $i);
							$data['year'] = $year;
							$data['class_id'] = $class_id;
							$data['section_id'] = $section_id;
							$data['shift_id'] = $shift_id;
							$data['group'] = $group;
							$data['subject_id'] = $subject_id;
							$data['subject_type'] = $subject_details->subject_type;
							$data['is_optional'] = '0';
							$data['data_entry_date'] = date('Y-m-d');
							$this->deleteStudentWiseSubjectByStudent($data['student_id'],$subject_id,$year);
							$this->db->insert('tbl_student_wise_subject', $data);
						}
						$i++;
					}
					//save data
					$compulsory_sl++;
				}
			    //end process for compulsory subjects

				//start process for optional subjects
				$total_optional_subjects = count($_POST['optional_subjects']);
				$optional_sl = 0;
				while ($optional_sl < $total_optional_subjects){
					$subject_id = $_POST['optional_subjects'][$optional_sl];
					$subject_details = $this->Admin_login->getSubjectTypeByClassExamTypeSubject($class_id,$subject_id);
					//save data
					$i = 1;
					while ($i <= $loop_time) {
						if ($this->input->post("is_allow_" . $i)) {
							$data = array();
							$data['student_id'] = $this->input->post("student_id_" . $i);
							$data['year'] = $year;
							$data['class_id'] = $class_id;
							$data['section_id'] = $section_id;
							$data['shift_id'] = $shift_id;
							$data['group'] = $group;
							$data['subject_id'] = $subject_id;
							$data['subject_type'] = $subject_details->subject_type;
							$data['is_optional'] = '1';
							$data['data_entry_date'] = date('Y-m-d');
							$this->deleteStudentWiseSubjectByStudent($data['student_id'],$subject_id,$year);
							$this->db->insert('tbl_student_wise_subject', $data);
						}
						$i++;
					}
					//save data
					$optional_sl++;
				}
				//start process for optional subjects

				$sdata['message'] = $this->lang->line('add_success_message');
				$this->session->set_userdata($sdata);
				redirect("subject_assigns/optional_subject_config");
			}
		}
	}

	public function deleteStudentWiseSubjectByStudent($student_id,$subject_id,$year){
		$this->db->delete('tbl_student_wise_subject', array('student_id' => $student_id, 'subject_id' => $subject_id, 'year' => $year));
		return true;
	}

	public function deleteStudentWiseSubject($class_id,$section_id,$shift_id,$group,$subject_id,$year){
		$this->db->delete('tbl_student_wise_subject', array('class_id' => $class_id, 'section_id' => $section_id, 'shift_id' => $shift_id, 'group' => $group, 'subject_id' => $subject_id, 'year' => $year));
		return true;
	}

    public function bulk_subject_assign()
    {
        if($_POST){
           $year = $this->input->post("year");
           $software_current_year = $this->Admin_login->getCurrentSystemYear();
           if($software_current_year != $year){
             $sdata['exception'] =  $year . " is not current system year";
             $this->session->set_userdata($sdata);
             redirect("subject_assigns/bulk_subject_assign");
           }

          $class_id = $this->input->post("class_id");

          $sdata['class_id'] = $class_id;
          $sdata['year'] = $year;
          $this->session->set_userdata($sdata);

          $student_list = $this->Admin_login->getStudentListByYear($year, $class_id);
          if(empty($student_list)){
            $sdata['exception'] = "There are no students in this class";
            $this->session->set_userdata($sdata);
            redirect("subject_assigns/bulk_subject_assign");
          }

          $subject_list = $this->Admin_login->getClassAndGroupWiseSubjectListWithoutChoosable($class_id);
//          echo '<pre>';
//          print_r($subject_list);
//          die;
          if(empty($subject_list)){
            $sdata['exception'] = "Please assign subject first for this class";
            $this->session->set_userdata($sdata);
            redirect("class_wise_subjects/index");
          }

          foreach ($student_list['students'] as $student) {
             if(isset($subject_list[$student['group']])){
                 $student_subject_list = array();
                 $i = 0;
                 $student_id = $student['id'];
                 foreach ($subject_list[$student['group']] as $subject) {
                    $student_subject_list[$i]['student_id'] = $student_id;
                    $student_subject_list[$i]['subject_id'] = $subject['subject_id'];
                    $student_subject_list[$i]['subject_type'] = $subject['subject_type'];
                    $student_subject_list[$i]['class_id'] = $student['class_id'];
                    $student_subject_list[$i]['group'] = $student['group'];
                    $student_subject_list[$i]['section_id'] = $student['section_id'];
                    $student_subject_list[$i]['shift_id'] = $student['shift_id'];
                    $student_subject_list[$i]['is_optional'] = '0';
                    $student_subject_list[$i]['year'] = $year;
                    $student_subject_list[$i]['data_entry_date'] = date('Y-m-d H:i:s');
                    $i++;
                 }
//				 echo '<pre>';
//				 print_r($student_subject_list);
//				 die;
                 $this->db->query("DELETE FROM tbl_student_wise_subject
                                   WHERE `student_id` = $student_id AND `subject_type` != 'CHO'
                                   AND `year` = '$year'");
                 $this->db->insert_batch('tbl_student_wise_subject', $student_subject_list);
             }
          }

          $sdata['message'] = $this->lang->line('add_success_message');
          $this->session->set_userdata($sdata);
          redirect("subject_assigns/bulk_subject_assign");

        }
        $data['title'] = 'Quick Subject Assign';
        $data['heading_msg'] = "Quick Subject Assign";
        $data['years'] = $this->Admin_login->getYearList(0, 0);
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('subject_assign/bulk_subject_assign', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


}
