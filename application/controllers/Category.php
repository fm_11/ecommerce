<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Category extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('E_commerce'));
        $this->load->library(array('session'));

        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('category');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $sdata['name'] = $name;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
        } else {
            $name = $this->session->userdata('name');
            $cond['name'] = $name;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('category/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->E_commerce->get_category_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['category'] = $this->E_commerce->get_category_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('category/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
      if ($_POST) {
        $data = array();
        $name=$this->input->post('name');
        $data['name']=$name;
        $data['is_hot_deal']=$this->input->post('is_hot_deal');
        $data['is_main_menu']=$this->input->post('is_main_menu');
        $data['order_no']=$this->input->post('order_no');
        if($this->E_commerce->checkifexist_category_by_name($name))
        {
          $sdata['exception'] = "This name '".$name."' already exist.";
          $this->session->set_userdata($sdata);
          redirect("category/add");
        }
        if ($this->E_commerce->add_category($data)) {
            $sdata['message'] = "Information Successfully Added";
            $this->session->set_userdata($sdata);
            redirect("category/index");
        } else {
            $sdata['exception'] = "Information could not add";
            $this->session->set_userdata($sdata);
            redirect("category/add");
        }
        $this->db->insert("tbl_category", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('category/index');
      }
      $data = array();
      $data['heading_msg'] = $data['title'] = $this->lang->line('category');
      $data['action'] = 'add';
      $data['is_show_button'] = "index";
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('category/add', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
      if ($_POST) {
        $data = array();
        if($this->E_commerce->checkifexist_update_category_by_name($_POST['name'],$_POST['id']))
        {
          $sdata['exception'] = "This name '".$_POST['name']."' already exist.";
          $this->session->set_userdata($sdata);
          redirect("category/edit/".$_POST['id']);
        }
        $data['id'] = $_POST['id'];
        $data['name'] = $_POST['name'];
        $data['is_hot_deal']=$this->input->post('is_hot_deal');
        $data['is_main_menu']=$this->input->post('is_main_menu');
        $data['order_no']=$this->input->post('order_no');
        $this->db->where('id', $data['id']);
        $this->db->update("tbl_category", $data);
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect('category/index');
      }
      $data = array();
      $data['heading_msg'] = $data['title'] = $this->lang->line('category');
      $data['action'] = 'edit/' . $id;
      $data['is_show_button'] = "index";
      $data['row'] = $this->E_commerce->read_category($id);
      $data['category'] = $this->db->query("SELECT * FROM `tbl_category` WHERE id = '$id'")->result_array();
      $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
      $data['maincontent'] = $this->load->view('category/edit', $data, true);
      $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->db->delete('tbl_category', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_sucess_message');
        $this->session->set_userdata($sdata);
        redirect('category/index');
    }

    public function updateCaseStatus()
    {
        $status = $this->input->get('status', true);
        $columname = $this->input->get('columname', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
          if($columname=='11')
          {
            $data['is_hot_deal'] = 0;
          }else{
            $data['is_main_menu'] = 0;
          }

        } else {
          if($columname=='11')
          {
            $data['is_hot_deal'] = 1;
          }else{
            $data['is_main_menu'] = 1;
          }

        }
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_category', $data);
        if ($status == 0) {

            echo '<a class="deleteTag" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1,'.$columname.')"><i class="ti-check-box"></i></a>';
        } else {

          echo '<a class="deleteTag" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0,'.$columname.')"><i class="ti-na"></i></a>';
        }
    }

  }

 ?>
