<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Audio_uploads extends CI_Controller
{

    public $SOFTWARE_START_YEAR = '';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function audio_message_upload()
    {
        //echo MEDIA_FOLDER; die;
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/audio/';
            $config['allowed_types'] = 'mp3';
            $config['max_size'] = '600000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
            //echo $extension; die;
            if($extension != 'mp3'){
                $sdata['exception'] = "Sorry .$extension file not allowed. Please upload .mp3 file !";
                $this->session->set_userdata($sdata);
                redirect("audio_uploads/audio_message_upload");
            }
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "audio_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/audio/" . $new_file_name)) {
                        $data = array();
                        $data['title'] = $this->input->post('title', true);
                        $data['location'] = $new_file_name;
                        $this->db->insert('tbl_audio_sms', $data);
                        $sdata['message'] = $this->lang->line('add_success_message');
                        $this->session->set_userdata($sdata);
                        redirect("messages/audio_message_upload");
                    } else {
                        $sdata['exception'] = "Sorry Audio Doesn't Upload !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("audio_uploads/audio_message_upload");
                    }
                } else {
                    $sdata['exception'] = $this->lang->line('add_error_message');
                    $this->session->set_userdata($sdata);
                    redirect("audio_uploads/audio_message_upload");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Audio SMS/Messages Upload';
            $data['heading_msg'] = "Audio SMS/Messages Upload";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('messages/audio_message_upload', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
