<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Combined_mark_sheet extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Message','Admin_login','common/custom_methods_model','Config_general'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Combined ' . $this->lang->line('mark') . ' ' . $this->lang->line('sheet');
        $data['heading_msg'] = 'Combined ' . $this->lang->line('mark') . ' ' . $this->lang->line('sheet');
        if ($_POST) {
            $year = $this->input->post('year', true);
            $exam = $this->input->post('exam', true);

			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class  = $class_shift_section_arr[0];
			$shift = $class_shift_section_arr[1];
			$section = $class_shift_section_arr[2];

            $group = $this->input->post('group', true);
            $from_roll = $this->input->post('from_roll', true);
            $to_roll = $this->input->post('to_roll', true);

            $view_type = $this->input->post('view_type', true);
            $data['year'] = $year;

            $student_id = 0;
            $st_where = "";
            if ($view_type == 'S') {
                if ($from_roll > $to_roll) {
                    $sdata['exception'] = "From roll cannot be greater than To roll";
                    $this->session->set_userdata($sdata);
                    redirect("combined_mark_sheet/index");
                }
                $roll_where_in_string = "";
                while ($from_roll <= $to_roll) {
                    $roll_where_in_string .= "'" . $from_roll ."',";
                    $from_roll++;
                }
                $roll_where_in_string_for_q = rtrim($roll_where_in_string, ',');
                //die;
                $st_where = " AND r.`roll_no` IN ($roll_where_in_string_for_q)";
            }



            $student_info = $this->db->query("SELECT r.`student_id` FROM `tbl_result_process` AS r WHERE
              r.`class_id` = '$class' AND r.`section_id` = '$section' AND
        r.`group` = '$group' AND r.`shift` = '$shift' AND  r.`exam_id` = '$exam' $st_where")->result_array();
            if (empty($student_info)) {
                $sdata['exception'] = "Sorry this student not found !";
                $this->session->set_userdata($sdata);
                redirect("combined_mark_sheet/index");
            }

            // echo '<pre>';
            // print_r($student_info);
            // die;

            $data['header_info'] = $this->Admin_login->fetReportHeader();

            $data['exam_list'] = $this->db->query("SELECT e.*,et.`name` AS exam_type_name, et.`type` FROM `tbl_exam` AS e
LEFT  JOIN `tbl_exam_type` AS et ON e.`exam_type_id` = et.`id`
WHERE e.`id` = '$exam' || e.`id` IN ((SELECT `combined_exam_id` FROM `tbl_exam_combined_result` WHERE `exam_id` = '$exam'))
ORDER BY e.`exam_order`")->result_array();
            // echo '<pre>';
            // print_r($data['exam_list']);
            // die;

            $retun_data_for_all_student = array();
            $retun_data_loop = 0;
            foreach ($student_info as $student) {
                $student_id = $student['student_id'];
                $result_process_data = array();
                $m = 0;
                foreach ($data['exam_list'] as $exam) {
                    $exam_id = $exam['id'];
                    $semester_result_info = $this->Student->result_all_student_after_process($exam_id, $class, $section, $group, $shift, $student_id);
                    if (!empty($semester_result_info)) {
                        $result_process_data[$m] = $semester_result_info;
                        $result_process_data[$m]['exam_name'] = $exam['name'];
                        $result_process_data[$m]['is_combined_result'] = $exam['is_combined_result'];

                        $highest_mark = $this->db->query("SELECT s.`subject_code`,MAX(s.`total_obtain`) as highest_mark,
                          s.`student_id` FROM `tbl_result_process_details` AS s
                          WHERE s.`exam_id` = '$exam_id' AND s.`class_id` = '$class' AND s.`section_id` = '$section' AND
                           s.`group` = '$group' AND s.`shift` = '$shift' GROUP BY s.`subject_code`")->result_array();

                        $highest_marks = array();
                        foreach ($highest_mark as $row):
                             $highest_marks[$row['subject_code']] = $row['highest_mark'];
                        endforeach;

                        $result_process_data[$m]['highest_marks'] = $highest_marks;

                        $m++;
                    }
                }
                if (!empty($result_process_data)) {
                    $data['student_result_data'][$retun_data_loop]['result_process_data'] = $result_process_data;
                    $data['student_result_data'][$retun_data_loop]['combined_result_details'] = $this->db->query("SELECT * FROM `tbl_combined_result_details`
                      WHERE `exam_id` = '$exam_id' AND `student_id` = '$student_id'")->result_array();
                }
                $retun_data_loop++;
            }
            // echo '<pre>';
            // print_r($data['student_result_data']);
            // die;

            $data['subject_list'] = $this->db->query("SELECT p.`subject_name`,p.`subject_code`,
              p.`subject_id`,p.`order_number`,p.`credit` FROM `tbl_result_process_details` AS  p
               WHERE p.`exam_id` = '$exam_id'
GROUP BY p.`subject_id` ORDER BY p.`order_number`")->result_array();

            // echo '<pre>';
            // print_r($data['subject_list']);
            // die;

            $class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class'")->result_array();
            $data['class_name'] = $class_info[0]['name'];

            $section_info = $this->db->query("SELECT name FROM tbl_section WHERE id ='$section'")->result_array();
            $data['section_name'] = $section_info[0]['name'];

            $group_info = $this->db->query("SELECT name FROM tbl_student_group WHERE id ='$group'")->result_array();
            $data['group_name'] = $group_info[0]['name'];

            $shift_info = $this->db->query("SELECT name FROM tbl_shift WHERE id ='$shift'")->result_array();
            $data['shift_name'] = $shift_info[0]['name'];

			$data['general_config'] = $this->Config_general->get_general_configurations('marksheet');
			$data['signature'] = $this->Admin_login->getSignatureByAccessCode('MS'); //MS= Marksheet

            if (isset($_POST['Preview'])) {
                //echo 99;
                //  die;
                $this->load->view('progress_report/combined_report_design_1', $data);
            }
            if (isset($_POST['pdf_download'])) {
                $data['is_pdf'] = 1;
                //Dom PDF
                $this->load->library('mydompdf');
                $html = $this->load->view('progress_report/combined_report_design_1', $data, true);
                $this->mydompdf->createPDF($html, 'CombinedMarkSheet', true, 'A4', 'landscape');
                //Dom PDF
            }
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
            $data['group'] = $this->Admin_login->getGroupList();
            $data['maincontent'] = $this->load->view('progress_report/combined_index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function get_only_combined_exam_by_year()
    {
        $year = $_GET['year'];
        $data = array();
        $data['exam_list'] = $this->db->query("SELECT * FROM `tbl_exam` AS e WHERE e.`is_combined_result` = '1' AND e.`year` = '$year' AND e.`status` = '1'")->result_array();
        $this->load->view('mark_sheets/ajax_exam_by_year', $data);
    }
}
