<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Special_care_fees extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login','Message','Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('special_care').' '.$this->lang->line('fees') . ' ' . $this->lang->line('collection');
        $data['heading_msg'] = $this->lang->line('special_care').' '.$this->lang->line('fees') . ' ' . $this->lang->line('collection');
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('special_care_fees/index/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student_fee->get_all_special_care_collected_fee(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['fees'] = $this->Student_fee->get_all_special_care_collected_fee(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('special_care_fees/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $class_id = $this->input->post('class_id', true);
            $section_id = $this->input->post('section_id', true);
            $student_id = $this->input->post('student_id', true);

            $cdata['c_class_id'] = $class_id;
            $cdata['c_section_id'] = $section_id;
            $cdata['c_student_id'] = $student_id;
            $this->session->set_userdata($cdata);

            $data = array();
            $data['class_id'] = $this->input->post('class_id', true);
            $data['section_id'] = $this->input->post('section_id', true);
            $data['student_id'] = $this->input->post('student_id', true);
            $data['year'] = $this->input->post('year', true);
            $data['month'] = $this->input->post('month', true);
            $data['amount'] = $this->input->post('amount', true);
            $data['date'] = date('Y-m-d H:i:s');
            $date = new DateTime();
            $data['receipt_no'] = $date->getTimestamp();
            $this->db->insert('tbl_monthly_special_care_fee', $data);
            redirect("special_care_fees/getSpecialCareFeeReceipt/" . $student_id . "/".date('Y-m-d'));
        } else {
            $data = array();
            $data['title'] = 'Special Care Fee Collection';
            $data['heading_msg'] = "Special Care Fee Collection";
            $data['action'] = '';
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();

            $data['c_class_id'] = $this->session->userdata('c_class_id');
            $data['c_section_id'] = $this->session->userdata('c_section_id');
            $data['c_student_id'] = $this->session->userdata('c_student_id');

            if ($data['c_class_id'] != '' && $data['c_section_id'] != '') {
                $c_class_id = $data['c_class_id'];
                $c_section_id = $data['c_section_id'];
                $data['students'] = $this->db->query("SELECT id,name,student_code FROM tbl_student WHERE class_id = '$c_class_id' AND section_id = '$c_section_id' AND status = '1'")->result_array();
            } else {
                $data['students'] = null;
            }
            $data['is_show_button'] = "index";
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('special_care_fees/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function getSpecialCareFeeReceipt($student_id, $date)
    {
        $data = array();
        $data['school_info'] = $this->Admin_login->get_contact_info();
        $data['copy_for'] = "Student Copy";
        $data['student_info'] = $this->db->query("SELECT s.`name`,s.`student_code`,s.`roll_no`,c.`name` AS class,sc.`name` AS section FROM `tbl_student` AS s
INNER JOIN `tbl_class` AS c ON s.`class_id` = c.`id`
INNER JOIN `tbl_section` AS sc ON s.`section_id` = sc.`id`
WHERE s.`id` = '$student_id'")->result_array();
        $this->load->library('numbertowords');
        $data['collection_info'] = $this->db->query("SELECT * FROM `tbl_monthly_special_care_fee` WHERE `student_id` = '$student_id' AND CAST(`date` AS DATE) = '$date'")->result_array();

        $this->load->view('special_care_fees/special_care_fee_receipt', $data);
    }

    public function delete($id)
    {
        $this->db->delete('tbl_monthly_special_care_fee', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("special_care_fees/index");
    }
}
