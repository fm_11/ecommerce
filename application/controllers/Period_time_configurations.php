<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Period_time_configurations extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Admin_login'));

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	function index(){
		$data = array();
		if($_POST){
			$shift_id = $this->input->post("shift_id");
			$data['shift_id'] = $shift_id;

			$data['periods'] = $this->db->query("SELECT p.*,pc.`start_time`,pc.`end_time` FROM `tbl_class_period` AS p
                                LEFT JOIN `tbl_period_time_configuration` AS pc ON pc.`period_id` = p.`id` AND pc.`shift_id` = $shift_id")->result_array();

			if(empty($data['periods'])){
				$sdata['exception'] = "Period not configure. Please configure first";
				$this->session->set_userdata($sdata);
				redirect("class_periods/index");
			}
			$data['process_list'] = $this->load->view('period_time_configurations/process_list', $data, true);
		}
		$data['title'] = 'Period Time Configuration';
		$data['heading_msg'] = 'Period Time Configuration';
		$data['shifts'] = $this->db->query("SELECT * FROM tbl_shift")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('period_time_configurations/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}


	function data_save(){
		if($_POST){
			$shift_id = $this->input->post("shift_id");
			$total_period = $this->input->post("total_period");
			$i = 0;
			while ($i < $total_period){

				$period_id = $this->input->post("period_id_" . $i);
				$this->db->query("DELETE FROM `tbl_period_time_configuration` WHERE `shift_id` = '$shift_id' 
                              AND `period_id` = '$period_id'");

				if(isset($_POST["is_checked_" . $i])){
					$data = array();
					$data['shift_id'] = $shift_id;
					$data['period_id'] = $period_id;
					$data['start_time'] =  $this->input->post("start_time_" . $i);
					$data['end_time'] = $this->input->post("end_time_" . $i);
					$this->db->insert("tbl_period_time_configuration", $data);
				}
				$i++;
			}
			$sdata['message'] =  $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect("period_time_configurations/index");
		}
	}
}

