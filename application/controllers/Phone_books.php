<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Phone_books extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->load->model(array('Message', 'Timekeeping', 'Admin_login'));
        $this->notification = array();
    }

    public function index()
    {
        //echo ROW_PER_PAGE;
        //die;
        $data = array();
        $data['title'] = $this->lang->line('contact') . ' ' . $this->lang->line('number') . ' ' . $this->lang->line('list');
        $data['heading_msg'] = $this->lang->line('contact') . ' ' . $this->lang->line('number') . ' ' . $this->lang->line('list');
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('phone_books/index/');
        $config['per_page'] = ROW_PER_PAGE;
        $config['total_rows'] = count($this->Message->get_all_phone_book(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['PhoneBookList'] = $this->Message->get_all_phone_book(ROW_PER_PAGE, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('phone_books/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            /* echo '<pre>';
            print_r($_POST);
            die(); */
            $insertData['contact_name'] = $_POST['ContactName'];
            $insertData['contact_number'] = $_POST['ContactNumber'];
            $insertData['category_id'] = $_POST['PhoneBookCategory'];

            $insertData['working_place'] = $_POST['working_place'];
            $insertData['designation'] = $_POST['designation'];
            $insertData['address'] = $_POST['address'];
            $insertData['remarks'] = $_POST['remarks'];

            $tableName = "tbl_message_contact_list";
            $returnData = $this->Message->DoCommonInsert($insertData, $tableName);
            if ($returnData['status'] == 1) {
                $sdata['message'] = $this->lang->line('add_success_message');
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
            }
            $this->session->set_userdata($sdata);

            redirect('phone_books/add');
        }
        $data = array();
        $data['title'] = $this->lang->line('contact') . ' ' . $this->lang->line('number') . ' ' . $this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('contact') . ' ' . $this->lang->line('number') . ' ' . $this->lang->line('add');
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['categoryList'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['maincontent'] = $this->load->view('phone_books/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function validate_mobile_number($phoneNumber)
    {
        if (!empty($phoneNumber)) { // phone number is not empty
            if (preg_match('/^\d{11}$/', $phoneNumber)) { // phone number is valid
                return true;
            // your other code here
            } else { // phone number is not valid
                return false;
            }
        } else { // phone number is empty
            return false;
        }
    }

    public function delete($id)
    {
        $Check = $this->db->query("SELECT `id` FROM `tbl_message_contact_list` WHERE `id` = '$id' LIMIT 1")->result_array();
        /* echo '<pre>';
        print_r($Check); */
        if (!empty($Check)) {
            if ($this->db->query("DELETE FROM tbl_message_contact_list WHERE `id` = '$id'")) {
                $sdata['message'] = $this->lang->line('delete_success_message');
            } else {
                $sdata['exception'] = $this->lang->line('delete_error_message');
            }
            $this->session->set_userdata($sdata);
            redirect('phone_books/index/', 'refresh');
        } else {
            $sdata['exception'] = $this->lang->line('delete_error_message');
            $this->session->set_userdata($sdata);
            redirect('phone_books/index/', 'refresh');
        }
    }

    public function edit($id)
    {
        if ($_POST) {
            /* echo '<pre>';
            print_r($_POST);
            die(); */
            $updateData = array();
            $updateData['contact_name'] = $_POST['ContactName'];
            $updateData['contact_number'] = $_POST['ContactNumber'];
            $updateData['category_id'] = $_POST['PhoneBookCategory'];
            $updateData['working_place'] = $_POST['working_place'];
            $updateData['designation'] = $_POST['designation'];
            $updateData['address'] = $_POST['address'];
            $updateData['remarks'] = $_POST['remarks'];

            $this->db->where('id', $id);
            $this->db->update('tbl_message_contact_list', $updateData);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('phone_books/index', 'refresh');
        }
        $data = array();
        $data['title'] = $this->lang->line('contact') . ' ' . $this->lang->line('number') . ' ' . $this->lang->line('update');
        $data['heading_msg'] = $this->lang->line('contact') . ' ' . $this->lang->line('number') . ' ' . $this->lang->line('update');
        $data['is_show_button'] = "index";
        $data['EditData'] = $this->db->query("SELECT * FROM tbl_message_contact_list WHERE `id` = '$id'")->row_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['categoryList'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['maincontent'] = $this->load->view('phone_books/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
