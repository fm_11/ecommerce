<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Teacher_batch_adds extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Teacher_info','Admin_login'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		//For Excel Read
		require_once APPPATH . 'third_party/PHPExcel.php';
		$this->excel = new PHPExcel();
		//For Excel Read


		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		//echo '<pre>'; print_r($user_info); die;
		$this->SOFTWARE_START_YEAR = $user_info[0]->config_info[0]['software_start_year'];
		$this->notification = array();
	}


	public function index()
	{
		$data = array();
		if ($_POST) {
			//For Excel Upload
			$configUpload['upload_path'] = FCPATH . 'uploads/excel/';
			$configUpload['allowed_types'] = 'xls|xlsx|csv';
			$configUpload['max_size'] = '15000';
			$this->load->library('upload', $configUpload);
			$this->upload->do_upload('txtFile');
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_name = $upload_data['file_name']; //uploded file name
			$extension = $upload_data['file_ext'];    // uploded file extension
			//echo FCPATH;
			//echo '<pre>'; print_r($_FILES); die;
			if ($extension == '.xlsx') {
				$objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
			} else {
				$objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
			}

			//Set to read only
			$objReader->setReadDataOnly(true);
			//Load excel file
			$objPHPExcel = $objReader->load(FCPATH . 'uploads/excel/' . $file_name);
			$totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			//loop from first data untill last data
			$returnDataForShow = array();
			for ($i = 2; $i <= $totalrows; $i++) {
					$name = trim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");
					if ($name == '') {
						break;
					}
					$index_num = trim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue(), " ");
				    $gender = trim($objWorksheet->getCellByColumnAndRow(2, $i)->getValue(), " ");
					$religion = trim($objWorksheet->getCellByColumnAndRow(3, $i)->getValue(), " ");
				    $category = trim($objWorksheet->getCellByColumnAndRow(4, $i)->getValue(), " ");
					$mobile = trim($objWorksheet->getCellByColumnAndRow(5, $i)->getValue(), " ");

					if ($gender == 'Male') {
						$gender = 'M';
					} else {
						$gender = 'F';
					}

					$returnDataForShow[$i]['name'] = $name;
					$returnDataForShow[$i]['index_num'] = $index_num;
					$returnDataForShow[$i]['gender'] = $gender;
					$returnDataForShow[$i]['religion'] = $religion;
				    $returnDataForShow[$i]['category'] = $category;
					$returnDataForShow[$i]['mobile'] = $mobile;
			}
			//For Excel Upload

//			echo '<pre>';
//			print_r($returnDataForShow);
//			die;

			$filedel = PUBPATH . 'uploads/excel/' . $file_name;
			unlink($filedel);
			$data['returnDataForShow'] = $returnDataForShow;
			$data['categories'] = $this->db->query("SELECT * FROM tbl_teacher_category")->result_array();
			$data['religions'] =  $this->Admin_login->getReligionList();
			$data['data_table'] = $this->load->view('teacher_batch_adds/data_table', $data, true);
		}

		$data['title'] = "Teacher Add By Excel";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('teacher_batch_adds/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function excel_data_save(){
		if($_POST){
			$number_of_rows = $this->input->post('number_of_rows');
			$i = 0;
			while ($i < $number_of_rows){
				$name = $this->input->post('name_' . $i);
				if($name != ''){
					$data = array();
					$data['name'] = $name;
					$data['teacher_index_no'] = $this->input->post('index_num_' . $i, true);
					$data['gender'] = $this->input->post('gender_' . $i, true);
					$data['religion'] = $this->input->post('religion_' . $i, true);
					$data['category_id'] = $this->input->post('category_id_' . $i, true);
					$data['mobile'] = $this->input->post('mobile_' . $i, true);
					$data['teacher_code'] = $this->Teacher_info->generateTeacherCode();
					$data['date_of_birth'] = NULL;
					$data['date_of_join'] = NULL;
					$this->db->insert('tbl_teacher', $data);
				}
				$i++;
			}
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect("teacher_batch_adds/index");
		}
	}
}
