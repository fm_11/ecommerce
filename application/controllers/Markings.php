<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Markings extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Student','Admin_login'));

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}

		$this->notification = array();
	}


	public function index()
	{
		$data = array();
		if ($_POST) {
			$exam_id = $_POST['exam_id'];
			$class_shift_section_id = $_POST['class_shift_section_id'];
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];
			$group_id = $_POST['group_id'];
			$subject_id = $_POST['subject_id'];
			$year = $_POST['year'];

			$data['posted_exam_id'] = $exam_id;
			$data['posted_class_shift_section_id'] = $class_shift_section_id;
			$data['posted_group_id'] = $group_id;
			$data['posted_subject_id'] = $subject_id;
			$data['posted_year'] = $year;


			// echo '<pre>';
			// print_r($data);
			// die;

			$cdata['m_year'] = $year;
			$cdata['m_exam_id'] = $exam_id;
			$cdata['m_class_shift_section_id'] = $class_shift_section_id;
			$cdata['m_group_id'] = $group_id;
			$cdata['m_subject_id'] = $subject_id;
			$this->session->set_userdata($cdata);

			//data ready
			$exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$exam_id'")->result_array();
			if ($exam_info[0]['exam_type_id'] <= 0) {
				$sdata['exception'] = "Please input first what type of exam it is.";
				$this->session->set_userdata($sdata);
				redirect('exams/index');
			}
			$exam_type_id = $exam_info[0]['exam_type_id'];

			$class_wise_subject_id = $this->db->query("SELECT * FROM `tbl_class_wise_subject` WHERE `class_id` = '$class_id' AND `exam_type_id` = '$exam_type_id' AND `subject_id` = '$subject_id'")->result_array();
			if (empty($class_wise_subject_id)) {
				$sdata['exception'] = "Please class and exam type wise subject entry first !";
				$this->session->set_userdata($sdata);
				redirect("markings/index");
			}
			$data['class_wise_subject_details'] = $class_wise_subject_id;
			$data['credit'] = $class_wise_subject_id[0]['credit'];
			//end data ready

			if (isset($_POST['download'])) {
				$this->generateExcelDataForMarking($exam_id, $class_shift_section_id, $group_id, $subject_id, $year);
			} elseif (isset($_POST['upload'])) {
				// echo '<pre>';
				// print_r($_FILES);
				// die;
				if ($_FILES['excelFile']['name'] == '') {
					$sdata['exception'] = "Please select a file";
					$this->session->set_userdata($sdata);
					redirect('markings/index');
				}
				$data['ProcessInfo'] = $this->markingDataUpload($exam_id, $exam_type_id, $class_shift_section_id, $group_id, $subject_id, $year);
				// echo '<pre>';
				// print_r($data['ProcessInfo']);
				// die;
			} else {
				$data['ProcessInfo'] = $this->marking_data_process($exam_id, $class_shift_section_id, $group_id, $subject_id, $year);
				//end marking process
			}

			if(isset($_POST['upload']) || isset($_POST['process'])){
				$data['ClassSubjectName'] = $this->getSubjectWiseMarkingConfig($class_id, $subject_id, $exam_type_id);
				$data['marking_heads'] = $this->Admin_login->get_main_marking_head($class_id);
				if(empty($data['marking_heads'])){
					$sdata['exception'] = "Please set class wise marking head";
					$this->session->set_userdata($sdata);
					redirect('main_marking_heads/edit/' . $class_id);
				}
				$data['marking_process_list'] = $this->load->view('markings/marking_process', $data, true);
			}
		}

		$data['title'] = 'Mark Input';
		$data['heading_msg'] = "Mark Input";

		$data['m_exam_id'] = $this->session->userdata('m_exam_id');
		$data['class_shift_section_id'] = $this->session->userdata('m_class_shift_section_id');
		$data['m_group_id'] = $this->session->userdata('m_group_id');
		$data['m_subject_id'] = $this->session->userdata('m_subject_id');
		$data['year'] = $this->session->userdata('m_year');
		$data['marking_type'] = $this->session->userdata('m_marking_type');


		$class_id = '';
		if ($data['class_shift_section_id'] != '') {
			$class_shift_section_arr = explode("-", $data['class_shift_section_id']);
			$class_id  = $class_shift_section_arr[0];
			// $shift_id = $class_shift_section_arr[1];
			// $section_id = $class_shift_section_arr[2];
		}

		if ($class_id != '' && $data['m_exam_id'] != '') {
			$m_class_id = $class_id;
			$m_exam_id = $data['m_exam_id'];
			$exam_info =  $this->db->query("SELECT * FROM tbl_exam WHERE id = '$m_exam_id'")->result_array();
			$exam_type_id = 0;
			if (!empty($exam_info)) {
				$exam_type_id = $exam_info[0]['exam_type_id'];
			}

			//  echo $exam_type_id;
			//  die;
			$data['subjects'] = $this->db->query("SELECT
        c.`subject_id`,
        s.`name`, s.`code`
        FROM
        `tbl_class_wise_subject` AS c
        LEFT JOIN `tbl_subject` AS s
        ON s.`id` = c.`subject_id`
        WHERE c.`exam_type_id` = '$exam_type_id' AND c.`class_id` = '$m_class_id' GROUP BY c.`subject_id` ORDER BY c.`order_number`")->result_array();
		} else {
			$data['subjects'] = array();
		}

		if ($data['m_exam_id'] != '') {
			$year = $data['year'];
			$data['ExamList'] = $this->db->query("SELECT * FROM `tbl_exam` WHERE `year` = '$year' ORDER BY exam_order")->result_array();
		} else {
			$data['ExamList'] = array();
		}

		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['GroupList'] = $this->Admin_login->getGroupList();
		$data['maincontent'] = $this->load->view('markings/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function marking_data_save()
	{
		if($_POST){
			$exam_id = $_POST['exam_id'];
			$class_shift_section_id = $_POST['posted_class_shift_section_id'];
			$group_id = $_POST['posted_group_id'];
			$subject_id = $_POST['posted_subject_id'];
			$year = $_POST['posted_year'];

			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			//exam details
			$exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$exam_id'")->result_array();
			if ($exam_info[0]['exam_type_id'] <= 0) {
				$sdata['exception'] = "Please input first what type of exam it is.";
				$this->session->set_userdata($sdata);
				redirect('exams/index');
			}
			$exam_type_id = $exam_info[0]['exam_type_id'];

			$data = array();
			$class_wise_subject_id = $this->db->query("SELECT * FROM `tbl_class_wise_subject` WHERE `class_id` = '$class_id'
          AND `exam_type_id` = '$exam_type_id' AND `subject_id` = '$subject_id'")->result_array();
			if (empty($class_wise_subject_id)) {
				$sdata['exception'] = "Please class and exam type wise subject entry first !";
				$this->session->set_userdata($sdata);
				redirect("markings/index");
			}

			$group_where = "";
			if ($group_id != 'all') {
				$group_where = " AND `group_id` = '$group_id' ";
			}
			$this->db->query("DELETE FROM tbl_marking WHERE `exam_id` = '$exam_id' AND `class_id` = '$class_id' AND `section_id` = '$section_id' $group_where  AND `shift_id` = '$shift_id' AND `subject_id` = '$subject_id'");
			//echo 555; die;
			$IterationNo = $_POST['IterationNo'];
			for ($A = 0; $A < $IterationNo; $A++) {
				$result_data = array();
				$result_data['student_id'] = $this->input->post("student_id_" . $A);
				$result_data['exam_id'] = $exam_id;
				$result_data['class_id'] = $class_id;
				$result_data['section_id'] = $section_id;
				$result_data['group_id'] = $group_id;
				$result_data['shift_id'] = $shift_id;
				$result_data['subject_id'] = $subject_id;
				$result_data['written'] = $this->input->post("written_" . $A);
				$result_data['objective'] = $this->input->post("objective_" . $A);
				$result_data['practical'] = $this->input->post("practical_" . $A);
				$result_data['class_test'] = $this->input->post("class_test_" . $A);
				$result_data['credit'] = $class_wise_subject_id[0]['credit'];
				$result_data['allocated_written'] = $class_wise_subject_id[0]['written'];
				$result_data['allocated_objective'] = $class_wise_subject_id[0]['objective'];
				$result_data['allocated_practical'] = $class_wise_subject_id[0]['practical'];
				$result_data['allocated_class_test'] = $class_wise_subject_id[0]['class_test'];

				if ($this->input->post("is_absent_" . $A)) {
					$result_data['is_absent'] = '1';
				}else{
					$result_data['is_absent'] = '0';
				}
				// echo '<pre>';
				//print_r($result_data); die;
				$this->db->insert('tbl_marking', $result_data);
			}
			$sdata['message'] = "Marking Processed Successfully !";
			$this->session->set_userdata($sdata);
			redirect("markings/index");
		}
	}

	public function markingDataUpload($exam_id, $exam_type_id, $class_shift_section_id, $group_id, $subject_id, $year)
	{
		//For Excel Read
		require_once APPPATH . 'third_party/PHPExcel.php';
		$this->excel = new PHPExcel();
		//For Excel Read
		// echo '<pre>';
		// print_r($_FILES);
		// die;
		$class_shift_section_arr = explode("-", $class_shift_section_id);
		$class_id  = $class_shift_section_arr[0];
		$shift_id = $class_shift_section_arr[1];
		$section_id = $class_shift_section_arr[2];


		$class_wise_subject_id = $this->db->query("SELECT * FROM `tbl_class_wise_subject`
        WHERE `class_id` = '$class_id' AND `exam_type_id` = '$exam_type_id' AND `subject_id` = '$subject_id'")->row();
		if (empty($class_wise_subject_id)) {
			$sdata['exception'] = "Please class and exam type wise subject entry first !";
			$this->session->set_userdata($sdata);
			redirect("markings/index");
		}

		$is_written_allow = $class_wise_subject_id->is_written_allow;
		$is_objective_allow = $class_wise_subject_id->is_objective_allow;
		$is_practical_allow = $class_wise_subject_id->is_practical_allow;
		$is_class_test_allow = $class_wise_subject_id->is_class_test_allow;


		//For Excel Upload
		$configUpload['upload_path'] = FCPATH . 'uploads/excel/';
		$configUpload['allowed_types'] = 'xls|xlsx|csv';
		$configUpload['max_size'] = '15000';
		$this->load->library('upload', $configUpload);
		$this->upload->do_upload('excelFile');
		$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
		$file_name = $upload_data['file_name']; //uploded file name
		$extension = $upload_data['file_ext'];    // uploded file extension
		//echo FCPATH;
		//echo '<pre>'; print_r($_FILES); die;
		if ($extension == '.xlsx') {
			$objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
		} else {
			$objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
		}

		//Set to read only
		$objReader->setReadDataOnly(true);
		//Load excel file
		$objPHPExcel = $objReader->load(FCPATH . 'uploads/excel/' . $file_name);
		$totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		//loop from first data untill last data

		$row_no = 0;
		$result_data = array();
		$generate_error_msg = "";
		for ($i = 2; $i <= $totalrows; $i++) {
			$student_code = trim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");

			$student_info_by_code = $this->Student->get_student_info_by_student_code($student_code);
			if (empty($student_info_by_code)) {
				$generate_error_msg .= $student_code . ', ';
			} else {
				$name = trim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue(), " ");
				if ($student_code == '' || $name == '') {
					break;
				}
				$roll_no = trim($objWorksheet->getCellByColumnAndRow(2, $i)->getValue(), " ");
				$is_absent = trim($objWorksheet->getCellByColumnAndRow(3, $i)->getValue(), " ");

				$written_from_excel = '';
				$objective_from_excel = '';
				$practical_from_excel = '';
				$class_test_from_excel = '';

				$index_no_for_mark = 4;
				if($is_written_allow == '1'){
					$written_from_excel = trim($objWorksheet->getCellByColumnAndRow($index_no_for_mark, $i)->getValue(), " ");
					$index_no_for_mark++;
				}

				if($is_objective_allow == '1'){
					$objective_from_excel = trim($objWorksheet->getCellByColumnAndRow($index_no_for_mark, $i)->getValue(), " ");
					$index_no_for_mark++;
				}

				if($is_practical_allow == '1'){
					$practical_from_excel = trim($objWorksheet->getCellByColumnAndRow($index_no_for_mark, $i)->getValue(), " ");
					$index_no_for_mark++;
				}

				if($is_class_test_allow == '1'){
					$class_test_from_excel = trim($objWorksheet->getCellByColumnAndRow($index_no_for_mark, $i)->getValue(), " ");
				}



				$result_data[$row_no]['id'] = $student_info_by_code->id;
				$result_data[$row_no]['name'] = $student_info_by_code->name;
				$result_data[$row_no]['roll_no'] = $roll_no;
				$result_data[$row_no]['student_code'] = $student_code;
				$result_data[$row_no]['class_id'] = $class_id;
				$result_data[$row_no]['section_id'] = $section_id;
				$result_data[$row_no]['group'] = $group_id;
				$result_data[$row_no]['shift_id'] = $shift_id;

				if(strtolower($is_absent) == 'yes'){
					$result_data[$row_no]['is_absent'] = '1';
				}else{
					$result_data[$row_no]['is_absent'] = '0';
				}



				if ($written_from_excel != '') {
					$result_data[$row_no]['written'] = $written_from_excel;
				} else {
					$result_data[$row_no]['written'] = 0;
				}

				if ($objective_from_excel != '') {
					$result_data[$row_no]['objective'] = $objective_from_excel;
				} else {
					$result_data[$row_no]['objective'] = 0;
				}

				if ($practical_from_excel != '') {
					$result_data[$row_no]['practical'] = $practical_from_excel;
				} else {
					$result_data[$row_no]['practical'] = 0;
				}

				if ($class_test_from_excel != '') {
					$result_data[$row_no]['class_test'] = $class_test_from_excel;
				} else {
					$result_data[$row_no]['class_test'] = 0;
				}
				$row_no++;
			}
		}
		//For Excel Upload

		$filedel = PUBPATH . 'uploads/excel/' . $file_name;
		if (unlink($filedel)) {
			if($generate_error_msg != ""){
				$sdata['message'] ='No student information found for these (' . $generate_error_msg . ') students';
				$this->session->set_userdata($sdata);
				redirect("markings/index");
			}
		}
		return $result_data;
	}

	public function generateExcelDataForMarking($exam_id, $class_shift_section_id, $group_id, $subject_id, $year)
	{
		$class_shift_section_arr = explode("-", $class_shift_section_id);
		$class_id  = $class_shift_section_arr[0];
		$shift_id = $class_shift_section_arr[1];
		$section_id = $class_shift_section_arr[2];

		$exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$exam_id'")->result_array();
		if ($exam_info[0]['exam_type_id'] <= 0) {
			$sdata['exception'] = "Please input first what type of exam it is.";
			$this->session->set_userdata($sdata);
			redirect('exams/index');
		}
		$exam_type_id = $exam_info[0]['exam_type_id'];

		$marking_data = $this->marking_data_process($exam_id, $class_shift_section_id, $group_id, $subject_id, $year);
		$class_wise_subject_details = $this->db->query("SELECT * FROM `tbl_class_wise_subject` WHERE `class_id` = '$class_id' AND `exam_type_id` = '$exam_type_id' AND `subject_id` = '$subject_id'")->result_array();

		$is_written_allow = $class_wise_subject_details[0]['is_written_allow'];
		$is_objective_allow = $class_wise_subject_details[0]['is_objective_allow'];
		$is_practical_allow = $class_wise_subject_details[0]['is_practical_allow'];
		$is_class_test_allow = $class_wise_subject_details[0]['is_class_test_allow'];


		$written = $class_wise_subject_details[0]['written'];
		$objective = $class_wise_subject_details[0]['objective'];
		$practical = $class_wise_subject_details[0]['practical'];
		$class_test = $class_wise_subject_details[0]['class_test'];

		$data['marking_heads'] = $this->Admin_login->get_main_marking_head($class_id);
		if(empty($data['marking_heads'])){
			$sdata['exception'] = "Please set class wise marking head";
			$this->session->set_userdata($sdata);
			redirect('main_marking_heads/edit/' . $class_id);
		}
		$marking_heads = $data['marking_heads'];

		//For Excel Read
		require_once APPPATH . 'third_party/PHPExcel.php';
		$object = new PHPExcel();
		//For Excel Read

		$object->setActiveSheetIndex(0);

		$table_columns = array("Student ID", "Name", "Roll","Is Absent?");

		if ($is_written_allow != '0') {
			array_push($table_columns, $marking_heads->written . ' (' . $written . ')');
		}

		if ($is_objective_allow != '0') {
			array_push($table_columns, $marking_heads->mcq . ' (' . $objective . ')');
		}

		if ($is_practical_allow != '0') {
			array_push($table_columns, $marking_heads->practical . ' (' . $practical . ')');
		}

		if ($is_class_test_allow != '0') {
			array_push($table_columns, $marking_heads->class_test . ' (' . $class_test . ')');
		}


		// echo '<pre>';
		// print_r($table_columns);
		// die;

		$column = 0;

		foreach ($table_columns as $field) {
			$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			$column++;
		}



		$excel_row = 2;

		foreach ($marking_data as $row) {
			$custom_cell_no = 0;
			$object->getActiveSheet()->setCellValueByColumnAndRow($custom_cell_no, $excel_row, $row['student_code']);
			$custom_cell_no++;
			$object->getActiveSheet()->setCellValueByColumnAndRow($custom_cell_no, $excel_row, $row['name']);
			$custom_cell_no++;
			$object->getActiveSheet()->setCellValueByColumnAndRow($custom_cell_no, $excel_row, $row['roll_no']);

			$custom_cell_no++;
			if($row['is_absent'] == '1'){
				$object->getActiveSheet()->setCellValueByColumnAndRow($custom_cell_no, $excel_row,'Yes');
			}else{
				$object->getActiveSheet()->setCellValueByColumnAndRow($custom_cell_no, $excel_row,'No');
			}

			if ($is_written_allow != '0') {
				$custom_cell_no++;
				$object->getActiveSheet()->setCellValueByColumnAndRow($custom_cell_no, $excel_row, $row['written']);
			}

			if ($is_objective_allow != '0') {
				$custom_cell_no++;
				$object->getActiveSheet()->setCellValueByColumnAndRow($custom_cell_no, $excel_row, $row['objective']);
			}

			if ($is_practical_allow != '0') {
				$custom_cell_no++;
				$object->getActiveSheet()->setCellValueByColumnAndRow($custom_cell_no, $excel_row, $row['practical']);
			}

			if ($is_class_test_allow != '0') {
				$custom_cell_no++;
				$object->getActiveSheet()->setCellValueByColumnAndRow($custom_cell_no, $excel_row, $row['class_test']);
			}

			$excel_row++;
		}

		$object->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$object->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$object->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$object->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$object->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$object->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$object->getActiveSheet()->getColumnDimension('G')->setWidth(15);

		$object->getActiveSheet()
			->getStyle('A1:A500')
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$object->getActiveSheet()
			->getStyle('C1:C500')
			->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$ClassSubjectName = $this->getSubjectWiseMarkingConfig($class_id, $subject_id, $exam_type_id);
		// echo '<pre>';
		// print_r($ClassSubjectName);
		// die;
		$file_name = 'Marking-' . $ClassSubjectName[0]['SubjectName'] . '-' . $ClassSubjectName[0]['ClassName'];
		$object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename=' . $file_name . '.xls');
		$object_writer->save('php://output');
	}




	public function getSubjectWiseMarkingConfig($class_id, $subject_id, $exam_type_id)
	{
		return $this->db->query("SELECT
        cs.`credit`, s.`name` AS SubjectName, s.`code` AS SubjectCode, c.`name` AS ClassName
        FROM
        `tbl_class_wise_subject` AS cs
        INNER JOIN `tbl_class` AS c
          ON c.`id` = cs.`class_id`
        INNER JOIN `tbl_subject` AS s
          ON s.`id` = cs.`subject_id`
        WHERE c.`id` = '$class_id'
        AND s.`id` = '$subject_id'  AND cs.`exam_type_id` = '$exam_type_id'")->result_array();
	}

	public function marking_data_process($exam_id, $class_shift_section_id, $group_id, $subject_id, $year)
	{
		$class_shift_section_arr = explode("-", $class_shift_section_id);
		$class_id  = $class_shift_section_arr[0];
		$shift_id = $class_shift_section_arr[1];
		$section_id = $class_shift_section_arr[2];

		$group_where = "";
		$marking_group_where = "";
		if ($group_id != 'all') {
			$group_where = " AND st.`group` = '$group_id' ";
			$marking_group_where = " AND m.`group_id` = '$group_id' ";
		}

		$marking_data = $this->db->query("SELECT
          st.*, IF(tab2.objective IS NULL, '0', tab2.objective) AS objective,
          IF(tab2.practical IS NULL, '0', tab2.practical) AS practical,
          IF(tab2.written IS NULL, '0', tab2.written) AS written,
          IF(tab2.class_test IS NULL, '0', tab2.class_test) AS class_test,
          IF(tab2.MarkingRow IS NULL, '0', tab2.MarkingRow) AS MarkingRow,
          tab2.is_absent
          FROM
          `tbl_student` AS st
          INNER JOIN
            (SELECT
              ss.`student_id` AS student_id
            FROM
              `tbl_student_wise_subject` AS ss
            WHERE ss.`subject_id` = '$subject_id' AND ss.`year` = '$year') AS tab
            ON tab.student_id = st.`id`
            LEFT JOIN

            (
            SELECT m.`id` AS MarkingRow, m.`objective`, m.`practical`, m.`written`,m.`class_test`, m.`student_id`,m.`is_absent` FROM
          `tbl_marking` AS m
          WHERE m.`exam_id` = '$exam_id'
          AND m.`class_id` = '$class_id'
          AND m.`section_id` = '$section_id'
          $marking_group_where
          AND m.`shift_id` = '$shift_id'
          AND m.`subject_id` = '$subject_id') AS tab2 on tab2.student_id = st.`id`
          WHERE st.`section_id` = '$section_id'
            $group_where AND st.`shift_id` = '$shift_id'
          AND st.`class_id` = '$class_id' AND st.`status` = 1 AND st.`year` = '$year' ORDER BY ABS(st.`roll_no`)")->result_array();
		return $marking_data;
	}
}
