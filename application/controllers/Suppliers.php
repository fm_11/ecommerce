<?php

class Suppliers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model(array('Timekeeping','Message','Inventory','common/insert_model', 'common/custom_methods_model'));
        $this->load->library(array('session'));
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('suppliers');
        $cond = array();
        if ($_POST) {
            $name = $this->input->post("name");
            $sdata['name'] = $name;
            $this->session->set_userdata($sdata);
            $cond['name'] = $name;
        } else {
            $name = $this->session->userdata('name');
            $cond['name'] = $name;
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url('suppliers/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Inventory->get_all_suppliers(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['suppliers'] = $this->Inventory->get_all_suppliers(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('suppliers/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $mobile = $this->input->post('mobile');
            if($this->Inventory->checkifexist_suppliers_by_mobile($mobile))
            {
              $sdata['exception'] = "This mobile number (" . $mobile . ") already exist";
              $this->session->set_userdata($sdata);
              redirect("suppliers/add");
            }
            if (strlen($mobile)>0 && strlen($mobile)<11) {
              $sdata['exception'] = "This mobile number '".$mobile."' is not acceptable.";
              $this->session->set_userdata($sdata);
              redirect("suppliers/add");
            }
            $data['mobile'] = $mobile;
            $data['name']=$this->input->post('name');
            $data['address']=$this->input->post('address');
            $data['note']=$this->input->post('note');
            if ($this->Inventory->add_suppliers($data)) {
                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("suppliers/index");
            } else {
                $sdata['exception'] = $this->lang->line('add_error_message');
                $this->session->set_userdata($sdata);
                redirect("suppliers/add");
            }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('suppliers').' '.$this->lang->line('list');
        $data['action'] = 'add';
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('suppliers/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id)
    {
        if ($_POST) {
          $data = array();
          $mobile=$this->input->post('mobile');
          if($this->Inventory->checkifexist_update_suppliers_by_mobile($mobile,$id))
          {
            $sdata['exception'] = "This mobile number (" . $mobile . " already exist";
            $this->session->set_userdata($sdata);
            redirect("suppliers/edit/".$id);
          }
          if (strlen($mobile)>0 && strlen($mobile)<11) {
            $sdata['exception'] = "This mobile number '".$mobile."' is not acceptable.";
            $this->session->set_userdata($sdata);
            redirect("suppliers/edit/".$id);
          }

          $data['id']=$id;
          $data['mobile']=$mobile;
          $data['name']=$this->input->post('name');
          $data['address']=$this->input->post('address');
          $data['note']=$this->input->post('note');

          if ($this->Inventory->edit_suppliers($data, $id)) {
              $sdata['message'] = $this->lang->line('edit_success_message');
              $this->session->set_userdata($sdata);
              redirect("suppliers/index");
          } else {
              $sdata['exception'] = $this->lang->line('edit_error_message');
              $this->session->set_userdata($sdata);
              redirect("suppliers/index");
          }
        }
        $data = array();
        $data['heading_msg'] = $data['title'] = $this->lang->line('suppliers').' '.$this->lang->line('edit');;
        $data['action'] = 'edit/' . $id;
	     	$data['is_show_button'] = "index";
        $data['row_data'] = $this->Inventory->read_suppliers($id);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('suppliers/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->Inventory->delete_suppliers($id);
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("suppliers/index");
    }

}
