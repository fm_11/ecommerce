<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Unassigned_marks_reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login', 'Student'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = "Unassigned Marks Report";
        $data['heading_msg'] = "Unassigned Marks Report";
        if ($_POST) {
            // echo '<pre>';
            // print_r($_POST);
            // die;
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['address'] = $SchoolInfo[0]['address'];
            $data['HeaderInfo'] = $Info;
            $exam_id = $this->input->post("exam_id");

            $data['exam_id'] = $exam_id;
            $exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$exam_id'")->result_array();
            $exam_type_id = $exam_info[0]['exam_type_id'];
            $data['exam_name'] = $exam_info[0]['name'];

            $ClassSectionInfos = $this->Admin_login->class_section_shift_marge_list();

            $return_data = array();
            $i = 0;
            foreach ($ClassSectionInfos as $class_section_shift) {

              $class_id = $class_section_shift['class_id'];
              $shift_id = $class_section_shift['shift_id'];
              $section_id = $class_section_shift['section_id'];

              $subject_list = $this->Admin_login->getClassAndGroupAndExamTypeWiseSubjectList($class_id, $exam_type_id);

                $group_list = $this->Admin_login->getGroupList();
                foreach ($group_list as $group) {

                    $group_id = $group['id'];

                    if(isset($subject_list[$group_id])){
                      $return_data[$i]['class_shift_section'] = $class_section_shift['class_name'] . '-' . $class_section_shift['shift_name'] . '-' . $class_section_shift['section_name'];
                      $return_data[$i]['group_name'] = $group['name'];
                      $return_data[$i]['total_subject'] = count($subject_list[$group_id]);
                      $completed_subject_list = $this->db->query("SELECT `subject_id` FROM `tbl_marking` WHERE `exam_id` = '$exam_id'
                                                AND `class_id` = '$class_id' AND `shift_id` = '$shift_id' AND `section_id` = '$section_id'
                                                AND `group_id` = '$group_id' GROUP BY `subject_id`")->result_array();

                      $return_data[$i]['completed_subject'] = count($completed_subject_list);
                      $return_data[$i]['remaining_subject'] = count($subject_list[$group_id]) - count($completed_subject_list);


                      $remaining_subject_list = $this->db->query("SELECT `name` FROM `tbl_subject` WHERE `id`
                                                IN (SELECT c.`subject_id` FROM `tbl_class_wise_subject` AS c
                                                WHERE c.`exam_type_id` = '$exam_type_id' AND c.`class_id` = '$class_id'
                                                AND c.`subject_id` NOT IN (SELECT `subject_id` FROM `tbl_marking` WHERE `exam_id` = '$exam_id'
                                                AND `class_id` = '$class_id' AND `shift_id` = '$shift_id' AND `section_id` = '$section_id'
                                                AND `group_id` = '$group_id' GROUP BY `subject_id`)
                                                AND FIND_IN_SET('$group_id',group_list) <> 0)")->result_array();
                      if(!empty($remaining_subject_list)){
                        $return_data[$i]['remaining_subject_list'] = implode(", ", array_column($remaining_subject_list, "name"));
                      }else{
                        $return_data[$i]['remaining_subject_list'] = "-";
                      }


                    }else{
                      $return_data[$i]['class_shift_section'] = $class_section_shift['class_name'] . '-' . $class_section_shift['shift_name'] . '-' . $class_section_shift['section_name'];
                      $return_data[$i]['group_name'] = $group['name'];
                      $return_data[$i]['total_subject'] = "Subject Not Assign";
                      $return_data[$i]['completed_subject'] = "-";
                      $return_data[$i]['remaining_subject'] = "-";
                      $return_data[$i]['remaining_subject_list'] = "-";
                    }

                    $i++;
                }
            }

            $data['return_data'] = $return_data;
            $data['report'] = $this->load->view('unassigned_marks_reports/report', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            $data['is_pdf'] = 1;
            //Dom PDF
            $this->load->library('mydompdf');
            $html = $this->load->view('unassigned_marks_reports/report', $data, true);
            $this->mydompdf->createPDF($html, 'UnassignedMarksReports', true, 'A4', 'portrait');
        //Dom PDF
        } else {
            $current_system_year = $this->Admin_login->getCurrentSystemYear();
            $data['exam_info'] = $this->db->query("SELECT * FROM `tbl_exam` WHERE `year` = '$current_system_year'")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('unassigned_marks_reports/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
