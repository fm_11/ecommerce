<?php

class Service_bridge extends CI_Controller
{

    /**
     * Constructor For ServiceBridge Class
     */
    public function __construct()
    {
        parent::__construct();
        $this->output->enable_profiler(false);
        $this->load->model(array('Timekeeping','Teacher_info','Admin_login','Student'));
    }


    public function index()
    {
        echo 'API';
    }

    public function getHomePageLatestNews()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT * FROM `tbl_home_page_news` ORDER BY `id` DESC")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }



    public function getTotalTeacher()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT COUNT(t.`id`) AS total_teacher FROM `tbl_teacher` AS t
WHERE t.`status` = '1'")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }

    public function getTeacherInfoByAllInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $id = $_POST["id"];
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT t.*,tp.`name` AS post_name,ts.`name` AS section_name,
pc.`name` AS pay_scale,bg.`name` AS blood_group
FROM `tbl_teacher` AS t
LEFT JOIN `tbl_teacher_post` AS tp ON tp.`id` = t.`post`
LEFT JOIN `tbl_teacher_section` AS ts ON ts.`id` = t.`section`
LEFT JOIN `tbl_pay_code` AS pc ON pc.`id` = t.`pay_code_id`
LEFT JOIN `tbl_blood_group` AS bg ON bg.`id` = t.`blood_group_id`
WHERE t.`id` = '$id'")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }


    public function getTeacherAttendanceInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $date = $_POST["date"];
            $login_status = $_POST["login_status"];
            $row_per_page = $_POST["row_per_page"];
            $segment = $_POST["segment"];

            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $this->load->database();

                //parameter
                $cond = array();
                if ($date != '') {
                    $cond['date'] = $date;
                } else {
                    $cond['date'] = "";
                }
                if ($login_status != '') {
                    $cond['login_status'] = $login_status;
                } else {
                    $cond['login_status'] = "";
                }
                //end parameter
                $data = $this->Timekeeping->get_all_teacher_timekeeping_list($row_per_page, $segment, $cond);
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }



    public function getTeacherInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $row_per_page = $_POST["row_per_page"];
            $segment = $_POST["segment"];

            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $this->load->database();

                //parameter
                $cond = array();
                $cond['status'] = 1;
              
                //end parameter
                $data = $this->Teacher_info->get_all_teachers($row_per_page, $segment, $cond);
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }


    public function getStudentInfo()
    {
        if ($_POST) {
            $myPin = '12345';

            $row_per_page = $_POST["row_per_page"];
            $segment = $_POST["segment"];
            $security_pin = $_POST["security_pin"];

            if ($security_pin == $myPin) {
                $this->load->database();
                //parameter
                $cond = array();
                //end parameter
                $data = $this->Student->get_all_student_list($row_per_page, $segment, $cond);
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }
	
	
	public function smsBalanceRecharge()
    {
        if ($_POST) {
            $myPin = '12345';           
            $security_pin = $_POST["security_pin"];
			$transaction_id = $_POST["transaction_id"];
			$quantity = $_POST["quantity"];
			
			$return_array = array();

            if ($security_pin == $myPin) {
				if($transaction_id == '' || $quantity == ''){
				   $return_array['status'] = 'error';
				   $return_array['message'] = 'Please input TransactionID and SMS quantity';
				   $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return_array), JSON_FORCE_OBJECT);
			    }else{
					$this->load->database();
					//data insert
					
					$data = array();
					$data['transaction_id'] = $transaction_id;
					$data['quantity'] = $quantity;
					$data['added_on'] = date('Y-m-d H:i:s');
					$this->db->insert('tbl_sms_balance', $data);
					$insert_id = $this->db->insert_id();
					
					$t_data = array();
					$t_data['type'] = 'I';
					$t_data['quantity'] = $quantity;
					$t_data['date'] = date('Y-m-d');
					$t_data['reference_id'] = $insert_id;
					$this->db->insert('tbl_sms_balance_transaction', $t_data);
					
					$return_array['status'] = 'success';
					$return_array['message'] = 'Balace Recharge Successfull';
					$this->output
						->set_content_type('application/json')
						->set_output(json_encode($return_array), JSON_FORCE_OBJECT);
					//data insert
				}		     
            } else {
                   $return_array['status'] = 'error';
				   $return_array['message'] = 'Please input a correct PIN';
				   $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return_array), JSON_FORCE_OBJECT);
            }
        } else {
                   $return_array['status'] = 'error';
				   $return_array['message'] = 'Direct access not allow';
				   $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($return_array), JSON_FORCE_OBJECT);
        }
    }
	


	public function getNumberOfStudent()
	{
		if ($_POST) {
			$myPin = '12345';
			$security_pin = $_POST["security_pin"];
			if ($security_pin == $myPin) {
				$this->load->database();
				//parameter
				$cond = array();
				$cond['status'] = '1';
				//end parameter
				$number_of_students = count($this->Student->get_all_student_list(0, 0, $cond));
				$data = array();
				$data['number_of_students'] = $number_of_students;
				$this->output
					->set_content_type('application/json')
					->set_output(json_encode($data));
			} else {
				$this->output
					->set_content_type('application/json')
					->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
			}
		} else {
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
		}
	}


    public function getStaffInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT si.*,tp.`name` AS post_name,ts.`name` AS section_name FROM `tbl_staff_info` AS si
LEFT JOIN `tbl_teacher_post` AS tp ON tp.`id` = si.`post`
LEFT JOIN `tbl_teacher_section` AS ts ON ts.`id` = si.`section`")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }

    public function getAllNotice()
    {
        if ($_POST) {
            $myPin = '12345';
            $row_per_page = $_POST["row_per_page"];
            $segment = $_POST["segment"];

            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $this->load->database();

                //parameter
                $cond = array();
                //end parameter
                $data = $this->Admin_login->get_all_notice($row_per_page, $segment, $cond);
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }



    public function getAllDocument()
    {
        if ($_POST) {
            $myPin = '12345';
            $row_per_page = $_POST["row_per_page"];
            $segment = $_POST["segment"];

            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $this->load->database();

                //parameter
                $cond = array();
                //end parameter
                $data = $this->Admin_login->get_all_document($row_per_page, $segment, $cond);
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }


    public function getAllDigitalContent()
    {
        if ($_POST) {
            $myPin = '12345';
            $row_per_page = $_POST["row_per_page"];
            $segment = $_POST["segment"];

            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $this->load->database();

                //parameter
                $cond = array();
                //end parameter
                $data = $this->Admin_login->getAllDigitalContent($row_per_page, $segment, $cond);
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }


    public function getTeacherStaffSection()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT * FROM `tbl_teacher_section`")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }




    public function getTeacherStaffPost()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT * FROM `tbl_teacher_post`")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }




    public function getTeacherForTimekeeping()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            $date = $_POST["date"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT t.* FROM `tbl_teacher` AS t WHERE t.`allowed_for_timekeeping` = '1' AND t.`syn_date` = '$date'
")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }


    public function getStaffForTimekeeping()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            $date = $_POST["date"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT s.* FROM `tbl_staff_info` AS s WHERE s.`allowed_for_timekeeping` = '1' AND s.`syn_date` = '$date'")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }




    public function getTeacherLeaveInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            $date = $_POST["date"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT t.* FROM `tbl_teacher_leave_applications` AS t
JOIN `tbl_teacher` AS tt ON tt.`id` = t.`teacher_id`
WHERE tt.`allowed_for_timekeeping` = '1' AND (t.`syn_date` = '$date' OR t.`deleted_on` = '$date')")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }




    public function getStaffLeaveInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            $date = $_POST["date"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT s.* FROM `tbl_staff_leave_applications` AS s
JOIN `tbl_staff_info` AS si ON si.`id` = s.`staff_id`
WHERE si.`allowed_for_timekeeping` = '1' AND (s.`syn_date` = '$date' OR s.`deleted_on` = '$date')")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }


    public function getHolidayInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT * FROM tbl_holiday")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }



    public function getManagingCommittee()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT * FROM tbl_managing_committee ORDER BY id ASC")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }

    public function getStudentClassInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT * FROM `tbl_class`")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }

    public function getStudentSectionInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT * FROM `tbl_section`")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }


    public function getStudentForTimekeeping()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            $date = $_POST["date"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT s.* FROM `tbl_student` AS s WHERE s.`allowed_for_timekeeping` = '1' AND s.`syn_date` = '$date'
")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }


    public function getStudentLeaveInfo()
    {
        if ($_POST) {
            $myPin = '12345';
            $security_pin = $_POST["security_pin"];
            $date = $_POST["date"];
            if ($security_pin == $myPin) {
                $dbConnection = $this->load->database('default', true);
                $data = $dbConnection->query("SELECT s.* FROM `tbl_student_leave_applications` AS s
JOIN `tbl_student` AS st ON st.`id` = s.`student_id`
WHERE st.`allowed_for_timekeeping` = '1' AND (s.`syn_date` = '$date' OR s.`deleted_on` = '$date')")->result_array();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($data));
            } else {
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
            }
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('status' => '0'), JSON_FORCE_OBJECT));
        }
    }
}
