<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Groups extends CI_Controller
{

     public $SOFTWARE_START_YEAR = '';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }


    public function index()
    {
        $data = array();
        $data['title'] = 'Student Group';
        $data['heading_msg'] = "Student Group";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['groups'] = $this->db->query("SELECT * FROM tbl_student_group  AS g
                        ORDER BY -g.`order` DESC;")->result_array();
        $data['maincontent'] = $this->load->view('groups/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
			$data['order'] = $_POST['order'];
            $this->db->insert("tbl_student_group", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('Groups/index');
        }
        $data = array();
        $data['title'] = 'Add Student Group';
        $data['heading_msg'] = "Add Student Group";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('Groups/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
			$data['order'] = $_POST['order'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_student_group', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('groups/index');
        }
        $data = array();
        $data['title'] = 'Update Student Group';
        $data['heading_msg'] = "Update Student Group";
        $data['is_show_button'] = "index";
        $data['group'] = $this->db->query("SELECT * FROM `tbl_student_group` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('groups/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $group_info = $this->db->where('group', $id)->get('tbl_student')->result_array();
        if (!empty($group_info)) {
            $sdata['exception'] = "Student Depended Data Found !";
            $this->session->set_userdata($sdata);
            redirect("groups/index");
        }
        $this->db->query("DELETE FROM tbl_student_group WHERE `id` = '$id'");
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('groups/index');

    }
}
