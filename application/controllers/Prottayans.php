<?php
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Prottayans extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model(array('Student', 'Admin_login'));
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index(){
		if($_POST){
			$data = array();
			$student_id =  $this->input->post('student_id', true);
			$data['fathers_name'] =  $this->input->post('fathers_name', true);
			$data['mothers_name'] =  $this->input->post('mothers_name', true);
			$data['report_type'] =  $this->input->post('report_type', true);
			$data['address'] =  $this->input->post('address', true);
			$data['student_info'] = $this->Student->get_student_info_by_student_id($student_id);
			$date_of_birth = $data['student_info'][0]['date_of_birth'];
			if($date_of_birth == ''){
				$data['date_of_birth'] = '';
			}else{
				if($data['report_type'] == 'B'){
					$explode_dob = explode("-",$date_of_birth);
					$data['date_of_birth'] = $this->language_number_convert->en2bn($explode_dob[2]) . '/' .$this->language_number_convert->en2bn($explode_dob[1]) . '/' .$this->language_number_convert->en2bn($explode_dob[0]);
				}else{
					$data['date_of_birth'] = date("d/m/Y", strtotime($date_of_birth));
				}
			}
			$data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->row();
			if($data['report_type'] == 'B'){
				$this->load->library('language_number_convert');
				$data['roll_no'] =  $this->language_number_convert->en2bn($data['student_info'][0]['roll_no']);
				$data['title'] = 'প্রত্যয়নপত্র';
				$data['heading_msg'] ='Certification Letter';
				$this->load->view('prottayans/view_bangla', $data);
			}else{
				$data['roll_no'] = $data['student_info'][0]['roll_no'];
				$data['title'] = 'Certification Letter';
				$data['heading_msg'] ='Certification Letter';
				$this->load->view('prottayans/view_english', $data);
			}
		}else{
			$data = array();
			$data['title'] = 'Certification Letter';
			$data['heading_msg'] = 'Certification Letter';
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['is_show_button'] = "index";
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
			$data['maincontent'] = $this->load->view('prottayans/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

}

