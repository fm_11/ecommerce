<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GeneratePdfController extends CI_Controller
{
    public function index()
    {
        $this->load->library('mydompdf');
        $html = $this->load->view('GeneratePdfView', [], true);
        $this->mydompdf->createPDF($html, 'mypdf', false);
    }
}
