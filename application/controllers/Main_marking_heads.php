<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Main_marking_heads extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index(){
		$data = array();
		$data['title'] = "Main Marking Head";
		$class_list = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
		$i = 0;
		$class_array = array();
		foreach ($class_list as $class){
			$class_id = $class['id'];
			$class_array[$i]['id'] = $class_id;
			$class_array[$i]['name'] = $class['name'];

			$data_list = $this->db->query("SELECT CONCAT(`written`, ', ' , `mcq` ,', ' ,`practical` ,', ' ,`class_test`) AS data_list
FROM `tbl_exam_main_marking_head` WHERE `class_id` = $class_id;")->row();
            if(empty($data_list)){
				$class_array[$i]['data_list'] = '-';
			}else{
				$class_array[$i]['data_list'] = $data_list->data_list;
			}

			$i++;
		}
		$data['is_show_button'] = "add";
		$data['class_list'] = $class_array;
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('main_marking_head/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add(){
		if($_POST){
			$classes =  $_POST['class_id'];
			foreach ($classes as $class_id){
				$data = array();
				$data['class_id'] = $class_id;
				$data['written'] = $_POST['written'];
				$data['mcq'] = $_POST['mcq'];
				$data['practical'] = $_POST['practical'];
				$data['class_test'] = $_POST['class_test'];
				$this->db->query("DELETE FROM `tbl_exam_main_marking_head` WHERE `class_id` = $class_id;");
				$this->db->insert("tbl_exam_main_marking_head", $data);
			}
			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect('main_marking_heads/add');
		}

		$data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`
WHERE `id` NOT IN (SELECT `class_id` FROM `tbl_exam_main_marking_head` GROUP BY `class_id`);")->result_array();
		$data['is_show_button'] = "index";
		$data['title'] = "Main Marking Head";
		$data['heading_msg'] = "Main Marking Head";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('main_marking_head/add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

    public function edit($class_id=null)
    {
        if ($_POST) {
			$class_id = $_POST['class_id'];
			$this->db->query("DELETE FROM `tbl_exam_main_marking_head` WHERE `class_id` = $class_id;");
            $data = array();
            $data['id'] = $_POST['id'];
			$data['class_id'] = $class_id;
            $data['written'] = $_POST['written'];
            $data['mcq'] = $_POST['mcq'];
            $data['practical'] = $_POST['practical'];
            $data['class_test'] = $_POST['class_test'];
			$this->db->insert("tbl_exam_main_marking_head", $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('main_marking_heads/index');
        }
        $data = array();
        $data['title'] = "Main Marking Head";
		$data['is_show_button'] = "index";
		$data['class_id'] = $class_id;
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['heads'] = $this->Admin_login->get_main_marking_head($class_id);
//        echo '<pre>';
//        print_r($data['heads']);
//        die;
        $data['maincontent'] = $this->load->view('main_marking_head/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
