<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Messages extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Message', 'Timekeeping', 'Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        date_default_timezone_set('Asia/Dhaka');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }








    public function index()
    {
        $data = array();
        $message_config = $this->db->query("SELECT * FROM tbl_message_config")->result_array();
        if (empty($message_config)) {
            $sdata['exception'] = "Message panel configuration not found to database.";
            $this->session->set_userdata($sdata);
            redirect("dashboard/index");
        }

        if ($message_config[0]['user_id'] != '') {
            //echo 555;die;
            $url = "http://api.smscpanel.net/g_api.php?token=". $message_config[0]['api_key'] ."&balance";
            $ch = curl_init(); // Initialize cURL
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
            $smsresult = curl_exec($ch);
            if (SCHOOL_NAME == 'greenlandms') {
                $data['sms_balance'] = ($smsresult / .40) . " SMS";
            } else {
                $data['sms_balance'] = $smsresult;
            }
        } else {
            $api_url = $message_config[0]['server_url'] . "/miscapi/" . $message_config[0]['api_key'] . "/getBalance";
            $ch = curl_init($api_url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $result = curl_exec($ch);
            curl_close($ch);
            $rdata = explode(":", $result);
            //echo '<pre>';
            // print_r($rdata);
            //die;
            $data['sms_balance'] = $rdata[1];
        }

        $data['title'] = 'SMS/Messages';
        $data['heading_msg'] = "SMS/Messages";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }














    
    public function getTemplateBodyById()
    {
        $template_id = $this->input->get('template_id', true);
        $template_info = $this->db->query("SELECT template_body FROM tbl_message_template WHERE `id` = '$template_id'")->result_array();
        echo $template_info[0]['template_body'];
    }

    public function all_type_send_sms()
    {
        $data = array();
        $data['title'] = 'SMS Send';
        $data['heading_msg'] = "SMS Send";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/all_type_send_sms_index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }








    public function AddNewContactFromExcel()
    {
        if ($_POST) {

            //For Excel Read
            require_once APPPATH . 'third_party/PHPExcel.php';
            $this->excel = new PHPExcel();
            //For Excel Read

            $category_id = $this->input->post('category_id', true);

            //For Excel Upload
            $configUpload['upload_path'] = FCPATH . 'uploads/excel/';
            $configUpload['allowed_types'] = 'xls|xlsx|csv';
            $configUpload['max_size'] = '15000';
            $this->load->library('upload', $configUpload);
            $this->upload->do_upload('txtFile');
            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name']; //uploded file name
            $extension = $upload_data['file_ext'];    // uploded file extension

            if ($extension == '.xlsx') {
                $objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
            } else {
                $objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
            }


            //Set to read only
            $objReader->setReadDataOnly(true);
            //Load excel file
            $objPHPExcel = $objReader->load(FCPATH . 'uploads/excel/' . $file_name);
            $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            //loop from first data untill last data
            $num = "";
            for ($i = 2; $i <= $totalrows; $i++) {
                $name = ltrim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");
                if ($name == '') {
                    break;
                }
                $number = ltrim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue(), " ");
                if ($this->validate_mobile_number($number)) {
                    $insertData = array();
                    $insertData['contact_name'] = $name;
                    $insertData['contact_number'] = $number;
                    $insertData['category_id'] = $category_id;
                    $tableName = "tbl_message_contact_list";
                    $this->Message->DoCommonInsert($insertData, $tableName);
                }
            }
            //For Excel Upload

            $filedel = PUBPATH . 'uploads/excel/' . $file_name;
            if (unlink($filedel)) {
                $sdata['message'] = "All Contact Has Been Added Successfully.";
                $this->session->set_userdata($sdata);
                redirect('messages/AddNewContactFromExcel', 'refresh');
            }
        }
        $data = array();
        $data['title'] = 'Add New Contact From Excel';
        $data['heading_msg'] = "Add New Contact From Excel";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['categoryList'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['maincontent'] = $this->load->view('messages/AddNewContactFromExcel', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function AddNewContact()
    {
        if (isset($_POST['submit'])) {
            /* echo '<pre>';
            print_r($_POST);
            die(); */
            $insertData['contact_name'] = $_POST['ContactName'];
            $insertData['contact_number'] = $_POST['ContactNumber'];
            $insertData['category_id'] = $_POST['PhoneBookCategory'];
            $tableName = "tbl_message_contact_list";
            $returnData = $this->Message->DoCommonInsert($insertData, $tableName);
            if ($returnData['status'] == 1) {
                $sdata['message'] = "New Contact Has Been Added Successfully.";
            } else {
                $sdata['exception'] = "New Contact Can't Be Added Successfully.";
            }
            $this->session->set_userdata($sdata);

            redirect('messages/AddNewContact', 'refresh');
        }
        $data = array();
        $data['title'] = 'Add New Contact';
        $data['heading_msg'] = "Add New Contact";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['categoryList'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['maincontent'] = $this->load->view('messages/AddNewContact', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function validate_mobile_number($phoneNumber)
    {
        if (!empty($phoneNumber)) { // phone number is not empty
            if (preg_match('/^\d{11}$/', $phoneNumber)) { // phone number is valid
                return true;
            // your other code here
            } else { // phone number is not valid
                return false;
            }
        } else { // phone number is empty
            return false;
        }
    }

    public function DeletePhoneBookData($id)
    {
        $Check = $this->db->query("SELECT `id` FROM `tbl_message_contact_list` WHERE `id` = '$id' LIMIT 1")->result_array();
        /* echo '<pre>';
        print_r($Check); */
        if (!empty($Check)) {
            if ($this->db->query("DELETE FROM tbl_message_contact_list WHERE `id` = '$id'")) {
                $sdata['message'] = "Data Deleted Successfully.";
            } else {
                $sdata['exception'] = "Data Can't Be Deleted Successfully!";
            }
            $this->session->set_userdata($sdata);
            redirect('messages/PhoneBookIndex/', 'refresh');
        } else {
            $sdata['exception'] = "Invalid Data Which You Want To Delete !";
            $this->session->set_userdata($sdata);
            redirect('messages/PhoneBookIndex/', 'refresh');
        }
    }

    public function EditPhoneBookData($id)
    {
        if ($_POST) {
            /* echo '<pre>';
            print_r($_POST);
            die(); */
            $updateData = array();
            $updateData['contact_name'] = $_POST['ContactName'];
            $updateData['contact_number'] = $_POST['ContactNumber'];
            $updateData['category_id'] = $_POST['PhoneBookCategory'];
            $this->db->where('id', $id);
            $this->db->update('tbl_message_contact_list', $updateData);
            $sdata['message'] = "Data Updated Successfully.";
            $this->session->set_userdata($sdata);
            redirect('messages/PhoneBookIndex', 'refresh');
        }
        $data = array();
        $data['title'] = 'Edit Phone Book';
        $data['heading_msg'] = "Edit Phone Book";
        $data['EditData'] = $this->db->query("SELECT * FROM tbl_message_contact_list WHERE `id` = '$id'")->row_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['categoryList'] = $this->db->query("SELECT * FROM tbl_message_category")->result_array();
        $data['maincontent'] = $this->load->view('messages/EditPhoneBookData', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function GetGuardianSMSReport()
    {
        $data = array();
        if (isset($_POST['submit'])) {
            /*  */
            $FromDate = $this->input->post('FromDate', true);
            $ToDate = $this->input->post('ToDate', true);
            $data['GuardianSMSReport'] = $this->Message->GetGuardianSMSReport($FromDate, $ToDate);
            /* echo '<pre>';
            print_r($data['GuardianSMSReport']);
            die(); */
        }

        $data['title'] = 'Guardian SMS Report';
        $data['heading_msg'] = "Guardian SMS Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        /* echo '<pre>';
        print_r($data); */
        $data['maincontent'] = $this->load->view('messages/GetGuardianSMSReport', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function GetDetailsSMSReport()
    {
        if (isset($_POST['submit'])) {
            /*  */
            $FromDate = $this->input->post('FromDate', true);
            $ToDate = $this->input->post('ToDate', true);
            $data['DetailsSMSReport'] = $this->Message->GetDetailsSMSReport($FromDate, $ToDate);
            /* echo '<pre>';
            print_r($data['DetailsSMSReport']);
            die(); */
        }


        $data['title'] = 'Details SMS Report';
        $data['heading_msg'] = "Details SMS Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/GetDetailsSMSReport', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function SMSSummaryReport()
    {
        if (isset($_POST['submit'])) {
            /*  */
            $FromDate = $this->input->post('FromDate', true);
            $ToDate = $this->input->post('ToDate', true);
            $data['DetailsSMSReport'] = $this->Message->GetDetailsSMSReport($FromDate, $ToDate);
            /* echo '<pre>';
            print_r($data['DetailsSMSReport']);
            die(); */
        }
        $data['title'] = 'SMS Summary Report';
        $data['heading_msg'] = "SMS Summary Report";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('messages/SMSSummaryReport', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    //////// edit by dip (11-06-17)////////////////
}
