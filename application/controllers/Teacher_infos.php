<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Teacher_infos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Teacher_info'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }




    public function teacher_id_card_print()
    {
        if ($_POST) {
            $IsPrintFor = $this->input->post('IsPrintFor', true);
            $Part = $this->input->post('Part', true);
            $IdCardFor = $this->input->post('IdCardFor', true);

            $data = array();

            $data['title'] = 'Teacher ID Card';
            $data['heading_msg'] = "Teacher ID Card";




            if ($IdCardFor == 'T') {
                $data['list'] = $this->db->query("SELECT t.*, p.name as post_name FROM tbl_teacher as t
                left join tbl_teacher_post as p on p.id = t.post
                ")->result_array();
                $data['photo_folder'] = 'teacher';
            } else {
                $data['list'] = $this->db->query("SELECT t.*, p.name as post_name FROM tbl_staff_info as t
                left join tbl_teacher_post as p on p.id = t.post
                ")->result_array();
                $data['photo_folder'] = 'staff';
            }



            $data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->result_array();

            if ($Part == 'F') {
                if ($IsPrintFor == 'Z' && SCHOOL_NAME == 'greenlandms') {
                    $this->load->view('teacher_infos/teacher_id_card_print', $data);
                } elseif ($IsPrintFor == 'Z' && SCHOOL_NAME == 'jtsc') {
                    $this->load->view('teacher_infos/teacher_id_card_print', $data);
                } elseif ($IsPrintFor == 'Z') {
                    $this->load->view('teacher_infos/teacher_id_card_print', $data);
                } else {
                    $this->load->view('teacher_infos/teacher_id_card_print', $data);
                }
            } else {
                $this->load->view('teacher_infos/greenlandms_teacher_id_card_back_part_print', $data);
            }
        } else {
            $data = array();
            $data['title'] = 'Teacher ID Card Print';
            $data['heading_msg'] = "Teacher ID Card Print";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('teacher_infos/teacher_id_card_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }







    public function staff_info()
    {
        $data = array();
        $data['title'] = 'Staff List';
        $data['heading_msg'] = "Staff List";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['staff_info'] = $this->db->query("SELECT si.*,tp.`name` AS post_name,ts.`name` AS section_name FROM `tbl_staff_info` AS si
LEFT JOIN `tbl_teacher_post` AS tp ON tp.`id` = si.`post`
LEFT JOIN `tbl_teacher_section` AS ts ON ts.`id` = si.`section`")->result_array();
        $data['maincontent'] = $this->load->view('teacher_infos/staff_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function staff_add()
    {
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/staff/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
            $config['max_size'] = '6000';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));

            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Staff_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/staff/" . $new_photo_name)) {
                        $data = array();
                        $data['name'] = $this->input->post('txtName', true);
                        $data['staff_index_no'] = $this->input->post('txtIndexNum', true);
                        $data['process_code'] = $this->input->post('process_code', true);
                        $data['post'] = $this->input->post('txtPost', true);
                        $data['pay_code_id'] = $this->input->post('txtPayCode', true);
                        $data['date_of_birth'] = $this->input->post('txtDOB', true);
                        $data['date_of_join'] = $this->input->post('txtDOJ', true);
                        $data['section'] = $this->input->post('txtSection', true);
                        $data['mobile'] = $this->input->post('txtMobile', true);
                        $data['address'] = $this->input->post('txtAddress', true);
                        $data['allowed_for_timekeeping'] = $this->input->post('allowed_for_timekeeping', true);
                        if ($this->input->post('allowed_for_timekeeping', true) == 1) {
                            $data['syn_date'] = $this->input->post('syn_date', true);
                        }
                        $data['photo_location'] = $new_photo_name;
                        $this->db->insert('tbl_staff_info', $data);

                        //shift_add
                        $staff_id = $this->db->insert_id();
                        $shiftdata = array();
                        $j = 0;
                        while ($j < count($_POST['txtShift'])) {
                            $shiftdata[$j]['staff_id'] = $staff_id;
                            $shiftdata[$j]['shift_id'] = $_POST['txtShift'][$j];
                            $j++;
                        }
                        $this->db->insert_batch('tbl_staff_shift', $shiftdata);
                        //shift_add

                        $sdata['message'] = "You are Successfully Staff Information Added ! ";
                        $this->session->set_userdata($sdata);
                        redirect("teacher_infos/staff_add");
                    } else {
                        $sdata['exception'] = "Sorry Staff Doesn't Added !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("teacher_infos/staff_add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Photo Does't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("teacher_infos/staff_add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Add New Staff Information';
            $data['heading_msg'] = "Add New Staff Information";
            $data['action'] = '';
            $data['shift'] = $this->db->query("SELECT * FROM tbl_shift")->result_array();
            $data['sections'] = $this->db->query("SELECT * FROM tbl_teacher_section")->result_array();
            $data['pay_codes'] = $this->db->query("SELECT * FROM tbl_pay_code")->result_array();
            $data['post'] = $this->db->query("SELECT * FROM tbl_teacher_post WHERE is_teacher = '0'")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('teacher_infos/staff_info_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function staff_delete($id)
    {
        $staff_photo_info = $this->Teacher_info->get_staff_photo_info_by_staff_id($id);
        $file = $staff_photo_info[0]['photo_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/staff/' . $file;
            if (unlink($filedel)) {
                $this->Teacher_info->delete_staff_info_by_staff_id($id);
            } else {
                $this->Teacher_info->delete_staff_info_by_staff_id($id);
            }
        } else {
            $this->Teacher_info->delete_staff_info_by_staff_id($id);
        }

        $sdata['message'] = "Staff Information Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("teacher_infos/staff_info");
    }

    public function staff_edit($id = null)
    {
        if ($_POST) {
            $file = $_FILES["txtPhoto"]['name'];
            if ($file != '') {
                $staff_photo_info = $this->Teacher_info->get_staff_photo_info_by_staff_id($this->input->post('id', true));
                $old_file = $staff_photo_info[0]['photo_location'];
                if (!empty($old_file)) {
                    $filedel = PUBPATH . MEDIA_FOLDER . '/staff/' . $old_file;
                    unlink($filedel);
                }
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/staff/';
                $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
                $config['max_size'] = '6000';
                $config['max_width'] = '4000';
                $config['max_height'] = '4000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = "Staff_" . time() . "_" . date('Y-m-d') . "." . $extension;
                        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/staff/" . $new_photo_name)) {
                            $data = array();
                            $data['id'] = $this->input->post('id', true);
                            $data['name'] = $this->input->post('txtName', true);
                            $data['staff_index_no'] = $this->input->post('txtIndexNum', true);
                            $data['process_code'] = $this->input->post('process_code', true);
                            $data['post'] = $this->input->post('txtPost', true);
                            $data['pay_code_id'] = $this->input->post('txtPayCode', true);
                            $data['date_of_birth'] = $this->input->post('txtDOB', true);
                            $data['date_of_join'] = $this->input->post('txtDOJ', true);
                            $data['section'] = $this->input->post('txtSection', true);
                            $data['mobile'] = $this->input->post('txtMobile', true);
                            $data['address'] = $this->input->post('txtAddress', true);
                            $data['allowed_for_timekeeping'] = $this->input->post('allowed_for_timekeeping', true);
                            if ($this->input->post('allowed_for_timekeeping', true) == 1) {
                                $data['syn_date'] = $this->input->post('syn_date', true);
                            }
                            $data['photo_location'] = $new_photo_name;
                            $this->Teacher_info->update_staff_info($data);


                            //Shift
                            $this->db->delete('tbl_staff_shift', array('staff_id' => $data['id']));
                            $shiftdata = array();
                            $staff_id = $data['id'];
                            $i = 0;
                            while ($i < count($_POST['txtShift'])) {
                                $shiftdata[$i]['staff_id'] = $staff_id;
                                $shiftdata[$i]['shift_id'] = $_POST['txtShift'][$i];
                                $i++;
                            }
                            $this->db->insert_batch('tbl_staff_shift', $shiftdata);
                            //Shift

                            $sdata['message'] = "You are Successfully Staff Information Updated ! ";
                            $this->session->set_userdata($sdata);
                            redirect("teacher_infos/staff_info");
                        } else {
                            $sdata['exception'] = "Sorry Staff Doesn't Updated !" . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("teacher_infos/staff_info");
                        }
                    } else {
                        $sdata['exception'] = "Sorry Photo Does't Upload !";
                        $this->session->set_userdata($sdata);
                        redirect("teacher_infos/staff_info");
                    }
                }
            } else {
                $data = array();
                $data['id'] = $this->input->post('id', true);
                $data['name'] = $this->input->post('txtName', true);
                $data['staff_index_no'] = $this->input->post('txtIndexNum', true);
                $data['process_code'] = $this->input->post('process_code', true);
                $data['post'] = $this->input->post('txtPost', true);
                $data['pay_code_id'] = $this->input->post('txtPayCode', true);
                $data['date_of_birth'] = $this->input->post('txtDOB', true);
                $data['date_of_join'] = $this->input->post('txtDOJ', true);
                $data['section'] = $this->input->post('txtSection', true);
                $data['mobile'] = $this->input->post('txtMobile', true);
                $data['address'] = $this->input->post('txtAddress', true);
                $data['allowed_for_timekeeping'] = $this->input->post('allowed_for_timekeeping', true);
                if ($this->input->post('allowed_for_timekeeping', true) == 1) {
                    $data['syn_date'] = $this->input->post('syn_date', true);
                }
                $this->Teacher_info->update_staff_info($data);

                //Shift
                $this->db->delete('tbl_staff_shift', array('staff_id' => $data['id']));
                $shiftdata = array();
                $staff_id = $data['id'];
                $i = 0;
                while ($i < count($_POST['txtShift'])) {
                    $shiftdata[$i]['staff_id'] = $staff_id;
                    $shiftdata[$i]['shift_id'] = $_POST['txtShift'][$i];
                    $i++;
                }
                $this->db->insert_batch('tbl_staff_shift', $shiftdata);
                //Shift

                $sdata['message'] = "You are Successfully Staff Information Updated ! ";
                $this->session->set_userdata($sdata);
                redirect("teacher_infos/staff_info");
            }
        } else {
            $data = array();
            $data['title'] = 'Update Staff Information';
            $data['heading_msg'] = "Update Staff Information";
            $data['action'] = 'edit';
            $data['staff_info'] = $this->Teacher_info->get_staff_photo_info_by_staff_id($id);

            $data['shift'] = $this->db->query("SELECT s.*,ts.`shift_id` FROM `tbl_shift` AS s
LEFT JOIN `tbl_staff_shift` AS ts ON ts.`shift_id` = s.`id` AND ts.`staff_id` = '$id'
ORDER BY s.`id`")->result_array();

            $data['sections'] = $this->db->query("SELECT * FROM tbl_teacher_section")->result_array();
            $data['pay_codes'] = $this->db->query("SELECT * FROM tbl_pay_code")->result_array();
            $data['post'] = $this->db->query("SELECT * FROM tbl_teacher_post WHERE is_teacher = '0'")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('teacher_infos/staff_info_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function teacher_section_info()
    {
        $data = array();
        $data['title'] = 'Teacher Section List';
        $data['heading_msg'] = "Teacher Section List";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['sections'] = $this->db->query("SELECT * FROM tbl_teacher_section")->result_array();
        $data['maincontent'] = $this->load->view('teacher_infos/teacher_section_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function educational_qualification()
    {
        $data = array();
        $data['title'] = 'Educational Qualification List';
        $data['heading_msg'] = "Educational Qualification List";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['qualifications'] = $this->db->query("SELECT * FROM tbl_educational_qualification")->result_array();
        $data['maincontent'] = $this->load->view('teacher_infos/educational_qualification_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function educational_qualification_add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('name', true);
            $this->db->insert('tbl_educational_qualification', $data);
            $sdata['message'] = "You are Successfully Data Added ! ";
            $this->session->set_userdata($sdata);
            redirect("teacher_infos/educational_qualification_add");
        } else {
            $data = array();
            $data['title'] = 'Educational Qualification';
            $data['heading_msg'] = "Add New Educational Qualification";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['action'] = '';
            $data['maincontent'] = $this->load->view('teacher_infos/educational_qualification_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function educational_qualification_edit($id=null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('name', true);
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_educational_qualification', $data);
            $sdata['message'] = "You are Successfully Data Updated ! ";
            $this->session->set_userdata($sdata);
            redirect("teacher_infos/educational_qualification");
        } else {
            $data = array();
            $data['title'] = 'Educational Qualification';
            $data['heading_msg'] = "Update Educational Qualification";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['action'] = 'edit';
            $data['qua'] = $this->db->query("SELECT * FROM tbl_educational_qualification WHERE id='$id'")->result_array();
            $data['maincontent'] = $this->load->view('teacher_infos/educational_qualification_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function educational_qualification_delete($id)
    {
        $qua = $this->db->query("SELECT * FROM tbl_teacher_educational_qua WHERE qua_id='$id'")->result_array();
        if (!empty($qua)) {
            $sdata['exception'] = "Data Doesn't Deleted. Depended data found !";
            $this->session->set_userdata($sdata);
            redirect("teacher_infos/educational_qualification");
        }
        $this->db->delete('tbl_educational_qualification', array('id' => $id));
        $sdata['message'] = "Data Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("teacher_infos/educational_qualification");
    }

    public function teacher_section_add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('txtSection', true);
            $this->db->insert('tbl_teacher_section', $data);
            $sdata['message'] = "You are Successfully Section Added ! ";
            $this->session->set_userdata($sdata);
            redirect("teacher_infos/teacher_section_add");
        } else {
            $data = array();
            $data['title'] = 'Teacher Section';
            $data['heading_msg'] = "Add New Teacher Section";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['action'] = '';
            $data['maincontent'] = $this->load->view('teacher_infos/teacher_section_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function teacher_section_delete($id)
    {
        $this->db->delete('tbl_teacher_section', array('id' => $id));
        $sdata['message'] = "Teacher Section Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("teacher_infos/teacher_section_info");
    }

    public function teacher_section_edit($id=null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('txtSection', true);
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_teacher_section', $data);
            $sdata['message'] = "You are Successfully Section Updated ! ";
            $this->session->set_userdata($sdata);
            redirect("teacher_infos/teacher_section_info");
        } else {
            $data = array();
            $data['title'] = 'Teacher Section';
            $data['heading_msg'] = "Update Teacher Section";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['action'] = 'edit';
            $data['section'] = $this->db->query("SELECT * FROM tbl_teacher_section WHERE id='$id'")->result_array();
            $data['maincontent'] = $this->load->view('teacher_infos/teacher_section_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }



    public function pay_code_info()
    {
        $data = array();
        $data['title'] = 'Pay Code List';
        $data['heading_msg'] = "Pay Code List";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['pay_codes'] = $this->db->query("SELECT * FROM tbl_pay_code")->result_array();
        $data['maincontent'] = $this->load->view('teacher_infos/pay_code_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function pay_code_delete($id)
    {
        $this->db->delete('tbl_pay_code', array('id' => $id));
        $sdata['message'] = "Pay Code Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("teacher_infos/pay_code_info");
    }

    public function pay_code_add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('txtPayCodeName', true);
            $this->db->insert('tbl_pay_code', $data);
            $sdata['message'] = "You are Successfully pay Code Added ! ";
            $this->session->set_userdata($sdata);
            redirect("teacher_infos/pay_code_add");
        } else {
            $data = array();
            $data['title'] = 'Pay Code';
            $data['heading_msg'] = "Add New Pay Code";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['action'] = '';
            $data['maincontent'] = $this->load->view('teacher_infos/pay_code_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function pay_code_edit($id=null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('txtPayCodeName', true);
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_pay_code', $data);
            $sdata['message'] = "You are Successfully Pay Code Updated ! ";
            $this->session->set_userdata($sdata);
            redirect("teacher_infos/pay_code_info");
        } else {
            $data = array();
            $data['title'] = 'Pay Code';
            $data['heading_msg'] = "Update Pay Code";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['action'] = 'edit';
            $data['pay_codes'] = $this->db->query("SELECT * FROM tbl_pay_code WHERE id='$id'")->result_array();
            $data['maincontent'] = $this->load->view('teacher_infos/pay_code_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
