<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Landing_page extends CI_Controller {

     function __construct() {

        parent::__construct();
        $data = array();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session'));
    }
	
	public function index(){
	    $data = array();
	    $this->load->view('landing_page/index', $data);
	}

}
