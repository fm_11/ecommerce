<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Educational_qualifications extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Educational Qualification';
        $data['heading_msg'] = "Educational Qualification";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['qualifications'] = $this->db->query("SELECT * FROM tbl_educational_qualification")->result_array();
        $data['maincontent'] = $this->load->view('educational_qualifications/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $this->db->insert("tbl_educational_qualification", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('educational_qualifications/index');
        }
        $data = array();
        $data['title'] = 'Add Educational Qualification';
        $data['heading_msg'] = "Add Educational Qualification";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('educational_qualifications/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_educational_qualification', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('educational_qualifications/index');
        }
        $data = array();
        $data['title'] = 'Update Educational Qualification';
        $data['heading_msg'] = "Update Educational Qualification";
        $data['is_show_button'] = "index";
        $data['qualifications'] = $this->db->query("SELECT * FROM `tbl_educational_qualification` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('educational_qualifications/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
        $this->db->delete('tbl_educational_qualification', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('educational_qualifications/index');
    }
}
