<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Progress_report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Message','Admin_login','common/custom_methods_model','Config_general'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('progress_report');
        $data['heading_msg'] = $this->lang->line('progress_report');
        if ($_POST) {
            $year = $this->input->post('year', true);
            $exam = $this->input->post('exam', true);

			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class  = $class_shift_section_arr[0];
			$shift = $class_shift_section_arr[1];
			$section = $class_shift_section_arr[2];

            $group = $this->input->post('group', true);
            $from_roll = $this->input->post('from_roll', true);
            $to_roll = $this->input->post('to_roll', true);

            $view_type = $this->input->post('view_type', true);
            $data['year'] = $year;

            $student_id = 0;
            $st_where = "";
            if ($view_type == 'S') {
                if ($from_roll > $to_roll) {
                    $sdata['exception'] = "From roll cannot be greater than To roll";
                    $this->session->set_userdata($sdata);
                    redirect("progress_report/index");
                }
                $roll_where_in_string = "";
                while ($from_roll <= $to_roll) {
                    $roll_where_in_string .= "'" . $from_roll ."',";
                    $from_roll++;
                }
                $roll_where_in_string_for_q = rtrim($roll_where_in_string, ',');
                //die;
                $st_where = " AND s.`roll_no` IN ($roll_where_in_string_for_q)";
            }



            $student_info = $this->db->query("SELECT s.`id` FROM `tbl_student` AS s WHERE
              s.`class_id` = '$class' AND s.`section_id` = '$section' AND
        s.`group` = '$group' AND s.`shift_id` = '$shift' AND  s.`status` = 1 $st_where")->result_array();
            if (empty($student_info)) {
                $sdata['exception'] = "Sorry this student not found !";
                $this->session->set_userdata($sdata);
                redirect("progress_report/index");
            }

            $data['header_info'] = $this->Admin_login->fetReportHeader();

            $data['exam_list'] = $this->db->query("SELECT e.*,et.`name` AS exam_type_name, et.`type` FROM `tbl_exam` AS e
LEFT  JOIN `tbl_exam_type` AS et ON e.`exam_type_id` = et.`id`
WHERE e.`year` = '$year' AND e.`is_applicable_for_final_calcultion` = '1'
ORDER BY e.`exam_order`")->result_array();
            // echo '<pre>';
            // print_r($data['exam_list']);
            // die;

            $retun_data_for_all_student = array();
            $retun_data_loop = 0;
            $result_found = 0;
            $exam_type_id = 0;
            $design_type = 2;


            foreach ($student_info as $student) {
                $student_id = $student['id'];
                if ($design_type == 1) {
                    $result_process_weekly_monthly_data = array();
                    $result_process_semester_data = array();
                    $wm = 0;
                    $sm = 0;
                    foreach ($data['exam_list'] as $exam) {
                        $exam_id = $exam['id'];

                        if ($exam['type'] != "S") {
                            $result_info = $this->Student->result_all_student_after_process($exam_id, $class, $section, $group, $shift, $student_id);
                            if (!empty($result_info)) {
                                $result_process_weekly_monthly_data[$wm] = $result_info;
                                $result_process_weekly_monthly_data[$wm]['exam_name'] = $exam['name'];

                                //highst mark

                                $highest_mark = $this->db->query("SELECT s.`subject_code`,MAX(s.`total_obtain`) as highest_mark,
                                s.`student_id` FROM `tbl_result_process_details` AS s
                                WHERE s.`exam_id` = '$exam_id' AND s.`class_id` = '$class' AND s.`section_id` = '$section' AND
                                 s.`group` = '$group' AND s.`shift` = '$shift' GROUP BY s.`subject_code`")->result_array();

                                $highest_marks = array();
                                foreach ($highest_mark as $highest_mark_row):
                                   $highest_marks[$highest_mark_row['subject_code']] = $highest_mark_row['highest_mark'];
                                endforeach;

                                $result_process_weekly_monthly_data[$wm]['highest_marks'] = $highest_marks;
                                //highst mark


                                $wm++;
                            }
                        } else {
                            $semester_result_info = $this->Student->result_all_student_after_process($exam_id, $class, $section, $group, $shift, $student_id);
                            if (!empty($semester_result_info)) {
                                $result_process_semester_data[$sm] = $semester_result_info;
                                $result_process_semester_data[$sm]['exam_name'] = $exam['name'];

                                //highst mark

                                $highest_mark = $this->db->query("SELECT s.`subject_code`,MAX(s.`total_obtain`) as highest_mark,
                                s.`student_id` FROM `tbl_result_process_details` AS s
                                WHERE s.`exam_id` = '$exam_id' AND s.`class_id` = '$class' AND s.`section_id` = '$section' AND
                                 s.`group` = '$group' AND s.`shift` = '$shift' GROUP BY s.`subject_code`")->result_array();

                                $highest_marks = array();
                                foreach ($highest_mark as $highest_mark_row):
                                   $highest_marks[$highest_mark_row['subject_code']] = $highest_mark_row['highest_mark'];
                                endforeach;

                                $result_process_semester_data[$sm]['highest_marks'] = $highest_marks;
                                //highst mark

                                $sm++;
                            }
                        }
                    }
                    if (!empty($result_process_weekly_monthly_data)) {
                        $data['student_result_data'][$retun_data_loop]['result_process_weekly_monthly_data'] = $result_process_weekly_monthly_data;
                    }
                    if (!empty($result_process_semester_data)) {
                        $data['student_result_data'][$retun_data_loop]['result_process_semester_data'] = $result_process_semester_data;
                    }
                    $retun_data_loop++;
                } else {
                    $result_process_all_data = array();
                    $asm = 0;
                    foreach ($data['exam_list'] as $exam) {
                        $exam_id = $exam['id'];
                        if($exam['is_annual_exam'] == '1'){
							$exam_type_id = $exam['exam_type_id'];
						}
                        $result_info = $this->Student->result_all_student_after_process($exam_id, $class, $section, $group, $shift, $student_id);

//						echo '<pre>';
//						print_r($result_info);
//						die;

                        $minimum_one_exam_data_found = 0;
                        if (!empty($result_info)) {
							$result_found++;
							$minimum_one_exam_data_found = 1;
                            $result_process_all_data[$asm] = $result_info;
                            $result_process_all_data[$asm]['exam_name'] = $exam['name'];

                            //highst mark

                            $highest_mark = $this->db->query("SELECT s.`subject_code`,MAX(s.`total_obtain`) as highest_mark,
                              s.`student_id` FROM `tbl_result_process_details` AS s
                              WHERE s.`exam_id` = '$exam_id' AND s.`class_id` = '$class' AND s.`section_id` = '$section' AND
                               s.`group` = '$group' AND s.`shift` = '$shift' GROUP BY s.`subject_code`")->result_array();

                            $highest_marks = array();
                            foreach ($highest_mark as $highest_mark_row):
                                 $highest_marks[$highest_mark_row['subject_code']] = $highest_mark_row['highest_mark'];
                            endforeach;

                            $result_process_all_data[$asm]['highest_marks'] = $highest_marks;
                            //highst mark
                            $asm++;
                        }else{
							$result_process_all_data[$asm][0]['result'] = array();
                            $result_process_all_data[$asm]['exam_name'] = $exam['name'];
							$result_process_all_data[$asm]['highest_marks'] = array();
							$asm++;
						}
                    }
                    if (!empty($result_process_all_data) && $minimum_one_exam_data_found == 1) {
                        $data['student_result_data'][$retun_data_loop]['result_process_all_data'] = $result_process_all_data;
                    }

                    $retun_data_loop++;
                }
            }

//            echo '<pre>';
//			print_r($data['student_result_data']);
//			die;


			if($result_found == 0){
			   $sdata['exception'] = "Sorry result not found !";
			   $this->session->set_userdata($sdata);
			   redirect("progress_report/index");
		   }



            $class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class'")->result_array();
            $data['class_name'] = $class_info[0]['name'];

            $section_info = $this->db->query("SELECT name FROM tbl_section WHERE id ='$section'")->result_array();
            $data['section_name'] = $section_info[0]['name'];

            $group_info = $this->db->query("SELECT name FROM tbl_student_group WHERE id ='$group'")->result_array();
            $data['group_name'] = $group_info[0]['name'];

            $shift_info = $this->db->query("SELECT name FROM tbl_shift WHERE id ='$shift'")->result_array();
            $data['shift_name'] = $shift_info[0]['name'];

            $grand_exam_id = $this->input->post('exam', true);
            $data['subject_list'] = $this->db->query("SELECT p.`subject_name`,p.`subject_code`,
              p.`subject_id`,p.`order_number`,p.`credit`,p.`subject_type`,p.`is_optional` FROM `tbl_result_process_details` AS  p
               WHERE p.`class_id` = $class AND p.`exam_id` = '$grand_exam_id'
GROUP BY p.`subject_id` ORDER BY p.`order_number`")->result_array();


            $data['subject_list_for_grand'] = $this->db->query("SELECT p.`subject_name`,p.`subject_code`,
              p.`subject_id`,p.`order_number`,p.`credit`,p.`subject_type`,p.`is_optional` FROM `tbl_result_process_details` AS  p
               WHERE p.`class_id` = $class AND p.`exam_id` IN (SELECT e.id FROM `tbl_exam` AS e
   WHERE e.`year` = '$year' AND e.`is_applicable_for_final_calcultion` = '1'
   ORDER BY e.`exam_order`) GROUP BY p.`subject_id` ORDER BY p.`order_number`")->result_array();



			$class_wise_marking_head = $this->Admin_login->getClassWiseSubjectMarkingHead($class, $exam_type_id);


			$colspan_for_exam_nam_part = 0;
			$data['class_test_marks_show'] = $class_wise_marking_head['is_class_test_allow'];
			$data['written_marks_show'] = $class_wise_marking_head['is_written_allow'];
			$data['objective_marks_show'] = $class_wise_marking_head['is_objective_allow'];
			$data['practical_marks_show'] = $class_wise_marking_head['is_practical_allow'];

			if($data['class_test_marks_show'] > 0){
				$colspan_for_exam_nam_part += 1;
			}
			if($data['written_marks_show'] > 0){
				$colspan_for_exam_nam_part += 1;
			}
			if($data['objective_marks_show'] > 0){
				$colspan_for_exam_nam_part += 1;
			}
			if($data['practical_marks_show'] > 0){
				$colspan_for_exam_nam_part += 1;
			}

			$data['colspan_for_exam_nam_part'] = $colspan_for_exam_nam_part + 3;

			$data['signature'] = $this->Admin_login->getSignatureByAccessCode('GFR'); //GFR= Grand Final Result

      $data['main_marking_heads'] = $this->Admin_login->get_main_marking_head($class);
      if(empty($data['main_marking_heads'])){
        $sdata['exception'] = "Please set class wise marking head";
        $this->session->set_userdata($sdata);
        redirect('main_marking_heads/edit/' . $class);
      }
			$mark_sheet_config = $this->Admin_login->getMarkSheetDesignConfig($class,$year,'GFM');
			$data['mark_sheet_config'] = $mark_sheet_config;
			if(!empty($mark_sheet_config)){
				if($mark_sheet_config['template_category'] == 'T1'){
					$this->load->view('progress_report/progress_report_design_1', $data);
				}else if($mark_sheet_config['template_category'] == 'T2'){
					$this->load->view('progress_report/progress_report_design_2', $data);
				}
				else if($mark_sheet_config['template_category'] == 'T3'){
					$this->load->view('progress_report/progress_report_design_3', $data);
				}
            }else{
				 $sdata['exception'] = "Sorry marksheet configuration not found. Please complete configuration first";
				 $this->session->set_userdata($sdata);
				 redirect("marksheet_designs/index");
			}
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
            $data['group'] = $this->Admin_login->getGroupList();
            $data['shift'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
            $data['maincontent'] = $this->load->view('progress_report/index', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function get_only_annual_exam_by_year()
    {
        $year = $_GET['year'];
        $data = array();
        $data['exam_list'] = $this->db->query("SELECT * FROM `tbl_exam` AS e WHERE e.`is_annual_exam` = '1' AND e.`year` = '$year' AND e.`status` = '1'")->result_array();
        $this->load->view('mark_sheets/ajax_exam_by_year', $data);
    }
}
