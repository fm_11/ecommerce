
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Student_attendance_sheets extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student', 'Admin_login','Students_report'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Student Attendance Sheet';
		$data['heading_msg'] = 'Student Attendance Sheet';
		if ($_POST) {
			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;
			$year = $this->input->post("year");
			$month = $this->input->post("month");

			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];


			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['year'] = $year;
			$data['shift_id'] = $shift_id;
			$data['month'] = $month;
			$data['year'] = $this->input->post("year");
			$data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['number_of_days'] = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			$data['class_name'] =$this->Students_report->get_class_name($class_id);
			$data['section_name'] =$this->Students_report->get_section_name($section_id);
			$data['shift_name'] =$this->Students_report->get_shift_name($shift_id);
			$data['month_name'] =$this->Students_report->get_month_name_by_id($month);
			$data['adata'] = $this->Students_report->get_student_attendance_sheet($data);
			$data['report'] = $this->load->view('student_attendance_sheets/student_attendance_sheet_table', $data, true);
			if(isset($_POST['pdf_download'])){
				$data['is_pdf'] = 1;
				$this->load->library('mydompdf');
				$this->load->view('report_content/report_main_content', $data);
				$html = $this->load->view('report_content/report_main_content', $data, true);
				$this->mydompdf->createPDF($html, 'student_attendance_sheets', true, 'A4', 'landscape');
			}else{
				$this->load->view('report_content/report_main_content', $data);
			}
		}
		else {
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['GroupList'] = $this->Admin_login->getGroupList();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('student_attendance_sheets/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}
}
