<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Expense_reports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        if ($_POST) {
            //echo '<pre>';
            // print_r($_POST);
            // die;

            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];

            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");
            $expense_category_id = $this->input->post("expense_category_id");
            $cost_center = $this->input->post("cost_center");
            $user_id = $this->input->post("user_id");

            if ($cost_center == 'all') {
                $Info['cost_center_name'] = "All";
            } else {
                $cost_center_info = $this->db->query("SELECT * FROM `tbl_cost_center` WHERE id = '$cost_center'")->result_array();
                $Info['cost_center_name'] = $cost_center_info [0]['name'];
            }

            $data['HeaderInfo'] = $Info;

            $data['from_date'] = $this->input->post("from_date");
            $data['to_date'] = $this->input->post("to_date");
            $data['expense_category_id'] = $this->input->post("expense_category_id");
            $data['cost_center_id'] = $this->input->post("cost_center");
            $data['user_id'] = $user_id;

            $where = "";
            if ($expense_category_id != 'all') {
                $where .= " AND dd.`expense_category_id` = '$expense_category_id'";
            }

            if ($cost_center != 'all') {
                $where .= " AND e.`cost_center_id` = '$cost_center'";
            }

            if ($user_id != 'all') {
                $where .= " AND e.`user_id` = '$user_id'";
            }


            $data['idata'] = $this->db->query("SELECT dd.*,ic.`name` AS category_name,
               dm.`name` AS deposit_method_name,e.`date` AS voucher_date FROM `tbl_ba_expense_details` AS dd
INNER JOIN `tbl_ba_expense` AS e ON e.`id` = dd.`expense_id`
INNER JOIN `tbl_ba_expense_category` AS ic ON dd.`expense_category_id` = ic.`id`
INNER JOIN `tbl_ba_deposit_method` AS dm ON dd.`deposit_method_id` = dm.`id`
WHERE (e.`date` BETWEEN '$from_date' AND '$to_date') $where ORDER BY e.`date` ASC")->result_array();
            //echo '<pre>';
            // print_r($data['idata']);
            // die;

            $data['report'] = $this->load->view('expense_reports/generate_basic_acc_expense_report', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            //echo 'gggg'; die;
            // Load all views as normal
            $data['is_pdf'] = 1;
            $html = $this->load->view('expense_reports/generate_basic_acc_expense_report', $data, true);
            $pdfFilePath = "ExpenseReport.pdf";
            $this->load->library('m_pdf');
            $this->m_pdf->pdf->setFooter('Print Date: {DATE j-m-Y}, Page {PAGENO} of {nb}');
            //generate the PDF from the given html
            $this->m_pdf->pdf->autoScriptToLang = true;
            $this->m_pdf->pdf->WriteHTML($html);
            //download it.
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
        } else {
            $data['title'] =  $this->lang->line('expense') . ' ' . $this->lang->line('report');
            $data['heading_msg'] =  $this->lang->line('expense') . ' ' . $this->lang->line('report');
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['users'] = $this->db->query("SELECT * FROM tbl_user")->result_array();
            $data['expense_category'] = $this->db->query("SELECT * FROM `tbl_ba_expense_category`")->result_array();
            $data['cost_center'] = $this->db->query("SELECT * FROM `tbl_cost_center`")->result_array();
            $data['maincontent'] = $this->load->view('expense_reports/basic_acc_expense_report', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
