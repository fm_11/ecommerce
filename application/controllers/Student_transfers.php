<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student_transfers extends CI_Controller
{
    
     public $SOFTWARE_START_YEAR = '';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student', 'Message','Admin_login','Student_transfer'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        //echo '<pre>'; print_r($user_info); die;
        $this->SOFTWARE_START_YEAR = $user_info[0]->config_info[0]['software_start_year'];
        
    }
    
   

    public function index()
    {	
        $data = array();
        $data['title'] = 'Student Transfer Information';
        $data['heading_msg'] = "Student Transfer Information";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_transfers/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Student_transfer->get_all_transfer_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['transfers'] = $this->Student_transfer->get_all_transfer_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('student_transfers/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
    
    	function add(){
		if($_POST){
			$old_class_id = $this->input->post('old_class_id', true);
            $old_section_id = $this->input->post('old_section_id', true);
            $old_group_id = $this->input->post('old_group_id', true);
            $old_shift_id = $this->input->post('old_shift_id', true);
            
            $new_class_id = $this->input->post('new_class_id', true);
            $new_section_id = $this->input->post('new_section_id', true);
            $new_group_id = $this->input->post('new_group_id', true);
            $new_shift_id = $this->input->post('new_shift_id', true);
            
            if($old_class_id == $new_class_id){
                $sdata['exception'] = "Transfer to the same class is not possible";
                $this->session->set_userdata($sdata);
                redirect("student_transfers/add");
            }
            
            $student_id = $this->input->post('student_id', true);
            $update_payment_transac = $this->input->post('update_payment_transac', true);
            
            $student_info = $this->db->query("SELECT id,year FROM `tbl_student` WHERE id = '$student_id'")->result_array();
			$student_current_year = $student_info[0]['year'];
			
			$data = array();
			$data['old_class_id'] = $old_class_id;
            $data['old_section_id'] = $old_section_id;
            $data['old_group_id'] = $old_group_id;
            $data['old_shift_id'] = $old_shift_id;
            
            $data['new_class_id'] = $new_class_id;
            $data['new_section_id'] = $new_section_id;
            $data['new_group_id'] = $new_group_id;
            $data['new_shift_id'] = $new_shift_id;
            
            $data['student_id'] = $student_id;
            $data['update_payment_transaction'] = $update_payment_transac;
            $data['year'] = $student_current_year;
            $data['reason'] = $this->input->post('reason', true);
			$data['date'] = date('Y-m-d H:i:s');

		    if($this->db->insert('tbl_student_transfer', $data)){
		        $student_info = array();
		        $student_info['class_id'] = $new_class_id;
                $student_info['section_id'] = $new_section_id;
                $student_info['group'] = $new_group_id;
                $student_info['shift_id'] = $new_shift_id;
                $this->db->where('id', $student_id);
                $this->db->update('tbl_student', $student_info);
                
                if($update_payment_transac == 1){
                    $this->db->trans_start();
                    $this->db->query("UPDATE tbl_fee_collection SET class_id = '$new_class_id', section_id = '$new_section_id',
                                      group_id = '$new_group_id', shift_id = '$new_shift_id' WHERE student_id = '$student_id' AND year = '$student_current_year'");
                                      
                    $this->db->query("UPDATE tbl_fee_collection_details SET class_id = '$new_class_id', section_id = '$new_section_id'
                                      WHERE student_id = '$student_id' AND year = '$student_current_year'");
                    $this->db->trans_complete(); 
                    $sdata['message'] = "The transfer and payment information update completed.";
                    $this->session->set_userdata($sdata);
                    redirect("student_transfers/index");
		        }else{
		            $sdata['message'] = "The transfer is completed.";
                    $this->session->set_userdata($sdata);
                    redirect("student_transfers/index");
		        } 
                
		    }else{
		        $sdata['exception'] = "Transfer data does not save.";
                $this->session->set_userdata($sdata);
                redirect("student_transfers/add");
		    }
		}else{		
		    $data = array();
            $data['title'] = 'Student Transfer';
            $data['heading_msg'] = "Student Transfer";
            $data['action'] = '';
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['shift_list'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
    		$data['year_list'] = $this->Admin_login->get_year_drop_down_list($this->SOFTWARE_START_YEAR,true);
		    $data['students'] = null;
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_transfers/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);	
		}
	}
	
	public function delete($id){
	    $data = $this->db->query("SELECT * FROM `tbl_student_transfer` WHERE id = '$id'")->result_array();
	    if(empty($data)){
	        $sdata['exception'] = "Transfer data not found.";
            $this->session->set_userdata($sdata);
            redirect("student_transfers/index");
	    }else{
	        
	        $old_class_id = $data[0]['old_class_id'];
	        $old_section_id = $data[0]['old_section_id'];
	        $old_group_id = $data[0]['old_group_id'];
	        $old_shift_id = $data[0]['old_shift_id'];
	        $year = $data[0]['year'];
	        $student_id = $data[0]['student_id'];
	        
	        $student_info = $this->db->query("SELECT id,year FROM `tbl_student` WHERE id = '$student_id'")->result_array();
			$student_current_year = $student_info[0]['year'];
			if($student_current_year != $year){
			    $sdata['exception'] = "Transfer delete not possible, because his class has changed.";
                $this->session->set_userdata($sdata);
                redirect("student_transfers/index");
			}
	        
	        $student_info = array();
	        $student_info['class_id'] = $old_class_id;
            $student_info['section_id'] = $old_section_id;
            $student_info['group'] =  $old_group_id;
            $student_info['shift_id'] =  $old_shift_id;
            $this->db->where('id', $student_id);
            $this->db->update('tbl_student', $student_info);
            
            if($data[0]['update_payment_transaction'] == 1){
                $this->db->trans_start();
                $this->db->query("UPDATE tbl_fee_collection SET class_id = '$old_class_id', section_id = '$old_section_id',
                                  group_id = '$old_group_id', shift_id = '$old_shift_id' WHERE student_id = '$student_id' AND year = '$year'");
                                  
                $this->db->query("UPDATE tbl_fee_collection_details SET class_id = '$old_class_id', section_id = '$old_section_id'
                                  WHERE student_id = '$student_id' AND year = '$year'");
                $this->db->trans_complete(); 
                $this->db->query("DELETE FROM tbl_student_transfer WHERE `id` = '$id'");
                $sdata['message'] = "Transfer data deleted and payment information update completed.";
                $this->session->set_userdata($sdata);
                redirect("student_transfers/index"); 
            }else{
                $this->db->query("DELETE FROM tbl_student_transfer WHERE `id` = '$id'");
                $sdata['message'] = "Transfer data deleted.";
                $this->session->set_userdata($sdata);
                redirect("student_transfers/index");
            }
	    }
	}
    

}
