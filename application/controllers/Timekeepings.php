<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Timekeepings extends CI_Controller
{
    public $is_device_integrated_attendance_system;
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model',));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');


        //For Excel Read
        require_once APPPATH . 'third_party/PHPExcel.php';
        $this->excel = new PHPExcel();
        //For Excel Read


        $user_info = $this->session->userdata('user_info');
        $this->is_device_integrated_attendance_system = $user_info[0]->config_info[0]['is_device_integrated_attendance_system'];
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Timekeeping Information';
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['heading_msg'] = "Timekeeping Information";
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }




    public function upload_timekeeping_data()
    {
        if ($_FILES) {
            // echo 66666; die;
            $this->load->library('upload');
            $config['upload_path'] = 'uploads/excel/';
            $config['allowed_types'] = 'xls|xlsx';
            $config['max_size'] = '2048000000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "Data_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], "uploads/excel/" . $new_file_name)) {

                       //echo $new_file_name;
                        //die;

                        if ($extension == '.xlsx') {
                            $objReader = PHPExcel_IOFactory::createReader('Excel2007');    // For excel 2007
                        } else {
                            $objReader = PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
                        }

                        //Set to read only
                        $objReader->setReadDataOnly(true);
                        //Load excel file
                        $objPHPExcel = $objReader->load(FCPATH . 'uploads/excel/' . $new_file_name);
                        $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
                        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);


                        //loop from first data untill last data
                        for ($i = 2; $i <= $totalrows; $i++) {
                            $user_id = ltrim($objWorksheet->getCellByColumnAndRow(0, $i)->getValue(), " ");
                            $card_num = ltrim($objWorksheet->getCellByColumnAndRow(1, $i)->getValue(), " ");
                            $badgenumber = ltrim($objWorksheet->getCellByColumnAndRow(2, $i)->getValue(), " ");
                            $login = trim($objWorksheet->getCellByColumnAndRow(3, $i)->getValue());
                            $logout = trim($objWorksheet->getCellByColumnAndRow(4, $i)->getValue());

                            $UNIX_DATE = ($login - 25569) * 86400;
                            $login_time = gmdate("Y-m-d H:i:s", $UNIX_DATE);

                            $date = gmdate("Y-m-d", $UNIX_DATE);

                            $UNIX_DATE = ($logout - 25569) * 86400;
                            $logout_time = gmdate("Y-m-d H:i:s", $UNIX_DATE);

                            $data = array('user_id' => $user_id, 'card_num' => $card_num, 'process_code' => $badgenumber,
                        'login_time' => $login_time, 'logout_time' => $logout_time ,'date' => $date);

                            //$this->db->insert('tbl_attendance_data', $data);


                            //login data insert
                            $login_data = $this->process_data($badgenumber, $login_time, $logout_time, $date);

                            if (!empty($login_data)) {
                                $person_id = $login_data['person_id'];
                                $this->db->query("DELETE FROM `tbl_logins` WHERE `date` = '$date' AND `person_id` = '$person_id' ");
                                $this->insert_model->add("tbl_logins", $login_data);
                            }
                            //end
                        //	echo '<pre>';
                    //	print_r($login_data);
                    //	die;
                        }

                        //echo '<pre>';
                        //print_r($data);
                        //die;
                        //For Excel Upload

                        $filedel = PUBPATH . 'uploads/excel/' . $new_file_name;
                        if (unlink($filedel)) {
                            $sdata['message'] = "You are Successfully Data Uploaded ! ";
                            $this->session->set_userdata($sdata);
                            redirect("timekeepings/upload_timekeeping_data");
                        }
                    } else {
                        $sdata['exception'] = "Sorry File Doesn't Upload !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("timekeepings/upload_timekeeping_data");
                    }
                } else {
                    $sdata['exception'] = "Sorry File Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("timekeepings/upload_timekeeping_data");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Upload Attendance Data';
            $data['heading_msg'] = "Upload Attendance Data";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('timekeepings/upload_attendance_data', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function process_data($process_code, $login, $logout, $day)
    {
        $get_info = $this->get_is_teacher_staff_or_student($process_code);
        $expected_time_info = $this->db->query("SELECT * FROM `tbl_timekeeping_config`")->result_array();
        if (!empty($get_info)) {
            $data = array(
                'person_id' => $get_info[0]['id'],
                'date' => $day,
                'login_time' => $login,
                'logout_time' => $logout,
                'expected_login_time' => $expected_time_info[0]['expected_login_time'],
                'expected_logout_time' => $expected_time_info[0]['expected_logout_time'],
                'process_code' => $process_code,
                'person_type' => $get_info[0]['person_type']
            );
        } else {
            $data = array();
        }
        return $data;
    }


    public function get_is_teacher_staff_or_student($process_code)
    {
        $teacher_info = $this->db->query("SELECT t.`id`,t.`name`,t.`process_code`,'T' AS 'person_type'  FROM `tbl_teacher` AS t WHERE t.`process_code` = '$process_code'")->result_array();
        if (empty($teacher_info)) {
            $staff_info = $this->db->query("SELECT s.`id`,s.`name`,s.`process_code`,'ST' AS 'person_type'  FROM `tbl_staff_info` AS s WHERE s.`staff_index_no` = '$process_code'")->result_array();
            if (empty($staff_info)) {
                $student_info = $this->db->query("SELECT s.`id`,s.`name`,s.`process_code`,'S' AS 'person_type'  FROM `tbl_student` AS s WHERE s.`process_code` = '$process_code'")->result_array();
                return $student_info;
            } else {
                return $staff_info;
            }
        } else {
            return $teacher_info;
        }
    }


    public function retrieve_attendance_data()
    {
        if ($_POST) {
            $startTime = array_sum(explode(' ', microtime()));
            $device_data =  $this->db->query("SELECT * FROM `tbl_attendance_data`")->result_array();
            foreach ($device_data as $row) {
                if (!empty($row['process_code'])) {
                    $process_code = $row['process_code'];
                    $login = $row['login_time'];
                    $logout = $row['logout_time'];
                    $day = $row['date'];
                    $login_data = $this->process_data($process_code, $login, $logout, $day);
                    if (!empty($login_data)) {
                        $person_id = $login_data['person_id'];
                        $this->db->query("DELETE FROM `tbl_logins` WHERE `date` = '$date' AND `person_id` = '$person_id' ");
                        $this->insert_model->add("tbl_logins", $login_data);
                    }
                }
            }
            $totalTime = array_sum(explode(' ', microtime())) - $startTime;
            $totalTime = round($totalTime / 1000, 4);
            $sdata['message'] = 'Time Keeping Data Retrieve Successfully in ' . $totalTime . ' seconds';
            $this->session->set_userdata($sdata);
            redirect("timekeepings/retrieve_attendance_data");
        } else {
            $data = array();
            $data['title'] = 'Retrieve Attendance Data';
            $data['heading_msg'] = "Retrieve Attendance Data";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('timekeepings/retrieve_attendance_data', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }



    public function staff_leave_list()
    {
        $data = array();
        $data['title'] = 'Staff Leave';
        $data['heading_msg'] = "Staff Leave";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('timekeepings/staff_leave_list/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Timekeeping->get_all_staff_leave_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['leaves'] = $this->Timekeeping->get_all_staff_leave_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('timekeepings/staff_leave_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function student_leave_list()
    {
        $data = array();
        $data['title'] = 'Student Leave';
        $data['heading_msg'] = "Student Leave";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('timekeepings/student_leave_list/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Timekeeping->get_all_student_leave_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['leaves'] = $this->Timekeeping->get_all_student_leave_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('timekeepings/student_leave_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function student_leave_add()
    {
        if ($_POST) {
            $student_id = $this->input->post('student_id', true);
            $date_from = $this->input->post('txtFromDate', true);
            $date_to = $this->input->post('txtToDate', true);
            $login_info = $this->db->query("SELECT id FROM tbl_logins WHERE person_id = '$student_id' AND person_type = 'S'
AND (date BETWEEN '$date_from' AND '$date_to')")->result_array();
            if (!empty($login_info)) {
                $sdata['exception'] = "Login Information Found for this student !";
                $this->session->set_userdata($sdata);
                redirect("timekeepings/student_leave_add");
            }

            $data = array();
            $data['student_id'] = $student_id;
            $data['class_id'] = $this->input->post('class_id', true);
            $data['section_id'] = $this->input->post('section_id', true);
            $data['group'] = $this->input->post('group', true);
            $data['date_from'] = $date_from;
            $data['date_to'] = $date_to;
            $data['status'] = 1;
            $data['reason'] = $this->input->post('txtReason', true);
            $this->db->insert('tbl_student_leave_applications', $data);

            //leave days save
            $insert_id = $this->db->insert_id();
            $leave_days = $this->get_leave_days($date_from, $date_to, $insert_id, $student_id);
            $this->db->insert_batch('tbl_student_leave_days', $leave_days);


            $sdata['message'] = "You are Successfully Added Leave Info.";
            $this->session->set_userdata($sdata);
            redirect("timekeepings/student_leave_add");
        } else {
            $data = array();
            $data['title'] = 'Student Leave';
            $data['heading_msg'] = "Student Leave";
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('timekeepings/student_leave_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function get_leave_days($from_date, $to_date, $insert_id, $student_id)
    {
        $begin = new DateTime($from_date);
        $end   = new DateTime($to_date);

        $leave_days = array();
        $leave_count = 0;
        for ($l = $begin; $l <= $end; $l->modify('+1 day')) {
            $leave_days[$leave_count]['student_id'] = $student_id;
            $leave_days[$leave_count]['leave_id'] = $insert_id;
            $leave_days[$leave_count]['date'] = $l->format("Y-m-d");
            $leave_count++;
        }
        return $leave_days;
    }

    public function student_leave_delete($id)
    {
        $data = array();
        $data['id'] = $id;
        $data['is_deleted'] = 1;
        $data['deleted_on'] = date('Y-m-d');
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_student_leave_applications', $data);
        $sdata['message'] = "Student Leave Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("timekeepings/student_leave_list");
    }

    public function getStudentByClassSectionAndGroup()
    {
        $class_id = $this->input->get('class_id', true);
        $section_id = $this->input->get('section_id', true);
        $group = $this->input->get('group', true);
        $data = array();
        $data['students'] = $this->db->query("SELECT id,name,roll_no FROM tbl_student WHERE `class_id` = '$class_id' AND section_id = '$section_id' AND `group` = '$group' AND `status` = 1 ORDER BY ABS(roll_no)")->result_array();
        $this->load->view('timekeepings/student_list_for_combo', $data);
    }



    public function teacher_leave_add()
    {
        if ($_POST) {
            $data = array();
            $data['teacher_id'] = $this->input->post('txtTeacherName', true);
            $data['date_from'] = $this->input->post('txtFromDate', true);
            $data['date_to'] = $this->input->post('txtToDate', true);
            $data['syn_date'] = $this->input->post('syn_date', true);
            $data['reason'] = $this->input->post('txtReason', true);
            $this->db->insert('tbl_teacher_leave_applications', $data);
            $sdata['message'] = "You are Successfully Added Leave Info.";
            $this->session->set_userdata($sdata);
            redirect("timekeepings/teacher_leave_add");
        } else {
            $data = array();
            $data['title'] = 'Teacher Leave';
            $data['heading_msg'] = "Teacher Leave";
            $data['teachers'] = $this->db->query("SELECT id,name,teacher_index_no FROM tbl_teacher")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('timekeepings/teacher_leave_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function teacher_leave_delete($id)
    {
        $data = array();
        $data['id'] = $id;
        $data['is_deleted'] = 1;
        $data['deleted_on'] = date('Y-m-d');
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_teacher_leave_applications', $data);
        $sdata['message'] = "Teacher Leave Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("timekeepings/teacher_leave_list");
    }

    public function staff_leave_delete($id)
    {
        $data = array();
        $data['id'] = $id;
        $data['is_deleted'] = 1;
        $data['deleted_on'] = date('Y-m-d');
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_staff_leave_applications', $data);
        $sdata['message'] = "Staff Leave Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("timekeepings/staff_leave_list");
    }

    public function staff_leave_add()
    {
        if ($_POST) {
            $data = array();
            $data['staff_id'] = $this->input->post('staff_id', true);
            $data['date_from'] = $this->input->post('txtFromDate', true);
            $data['date_to'] = $this->input->post('txtToDate', true);
            $data['syn_date'] = $this->input->post('syn_date', true);
            $data['reason'] = $this->input->post('txtReason', true);
            $this->db->insert('tbl_staff_leave_applications', $data);
            $sdata['message'] = "You are Successfully Added Leave Info.";
            $this->session->set_userdata($sdata);
            redirect("timekeepings/staff_leave_add");
        } else {
            $data = array();
            $data['title'] = 'Staff Leave';
            $data['heading_msg'] = "Staff Leave";
            $data['staff'] = $this->db->query("SELECT id,name,staff_index_no FROM tbl_staff_info")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('timekeepings/staff_leave_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function student_timekeeping_list()
    {
        $data = array();
        $data['title'] = 'Student Timekeeping';
        $data['heading_msg'] = "Student Timekeeping";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('timekeepings/student_timekeeping_list/');
        $config['per_page'] = 20;

        $data['is_device_integrated_attendance_system'] = $this->is_device_integrated_attendance_system;
        if ($this->is_device_integrated_attendance_system == "Y") {
            $config['total_rows'] = count($this->Timekeeping->get_all_student_timekeeping_list_for_device_data(0, 0, $cond));
            $this->pagination->initialize($config);
            $data['timekeepings'] = $this->Timekeeping->get_all_student_timekeeping_list_for_device_data(20, (int)$this->uri->segment(3), $cond);
        } else {
            $config['total_rows'] = count($this->Timekeeping->get_all_student_timekeeping_list(0, 0, $cond));
            $this->pagination->initialize($config);
            $data['timekeepings'] = $this->Timekeeping->get_all_student_timekeeping_list(20, (int)$this->uri->segment(3), $cond);
        }

        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('timekeepings/student_timekeeping_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }





    public function validate_mobile_number($phoneNumber)
    {
        if (!empty($phoneNumber)) { // phone number is not empty
            if (preg_match('/^\d{11}$/', $phoneNumber)) { // phone number is valid
                return true;
            // your other code here
            } else { // phone number is not valid
                return false;
            }
        } else { // phone number is empty
            return false;
        }
    }



    public function staff_timekeeping_list()
    {
        $data = array();
        $data['title'] = 'Staff Timekeeping';
        $data['heading_msg'] = "Staff Timekeeping";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('timekeepings/staff_timekeeping_list/');
        $config['per_page'] = 20;

        $data['is_device_integrated_attendance_system'] = $this->is_device_integrated_attendance_system;
        if ($this->is_device_integrated_attendance_system == "Y") {
            $config['total_rows'] = count($this->Timekeeping->get_all_staff_timekeeping_list_for_device_data(0, 0, $cond));
            $this->pagination->initialize($config);
            $data['timekeepings'] = $this->Timekeeping->get_all_staff_timekeeping_list_for_device_data(20, (int)$this->uri->segment(3), $cond);
        } else {
            $config['total_rows'] = count($this->Timekeeping->get_all_staff_timekeeping_list(0, 0, $cond));
            $this->pagination->initialize($config);
            $data['timekeepings'] = $this->Timekeeping->get_all_staff_timekeeping_list(20, (int)$this->uri->segment(3), $cond);
        }

        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('timekeepings/staff_timekeeping_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function staff_timekeepings_add()
    {
        $data = array();
        $data['title'] = 'Staff Timekeeping';
        $date = date('Y-m-d');
        $data['date'] = $date;
        $data['heading_msg'] = "Staff Manual Login";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('timekeepings/staff_manual_login', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add_staff_timekeepings()
    {
        $data = array();
        $data['title'] = 'Staff Timekeeping';
        $date = $this->input->post("txtDate");
        $data['date'] = $date;
        $data['heading_msg'] = "Staff Manual Login";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['staff_info'] = $this->db->query("SELECT s.*,sl.`date`,sl.`login_status` AS process_status FROM `tbl_staff_info` AS s
LEFT JOIN `tbl_staff_logins` AS sl ON sl.`staff_id` = s.`id` AND sl.`date` = '$date'")->result_array();
        $data['maincontent'] = $this->load->view('timekeepings/staff_timekeeping_form', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function add_staff_login_data()
    {
        $date = $this->input->post("date");
        $loop_time = $this->input->post("loop_time");
        $i = 1;
        while ($i <= $loop_time) {
            $data = array();
            $data['staff_id'] = $this->input->post("staff_id_" . $i);
            $data['date'] = $date;
            $data['login_status'] = $this->input->post("login_status_" . $i);
            $this->db->delete('tbl_staff_logins', array('staff_id' => $data['staff_id'], 'date' => $date));
            $this->db->insert('tbl_staff_logins', $data);
            $i++;
        }
        $sdata['message'] = "Staff Login Info. Successfully Updated.";
        $this->session->set_userdata($sdata);
        redirect("timekeepings/staff_timekeeping_list");
    }









    public function movement_register_list()
    {
        $data = array();
        $data['title'] = 'Teacher/Staff Movement Register';
        $data['heading_msg'] = "Teacher/Staff Movement Register";
        $cond = array();
        if ($_POST) {
            $person_type = $this->input->post("person_type");
            $name = $this->input->post("name");
            $date = $this->input->post("date");
            $cond['person_type'] = $person_type;
            $cond['name'] = $name;
            $cond['date'] = $date;
            $sdata['person_type'] = $person_type;
            $sdata['name'] = $name;
            $sdata['date'] = $date;
            $this->session->set_userdata($sdata);
        } else {
            $person_type = $this->session->userdata('person_type');
            $name = $this->session->userdata('name');
            $date = $this->session->userdata('date');

            if ($name == '') {
                $cond['name'] = '';
                $sdata['name'] = $name;
                $this->session->set_userdata($sdata);
            } else {
                $cond['name'] = $name;
            }

            if ($date == '') {
                $cond['date'] = '';
                $sdata['date'] = $date;
                $this->session->set_userdata($sdata);
            } else {
                $cond['date'] = $date;
            }

            if ($person_type == '') {
                $cond['person_type'] = 'T';
                $sdata['person_type'] = $person_type;
                $this->session->set_userdata($sdata);
            } else {
                $cond['person_type'] = $person_type;
            }
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('timekeepings/movement_register_list/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Timekeeping->get_all_movement_register_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['records'] = $this->Timekeeping->get_all_movement_register_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('timekeepings/movement_register_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function movement_register_delete($id)
    {
        $movement_info = $this->db->where('id', $id)->get('tbl_teacher_staff_movement_registers')->result_array();
        if ($movement_info[0]['attendance_auto'] == 'Y') {
            $person_id = $movement_info[0]['person_id'];
            $person_type = $movement_info[0]['person_type'];
            $date =  $movement_info[0]['date'];
            $this->db->delete('tbl_teacher_staff_movement_registers', array('id' => $id));
            $this->db->delete('tbl_logins', array('person_id' => $person_id, 'date' => $date, 'person_type' => $person_type));
            $sdata['message'] = "You are Successfully Movement Register and Login/Logout Data Deleted !";
            $this->session->set_userdata($sdata);
            redirect("timekeepings/movement_register_list");
        } else {
            $this->db->delete('tbl_teacher_staff_movement_registers', array('id' => $id));
            $sdata['message'] = "You are Successfully Movement Register Data Deleted !";
            $this->session->set_userdata($sdata);
            redirect("timekeepings/movement_register_list");
        }
    }


    public function movement_register_add()
    {
        if ($_POST) {
            $person = explode("-", $this->input->post('person_id'));
            $person_id = $person[1];
            $person_type = $person[0];
            $data = array();
            $data['person_id'] = $person_id;
            $data['person_type'] = $person_type;
            $data['date'] = $this->input->post('txtDate');
            $data['start_time'] = $this->input->post('outTime');
            $data['end_time'] = $this->input->post('inTime');
            $data['location'] = $this->input->post('txtLocation');
            $data['purpose'] = $this->input->post('txtPurpose');
            $data['attendance_auto'] = $this->input->post('txtAutoAttendance');
            if ($this->db->insert('tbl_teacher_staff_movement_registers', $data)) {
                if ($data['attendance_auto'] == 'Y') {
                    if ($this->add_login_logout_data_to_device_database($data)) {
                        $sdata['message'] = "You are Successfully Movement Register Added !";
                        $this->session->set_userdata($sdata);
                        redirect("timekeepings/movement_register_add");
                    } else {
                        $sdata['exception'] = "Movement Register data Added but auto login/logout failed !";
                        $this->session->set_userdata($sdata);
                        redirect("timekeepings/movement_register_add");
                    }
                } else {
                    $sdata['message'] = "You are Successfully Movement Register Added !";
                    $this->session->set_userdata($sdata);
                    redirect("timekeepings/movement_register_add");
                }
            } else {
                $sdata['exception'] = "Movement Register data doesn't Added !";
                $this->session->set_userdata($sdata);
                redirect("timekeepings/movement_register_add");
            }
        }
        $data = array();
        $data['title'] = 'Movement Register';
        $data['action'] = 'add';
        $data['heading_msg'] = "Movement Register";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['teachers'] = $this->db->query("SELECT id,name,teacher_index_no FROM `tbl_teacher` ORDER BY id")->result_array();
        $data['staff'] = $this->db->query("SELECT id,name,staff_index_no FROM `tbl_staff_info`")->result_array();
        $data['maincontent'] = $this->load->view('timekeepings/movement_register_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function add_login_logout_data_to_device_database($data)
    {
        $person_id = $data['person_id'];
        $person_type = $data['person_type'];
        if ($person_type == 'T') {
            $person_info = $this->db->where('id', $person_id)->get('tbl_teacher')->result_array();
            $process_code = $person_info[0]['process_code'];
        } else {
            $person_info = $this->db->where('id', $person_id)->get('tbl_staff_info')->result_array();
            $process_code = $person_info[0]['process_code'];
        }

        $ldata = array();
        $ldata['person_id'] = $data['person_id'];
        $ldata['login_time'] = "10:00:00";
        $ldata['logout_time'] = "16:00:00";
        $ldata['expected_login_time'] = "10:00:00";
        $ldata['expected_logout_time'] = "16:00:00";
        $ldata['process_code'] = $process_code;
        $ldata['date'] = $data['date'];
        $ldata['is_manual_login'] = '1';
        $ldata['person_type'] = $data['person_type'];
        if ($this->db->insert('tbl_logins', $ldata)) {
            return true;
        } else {
            return false;
        }
    }

    public function movement_register_edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id');
            $data['end_time'] = $this->input->post('inTime');
            $data['location'] = $this->input->post('txtLocation');
            $data['purpose'] = $this->input->post('txtPurpose');
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_teacher_staff_movement_registers', $data);
            $sdata['message'] = "You are Successfully Movement Register Updated !";
            $this->session->set_userdata($sdata);
            redirect("timekeepings/movement_register_list");
        }
        $data = array();
        $data['title'] = 'Movement Register';
        $data['action'] = 'edit';
        $data['heading_msg'] = "Movement Register";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['movement_info'] = $this->db->query("SELECT * FROM `tbl_teacher_staff_movement_registers` WHERE id = '$id'")->result_array();
        $data['maincontent'] = $this->load->view('timekeepings/movement_register_add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
