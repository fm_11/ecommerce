<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Teachers extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Teacher_info','Admin_login'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('teacher') . '/' . $this->lang->line('staff');
        $data['heading_msg'] = $this->lang->line('teacher') . '/' . $this->lang->line('staff');
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('teachers/index/');
        $config['per_page'] = ROW_PER_PAGE;
        $config['total_rows'] = count($this->Teacher_info->get_all_teachers(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['teachers'] = $this->Teacher_info->get_all_teachers(ROW_PER_PAGE, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('teachers/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function updateMsgStatusTeacherStatus()
    {
        $status = $this->input->get('status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($status == 1) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }
        $this->Teacher_info->update_teacher_status($data);
        if ($status == 0) {
            echo '<a class="deleteTag" title="Active" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
        } else {
            echo '<a class="deleteTag" title="Inactive" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
        }
    }

    public function delete($id)
    {
        $teacher_photo_info = $this->Teacher_info->get_teacher_photo_info_by_teacher_id($id);
        $file = $teacher_photo_info[0]['photo_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/teacher/' . $file;
            if (unlink($filedel)) {
                $this->Teacher_info->delete_teacher_info_by_teacher_id($id);
            } else {
                $this->Teacher_info->delete_teacher_info_by_teacher_id($id);
            }
        } else {
            $this->Teacher_info->delete_teacher_info_by_teacher_id($id);
        }

        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("teachers/index");
    }

    public function add()
    {
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER . '/teacher/';
            $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
            $config['max_size'] = '6000';
            $config['max_width'] = '4000';
            $config['max_height'] = '4000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));

            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_photo_name = "Teacher_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/teacher/" . $new_photo_name)) {
                        $data = array();
						$data['category_id'] = $this->input->post('category_id', true);
						$data['teacher_code'] = $this->Teacher_info->generateTeacherCode();
                        $data['name'] = $this->input->post('txtName', true);
                        $data['post'] = $this->input->post('txtPost', true);
                        $data['pay_code_id'] = $this->input->post('txtPayCode', true);
                        $data['section'] = $this->input->post('txtSection', true);
                        $data['subject'] = $this->input->post('txtSubject', true);
                        $data['national_id'] = $this->input->post('txtNID', true);

						$dob =  $this->input->post('txtDOB', true);
						if($dob == ''){
							$data['date_of_birth'] = NULL;
						}else{
							$data['date_of_birth'] = $dob;
						}

						$doj = $this->input->post('txtDOJ', true);
						if($doj == ''){
							$data['date_of_join'] = NULL;
						}else{
							$data['date_of_join'] = $doj;
						}

                        $data['fathers_name'] = $this->input->post('txtFathersName', true);
                        $data['mothers_name'] = $this->input->post('txtMothersName', true);
                        $data['present_address'] = $this->input->post('txtPresentAddress', true);
                        $data['permanent_address'] = $this->input->post('txtPermanentAddress', true);
                        $data['mobile'] = $this->input->post('txtMobile', true);
                        $data['email'] = $this->input->post('txtEmail', true);

                        $data['teacher_index_no'] = $this->input->post('txtIndexNum', true);
                        $data['process_code'] = $this->input->post('process_code', true);
                        $data['bank_name'] = $this->input->post('txtBankName', true);
                        $data['bank_acc'] = $this->input->post('txtBankAcc', true);
                        $data['gender'] = $this->input->post('txtGender', true);
                        $data['religion'] = $this->input->post('txtReligion', true);
                        $data['blood_group_id'] = $this->input->post('blood_group_id', true);
                        $data['is_permanent'] = $this->input->post('txtIsPermanent', true);
						$data['is_dashboard_show'] = $this->input->post('is_dashboard_show', true);

						//update dashboard info
						$is_dashboard_show = $this->input->post('is_dashboard_show', true);
						if($is_dashboard_show == '1'){
							$this->db->query("UPDATE `tbl_teacher` SET `is_dashboard_show` = '0'");
						}
						//update dashboard info

                        $data['allowed_for_timekeeping'] = $this->input->post('allowed_for_timekeeping', true);
                        if ($this->input->post('allowed_for_timekeeping', true) == 1) {
                            $data['syn_date'] = $this->input->post('syn_date', true);
                        }
                        $data['photo_location'] = $new_photo_name;
                        if ($this->Teacher_info->add_teacher_info($data)) {
                        	if(count($_POST['txtEduQua']) > 0){
								$edata = array();
								$teacher_id = $this->db->insert_id();
								$i = 0;
								while ($i < count($_POST['txtEduQua'])) {
									$edata[$i]['teacher_id'] = $teacher_id;
									$edata[$i]['qua_id'] = $_POST['txtEduQua'][$i];
									$i++;
								}
								$this->db->insert_batch('tbl_teacher_educational_qua', $edata);

								//shift_add
								$shiftdata = array();
								$j = 0;
								while ($j < count($_POST['txtShift'])) {
									$shiftdata[$j]['teacher_id'] = $teacher_id;
									$shiftdata[$j]['shift_id'] = $_POST['txtShift'][$j];
									$j++;
								}
								$this->db->insert_batch('tbl_teacher_shift', $shiftdata);
								//shift_add
							}
                            $sdata['message'] = $this->lang->line('add_success_message');
                            $this->session->set_userdata($sdata);
                            redirect("teachers/add");
                        } else {
                            $sdata['exception'] = $this->lang->line('add_error_message') . "Sorry Teacher Doesn't Added !";
                            $this->session->set_userdata($sdata);
                            redirect("teachers/add");
                        }
                    } else {
                        $sdata['exception'] = $this->lang->line('add_error_message') . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("teachers/add");
                    }
                } else {
                    $sdata['exception'] = $this->lang->line('add_error_message') . "Sorry Photo Does't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("teachers/add");
                }
            }
        } else {
            $data = array();
            $data['title'] = $this->lang->line('teacher') . '/' . $this->lang->line('staff') . ' ' . $this->lang->line('add');
            $data['heading_msg'] = $this->lang->line('teacher') . '/' . $this->lang->line('staff') . ' ' . $this->lang->line('add');
            $data['action'] = '';
            $data['is_show_button'] = "index";
			$data['religions'] =  $this->Admin_login->getReligionList();
			$data['categories'] = $this->db->query("SELECT * FROM tbl_teacher_category")->result_array();
            $data['sections'] = $this->db->query("SELECT * FROM tbl_teacher_section")->result_array();
            $data['educational_qualification'] = $this->db->query("SELECT * FROM tbl_educational_qualification")->result_array();
            $data['shift'] = $this->db->query("SELECT * FROM tbl_shift")->result_array();
            $data['pay_codes'] = $this->db->query("SELECT * FROM tbl_pay_code")->result_array();
            $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
            $data['post'] = $this->db->query("SELECT * FROM tbl_teacher_post")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('teachers/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }



    public function edit($id = null)
    {
        if ($_POST) {
            $file = $_FILES["txtPhoto"]['name'];
            if ($file != '') {
                $teacher_photo_info = $this->Teacher_info->get_teacher_photo_info_by_teacher_id($this->input->post('id', true));
                $old_file = $teacher_photo_info[0]['photo_location'];
                if (!empty($old_file)) {
                    $filedel = PUBPATH . MEDIA_FOLDER . '/teacher/' . $old_file;
                    unlink($filedel);
                }
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/teacher/';
                $config['allowed_types'] = 'jpg|png|JPEG|jpeg';
                $config['max_size'] = '6000';
                $config['max_width'] = '4000';
                $config['max_height'] = '4000';
                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = "Teacher_" . time() . "_" . date('Y-m-d') . "." . $extension;
                        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/teacher/" . $new_photo_name)) {
                            $data = array();
                            $data['id'] = $this->input->post('id', true);
							$data['category_id'] = $this->input->post('category_id', true);
                            $data['name'] = $this->input->post('txtName', true);
                            $data['post'] = $this->input->post('txtPost', true);
                            $data['pay_code_id'] = $this->input->post('txtPayCode', true);
                            $data['section'] = $this->input->post('txtSection', true);
                            $data['subject'] = $this->input->post('txtSubject', true);
                            $data['national_id'] = $this->input->post('txtNID', true);

							$dob =  $this->input->post('txtDOB', true);
							if($dob == ''){
								$data['date_of_birth'] = NULL;
							}else{
								$data['date_of_birth'] = $dob;
							}

							$doj = $this->input->post('txtDOJ', true);
							if($doj == ''){
								$data['date_of_join'] = NULL;
							}else{
								$data['date_of_join'] = $doj;
							}

                            $data['fathers_name'] = $this->input->post('txtFathersName', true);
                            $data['mothers_name'] = $this->input->post('txtMothersName', true);
                            $data['present_address'] = $this->input->post('txtPresentAddress', true);
                            $data['permanent_address'] = $this->input->post('txtPermanentAddress', true);

                            $data['gender'] = $this->input->post('txtGender', true);
                            $data['religion'] = $this->input->post('txtReligion', true);
                            $data['blood_group_id'] = $this->input->post('blood_group_id', true);
                            $data['teacher_index_no'] = $this->input->post('txtIndexNum', true);
                            $data['process_code'] = $this->input->post('process_code', true);
                            $data['bank_name'] = $this->input->post('txtBankName', true);
                            $data['bank_acc'] = $this->input->post('txtBankAcc', true);
                            $data['is_permanent'] = $this->input->post('txtIsPermanent', true);

                            //update dashboard info
							$is_dashboard_show = $this->input->post('is_dashboard_show', true);
                            if($is_dashboard_show == '1'){
                            	$this->db->query("UPDATE `tbl_teacher` SET `is_dashboard_show` = '0'");
							}
							//update dashboard info

                            $data['is_dashboard_show'] = $this->input->post('is_dashboard_show', true);
                            $data['mobile'] = $this->input->post('txtMobile', true);
                            $data['email'] = $this->input->post('txtEmail', true);
                            $data['allowed_for_timekeeping'] = $this->input->post('allowed_for_timekeeping', true);
                            if ($this->input->post('allowed_for_timekeeping', true) == 1) {
                                $data['syn_date'] = $this->input->post('syn_date', true);
                            }
                            $data['photo_location'] = $new_photo_name;
                            $this->Teacher_info->update_teacher_status($data);

                            $this->db->delete('tbl_teacher_educational_qua', array('teacher_id' => $data['id']));
                            $edata = array();
                            $teacher_id = $data['id'];
                            $i = 0;
                            while ($i < count($_POST['txtEduQua'])) {
                                $edata[$i]['teacher_id'] = $teacher_id;
                                $edata[$i]['qua_id'] = $_POST['txtEduQua'][$i];
                                $i++;
                            }
                            $this->db->insert_batch('tbl_teacher_educational_qua', $edata);

                            //Shift
                            $this->db->delete('tbl_teacher_shift', array('teacher_id' => $data['id']));
                            $shiftdata = array();
                            $teacher_id = $data['id'];
                            $i = 0;
                            while ($i < count($_POST['txtShift'])) {
                                $shiftdata[$i]['teacher_id'] = $teacher_id;
                                $shiftdata[$i]['shift_id'] = $_POST['txtShift'][$i];
                                $i++;
                            }
                            $this->db->insert_batch('tbl_teacher_shift', $shiftdata);
                            //Shift


                            $sdata['message'] = $this->lang->line('edit_success_message');
                            $this->session->set_userdata($sdata);
                            redirect("teachers/index");
                        } else {
                            $sdata['exception'] = $this->lang->line('edit_error_message') . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("teachers/index");
                        }
                    } else {
                        $sdata['exception'] = $this->lang->line('edit_error_message');
                        $this->session->set_userdata($sdata);
                        redirect("teachers/index");
                    }
                }
            } else {
                $data = array();
                $data['id'] = $this->input->post('id', true);
				$data['category_id'] = $this->input->post('category_id', true);
                $data['name'] = $this->input->post('txtName', true);
                $data['post'] = $this->input->post('txtPost', true);
                $data['pay_code_id'] = $this->input->post('txtPayCode', true);
                $data['section'] = $this->input->post('txtSection', true);
                $data['subject'] = $this->input->post('txtSubject', true);
                $data['national_id'] = $this->input->post('txtNID', true);

                $data['gender'] = $this->input->post('txtGender', true);
                $data['religion'] = $this->input->post('txtReligion', true);
                $data['blood_group_id'] = $this->input->post('blood_group_id', true);
                $data['fathers_name'] = $this->input->post('txtFathersName', true);
                $data['mothers_name'] = $this->input->post('txtMothersName', true);
                $data['present_address'] = $this->input->post('txtPresentAddress', true);
                $data['permanent_address'] = $this->input->post('txtPermanentAddress', true);

				$dob =  $this->input->post('txtDOB', true);
				if($dob == ''){
					$data['date_of_birth'] = NULL;
				}else{
					$data['date_of_birth'] = $dob;
				}

                $doj = $this->input->post('txtDOJ', true);
                if($doj == ''){
					$data['date_of_join'] = NULL;
				}else{
					$data['date_of_join'] = $doj;
				}

                $data['teacher_index_no'] = $this->input->post('txtIndexNum', true);
                $data['process_code'] = $this->input->post('process_code', true);
                $data['bank_name'] = $this->input->post('txtBankName', true);
                $data['bank_acc'] = $this->input->post('txtBankAcc', true);
                $data['is_permanent'] = $this->input->post('txtIsPermanent', true);
				$data['is_dashboard_show'] = $this->input->post('is_dashboard_show', true);
				//update dashboard info
				$is_dashboard_show = $this->input->post('is_dashboard_show', true);
				if($is_dashboard_show == '1'){
					$this->db->query("UPDATE `tbl_teacher` SET `is_dashboard_show` = '0'");
				}
				//update dashboard info

				$data['mobile'] = $this->input->post('txtMobile', true);
                $data['email'] = $this->input->post('txtEmail', true);
                $data['allowed_for_timekeeping'] = $this->input->post('allowed_for_timekeeping', true);
                if ($this->input->post('allowed_for_timekeeping', true) == 1) {
                    $data['syn_date'] = $this->input->post('syn_date', true);
                }
                $this->Teacher_info->update_teacher_status($data);

                $this->db->delete('tbl_teacher_educational_qua', array('teacher_id' => $data['id']));
                $edata = array();
                $teacher_id = $data['id'];
                $i = 0;
                while ($i < count($_POST['txtEduQua'])) {
                    $edata[$i]['teacher_id'] = $teacher_id;
                    $edata[$i]['qua_id'] = $_POST['txtEduQua'][$i];
                    $i++;
                }
                $this->db->insert_batch('tbl_teacher_educational_qua', $edata);


                //Shift
                $this->db->delete('tbl_teacher_shift', array('teacher_id' => $data['id']));
                $shiftdata = array();
                $teacher_id = $data['id'];
                $i = 0;
                while ($i < count($_POST['txtShift'])) {
                    $shiftdata[$i]['teacher_id'] = $teacher_id;
                    $shiftdata[$i]['shift_id'] = $_POST['txtShift'][$i];
                    $i++;
                }
                $this->db->insert_batch('tbl_teacher_shift', $shiftdata);
                //Shift

                $sdata['message'] = $this->lang->line('edit_success_message');
                $this->session->set_userdata($sdata);
                redirect("teachers/index");
            }
        } else {
            //  echo $id;
            //    die;
            $data = array();
            $data['title'] =  $this->lang->line('teacher') . '/' . $this->lang->line('staff') . ' ' . $this->lang->line('update');
            $data['heading_msg'] = $this->lang->line('teacher') . '/' . $this->lang->line('staff') . ' ' . $this->lang->line('update');
            $data['is_show_button'] = "index";
            $data['teacher_info'] = $this->Teacher_info->get_teacher_photo_info_by_teacher_id($id);
            $data['educational_qualification'] = $this->db->query("SELECT e.*,teq.`qua_id` FROM `tbl_educational_qualification` AS e
LEFT JOIN `tbl_teacher_educational_qua` AS teq ON teq.`qua_id` = e.`id` AND teq.`teacher_id` = '$id'
ORDER BY e.`id`")->result_array();

            $data['shift'] = $this->db->query("SELECT s.*,ts.`shift_id` FROM `tbl_shift` AS s
LEFT JOIN `tbl_teacher_shift` AS ts ON ts.`shift_id` = s.`id` AND ts.`teacher_id` = '$id'
ORDER BY s.`id`")->result_array();
			$data['religions'] =  $this->Admin_login->getReligionList();
			$data['categories'] = $this->db->query("SELECT * FROM tbl_teacher_category")->result_array();
            $data['pay_codes'] = $this->db->query("SELECT * FROM tbl_pay_code")->result_array();
            $data['sections'] = $this->db->query("SELECT * FROM tbl_teacher_section")->result_array();
            $data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
            $data['post'] = $this->db->query("SELECT * FROM tbl_teacher_post")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('teachers/edit', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
