<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Purchase extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        $this->load->model(array('Account', 'Admin_login','Product'));
        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $cond = array();
        if ($_POST) {
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");

            $sdata['from_date'] = $from_date;
            $sdata['to_date'] = $to_date;

            $this->session->set_userdata($sdata);
            $cond['from_date'] = $from_date;
            $cond['to_date'] = $to_date;
        } else {
            $from_date = $this->session->userdata('from_date');
            $to_date = $this->session->userdata('to_date');
            $cond['from_date'] = $from_date;
            $cond['to_date'] = $to_date;
        }
        $data['title'] = $this->lang->line('purchase').' '.$this->lang->line('list');
        $data['heading_msg'] = $this->lang->line('purchase').' '.$this->lang->line('list');
        $this->load->library('pagination');
        $config['base_url'] = site_url('purchase/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Account->get_all_purchase_list(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['deposits'] = $this->Account->get_all_purchase_list(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('purchase/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        $user_info = $this->session->userdata('user_info');
        //echo $user_info[0]->id;
        //  die;
        $user_asset_head = $this->Admin_login->getUserWiseAssetHead($user_info[0]->id);
		 // echo '<pre>';
        // print_r($user_asset_head);
        // die;
         if (empty($user_asset_head)) {
             $sdata['exception'] = "Please set asset head for this user";
             $this->session->set_userdata($sdata);
             redirect("user_wise_income_heads/index");
         }

        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
			$total_paid =  $this->input->post("total_paid");
            $mdata = array();
            $mdata['purchase_date'] = $this->input->post("purchase_date");
            $mdata['supplier_id'] = $this->input->post("supplier_id");
            $mdata['purchase_no'] = $this->input->post("purchase_no");
            $mdata['process_date'] = date("Y-m-d H:i:s");
            $mdata['sub_total'] = $this->input->post("sub_total");
            $mdata['discount'] = $this->input->post("discount");
            $mdata['discount_amount'] = $this->input->post("discount_amount");
            $mdata['total_amount'] = $this->input->post("total_amount");
		      	$mdata['total_paid'] = $total_paid;
		      	$mdata['total_due'] = $this->input->post("total_due");
            $mdata['user_id'] = $user_info[0]->id;


			// $available_balance = $this->Account->getAssetHeadWiseAvailableBalance($user_asset_head[0]['user_id']);
			// if($available_balance < $total_paid){
			// 	$sdata['exception'] = "There is not enough amount in your account";
			// 	$this->session->set_userdata($sdata);
			// 	redirect("purchase/add");
			// }


            if ($this->db->insert('tbl_in_purchase', $mdata)) {
                $insert_id = $this->db->insert_id();
                $loop_time = $this->input->post("num_of_row");
			         	$inventory_tran_data = array();
                $i = 0;
                while ($i < $loop_time) {
                    $data = array();
                    $data['purchase_id'] = $insert_id;
                    $data['product_id'] = $this->input->post("inventory_product_id_" . $i);
                    $data['unit_price'] = $this->input->post("unit_price_" . $i);
                    $data['quantity'] = $this->input->post("quantity_" . $i);
                    $data['total_amount'] = $this->input->post("total_amount_" . $i);
                    $this->db->insert('tbl_in_purchases_details', $data);

            				$inventory_tran_data[$i]['user_id'] = $user_info[0]->id;
            				$inventory_tran_data[$i]['product_id'] = $data['product_id'];
            				$inventory_tran_data[$i]['type'] = 'I';
            				$inventory_tran_data[$i]['quantity'] = $data['quantity'];
            				$inventory_tran_data[$i]['amount'] = $data['total_amount'];
            				$inventory_tran_data[$i]['from_transaction_type'] = 'P'; //Purchase balance
            				$inventory_tran_data[$i]['date'] = $this->input->post("purchase_date");
            				$inventory_tran_data[$i]['reference_id'] = $insert_id;
            				$inventory_tran_data[$i]['remarks'] = 'From Purchase (ID - ' . $insert_id . ')';

                    $i++;
                }

				$this->db->insert_batch('tbl_inventory_in_out_transaction', $inventory_tran_data);

				 //asset and vendor transaction entry entry
				if($total_paid > 0){
					$vendor_tran = array();
					$vendor_tran['supplier_id'] = $this->input->post("supplier_id");
					$vendor_tran['date'] = $this->input->post("purchase_date");
					$vendor_tran['amount'] = $total_paid;
					$vendor_tran['type'] = 'I';
					$vendor_tran['from_transaction'] = 'P';
					$vendor_tran['asset_head_id'] = $user_asset_head[0]['asset_category_id'];
					$vendor_tran['user_id'] = $user_info[0]->id;
					$this->db->insert('tbl_in_supplier_transaction', $vendor_tran);

					// $assetdata = array();
					// $assetdata['user_id'] = $user_asset_head[0]['user_id'];
					// $assetdata['asset_id'] = $user_asset_head[0]['asset_category_id'];
					// $assetdata['amount'] = $total_paid;
					// $assetdata['type'] = 'O';
					// $assetdata['from_transaction_type'] = 'PE';
					// $assetdata['date'] = $this->input->post("purchase_date");
					// $assetdata['reference_id'] = $insert_id;
					// $assetdata['added_by'] = $user_info[0]->id;
					// $assetdata['remarks'] = 'From Purchase Expense (ID - ' . $insert_id . ')';
					// $this->db->insert('tbl_user_wise_asset_transaction', $assetdata);
				}
                //asset and vendor transaction entry entry

                $sdata['message'] = $this->lang->line('add_success_message');
                $this->session->set_userdata($sdata);
                redirect("purchase/add");
            }
        }
        $data = array();
        $data['title'] = $this->lang->line('purchase') . ' ' . $this->lang->line('add');
        $data['heading_msg'] = $this->lang->line('purchase') . ' ' . $this->lang->line('add');
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['is_show_button'] = "index";
        $data['inventory_product'] = $this->Product->get_all_product_info();
        $data['suppliers'] = $this->Account->get_supplier_list();
        $data['purchase_no']=$this->generateSMSTransactionID();
        $data['maincontent'] = $this->load->view('purchase/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
        if ($this->db->delete('tbl_in_purchases_details', array('purchase_id' => $id))) {
            $this->db->delete('tbl_in_purchase', array('id' => $id));
          //  $this->db->delete('tbl_user_wise_asset_transaction', array('reference_id' => $id,'type' => 'I','from_transaction_type' => 'OI'));
            $sdata['message'] = $this->lang->line('delete_success_message');
            $this->session->set_userdata($sdata);
            redirect("purchase/index");
        }
    }
    public function get_supplier_address()
    {
      $supplier_id = $this->input->get("supplier_id");
      $data = array();
      $info = $this->db->query("SELECT `address` FROM `tbl_supplier` WHERE id = $supplier_id;")->row();
      $data['address'] = $info->address;

      $this->load->view('purchase/supplier_address', $data);
    }
    function generateSMSTransactionID(){
      $date = new DateTime();
      $code = $date->getTimestamp();
      $total_collection = count($this->db->query("SELECT id FROM tbl_in_purchase")->result_array());
      $code .= $total_collection;
      $code .= $this->generateRandomString(5);
      return $code;
    }

  function generateRandomString($length = 2) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
  public function purchase_details_view($id)
  {
      $this->load->library('numbertowords');
      //echo 'fgg';
      //die;
      $data = array();
      $data['title'] = $this->lang->line('purchase') . ' ' . $this->lang->line('details');
      $data['school_info'] = $this->Admin_login->get_contact_info();
      $data['purchase_master_info'] = $this->Account->get_master_purchase_by_id($id);
      $data['purchase_detail_info'] =$this->Account->get_child_purchase_by_id($id);

      $data['signature'] = $this->Admin_login->getSignatureByAccessCode('FR'); //FR= Fee receipt
    //
    //   echo '<pre>';
    // print_r($data);
    // die;
      $this->load->view('purchase/purchase_details', $data);
  }
  public function get_inventory_product_for_list()
	{
		$inventory_product = $this->Product->get_all_product_info();
		echo json_encode($inventory_product);
	}
}
