
<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Collection_summery extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('collection') . ' ' . $this->lang->line('summary') . ' ' . $this->lang->line('report');
        $data['heading_msg'] = $this->lang->line('collection') . ' ' . $this->lang->line('summary') . ' ' . $this->lang->line('report');
        if ($_POST) {
//            echo '<pre>';
//            print_r($_POST);
//            die;
            $this->load->library('numbertowords');
            $SchoolInfo = $this->Admin_login->fetReportHeader();
            $Info = array();
            $Info['school_name'] = $SchoolInfo[0]['school_name'];
            $Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
            $Info['address'] = $SchoolInfo[0]['address'];
            $data['HeaderInfo'] = $Info;
            $class_id = $this->input->post("class_id");
            $from_date = $this->input->post("from_date");
            $to_date = $this->input->post("to_date");
            $report_for = $this->input->post("report_for");
            $user_id = $this->input->post("user_id");


            $data['class_id'] = $class_id;
            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
            $data['report_for'] = $this->input->post("report_for");
            $data['user_id'] = $user_id;

            if ($class_id == 'all') {
                $data['class_name'] = "All";
                $where = "";
            } else {
                $class_info = $this->db->query("SELECT * FROM tbl_class WHERE id ='$class_id'")->result_array();
                $data['class_name'] = $class_info[0]['name'];
                $where = " AND c.`class_id` = '$class_id' ";
            }

            $date_year = DateTime::createFromFormat("Y-m-d", $from_date);
            $year = $date_year->format("Y");

            $is_resident_transaction_where = "";
            if ($report_for != 'all') {
                $is_resident_transaction_where = " AND c.`is_resident_transaction` = '$report_for' ";
            }

            $user_id_where = "";
            if ($user_id != 'all') {
                $user_id_where = " AND c.`user_id` = '$user_id' ";
            }


            $data['rdata'] = $this->db->query("SELECT c.*,s.`name`,s.`student_code`,s.`roll_no`,cl.name as class_name,
			(SELECT SUM(cd.paid_amount) FROM `tbl_fee_collection_details` AS cd WHERE cd.`collection_id` = c.`id`) AS total_amount,
			(SELECT SUM(lf.amount) FROM tbl_fee_late_fine as lf WHERE lf.collection_id = c.`id`) as total_late_fee,
			(SELECT SUM(trans.amount) FROM tbl_student_monthly_transport_fee as trans WHERE trans.collection_id = c.`id`) as total_transport_fee
            FROM `tbl_fee_collection` AS c
            INNER JOIN `tbl_student` AS s ON c.`student_id` = s.`id`
            LEFT JOIN tbl_class as cl on cl.id = c.class_id
            WHERE c.`collection_status` = '1' AND (c.`date` BETWEEN '$from_date' AND '$to_date') $where $is_resident_transaction_where $user_id_where ORDER BY c.`entry_date`")->result_array();
            //echo $this->db->last_query(); die;
            //$late_fee = $this->db->query("SELECT SUM(amount) as total_late_fee FROM tbl_fee_late_fine WHERE collection_id
            // IN(SELECT id FROM tbl_fee_collection WHERE (date BETWEEN '$from_date' AND '$to_date') $l_where)
            //")->result_array();

            //$data['total_late_fee'] = $late_fee[0]['total_late_fee'];

            $data['report'] = $this->load->view('collection_summery/fee_collection_summery_standard_table', $data, true);
        }
        if (isset($_POST['pdf_download'])) {
            $data['is_pdf'] = 1;
            //Dom PDF
            $this->load->library('mydompdf');
            $html = $this->load->view('collection_summery/fee_collection_summery_standard_table', $data, true);
            $this->mydompdf->createPDF($html, 'FeeCollectionSummeryReport', true);
        } else {
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
            $data['students'] = $this->db->query("SELECT id,name,student_code FROM tbl_student")->result_array();
            $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
            $data['users'] = $this->db->query("SELECT * FROM tbl_user")->result_array();
            $data['income_category'] = $this->db->query("SELECT * FROM `tbl_ba_income_category`")->result_array();
            $data['maincontent'] = $this->load->view('collection_summery/fee_collection_summery_standard', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }
}
