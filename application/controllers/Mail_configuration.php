<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mail_configuration extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Admin_login'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = $this->lang->line('mail_configuration');
        $data['heading_msg'] = $this->lang->line('mail_configuration');
        $data['mail_info'] = $this->Admin_login->get_mail_info();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('mail_configuration/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function update()
    {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['mail_username'] = $this->input->post('mail_username', true);
            $data['mail_password'] = $this->input->post('mail_password', true);
            $data['mail_encryption'] = $this->input->post('mail_encryption', true);
            $data['admin_mail_address'] = $this->input->post('admin_mail_address', true);
            $data['mail_host'] = $this->input->post('mail_host', true);
            $data['mail_driver'] = $this->input->post('mail_driver', true);
            $data['mail_port'] = $this->input->post('mail_port', true);
            $data['mail_from_address'] = $this->input->post('mail_from_address', true);
            $data['mail_from_name'] = $this->input->post('mail_from_name', true);
            $data['customer_notify'] = $this->input->post('customer_notify', true);
            $data['admin_notify'] = $this->input->post('admin_notify', true);
            $this->Admin_login->update_mail_info($data);
            $sdata['message'] = "You are Successfully Updated Mail Config ! ";
            $this->session->set_userdata($sdata);
            redirect("mail_configuration/index");
     }
}
