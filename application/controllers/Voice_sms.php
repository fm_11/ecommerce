<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Voice_sms extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Admin_login','Message','Student'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function voice_sms_send_list(){
		$data['title'] = 'Send Voice SMS To Guardian';
		$data['heading_msg'] = "Send Voice SMS To Guardian";
		$cond = array();
		$this->load->library('pagination');
		$config['base_url'] = site_url('voice_sms/voice_sms_send/');
		$config['per_page'] = 10;
		$config['total_rows'] = count($this->Message->get_all_voice_sms_request_list(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['requests'] = $this->Message->get_all_voice_sms_request_list(10, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
	    $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('voice_sms/to_guardian_voice_sms_list', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	function details_report($request_id){
		$data['title'] = 'Send Voice SMS To Guardian';
		$data['heading_msg'] = "Send Voice SMS To Guardian";
		$data['all_data'] = $this->db->query("SELECT * FROM tbl_voice_sms_request_details WHERE request_id = $request_id;")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('voice_sms/voice_sms_details_report', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function voice_sms_send()
	{
		$data = array();
		if ($_POST) {
			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['shift_id'] = $shift_id;
			$data['group'] = $this->input->post('group_id', true);

			$sdata['class_shift_section_id'] = $class_shift_section_id;
			$sdata['group_id'] = $data['group'];
			$this->session->set_userdata($sdata);
			$data['student_info'] = $this->Student->get_all_student_list(0, 0, $data);
			$data['audios'] = $this->db->query("SELECT * FROM tbl_audio_sms")->result_array();
		}
		$data['title'] = 'Send Voice SMS To Guardian';
		$data['heading_msg'] = "Send Voice SMS To Guardian";
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('voice_sms/to_guardian_voice_sms_send_form', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function to_guardian_voice_sms_send()
	{
		if ($_POST) {
			$audio_id = $this->input->post('audio_id', true);
			$data = array();
			$data['date_time'] = date('Y-m-d H:i:s');
			$data['audio_id'] = $audio_id;
			$data['status'] = "P";
			$data['total_number'] = 0;
			$data['total_cost'] = 0;
			$data['class_id'] = $this->input->post('class_id', true);
			$data['section_id'] = $this->input->post('section_id', true);
			$data['group_id'] = $this->input->post('group_id', true);
			$data['shift_id'] = $this->input->post('shift_id', true);
			$this->db->insert("tbl_voice_sms_request", $data);
			$insert_id = $this->db->insert_id();
			$loop_time = $this->input->post('loop_time', true);
			$i = 0;
			$index = 0;
			$numbers = array();
			while ($i < $loop_time) {
				$is_send = $this->input->post('is_send_' . $i, true);
				if (isset($is_send) && $is_send != '') {

					//sms send data ready
					$numbers[$index]['request_id'] = $insert_id;
					$numbers[$index]['msisdn'] = $this->input->post('guardian_mobile_' . $i, true);
					$numbers[$index]['status'] = "PENDING";
					$index++;
					//sms send data ready end
				}
				$i++;
			}
			$this->db->insert_batch('tbl_voice_sms_request_details', $numbers);

			$sdata['message'] = "Voice Message Send Request Send Successfully";
			$this->session->set_userdata($sdata);
			redirect("voice_sms/voice_sms_send");
		}
	}



	public function audio_message_upload()
	{
		//echo MEDIA_FOLDER; die;
		if ($_POST) {
			$this->load->library('upload');
			$config['upload_path'] = MEDIA_FOLDER . '/audio/';
			$config['allowed_types'] = 'mp3';
			$config['max_size'] = '600000';
			$this->upload->initialize($config);
			$extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
			//echo $extension; die;
			if ($extension != 'mp3') {
				$sdata['exception'] = "Sorry .$extension file not allowed. Please upload .mp3 file !";
				$this->session->set_userdata($sdata);
				redirect("voice_sms/audio_message_upload");
			}
			foreach ($_FILES as $field => $file) {
				if ($file['error'] == 0) {
					$new_file_name = "audio_" . time() . "_" . date('Y-m-d') . "." . $extension;
					if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/audio/" . $new_file_name)) {
						$data = array();
						$data['title'] = $this->input->post('title', true);
						$data['location'] = $new_file_name;
						$this->db->insert('tbl_audio_sms', $data);
						$sdata['message'] = "You are Successfully Audio Uploaded ! ";
						$this->session->set_userdata($sdata);
						redirect("voice_sms/audio_message_upload");
					} else {
						$sdata['exception'] = "Sorry Audio Doesn't Upload !" . $this->upload->display_errors();
						$this->session->set_userdata($sdata);
						redirect("voice_sms/audio_message_upload");
					}
				} else {
					$sdata['exception'] = "Sorry Audio Doesn't Upload !";
					$this->session->set_userdata($sdata);
					redirect("voice_sms/audio_message_upload");
				}
			}
		} else {
			$data = array();
			$data['title'] = 'Audio SMS/Messages Upload';
			$data['heading_msg'] = "Audio SMS/Messages Upload";
		    $cond = array();
			$this->load->library('pagination');
			$config['base_url'] = site_url('voice_sms/audio_message_upload/');
			$config['per_page'] = 10;
			$config['total_rows'] = count($this->Message->get_all_voice_audio_list(0, 0, $cond));
			$this->pagination->initialize($config);
			$data['voices'] = $this->Message->get_all_voice_audio_list(10, (int)$this->uri->segment(3), $cond);
			$data['counter'] = (int)$this->uri->segment(3);
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('voice_sms/audio_message_upload', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}

	function audio_delete($id){
		$voice_info = $this->db->where('id', $id)->get('tbl_audio_sms')->result_array();
		$file = $voice_info[0]['location'];
		if (!empty($file)) {
			$filedel = PUBPATH . MEDIA_FOLDER . '/audio/' . $file;
			if (unlink($filedel)) {
				$this->db->delete('tbl_audio_sms', array('id' => $id));
			} else {
				$this->db->delete('tbl_audio_sms', array('id' => $id));
			}
		} else {
			$this->db->delete('tbl_audio_sms', array('id' => $id));
		}

		$sdata['message'] = $this->lang->line('delete_success_message');
		$this->session->set_userdata($sdata);
		redirect("voice_sms/audio_message_upload");
	}


}
