<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Student_attendance_details extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student', 'Admin_login','Students_report','Timekeeping'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Student Attendance Details';
		$data['heading_msg'] = 'Student Attendance Details';
		if ($_POST) {
			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;
			$year = $this->input->post("year");
			$month = $this->input->post("month");
			$group_id = $this->input->post("group_id");
			$period_id = $this->input->post("period_id");

			$class_shift_section_id =  $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['month'] = $month;
			$data['year'] = $year;
			$data['group_id'] = $group_id;
			$data['period_id'] = $period_id;

			if($period_id != 'FM'){
				$data['period_name'] = $this->Students_report->get_period_name($period_id);
			}else{
				$data['period_name'] = "From Machine";
			}
			$data['class_name'] =$this->Students_report->get_class_name($class_id);
			$data['section_name'] =$this->Students_report->get_section_name($section_id);
			$data['shift_name'] =$this->Students_report->get_shift_name($shift_id);
			$data['number_of_days'] = cal_days_in_month(CAL_GREGORIAN, $data['month'], $year);

			$pdata = array();
			$pdata['month'] = $month;
			$pdata['year'] = $year;
			$pdata['class_id'] = $class_id;
			$pdata['shift_id'] = $shift_id;
			$pdata['section_id'] = $section_id;
			$pdata['group_id'] = $group_id;
			$pdata['period_id'] = $period_id;
			$pdata['number_of_days'] = $data['number_of_days'];
			$pdata['from_date'] = $year . '-' . sprintf("%02d", $month) . '-01';
			$pdata['to_date'] = $year . '-' . sprintf("%02d", $month) . '-' . $data['number_of_days'];
			$data['adata'] = $this->Timekeeping->get_student_attendance_details($pdata);
			$data['report'] = $this->load->view('student_attendance_details/student_attendance_details_report_table', $data, true);
			if(isset($_POST['pdf_download'])){
				$data['is_pdf'] = 1;
				$this->load->library('mydompdf');
				$this->load->view('report_content/report_main_content', $data);
				$html = $this->load->view('report_content/report_main_content', $data, true);
				$this->mydompdf->createPDF($html, 'student_attendance_sheets', true, 'A4', 'landscape');
			}else{
				$this->load->view('report_content/report_main_content', $data);
			}
		}
		else {
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['GroupList'] = $this->Admin_login->getGroupList();
			$data['period_list'] = $this->db->query("SELECT * FROM `tbl_class_period`")->result_array();
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('student_attendance_details/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}
}
