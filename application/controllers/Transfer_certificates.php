<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Transfer_certificates extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->model(array('Student', 'Admin_login'));
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}


	public function index()
	{
		$data = array();
		$cond = array();
		if ($_POST) {
			$sl_no = $this->input->post("sl_no");
			$sdata['sl_no'] = $sl_no;
			$this->session->set_userdata($sdata);
			$cond['sl_no'] = $sl_no;
		} else {
			$sl_no = $this->session->userdata('sl_no');
			$cond['sl_no'] = $sl_no;
		}
		$data['title'] = 'Transfer Certificate';
		$data['heading_msg'] = "Transfer Certificate";
		$this->load->library('pagination');
		$config['base_url'] = site_url('transfer_certificates/index/');
		$config['per_page'] = 20;
		$config['total_rows'] = count($this->Admin_login->get_all_transfer_certificate(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['testimonials'] = $this->Admin_login->get_all_transfer_certificate(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['is_show_button'] = "add";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('transfer_certificates/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add(){
		if($_POST){
			$student_id = $this->input->post('student_id', true);

			//student update
			$student_data = array();
			$student_data['admitted_class_id'] =  $this->input->post('admitted_class_id', true);
			$student_data['status'] = '0';
			$student_data['present_address'] = $this->input->post('address', true);
			$this->db->where('id', $student_id);
			$this->db->update('tbl_student', $student_data);

			$data = array();
			$data['student_id'] =  $student_id;
			$data['sl_no'] = $this->generateSerialNo(date('Y'));
			$data['student_id'] =  $this->input->post('student_id', true);
			$data['name'] =  $this->input->post('name', true);
			$data['fathers_name'] =  $this->input->post('fathers_name', true);
			$data['mothers_name'] =  $this->input->post('mothers_name', true);
			$data['address'] =  $this->input->post('address', true);
			$data['reason'] =  $this->input->post('reason', true);
			$data['left_date'] =  $this->input->post('left_date', true);
			$data['issue_date'] =  $this->input->post('issue_date', true);
			$this->db->insert("tbl_transfer_certificate", $data);
			$sdata['message'] = $this->lang->line('add_success_message') . ' ( SL. No - ' . $data['sl_no'] . ')';
			$this->session->set_userdata($sdata);
			redirect('transfer_certificates/index');
		}
		$data = array();
		$data['title'] = 'Transfer Certificate';
		$data['heading_msg'] = "Transfer Certificate";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['is_show_button'] = "index";
		$data['years'] = $this->Admin_login->getYearList(0, 0);
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['groups'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$data['classes'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
		$data['maincontent'] = $this->load->view('transfer_certificates/add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function generateSerialNo($year)
	{
		$code = "";
		$code .= ($year * 2) . date('m');
		$total = count($this->db->query("SELECT id FROM tbl_transfer_certificate")->result_array());
		return $code.str_pad(($total + 1), 8, '0', STR_PAD_LEFT);
	}

	public function view($id){
		$data = array();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['is_show_button'] = "index";
		$cond = array();
		$cond['id'] = $id;
		$data['transfer_info'] = $this->Admin_login->get_all_transfer_certificate(0, 0, $cond);
		$student_id = $data['transfer_info'][0]['student_id'];
		$data['student_info'] = $this->Student->get_student_info_by_student_id($student_id);
		$year = $data['student_info'][0]['year'];
		$subject_list_by_student = $this->db->query("SELECT su.name
							FROM
							`tbl_student_wise_subject` AS s
							LEFT JOIN `tbl_subject` AS su
							ON su.`id` = s.`subject_id`
							WHERE s.`student_id` = '$student_id' AND s.`year` = '$year'")->result_array();
		$data['subject_list_by_student'] = implode(", ", array_column($subject_list_by_student, "name"));
		$data['school_info'] = $this->db->query("SELECT * FROM tbl_contact_info")->row();
		$data['title'] = 'Transfer Certificate (' . $data['transfer_info'][0]['sl_no'] . ')';
		$data['heading_msg'] ='Transfer Certificate (' . $data['transfer_info'][0]['sl_no'] . ')';
		$this->load->view('transfer_certificates/view', $data);
	}

}
