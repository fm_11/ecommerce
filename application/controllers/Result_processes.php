<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Result_processes extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Student','Config_general' ,'Message', 'Admin_login', 'common/custom_methods_model'));
		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		date_default_timezone_set("Asia/Dhaka");
		$this->notification = array();
	}


	public function getClassWiseSubjectOrder($ClassID, $exam_type_id)
	{
		$rows = $this->db->query("SELECT s.`subject_id`,s.`order_number` FROM `tbl_class_wise_subject` AS s
              WHERE s.`class_id` = '$ClassID' AND s.`exam_type_id` = '$exam_type_id' ORDER BY s.`order_number`;")->result_array();
		$orders = array();
		foreach ($rows as $row) {
			$orders[$row['subject_id']]['subject_id'] = $row['subject_id'];
			$orders[$row['subject_id']]['order_number'] = $row['order_number'];
		}
		return $orders;
	}


	public function getStudentWiseOptionalSubject($class_id, $year)
	{
		$rows = $this->db->query("SELECT `student_id`,`subject_id` FROM `tbl_student_wise_subject`
WHERE `class_id` = $class_id AND `year` = $year AND `is_optional` = '1';")->result_array();
		$students = array();
		foreach ($rows as $row) {
			$students[$row['student_id']]['student_id'] = $row['student_id'];
			$students[$row['student_id']]['subject_id'] = $row['subject_id'];
		}
		return $students;
	}


	public function position_search($positions, $field, $value)
	{
		foreach ($positions as $key => $position) {
			if ($position[$field] === $value) {
				return $key;
			}
		}
		return false;
	}


	public function order_by_cgpa($a, $b)
	{
		return $b['CGPA'] > $a['CGPA'] ? 1 : -1;
	}

	public function getClassAndGroupAndExamTypeWiseSubjectList($class_id, $exam_type_id)
	{
		$groups = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
		$returnSubjectList = array();
		foreach ($groups as $group) {
			$group_id = $group['id'];
			$subjects = $this->db->query("SELECT s.`name`,s.`code`,c.* FROM `tbl_class_wise_subject` AS c
                          INNER JOIN `tbl_subject` as s on s.`id` = c.`subject_id`
                          WHERE c.`exam_type_id` = '$exam_type_id' AND c.`class_id` = '$class_id'
                           AND FIND_IN_SET('$group_id',group_list) <> 0 GROUP BY c.`subject_id` ORDER BY c.`order_number`")->result_array();


			foreach ($subjects as $subject) {
				$returnSubjectList[$group_id][$subject['subject_id']]['subject_id'] = $subject['subject_id'];
				$returnSubjectList[$group_id][$subject['subject_id']]['name'] = $subject['name'];
				$returnSubjectList[$group_id][$subject['subject_id']]['code'] = $subject['code'];
				$returnSubjectList[$group_id][$subject['subject_id']]['credit'] = $subject['credit'];
				$returnSubjectList[$group_id][$subject['subject_id']]['class_test'] = $subject['class_test'];
				$returnSubjectList[$group_id][$subject['subject_id']]['written'] = $subject['written'];
				$returnSubjectList[$group_id][$subject['subject_id']]['objective'] = $subject['objective'];
				$returnSubjectList[$group_id][$subject['subject_id']]['practical'] = $subject['practical'];
				$returnSubjectList[$group_id][$subject['subject_id']]['is_class_test_allow'] = $subject['is_class_test_allow'];
				$returnSubjectList[$group_id][$subject['subject_id']]['is_written_allow'] = $subject['is_written_allow'];
				$returnSubjectList[$group_id][$subject['subject_id']]['is_objective_allow'] = $subject['is_objective_allow'];
				$returnSubjectList[$group_id][$subject['subject_id']]['is_practical_allow'] = $subject['is_practical_allow'];
				$returnSubjectList[$group_id][$subject['subject_id']]['order_number'] = $subject['order_number'];
				$returnSubjectList[$group_id][$subject['subject_id']]['group_list'] = $subject['group_list'];
				$returnSubjectList[$group_id][$subject['subject_id']]['merge_code'] = $subject['merge_code'];
				$returnSubjectList[$group_id][$subject['subject_id']]['class_test_pass_marks'] = $subject['class_test_pass_marks'];
				$returnSubjectList[$group_id][$subject['subject_id']]['written_pass_marks'] = $subject['written_pass_marks'];
				$returnSubjectList[$group_id][$subject['subject_id']]['objective_pass_marks'] = $subject['objective_pass_marks'];
				$returnSubjectList[$group_id][$subject['subject_id']]['practical_pass_marks'] = $subject['practical_pass_marks'];
				$returnSubjectList[$group_id][$subject['subject_id']]['acceptance_written'] = $subject['acceptance_written'];
				$returnSubjectList[$group_id][$subject['subject_id']]['acceptance_objective'] = $subject['acceptance_objective'];
				$returnSubjectList[$group_id][$subject['subject_id']]['acceptance_practical'] = $subject['acceptance_practical'];
				$returnSubjectList[$group_id][$subject['subject_id']]['acceptance_class_test'] = $subject['acceptance_class_test'];
				$returnSubjectList[$group_id][$subject['subject_id']]['subject_type'] = $subject['subject_type'];
			}
		}
		return $returnSubjectList;
	}


	public function getMarkingListByStudent($StudentID, $exam_id)
	{
		$marking_dataList = $this->db->query("SELECT * FROM `tbl_marking`
        WHERE `student_id` = '$StudentID' AND `exam_id` = '$exam_id'")->result_array();
//		echo '<pre>';
//		print_r($marking_dataList);
//		die;

		$returnDataList = array();
		foreach ($marking_dataList as $marking_data) {
			$returnDataList[$marking_data['subject_id']]['student_id'] = $marking_data['student_id'];
			$returnDataList[$marking_data['subject_id']]['subject_id'] = $marking_data['subject_id'];
			$returnDataList[$marking_data['subject_id']]['written'] = $marking_data['written'];
			$returnDataList[$marking_data['subject_id']]['objective'] = $marking_data['objective'];
			$returnDataList[$marking_data['subject_id']]['practical'] = $marking_data['practical'];
			$returnDataList[$marking_data['subject_id']]['class_test'] = $marking_data['class_test'];
			$returnDataList[$marking_data['subject_id']]['allocated_written'] = $marking_data['allocated_written'];
			$returnDataList[$marking_data['subject_id']]['allocated_objective'] = $marking_data['allocated_objective'];
			$returnDataList[$marking_data['subject_id']]['allocated_practical'] = $marking_data['allocated_practical'];
			$returnDataList[$marking_data['subject_id']]['allocated_class_test'] = $marking_data['allocated_class_test'];
			$returnDataList[$marking_data['subject_id']]['credit'] = $marking_data['credit'];
			$returnDataList[$marking_data['subject_id']]['is_absent'] = $marking_data['is_absent'];
		}
		return $returnDataList;
	}

	public function getGradePointRangeData($class_id)
	{
		return $this->db->query("SELECT * FROM `tbl_result_grade_setup`
       WHERE `class_id` = '$class_id' ORDER BY `start_range` DESC")->result_array();
	}



	public function getNumberToGradePoint($Number, $Credit, $grade_list)
	{
		$Value = ($Number / $Credit);
		//echo $Value; die;
		//echo $Number . '/' . $pass_mark . '/' . $Value . '<br>';
		$grade_array = array();
		foreach ($grade_list as $grade) {
			$start_range = $grade['start_range'] / 100;
			$end_range = ($grade['end_range'] + 1) / 100;
			//echo $start_range . '/' . $end_range . '<br>';
			$numeric_gpa = $grade['numeric_gpa'];
			$alpha_gpa = $grade['alpha_gpa'];
			if ($Value >= $start_range && $Value <= $end_range) {
				$grade_array['alpha_gpa'] = $alpha_gpa;
				$grade_array['numeric_gpa'] = $numeric_gpa;
				break;
			}
		}
		return $grade_array;
	}


	public function index()
	{
		$data = array();
		if ($_POST) {
			$exam_id = $this->input->post("exam_id");
			$group_id = $this->input->post("group_id");
			$class_shift_section_id = $this->input->post("class_shift_section_id");
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id = $class_shift_section_arr[0];
			$section_id = $class_shift_section_arr[2];
			$shift_id = $class_shift_section_arr[1];

			$sdata['rp_group_id'] = $group_id;
			$sdata['rp_exam_id'] = $exam_id;
			$sdata['rp_class_shift_section_id'] = $class_shift_section_id;
			$this->session->set_userdata($sdata);

			$data = array();
			$data['exam_id'] = $exam_id;
			$data['class_id'] = $class_id;
			$data['section_id'] = $section_id;
			$data['shift_id'] = $shift_id;
			$data['group_id'] = $group_id;

			//exam details
			$exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$exam_id'")->result_array();
			if ($exam_info[0]['exam_type_id'] <= 0) {
				$sdata['exception'] = "Please input first what type of exam it is.";
				$this->session->set_userdata($sdata);
				redirect('exams/index');
			}
			$year = $exam_info[0]['year'];
			$exam_type_id = $exam_info[0]['exam_type_id'];

			$cond = array();
			$cond['class_id'] = $class_id;
			$cond['shift_id'] = $shift_id;
			$cond['section_id'] = $section_id;
			$cond['group'] = $group_id;
			$StudentList = $this->Student->get_all_student_list(0, 0, $cond);

			$subject_list = $this->getClassAndGroupAndExamTypeWiseSubjectList($class_id, $exam_type_id);
			$grade_point_data = $this->getGradePointRangeData($class_id);
			if (empty($grade_point_data)) {
				$sdata['exception'] = "Please complete grade point configuration first";
				$this->session->set_userdata($sdata);
				redirect('result_grades/add');
			}
			$students_optional_subject = $this->getStudentWiseOptionalSubject($class_id, $year);

			//echo '<pre>';
			//print_r($subject_list); die;

			if (!empty($StudentList)) {
				$loop_time = count($StudentList);
				$i = 0;
				while ($i < $loop_time) {
					$ResultData = $this->resultProcessByStudent($StudentList[$i], $class_id, $exam_info, $subject_list, $grade_point_data, $students_optional_subject);
//                  //$Result = $this->result_by_student($StudentList[$i]['id'], $class_id, $exam_id);
//                  echo '<pre>';
//                  print_r($ResultData);
//                  die;
					if ($ResultData['total_number_achieved'] != 0) {
						$student_result = array();
						$student_result['id'] = $ResultData['student_id'];
						$student_result['exam_id'] = $exam_id;
						$student_result['class_id'] = $class_id;
						$student_result['name'] = $ResultData['student_name'];
						$student_result['roll_no'] = $ResultData['roll_no'];
						$student_result['total_number_achieved'] = $ResultData['total_number_achieved'];
						$student_result['CGPA'] = $ResultData['CGPA'];
						$student_result['CGPAwithFourthSubject'] = $ResultData['CGPA'];
						$data['student_result'][$i] = $student_result;
					}
					$i++;
				}

				if (empty($data['student_result'])) {
					$sdata['exception'] = "Please complete marking first";
					$this->session->set_userdata($sdata);
					redirect('result_processes/index');
				}


				$ExamName = $this->db->query("SELECT * FROM `tbl_exam` WHERE id='$exam_id'")->result_array();
				$ClassName = $this->db->query("SELECT * FROM `tbl_class` WHERE id='$class_id'")->result_array();
				$SectionName = $this->db->query("SELECT * FROM `tbl_section` WHERE id='$section_id'")->result_array();
				$SchoolInfo = $this->db->query("SELECT `school_name`,`eiin_number` FROM `tbl_contact_info`")->result_array();

				$Info = array();
				$Info['ExamName'] = $ExamName[0]['name'];
				$Info['ClassName'] = $ClassName[0]['name'];
				$Info['SectionName'] = $SectionName[0]['name'];
				$Info['school_name'] = $SchoolInfo[0]['school_name'];
				$Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
				$data['ResultInfo'] = $Info;
			} else {
				$sdata['exception'] = "Student information not found";
				$this->session->set_userdata($sdata);
				redirect('result_processes/index');
			}
		}
		$data['title'] = 'Result Process';
		$data['heading_msg'] = "Result Process";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$current_system_year = $this->Admin_login->getCurrentSystemYear();
		$data['ExamList'] = $this->db->query("SELECT * FROM `tbl_exam` WHERE `year` = '$current_system_year'")->result_array();
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['GroupList'] = $this->Admin_login->getGroupList();
		$data['maincontent'] = $this->load->view('result_processes/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}


	public function margeCalculation($subjectList, $marking_data, $subject_id, $merge_code)
	{
		$returnData = array();
		foreach ($subjectList as $merge_subject_row) {

			if ($merge_subject_row['subject_id'] != $subject_id && $merge_subject_row['merge_code'] == $merge_code) {
				$merge_subject_id = $merge_subject_row['subject_id'];

				//acceptance mark calculation
				$accept_written_mark = ($marking_data[$merge_subject_row['subject_id']]['written'] * $merge_subject_row['acceptance_written']) / 100;
				$accept_objective_mark = ($marking_data[$merge_subject_row['subject_id']]['objective'] * $merge_subject_row['acceptance_objective']) / 100;
				$accept_practical_mark = ($marking_data[$merge_subject_row['subject_id']]['practical'] * $merge_subject_row['acceptance_practical']) / 100;
				$accept_class_test_mark = ($marking_data[$merge_subject_row['subject_id']]['class_test'] * $merge_subject_row['acceptance_class_test']) / 100;
				//end acceptance mark calculation

				$this_subject_fail = 0;
				if ($merge_subject_row['written_pass_marks'] > 0 && $marking_data[$merge_subject_row['subject_id']]['written'] < $merge_subject_row['written_pass_marks']) {
					$this_subject_fail = 1;
				}

				if ($merge_subject_row['objective_pass_marks'] > 0 && $marking_data[$merge_subject_row['subject_id']]['objective'] < $merge_subject_row['objective_pass_marks']) {
					$this_subject_fail = 1;
				}

				if ($merge_subject_row['practical_pass_marks'] > 0 && $marking_data[$merge_subject_row['subject_id']]['practical'] < $merge_subject_row['practical_pass_marks']) {
					$this_subject_fail = 1;
				}

				if ($merge_subject_row['class_test_pass_marks'] > 0 && $marking_data[$merge_subject_row['subject_id']]['class_test'] < $merge_subject_row['class_test_pass_marks']) {
					$this_subject_fail = 1;
				}

				$merge_subject_total_number = ($accept_written_mark +
					$accept_objective_mark +
					$accept_practical_mark +
					$accept_class_test_mark);

				$merge_subject_credit = $marking_data[$merge_subject_row['subject_id']]['credit'];
				$returnData['merge_subject_total_number'] = $merge_subject_total_number;
				$returnData['merge_subject_credit'] = $merge_subject_credit;
				$returnData['merge_subject_id'] = $merge_subject_id;
				$returnData['merge_subject_fail'] = $this_subject_fail;
				break;
			}
		}
		return $returnData;
	}

	public function result_process_data_save_new()
	{
		$exam_id = $this->input->post("exam_id");
		$class_id = $this->input->post("class_id");
		$section_id = $this->input->post("section_id");
		$shift_id = $this->input->post("shift_id");
		$group_id = $this->input->post("group_id");

		//exam details
		$exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$exam_id'")->result_array();
		if ($exam_info[0]['exam_type_id'] <= 0) {
			$sdata['exception'] = "Please input first what type of exam it is.";
			$this->session->set_userdata($sdata);
			redirect('exams/index');
		}
		$year = $exam_info[0]['year'];
		$exam_type_id = $exam_info[0]['exam_type_id'];
		$percentage_of_grand_result = $exam_info[0]['percentage_of_grand_result'];
		$is_applicable_for_final_calcultion = $exam_info[0]['is_applicable_for_final_calcultion'];
		$is_annual_exam = $exam_info[0]['is_annual_exam'];
		$is_combined_result = $exam_info[0]['is_combined_result'];
		$combined_percentage = $exam_info[0]['combined_percentage'];


		$config = $this->db->query("SELECT annual_mark_calculate_method,calculable_total_mark_percentage,
									how_to_generate_position,multiple_atudent_allow_same_position,
									is_calculate_cgpa_when_fail FROM `tbl_config`")->result_array();
		$annual_mark_calculate_method = "";
		$multiple_student_allow_same_position = '0';
		if (!empty($config)) {
			$annual_mark_calculate_method = $config[0]['annual_mark_calculate_method'];
			$multiple_student_allow_same_position =  $config[0]['multiple_atudent_allow_same_position'];
		}

		$cond = array();
		$cond['class_id'] = $class_id;
		$cond['shift_id'] = $shift_id;
		$cond['section_id'] = $section_id;
		$cond['group'] = $group_id;
		$StudentList = $this->Student->get_all_student_list(0, 0, $cond);

		$subject_list = $this->getClassAndGroupAndExamTypeWiseSubjectList($class_id, $exam_type_id);
		$grade_point_data = $this->getGradePointRangeData($class_id);
		if (empty($grade_point_data)) {
			$sdata['exception'] = "Please complete grade point configuration first";
			$this->session->set_userdata($sdata);
			redirect('result_grades/add');
		}
		$students_optional_subject = $this->getStudentWiseOptionalSubject($class_id, $year);

		$how_to_generate_position = 1;
		$position_create_for_failed_students = 0;
		$general_config = $this->Config_general->get_general_configurations('marksheet');
		if(isset($general_config->how_to_generate_position)){
			$how_to_generate_position = $general_config->how_to_generate_position;
		}
		if(isset($general_config->position_create_for_failed_students)){
			$position_create_for_failed_students = $general_config->position_create_for_failed_students;
		}


		//$how_to_generate_position = 0 (Position with GPA) & 1 = (Position with Total Mark)
		//echo $how_to_generate_position; die;


		if (!empty($StudentList)) {
			$loop_time = count($StudentList);

			if($loop_time == 0){
				$sdata['exception'] = "Not result information found";
				$this->session->set_userdata($sdata);
				redirect('result_processes/index');
			}

			$i = 0;
			while ($i < $loop_time) {
				$ResultData = $this->resultProcessByStudent($StudentList[$i], $class_id, $exam_info, $subject_list, $grade_point_data, $students_optional_subject);
//                echo '<pre>';
//                print_r($ResultData);
//                die;
				if ($ResultData['total_number_achieved'] != 0) {
					$student_id = $ResultData['student_id'];
					//echo $student_id; die;
					$student_result = array();
					$student_result['exam_id'] = $exam_id;
					$student_result['student_id'] = $student_id;
					$student_result['class_id'] = $class_id;
					$student_result['roll_no'] = $ResultData['roll_no'];
					$student_result['reg_no'] = $ResultData['reg_no'];
					$student_result['section_id'] = $section_id;
					$student_result['group'] = $group_id;
					$student_result['shift'] = $shift_id;
					$student_result['year'] = $StudentList[$i]['year'];
					$student_result['total_credit'] = $ResultData['total_credit'];
					$total_credit = $ResultData['total_credit'];
					$student_result['total_obtain_mark'] = $ResultData['total_number_achieved'];
					$student_result['gpa_with_optional'] = $ResultData['CGPA'];
					$student_result['gpa_without_optional'] = $ResultData['CGPAwithoutFourthSubject'];
					$student_result['c_alpha_gpa_with_optional'] = $this->get_numeric_gpa_to_alfa_gpa($ResultData['CGPA']);
					$student_result['c_alpha_gpa_without_optional'] = $this->get_numeric_gpa_to_alfa_gpa($ResultData['CGPAwithoutFourthSubject']);
					$student_result['average_percentage_for_final'] = 0;
					$student_result['average_mark_for_final'] = 0;
					$student_result['calculable_subject'] = $ResultData['calculable_subject'];
					$student_result['calculable_total_grade_point'] = $ResultData['calculable_total_grade_point'];
					$student_result['class_position'] = 0;
					$student_result['shift_position'] = 0;
					$student_result['section_position'] = 0;
					$student_result['group_position'] = 0;
					$student_result['number_of_failed_subject'] = $ResultData['number_of_failed_subject'];
					$student_result['is_annual_exam'] = $is_annual_exam;
					$student_result['date'] = date("Y-m-d H:i:s");
					$this->db->query("DELETE FROM `tbl_combined_result_details` WHERE `exam_id` = '$exam_id' AND `student_id` = '$student_id'");
					$this->db->query("DELETE FROM `tbl_result_process_details` WHERE `exam_id` = '$exam_id' AND `student_id` = '$student_id'");
					$this->db->query("DELETE FROM `tbl_result_process` WHERE `exam_id` = '$exam_id' AND `student_id` = '$student_id'");
					$this->db->insert("tbl_result_process", $student_result);
					$result_id = $this->db->insert_id();

					$result_details_data = array();
					foreach ($ResultData['subject_wise_data'] as $subject_wise_data) {
						$result_detail = array();
						$result_detail['result_id'] = $result_id;
						$result_detail['exam_id'] = $exam_id;
						$result_detail['student_id'] = $student_id;
						$result_detail['class_id'] = $class_id;
						$result_detail['section_id'] = $section_id;
						$result_detail['group'] = $group_id;
						$result_detail['shift'] = $shift_id;
						$result_detail['year'] = $StudentList[$i]['year'];
						$result_detail['subject_id'] = $subject_wise_data['subject_id'];
						$result_detail['subject_name'] = $subject_wise_data['subject_name'];
						$result_detail['subject_code'] = $subject_wise_data['subject_code'];
						$result_detail['order_number'] = $subject_wise_data['order_number'];
						$result_detail['credit'] = $subject_wise_data['credit'];
						$result_detail['allocated_written'] = $subject_wise_data['allocated_written'];
						$result_detail['allocated_objective'] = $subject_wise_data['allocated_objective'];
						$result_detail['allocated_practical'] = $subject_wise_data['allocated_practical'];
						$result_detail['allocated_class_test'] = $subject_wise_data['allocated_class_test'];
						$result_detail['acceptance_written_percentage'] = $subject_wise_data['acceptance_written'];
						$result_detail['acceptance_objective_percentage'] = $subject_wise_data['acceptance_objective'];
						$result_detail['acceptance_practical_percentage'] = $subject_wise_data['acceptance_practical'];
						$result_detail['acceptance_class_test_percentage'] = $subject_wise_data['acceptance_class_test'];
						$result_detail['written'] = $subject_wise_data['written'];
						$result_detail['objective'] = $subject_wise_data['objective'];
						$result_detail['practical'] = $subject_wise_data['practical'];
						$result_detail['class_test'] = $subject_wise_data['class_test'];
						$result_detail['total_extra_mark'] = $subject_wise_data['total_extra_mark'];
						$result_detail['total_number_get_from_other_exam'] = $subject_wise_data['total_number_get_from_other_exam'];
						$result_detail['total_obtain'] = $subject_wise_data['total_obtain_mark'];
						$result_detail['calculable_total_obtain'] = $subject_wise_data['calculable_total_obtain'];
						$result_detail['alpha_gpa'] = $subject_wise_data['alpha_gpa'];
						$result_detail['numeric_gpa'] = $subject_wise_data['grade_point'];
						$result_detail['is_optional'] = $subject_wise_data['is_optional'];
						$result_detail['is_absent'] = $subject_wise_data['is_absent'];
						$result_detail['merge_code'] = $subject_wise_data['merge_code'];
						$result_detail['merge_subject_id'] = $subject_wise_data['merge_subject_id'];
						$result_detail['subject_type'] = $subject_wise_data['subject_type'];
						$result_details_data[] = $result_detail;
					}
					$this->db->insert_batch('tbl_result_process_details', $result_details_data);

					//for annual calculation
					if ($annual_mark_calculate_method == 'TMS' && $is_annual_exam == 1) {
						$total_average_mark = 0;
						$ar_exam_list = $this->db->query("SELECT * FROM `tbl_exam` 
                                        WHERE is_applicable_for_final_calcultion = 1 AND 
                                        year = '$year' order by exam_order")->result_array();
						$k = 0;
						while ($k < count($ar_exam_list)) {
							$ar_exam_id = $ar_exam_list[$k]['id'];
							$ar_mark_info = $this->db->query("SELECT total_obtain_mark FROM `tbl_result_process` 
                                            WHERE student_id = '$student_id' AND exam_id = '$ar_exam_id' 
                                            AND year = '$year'")->result_array();
							if (!empty($ar_mark_info)) {
								$total_average_mark = $total_average_mark + $ar_mark_info[0]['total_obtain_mark'];
							}
							$k++;
						}
						$save_data = array();
						$save_data['grand_average_mark'] = $total_average_mark;
						$this->db->where('id', $result_id);
						$this->db->update('tbl_result_process', $save_data);
					}else{
						//annual position for semester wise percentage
						if ($is_applicable_for_final_calcultion == 1) {
							$save_data  = array();
							//need dynamic config
							$is_average_mark_percentage_by_total_credit = 0;
							if ($is_average_mark_percentage_by_total_credit == 1) {
								$save_data['average_percentage_for_final'] =  $percentage_of_grand_result;
								$save_data['average_mark_for_final'] = ($ResultData['total_number_achieved'] * $percentage_of_grand_result) / $total_credit;
							} else {
								$save_data['average_percentage_for_final'] =  $percentage_of_grand_result;
								$save_data['average_mark_for_final'] = ($ResultData['total_number_achieved'] * $percentage_of_grand_result) / 100;
							}

							//final calculation
							if ($is_annual_exam == 1) {
								$total_average_mark = 0;
								$ar_exam_list = $this->db->query("SELECT * FROM `tbl_exam` WHERE is_applicable_for_final_calcultion = 1 
											 AND year = '$year' order by exam_order")->result_array();
								$k = 0;
								while ($k < count($ar_exam_list)) {
									$ar_exam_id = $ar_exam_list[$k]['id'];
									if ($exam_id == $ar_exam_id) {
										$total_average_mark += $save_data['average_mark_for_final'];
									} else {
										$ar_mark_info = $this->db->query("SELECT * FROM `tbl_result_process` WHERE student_id = '$student_id'
													AND exam_id = '$ar_exam_id' AND year = '$year'")->result_array();
										if (!empty($ar_mark_info)) {
											$total_average_mark += $ar_mark_info[0]['average_mark_for_final'];
										}
									}
									$k++;
								}
								$save_data['grand_average_mark'] = number_format($total_average_mark, 4);
								$save_data['grand_average_cgpa'] =  number_format($this->number_to_num_grade($total_average_mark, 100), 4);
								$save_data['grand_average_grade'] =  $this->number_to_alfa_grade($total_average_mark, 100);
							}
							//final calculation

							$this->db->where('id', $result_id);
							$this->db->update('tbl_result_process', $save_data);
						}
						//end annual position for semester wise percentage
					}
					//end for annual calculation
				}
				$i++;
			}

			//combined result calculation
			if ($is_combined_result == 1) {
				$this->combinedResultCalculation($StudentList, $exam_id, $combined_percentage, $class_id, $section_id, $group_id, $shift_id,$multiple_student_allow_same_position);
			}
			//end combined result calculation

			//position calculation for normal exam
			if ($is_combined_result != 1 && $is_annual_exam != 1) {
				$this->normalResultCalculation($exam_id, $class_id, $section_id, $group_id, $shift_id,$multiple_student_allow_same_position, $how_to_generate_position, $position_create_for_failed_students);
			}
			//end position calculation for normal exam

			//position calculation for grand exam
			if($is_annual_exam == 1){
				$this->grandResultCalculation($exam_id, $class_id, $section_id, $group_id, $shift_id,$multiple_student_allow_same_position);
			}
			//end position calculation for grand exam

			$sdata['message'] = "Result Processed Successfully !";
			$this->session->set_userdata($sdata);
			redirect("result_processes/index");
		}else{
			$sdata['exception'] = "Student not found";
			$this->session->set_userdata($sdata);
			redirect('result_processes/index');
		}
	}


	public function grandResultCalculation($exam_id, $class_id, $section_id, $group_id, $shift_id,$multiple_student_allow_same_position){
		//class wise position
		$class_positions = $this->db->query("SELECT id,student_id,exam_id,grand_average_mark AS total_mark_for_position,
							gpa_with_optional,gpa_without_optional FROM tbl_result_process
							WHERE exam_id = '$exam_id' AND class_id = '$class_id'
							ORDER BY grand_average_mark DESC;")->result_array();
		$this->calculatePosition($class_positions,$multiple_student_allow_same_position,'CW',$exam_id);

		//class and shift wise position
		$class_shift_positions = $this->db->query("SELECT id,student_id,exam_id,grand_average_mark AS total_mark_for_position,
							gpa_with_optional,gpa_without_optional FROM tbl_result_process
							WHERE exam_id = '$exam_id' AND class_id = '$class_id' AND `shift` = '$shift_id'
							ORDER BY grand_average_mark DESC;")->result_array();
		$this->calculatePosition($class_shift_positions,$multiple_student_allow_same_position,'SW',$exam_id);

		//class, shift and section wise position
		$class_shift_section_positions = $this->db->query("SELECT id,student_id,exam_id,grand_average_mark AS total_mark_for_position,
							gpa_with_optional,gpa_without_optional FROM tbl_result_process
							WHERE exam_id = '$exam_id' AND class_id = '$class_id' AND section_id = '$section_id' AND `shift` = '$shift_id'
							ORDER BY grand_average_mark DESC;")->result_array();
		$this->calculatePosition($class_shift_section_positions,$multiple_student_allow_same_position,'SECW',$exam_id);

		//class, shift and section wise position
		$class_shift_section_group_positions = $this->db->query("SELECT id,student_id,exam_id,grand_average_mark AS total_mark_for_position,
							gpa_with_optional,gpa_without_optional FROM tbl_result_process
							WHERE exam_id = '$exam_id' AND class_id = '$class_id' AND section_id = '$section_id' AND `shift` = '$shift_id'
							AND  `group` = '$group_id' ORDER BY grand_average_mark DESC;")->result_array();
		$this->calculatePosition($class_shift_section_group_positions,$multiple_student_allow_same_position,'GW',$exam_id);

		return true;
	}

	public function normalResultCalculation($exam_id, $class_id, $section_id, $group_id, $shift_id,$multiple_student_allow_same_position,$how_to_generate_position,$position_create_for_failed_students){
		$failed_position_where = "";
		if($position_create_for_failed_students == 0){
			$failed_position_where = " AND `c_alpha_gpa_with_optional` != 'F' ";
		}
		if($how_to_generate_position == '0'){ //0 = position with GPA
			$position_where = " ORDER BY gpa_with_optional DESC, total_obtain_mark DESC ,ABS(roll_no) ASC ";
		}else{
			$position_where = " ORDER BY total_obtain_mark DESC, gpa_with_optional DESC, ABS(roll_no) ASC ";
		}
		//class wise position
		$class_positions = $this->db->query("SELECT id,student_id,exam_id,total_obtain_mark AS total_mark_for_position,
							gpa_with_optional,gpa_without_optional FROM tbl_result_process
							WHERE exam_id = '$exam_id' AND class_id = '$class_id' $failed_position_where 
							$position_where;")->result_array();
		$this->calculatePosition($class_positions,$multiple_student_allow_same_position,'CW',$exam_id);

		//class and shift wise position
		$class_shift_positions = $this->db->query("SELECT id,student_id,exam_id,total_obtain_mark AS total_mark_for_position,
							gpa_with_optional,gpa_without_optional FROM tbl_result_process
							WHERE exam_id = '$exam_id' AND class_id = '$class_id'
							  $failed_position_where AND `shift` = '$shift_id'
							$position_where;")->result_array();
		$this->calculatePosition($class_shift_positions,$multiple_student_allow_same_position,'SW',$exam_id);

		//class, shift and section wise position
		$class_shift_section_positions = $this->db->query("SELECT id,student_id,exam_id,total_obtain_mark AS total_mark_for_position,
							gpa_with_optional,gpa_without_optional FROM tbl_result_process
							WHERE exam_id = '$exam_id' AND class_id = '$class_id' AND section_id = '$section_id'
							 AND `shift` = '$shift_id' $failed_position_where  
							$position_where;")->result_array();
		$this->calculatePosition($class_shift_section_positions,$multiple_student_allow_same_position,'SECW',$exam_id);

		//class, shift and section wise position
		$class_shift_section_group_positions = $this->db->query("SELECT id,student_id,exam_id,total_obtain_mark AS total_mark_for_position,
							gpa_with_optional,gpa_without_optional FROM tbl_result_process
							WHERE exam_id = '$exam_id' AND class_id = '$class_id' AND  `group` = '$group_id' $failed_position_where 
							 $position_where;")->result_array();
		$this->calculatePosition($class_shift_section_group_positions,$multiple_student_allow_same_position,'GW',$exam_id);

		return true;
	}

	public function combinedResultCalculation($StudentList, $exam_id, $combined_percentage, $class_id, $section_id, $group_id, $shift_id,$multiple_student_allow_same_position){
		$combined_exam_info = $this->db->query("SELECT * FROM `tbl_exam_combined_result`
                   WHERE `exam_id` = '$exam_id'")->result_array();
		if (!empty($combined_exam_info)) {
			foreach ($StudentList as $combined_student) {
				$cr = 0;
				$combined_result_data = array();
				//for parent exam
				$combined_result_data[$cr]['exam_id'] = $exam_id;
				$student_id = $combined_student['id'];
				$combined_result_data[$cr]['student_id'] = $student_id;

				$combined_result_data[$cr]['class_id'] =  $class_id;
				$combined_result_data[$cr]['section_id'] =  $section_id;
				$combined_result_data[$cr]['group_id'] =  $group_id;
				$combined_result_data[$cr]['shift_id'] =  $shift_id;

				$combined_exam_id = $exam_id;
				$combined_result_data[$cr]['combined_exam_id'] = $exam_id;
				$combined_result_data[$cr]['percentage_from_total'] = $combined_percentage;
				$result_info_for_this_student_this_exam = $this->db->query("SELECT total_obtain_mark FROM `tbl_result_process`
                           WHERE `exam_id` = '$combined_exam_id' AND `student_id` = '$student_id'")->result_array();
				if (empty($result_info_for_this_student_this_exam)) {
					$combined_result_data[$cr]['total_obtained_mark'] = 0;
					$combined_result_data[$cr]['calculable_mark_from_total'] = 0;
				} else {
					$combined_result_data[$cr]['total_obtained_mark'] = $result_info_for_this_student_this_exam[0]['total_obtain_mark'];
					$combined_result_data[$cr]['calculable_mark_from_total'] = $result_info_for_this_student_this_exam[0]['total_obtain_mark'] * $combined_percentage / 100;
				}

				$cr++;

				foreach ($combined_exam_info as $combined_exam) {
					$combined_result_data[$cr]['exam_id'] = $exam_id;
					$combined_result_data[$cr]['student_id'] = $student_id;

					$combined_result_data[$cr]['class_id'] =  $class_id;
					$combined_result_data[$cr]['section_id'] =  $section_id;
					$combined_result_data[$cr]['group_id'] =  $group_id;
					$combined_result_data[$cr]['shift_id'] =  $shift_id;

					$combined_exam_id = $combined_exam['combined_exam_id'];
					$combined_result_data[$cr]['combined_exam_id'] = $combined_exam_id;
					$combined_result_data[$cr]['percentage_from_total'] = $combined_exam['percentage'];
					$result_info_for_this_student_this_exam = $this->db->query("SELECT total_obtain_mark FROM `tbl_result_process`
                               WHERE `exam_id` = '$combined_exam_id' AND `student_id` = '$student_id'")->result_array();
					if (empty($result_info_for_this_student_this_exam)) {
						$combined_result_data[$cr]['total_obtained_mark'] = 0;
						$combined_result_data[$cr]['calculable_mark_from_total'] = 0;
					} else {
						$combined_result_data[$cr]['total_obtained_mark'] = $result_info_for_this_student_this_exam[0]['total_obtain_mark'];
						$combined_result_data[$cr]['calculable_mark_from_total'] = $result_info_for_this_student_this_exam[0]['total_obtain_mark'] * $combined_exam['percentage'] / 100;
					}
					$cr++;
				}
				$this->db->insert_batch('tbl_combined_result_details', $combined_result_data);
			}


			//class wise position
			$class_combined_positions = $this->db->query("SELECT cr.`student_id`,SUM(cr.`calculable_mark_from_total`) AS total_mark_for_position
									FROM `tbl_combined_result_details` AS cr
									WHERE cr.`class_id` = '$class_id' GROUP BY cr.`student_id` 
									ORDER BY SUM(cr.`calculable_mark_from_total`) DESC")->result_array();
			$this->calculatePosition($class_combined_positions,$multiple_student_allow_same_position,'CW',$exam_id);


			//class and shift wise position
			$class_shift_combined_positions = $this->db->query("SELECT cr.`student_id`,SUM(cr.`calculable_mark_from_total`) AS total_mark_for_position 
									FROM `tbl_combined_result_details` AS cr
									WHERE cr.`class_id` = '$class_id'  AND cr.`shift_id` = '$shift_id' GROUP BY cr.`student_id` 
									ORDER BY SUM(cr.`calculable_mark_from_total`) DESC")->result_array();
			$this->calculatePosition($class_shift_combined_positions,$multiple_student_allow_same_position,'SW',$exam_id);

			//class, shift and section wise position
			$class_shift_section_combined_positions = $this->db->query("SELECT cr.`student_id`,SUM(cr.`calculable_mark_from_total`) AS total_mark_for_position 
									FROM `tbl_combined_result_details` AS cr
									WHERE cr.`class_id` = '$class_id'  AND cr.`shift_id` = '$shift_id' AND cr.`section_id` = '$section_id' GROUP BY cr.`student_id` 
									ORDER BY SUM(cr.`calculable_mark_from_total`) DESC")->result_array();
			$this->calculatePosition($class_shift_section_combined_positions,$multiple_student_allow_same_position,'SECW',$exam_id);

			//class, shift, section and group wise position
			$class_shift_section_group_combined_positions = $this->db->query("SELECT cr.`student_id`,SUM(cr.`calculable_mark_from_total`) AS total_mark_for_position 
									FROM `tbl_combined_result_details` AS cr
									WHERE cr.`class_id` = '$class_id'  AND cr.`shift_id` = '$shift_id' AND cr.`section_id` = '$section_id' 
									AND cr.`group_id` = '$group_id' GROUP BY cr.`student_id` 
									ORDER BY SUM(cr.`calculable_mark_from_total`) DESC")->result_array();
			$this->calculatePosition($class_shift_section_group_combined_positions,$multiple_student_allow_same_position,'GW',$exam_id);

		}
		return true;
	}

	public function calculatePosition($resultData,$multiple_student_allow_same_position,$positionType,$exam_id){
		if($multiple_student_allow_same_position == '1'){
			$updateArray = array();
			$same_position_count = 0;
			for ($x = 0; $x < count($resultData); $x++) {
				if ($x == 0) {
					$position = ($x + 1);
				} else {
					if ($resultData[$x - 1]['total_mark_for_position'] == $resultData[$x]['total_mark_for_position']) {
						$same_position_count = $same_position_count + 1;
						$position = ($x + 1) - $same_position_count;
					} else {
						if ($same_position_count > 0) {
							$position = ($x + 1) - $same_position_count;
						} else {
							$position = ($x + 1);
						}
					}
				}
				if($positionType == 'CW'){
					$updateArray[] = array(
						'student_id'=> $resultData[$x]['student_id'],
						'class_position' => $position
					);
				}
				if($positionType == 'SW'){
					$updateArray[] = array(
						'student_id'=> $resultData[$x]['student_id'],
						'shift_position' => $position
					);
				}
				if($positionType == 'SECW'){
					$updateArray[] = array(
						'student_id'=> $resultData[$x]['student_id'],
						'section_position' => $position
					);
				}if($positionType == 'GW'){
					$updateArray[] = array(
						'student_id'=> $resultData[$x]['student_id'],
						'group_position' => $position
					);
				}

			}
			$this->db->where('exam_id', $exam_id);
			$this->db->update_batch('tbl_result_process', $updateArray, 'student_id');
		}else{
			$updateArray = array();
			$cp = 1;
			foreach ($resultData as $position) {
				$student_id = $position['student_id'];
				if($positionType == 'CW'){
					$updateArray['class_position'] = $cp;
				}
				if($positionType == 'SW'){
					$updateArray['shift_position'] = $cp;
				}
				if($positionType == 'SECW'){
					$updateArray['section_position'] = $cp;
				}
				if($positionType == 'GW'){
					$updateArray['group_position'] = $cp;
				}
				$this->db->where('student_id', $student_id);
				$this->db->where('exam_id', $exam_id);
				$this->db->update('tbl_result_process', $updateArray);
				$cp++;
			}
		}
		return true;
	}


	public function resultProcessByStudent($StudentInfo, $ClassID, $exam_info, $subject_list, $grade_point_data, $students_optional_subject)
	{

//		echo '<pre>';
//		print_r($subject_list);
//		die;

		$exam_type_id = $exam_info[0]['exam_type_id'];
		$StudentID = $StudentInfo['id'];
		$year = $exam_info[0]['year'];
		$exam_id = $exam_info[0]['id'];
		$group_id = $StudentInfo['group'];

		//global variable
		$IsPassed = 1;
		$number_of_failed_subject = 0;
		$num_of_cal_sub = 0;
		$total_grade_point = 0;
		//global variable

		$is_annual_exam = 0;
		if (isset($exam_info[0]['is_annual_exam'])) {
			$is_annual_exam = $exam_info[0]['is_annual_exam'];
		}

		$total_number_achieved = 0;
		$is_calculate_cgpa_when_fail = 0;
		$num_of_subject_allow_for_grace = 0;
		$num_of_subject_allow_for_grace_count = 0;
		$calculable_total_mark_percentage = 100;
		$annual_mark_calculate_method = "";
		$config = $this->db->query("SELECT annual_mark_calculate_method,calculable_total_mark_percentage,
        is_calculate_cgpa_when_fail, num_of_subject_allow_for_grace FROM `tbl_config`")->result_array();
		if (!empty($config)) {
			$is_calculate_cgpa_when_fail = $config[0]['is_calculate_cgpa_when_fail'];
			$num_of_subject_allow_for_grace = $config[0]['num_of_subject_allow_for_grace'];
			$calculable_total_mark_percentage = $config[0]['calculable_total_mark_percentage'];
			$annual_mark_calculate_method = $config[0]['annual_mark_calculate_method'];
		}

		$marking_data = $this->getMarkingListByStudent($StudentID, $exam_id);

		if (isset($subject_list[$group_id])) {
			$subjectList = $subject_list[$group_id];
			$returnResultData = array();
			$returnResultDataArray = array();
			$subject_row_num = 0;
			$optional_subject_point = 0;
			$is_optional_found = 0;
			$total_merge_subject = 0;
			$total_merge_subject_grade_point = 0;
			$total_credit = 0;
			foreach ($subjectList as $subject_row) {
				if(isset($marking_data[$subject_row['subject_id']])){
					$returnResultData[$subject_row_num]['subject_id'] = $subject_row['subject_id'];
					$returnResultData[$subject_row_num]['subject_name'] = $subject_row['name'];
					$returnResultData[$subject_row_num]['subject_code'] = $subject_row['code'];
					$returnResultData[$subject_row_num]['order_number'] = $subject_row['order_number'];
					$returnResultData[$subject_row_num]['subject_type'] = $subject_row['subject_type'];


					if ($marking_data[$subject_row['subject_id']]) {
//							echo '<pre>';
//							print_r($marking_data[$subject_row['subject_id']]);
//							die;
						$returnResultData[$subject_row_num]['allocated_written'] = $marking_data[$subject_row['subject_id']]['allocated_written'];
						$returnResultData[$subject_row_num]['allocated_objective'] = $marking_data[$subject_row['subject_id']]['allocated_objective'];
						$returnResultData[$subject_row_num]['allocated_practical'] = $marking_data[$subject_row['subject_id']]['allocated_practical'];
						$returnResultData[$subject_row_num]['allocated_class_test'] = $marking_data[$subject_row['subject_id']]['allocated_class_test'];

						$returnResultData[$subject_row_num]['acceptance_written'] = $subject_row['acceptance_written'];
						$returnResultData[$subject_row_num]['acceptance_objective'] = $subject_row['acceptance_objective'];
						$returnResultData[$subject_row_num]['acceptance_practical'] = $subject_row['acceptance_practical'];
						$returnResultData[$subject_row_num]['acceptance_class_test'] = $subject_row['acceptance_class_test'];


						$returnResultData[$subject_row_num]['written'] = $marking_data[$subject_row['subject_id']]['written'];
						$returnResultData[$subject_row_num]['objective'] = $marking_data[$subject_row['subject_id']]['objective'];
						$returnResultData[$subject_row_num]['practical'] = $marking_data[$subject_row['subject_id']]['practical'];
						$returnResultData[$subject_row_num]['class_test'] = $marking_data[$subject_row['subject_id']]['class_test'];

						$this_optional_subject = 0;
						$this_subject_fail = 0;

						if($subject_row['subject_type'] != 'UNC'){
							if ($subject_row['written_pass_marks'] > 0 && $marking_data[$subject_row['subject_id']]['written'] < $subject_row['written_pass_marks']) {
								$this_subject_fail = 1;
							}

							if ($subject_row['objective_pass_marks'] > 0 && $marking_data[$subject_row['subject_id']]['objective'] < $subject_row['objective_pass_marks']) {
								$this_subject_fail = 1;
							}

							if ($subject_row['practical_pass_marks'] > 0 && $marking_data[$subject_row['subject_id']]['practical'] < $subject_row['practical_pass_marks']) {
								$this_subject_fail = 1;
							}

							if ($subject_row['class_test_pass_marks'] > 0 && $marking_data[$subject_row['subject_id']]['class_test'] < $subject_row['class_test_pass_marks']) {
								$this_subject_fail = 1;
							}
							$total_credit += $marking_data[$subject_row['subject_id']]['credit'];
						}


						$returnResultData[$subject_row_num]['credit'] = $marking_data[$subject_row['subject_id']]['credit'];
						$returnResultData[$subject_row_num]['is_absent'] = $marking_data[$subject_row['subject_id']]['is_absent'];


						//optional subject
						$returnResultData[$subject_row_num]['is_optional'] = '0';
						if (isset($students_optional_subject[$StudentInfo['id']])) {
							if ($students_optional_subject[$StudentInfo['id']]['subject_id'] == $subject_row['subject_id']) {
								$returnResultData[$subject_row_num]['is_optional'] = '1';
								$is_optional_found = 1;
								$this_optional_subject = 1;
							}
						}
						//end optional subject


						//marge calculation
						$merge_code = $subject_row['merge_code'];
						$merge_subject_total_number = 0;
						$merge_subject_credit = 0;
						$merge_subject_id = 0;
						$merge_subject_fail = 0;
						if ($merge_code > 0) {
							$total_merge_subject += 1;
							$merge_calculation = $this->margeCalculation($subjectList, $marking_data, $subject_row['subject_id'], $merge_code);
							//echo '<pre>';
							//print_r($merge_calculation);
							$merge_subject_total_number = $merge_calculation['merge_subject_total_number'];
							$merge_subject_credit = $merge_calculation['merge_subject_credit'];
							$merge_subject_id = $merge_calculation['merge_subject_id'];
							$merge_subject_fail = $merge_calculation['merge_subject_fail'];
						}
						$returnResultData[$subject_row_num]['merge_code'] = $merge_code;
						$returnResultData[$subject_row_num]['merge_subject_id'] = $merge_subject_id;
						//marge calculation


						if($subject_row['subject_type'] != 'UNC') {
							if (($marking_data[$subject_row['subject_id']]['is_absent'] == '1' || $this_subject_fail == 1) && $this_optional_subject != 1) {
								$IsPassed = 0;
							}
						}



						//extra mark calculation
						$total_extra_mark = 0;
						$returnResultData[$subject_row_num]['total_extra_mark'] = $total_extra_mark;
						//extra mark calculation

						$total_obtain_mark = ($marking_data[$subject_row['subject_id']]['written'] +
							$marking_data[$subject_row['subject_id']]['objective'] +
							$marking_data[$subject_row['subject_id']]['practical'] +
							$marking_data[$subject_row['subject_id']]['class_test']);


						//acceptance mark calculation
						$accept_written_mark = ($marking_data[$subject_row['subject_id']]['written'] * $returnResultData[$subject_row_num]['acceptance_written']) / 100;
						$accept_objective_mark = ($marking_data[$subject_row['subject_id']]['objective'] * $returnResultData[$subject_row_num]['acceptance_objective']) / 100;
						$accept_practical_mark = ($marking_data[$subject_row['subject_id']]['practical'] * $returnResultData[$subject_row_num]['acceptance_practical']) / 100;
						$accept_class_test_mark = ($marking_data[$subject_row['subject_id']]['class_test'] * $returnResultData[$subject_row_num]['acceptance_class_test']) / 100;
						//end acceptance mark calculation
						//echo $accept_written_mark; die;


						$calculable_total_obtain_mark = $accept_written_mark + $accept_objective_mark + $accept_practical_mark + $accept_class_test_mark;

						$returnResultData[$subject_row_num]['total_obtain_mark'] = $total_obtain_mark;
						$returnResultData[$subject_row_num]['calculable_total_obtain'] = $calculable_total_obtain_mark;
						$returnResultData[$subject_row_num]['total_number_get_from_other_exam'] = 0;

						if ($this_subject_fail == 0) {
							if ($merge_code > 0) {

								$total_mark_with_merge = $calculable_total_obtain_mark + $merge_subject_total_number;
								$total_credit_with_merge_sub_credit = $marking_data[$subject_row['subject_id']]['credit'] + $merge_subject_credit;
								if($merge_subject_fail == 1){
									$grade['alpha_gpa'] = 'F';
									$grade['numeric_gpa'] = '0';
								}else{
									$grade = $this->getNumberToGradePoint($total_mark_with_merge, $total_credit_with_merge_sub_credit, $grade_point_data);
								}
								$total_merge_subject_grade_point = $total_merge_subject_grade_point +  $grade['numeric_gpa'];
							} else {
								$grade = $this->getNumberToGradePoint($calculable_total_obtain_mark, $marking_data[$subject_row['subject_id']]['credit'], $grade_point_data);
							}

							$returnResultData[$subject_row_num]['alpha_gpa'] = $grade['alpha_gpa'];
							$returnResultData[$subject_row_num]['grade_point'] = $grade['numeric_gpa'];
							$total_grade_point += $grade['numeric_gpa'];
						} else {
							$returnResultData[$subject_row_num]['alpha_gpa'] = 'F';
							$returnResultData[$subject_row_num]['grade_point'] = '0';
							$total_grade_point += 0;
						}

						if ($returnResultData[$subject_row_num]['is_optional'] == 1) {
							$optional_subject_point = $returnResultData[$subject_row_num]['grade_point'];
						}


						if($subject_row['subject_type'] != 'UNC'){
							$num_of_cal_sub += 1;
							if ($returnResultData[$subject_row_num]['alpha_gpa'] == 'F' &&  $this_optional_subject != 1) {
								$number_of_failed_subject += 1;
								$IsPassed = 0;
							}

							$total_number_achieved = $total_number_achieved + $calculable_total_obtain_mark;
						}

					}
					$subject_row_num++;
				}
			}




			//echo $total_number_achieved; die;

//				echo '<pre>';
//				print_r($returnResultData);
//				die;

			$returnResultDataArray['student_id'] = $StudentInfo['id'];
			$returnResultDataArray['student_name'] = $StudentInfo['name'];
			$returnResultDataArray['roll_no'] = $StudentInfo['roll_no'];
			$returnResultDataArray['reg_no'] = $StudentInfo['reg_no'];
			$returnResultDataArray['father_name'] = $StudentInfo['father_name'];
			$returnResultDataArray['mother_name'] = $StudentInfo['mother_name'];
			$returnResultDataArray['date_of_birth'] = $StudentInfo['date_of_birth'];
			$returnResultDataArray['class_name'] = $StudentInfo['class_name'];
			$returnResultDataArray['section_name'] = $StudentInfo['section_name'];
			$returnResultDataArray['group_name'] = $StudentInfo['group_name'];
			$returnResultDataArray['shift_name'] = $StudentInfo['shift_name'];
			$returnResultDataArray['subject_wise_data'] = $returnResultData;
			$returnResultDataArray['total_number_achieved'] = $total_number_achieved;
			$returnResultDataArray['number_of_failed_subject'] = $number_of_failed_subject;
			$returnResultDataArray['exam_name'] = $exam_info[0]['name'];
			$returnResultDataArray['total_credit'] = $total_credit;

			//total grade point after merge
			$total_grade_point = ($total_grade_point - ($total_merge_subject_grade_point / 2));


			//grade point calculation for optional
			$optional_added_mark = 0;
			if($is_optional_found == 1){
				if($optional_subject_point >= 2){
					$optional_added_mark =  $optional_subject_point - 2;
					$total_grade_point = $total_grade_point - 2;
				}else{
					$optional_added_mark =  0;
					$total_grade_point = $total_grade_point - $optional_subject_point;
				}
				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			$returnResultDataArray['calculable_total_grade_point'] = $total_grade_point;

			//merge subject sub
			$num_of_cal_sub =  $num_of_cal_sub - ($total_merge_subject / 2);
			$returnResultDataArray['calculable_subject'] = $num_of_cal_sub;

			if ($IsPassed > 0 || $is_calculate_cgpa_when_fail == 1) {
				if ($num_of_cal_sub == 0) {
					$returnResultDataArray['CGPA'] = '0.00';
					$returnResultDataArray['CGPAwithoutFourthSubject'] = '0.00';

				} else {
					$returnResultDataArray['CGPA'] = round($returnResultDataArray['calculable_total_grade_point'] / $returnResultDataArray['calculable_subject'], 2);
					if ($returnResultDataArray['CGPA'] > 5) {
						$returnResultDataArray['CGPA'] = 5.00;
					}
					$returnResultDataArray['CGPAwithoutFourthSubject'] = round(($returnResultDataArray['calculable_total_grade_point'] - $optional_added_mark) / $returnResultDataArray['calculable_subject'], 2);
					if ($returnResultDataArray['CGPAwithoutFourthSubject'] > 5) {
						$returnResultDataArray['CGPAwithoutFourthSubject'] = 5.00;
					}
				}
			} else {
				$returnResultDataArray['CGPA'] = '0.00';
				$returnResultDataArray['CGPAwithoutFourthSubject'] = '0.00';
			}

			return $returnResultDataArray;
		}
	}


	public function result_by_student($StudentID, $ClassID, $ExamID)
	{

		//exam details
		$exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$ExamID'")->result_array();
		if ($exam_info[0]['exam_type_id'] <= 0) {
			$sdata['exception'] = "Please input first what type of exam it is.";
			$this->session->set_userdata($sdata);
			redirect('students/exam_list');
		}
		$exam_type_id = $exam_info[0]['exam_type_id'];
		$year = $exam_info[0]['year'];

		$is_annual_exam = 0;
		if (isset($exam_info[0]['is_annual_exam'])) {
			$is_annual_exam = $exam_info[0]['is_annual_exam'];
		}


		$is_pass_to_different_segment_of_result = 0;
		$is_process_bangla_english_separate_calculation = 0;
		$is_calculate_cgpa_when_fail = 0;
		$num_of_subject_allow_for_grace = 0;
		$num_of_subject_allow_for_grace_count = 0;
		$calculable_total_mark_percentage = 100;
		$annual_mark_calculate_method = "";
		$config = $this->db->query("SELECT annual_mark_calculate_method,calculable_total_mark_percentage,is_pass_to_different_segment_of_result,is_calculate_cgpa_when_fail,is_process_bangla_english_separate,num_of_subject_allow_for_grace FROM `tbl_config`")->result_array();
		if (!empty($config)) {
			$is_pass_to_different_segment_of_result = $config[0]['is_pass_to_different_segment_of_result'];
			$is_calculate_cgpa_when_fail = $config[0]['is_calculate_cgpa_when_fail'];
			$is_process_bangla_english_separate_calculation = $config[0]['is_process_bangla_english_separate'];
			$num_of_subject_allow_for_grace = $config[0]['num_of_subject_allow_for_grace'];
			$calculable_total_mark_percentage = $config[0]['calculable_total_mark_percentage'];
			$annual_mark_calculate_method = $config[0]['annual_mark_calculate_method'];
		}

		//echo $is_process_bangla_english_separate_calculation; die;

		$subject_order = $this->getClassWiseSubjectOrder($ClassID, $exam_type_id);
		//echo '<pre>';
		//print_r($subject_order);
		// die;


		$IsPassed = 1;
		$number_of_failed_subject = 0;
		$IsEligibleToAddFourthSubject = 0;
		$BengaliMarks = 0;
		$BengaliCredit = 0;
		$EnglishMarks = 0;
		$EnglishCredit = 0;
		$TotalNumberAchived = 0;
		$TCGPA = 0;
		$TCGPAForWithoutOptional = 0;
		$ReturnValue = array();
		$data = $this->db->query("SELECT
  s.id AS StudentID,
  s.`name` AS StudentName,
  s.`roll_no` AS StudentRole,
  s.`reg_no` AS StudentReg,
  s.`father_name`,
  s.`mother_name`,
  s.`date_of_birth`,
  su.`id` AS SubjectId,
  su.`name` AS SubjectName,
  su.`code` AS SubjectCode,
  su.`relational_subject_id`,
  c.`name` AS ClassName,
  sec.`name` AS SectionName,
  m.`written`,
  m.`objective`,
  m.`practical`,
  m.`class_test`,
  m.`allocated_written`,
  m.`allocated_objective`,
  m.`allocated_practical`,
  m.`allocated_class_test`,
  m.`credit`,
  m.`calculable_amount`,
  e.`name` AS ExamName, a.`is_optional`,a.`year` as subject_year
  FROM
  `tbl_student_wise_subject` AS a
  INNER JOIN `tbl_student` AS s
    ON s.`id` = a.`student_id` AND a.`year` = '$year'
  INNER JOIN `tbl_subject` AS su
    ON su.`id` = a.`subject_id`
  INNER JOIN `tbl_class` AS c
    ON c.`id` = a.`class_id`
  INNER JOIN `tbl_marking` AS m
    ON (
      m.`student_id` = a.`student_id`
      AND m.`class_id` = a.`class_id`
      AND m.`subject_id` = a.`subject_id`
      AND m.`exam_id` = '$ExamID'
    )
  INNER JOIN `tbl_section` AS sec
    ON sec.`id` = s.`section_id`
    INNER JOIN `tbl_exam` AS e ON e.`id` = '$ExamID'
WHERE a.`student_id` = '$StudentID'
  AND a.`class_id` = '$ClassID' AND s.`status` = '1' ORDER BY su.`code`")->result_array();

		// echo '<pre>';
		// print_r($data); die;

		$num_of_cal_sub = count($data);

		//for wisdomcollege
		if (SCHOOL_NAME == 'wisdomcollege') {

			//start  physics 174 and 175
			$PhysicsMarks = 0;
			$PhysicsCredit = 0;

			$is_physics_found = 0;
			$physics_grace_due = 0;
			$physics_objective_grace_due = 0;
			$physics_grace_subject_count = 0;

			$is_physics_fail = 0;

			//global var for segment wise pass fail
			$total_got_written = 0;
			$total_got_objective = 0;
			$total_allocated_written = 0;
			$total_allocated_objective = 0;
			//end global var for segment wise pass fail


			//marge subject calculation

			// $is_bangla_found = 0;
			// $bangla_grace_due = 0;
			// $bangla_objective_grace_due = 0;
			// $bangla_grace_subject_count = 0;
			//
			// $is_Bangla_fail = 0;
			//
			// //global var for segment wise pass fail
			// $total_got_written = 0;
			// $total_got_objective = 0;
			// $total_allocated_written = 0;
			// $total_allocated_objective = 0;
			// //end global var for segment wise pass fail
			//
			// if ($is_process_bangla_english_separate_calculation == 1) {
			//     for ($A = 0; $A < count($data); $A++) {
			//         if ($data[$A]['SubjectCode'] == '101' || $data[$A]['SubjectCode'] == '102') {
			//             $subject_id = $data[$A]['SubjectId'];
			//
			//             $class_test_grace = 0;
			//             $written_grace = 0;
			//             $objective_grace = 0;
			//             $practical_grace = 0;
			//             $grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
			//             if (!empty($grace_mark)) {
			//                 $class_test_grace =  $grace_mark[0]['class_test_grace'];
			//                 $written_grace = $grace_mark[0]['written_grace'];
			//                 $objective_grace = $grace_mark[0]['objective_grace'];
			//                 $practical_grace = $grace_mark[0]['practical_grace'];
			//                 //echo '<pre>';
			//                //print_r($grace_mark);
			//                //die;
			//             }
			//
			//
			//
			//             /*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
			//                 $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
			//                 if($written_alfa_gpa == 'F'){
			//                     if($bangla_grace_due > 0){
			//                         $written_grace = $bangla_grace_due;
			//                     }
			//                     if($written_grace > 0){
			//                         $pass_mark = round($data[$A]['allocated_written'] / 3);
			//                         //echo $pass_mark; die;
			//                         $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
			//                         if($need_grace_mark_for_pass <= $written_grace){
			//                             if($bangla_grace_subject_count == 0){
			//                                $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
			//                             }
			//                             $bangla_grace_subject_count = $bangla_grace_subject_count + 1;
			//                             $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
			//                             $bangla_grace_due = $written_grace - $need_grace_mark_for_pass;
			//                            // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
			//                             //echo $need_grace_mark_for_pass; die;
			//                         }
			//                     }
			//                 }
			//             }
			//
			//
			//             if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
			//                 $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
			//                 if($objective_alfa_gpa == 'F'){
			//                     if($bangla_objective_grace_due > 0){
			//                         $objective_grace = $bangla_objective_grace_due;
			//                     }
			//                     if($objective_grace > 0){
			//                         $pass_mark = round($data[$A]['allocated_objective'] / 3);
			//                         //echo $pass_mark; die;
			//                         $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
			//                         if($need_grace_mark_for_pass <= $objective_grace){
			//                             if($bangla_grace_subject_count == 0){
			//                                $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
			//                             }
			//                             $bangla_grace_subject_count = $bangla_grace_subject_count + 1;
			//                             $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
			//                             $bangla_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
			//                             //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
			//                             //echo $need_grace_mark_for_pass; die;
			//                         }
			//                     }
			//                 }
			//             }*/
			//
			//             //locally value assign for segment wise pass fail
			//             $total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
			//             $total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];
			//
			//             $total_got_written = $total_got_written + $data[$A]['written'];
			//             $total_got_objective = $total_got_objective + $data[$A]['objective'];
			//             //end locally value assign for segment wise pass fail
			//
			//
			//             //Extra head calculation
			//             $total_extra_mark = 0;
			//             $extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
			//             if (!empty($extra_marks)) {
			//                 foreach ($extra_marks as $datacol_row):
			//                     $total_extra_mark = $total_extra_mark + $datacol_row['mark'];
			//                 endforeach;
			//             }
			//             $ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;
			//
			//             //Extra head calculation
			//             $calculable_total_obtain = (($data[$A]['class_test'] + $data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
			//             //echo $calculable_total_obtain;
			//             //die;
			//             $BengaliMarks = $BengaliMarks + ($calculable_total_obtain + $total_extra_mark);
			//
			//             $BengaliCredit = $BengaliCredit + $data[$A]['credit'];
			//             $is_bangla_found++;
			//         }
			//     }
			//
			//     //echo $BengaliMarks; die;
			// }
			//
			//
			// if ($is_bangla_found == 2) {
			//
			//     //decesion make for segment wise pass fail
			//     if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
			//         $written_alfa_gpa =  $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
			//         if ($written_alfa_gpa == 'F') {
			//             $is_Bangla_fail = 1;
			//         }
			//     }
			//
			//     if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
			//         $objective_alfa_gpa =  $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
			//         if ($objective_alfa_gpa == 'F') {
			//             $is_Bangla_fail = 1;
			//         }
			//     }
			//     //end decesion make for segment wise pass fail
			//
			//     $num_of_cal_sub = $num_of_cal_sub - 1;
			// }
			// echo $num_of_subject_allow_for_grace_count;
			//die;

			//marge subject calculation end


			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '174' || $data[$A]['SubjectCode'] == '175') {
					//  echo $data[$A]['SubjectId']; die;
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            $is_physics_fail = 1;
                            if($physics_grace_due > 0){
                                $written_grace = $physics_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($physics_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $physics_grace_subject_count = $physics_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $physics_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            $is_physics_fail = 1;
                            if($physics_objective_grace_due > 0){
                                $objective_grace = $physics_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($physics_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $physics_grace_subject_count = $physics_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $physics_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }*/


					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail

					$calculable_total_obtain = (($data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$PhysicsMarks = $PhysicsMarks + ($calculable_total_obtain + $total_extra_mark);

					$PhysicsCredit = $PhysicsCredit + $data[$A]['credit'];
					$is_physics_found++;
				}
			}

			// echo $PhysicsMarks; die;


			if ($is_physics_found == 2) {

				//decesion make for segment wise pass fail
				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
					if ($written_alfa_gpa == 'F') {
						$is_physics_fail = 1;
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
					if ($objective_alfa_gpa == 'F') {
						$is_physics_fail = 1;
					}
				}
				//end decesion make for segment wise pass fail


				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			//end physics 174 and 175


			//start  chemistry 176 and 177
			$chemistryMarks = 0;
			$chemistryCredit = 0;

			$is_chemistry_found = 0;
			$chemistry_grace_due = 0;
			$chemistry_objective_grace_due = 0;
			$chemistry_grace_subject_count = 0;

			$is_chemistry_fail = 0;


			//global var for segment wise pass fail
			$total_got_written = 0;
			$total_got_objective = 0;
			$total_allocated_written = 0;
			$total_allocated_objective = 0;
			//end global var for segment wise pass fail

			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '176' || $data[$A]['SubjectCode'] == '177') {
					//  echo $data[$A]['SubjectId']; die;
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            $is_chemistry_fail = 1;
                            if($chemistry_grace_due > 0){
                                $written_grace = $chemistry_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($chemistry_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $chemistry_grace_subject_count = $chemistry_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $chemistry_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            $is_chemistry_fail = 1;
                            if($chemistry_objective_grace_due > 0){
                                $objective_grace = $chemistry_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($chemistry_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $chemistry_grace_subject_count = $chemistry_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $chemistry_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }
                    */

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail

					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$chemistryMarks = $chemistryMarks + ($calculable_total_obtain + $total_extra_mark);

					$chemistryCredit = $chemistryCredit + $data[$A]['credit'];
					$is_chemistry_found++;
				}
			}

			// echo $chemistryMarks; die;


			if ($is_chemistry_found == 2) {

				//decesion make for segment wise pass fail
				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
					if ($written_alfa_gpa == 'F') {
						$is_chemistry_fail = 1;
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
					if ($objective_alfa_gpa == 'F') {
						$is_chemistry_fail = 1;
					}
				}
				//end decesion make for segment wise pass fail

				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			//end chemistry 174 and 175


			//start  Biology  178 and 179
			$BiologyMarks = 0;
			$BiologyCredit = 0;

			$is_Biology_found = 0;
			$Biology_grace_due = 0;
			$Biology_objective_grace_due = 0;
			$Biology_grace_subject_count = 0;

			$is_Biology_fail = 0;

			//global var for segment wise pass fail
			$total_got_written = 0;
			$total_got_objective = 0;
			$total_allocated_written = 0;
			$total_allocated_objective = 0;
			//end global var for segment wise pass fail

			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '178' || $data[$A]['SubjectCode'] == '179') {
					//  echo $data[$A]['SubjectId']; die;
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            $is_Biology_fail = 1;
                            if($Biology_grace_due > 0){
                                $written_grace = $Biology_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($Biology_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Biology_grace_subject_count = $Biology_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $Biology_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            $is_Biology_fail = 1;
                            if($Biology_objective_grace_due > 0){
                                $objective_grace = $Biology_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($Biology_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Biology_grace_subject_count = $Biology_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $Biology_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }*/

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail


					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$BiologyMarks = $BiologyMarks + ($calculable_total_obtain + $total_extra_mark);

					$BiologyCredit = $BiologyCredit + $data[$A]['credit'];
					$is_Biology_found++;
				}
			}

			// echo $BiologyMarks; die;


			if ($is_Biology_found == 2) {

				//decesion make for segment wise pass fail
				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
					if ($written_alfa_gpa == 'F') {
						$is_Biology_fail = 1;
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
					if ($objective_alfa_gpa == 'F') {
						$is_Biology_fail = 1;
					}
				}
				//end decesion make for segment wise pass fail

				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			//end Biology 178 and 179


			//start  Highermath  265 and 266
			$HighermathMarks = 0;
			$HighermathCredit = 0;

			$is_Highermath_found = 0;
			$Highermath_grace_due = 0;
			$Highermath_objective_grace_due = 0;
			$Highermath_grace_subject_count = 0;

			$is_Highermath_fail = 0;

			//global var for segment wise pass fail
			$total_got_written = 0;
			$total_got_objective = 0;
			$total_allocated_written = 0;
			$total_allocated_objective = 0;
			//end global var for segment wise pass fail

			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '265' || $data[$A]['SubjectCode'] == '266') {
					//  echo $data[$A]['SubjectId']; die;
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            $is_Highermath_fail = 1;
                            if($Highermath_grace_due > 0){
                                $written_grace = $Highermath_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($Highermath_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Highermath_grace_subject_count = $Highermath_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $Highermath_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            $is_Highermath_fail = 1;
                            if($Highermath_objective_grace_due > 0){
                                $objective_grace = $Highermath_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($Highermath_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Highermath_grace_subject_count = $Highermath_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $Highermath_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }*/

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail


					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$HighermathMarks = $HighermathMarks + ($calculable_total_obtain + $total_extra_mark);

					$HighermathCredit = $HighermathCredit + $data[$A]['credit'];
					$is_Highermath_found++;
				}
			}

			// echo $HighermathMarks; die;


			if ($is_Highermath_found == 2) {

				//decesion make for segment wise pass fail
				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
					if ($written_alfa_gpa == 'F') {
						$is_Highermath_fail = 1;
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
					if ($objective_alfa_gpa == 'F') {
						$is_Highermath_fail = 1;
					}
				}
				//end decesion make for segment wise pass fail

				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			//end  Highermath  265 and 266

			//start  Agriculture  239 and 240
			$AgricultureMarks = 0;
			$AgricultureCredit = 0;

			$is_Agriculture_found = 0;
			$Agriculture_grace_due = 0;
			$Agriculture_objective_grace_due = 0;
			$Agriculture_grace_subject_count = 0;

			$is_Agriculture_fail = 0;

			//global var for segment wise pass fail
			$total_got_written = 0;
			$total_got_objective = 0;
			$total_allocated_written = 0;
			$total_allocated_objective = 0;
			//end global var for segment wise pass fail

			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '239' || $data[$A]['SubjectCode'] == '240') {
					//  echo $data[$A]['SubjectId']; die;
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            $is_Agriculture_fail = 1;
                            if($Agriculture_grace_due > 0){
                                $written_grace = $Agriculture_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($Agriculture_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Agriculture_grace_subject_count = $Agriculture_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $Agriculture_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            $is_Agriculture_fail = 1;
                            if($Agriculture_objective_grace_due > 0){
                                $objective_grace = $Agriculture_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($Agriculture_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Agriculture_grace_subject_count = $Agriculture_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $Agriculture_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }*/

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail


					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$AgricultureMarks = $AgricultureMarks + ($calculable_total_obtain + $total_extra_mark);

					$AgricultureCredit = $AgricultureCredit + $data[$A]['credit'];
					$is_Agriculture_found++;
				}
			}

			// echo $AgricultureMarks; die;


			if ($is_Agriculture_found == 2) {
				//decesion make for segment wise pass fail
				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
					if ($written_alfa_gpa == 'F') {
						$is_Agriculture_fail = 1;
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
					if ($objective_alfa_gpa == 'F') {
						$is_Agriculture_fail = 1;
					}
				}
				//end decesion make for segment wise pass fail
				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			//end  Agriculture  239 and 240


			//start  HistoryCulture  267 and 268
			$HistoryCultureMarks = 0;
			$HistoryCultureCredit = 0;

			$is_HistoryCulture_found = 0;
			$HistoryCulture_grace_due = 0;
			$HistoryCulture_objective_grace_due = 0;
			$HistoryCulture_grace_subject_count = 0;

			$is_HistoryCulture_fail = 0;

			//global var for segment wise pass fail
			$total_got_written = 0;
			$total_got_objective = 0;
			$total_allocated_written = 0;
			$total_allocated_objective = 0;
			//end global var for segment wise pass fail

			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '267' || $data[$A]['SubjectCode'] == '268') {
					//  echo $data[$A]['SubjectId']; die;
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            $is_HistoryCulture_fail = 1;
                            if($HistoryCulture_grace_due > 0){
                                $written_grace = $HistoryCulture_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($HistoryCulture_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $HistoryCulture_grace_subject_count = $HistoryCulture_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $HistoryCulture_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            $is_HistoryCulture_fail = 1;
                            if($HistoryCulture_objective_grace_due > 0){
                                $objective_grace = $HistoryCulture_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($HistoryCulture_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $HistoryCulture_grace_subject_count = $HistoryCulture_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $HistoryCulture_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }*/

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail


					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$HistoryCultureMarks = $HistoryCultureMarks + ($calculable_total_obtain + $total_extra_mark);

					$HistoryCultureCredit = $HistoryCultureCredit + $data[$A]['credit'];
					$is_HistoryCulture_found++;
				}
			}

			// echo $HistoryCultureMarks; die;


			if ($is_HistoryCulture_found == 2) {
				//decesion make for segment wise pass fail
				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
					if ($written_alfa_gpa == 'F') {
						$is_HistoryCulture_fail = 1;
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
					if ($objective_alfa_gpa == 'F') {
						$is_HistoryCulture_fail = 1;
					}
				}
				//end decesion make for segment wise pass fail
				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			//end  HistoryCulture   267 and 268


			//start Economy 109 and 110
			$EconomyMarks = 0;
			$EconomyCredit = 0;

			$is_Economy_found = 0;
			$Economy_grace_due = 0;
			$Economy_objective_grace_due = 0;
			$Economy_grace_subject_count = 0;

			$is_Economy_fail = 0;

			//global var for segment wise pass fail
			$total_got_written = 0;
			$total_got_objective = 0;
			$total_allocated_written = 0;
			$total_allocated_objective = 0;
			//end global var for segment wise pass fail

			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '109' || $data[$A]['SubjectCode'] == '110') {
					//  echo $data[$A]['SubjectId']; die;
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            $is_Economy_fail = 1;
                            if($Economy_grace_due > 0){
                                $written_grace = $Economy_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($Economy_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Economy_grace_subject_count = $Economy_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $Economy_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            $is_Economy_fail = 1;
                            if($Economy_objective_grace_due > 0){
                                $objective_grace = $Economy_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($Economy_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Economy_grace_subject_count = $Economy_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $Economy_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }*/

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//if($StudentID == 227){
					//  echo $data[$A]['allocated_written'].'/'.$data[$A]['written'].'<br>';
					// echo $data[$A]['allocated_objective'].'/'.$data[$A]['objective'].'<br>';
					//}
					//end locally value assign for segment wise pass fail


					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$EconomyMarks = $EconomyMarks + ($calculable_total_obtain + $total_extra_mark);

					$EconomyCredit = $EconomyCredit + $data[$A]['credit'];
					$is_Economy_found++;
				}
			}

			// echo $EconomyMarks; die;

			// if($StudentID == 227){
			//     die;
			//  }


			if ($is_Economy_found == 2) {
				//decesion make for segment wise pass fail
				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
					/*if($StudentID == 227){
                           echo $total_allocated_written.'/'.$total_got_written.'/'.$written_alfa_gpa;
                           die;
                     }*/
					if ($written_alfa_gpa == 'F') {
						$is_Economy_fail = 1;
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
					if ($objective_alfa_gpa == 'F') {
						$is_Economy_fail = 1;
					}
				}
				//end decesion make for segment wise pass fail
				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			//end Economy 109 and 110


			//start StudyOfIslam  249 and 250
			$StudyOfIslamMarks = 0;
			$StudyOfIslamCredit = 0;

			$is_StudyOfIslam_found = 0;
			$StudyOfIslam_grace_due = 0;
			$StudyOfIslam_objective_grace_due = 0;
			$StudyOfIslam_grace_subject_count = 0;

			$is_StudyOfIslam_fail = 0;

			//global var for segment wise pass fail
			$total_got_written = 0;
			$total_got_objective = 0;
			$total_allocated_written = 0;
			$total_allocated_objective = 0;
			//end global var for segment wise pass fail

			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '249' || $data[$A]['SubjectCode'] == '250') {
					//  echo $data[$A]['SubjectId']; die;
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            $is_StudyOfIslam_fail = 1;
                            if($StudyOfIslam_grace_due > 0){
                                $written_grace = $StudyOfIslam_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($StudyOfIslam_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $StudyOfIslam_grace_subject_count = $StudyOfIslam_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $StudyOfIslam_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            $is_StudyOfIslam_fail = 1;
                            if($StudyOfIslam_objective_grace_due > 0){
                                $objective_grace = $StudyOfIslam_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($StudyOfIslam_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $StudyOfIslam_grace_subject_count = $StudyOfIslam_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $StudyOfIslam_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }*/

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail


					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$StudyOfIslamMarks = $StudyOfIslamMarks + ($calculable_total_obtain + $total_extra_mark);

					$StudyOfIslamCredit = $StudyOfIslamCredit + $data[$A]['credit'];
					$is_StudyOfIslam_found++;
				}
			}

			// echo $StudyOfIslamMarks; die;


			if ($is_StudyOfIslam_found == 2) {

				//decesion make for segment wise pass fail
				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
					if ($written_alfa_gpa == 'F') {
						$is_StudyOfIslam_fail = 1;
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
					if ($objective_alfa_gpa == 'F') {
						$is_StudyOfIslam_fail = 1;
					}
				}
				//end decesion make for segment wise pass fail

				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			//end StudyOfIslam 249 and 250

			//start Civics  269 and 270
			$CivicsMarks = 0;
			$CivicsCredit = 0;

			$is_Civics_found = 0;
			$Civics_grace_due = 0;
			$Civics_objective_grace_due = 0;
			$Civics_grace_subject_count = 0;

			$is_Civics_fail = 0;

			//global var for segment wise pass fail
			$total_got_written = 0;
			$total_got_objective = 0;
			$total_allocated_written = 0;
			$total_allocated_objective = 0;
			//end global var for segment wise pass fail

			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '269' || $data[$A]['SubjectCode'] == '270') {
					//  echo $data[$A]['SubjectId']; die;
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            $is_Civics_fail = 1;
                            if($Civics_grace_due > 0){
                                $written_grace = $Civics_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($Civics_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Civics_grace_subject_count = $Civics_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $Civics_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            $is_Civics_fail = 1;
                            if($Civics_objective_grace_due > 0){
                                $objective_grace = $Civics_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($Civics_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $Civics_grace_subject_count = $Civics_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $Civics_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }*/

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail


					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$CivicsMarks = $CivicsMarks + ($calculable_total_obtain + $total_extra_mark);

					$CivicsCredit = $CivicsCredit + $data[$A]['credit'];
					$is_Civics_found++;
				}
			}

			// echo $CivicsMarks; die;


			if ($is_Civics_found == 2) {

				//decesion make for segment wise pass fail
				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
					if ($written_alfa_gpa == 'F') {
						$is_Civics_fail = 1;
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
					if ($objective_alfa_gpa == 'F') {
						$is_Civics_fail = 1;
					}
				}
				//end decesion make for segment wise pass fail

				$num_of_cal_sub = $num_of_cal_sub - 1;
			}

			//end Civics 269 and 270
		}


		//end for wisdom college


		$is_bangla_found = 0;
		$bangla_grace_due = 0;
		$bangla_objective_grace_due = 0;
		$bangla_grace_subject_count = 0;

		$is_Bangla_fail = 0;

		//global var for segment wise pass fail
		$total_got_written = 0;
		$total_got_objective = 0;
		$total_allocated_written = 0;
		$total_allocated_objective = 0;
		//end global var for segment wise pass fail

		if ($is_process_bangla_english_separate_calculation == 1) {
			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '101' || $data[$A]['SubjectCode'] == '102') {
					$subject_id = $data[$A]['SubjectId'];

					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}


					/*if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                        $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                        if($written_alfa_gpa == 'F'){
                            if($bangla_grace_due > 0){
                                $written_grace = $bangla_grace_due;
                            }
                            if($written_grace > 0){
                                $pass_mark = round($data[$A]['allocated_written'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                if($need_grace_mark_for_pass <= $written_grace){
                                    if($bangla_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $bangla_grace_subject_count = $bangla_grace_subject_count + 1;
                                    $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                    $bangla_grace_due = $written_grace - $need_grace_mark_for_pass;
                                   // echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }


                    if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_objective'] > 0){
                        $objective_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['objective'], $data[$A]['allocated_objective']);
                        if($objective_alfa_gpa == 'F'){
                            if($bangla_objective_grace_due > 0){
                                $objective_grace = $bangla_objective_grace_due;
                            }
                            if($objective_grace > 0){
                                $pass_mark = round($data[$A]['allocated_objective'] / 3);
                                //echo $pass_mark; die;
                                $need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];
                                if($need_grace_mark_for_pass <= $objective_grace){
                                    if($bangla_grace_subject_count == 0){
                                       $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                    }
                                    $bangla_grace_subject_count = $bangla_grace_subject_count + 1;
                                    $data[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass),2);
                                    $bangla_objective_grace_due = $objective_grace - $need_grace_mark_for_pass;
                                    //echo $data[$A]['SubjectCode'] . '/' .$num_of_subject_allow_for_grace . '/' .$num_of_subject_allow_for_grace_count.'<br>';
                                    //echo $need_grace_mark_for_pass; die;
                                }
                            }
                        }
                    }*/

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail


					//Extra head calculation
					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['class_test'] + $data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					//echo $calculable_total_obtain;
					//die;
					$BengaliMarks = $BengaliMarks + ($calculable_total_obtain + $total_extra_mark);

					$BengaliCredit = $BengaliCredit + $data[$A]['credit'];
					$is_bangla_found++;
				}
			}

			//echo $BengaliMarks; die;
		}


		if ($is_bangla_found == 2) {

			//decesion make for segment wise pass fail
			if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
				$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
				if ($written_alfa_gpa == 'F') {
					$is_Bangla_fail = 1;
				}
			}

			if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
				$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
				if ($objective_alfa_gpa == 'F') {
					$is_Bangla_fail = 1;
				}
			}
			//end decesion make for segment wise pass fail

			$num_of_cal_sub = $num_of_cal_sub - 1;
		}
		// echo $num_of_subject_allow_for_grace_count;
		//die;


		$is_english_found = 0;
		$english_grace_due = 0;
		$english_grace_subject_count = 0;

		$is_English_fail = 0;

		//global var for segment wise pass fail
		$total_got_written = 0;
		$total_got_objective = 0;
		$total_allocated_written = 0;
		$total_allocated_objective = 0;
		//end global var for segment wise pass fail

		if ($is_process_bangla_english_separate_calculation == 1) {
			for ($A = 0; $A < count($data); $A++) {
				if ($data[$A]['SubjectCode'] == '107' || $data[$A]['SubjectCode'] == '108') {
					$subject_id = $data[$A]['SubjectId'];
					$class_test_grace = 0;
					$written_grace = 0;
					$objective_grace = 0;
					$practical_grace = 0;
					$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
					if (!empty($grace_mark)) {
						$class_test_grace = $grace_mark[0]['class_test_grace'];
						$written_grace = $grace_mark[0]['written_grace'];
						$objective_grace = $grace_mark[0]['objective_grace'];
						$practical_grace = $grace_mark[0]['practical_grace'];
						//echo '<pre>';
						//print_r($grace_mark);
						//die;
					}

					/* if($is_pass_to_different_segment_of_result == '1' && $data[$A]['allocated_written'] > 0){
                         $written_alfa_gpa =  $this->number_to_alfa_grade($data[$A]['written'], $data[$A]['allocated_written']);
                         //echo $written_alfa_gpa.'/'.$data[$A]['SubjectCode'].'/'.$data[$A]['written'].'/'.$data[$A]['allocated_written'].'<br>';
                         if($written_alfa_gpa == 'F'){

                             if($english_grace_due > 0){
                                 $written_grace = $english_grace_due;
                             }
                             if($written_grace > 0){
                                 $pass_mark = round($data[$A]['allocated_written'] / 3);
                                 //echo $pass_mark; die;
                                 $need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];
                                 if($need_grace_mark_for_pass <= $written_grace){
                                     if($english_grace_subject_count == 0){
                                        $num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
                                     }
                                     $english_grace_subject_count = $english_grace_subject_count + 1;
                                     $data[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass),2);
                                     $english_grace_due = $written_grace - $need_grace_mark_for_pass;
                                 }
                             }
                         }
                     }*/

					//locally value assign for segment wise pass fail
					$total_allocated_written = $total_allocated_written + $data[$A]['allocated_written'];
					$total_allocated_objective = $total_allocated_objective + $data[$A]['allocated_objective'];

					$total_got_written = $total_got_written + $data[$A]['written'];
					$total_got_objective = $total_got_objective + $data[$A]['objective'];
					//end locally value assign for segment wise pass fail


					//Extra head calculation

					$total_extra_mark = 0;
					$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
					if (!empty($extra_marks)) {
						foreach ($extra_marks as $datacol_row):
							$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
						endforeach;
					}
					$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;
					//echo $data[$A]['calculable_amount']; die;
					//Extra head calculation
					$calculable_total_obtain = (($data[$A]['class_test'] + $data[$A]['written'] + $data[$A]['objective'] + $data[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
					$EnglishMarks = $EnglishMarks + ($calculable_total_obtain + $total_extra_mark);

					$EnglishCredit = $EnglishCredit + $data[$A]['credit'];
					$is_english_found++;
				}
			}
		}
		//  echo  $num_of_subject_allow_for_grace_count;
		// die;
		if ($is_english_found == 2) {

			//decesion make for segment wise pass fail
			if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_written > 0) {
				$written_alfa_gpa = $this->number_to_alfa_grade($total_got_written, $total_allocated_written);
				if ($written_alfa_gpa == 'F') {
					$is_English_fail = 1;
				}
			}

			if ($is_pass_to_different_segment_of_result == '1' && $total_allocated_objective > 0) {
				$objective_alfa_gpa = $this->number_to_alfa_grade($total_got_objective, $total_allocated_objective);
				if ($objective_alfa_gpa == 'F') {
					$is_English_fail = 1;
				}
			}
			//end decesion make for segment wise pass fail

			$num_of_cal_sub = $num_of_cal_sub - 1;
		}
		//echo count($data); die;
		$dttt = array();
		for ($A = 0; $A < count($data); $A++) {
			// echo $is_process_bangla_english_separate_calculation; die;
			if ($is_process_bangla_english_separate_calculation == 1 && ($data[$A]['SubjectCode'] == '101' || $data[$A]['SubjectCode'] == '102')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = $data[$A]['class_test'];
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];


				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['class_test'] + $ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				// echo $calculable_total_obtain; die;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				//echo $calculable_total_obtain . '/';
				//die;
				$BanglaMarksAfterCalculate = ($BengaliMarks / $BengaliCredit) * 100;
				if ($data[$A]['SubjectCode'] == '101') {

					//echo $BanglaPoint; die;
					$TGPforThisSubject = $this->number_to_num_grade($BanglaMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($BanglaMarksAfterCalculate, 100);
				}


				if ($is_Bangla_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($BanglaMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($BanglaMarksAfterCalculate, 100);
				}

				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}
			} elseif ($is_process_bangla_english_separate_calculation == 1 && ($data[$A]['SubjectCode'] == '107' || $data[$A]['SubjectCode'] == '108')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = $data[$A]['class_test'];
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['class_test'] + $ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				$EnglishMarksAfterCalculate = ($EnglishMarks / $EnglishCredit) * 100;
				if ($data[$A]['SubjectCode'] == '107') {
					$TGPforThisSubject = $this->number_to_num_grade($EnglishMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($EnglishMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($EnglishMarks, $EnglishCredit); die;
				}


				if ($is_English_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($EnglishMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($EnglishMarksAfterCalculate, 100);
				}


				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}
				//echo '<pre>';
				//print_r($ReturnValue);
			} elseif (SCHOOL_NAME == 'wisdomcollege' && ($data[$A]['SubjectCode'] == '174' || $data[$A]['SubjectCode'] == '175')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = $data[$A]['class_test'];
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				$PhysicsMarksAfterCalculate = ($PhysicsMarks / $PhysicsCredit) * 100;
				if ($data[$A]['SubjectCode'] == '174') {
					$TGPforThisSubject = $this->number_to_num_grade($PhysicsMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($PhysicsMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($PhysicsMarks, $PhysicsCredit); die;
				}

				if ($is_physics_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($PhysicsMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($PhysicsMarksAfterCalculate, 100);
				}

				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				//echo '<pre>';
				//print_r($ReturnValue);
			} elseif (SCHOOL_NAME == 'wisdomcollege' && ($data[$A]['SubjectCode'] == '176' || $data[$A]['SubjectCode'] == '177')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = 0;
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				$chemistryMarksAfterCalculate = ($chemistryMarks / $chemistryCredit) * 100;
				if ($data[$A]['SubjectCode'] == '176') {
					$TGPforThisSubject = $this->number_to_num_grade($chemistryMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($chemistryMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($chemistryMarks, $chemistryCredit); die;
				}

				if ($is_chemistry_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($chemistryMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($chemistryMarksAfterCalculate, 100);
				}


				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				//echo '<pre>';
				//print_r($ReturnValue);
			} elseif (SCHOOL_NAME == 'wisdomcollege' && ($data[$A]['SubjectCode'] == '178' || $data[$A]['SubjectCode'] == '179')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = 0;
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				$BiologyMarksAfterCalculate = ($BiologyMarks / $BiologyCredit) * 100;
				if ($data[$A]['SubjectCode'] == '178') {
					$TGPforThisSubject = $this->number_to_num_grade($BiologyMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($BiologyMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($BiologyMarks, $BiologyCredit); die;
				}

				if ($is_Biology_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($BiologyMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($BiologyMarksAfterCalculate, 100);
				}


				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				//echo '<pre>';
				//print_r($ReturnValue);
			} elseif (SCHOOL_NAME == 'wisdomcollege' && ($data[$A]['SubjectCode'] == '265' || $data[$A]['SubjectCode'] == '266')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = 0;
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				$HighermathMarksAfterCalculate = ($HighermathMarks / $HighermathCredit) * 100;
				if ($data[$A]['SubjectCode'] == '265') {
					$TGPforThisSubject = $this->number_to_num_grade($HighermathMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($HighermathMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($HighermathMarks, $HighermathCredit); die;
				}

				if ($is_Highermath_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($HighermathMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($HighermathMarksAfterCalculate, 100);
				}


				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				//echo '<pre>';
				//print_r($ReturnValue);
			} elseif (SCHOOL_NAME == 'wisdomcollege' && ($data[$A]['SubjectCode'] == '239' || $data[$A]['SubjectCode'] == '240')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = 0;
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				$AgricultureMarksAfterCalculate = ($AgricultureMarks / $AgricultureCredit) * 100;
				if ($data[$A]['SubjectCode'] == '239') {
					$TGPforThisSubject = $this->number_to_num_grade($AgricultureMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($AgricultureMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($AgricultureMarks, $AgricultureCredit); die;
				}

				if ($is_Agriculture_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($AgricultureMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($AgricultureMarksAfterCalculate, 100);
				}


				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				//echo '<pre>';
				//print_r($ReturnValue);
			} elseif (SCHOOL_NAME == 'wisdomcollege' && ($data[$A]['SubjectCode'] == '267' || $data[$A]['SubjectCode'] == '268')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = 0;
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				$HistoryCultureMarksAfterCalculate = ($HistoryCultureMarks / $HistoryCultureCredit) * 100;
				if ($data[$A]['SubjectCode'] == '267') {
					$TGPforThisSubject = $this->number_to_num_grade($HistoryCultureMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($HistoryCultureMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($HistoryCultureMarks, $HistoryCultureCredit); die;
				}

				if ($is_HistoryCulture_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($HistoryCultureMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($HistoryCultureMarksAfterCalculate, 100);
				}


				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				//echo '<pre>';
				//print_r($ReturnValue);
			} elseif (SCHOOL_NAME == 'wisdomcollege' && ($data[$A]['SubjectCode'] == '109' || $data[$A]['SubjectCode'] == '110')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = 0;
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				$EconomyMarksAfterCalculate = ($EconomyMarks / $EconomyCredit) * 100;
				if ($data[$A]['SubjectCode'] == '109') {
					$TGPforThisSubject = $this->number_to_num_grade($EconomyMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($EconomyMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($EconomyMarks, $EconomyCredit); die;
				}

				if ($is_Economy_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($EconomyMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($EconomyMarksAfterCalculate, 100);
				}


				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				//echo '<pre>';
				//print_r($ReturnValue);
			} elseif (SCHOOL_NAME == 'wisdomcollege' && ($data[$A]['SubjectCode'] == '249' || $data[$A]['SubjectCode'] == '250')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = 0;
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;
				$StudyOfIslamMarksAfterCalculate = ($StudyOfIslamMarks / $StudyOfIslamCredit) * 100;
				if ($data[$A]['SubjectCode'] == '249') {
					$TGPforThisSubject = $this->number_to_num_grade($StudyOfIslamMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($StudyOfIslamMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($StudyOfIslamMarks, $StudyOfIslamCredit); die;
				}

				if ($is_StudyOfIslam_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($StudyOfIslamMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($StudyOfIslamMarksAfterCalculate, 100);
				}


				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				//echo '<pre>';
				//print_r($ReturnValue);
			} elseif (SCHOOL_NAME == 'wisdomcollege' && ($data[$A]['SubjectCode'] == '269' || $data[$A]['SubjectCode'] == '270')) {
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = 0;
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				//Extra head calculation
				$subject_id = $data[$A]['SubjectId'];
				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;

				$ReturnValue[$A]['total_number_get_from_other_exam'] = 0;

				//Extra head calculation
				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;

				$CivicsMarksAfterCalculate = ($CivicsMarks / $CivicsCredit) * 100;
				if ($data[$A]['SubjectCode'] == '269') {
					$TGPforThisSubject = $this->number_to_num_grade($CivicsMarksAfterCalculate, 100);
					$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
					$TCGPA = $TCGPA + $TGPforThisSubject;
					$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
					$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($CivicsMarksAfterCalculate, 100);
					//echo  $this->number_to_num_grade($CivicsMarks, $CivicsCredit); die;
				}

				if ($is_Civics_fail == 1) {
					$ReturnValue[$A]['AlphaGPA'] = "F";
					$ReturnValue[$A]['NumericGPA'] = 0.00;
				} else {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($CivicsMarksAfterCalculate, 100);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($CivicsMarksAfterCalculate, 100);
				}


				if ($ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				//echo '<pre>';
				//print_r($ReturnValue);
			} else {
				//die;
				$ReturnValue[$A]['StudentID'] = $data[$A]['StudentID'];
				$ReturnValue[$A]['StudentName'] = $data[$A]['StudentName'];
				$ReturnValue[$A]['StudentRole'] = $data[$A]['StudentRole'];
				$ReturnValue[$A]['StudentReg'] = $data[$A]['StudentReg'];
				$ReturnValue[$A]['father_name'] = $data[$A]['father_name'];
				$ReturnValue[$A]['mother_name'] = $data[$A]['mother_name'];
				$ReturnValue[$A]['date_of_birth'] = $data[$A]['date_of_birth'];
				$ReturnValue[$A]['SubjectId'] = $data[$A]['SubjectId'];
				$ReturnValue[$A]['SubjectName'] = $data[$A]['SubjectName'];
				$ReturnValue[$A]['SubjectCode'] = $data[$A]['SubjectCode'];

				$ReturnValue[$A]['ClassName'] = $data[$A]['ClassName'];
				$ReturnValue[$A]['SectionName'] = $data[$A]['SectionName'];

				if (!empty($subject_order) && isset($subject_order[$data[$A]['SubjectId']])) {
					$ReturnValue[$A]['order_number'] = $subject_order[$data[$A]['SubjectId']]['order_number'];
				} else {
					$ReturnValue[$A]['order_number'] = 0;
				}
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['allocated_written'] = $data[$A]['allocated_written'];
				$ReturnValue[$A]['allocated_objective'] = $data[$A]['allocated_objective'];
				$ReturnValue[$A]['allocated_practical'] = $data[$A]['allocated_practical'];
				$ReturnValue[$A]['allocated_class_test'] = $data[$A]['allocated_class_test'];
				$ReturnValue[$A]['written'] = $data[$A]['written'];
				$ReturnValue[$A]['objective'] = $data[$A]['objective'];
				$ReturnValue[$A]['practical'] = $data[$A]['practical'];
				$ReturnValue[$A]['class_test'] = $data[$A]['class_test'];
				$ReturnValue[$A]['credit'] = $data[$A]['credit'];
				$ReturnValue[$A]['calculable_amount'] = $data[$A]['calculable_amount'];
				$ReturnValue[$A]['ExamName'] = $data[$A]['ExamName'];

				$subject_id = $data[$A]['SubjectId'];


				$class_test_grace = 0;
				$written_grace = 0;
				$objective_grace = 0;
				$practical_grace = 0;
				$grace_mark = $this->db->query("SELECT * FROM tbl_grace_number WHERE subject_id = '$subject_id' AND class_id ='$ClassID' AND exam_id ='$ExamID'")->result_array();
				if (!empty($grace_mark)) {
					$class_test_grace = $grace_mark[0]['class_test_grace'];
					$written_grace = $grace_mark[0]['written_grace'];
					$objective_grace = $grace_mark[0]['objective_grace'];
					$practical_grace = $grace_mark[0]['practical_grace'];
					//echo '<pre>';
					//print_r($grace_mark);
					//die;
				}

				$is_fail = 0;
				$grace_take = 0;
				if ($is_pass_to_different_segment_of_result == '1' && $ReturnValue[$A]['allocated_written'] > 0) {
					$written_alfa_gpa = $this->number_to_alfa_grade($data[$A]['written'], $ReturnValue[$A]['allocated_written']);
					if ($written_alfa_gpa == 'F') {
						if ($written_grace > 0 && $num_of_subject_allow_for_grace_count < $num_of_subject_allow_for_grace) {
							$pass_mark = round($ReturnValue[$A]['allocated_written'] / 3);
							$need_grace_mark_for_pass = $pass_mark - $data[$A]['written'];

							if ($need_grace_mark_for_pass <= $written_grace) {
								$grace_take = 1;
								$ReturnValue[$A]['written'] = number_format(($data[$A]['written'] + $need_grace_mark_for_pass), 2);
							} else {
								$ReturnValue[$A]['AlphaGPA'] = 'F';
								$ReturnValue[$A]['NumericGPA'] = '0.00';
								$is_fail = 1;
							}
						} else {
							$ReturnValue[$A]['AlphaGPA'] = 'F';
							$ReturnValue[$A]['NumericGPA'] = '0.00';
							$is_fail = 1;
						}
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $ReturnValue[$A]['allocated_objective'] > 0 && $is_fail == 0) {
					$objective_alfa_gpa = $this->number_to_alfa_grade($data[$A]['objective'], $ReturnValue[$A]['allocated_objective']);
					if ($objective_alfa_gpa == 'F') {
						if ($objective_grace > 0 && $num_of_subject_allow_for_grace_count < $num_of_subject_allow_for_grace) {
							$pass_mark = round($ReturnValue[$A]['allocated_objective'] / 3);
							$need_grace_mark_for_pass = $pass_mark - $data[$A]['objective'];

							if ($need_grace_mark_for_pass <= $objective_grace) {
								$grace_take = 1;
								$ReturnValue[$A]['objective'] = number_format(($data[$A]['objective'] + $need_grace_mark_for_pass), 2);
							} else {
								$ReturnValue[$A]['AlphaGPA'] = 'F';
								$ReturnValue[$A]['NumericGPA'] = '0.00';
								$is_fail = 1;
							}
						} else {
							$ReturnValue[$A]['AlphaGPA'] = 'F';
							$ReturnValue[$A]['NumericGPA'] = '0.00';
							$is_fail = 1;
						}
					}
				}

				if ($is_pass_to_different_segment_of_result == '1' && $ReturnValue[$A]['allocated_practical'] > 0 && $is_fail == 0) {
					$practical_alfa_gpa = $this->number_to_alfa_grade($data[$A]['practical'], $ReturnValue[$A]['allocated_practical']);
					if ($practical_alfa_gpa == 'F') {
						if ($practical_grace > 0 && $num_of_subject_allow_for_grace_count < $num_of_subject_allow_for_grace) {
							$pass_mark = round($ReturnValue[$A]['allocated_practical'] / 3);
							$need_grace_mark_for_pass = $pass_mark - $data[$A]['practical'];

							if ($need_grace_mark_for_pass <= $practical_grace) {
								$grace_take = 1;
								$ReturnValue[$A]['practical'] = number_format(($data[$A]['practical'] + $need_grace_mark_for_pass), 2);
							} else {
								$ReturnValue[$A]['AlphaGPA'] = 'F';
								$ReturnValue[$A]['NumericGPA'] = '0.00';
								$is_fail = 1;
							}
						} else {
							$ReturnValue[$A]['AlphaGPA'] = 'F';
							$ReturnValue[$A]['NumericGPA'] = '0.00';
							$is_fail = 1;
						}
					}
				}

				if ($grace_take == 1) {
					// echo $data[$A]['SubjectCode']. '/'. $grace_take . '/' .$num_of_subject_allow_for_grace_count.'<br>';
					$num_of_subject_allow_for_grace_count = $num_of_subject_allow_for_grace_count + 1;
				}

				//Extra head calculation

				$total_extra_mark = 0;
				$extra_marks = $this->db->query("SELECT mark FROM tbl_student_wise_extra_mark WHERE student_id = '$StudentID' AND subject_id ='$subject_id' AND exam_id ='$ExamID'")->result_array();
				if (!empty($extra_marks)) {
					foreach ($extra_marks as $datacol_row):
						$total_extra_mark = $total_extra_mark + $datacol_row['mark'];
					endforeach;
				}
				$ReturnValue[$A]['total_extra_mark'] = $total_extra_mark;


				//End Extra head calculation


				//Annual Result Calculation
				$total_number_get_from_other_exam = 0;
				$student_id = $data[$A]['StudentID'];
				if ($is_annual_exam == 1 && $annual_mark_calculate_method == 'SWP') {
					$ar_exam_list = $this->db->query("SELECT * FROM `tbl_exam` WHERE is_applicable_for_final_calcultion = 1 AND year = '$year' order by exam_order")->result_array();
					//echo '<pre>';
					//print_r($ar_exam_list);
					//die;
					$k = 0;
					while ($k < count($ar_exam_list)) {
						$ar_exam_id = $ar_exam_list[$k]['id'];
						$ar_percentage_of_grand_result = $ar_exam_list[$k]['percentage_of_grand_result'];
						if ($ExamID != $ar_exam_id) {
							$ar_mark_info = $this->db->query("SELECT * FROM `tbl_result_process_details` WHERE subject_id = '$subject_id' AND student_id = '$student_id' AND exam_id = '$ar_exam_id' AND year = '$year'")->result_array();
							if (!empty($ar_mark_info)) {
								//echo $ar_mark_info[0]['total_obtain'].'/';
								$total_number_get_from_other_exam = $total_number_get_from_other_exam + ($ar_mark_info[0]['total_obtain'] * $ar_percentage_of_grand_result / 100);
							}
						}
						$k++;
					}
				}

				//echo $total_number_get_from_other_exam; die;
				$ReturnValue[$A]['total_number_get_from_other_exam'] = $total_number_get_from_other_exam;
				//End Annual Result Calculation

				//if($data[$A]['SubjectCode'] =='107'){
				//   echo $data[$A]['calculable_amount']; die;
				//}

				$calculable_total_obtain = (($ReturnValue[$A]['written'] + $ReturnValue[$A]['objective'] + $ReturnValue[$A]['practical'] + $ReturnValue[$A]['class_test']) * $data[$A]['calculable_amount']) / 100;
				$ReturnValue[$A]['calculable_total_obtain'] = $calculable_total_obtain;
				$TotalNumber = $calculable_total_obtain + $total_extra_mark + $total_number_get_from_other_exam;
				$ReturnValue[$A]['total_number'] = $TotalNumber;
				$ReturnValue[$A]['total_number_without_other_exam_mark'] = $calculable_total_obtain + $total_extra_mark;
				$TotalNumberAchived = $TotalNumberAchived + $TotalNumber;


				$TGPforThisSubject = $this->number_to_num_grade($TotalNumber, $data[$A]['credit']);
				$dttt[$data[$A]['SubjectName']] = $TGPforThisSubject;
				$TCGPA = $TCGPA + $TGPforThisSubject;
				$TCGPAForWithoutOptional = $TCGPAForWithoutOptional + $TGPforThisSubject;
				$dttt[$data[$A]['SubjectName']] = $this->number_to_num_grade($TotalNumber, $data[$A]['credit']);


				if ($is_fail == 0) {
					$ReturnValue[$A]['AlphaGPA'] = $this->number_to_alfa_grade($TotalNumber, $data[$A]['credit']);
					$ReturnValue[$A]['NumericGPA'] = $this->number_to_num_grade($TotalNumber, $data[$A]['credit']);
				}

				if ($data[$A]['is_optional'] != '1' && $ReturnValue[$A]['AlphaGPA'] == 'F') {
					$number_of_failed_subject = $number_of_failed_subject + 1;
				}

				if ($data[$A]['is_optional'] == '1') {
					if ($ReturnValue[$A]['NumericGPA'] <= '2') {
						$IsEligibleToAddFourthSubject = 0;
						$TCGPA = $TCGPA - $ReturnValue[$A]['NumericGPA'];
						$TCGPAForWithoutOptional = $TCGPAForWithoutOptional - $ReturnValue[$A]['NumericGPA'];
						$num_of_cal_sub = $num_of_cal_sub - 1;
					} else {
						$IsEligibleToAddFourthSubject = 1;
						$TCGPA = $TCGPA - 2;
						$TCGPAForWithoutOptional = $TCGPAForWithoutOptional - $ReturnValue[$A]['NumericGPA'];
						$num_of_cal_sub = $num_of_cal_sub - 1;
					}
				}
			}

			$ReturnValue[$A]['is_optional'] = $data[$A]['is_optional'];
			if ($ReturnValue[$A]['AlphaGPA'] == 'F' && $data[$A]['is_optional'] == '0') {
				$IsPassed = 0;
			}
		}
		//error_reporting(E_ERROR | E_PARSE);
		$ReturnValue['TotalNumberAchived'] = $TotalNumberAchived;

		$ReturnValue['calculable_total_mark_percentage'] = $calculable_total_mark_percentage;
		$ReturnValue['calculable_total_mark'] = ($TotalNumberAchived * $calculable_total_mark_percentage) / 100;
		// echo $calculable_total_mark_percentage.'/'.$ReturnValue['calculable_total_mark']; die;

		/*if($StudentID == 445){
            echo '<pre>';
            print_r($dttt);
            echo $num_of_cal_sub.'/'.$TCGPA . '/' . $TCGPAForWithoutOptional; die;
        }*/
		// echo $num_of_cal_sub; die;
		//echo $TCGPA . '/' . $TCGPAForWithoutOptional;
		//die;
		//echo '<pre>';
		// print_r($ReturnValue);
		//die;
		$ReturnValue['calculatable_subject'] = $num_of_cal_sub;
		$ReturnValue['number_of_failed_subject'] = $number_of_failed_subject;
		//  echo '<pre>';
		//print_r($ReturnValue);
		//die;
		if ($IsPassed > 0 || $is_calculate_cgpa_when_fail == 1) {
			//echo '/'.$StudentID.'/';
			//echo $StudentID.'/'.$num_of_cal_sub.'//';
			if ($num_of_cal_sub == 0) {
				$ReturnValue['CGPA'] = 0.00;
				$ReturnValue['CGPAwithFourthSubject'] = 0.00;
			} else {
				$ReturnValue['CGPA'] = round($TCGPA / $num_of_cal_sub, 2);
				$tttt = $TCGPA - 2;
				$mmm = $num_of_cal_sub - 1;

				$CGPAwithoutFourthSubject = round($tttt / $mmm, 2);
				if ($CGPAwithoutFourthSubject > 5 || $CGPAwithoutFourthSubject > $ReturnValue['CGPA']) {
					$ReturnValue['CGPAwithoutFourthSubject'] = round($TCGPAForWithoutOptional / $num_of_cal_sub, 2);
				} else {
					$ReturnValue['CGPAwithoutFourthSubject'] = round($tttt / $mmm, 2);
				}

				if ($ReturnValue['CGPA'] > 5) {
					$ReturnValue['CGPA'] = 5.00;
				}

				$ReturnValue['CGPAwithFourthSubject'] = round($TCGPAForWithoutOptional / $num_of_cal_sub, 2);
				if ($ReturnValue['CGPAwithFourthSubject'] > 5) {
					$ReturnValue['CGPAwithFourthSubject'] = 5.00;
				}
			}
		} else {
			$ReturnValue['CGPA'] = '0.00';
			$ReturnValue['CGPAwithFourthSubject'] = '0.00';
			$ReturnValue['CGPAwithoutFourthSubject'] = '0.00';
		}


		//echo '<pre>';
		// print_r($ReturnValue);
		//die;
		return $ReturnValue;
	}


	public function result_process_data_save()
	{
		$exam_id = $this->input->post("exam_id");
		$class_id = $this->input->post("class_id");
		$section_id = $this->input->post("section_id");
		$shift_id = $this->input->post("shift_id");
		$group_id = $this->input->post("group_id");

		//exam details
		$exam_info = $this->db->query("SELECT * FROM `tbl_exam` WHERE `id` = '$exam_id'")->result_array();
		if ($exam_info[0]['exam_type_id'] <= 0) {
			$sdata['exception'] = "Please input first what type of exam it is.";
			$this->session->set_userdata($sdata);
			redirect('exams/index');
		}
		$year = $exam_info[0]['year'];
		$percentage_of_grand_result = $exam_info[0]['percentage_of_grand_result'];
		$is_applicable_for_final_calcultion = $exam_info[0]['is_applicable_for_final_calcultion'];
		$is_annual_exam = $exam_info[0]['is_annual_exam'];
		$is_combined_result = $exam_info[0]['is_combined_result'];
		$combined_percentage = $exam_info[0]['combined_percentage'];


		$config = $this->db->query("SELECT annual_mark_calculate_method,calculable_total_mark_percentage,how_to_generate_position,multiple_atudent_allow_same_position,is_calculate_cgpa_when_fail FROM `tbl_config`")->result_array();
		$annual_mark_calculate_method = "";
		if (!empty($config)) {
			$annual_mark_calculate_method = $config[0]['annual_mark_calculate_method'];
		}


		$StudentList = $this->db->query("SELECT
s.`id`, s.`name`, s.`roll_no`, s.`reg_no`, s.`year`
FROM
`tbl_student` AS s
WHERE s.`class_id` = '$class_id' AND s.`shift_id` = '$shift_id' AND s.`group` = '$group_id'
AND s.`section_id` = '$section_id' AND s.`status` = 1 AND s.`year` = '$year'")->result_array();
		//echo '<pre>';
		//print_r($StudentList);

		if (!empty($StudentList)) {
			$loop_time = count($StudentList);

			$i = 0;
			while ($i < $loop_time) {
				$Result = $this->result_by_student($StudentList[$i]['id'], $class_id, $exam_id);
				// echo '<pre>';
				//print_r($Result);
				//die;
				if ($Result['TotalNumberAchived'] != 0) {
					$student_result = array();
					$student_id = $StudentList[$i]['id'];
					$student_result['student_id'] = $student_id;
					$student_result['exam_id'] = $exam_id;
					$student_result['class_id'] = $class_id;
					$student_result['reg_no'] = $Result[0]['StudentReg'];
					$student_result['roll_no'] = $Result[0]['StudentRole'];
					$student_result['section_id'] = $section_id;
					$student_result['group'] = $group_id;
					$student_result['shift'] = $shift_id;
					$student_result['year'] = $StudentList[$i]['year'];
					$student_result['total_obtain_mark'] = $Result['TotalNumberAchived'];
					$student_result['calculable_total_mark_percentage'] = $Result['calculable_total_mark_percentage'];
					$student_result['calculable_total_mark'] = $Result['calculable_total_mark'];
					$student_result['gpa_with_optional'] = $Result['CGPA'];
					$student_result['gpa_without_optional'] = $Result['CGPAwithoutFourthSubject'];
					$student_result['c_alpha_gpa_with_optional'] = $this->get_numeric_gpa_to_alfa_gpa($Result['CGPA']);
					$student_result['c_alpha_gpa_without_optional'] = $this->get_numeric_gpa_to_alfa_gpa($Result['CGPAwithoutFourthSubject']);
					$student_result['calculatable_subject'] = $Result['calculatable_subject'];
					$student_result['calculable_amount'] = $Result[0]['calculable_amount'];
					$student_result['position'] = 0;
					$student_result['is_annual_exam'] = $is_annual_exam;
					$student_result['number_of_failed_subject'] = $Result['number_of_failed_subject'];
					//echo $Result[0]['number_of_failed_subject']; die;
					date_default_timezone_set("Asia/Dhaka");
					$student_result['date'] = date("Y-m-d H:i:s");
					// echo '<pre>';
					//  print_r($student_result);
					//  die;
					$this->db->query("DELETE FROM `tbl_combined_result_details` WHERE `exam_id` = '$exam_id' AND `student_id` = '$student_id'");
					$this->db->query("DELETE FROM `tbl_result_process_details` WHERE `exam_id` = '$exam_id' AND `student_id` = '$student_id'");
					$this->db->query("DELETE FROM `tbl_result_process` WHERE `exam_id` = '$exam_id' AND `student_id` = '$student_id'");
					$this->db->insert("tbl_result_process", $student_result);

					$result_id = $this->db->insert_id();
					// echo '<pre>';
					// print_r($Result);
					// die;
					//for annual calculation
					$total_credit = 0;
					//for annual calculation
					//subject wise marks save
					$total_subject_this_student = count($Result) - 8;
					//echo count($Result); die;
					$result_details_data = array();
					for ($A = 0; $A < $total_subject_this_student; $A++) {
						$result_detail = array();
						$result_detail['result_id'] = $result_id;
						$result_detail['exam_id'] = $exam_id;
						$result_detail['student_id'] = $student_id;
						$result_detail['class_id'] = $class_id;
						$result_detail['section_id'] = $section_id;
						$result_detail['group'] = $group_id;
						$result_detail['shift'] = $shift_id;
						$result_detail['year'] = $StudentList[$i]['year'];
						$result_detail['subject_id'] = $Result[$A]['SubjectId'];
						$result_detail['subject_name'] = $Result[$A]['SubjectName'];
						$result_detail['subject_code'] = $Result[$A]['SubjectCode'];
						$result_detail['order_number'] = $Result[$A]['order_number'];
						$result_detail['credit'] = $Result[$A]['credit'];
						//for annual calculation
						$total_credit = $total_credit + $Result[$A]['credit'];
						//for annual calculation
						$result_detail['allocated_written'] = $Result[$A]['allocated_written'];
						$result_detail['allocated_objective'] = $Result[$A]['allocated_objective'];
						$result_detail['allocated_practical'] = $Result[$A]['allocated_practical'];
						$result_detail['allocated_class_test'] = $Result[$A]['allocated_class_test'];
						$result_detail['written'] = $Result[$A]['written'];
						$result_detail['objective'] = $Result[$A]['objective'];
						$result_detail['practical'] = $Result[$A]['practical'];
						$result_detail['class_test'] = $Result[$A]['class_test'];
						$result_detail['total_extra_mark'] = $Result[$A]['total_extra_mark'];
						$result_detail['total_number_get_from_other_exam'] = $Result[$A]['total_number_get_from_other_exam'];
						$result_detail['calculable_total_obtain'] = $Result[$A]['calculable_total_obtain'];
						$result_detail['total_obtain'] = $Result[$A]['total_number'];
						$result_detail['calculable_amount'] = $Result[$A]['calculable_amount'];
						$result_detail['alpha_gpa'] = $Result[$A]['AlphaGPA'];
						$result_detail['numeric_gpa'] = $Result[$A]['NumericGPA'];
						$result_detail['is_optional'] = $Result[$A]['is_optional'];
						$result_details_data[] = $result_detail;
						// echo $total_subject_this_student.'/';
					}
					$this->db->insert_batch('tbl_result_process_details', $result_details_data);

					//for annual calculation
					if ($annual_mark_calculate_method == 'TMS' && $is_annual_exam == 1) {
						$total_average_mark = 0;
						$ar_exam_list = $this->db->query("SELECT * FROM `tbl_exam` WHERE is_applicable_for_final_calcultion = 1 AND year = '$year' order by exam_order")->result_array();
						$k = 0;
						while ($k < count($ar_exam_list)) {
							$ar_exam_id = $ar_exam_list[$k]['id'];
							$ar_mark_info = $this->db->query("SELECT total_obtain_mark FROM `tbl_result_process` WHERE student_id = '$student_id' AND exam_id = '$ar_exam_id' AND year = '$year'")->result_array();
							if (!empty($ar_mark_info)) {
								$total_average_mark = $total_average_mark + $ar_mark_info[0]['total_obtain_mark'];
							}
							$k++;
						}
						$save_data = array();
						$save_data['grand_average_mark'] = $total_average_mark;
						$this->db->where('id', $result_id);
						$this->db->update('tbl_result_process', $save_data);
					}
					//end for annual calculation


					//annual position for semester wise prcentage
					$annual_mark_calculate_method = "CSWP";
					if ($annual_mark_calculate_method == 'CSWP') {
						//for annual calculation
						$average_mark = 0;
						$average_gpa = 0;
						if ($is_applicable_for_final_calcultion == 1) {
							//echo 'hhh'.$Result['TotalNumberAchived'];
							//die;
							$save_data = array();
							$is_average_mark_percentage_by_total_credit = 0;
							if ($is_average_mark_percentage_by_total_credit == 1) {
								$save_data['average_percentage_for_final'] = $percentage_of_grand_result;
								$save_data['average_mark'] = ($Result['TotalNumberAchived'] * $percentage_of_grand_result) / $total_credit;
							} else {
								$save_data['average_percentage_for_final'] = $percentage_of_grand_result;
								$save_data['average_mark'] = ($Result['TotalNumberAchived'] * $percentage_of_grand_result) / 100;
							}
							$save_data['average_gpa'] = ($Result['CGPA'] * $percentage_of_grand_result) / 100;
							//echo '<pre>';
							//print_r($save_data);
							//die;


							//final
							if ($is_annual_exam == 1) {
								$total_mark = 0;
								$total_average_mark = 0;
								$ar_exam_list = $this->db->query("SELECT * FROM `tbl_exam` WHERE is_applicable_for_final_calcultion = 1 AND year = '$year' order by exam_order")->result_array();
								$k = 0;
								while ($k < count($ar_exam_list)) {
									$ar_exam_id = $ar_exam_list[$k]['id'];
									if ($exam_id == $ar_exam_id) {
										$total_average_mark += $save_data['average_mark'];
									} else {
										$ar_mark_info = $this->db->query("SELECT * FROM `tbl_result_process` WHERE student_id = '$student_id' AND exam_id = '$ar_exam_id' AND year = '$year'")->result_array();
										if (!empty($ar_mark_info)) {
											$total_average_mark += $ar_mark_info[0]['average_mark'];
										}
									}
									$k++;
								}
								$save_data['grand_average_mark'] = number_format($total_average_mark, 4);
								$save_data['grand_average_cgpa'] = number_format($this->number_to_num_grade($total_average_mark, 100), 4);
								$save_data['grand_average_grade'] = $this->number_to_alfa_grade($total_average_mark, 100);
								//  echo '<pre>';
								//print_r($save_data);
								//  die;
							}

							//final


							$this->db->where('id', $result_id);
							$this->db->update('tbl_result_process', $save_data);
						}
						//for annual calculation
					}

					//end annual position for semester wise percentage


					//echo '<pre>';
					//print_r($save_data);
					//die;
					//subject wise marks save end
				}
				$i++;
			}


			//combined result calculation

			if ($is_combined_result == 1) {
				$combined_exam_info = $this->db->query("SELECT * FROM `tbl_exam_combined_result`
                   WHERE `exam_id` = '$exam_id'")->result_array();
				if (!empty($combined_exam_info)) {
					foreach ($StudentList as $combined_student) {
						// echo '<pre>';
						// print_r($combined_student);
						// die;
						$cr = 0;
						$combined_result_data = array();
						//for parent exam
						$combined_result_data[$cr]['exam_id'] = $exam_id;
						$student_id = $combined_student['id'];
						$combined_result_data[$cr]['student_id'] = $student_id;

						$combined_result_data[$cr]['class_id'] = $class_id;
						$combined_result_data[$cr]['section_id'] = $section_id;
						$combined_result_data[$cr]['group_id'] = $group_id;
						$combined_result_data[$cr]['shift_id'] = $shift_id;

						$combined_exam_id = $exam_id;
						$combined_result_data[$cr]['combined_exam_id'] = $exam_id;
						$combined_result_data[$cr]['percentage_from_total'] = $combined_percentage;
						$result_info_for_this_student_this_exam = $this->db->query("SELECT total_obtain_mark FROM `tbl_result_process`
                           WHERE `exam_id` = '$combined_exam_id' AND `student_id` = '$student_id'")->result_array();
						if (empty($result_info_for_this_student_this_exam)) {
							$combined_result_data[$cr]['total_obtained_mark'] = 0;
							$combined_result_data[$cr]['calculable_mark_from_total'] = 0;
						} else {
							$combined_result_data[$cr]['total_obtained_mark'] = $result_info_for_this_student_this_exam[0]['total_obtain_mark'];
							$combined_result_data[$cr]['calculable_mark_from_total'] = $result_info_for_this_student_this_exam[0]['total_obtain_mark'] * $combined_percentage / 100;
						}

						$cr++;

						foreach ($combined_exam_info as $combined_exam) {
							$combined_result_data[$cr]['exam_id'] = $exam_id;
							$combined_result_data[$cr]['student_id'] = $student_id;

							$combined_result_data[$cr]['class_id'] = $class_id;
							$combined_result_data[$cr]['section_id'] = $section_id;
							$combined_result_data[$cr]['group_id'] = $group_id;
							$combined_result_data[$cr]['shift_id'] = $shift_id;

							$combined_exam_id = $combined_exam['combined_exam_id'];
							$combined_result_data[$cr]['combined_exam_id'] = $combined_exam_id;
							$combined_result_data[$cr]['percentage_from_total'] = $combined_exam['percentage'];
							$result_info_for_this_student_this_exam = $this->db->query("SELECT total_obtain_mark FROM `tbl_result_process`
                               WHERE `exam_id` = '$combined_exam_id' AND `student_id` = '$student_id'")->result_array();
							if (empty($result_info_for_this_student_this_exam)) {
								$combined_result_data[$cr]['total_obtained_mark'] = 0;
								$combined_result_data[$cr]['calculable_mark_from_total'] = 0;
							} else {
								$combined_result_data[$cr]['total_obtained_mark'] = $result_info_for_this_student_this_exam[0]['total_obtain_mark'];
								$combined_result_data[$cr]['calculable_mark_from_total'] = $result_info_for_this_student_this_exam[0]['total_obtain_mark'] * $combined_exam['percentage'] / 100;
							}
							$cr++;
						}
						$this->db->insert_batch('tbl_combined_result_details', $combined_result_data);
					}


					//position calculation
					$combined_positions = $this->db->query("SELECT cr.`student_id`,SUM(cr.`calculable_mark_from_total`) AS calculable_mark_from_total FROM `tbl_combined_result_details` AS cr
WHERE cr.`class_id` = '$class_id' AND cr.`section_id` = '$section_id' AND cr.`group_id` = '$group_id' AND cr.`shift_id` = '$shift_id'
GROUP BY cr.`student_id` ORDER BY SUM(cr.`calculable_mark_from_total`) DESC")->result_array();


					$cpupdateArray = array();
					$cp = 1;
					foreach ($combined_positions as $combined_position) {
						$student_id = $combined_position['student_id'];
						$cpupdateArray['position'] = $cp;

						$this->db->where('student_id', $student_id);
						$this->db->where('exam_id', $exam_id);
						$this->db->update('tbl_result_process', $cpupdateArray);
						//  echo $this->db->last_query() . '<br>';

						$cp++;
					}
					//  die;


					// print_r($cpupdateArray);
					// die;

					//end position calculation
				}
			} else {  //end combined result calculation
				//position update

				$position_where = "";
				if (!empty($config)) {
					$how_to_generate_position = $config[0]['how_to_generate_position'];
					$multiple_atudent_allow_same_position = $config[0]['multiple_atudent_allow_same_position'];
					$is_calculate_cgpa_when_fail = $config[0]['is_calculate_cgpa_when_fail'];
					$calculable_total_mark_percentage = $config[0]['calculable_total_mark_percentage'];
				} else {
					$how_to_generate_position = 'CS';
					$multiple_atudent_allow_same_position = 0;
					$is_calculate_cgpa_when_fail = 0;
				}

				if ($how_to_generate_position == 'CS') {
					$position_where = " AND `section_id` = '$section_id' ";
				}

				$when_fail_where = "";
				if ($is_calculate_cgpa_when_fail == 0) {
					$when_fail_where = " AND `gpa_with_optional` != '0.00' ";
				}


				//    echo  $annual_mark_calculate_method;
				//    die;


				//annual position update
				if ($annual_mark_calculate_method == 'CSWP' && $is_annual_exam == 1) {
					$annual_positions = $this->db->query("SELECT id,grand_average_mark FROM tbl_result_process
            WHERE exam_id = '$exam_id' AND class_id = '$class_id' AND `section_id` = '$section_id'
            AND `shift` = '$shift_id' AND `group` = '$group_id' ORDER BY grand_average_mark DESC;")->result_array();

					//echo '<pre>';
					// print_r($annual_positions);
					// die;
					$multiple_atudent_allow_same_position = 1;
					if (!empty($annual_positions)) {
						if ($multiple_atudent_allow_same_position == 1) {
							$updateArray = array();
							$same_position_count = 0;
							for ($x = 0; $x < count($annual_positions); $x++) {
								if ($x == 0) {
									$position = ($x + 1);
								} else {
									if ($annual_positions[$x - 1]['grand_average_mark'] == $annual_positions[$x]['grand_average_mark']) {
										$same_position_count = $same_position_count + 1;
										$position = ($x + 1) - $same_position_count;
									} else {
										if ($same_position_count > 0) {
											$position = ($x + 1) - $same_position_count;
										} else {
											$position = ($x + 1);
										}
									}
								}
								$updateArray[] = array(
									'id' => $annual_positions[$x]['id'],
									'annual_position' => $position
								);
							}
							$this->db->update_batch('tbl_result_process', $updateArray, 'id');
						}
					}
				}
				//annual possion update


				$positions = $this->db->query("SELECT id,student_id,exam_id,total_obtain_mark,calculable_total_mark,gpa_with_optional,gpa_without_optional FROM tbl_result_process
                WHERE exam_id = '$exam_id' AND class_id = '$class_id' AND  `group` = '$group_id' AND `shift` = '$shift_id' $position_where $when_fail_where
                ORDER BY calculable_total_mark DESC, gpa_with_optional DESC;")->result_array();

				//for SMMadrashah
				if ($calculable_total_mark_percentage < 100) {
					$data['extra_head'] = $this->db->query("SELECT exa.*,exh.name as head_name FROM `tbl_class_wise_extra_head_amount` as exa
                               INNER JOIN tbl_exam_marking_extra_head as exh on exa.head_id = exh.id WHERE exa.class_id = '$class_id' ORDER BY exh.id ASC")->result_array();

					$position_row_num = 0;
					foreach ($positions as $position_row):
						$student_id = $position_row['student_id'];
						$total_extra_head_mark_db = $this->db->query("SELECT SUM(`allowable_mark`) as total_allowable_mark FROM `tbl_student_wise_extra_mark` WHERE  `exam_id` = '$exam_id' AND `student_id` = '$student_id'")->result_array();
						$total_extra_head_mark = $total_extra_head_mark_db[0]['total_allowable_mark'];
						//echo $positions[$position_row_num]['calculable_total_mark'] . '/' . $total_extra_head_mark;; die;
						$positions[$position_row_num]['calculable_total_mark_before_calculation'] = $positions[$position_row_num]['calculable_total_mark'];
						$positions[$position_row_num]['calculable_total_mark'] = $positions[$position_row_num]['calculable_total_mark'] + $total_extra_head_mark;
						$positions[$position_row_num]['total_extra_head_mark'] = $total_extra_head_mark;
						$position_row_num++;
					endforeach;


					$sort = array();
					foreach ($positions as $k => $v) {
						$sort['calculable_total_mark'][$k] = $v['calculable_total_mark'];
					}

					array_multisort($sort['calculable_total_mark'], SORT_DESC, $positions);

					//echo "<pre>";
					// print_r($arr);
				}


				//echo '<pre>';
				//print_r($positions); die;
				//echo	$this->db->last_query(); die;

				if (!empty($positions)) {
					//echo $multiple_atudent_allow_same_position; die;
					if ($multiple_atudent_allow_same_position == 1) {
						$updateArray = array();
						$same_position_count = 0;
						for ($x = 0; $x < count($positions); $x++) {
							if ($positions[$x]['gpa_with_optional'] == '0.00') {
								$position = 0;
							} else {
								if ($x == 0) {
									$position = ($x + 1);
								} else {
									if ($positions[$x - 1]['calculable_total_mark'] == $positions[$x]['calculable_total_mark']) {
										$same_position_count = $same_position_count + 1;
										$position = ($x + 1) - $same_position_count;
										//echo $positions[$x - 1]['calculable_total_mark'].'/'.$positions[$x]['calculable_total_mark'].'/'.$position.'/'.$positions[$x]['id'].'<br>';
									} else {
										if ($same_position_count > 0) {
											$position = ($x + 1) - $same_position_count;
										} else {
											$position = ($x + 1);
										}
									}
								}
							}

							$updateArray[] = array(
								'id' => $positions[$x]['id'],
								'position' => $position
							);
						}
					} else {
						$updateArray = array();
						for ($x = 0; $x < count($positions); $x++) {
							if ($positions[$x]['gpa_with_optional'] == '0.00') {
								$position = 0;
							} else {
								$position = $x + 1;
							}

							$updateArray[] = array(
								'id' => $positions[$x]['id'],
								'position' => $position
							);
						}
					}

					// echo '<pre>';
					// print_r($updateArray);
					//die;

					$this->db->update_batch('tbl_result_process', $updateArray, 'id');

					//position update
				}
				//end
			}

			$sdata['message'] = "Result Processed Successfully !";
			$this->session->set_userdata($sdata);
			redirect("result_processes/index");
		}
	}


	public function get_numeric_gpa_to_alfa_gpa($gpa)
	{
		if (($gpa >= '5')) {
			return 'A+';
		} elseif ($gpa >= '4') {
			return "A";
		} elseif ($gpa >= '3.5') {
			return "A-";
		} elseif ($gpa >= '3.0') {
			return "B";
		} elseif ($gpa >= '2.0') {
			return "C";
		} elseif ($gpa >= '1.0') {
			return "D";
		} elseif ($gpa <= 0) {
			return "F";
		}
	}

	public function number_to_alfa_grade($Number, $Credit)
	{
		$Value = ($Number / $Credit);
		$pass_mark = floor($Credit / 3);

		//echo $Credit.'/'.($Credit / 3). '/' .$pass_mark.'/'.$Number.'/'.$Value.'<br>';
		if (($Value >= '0.8')) {
			return 'A+';
		} elseif (($Value >= '0.7' && $Value < '0.8')) {
			return "A";
		} elseif (($Value >= '0.6' && $Value < '0.7')) {
			return "A-";
		} elseif (($Value >= '0.5' && $Value < '0.6')) {
			return "B";
		} elseif (($Value >= '0.4' && $Value < '0.5')) {
			return "C";
		} elseif (($Value >= '0.33' && $Value < '0.4')) {
			return "D";
		} elseif (($Value < '0.33')) {
			if ($Number >= $pass_mark) {
				return "D";
			}
			return "F";
		}
	}

	public function number_to_num_grade($Number, $Credit)
	{
		$Value = ($Number / $Credit);
		$pass_mark = floor($Credit / 3);
		if (($Value >= '0.8')) {
			return 5;
		} elseif (($Value >= '0.7' && $Value < '0.8')) {
			return 4;
		} elseif (($Value >= '0.6' && $Value < '0.7')) {
			return 3.5;
		} elseif (($Value >= '0.5' && $Value < '0.6')) {
			return 3;
		} elseif (($Value >= '0.4' && $Value < '0.5')) {
			return 2;
		} elseif (($Value >= '0.33' && $Value < '0.4')) {
			return 1;
		} elseif (($Value < '0.33')) {
			if ($Number >= $pass_mark) {
				return 1;
			}
			return 0;
		}
	}
}
