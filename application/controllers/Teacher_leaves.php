
<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Teacher_leaves extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');
		$this->load->model(array('Timekeeping', 'Admin_login'));
		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$cond = array();
		if ($_POST) {
			$name = $this->input->post("name");
			$from_date = $this->input->post("from_date");
			$to_date = $this->input->post("to_date");

			$sdata['name'] = $name;
			$sdata['from_date'] = $from_date;
			$sdata['to_date'] = $to_date;
			$this->session->set_userdata($sdata);
			$cond['name'] = $name;
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
		} else {
			$name = $this->session->userdata('name');
			$from_date = $this->session->userdata('from_date');
			$to_date = $this->session->userdata('to_date');
			$cond['from_date'] = $from_date;
			$cond['to_date'] = $to_date;
			$cond['name'] = $name;
		}
		$data['title'] = 'Teacher/Staff Leave';
		$data['heading_msg'] = "Teacher/Staff Leave";

		$this->load->library('pagination');
		$config['base_url'] = site_url('teacher_leaves/index/');
		$config['per_page'] = 20;
		$config['total_rows'] = count($this->Timekeeping->get_all_teacher_staff_leave_list(0, 0, $cond));
		$this->pagination->initialize($config);
		$data['leaves'] = $this->Timekeeping->get_all_teacher_staff_leave_list(20, (int)$this->uri->segment(3), $cond);
		$data['counter'] = (int)$this->uri->segment(3);
		$data['is_show_button'] = "add";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('teacher_leaves/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function add()
	{
		if ($_POST) {
			$teacher_id = $this->input->post('teacher_id', true);
			$date_from = $this->input->post('date_from', true);
			$date_to = $this->input->post('date_to', true);
			if($date_from > $date_to){
				$sdata['exception'] = $this->lang->line('add_error_message') . " (From date can not be greater than to date)";
				$this->session->set_userdata($sdata);
				redirect("teacher_leaves/add");
			}

			$login_info = $this->db->query("SELECT id FROM tbl_teacher_staff_logins
						 WHERE teacher_id = '$teacher_id' AND (date BETWEEN '$date_from' AND '$date_to')")->result_array();
			if (!empty($login_info)) {
				$sdata['exception'] = $this->lang->line('add_error_message') . " (Login information found for this teacher)";
				$this->session->set_userdata($sdata);
				redirect("teacher_leaves/add");
			}
			$leave_days = $this->get_leave_days($date_from, $date_to);
			if(count($leave_days) <= 0){
				$sdata['exception'] = $this->lang->line('add_error_message') . " (Working days not found between from date and two date)";
				$this->session->set_userdata($sdata);
				redirect("teacher_leaves/add");
			}
			$data = array();
			$data['teacher_id'] = $teacher_id;
			$data['date_from'] = $date_from;
			$data['date_to'] = $date_to;
			$data['leave_type_id'] = $this->input->post('leave_type_id', true);
			$data['num_of_days'] = count($leave_days);
			$data['reason'] = $this->input->post('reason', true);
			$this->db->insert('tbl_teacher_staff_leave_applications', $data);

			//leave days save
			$insert_id = $this->db->insert_id();
			$leave_days_data = array();
			$j = 0;
			foreach ($leave_days as $day){
				$leave_days_data[$j]['leave_id'] = $insert_id;
				$leave_days_data[$j]['teacher_id'] = $teacher_id;
				$leave_days_data[$j]['date'] = $day['date'];
				$j++;
			}
			$this->db->insert_batch('tbl_teacher_staff_leave_days', $leave_days_data);

			$sdata['message'] = $this->lang->line('add_success_message');
			$this->session->set_userdata($sdata);
			redirect("teacher_leaves/add");
		}
		$data = array();
		$data['title'] = 'Teacher/Staff Leave';
		$data['heading_msg'] = "Teacher/Staff Leave";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['is_show_button'] = "index";
		$data['leave_types'] = $this->db->query("SELECT * FROM tbl_leave_type")->result_array();
		$data['teachers'] = $this->db->query("SELECT * FROM tbl_teacher WHERE status = '1'")->result_array();
		$data['maincontent'] = $this->load->view('teacher_leaves/add', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function get_leave_days($from_date, $to_date)
	{
		$begin = new DateTime($from_date);
		$end   = new DateTime($to_date);

		$leave_days = array();
		$leave_count = 0;
		for ($l = $begin; $l <= $end; $l->modify('+1 day')) {
			$date = $l->format("Y-m-d");
			$holiday = $this->db->query("SELECT * FROM tbl_holiday WHERE `date` = '$date'")->result_array();
			if(empty($holiday)){
				$leave_days[$leave_count]['date'] = $date;
			}
			$leave_count++;
		}
		return $leave_days;
	}

	public function edit($id=null)
	{
		if ($_POST) {
			$teacher_id = $this->input->post('teacher_id', true);
			$date_from = $this->input->post('date_from', true);
			$date_to = $this->input->post('date_to', true);
			if($date_from > $date_to){
				$sdata['exception'] = $this->lang->line('edit_error_message') . " (From date can not be greater than to date)";
				$this->session->set_userdata($sdata);
				redirect("teacher_leaves/index");
			}

			$login_info = $this->db->query("SELECT id FROM tbl_teacher_staff_logins
						 WHERE teacher_id = '$teacher_id' AND (date BETWEEN '$date_from' AND '$date_to')")->result_array();
			if (!empty($login_info)) {
				$sdata['exception'] = $this->lang->line('edit_error_message') . " (Login information found for this teacher)";
				$this->session->set_userdata($sdata);
				redirect("teacher_leaves/index");
			}

			$id = $this->input->post('id', true);
			$this->db->delete('tbl_teacher_staff_leave_days', array('leave_id' => $id));

			$leave_days = $this->get_leave_days($date_from, $date_to);
			if(count($leave_days) <= 0){
				$sdata['exception'] = $this->lang->line('edit_error_message') . " (Working days not found between from date and two date)";
				$this->session->set_userdata($sdata);
				redirect("teacher_leaves/index");
			}

			$data = array();
			$data['id'] = $id;
			$data['teacher_id'] = $teacher_id;
			$data['date_from'] = $date_from;
			$data['date_to'] = $date_to;
			$data['leave_type_id'] = $this->input->post('leave_type_id', true);
			$data['num_of_days'] = count($leave_days);
			$data['reason'] = $this->input->post('reason', true);
			$this->db->where('id', $data['id']);
			$this->db->update('tbl_teacher_staff_leave_applications', $data);

			//leave days save
			$leave_days_data = array();
			$j = 0;
			foreach ($leave_days as $day){
				$leave_days_data[$j]['leave_id'] = $id;
				$leave_days_data[$j]['teacher_id'] = $teacher_id;
				$leave_days_data[$j]['date'] = $day['date'];
				$j++;
			}
			$this->db->insert_batch('tbl_teacher_staff_leave_days', $leave_days_data);

			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect("teacher_leaves/index");
		}
		$data = array();
		$data['title'] = 'Teacher/Staff Leave';
		$data['heading_msg'] = "Teacher/Staff Leave";
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['is_show_button'] = "index";
		$data['leave_types'] = $this->db->query("SELECT * FROM tbl_leave_type")->result_array();
		$data['teachers'] = $this->db->query("SELECT * FROM tbl_teacher WHERE status = '1'")->result_array();
		$data['leave_info'] = $this->db->query("SELECT * FROM tbl_teacher_staff_leave_applications WHERE id = $id")->row();
		$data['maincontent'] = $this->load->view('teacher_leaves/edit', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}



	public function delete($id)
	{
		$this->db->delete('tbl_teacher_staff_leave_days', array('leave_id' => $id));
		$this->db->delete('tbl_teacher_staff_leave_applications', array('id' => $id));
		$sdata['message'] = $this->lang->line('delete_success_message');
		$this->session->set_userdata($sdata);
		redirect("teacher_leaves/index");
	}
}
