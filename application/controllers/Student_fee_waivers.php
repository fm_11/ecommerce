<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Student_fee_waivers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Student_fee', 'Admin_login', 'Message', 'Timekeeping'));
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');
        if (empty($user_info)) {
            $sdata = array();
            $sdata['message'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        //  echo  $this->lang->line('fee');
        //  die;
        $data = array();
        $data['title'] = $this->lang->line('students') .' ' . $this->lang->line('fees') .' ' . $this->lang->line('waiver');
        $data['heading_msg'] = $this->lang->line('students') .' ' . $this->lang->line('fees') .' ' . $this->lang->line('waiver');
        $data['is_show_button'] = "add";
        $cond = array();
        $this->load->library('pagination');
        $config['base_url'] = site_url('student_fee_waivers/index/');
        $config['per_page'] = 20;
        $config['total_rows'] = count($this->Student_fee->get_all_student_fee_waiver(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['fees'] = $this->Student_fee->get_all_student_fee_waiver(20, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('fee_waivers/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function delete($id)
    {
        $this->db->delete('tbl_student_fee_waiver', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("student_fee_waivers/index");
    }

    public function add()
    {
        $data = array();
        $data['title'] = $this->lang->line('students') .' ' . $this->lang->line('fees') .' ' . $this->lang->line('waiver');
        $data['heading_msg'] = $this->lang->line('students') .' ' . $this->lang->line('fees') .' ' . $this->lang->line('waiver');
        if ($_POST) {
            $year = $this->input->post("year");
            $group_id = $this->input->post("group_id");
            $fee_sub_category_id = $this->input->post("fee_sub_category_id");

            $class_shift_section_id =  $this->input->post("class_shift_section_id");
            $class_shift_section_arr = explode("-", $class_shift_section_id);
            $class_id  = $class_shift_section_arr[0];
            $shift_id = $class_shift_section_arr[1];
            $section_id = $class_shift_section_arr[2];

            $data['year'] = $year;
            $data['class_shift_section_id'] = $class_shift_section_id;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group_id'] = $group_id;
            $data['fee_sub_category_id'] = $fee_sub_category_id;
            $data['shift_id'] = $shift_id;
            $data['student_info'] = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code`,sl.`year`,
              sl.`amount`, sl.`is_percentage` FROM `tbl_student` AS s
LEFT JOIN `tbl_student_fee_waiver` AS sl ON (sl.`student_id` = s.`id` AND sl.`year` = '$year' AND sl.`fees_category_id` = '$fee_sub_category_id')
WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group_id'
AND s.`shift_id` = '$shift_id' AND s.`status` = '1' ORDER BY ABS(s.roll_no)")->result_array();
            $data['waiver_setting'] = $this->db->query("SELECT * FROM tbl_fee_waiver_setting")->result_array();
            $data['waivers_list'] = $this->load->view('fee_waivers/student_fee_waiver_add_form', $data, true);
        }
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
        $data['years'] = $this->Admin_login->getYearList(0, 0);
        $data['sub_categorys'] = $this->db->query("SELECT * FROM tbl_fee_sub_category WHERE is_waiver_applicable = '1'")->result_array();
        $data['groups'] = $this->Admin_login->getGroupList();
        $data['maincontent'] = $this->load->view('fee_waivers/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);

    }

    public function student_fee_waiver_data_save()
    {
        $year = $this->input->post("year");
        $loop_time = $this->input->post("loop_time");
        $class_id = $this->input->post("class_id");
        $section_id = $this->input->post("section_id");
        $group_id = $this->input->post("group_id");
        $shift_id = $this->input->post("shift_id");
        $fee_sub_category_id = $this->input->post("fee_sub_category_id");
        $this->db->delete('tbl_student_fee_waiver', array('class_id' => $class_id, 'section_id' => $section_id,
        'group_id' => $group_id, 'shift_id' => $shift_id, 'fees_category_id' => $fee_sub_category_id, 'year' => $year));
        $i = 1;
        while ($i <= $loop_time) {
            if ($this->input->post("is_allow_" . $i)) {
                $data = array();
                $data['student_id'] = $this->input->post("student_id_" . $i);
                $data['year'] = $year;
                $data['class_id'] = $class_id;
                $data['section_id'] = $section_id;
                $data['group_id'] = $group_id;
                $data['shift_id'] = $shift_id;
                $data['fees_category_id'] = $fee_sub_category_id;
                $data['date'] = date('Y-m-d');
                $data['is_percentage'] = $this->input->post("is_percentage_" . $i);
                $data['amount'] = $this->input->post("waiver_amount_" . $i);
                $this->db->insert('tbl_student_fee_waiver', $data);
            }
            $i++;
        }
        $sdata['message'] = $this->lang->line('add_success_message');
        $this->session->set_userdata($sdata);
        redirect("student_fee_waivers/index");
    }

    public function student_fee_waiver_from_total_amount_add()
    {
        if ($_POST) {
            $data = array();
            $data['title'] = 'Student Fee Waiver From Total Amount';
            $data['heading_msg'] = "Student Fee Waiver From Total Amount";
            $year = $this->input->post("year");
            $class_id = $this->input->post("class_id");
            $section_id = $this->input->post("section_id");
            $group = $this->input->post("group");
            $shift_id = $this->input->post("shift_id");
            $data['year'] = $year;
            $data['class_id'] = $class_id;
            $data['section_id'] = $section_id;
            $data['group'] = $group;
            $data['shift_id'] = $shift_id;
            $data['student_info'] = $this->db->query("SELECT s.id,s.`name`,s.`roll_no`,s.`student_code`,sl.`year`,sl.`amount` FROM `tbl_student` AS s
    LEFT JOIN `tbl_waiver_from_total_amount` AS sl ON sl.`student_id` = s.`id` AND sl.`year` = '$year'
    WHERE s.`class_id` = '$class_id' AND s.`section_id` = '$section_id' AND s.`group` = '$group' AND s.`shift_id` = '$shift_id' AND s.`status` = '1' ORDER BY s.roll_no")->result_array();
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['waiver_setting'] = $this->db->query("SELECT * FROM tbl_fee_waiver_setting")->result_array();
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['maincontent'] = $this->load->view('student_fees/student_fee_waiver_from_total_amount_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        } else {
            $data = array();
            $data['title'] = 'Student Fee Waiver From Total Amount';
            $data['heading_msg'] = "Student Fee Waiver From Total Amount";
            $config = $this->db->query("SELECT fee_module_type FROM tbl_config")->result_array();
            $data['fee_module_type'] = $config[0]['fee_module_type'];
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['class_list'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['shift_list'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
            $data['section_list'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
            $data['group_list'] = $this->Admin_login->getGroupList();
            $data['maincontent'] = $this->load->view('student_fees/student_fee_waiver_from_total_amount_add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function student_fee_waiver_from_total_amount_data_save()
    {
        $year = $this->input->post("year");
        $loop_time = $this->input->post("loop_time");
        $class_id = $this->input->post("class_id");
        $section_id = $this->input->post("section_id");
        $group = $this->input->post("group");
        $this->db->delete('tbl_waiver_from_total_amount', array('class_id' => $class_id, 'section_id' => $section_id, 'group' => $group, 'year' => $year));
        $i = 1;
        while ($i <= $loop_time) {
            if ($this->input->post("is_allow_" . $i)) {
                $data = array();
                $data['student_id'] = $this->input->post("student_id_" . $i);
                $data['year'] = $year;
                $data['class_id'] = $class_id;
                $data['section_id'] = $section_id;
                $data['group'] = $group;
                $data['date'] = date('Y-m-d');
                $data['amount'] = $this->input->post("waiver_amount_" . $i);
                $this->db->insert('tbl_waiver_from_total_amount', $data);
            }
            $i++;
        }
        $sdata['message'] = "Student Waiver Data Successfully Updated.";
        $this->session->set_userdata($sdata);
        redirect("student_fees/student_fee_waiver_index");
    }
}
