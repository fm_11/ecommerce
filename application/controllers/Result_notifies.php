<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Result_notifies extends CI_Controller
{

    public $SOFTWARE_START_YEAR = '';

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    function to_result_notify()
    {
        $data = array();
        if ($_POST) {
            $data['class_id'] = $this->input->post('txtClass', true);
            $data['section_id'] = $this->input->post('txtSection', true);
            $data['group_id'] = $this->input->post('txtGroup', true);
            $data['shift_id'] = $this->input->post('txtShift', true);
            $data['exam_id'] = $this->input->post('txtExam', true);
            $data['sms_type'] = $this->input->post('txtSMSType', true);
            $data['with_school_name'] = $this->input->post('txtWithSchoolName', true);
            $data['student_info'] = $this->Message->get_student_info_by_student_id($data);
        }
        $data['title'] = 'Result Notify SMS Send';
        $data['heading_msg'] = "Result Notify SMS Send";
        $data['class'] = $this->db->query("SELECT * FROM tbl_class")->result_array();
        $data['section'] = $this->db->query("SELECT * FROM tbl_section")->result_array();
        $data['group'] = $this->db->query("SELECT * FROM tbl_student_group")->result_array();
        $data['shift'] = $this->db->query("SELECT * FROM tbl_shift")->result_array();
        $data['exam'] = $this->db->query("SELECT * FROM tbl_exam")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('result_notifies/result_notify_sms_send_form', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
