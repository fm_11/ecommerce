<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Customers extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Customer Information';
        $data['heading_msg'] = "Customer Information";
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['customers'] = $this->db->query("SELECT * FROM tbl_customers")->result_array();
        $data['maincontent'] = $this->load->view('customers/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $_POST['name'];
            $data['phone'] = $_POST['phone'];
            $data['email'] = $_POST['email'];
            $data['address'] = $_POST['address'];
            $data['password'] = $_POST['password'];
            $this->db->insert("tbl_customers", $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect('customers/index');
        }
        $data = array();
        $data['title'] = 'Add Customer Information';
        $data['heading_msg'] = "Add Customer Information";
        $data['is_show_button'] = "index";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('customers/add', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['phone'] = $_POST['phone'];
            $data['email'] = $_POST['email'];
            $data['address'] = $_POST['address'];
            $data['password'] = $_POST['password'];
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_customers', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect('customers/index');
        }
        $data = array();
        $data['title'] = 'Update Customer Information';
        $data['heading_msg'] = "Update Customer Information";
        $data['is_show_button'] = "index";
        $data['customers'] = $this->db->query("SELECT * FROM `tbl_customers` WHERE id = '$id'")->result_array();
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['maincontent'] = $this->load->view('customers/edit', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }


    public function delete($id)
    {
        $this->db->delete('tbl_customers', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect('customers/index');
    }
}
