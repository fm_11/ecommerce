<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Headmaster_lists extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data['title'] = 'All Headmasters';
        $data['heading_msg'] = 'All Headmasters';
        $data['is_show_button'] = "add";
        $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
        $data['all_headmasters'] = $this->db->query("SELECT * FROM tbl_all_headmasters ORDER BY id ASC")->result_array();
        $data['maincontent'] = $this->load->view('headmaster_lists/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function add()
    {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('txtName', true);
            $data['edu_qua'] = $this->input->post('txtEduQua', true);
            $data['from_date'] = $this->input->post('txtFromDate', true);
            $to_date = $this->input->post('txtToDate', true);
            if($to_date == ''){
				$data['to_date'] = NULL;
			}else{
				$data['to_date'] = $to_date;
			}
            $this->db->insert('tbl_all_headmasters', $data);
            $sdata['message'] = $this->lang->line('add_success_message');
            $this->session->set_userdata($sdata);
            redirect("headmaster_lists/index");
        } else {
            $data = array();
            $data['title'] = 'All Headmasters';
            $data['heading_msg'] = "All Headmasters";
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['action'] = '';
            $data['maincontent'] = $this->load->view('headmaster_lists/add', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function edit($id = null)
    {
        if ($_POST) {
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['name'] = $this->input->post('txtName', true);
            $data['edu_qua'] = $this->input->post('txtEduQua', true);
            $data['from_date'] = $this->input->post('txtFromDate', true);
			$to_date = $this->input->post('txtToDate', true);
			if($to_date == ''){
				$data['to_date'] = NULL;
			}else{
				$data['to_date'] = $to_date;
			}
            $this->db->where('id', $data['id']);
            $this->db->update('tbl_all_headmasters', $data);
            $sdata['message'] = $this->lang->line('edit_success_message');
            $this->session->set_userdata($sdata);
            redirect("headmaster_lists/index");
        } else {
            $data = array();
            $data['title'] = 'All Headmasters';
            $data['heading_msg'] = "All Headmasters";
            $data['is_show_button'] = "index";
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $data['action'] = 'edit';
            $data['headmasters'] = $this->db->query("SELECT * FROM tbl_all_headmasters WHERE id = '$id'")->result_array();
            $data['maincontent'] = $this->load->view('headmaster_lists/edit', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }


    public function delete($id)
    {
        $this->db->delete('tbl_all_headmasters', array('id' => $id));
        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("headmaster_lists/index");
    }

}

?>
