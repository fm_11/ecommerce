<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admissions extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Admin_login','Student'));
    }

    public function index()
    {
        $data = array();
        $data['title'] = 'Admission';
        $data['heading_msg'] = "Admission";
        $data['top_menu'] = $this->load->view('admissions/admissions_menu', '', true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    public function get_admission_contact()
    {
        if ($_POST) {
            $allowedTags='<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
            $allowedTags.='<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
            $sContent = strip_tags(stripslashes($_POST['txtNews']),$allowedTags);
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['title'] = $this->input->post('txtTitle', true);
            $data['contact_info'] = $sContent;
            $this->Admin_login->add_contact_info_update($data);
            $sdata['message'] = "You are Successfully Contact Info. Updated.";
            $this->session->set_userdata($sdata);
            redirect("admissions/get_admission_contact");
        } else {
            $data = array();
            $data['title'] = 'Admission Contact Info.';
            $data['heading_msg'] = "Admission Contact Info.";
            $data['add_contact'] = $this->db->query("SELECT * FROM `tbl_admission_contact`")->row();
            $data['top_menu'] = $this->load->view('admissions/admissions_menu', '', true);
            $data['maincontent'] = $this->load->view('admissions/add_contact_edit_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function get_admission_process()
    {
        if ($_POST) {
            $allowedTags='<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
            $allowedTags.='<li><ol><ul><span><div><br><ins><del><table><tr><td><th><a>';
            $sContent = strip_tags(stripslashes($_POST['txtDes']),$allowedTags);
            $data = array();
            $data['id'] = $this->input->post('id', true);
            $data['title'] = $this->input->post('txtTitle', true);
            $data['description'] = $sContent;
            $this->Admin_login->add_process_info_update($data);
            $sdata['message'] = "You are Successfully Admission Process Info. Updated.";
            $this->session->set_userdata($sdata);
            redirect("admissions/get_admission_process");
        } else {
            $data = array();
            $data['title'] = 'Admission Process';
            $data['heading_msg'] = "Admission Process";
            $data['add_process'] = $this->db->query("SELECT * FROM `tbl_admission_process`")->row();
            $data['top_menu'] = $this->load->view('admissions/admissions_menu', '', true);
            $data['maincontent'] = $this->load->view('admissions/add_process_edit_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    function get_admission_form_list(){
        $data = array();
        $data['title'] = 'Admission Form';
        $data['heading_msg'] = "Admission Form";
        $data['add_form'] = $this->db->query("SELECT af.*,c.`name` AS class_name FROM `tbl_admission_form` AS af
LEFT JOIN `tbl_class` AS c ON af.`class_id` = c.`id`")->result_array();
        $data['top_menu'] = $this->load->view('admissions/admissions_menu', '', true);
        $data['maincontent'] = $this->load->view('admissions/admission_form_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function admission_form_add(){
        if ($_POST) {
            $this->load->library('upload');
            $config['upload_path'] = MEDIA_FOLDER .  '/add_form/';
            $config['allowed_types'] = 'pdf|doc|png|JPEG|jpeg|jpg|docx|xls';
            $config['max_size'] = '60000';
            $this->upload->initialize($config);
            $extension = strtolower(substr(strrchr($_FILES['txtFile']['name'], '.'), 1));
            foreach ($_FILES as $field => $file) {
                if ($file['error'] == 0) {
                    $new_file_name = "ADD_FORM_" . time() . "_" . date('Y-m-d') . "." . $extension;
                    if (move_uploaded_file($_FILES["txtFile"]["tmp_name"], MEDIA_FOLDER . "/add_form/" . $new_file_name)) {
                        $data = array();
                        $data['class_id'] = $this->input->post('txtClassName', true);
                        $data['form_name'] = $this->input->post('txtFormName', true);
                        $data['date'] = $this->input->post('txtDate', true);
                        $data['location'] = $new_file_name;
                        $this->Admin_login->add_admission_form($data);
                        $sdata['message'] = "You are Successfully Form Added ! ";
                        $this->session->set_userdata($sdata);
                        redirect("admissions/admission_form_add");
                    } else {
                        $sdata['exception'] = "Sorry Form Doesn't Upload !" . $this->upload->display_errors();
                        $this->session->set_userdata($sdata);
                        redirect("admissions/admission_form_add");
                    }
                } else {
                    $sdata['exception'] = "Sorry Form Doesn't Upload !";
                    $this->session->set_userdata($sdata);
                    redirect("admissions/admission_form_add");
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Admission Form';
            $data['heading_msg'] = "Add New Admission Form";
            $data['top_menu'] = $this->load->view('admissions/admissions_menu', '', true);
            $data['class'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
            $data['maincontent'] = $this->load->view('admissions/admission_form_add_form', $data, true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    function admission_form_delete($id){
        $form_file_info = $this->db->where('id', $id)->get('tbl_admission_form')->result_array();
        $file = $form_file_info[0]['location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/add_form/' . $file;
            if (unlink($filedel)) {
                $this->db->delete('tbl_admission_form', array('id' => $id));
            } else {
                $this->db->delete('tbl_admission_form', array('id' => $id));
            }
        } else {
            $this->db->delete('tbl_admission_form', array('id' => $id));
        }

        $sdata['message'] = "Form Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("admissions/get_admission_form_list");
    }

    function get_apply_student_list(){
        $data = array();
        $data['title'] = 'Applied Student List';
        $data['heading_msg'] = "Applied Student List";
        $cond = array();
        if($_POST){
            $class_id = $this->input->post("txtClass");
            $pin = $this->input->post("txtPIN");
            $year = $this->input->post("txtYear");
            if($class_id != ''){
                $sdata['class_id'] = $class_id;
                $this->session->set_userdata($sdata);
                $cond['class_id'] = $class_id;
            }else{
                $sdata['class_id'] = "";
                $this->session->set_userdata($sdata);
                $cond['class_id'] = "";
            }
            if($pin != ''){
                $sdata['pin'] = $pin;
                $this->session->set_userdata($sdata);
                $cond['pin'] = $pin;
            }else{
                $sdata['pin'] = "";
                $this->session->set_userdata($sdata);
                $cond['pin'] = "";
            }
            if($year != ''){
                $sdata['year'] = $year;
                $this->session->set_userdata($sdata);
                $cond['year'] = $year;
            }else{
                $sdata['year'] = "";
                $this->session->set_userdata($sdata);
                $cond['year'] = "";
            }
        } else {
            $cond['class_id'] = "";
            $cond['pin'] = "";
            $cond['year'] = "";
            $sdata['class_id'] = $cond['class_id'];
            $sdata['pin'] = $cond['pin'];
            $sdata['year'] = $cond['year'];
            $this->session->set_userdata($sdata);
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('admissions/get_apply_student_list/');
        $config['per_page'] = 15;
        $config['total_rows'] = count($this->Student->get_all_applied_student(0, 0, $cond));
        $this->pagination->initialize($config);
        $data['students'] = $this->Student->get_all_applied_student(15, (int)$this->uri->segment(3), $cond);
        $data['counter'] = (int)$this->uri->segment(3);
        $data['class'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
        $data['top_menu'] = $this->load->view('admissions/admissions_menu', '', true);
        $data['maincontent'] = $this->load->view('admissions/applied_student_list', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

    function updateMsgStatusAppliedStudentStatus(){
        $payment_status = $this->input->get('payment_status', true);
        $id = $this->input->get('id', true);
        $data = array();
        $data['id'] = $this->input->get('id', true);
        if ($payment_status == 1) {
            $data['payment_status'] = 0;
        } else {
            $data['payment_status'] = 1;
        }
        $this->db->where('id', $data['id']);
        $this->db->update('tbl_admission', $data);
        if ($payment_status == 0) {
            echo '<a class="approve_icon" title="Approve" href="#" onclick="msgStatusUpdate(' . $id . ',1)"></a>';
        } else {
            echo '<a class="reject_icon" title="Reject" href="#" onclick="msgStatusUpdate(' . $id . ',0)"></a>';
        }
    }
	
	
	function admission_fee_submitted(){
   /* $data = array();
   echo "<pre>";
   print_r($_POST);
   echo "</pre>"; 
   die(); */
   $admissionFee = $this->input->post('admissionFee');
   $id = $this->input->post('id');
   $sql = "UPDATE tbl_admission SET payment_amount='".$admissionFee."',payment_status='1',payment_recive_datetime=NOW() WHERE id=".$id;
      $update = $this->db->query($sql);
   $afftectedRows = $this->db->affected_rows();
        $sdata['message'] = "You are Successfully Payment Information Updated.";
        $this->session->set_userdata($sdata);
        $this->applied_student_view($id);
       /*  $data['title'] = 'Student Details';
        $data['heading_msg'] = "Applied Student Details";
        $data['top_menu'] = $this->load->view('admissions/admissions_menu', '', true);
        $data['student_info'] = $this->db->query("SELECT a.*,c.`name` AS class_name FROM `tbl_admission` AS a
LEFT JOIN `tbl_class` AS c ON a.`class_id` = c.`id` WHERE a.`id` = '$id'")->result_array();

        $data['maincontent'] = $this->load->view('admissions/applied_student_details', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data); */  
 }
	

    function applied_student_delete($id){
        $student_photo_info = $this->db->where('id', $id)->get('tbl_admission')->result_array();
        $file = $student_photo_info[0]['photo'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/student/' . $file;
            if (unlink($filedel)) {
                $this->db->delete('tbl_admission', array('id' => $id));
            } else {
                $this->db->delete('tbl_admission', array('id' => $id));
            }
        } else {
            $this->db->delete('tbl_admission', array('id' => $id));
        }

        $sdata['message'] = "Applied Student Deleted Successfully !";
        $this->session->set_userdata($sdata);
        redirect("admissions/get_apply_student_list");
    }

    function applied_student_view($id){
        $data = array();
        $data['title'] = 'Student Details';
        $data['heading_msg'] = "Applied Student Details";
        $data['top_menu'] = $this->load->view('admissions/admissions_menu', '', true);
        $data['student_info'] = $this->db->query("SELECT a.*,c.`name` AS class_name,g.name as group_name FROM `tbl_admission` AS a
LEFT JOIN `tbl_class` AS c ON a.`class_id` = c.`id` LEFT JOIN `tbl_student_group` AS g ON a.`group` = g.`id` WHERE a.`id` = '$id'")->result_array();
        $data['maincontent'] = $this->load->view('admissions/applied_student_details', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }
}
