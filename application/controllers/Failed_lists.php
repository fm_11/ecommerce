<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Failed_lists extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student', 'Admin_login'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = 'Fail List Subject Details';
		$data['heading_msg'] = "Fail List Subject Details";
		if ($_POST) {
			$year = $_POST['year'];
			$exam_id = $_POST['exam_id'];
			$class_shift_section_id = $_POST['class_shift_section_id'];
			$sdata['exam_id'] = $exam_id;
			$sdata['year'] = $year;
			$sdata['class_shift_section_id'] = $class_shift_section_id;
			$this->session->set_userdata($sdata);

			$class_shift_section_id = $_POST['class_shift_section_id'];
			$data['class_shift_section_id'] = $class_shift_section_id;
			$data['exam_id'] = $exam_id;
			$data['year'] = $year;
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$class_id  = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section_id = $class_shift_section_arr[2];

			$data['exam_list'] = $this->db->query("SELECT * FROM tbl_exam WHERE year ='$year'")->result_array();

			$class_info = $this->db->query("SELECT name FROM tbl_class WHERE id ='$class_id'")->result_array();
			$shift_info = $this->db->query("SELECT name FROM tbl_shift WHERE id ='$shift_id'")->result_array();
			$section_info = $this->db->query("SELECT name FROM tbl_section WHERE id ='$section_id'")->result_array();
			$data['exam_info'] = $this->db->query("SELECT * FROM tbl_exam WHERE id ='$exam_id'")->result_array();
			$data['class_shift_section_name'] = $class_info[0]['name'] . '-' .  $shift_info[0]['name'] . '-' . $section_info[0]['name'];

			$data['failed_data'] =  $this->db->query("SELECT s.`name`,s.`student_code`,r.`roll_no`,rd.`student_id`,
									GROUP_CONCAT(DISTINCT CONCAT(rd.`subject_name`,' (' , ROUND(rd.`total_obtain`,2) , ')') 
									ORDER BY rd.`subject_name` DESC) AS failed_subjects
									FROM `tbl_result_process_details` AS rd
									INNER JOIN `tbl_result_process` AS r ON r.`id` = rd.`result_id`
									INNER JOIN `tbl_student` AS s ON s.`id` = r.`student_id`
									WHERE rd.`alpha_gpa` = 'F' AND rd.`class_id` = $class_id AND rd.`shift` = $shift_id AND rd.`section_id` = $section_id
									GROUP BY rd.`student_id` ORDER BY ABS(r.`roll_no`);")->result_array();

			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$data['HeaderInfo'] = $SchoolInfo;
			$data['report'] = $this->load->view('failed_lists/report', $data, true);
		}

		if (isset($_POST['pdf_download'])) {
			$data['is_pdf'] = 1;
			//Dom PDF
			$this->load->library('mydompdf');
			$html = $this->load->view('failed_lists/report', $data, true);
			$file_name = $data['title'];
			$this->mydompdf->createPDF($html, $file_name, true, 'A4', 'portrait');
			//Dom PDF
		}else{
			$data['title'] = 'Fail List Subject Details';
			$data['heading_msg'] = "Fail List Subject Details";
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
			$data['years'] = $this->Admin_login->getYearList(0, 0);
			$data['group_list'] = $this->Admin_login->getGroupList();
			$data['maincontent'] = $this->load->view('failed_lists/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}
}
