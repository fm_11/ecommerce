<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Slides extends CI_Controller
{
    public $SOFTWARE_START_YEAR = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model(array('Timekeeping','Message', 'common/insert_model', 'common/custom_methods_model'));
        $this->load->library('session');
        date_default_timezone_set('Asia/Dhaka');
        $user_info = $this->session->userdata('user_info');

        if (empty($user_info)) {
            $sdata = array();
            $sdata['exception'] = "Please Login Vaild User !";
            $this->session->set_userdata($sdata);
            redirect("login/index");
        }
        $this->notification = array();
    }

    public function index()
    {
        $data['title'] = 'Slide Images';
        $data['heading_msg'] = 'Slide Images';
        $data['is_show_button'] = "add";
        $data['images'] = $this->db->query("SELECT * FROM tbl_slide_images ORDER BY id DESC")->result_array();
    		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
    		$data['maincontent'] = $this->load->view('slides/index', $data, true);
        $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
    }

	public function updateMsgStatusSlideImageStatus()
	{
		$is_view = $this->input->get('is_view', true);
		$id = $this->input->get('id', true);
		$data = array();
		$data['id'] = $this->input->get('id', true);
		if ($is_view == 1) {
			$data['is_view'] = 0;
		} else {
			$data['is_view'] = 1;
		}
		$this->db->where('id', $data['id']);
		$this->db->update('tbl_slide_images', $data);
		if ($is_view == 0) {
			echo '<a class="deleteTag" title="Approve" href="#" onclick="msgStatusUpdate(' . $id . ',1)"><i class="ti-check-box"></i></a>';
		} else {
			echo '<a class="deleteTag" title="Reject" href="#" onclick="msgStatusUpdate(' . $id . ',0)"><i class="ti-na"></i></a>';
		}
	}

    public function add()
    {
        if ($_POST) {
            $total_slide_image = count($this->db->query("SELECT * FROM tbl_slide_images")->result_array());
            if ($total_slide_image >= 10) {
                $sdata['exception'] = "Sorry Slide Image Doesn't Upload.Because already 10 slide image Exist.Please Delete one and Upload !";
                $this->session->set_userdata($sdata);
                redirect("dashboard/slide_image_add");
            } else {
                $this->load->library('upload');
                $config['upload_path'] = MEDIA_FOLDER . '/website/images/slide/';
                $config['allowed_types'] = 'png|JPEG|jpeg|jpg';
                $config['max_height'] = '0';
                $config['max_width'] = '0';
                $config['max_size'] = '0';

                $this->upload->initialize($config);
                $extension = strtolower(substr(strrchr($_FILES['txtPhoto']['name'], '.'), 1));
                // echo '<pre>';
                // print_r($_FILES);
                // die;
                foreach ($_FILES as $field => $file) {
                    if ($file['error'] == 0) {
                        $new_photo_name = "SL_" . time() . "_" . date('Y-m-d') . "." . $extension;
                        if (move_uploaded_file($_FILES["txtPhoto"]["tmp_name"], MEDIA_FOLDER . "/website/images/slide/" . $new_photo_name)) {
                            $data = array();
                            $data['title'] = $this->input->post('txtTitle', true);
                            $data['date'] = $this->input->post('txtDate', true);
                            $data['text_1'] = $this->input->post('text_1', true);
                            $data['text_2'] = $this->input->post('text_2', true);
                            $data['is_view'] = 0;
                            $data['photo_location'] = $new_photo_name;
                            $this->db->insert('tbl_slide_images', $data);
                            $sdata['message'] = $this->lang->line('add_success_message');
                            $this->session->set_userdata($sdata);
                            redirect("slides/index");
                        } else {
                          echo $this->upload->display_errors();
                          die;
                            $sdata['exception'] = "Sorry Slide Image Doesn't Upload.Please Check Image Size or Height Width !" . $this->upload->display_errors();
                            $this->session->set_userdata($sdata);
                            redirect("slides/add");
                        }
                    } else {
                        $sdata['exception'] = "Sorry Slide Image Doesn't Upload.Please Check Image Size or Height Width !";
                        $this->session->set_userdata($sdata);
                        redirect("slides/add");
                    }
                }
            }
        } else {
            $data = array();
            $data['title'] = 'Slide Image';
            $data['heading_msg'] = "Add Slide Image";
            $data['is_show_button'] = "index";
			      $data['maincontent'] = $this->load->view('slides/add', $data, true);
            $data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
            $this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
        }
    }

    public function delete($id)
    {
        $image_info = $this->db->where('id', $id)->get('tbl_slide_images')->result_array();
        $file = $image_info[0]['photo_location'];
        if (!empty($file)) {
            $filedel = PUBPATH . MEDIA_FOLDER . '/images/slide/' . $file;
            if (unlink($filedel)) {
                $this->db->delete('tbl_slide_images', array('id' => $id));
            } else {
                $this->db->delete('tbl_slide_images', array('id' => $id));
            }
        } else {
            $this->db->delete('tbl_slide_images', array('id' => $id));
        }

        $sdata['message'] = $this->lang->line('delete_success_message');
        $this->session->set_userdata($sdata);
        redirect("slides/index");
    }

}

?>
