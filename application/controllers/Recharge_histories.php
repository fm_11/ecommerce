<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Recharge_histories extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student_fee', 'Admin_login', 'Student', 'Account'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		if (empty($user_info)) {
			$sdata = array();
			$sdata['message'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['title'] = "Recharge Histories";
		$data['heading_msg'] = "Recharge Histories";
		if ($_POST) {
			// echo '<pre>';
			// print_r($_POST);
			// die;
			$SchoolInfo = $this->Admin_login->fetReportHeader();
			$Info = array();
			$Info['school_name'] = $SchoolInfo[0]['school_name'];
			$Info['eiin_number'] = $SchoolInfo[0]['eiin_number'];
			$Info['address'] = $SchoolInfo[0]['address'];
			$data['HeaderInfo'] = $Info;
			$from_date = $this->input->post("from_date");
			$to_date = $this->input->post("to_date");

			$data['from_date'] = $from_date;
			$data['to_date'] = $to_date;

			$data['recharge_histories'] = $this->db->query("SELECT * FROM `tbl_sms_balance` WHERE 
											DATE(`added_on`) BETWEEN '$from_date' AND '$to_date'")->result_array();
			$data['report'] = $this->load->view('recharge_histories/report', $data, true);
		}
		if (isset($_POST['pdf_download'])) {
			$data['is_pdf'] = 1;
			//Dom PDF
			$this->load->library('mydompdf');
			$html = $this->load->view('recharge_histories/report', $data, true);
			$this->mydompdf->createPDF($html, 'RechargeHistories', true, 'A4', 'portrait');
			//Dom PDF
		} else {
			$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
			$data['maincontent'] = $this->load->view('recharge_histories/index', $data, true);
			$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
		}
	}
}

