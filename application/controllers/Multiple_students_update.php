<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Multiple_students_update extends CI_Controller
{
	public $SOFTWARE_START_YEAR = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model(array('Student', 'Message', 'Admin_login', 'common/custom_methods_model', 'Student_fee'));
		$this->load->library('session');
		$user_info = $this->session->userdata('user_info');

		//For Excel Read
		require_once APPPATH . 'third_party/PHPExcel.php';
		$this->excel = new PHPExcel();
		//For Excel Read


		if (empty($user_info)) {
			$sdata = array();
			$sdata['exception'] = "Please Login Vaild User !";
			$this->session->set_userdata($sdata);
			redirect("login/index");
		}
		//echo '<pre>'; print_r($user_info); die;
		$this->SOFTWARE_START_YEAR = $user_info[0]->config_info[0]['software_start_year'];
		$this->notification = array();
	}

	public function index()
	{
		$data = array();
		$data['categories'] = $this->Student->getStudentCategory();
		$data['group_list'] = $this->Admin_login->getGroupList();
		if ($_POST) {
			$class_shift_section_id = $this->input->post('class_shift_section_id');
			$class_shift_section_arr = explode("-", $class_shift_section_id);
			$data['class_shift_section_id'] = $class_shift_section_id;
			$class = $class_shift_section_arr[0];
			$shift_id = $class_shift_section_arr[1];
			$section = $class_shift_section_arr[2];

			$group = $this->input->post('group', true);
			$data['group_id'] = $group;

			$category_id = $this->input->post('category_id', true);
			$data['category_id'] = $category_id;

			$data['title'] = 'Multiple Student Information Update';
			$data['heading_msg'] = "Multiple Student Information Update";
			$data['section'] = $this->db->query("SELECT * FROM `tbl_section`")->result_array();
			$data['class'] = $this->db->query("SELECT * FROM `tbl_class`")->result_array();
			$data['shift'] = $this->db->query("SELECT * FROM `tbl_shift`")->result_array();
			$data['blood_group_list'] = $this->db->query("SELECT * FROM `tbl_blood_group`")->result_array();
			$data['religions'] = $this->Admin_login->getReligionList();
			$config = $this->db->query("SELECT * FROM `tbl_config`")->result_array();
			if (empty($config)) {
				$data['is_transport_fee_applicable'] = 0;
			} else {
				$data['is_transport_fee_applicable'] = $config[0]['is_transport_fee_applicable'];
			}

			$data['students'] = $this->db->query("SELECT * FROM tbl_student 
                                WHERE class_id = $class AND section_id = $section
                                AND shift_id = $shift_id AND `group` = $group AND category_id = $category_id
                                AND status = '1' ORDER BY ABS(roll_no)")->result_array();

			$data['data_table'] = $this->load->view('multiple_students_update/multiple_student_update_form', $data, true);
		}

		$data['title'] = 'Multiple Student Update';
		$data['heading_msg'] = "Multiple Student Update";
		$data['class_section_shift_marge_list'] = $this->Admin_login->class_section_shift_marge_list();
		$data['main_menu'] = $this->load->view('admin_logins/main_menu_' . $this->session->userdata('site_menu'), '', true);
		$data['maincontent'] = $this->load->view('multiple_students_update/index', $data, true);
		$this->load->view('admin_logins/index_' . $this->session->userdata('site_menu'), $data);
	}

	public function data_save()
	{
		if ($_POST) {
			//echo count($_POST);
			//echo phpinfo(); die;

			$config = $this->db->query("SELECT * FROM `tbl_config`")->result_array();
			if (empty($config)) {
				$is_transport_fee_applicable = 0;
			} else {
				$is_transport_fee_applicable = $config[0]['is_transport_fee_applicable'];
			}

			//echo '<pre>';
			//print_r($_POST); die;
			$loop_time = $this->input->post("loop_time");
			$i = 1;
			while ($i <= $loop_time) {
				if ($this->input->post("is_allow_" . $i)) {
					$data = array();
					$data['id'] = $this->input->post("id_" . $i);
					$data['name'] = $this->input->post("name_" . $i);
					$data['roll_no'] = $this->input->post("roll_no_" . $i);
					$data['father_name'] = $this->input->post("father_name_" . $i);
					$data['mother_name'] = $this->input->post("mother_name_" . $i);
					$data['section_id'] = $this->input->post("section_id_" . $i);
					$data['group'] = $this->input->post("group_" . $i);
					$data['category_id'] = $this->input->post("category_id_" . $i);

					$data['shift_id'] = $this->input->post("shift_" . $i);
					$data['process_code'] = $this->input->post("process_code_" . $i);

					if ($is_transport_fee_applicable != '0') {
						$data['transport_fee_amount'] = $this->input->post("transport_fee_amount_" . $i);
					}

					$data['gender'] = $this->input->post("gender_" . $i);
					$data['blood_group_id'] = $this->input->post("blood_group_id_" . $i);
					$data['religion'] = $this->input->post("religion_" . $i);
					$data['guardian_mobile'] = $this->input->post("guardian_mobile_" . $i);
					$this->db->where('id', $data['id']);
					$this->db->update('tbl_student', $data);
				}
				$i++;
			}
			$sdata['message'] = $this->lang->line('edit_success_message');
			$this->session->set_userdata($sdata);
			redirect("multiple_students_update/index");
		}
	}


}
